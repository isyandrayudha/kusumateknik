package com.msf.project.sales1crm.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.adapter.ActivityCallsAdapter;
import com.msf.project.sales1crm.adapter.ActivityEventListAdapter;
import com.msf.project.sales1crm.adapter.ActivityTodoAdapter;
import com.msf.project.sales1crm.customview.CustomEditText;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.model.ActivityCallsModel;
import com.msf.project.sales1crm.model.ActivityEventModel;
import com.msf.project.sales1crm.model.ActivityTodoModel;
import com.msf.project.sales1crm.model.Response;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ActivityDuaFragment extends Fragment {

    @BindView(R.id.etSearch)
    CustomEditText etSearch;
    @BindView(R.id.ivClear)
    ImageView ivClear;
    @BindView(R.id.tvRefresh)
    CustomTextView tvRefresh;
    @BindView(R.id.rlNoConnection)
    RelativeLayout rlNoConnection;
    @BindView(R.id.event)
    CustomTextView event;
    @BindView(R.id.lcEvent)
    RecyclerView lcEvent;
    @BindView(R.id.lcTodo)
    RecyclerView lcTodo;
    @BindView(R.id.lcCalls)
    RecyclerView lcCalls;
    Unbinder unbinder;
    private View view;
    private Context context;
    private Response activityModel;
    private ActivityEventModel eventModel;
    private ActivityTodoModel todoModel;
    private ActivityCallsModel callsModel;

    private List<ActivityEventModel> eventModelList = new ArrayList<>();
    private List<ActivityTodoModel> todoModelList = new ArrayList<>();
    private List<ActivityCallsModel> callsModelList = new ArrayList<>();

    private ActivityEventListAdapter eventListAdapter;
    private ActivityTodoAdapter todoAdapter;
    private ActivityCallsAdapter callsAdapter;

    public static final int RESULT_OK = -1;


    public static ActivityDuaFragment newInstance() {
        ActivityDuaFragment fragment = new ActivityDuaFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_activitylist, container, false);

        unbinder = ButterKnife.bind(this, this.view);
        return this.view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);
        this.context = getActivity();

        initView();
    }

    private void initView(){
        try{
            if (getArguments() != null){
                activityModel = getArguments().getParcelable("ApiKey");
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
        try{
            JSONArray array = new JSONArray(activityModel.getEvent());
            Log.d("TAG", "EventActivity : " + activityModel.getEvent());
            Log.d("TAG", "Event array : " + array.length());
            for(int i = 0; i < array.length(); i++) {
                eventModel = new ActivityEventModel();
                eventModel.setId(array.getJSONObject(i).getString("id"));
                eventModel.setName(array.getJSONObject(i).getString("name"));
                eventModel.setAccountId(array.getJSONObject(i).getString("account_id"));
                eventModel.setAssignTo(array.getJSONObject(i).getString("assign_to"));
                eventModel.setDate(array.getJSONObject(i).getString("date"));
                eventModel.setDescription(array.getJSONObject(i).getString("description"));
                eventModel.setStatus(array.getJSONObject(i).getString("status"));
                eventModel.setGoogleCalendarId(array.getJSONObject(i).getString("google_calendar_id"));
                eventModel.setMicrosoftCalendarId(array.getJSONObject(i).getString("microsoft_calendar_id"));
                eventModel.setCreatedAt(array.getJSONObject(i).getString("created_at"));
                eventModel.setCreatedBy(array.getJSONObject(i).getString("created_by"));
                eventModel.setUpdatedAt(array.getJSONObject(i).getString("updated_at"));
                eventModel.setUpdatedBy(array.getJSONObject(i).getString("updated_by"));
                eventModel.setDeletedAt(array.getJSONObject(i).getString("deleted_at"));
                eventModel.setDeletedBy(array.getJSONObject(i).getString("deleted_by"));
                eventModelList.add(eventModel);
            }

        } catch (JSONException e){
            e.printStackTrace();
        }

        if (eventModelList.size() >0) {
            setActivityEventList();
        }

    }

    private void setActivityEventList() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false);
        lcEvent.setLayoutManager(layoutManager);

        eventListAdapter = new ActivityEventListAdapter(context,this.eventModelList);
        lcEvent.setAdapter(eventListAdapter);

    }

    private void setActivityTodoList(){

    }
    private void setActivityCallsList(){

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
