package com.msf.project.sales1crm.presenter;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;

import com.msf.project.sales1crm.callback.ActivityTodayCallback;
import com.msf.project.sales1crm.model.ActivityCallsModel;
import com.msf.project.sales1crm.model.ActivityEventModel;
import com.msf.project.sales1crm.model.ActivityTodoModel;
import com.msf.project.sales1crm.service.NetworkConnection;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.PreferenceUtility;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;
import com.msf.project.sales1crm.utility.Sales1CRMUtilsJSON;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ActivityTodayPresenter {
    private final String TAG = ActivityTodayPresenter.class.getSimpleName();
    private Context context;
    private Messenger messenger;
    private Message message;
    private Bundle bundle;
    private Handler handler;
    private String failureResponse = "";
    private String[] stringResponse = {""};
    private ActivityTodayCallback callback;
    private String result= "NG";
    private List<ActivityEventModel> aEventModels= new ArrayList<>();
    private List<ActivityTodoModel> aTodoModels = new ArrayList<>();
    private List<ActivityCallsModel> aCallsModels = new ArrayList<>();

    public ActivityTodayPresenter(Context context, ActivityTodayCallback callback){
        this.context = context;
        this.callback = callback;
    }

    public void doNetworkService(String url,String params, String from) {
        Log.d(TAG,"url activity TODAY : " + url);
        Log.d(TAG,"params : " + params);
        Intent i = new Intent(this.context, NetworkConnection.class);
        this.messenger = new Messenger(this.handler);
        i.putExtra("messenger", this.messenger);
        i.putExtra("url", url);
        i.putExtra("params", params);
        i.putExtra("from", from);
        this.context.startService(i);
    }


    public void setupEventToday(int apiIndex) {
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)) {
            obtainEvenTodayItem(apiIndex);
        }else{
            callback.finishAEventToday(this.result, this.aEventModels);
        }
    }

    public void obtainEvenTodayItem(int apiIndex) {
        this.handler = new Handler(this.context.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                parseEventTodayItem(msg);
            }
        };
        JSONObject  jsonObject = new JSONObject();
        try{
            jsonObject.put("api_key", PreferenceUtility.getInstance().loadDataString(context,PreferenceUtility.API_KEY));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        doNetworkService(ApiParam.urlBuilder(this.context,apiIndex),jsonObject.toString(),"ApiKey");
    }

    private void parseEventTodayItem(Message message) {
        this.message = message;
        this.bundle = this.message.getData();
        this.stringResponse[0] = this.bundle.getString("network_response");
        this.failureResponse = this.bundle.getString("network_failure");
        Log.d(TAG, "responseString[0] Activity EVENT TODAY  " + this.stringResponse[0]);
        if (this.failureResponse.equalsIgnoreCase("yes")){
            this.callback.finishAEventToday(this.result, this.aEventModels);
        }else{
            try {
                this.result = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                if (this.result.equalsIgnoreCase("OK")){
                    this.aEventModels = Sales1CRMUtilsJSON.JSONUtility.getActivityEventToday(this.stringResponse[0]);
                }
                this.callback.finishAEventToday(this.result, this.aEventModels);
            } catch (JSONException e) {
                Log.d(TAG, "Exception detail Response " + e.getMessage());
            }
        }
    }

    public void setupMoreEventToday(int apiIndex){
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)) {
            obtainMoreEventToday(apiIndex);
        }else{
            callback.finishAEventMoreToday(this.result, this.aEventModels);
        }
    }

    public void obtainMoreEventToday(int apiIndex) {
        this.handler = new Handler(this.context.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                parseMoreEventToday(msg);
            }

        };
        JSONObject jsonObject = new JSONObject();
        try{
            jsonObject.put("api_key",PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.API_KEY));
        } catch (JSONException e){
            e.printStackTrace();
        }
        doNetworkService(ApiParam.urlBuilder(this.context,apiIndex),jsonObject.toString(),"ApiKey");
    }

    public void parseMoreEventToday (Message message) {
        this.message = message;
        this.bundle = this.message.getData();
        this.stringResponse[0] = this.bundle.getString("network_response");
        this.failureResponse = this.bundle.getString("network_failure");
        Log.d(TAG, "responseString[0] Activity EVENT TODAY " + this.stringResponse[0]);
        if (this.failureResponse.equalsIgnoreCase("yes")){
            this.callback.finishAEventMoreToday(this.result, this.aEventModels);
        }else{
            try {
                this.result = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                if (this.result.equalsIgnoreCase("OK")){
                    this.aEventModels = Sales1CRMUtilsJSON.JSONUtility.getActivityEventToday(this.stringResponse[0]);
                }
                this.callback.finishAEventMoreToday(this.result, this.aEventModels);
            } catch (JSONException e) {
                Log.d(TAG, "Exception detail Response " + e.getMessage());
            }
        }
     }

     public void setupTodoToday(int apiIndex) {
         if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)) {
             obtainTodoToday(apiIndex);
         }else{
             callback.finishATodoToday(this.result, this.aTodoModels);
         }
     }

     public void obtainTodoToday(int apiIndex){
         this.handler = new Handler(this.context.getMainLooper()) {
             @Override
             public void handleMessage(Message msg) {
                 parseTodoToday(msg);
             }
         };
         JSONObject  jsonObject = new JSONObject();
         try{
             jsonObject.put("api_key", PreferenceUtility.getInstance().loadDataString(context,PreferenceUtility.API_KEY));
         } catch (JSONException e) {
             e.printStackTrace();
         }
         doNetworkService(ApiParam.urlBuilder(this.context,apiIndex),jsonObject.toString(),"ApiKey");
     }

     private void parseTodoToday(Message message){
         this.message = message;
         this.bundle = this.message.getData();
         this.stringResponse[0] = this.bundle.getString("network_response");
         this.failureResponse = this.bundle.getString("network_failure");
         Log.d(TAG, "responseString[0] Activity TODO TODAY  info " + this.stringResponse[0]);
         if (this.failureResponse.equalsIgnoreCase("yes")){
             this.callback.finishATodoToday(this.result, this.aTodoModels);
         }else{
             try {
                 this.result = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                 if (this.result.equalsIgnoreCase("OK")){
                     this.aTodoModels = Sales1CRMUtilsJSON.JSONUtility.getActivityTodoToday(this.stringResponse[0]);
                 }
                 this.callback.finishATodoToday(this.result, this.aTodoModels);
             } catch (JSONException e) {
                 Log.d(TAG, "Exception detail Response " + e.getMessage());
             }
         }
     }

     public void setupMoreTodoToday(int apiIndex) {
         if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)) {
             obtainMoreTodoToday(apiIndex);
         }else{
             callback.finishATodoMoreToday(this.result, this.aTodoModels);
         }
     }
     public void obtainMoreTodoToday(int apiIndex){
         this.handler = new Handler(this.context.getMainLooper()) {
             @Override
             public void handleMessage(Message msg) {
                 parseMoreTodoToday(msg);
             }

         };
         JSONObject jsonObject = new JSONObject();
         try{
             jsonObject.put("api_key",PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.API_KEY));
         } catch (JSONException e){
             e.printStackTrace();
         }
         doNetworkService(ApiParam.urlBuilder(this.context,apiIndex),jsonObject.toString(),"ApiKey");
     }
     private void parseMoreTodoToday(Message message){
         this.message = message;
         this.bundle = this.message.getData();
         this.stringResponse[0] = this.bundle.getString("network_response");
         this.failureResponse = this.bundle.getString("network_failure");
         Log.d(TAG, "responseString[0] Activity TODO TODAY MORE info " + this.stringResponse[0]);
         if (this.failureResponse.equalsIgnoreCase("yes")){
             this.callback.finishATodoMoreToday(this.result, this.aTodoModels);
         }else{
             try {
                 this.result = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                 if (this.result.equalsIgnoreCase("OK")){
                     this.aTodoModels = Sales1CRMUtilsJSON.JSONUtility.getActivityTodoList(this.stringResponse[0]);
                 }
                 this.callback.finishATodoMoreToday(this.result, this.aTodoModels);
             } catch (JSONException e) {
                 Log.d(TAG, "Exception detail Response " + e.getMessage());
             }
         }
     }

     public void setupCallsToday(int apiIndex) {
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)) {
            obtainCallsToday(apiIndex);
        } else {
            callback.finishACallsToday(this.result,this.aCallsModels);
        }

     }

     public void obtainCallsToday(int apiIndex) {
        this.handler = new Handler(this.context.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                parseCallsToday(msg);
            }
        };
        JSONObject jsonObject = new JSONObject();
         try{
             jsonObject.put("api_key", PreferenceUtility.getInstance().loadDataString(context,PreferenceUtility.API_KEY));
         } catch (JSONException e) {
             e.printStackTrace();
         }
         doNetworkService(ApiParam.urlBuilder(this.context,apiIndex),jsonObject.toString(),"ApiKey");
     }

     private void parseCallsToday(Message message){
         this.message = message;
         this.bundle = this.message.getData();
         this.stringResponse[0] = this.bundle.getString("network_response");
         this.failureResponse = this.bundle.getString("network_failure");
         Log.d(TAG, "responseString[0] Activity CALLS TODAY  info " + this.stringResponse[0]);
         if (this.failureResponse.equalsIgnoreCase("yes")){
             this.callback.finishACallsToday(this.result, this.aCallsModels);
         }else{
             try {
                 this.result = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                 if (this.result.equalsIgnoreCase("OK")){
                     this.aCallsModels = Sales1CRMUtilsJSON.JSONUtility.getActivityCallsToday(this.stringResponse[0]);
                 }
                 this.callback.finishACallsToday(this.result, this.aCallsModels);
             } catch (JSONException e) {
                 Log.d(TAG, "Exception detail Response " + e.getMessage());
             }
         }

     }

     public void setupMoreCallsToday(int apiIndex) {
         if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)) {
             obtainMoreCallsToday(apiIndex);
         }else{
             callback.finishACallsMoreToday(this.result, this.aCallsModels);
         }
     }
     public void obtainMoreCallsToday(int apiIndex) {
         this.handler = new Handler(this.context.getMainLooper()) {
             @Override
             public void handleMessage(Message msg) {
                 parseMoreCallsToday(msg);
             }

         };
         JSONObject jsonObject = new JSONObject();
         try{
             jsonObject.put("api_key",PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.API_KEY));
         } catch (JSONException e){
             e.printStackTrace();
         }
         doNetworkService(ApiParam.urlBuilder(this.context,apiIndex),jsonObject.toString(),"ApiKey");
     }

     private void parseMoreCallsToday(Message message){
         this.message = message;
         this.bundle = this.message.getData();
         this.stringResponse[0] = this.bundle.getString("network_response");
         this.failureResponse = this.bundle.getString("network_failure");
         Log.d(TAG, "responseString[0] Activity CALLS TODAY MORE info " + this.stringResponse[0]);
         if (this.failureResponse.equalsIgnoreCase("yes")){
             this.callback.finishACallsMoreToday(this.result, this.aCallsModels);
         }else{
             try {
                 this.result = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                 if (this.result.equalsIgnoreCase("OK")){
                     this.aCallsModels = Sales1CRMUtilsJSON.JSONUtility.getActivityCallsToday(this.stringResponse[0]);
                 }
                 this.callback.finishATodoMoreToday(this.result, this.aTodoModels);
             } catch (JSONException e) {
                 Log.d(TAG, "Exception detail Response " + e.getMessage());
             }
         }
     }

    }

