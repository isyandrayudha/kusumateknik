package com.msf.project.sales1crm.fragment;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.callback.TotalStatsCallback;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.customview.SpinnerCustomAdapter;
import com.msf.project.sales1crm.model.TotalModel;
import com.msf.project.sales1crm.presenter.ViewReportAccountTaskPresenter;
import com.msf.project.sales1crm.service.GPSTracker;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ViewReportAccountTaskFragment extends Fragment implements TotalStatsCallback {

    @BindView(R.id.lineChart)
    LineChart lineChart;

    @BindView(R.id.spMonth1)
    Spinner spMonth1;

    @BindView(R.id.spYear1)
    Spinner spYear1;

    @BindView(R.id.spMonth2)
    Spinner spMonth2;

    @BindView(R.id.spYear2)
    Spinner spYear2;

    @BindView(R.id.tvSubmit)
    CustomTextView tvSubmit;

    private View view;
    private Context context;
    private ViewReportAccountTaskPresenter presenter;
    private SpinnerCustomAdapter spinnerCustomAdapter;
    private List<TotalModel> list = new ArrayList<>();
    private List<TotalModel> list1 = new ArrayList<>();
    private String[] months = {"Choose Month", "January", "February", "March", "April", "May", "Juni", "July", "August", "September", "October", "November", "Desember"};
    private String[] years = {"Choose Year", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019"};
    private String month_1="", year_1="", month_2="", year_2="";

    public static ViewReportAccountTaskFragment newInstance() {
        ViewReportAccountTaskFragment fragment = new ViewReportAccountTaskFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_accounttask, container, false);

        return this.view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);
        this.context = getActivity();

        this.presenter = new ViewReportAccountTaskPresenter(context, this);

        initData();
    }

    private void initData(){
        spinnerCustomAdapter = new SpinnerCustomAdapter(context,
                R.layout.spinner_default, years);
        spinnerCustomAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spYear1.setAdapter(spinnerCustomAdapter);
        spYear1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                if (position != 0){
                    year_1 = years[position];
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerCustomAdapter = new SpinnerCustomAdapter(context,
                R.layout.spinner_default, months);
        spinnerCustomAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spMonth1.setAdapter(spinnerCustomAdapter);
        spMonth1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                if (position != 0){
                    month_1 = String.valueOf(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerCustomAdapter = new SpinnerCustomAdapter(context,
                R.layout.spinner_default, years);
        spinnerCustomAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spYear2.setAdapter(spinnerCustomAdapter);
        spYear2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                if (position != 0){
                    year_2 = years[position];
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerCustomAdapter = new SpinnerCustomAdapter(context,
                R.layout.spinner_default, months);
        spinnerCustomAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spMonth2.setAdapter(spinnerCustomAdapter);
        spMonth2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                if (position != 0){
                    month_2 = String.valueOf(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        tvSubmit.setOnClickListener(click);
    }

    public void getDataFromAPI(){
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(context)){
            GPSTracker gpsTracker = new GPSTracker(context);
            if (gpsTracker.canGetLocation()) {
                presenter.setupTotal(ApiParam.API_098, month_1, year_1, month_2, year_2);
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gpsTracker.showSettingsAlert();
            }
        }
    }

    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.tvSubmit:
                    getDataFromAPI();
                    break;
            }
        }
    };

    @Override
    public void finished(String result, List<TotalModel> list, List<TotalModel> list1) {
        if (result.equalsIgnoreCase("OK")){

            ArrayList<Entry> opporValue = new ArrayList<Entry>();
            ArrayList<Entry> opporValue1 = new ArrayList<Entry>();
            ArrayList<String> leadString = new ArrayList<String>();
            for (int x = 0; x < list.size(); x++){
                opporValue.add(new Entry(x+1, Integer.parseInt(list.get(x).getTotal())));
                opporValue1.add(new Entry(x+1, Integer.parseInt(list1.get(x).getTotal())));
                leadString.add(String.valueOf(Integer.valueOf(x+1)));
            }

            setDataLead(opporValue, opporValue1);
        }
    }

    private void setDataLead(ArrayList<Entry> yVals, ArrayList<Entry> yVals1){
        // create a dataset and give it a type
        LineDataSet set1 = new LineDataSet(yVals, "Periode 1");
        set1.setFillAlpha(110);
        set1.setFillColor(Color.BLUE);

        // set the line to be drawn like this "- - - - - -"
        // set1.enableDashedLine(10f, 5f, 0f);
        // set1.enableDashedHighlightLine(10f, 5f, 0f);
        set1.setColor(Color.BLUE);
        set1.setCircleColor(Color.BLUE);
        set1.setLineWidth(1f);
        set1.setCircleRadius(2f);
        set1.setDrawCircleHole(false);
        set1.setValueTextSize(8f);
        set1.setDrawFilled(true);

        LineDataSet set2 = new LineDataSet(yVals1, "Periode 2");
        set2.setFillAlpha(110);
        set2.setFillColor(Color.GREEN);

        // set the line to be drawn like this "- - - - - -"
        // set2.enableDashedLine(10f, 5f, 0f);
        // set2.enableDashedHighlightLine(10f, 5f, 0f);
        set2.setColor(Color.GREEN);
        set2.setCircleColor(Color.GREEN);
        set2.setLineWidth(1f);
        set2.setCircleRadius(2f);
        set2.setDrawCircleHole(false);
        set2.setValueTextSize(8f);
        set2.setDrawFilled(true);

        XAxis xAxis = lineChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawAxisLine(true);

        YAxis leftAxis = lineChart.getAxisLeft();
        leftAxis.setDrawGridLines(false);

        YAxis rightAxis = lineChart.getAxisRight();
        rightAxis.setTextColor(Color.WHITE);

        ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        dataSets.add(set1); // add the datasets
        dataSets.add(set2); // add the datasets

        // create a data object with the datasets
        LineData data = new LineData(dataSets);
        data.setDrawValues(false);

        // set data
        lineChart.setData(data);
        lineChart.animateY(1000);
        lineChart.setDragEnabled(true);
        lineChart.setScaleEnabled(true);
        lineChart.highlightValue(null);
        lineChart.invalidate();
        lineChart.getDescription().setEnabled(false);
    }
}
