package com.msf.project.sales1crm.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

/**
 * Created by christianmacbook on 22/05/18.
 */

public class EventModel implements Comparable, Parcelable {
    private String id;
    private String name;
    private String account_id;
    private String account_name;
    private String assignto;
    private String assignto_name;
    private String date;
    private String date_interval;
    private String description;
    private String created_at;
    private String photo_1;
    private String photo_2;
    private String photo_3;
    private String nama1;
    private String nama2;
    private String nama3;
    private String event_type_id;
    private String event_purpose_id;
    private String event_type_name;
    private String event_purpose_name;
    private String follow_up;
    private String meet_to;
    private String brand;
    private String type;
    private String serial_number;
    private String condition;
    private String create_at;
    private String product;
    private String id_product_event;
    private String product_status;

    public EventModel() {
        setId("");
        setName("");
        setAccount_id("");
        setAccount_name("");
        setAssignto("");
        setAssignto_name("");
        setDate("");
        setDate_interval("");
        setDescription("");
        setCreated_at("");
        setPhoto_1("");
        setPhoto_2("");
        setPhoto_3("");
        setNama1("");
        setNama2("");
        setNama3("");
        setEvent_type_id("");
        setEvent_purpose_id("");
        setFollow_up("");
        setMeet_to("");
        setBrand("");
        setType("");
        setSerial_number("");
        setCondition("");
        setCreate_at("");
        setProduct("");
        setId_product_event("");
        setProduct_status("");
    }

    public EventModel(Parcel in) {
        readFromParcel(in);
    }

    public void readFromParcel(Parcel in) {
        id = in.readString();
        name = in.readString();
        account_id = in.readString();
        account_name = in.readString();
        assignto = in.readString();
        assignto_name = in.readString();
        date = in.readString();
        date_interval = in.readString();
        description = in.readString();
        created_at = in.readString();
        photo_1 = in.readString();
        photo_2 = in.readString();
        photo_3 = in.readString();
        nama1 = in.readString();
        nama2 = in.readString();
        nama3 = in.readString();
        event_type_id = in.readString();
        event_purpose_id = in.readString();
        event_type_name = in.readString();
        event_purpose_name = in.readString();
        follow_up = in.readString();
        meet_to = in.readString();
        brand = in.readString();
        type = in.readString();
        serial_number = in.readString();
        condition = in.readString();
        create_at = in.readString();
        product = in.readString();
        id_product_event = in.readString();
        product_status = in.readString();
    }

    public static final Creator<EventModel> CREATOR = new Creator<EventModel>() {
        @Override
        public EventModel createFromParcel(Parcel in) {
            return new EventModel(in);
        }

        @Override
        public EventModel[] newArray(int size) {
            return new EventModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAccount_id() {
        return account_id;
    }

    public void setAccount_id(String account_id) {
        this.account_id = account_id;
    }

    public String getAccount_name() {
        return account_name;
    }

    public void setAccount_name(String account_name) {
        this.account_name = account_name;
    }

    public String getAssignto() {
        return assignto;
    }

    public void setAssignto(String assignto) {
        this.assignto = assignto;
    }

    public String getAssignto_name() {
        return assignto_name;
    }

    public void setAssignto_name(String assignto_name) {
        this.assignto_name = assignto_name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate_interval() {
        return date_interval;
    }

    public void setDate_interval(String date_interval) {
        this.date_interval = date_interval;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getPhoto_1() {
        return photo_1;
    }

    public void setPhoto_1(String photo_1) {
        this.photo_1 = photo_1;
    }

    public String getPhoto_2() {
        return photo_2;
    }

    public void setPhoto_2(String photo_2) {
        this.photo_2 = photo_2;
    }

    public String getPhoto_3() {
        return photo_3;
    }

    public void setPhoto_3(String photo_3) {
        this.photo_3 = photo_3;
    }

    public String getNama1() {
        return nama1;
    }

    public void setNama1(String nama1) {
        this.nama1 = nama1;
    }

    public String getNama2() {
        return nama2;
    }

    public void setNama2(String nama2) {
        this.nama2 = nama2;
    }

    public String getNama3() {
        return nama3;
    }

    public void setNama3(String nama3) {
        this.nama3 = nama3;
    }

    public String getEvent_type_id() {
        return event_type_id;
    }

    public void setEvent_type_id(String event_type_id) {
        this.event_type_id = event_type_id;
    }

    public String getEvent_purpose_id() {
        return event_purpose_id;
    }

    public void setEvent_purpose_id(String event_purpose_id) {
        this.event_purpose_id = event_purpose_id;
    }

    public String getEvent_type_name() {
        return event_type_name;
    }

    public void setEvent_type_name(String event_type_name) {
        this.event_type_name = event_type_name;
    }

    public String getEvent_purpose_name() {
        return event_purpose_name;
    }

    public void setEvent_purpose_name(String event_purpose_name) {
        this.event_purpose_name = event_purpose_name;
    }

    public String getFollow_up() {
        return follow_up;
    }

    public void setFollow_up(String follow_up) {
        this.follow_up = follow_up;
    }

    public String getMeet_to() {
        return meet_to;
    }

    public void setMeet_to(String meet_to) {
        this.meet_to = meet_to;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSerial_number() {
        return serial_number;
    }

    public void setSerial_number(String serial_number) {
        this.serial_number = serial_number;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getCreate_at() {
        return create_at;
    }

    public void setCreate_at(String create_at) {
        this.create_at = create_at;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getId_product_event() {
        return id_product_event;
    }

    public void setId_product_event(String id_product_event) {
        this.id_product_event = id_product_event;
    }

    public String getProduct_status() {
        return product_status;
    }

    public void setProduct_status(String product_status) {
        this.product_status = product_status;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(account_id);
        dest.writeString(account_name);
        dest.writeString(assignto);
        dest.writeString(assignto_name);
        dest.writeString(date);
        dest.writeString(date_interval);
        dest.writeString(description);
        dest.writeString(created_at);
        dest.writeString(photo_1);
        dest.writeString(photo_2);
        dest.writeString(photo_3);
        dest.writeString(nama1);
        dest.writeString(nama2);
        dest.writeString(nama3);
        dest.writeString(event_type_id);
        dest.writeString(event_purpose_id);
        dest.writeString(event_type_name);
        dest.writeString(event_purpose_name);
        dest.writeString(follow_up);
        dest.writeString(meet_to);
        dest.writeString(brand);
        dest.writeString(type);
        dest.writeString(serial_number);
        dest.writeString(condition);
        dest.writeString(create_at);
        dest.writeString(product);
        dest.writeString(id_product_event);
        dest.writeString(product_status);
    }

    @Override
    public int compareTo(@NonNull Object o) {
        return 0;
    }
}
