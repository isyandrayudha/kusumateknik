package com.msf.project.sales1crm.presenter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;

import com.msf.project.sales1crm.callback.TaskByStatusCallback;
import com.msf.project.sales1crm.model.TaskModel;
import com.msf.project.sales1crm.service.NetworkConnection;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.PreferenceUtility;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;
import com.msf.project.sales1crm.utility.Sales1CRMUtilsJSON;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class TaskbyStatusPresenter {
    private final String TAG = TaskbyStatusPresenter.class.getSimpleName();
    private Context context;
    private Messenger messenger;
    private Message message;
    private Bundle bundle;
    private Handler handler;
    private String failureResponse = "";
    private String[] stringResponse = {""};
    private TaskByStatusCallback callback;
    private String result = "NG";
    private List<TaskModel> list = new ArrayList<>();

    public TaskbyStatusPresenter(Context context, TaskByStatusCallback listener) {
        this.context = context;
        this.callback = listener;
    }

    private void doNetworkService(String url, String params, String from)  {
        Log.d(TAG, "url login " + url);
        Log.d(TAG, "params : " + params);
        Intent networkIntent = new Intent(this.context, NetworkConnection.class);
        this.messenger = new Messenger(this.handler);
        networkIntent.putExtra("messenger", this.messenger);
        networkIntent.putExtra("url", url);
        networkIntent.putExtra("params", params);
        networkIntent.putExtra("from", from);
        this.context.startService(networkIntent);
    }

    public void setupNotStarted(int apiIndex, int offset, int sort, String account_id, String date, String task_status) {
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)) {
            obtainListNotStarted(apiIndex, offset, sort, account_id, date, task_status);
        }else{
            callback.notStarted(this.result, this.list);
        }
    }

    public void obtainListNotStarted(int apiIndex, int offset, int sort, String account_id, String date, String task_status) {
        this.handler = new Handler(this.context.getMainLooper()) {
            @Override
            public void handleMessage(Message msg)
            {
                parseListNotStarted(msg);
            }
        };

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("api_key", PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.API_KEY));
            jsonObject.put("offset", offset+"");
            jsonObject.put("sort", sort+"");
//            jsonObject.put("search", search);
            jsonObject.put("due_date", date);
            jsonObject.put("account_id", account_id);
            jsonObject.put("task_status",task_status+"1");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        doNetworkService(ApiParam.urlBuilder(this.context, apiIndex), jsonObject.toString(), "TaskByStatus");
    }

    private void parseListNotStarted(Message message) {
        this.message = message;
        this.bundle = this.message.getData();
        this.stringResponse[0] = this.bundle.getString("network_response");
        this.failureResponse = this.bundle.getString("network_failure");
        Log.d(TAG, "responseString[0] login info " + this.stringResponse[0]);
        if (this.failureResponse.equalsIgnoreCase("yes")){
            this.callback.notStarted(this.result, this.list);
        }else{
            try {
                this.result = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                if (this.result.equalsIgnoreCase("OK")){
                    this.list = Sales1CRMUtilsJSON.JSONUtility.getTaskListbyStatus(this.stringResponse[0]);
                }
                this.callback.notStarted(this.result, this.list);
            } catch (JSONException e) {
                Log.d(TAG, "Exception detail Response " + e.getMessage());
            }
        }
    }

    public void setupMoreNotStarted(int apiIndex, int offset, int sort, String account_id, String date, String task_status) {
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)) {
            obtainMoreNotStarted(apiIndex, offset, sort, account_id, date, task_status);
        }else{
            callback.notstartedMore(this.result, this.list);
        }
    }

    public void obtainMoreNotStarted(int apiIndex, int offset, int sort, String account_id, String date, String task_status) {
        this.handler = new Handler(this.context.getMainLooper()) {

            @Override
            public void handleMessage(Message msg) {
                parseMoreListNotStarted(msg);
            }

        };

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("api_key", PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.API_KEY));
            jsonObject.put("offset", offset+"");
            jsonObject.put("sort", sort+"");
//            jsonObject.put("search", search);
            jsonObject.put("due_date", date);
            jsonObject.put("account_id", account_id);
            jsonObject.put("task_status", task_status+"1");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        doNetworkService(ApiParam.urlBuilder(this.context, apiIndex), jsonObject.toString(), "TaskByStatus");
    }

    private void parseMoreListNotStarted(Message message) {
        this.message = message;
        this.bundle = this.message.getData();
        this.stringResponse[0] = this.bundle.getString("network_response");
        this.failureResponse = this.bundle.getString("network_failure");
        Log.d(TAG, "responseString[0] login info " + this.stringResponse[0]);
        if (this.failureResponse.equalsIgnoreCase("yes")){
            this.callback.notstartedMore(this.result, this.list);
        }else{
            try {
                this.result = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                if (this.result.equalsIgnoreCase("OK")){
                    this.list = Sales1CRMUtilsJSON.JSONUtility.getTaskListbyStatus(this.stringResponse[0]);
                }
                this.callback.notstartedMore(this.result, this.list);
            } catch (JSONException e) {
                Log.d(TAG, "Exception detail Response " + e.getMessage());
            }
        }
    }

    public void setupPending(int apiIndex, int offset, int sort, String account_id, String date, String task_status){
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)) {
            obtainListPending(apiIndex, offset, sort, account_id, date, task_status);
        }else{
            callback.pending(this.result, this.list);
        }
    }
    public void obtainListPending(int apiIndex, int offset, int sort, String account_id, String date, String task_status){
        this.handler = new Handler(this.context.getMainLooper()) {
            @Override
            public void handleMessage(Message msg)
            {
                parseListPending(msg);
            }
        };

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("api_key", PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.API_KEY));
            jsonObject.put("offset", offset+"");
            jsonObject.put("sort", sort+"");
//            jsonObject.put("search", search);
            jsonObject.put("due_date", date);
            jsonObject.put("account_id", account_id);
            jsonObject.put("task_status",task_status+"2");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        doNetworkService(ApiParam.urlBuilder(this.context, apiIndex), jsonObject.toString(), "TaskByStatus");
    }
    private void parseListPending(Message message){
        this.message = message;
        this.bundle = this.message.getData();
        this.stringResponse[0] = this.bundle.getString("network_response");
        this.failureResponse = this.bundle.getString("network_failure");
        Log.d(TAG, "responseString[0] login info " + this.stringResponse[0]);
        if (this.failureResponse.equalsIgnoreCase("yes")){
            this.callback.pending(this.result, this.list);
        }else{
            try {
                this.result = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                if (this.result.equalsIgnoreCase("OK")){
                    this.list = Sales1CRMUtilsJSON.JSONUtility.getTaskListbyStatus(this.stringResponse[0]);
                }
                this.callback.pending(this.result, this.list);
            } catch (JSONException e) {
                Log.d(TAG, "Exception detail Response " + e.getMessage());
            }
        }
    }
    public void setupPendingMore(int apiIndex, int offset, int sort,  String account_id, String date, String task_status){
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)) {
            obtainPendingMore(apiIndex, offset, sort, account_id, date, task_status);
        }else{
            callback.pendingMore(this.result, this.list);
        }
    }

    public void obtainPendingMore(int apiIndex, int offset, int sort, String account_id, String date, String task_status){
        this.handler = new Handler(this.context.getMainLooper()) {

            @Override
            public void handleMessage(Message msg) {
                parsePendingMore(msg);
            }

        };

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("api_key", PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.API_KEY));
            jsonObject.put("offset", offset+"");
            jsonObject.put("sort", sort+"");
//            jsonObject.put("search", search);
            jsonObject.put("due_date", date);
            jsonObject.put("account_id", account_id);
            jsonObject.put("task_status", task_status+"2");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        doNetworkService(ApiParam.urlBuilder(this.context, apiIndex), jsonObject.toString(), "TaskByStatus");
    }
    public void parsePendingMore(Message message){
        this.message = message;
        this.bundle = this.message.getData();
        this.stringResponse[0] = this.bundle.getString("network_response");
        this.failureResponse = this.bundle.getString("network_failure");
        Log.d(TAG, "responseString[0] login info " + this.stringResponse[0]);
        if (this.failureResponse.equalsIgnoreCase("yes")){
            this.callback.pendingMore(this.result, this.list);
        }else{
            try {
                this.result = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                if (this.result.equalsIgnoreCase("OK")){
                    this.list = Sales1CRMUtilsJSON.JSONUtility.getTaskListbyStatus(this.stringResponse[0]);
                }
                this.callback.pendingMore(this.result, this.list);
            } catch (JSONException e) {
                Log.d(TAG, "Exception detail Response " + e.getMessage());
            }
        }
    }

    public void setupInProgress(int apiIndex, int offset, int sort, String account_id, String date, String task_status){
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)) {
            obtainListInProgress(apiIndex, offset, sort, account_id, date, task_status);
        }else{
            callback.inProgress(this.result, this.list);
        }
    }
    public void obtainListInProgress(int apiIndex, int offset, int sort, String account_id, String date, String task_status){
        this.handler = new Handler(this.context.getMainLooper()) {
            @Override
            public void handleMessage(Message msg)
            {
                parseListInProgress(msg);
            }
        };

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("api_key", PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.API_KEY));
            jsonObject.put("offset", offset+"");
            jsonObject.put("sort", sort+"");
//            jsonObject.put("search", search);
            jsonObject.put("due_date", date);
            jsonObject.put("account_id", account_id);
            jsonObject.put("task_status",task_status+"3");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        doNetworkService(ApiParam.urlBuilder(this.context, apiIndex), jsonObject.toString(), "TaskByStatus");
    }
    private void parseListInProgress(Message message){
        this.message = message;
        this.bundle = this.message.getData();
        this.stringResponse[0] = this.bundle.getString("network_response");
        this.failureResponse = this.bundle.getString("network_failure");
        Log.d(TAG, "responseString[0] login info " + this.stringResponse[0]);
        if (this.failureResponse.equalsIgnoreCase("yes")){
            this.callback.inProgress(this.result, this.list);
        }else{
            try {
                this.result = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                if (this.result.equalsIgnoreCase("OK")){
                    this.list = Sales1CRMUtilsJSON.JSONUtility.getTaskListbyStatus(this.stringResponse[0]);
                }
                this.callback.inProgress(this.result, this.list);
            } catch (JSONException e) {
                Log.d(TAG, "Exception detail Response " + e.getMessage());
            }
        }
    }
    public void setupInProgressMore(int apiIndex, int offset, int sort, String account_id, String date, String task_status){
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)) {
            obtainInProgressMore(apiIndex, offset, sort, account_id, date, task_status);
        }else{
            callback.inProgressMore(this.result, this.list);
        }
    }

    public void obtainInProgressMore(int apiIndex, int offset, int sort, String account_id, String date, String task_status){
        this.handler = new Handler(this.context.getMainLooper()) {

            @Override
            public void handleMessage(Message msg) {
                parseInProgressMore(msg);
            }

        };

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("api_key", PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.API_KEY));
            jsonObject.put("offset", offset+"");
            jsonObject.put("sort", sort+"");
//            jsonObject.put("search", search);
            jsonObject.put("due_date", date);
            jsonObject.put("account_id", account_id);
            jsonObject.put("task_status", task_status+"3");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        doNetworkService(ApiParam.urlBuilder(this.context, apiIndex), jsonObject.toString(), "TaskByStatus");
    }
    public void parseInProgressMore(Message message){
        this.message = message;
        this.bundle = this.message.getData();
        this.stringResponse[0] = this.bundle.getString("network_response");
        this.failureResponse = this.bundle.getString("network_failure");
        Log.d(TAG, "responseString[0] login info " + this.stringResponse[0]);
        if (this.failureResponse.equalsIgnoreCase("yes")){
            this.callback.inProgressMore(this.result, this.list);
        }else{
            try {
                this.result = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                if (this.result.equalsIgnoreCase("OK")){
                    this.list = Sales1CRMUtilsJSON.JSONUtility.getTaskListbyStatus(this.stringResponse[0]);
                }
                this.callback.inProgressMore(this.result, this.list);
            } catch (JSONException e) {
                Log.d(TAG, "Exception detail Response " + e.getMessage());
            }
        }
    }

    public void setupCompleted(int apiIndex, int offset, int sort, String account_id, String date, String task_status){
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)) {
            obtainListCompleted(apiIndex, offset, sort, account_id, date, task_status);
        }else{
            callback.completed(this.result, this.list);
        }
    }
    public void obtainListCompleted(int apiIndex, int offset, int sort, String account_id, String date, String task_status){
        this.handler = new Handler(this.context.getMainLooper()) {
            @Override
            public void handleMessage(Message msg)
            {
                parseListCompleted(msg);
            }
        };

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("api_key", PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.API_KEY));
            jsonObject.put("offset", offset+"");
            jsonObject.put("sort", sort+"");
//            jsonObject.put("search", search);
            jsonObject.put("due_date", date);
            jsonObject.put("account_id", account_id);
            jsonObject.put("task_status",task_status+"4");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        doNetworkService(ApiParam.urlBuilder(this.context, apiIndex), jsonObject.toString(), "TaskByStatus");
    }

    private void parseListCompleted(Message message){
        this.message = message;
        this.bundle = this.message.getData();
        this.stringResponse[0] = this.bundle.getString("network_response");
        this.failureResponse = this.bundle.getString("network_failure");
        Log.d(TAG, "responseString[0] login info " + this.stringResponse[0]);
        if (this.failureResponse.equalsIgnoreCase("yes")){
            this.callback.completed(this.result, this.list);
        }else{
            try {
                this.result = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                if (this.result.equalsIgnoreCase("OK")){
                    this.list = Sales1CRMUtilsJSON.JSONUtility.getTaskListbyStatus(this.stringResponse[0]);
                }
                this.callback.completed(this.result, this.list);
            } catch (JSONException e) {
                Log.d(TAG, "Exception detail Response " + e.getMessage());
            }
        }
    }
    public void setupCompletedMore(int apiIndex, int offset, int sort, String account_id, String date, String task_status){
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)) {
            obtainCompletedMore(apiIndex, offset, sort,  account_id, date, task_status);
        }else{
            callback.completedMore(this.result, this.list);
        }
    }

    public void obtainCompletedMore(int apiIndex, int offset, int sort, String account_id, String date, String task_status){
        this.handler = new Handler(this.context.getMainLooper()) {

            @Override
            public void handleMessage(Message msg) {
                parseCompletedMore(msg);
            }

        };

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("api_key", PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.API_KEY));
            jsonObject.put("offset", offset+"");
            jsonObject.put("sort", sort+"");
//            jsonObject.put("search", search);
            jsonObject.put("due_date", date);
            jsonObject.put("account_id", account_id);
            jsonObject.put("task_status", task_status+"4");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        doNetworkService(ApiParam.urlBuilder(this.context, apiIndex), jsonObject.toString(), "TaskByStatus");
    }
    public void parseCompletedMore(Message message){
        this.message = message;
        this.bundle = this.message.getData();
        this.stringResponse[0] = this.bundle.getString("network_response");
        this.failureResponse = this.bundle.getString("network_failure");
        Log.d(TAG, "responseString[0] login info " + this.stringResponse[0]);
        if (this.failureResponse.equalsIgnoreCase("yes")){
            this.callback.completedMore(this.result, this.list);
        }else{
            try {
                this.result = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                if (this.result.equalsIgnoreCase("OK")){
                    this.list = Sales1CRMUtilsJSON.JSONUtility.getTaskListbyStatus(this.stringResponse[0]);
                }
                this.callback.completedMore(this.result, this.list);
            } catch (JSONException e) {
                Log.d(TAG, "Exception detail Response " + e.getMessage());
            }
        }
    }

    public void setupWaiting(int apiIndex, int offset, int sort, String account_id, String date, String task_status){
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)) {
            obtainListWaiting(apiIndex, offset, sort,  account_id, date, task_status);
        }else{
            callback.waiting(this.result, this.list);
        }
    }
    public void obtainListWaiting(int apiIndex, int offset, int sort,  String account_id, String date, String task_status){
        this.handler = new Handler(this.context.getMainLooper()) {
            @Override
            public void handleMessage(Message msg)
            {
                parseListWaiting(msg);
            }
        };

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("api_key", PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.API_KEY));
            jsonObject.put("offset", offset+"");
            jsonObject.put("sort", sort+"");
//            jsonObject.put("search", search);
            jsonObject.put("due_date", date);
            jsonObject.put("account_id", account_id);
            jsonObject.put("task_status",task_status+"5");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        doNetworkService(ApiParam.urlBuilder(this.context, apiIndex), jsonObject.toString(), "TaskByStatus");
    }
    private void parseListWaiting(Message message){
        this.message = message;
        this.bundle = this.message.getData();
        this.stringResponse[0] = this.bundle.getString("network_response");
        this.failureResponse = this.bundle.getString("network_failure");
        Log.d(TAG, "responseString[0] login info " + this.stringResponse[0]);
        if (this.failureResponse.equalsIgnoreCase("yes")){
            this.callback.waiting(this.result, this.list);
        }else{
            try {
                this.result = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                if (this.result.equalsIgnoreCase("OK")){
                    this.list = Sales1CRMUtilsJSON.JSONUtility.getTaskListbyStatus(this.stringResponse[0]);
                }
                    this.callback.waiting(this.result, this.list);
            } catch (JSONException e) {
                Log.d(TAG, "Exception detail Response " + e.getMessage());
            }
        }
    }
    public void setupMoreWaiting(int apiIndex, int offset, int sort,  String account_id, String date, String task_status){
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)) {
            obtainWaitingMore(apiIndex, offset, sort, account_id, date, task_status);
        }else{
            callback.waitingMore(this.result, this.list);
        }
    }

    public void obtainWaitingMore(int apiIndex, int offset, int sort,  String account_id, String date, String task_status){
        this.handler = new Handler(this.context.getMainLooper()) {

            @Override
            public void handleMessage(Message msg) {
                parseWaitingMore(msg);
            }

        };

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("api_key", PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.API_KEY));
            jsonObject.put("offset", offset+"");
            jsonObject.put("sort", sort+"");
//            jsonObject.put("search", search);
            jsonObject.put("due_date", date);
            jsonObject.put("account_id", account_id);
            jsonObject.put("task_status", task_status+"5");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        doNetworkService(ApiParam.urlBuilder(this.context, apiIndex), jsonObject.toString(), "TaskByStatus");
    }
    public void parseWaitingMore(Message message){
        this.message = message;
        this.bundle = this.message.getData();
        this.stringResponse[0] = this.bundle.getString("network_response");
        this.failureResponse = this.bundle.getString("network_failure");
        Log.d(TAG, "responseString[0] login info " + this.stringResponse[0]);
        if (this.failureResponse.equalsIgnoreCase("yes")){
            this.callback.waitingMore(this.result, this.list);
        }else{
            try {
                this.result = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                if (this.result.equalsIgnoreCase("OK")){
                    this.list = Sales1CRMUtilsJSON.JSONUtility.getTaskListbyStatus(this.stringResponse[0]);
                }
                this.callback.waitingMore(this.result, this.list);
            } catch (JSONException e) {
                Log.d(TAG, "Exception detail Response " + e.getMessage());
            }
        }
    }


}