package com.msf.project.sales1crm.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.msf.project.sales1crm.AccountDetailActivity;
import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.model.AccountModel;
import com.msf.project.sales1crm.utility.PreferenceUtility;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by christianmacbook on 11/05/18.
 */

public class AccountListCardAdapter extends RecyclerView.Adapter<AccountListCardAdapter.AccountListCardHolder> {
    private Context context;
    private Bitmap icon;
    private String url;
    private List<AccountModel> listData;


    public AccountListCardAdapter(Context context, List<AccountModel> objects) {
        this.context = context;
        this.listData = objects;

        this.url = PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.URL);
    }

    public static abstract class Row {
    }

    // This is for making Section, ex: A B C D etc
    public static final class Section extends Row {
        public final String text;

        public Section(String text) {
            this.text = text;
        }
    }

    // This is for making Row, ex: A B C D etc
    public static final class Item extends Row {
        public final AccountModel text;

        public Item(AccountModel text) {
            this.text = text;
        }

        public AccountModel getItem() {
            return text;
        }
    }

    private List<Row> rows;

    public void setRows(List<Row> rows) {
        this.rows = rows;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return rows.size();
    }

    @Override
    public AccountListCardAdapter.AccountListCardHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context)
                .inflate(R.layout.card_account, null);
        return new AccountListCardHolder(v);
    }

    @Override
    public void onBindViewHolder(AccountListCardAdapter.AccountListCardHolder holder, int position) {
        final Item item = (Item) rows.get(position);
        holder.tvAccountName.setText(item.text.getAccount_name());
        holder.tvindustry.setText(item.text.getIndustry_name());
        holder.tvindustryy.setText(item.text.getIndustry_name());
        holder.tvType.setText(item.text.getType_name());
        holder.tvOwnerName.setText(item.text.getOwner_name());
        holder.tvtelephone.setText(item.text.getPhone());
        holder.tvCountOpportunity.setText(item.text.getCount_opportunity());
        holder.tvCountTask.setText(item.text.getCount_task());
        holder.tvCountEvent.setText(item.text.getCount_event());
        Sales1CRMUtils.Utils.imageLoader.DisplayImage(url + item.text.getImageUrl(),
                holder.ivImage, Bitmap.Config.ARGB_8888, 100, Sales1CRMUtils.TYPE_RECT);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    Intent intent = new Intent(context, AccountDetailActivity.class);
                    intent.putExtra("accountModel", item.getItem());
                    context.startActivity(intent);
            }
        });
    }

    public class AccountListCardHolder extends RecyclerView.ViewHolder {
        CustomTextView tvAccountName,tvindustry, tvindustryy,
                tvType, tvOwnerName, tvtelephone, tvCountOpportunity,tvCountTask, tvCountEvent;
        CircleImageView ivImage;
        public AccountListCardHolder(View itemView) {
            super(itemView);
            tvAccountName = (CustomTextView) itemView
                    .findViewById(R.id.tvAccountName);
            tvindustry = (CustomTextView) itemView
                    .findViewById(R.id.tvindustry);
            tvindustryy = (CustomTextView) itemView
                    .findViewById(R.id.tvindustryy);
            tvType = (CustomTextView) itemView
                    .findViewById(R.id.tvType);
            tvOwnerName = (CustomTextView) itemView
                    .findViewById(R.id.tvOwnerName);
            tvtelephone = (CustomTextView) itemView
                    .findViewById(R.id.tvtelephone);
            tvCountOpportunity = (CustomTextView) itemView
                    .findViewById(R.id.tvCountOpportunity);
            tvCountTask = (CustomTextView)  itemView
                    .findViewById(R.id.tvCountTask);
            tvCountEvent = (CustomTextView) itemView
                    .findViewById(R.id.tvCountEvent);
            ivImage = (CircleImageView) itemView.findViewById(R.id.ivImage);

        }
    }
}
