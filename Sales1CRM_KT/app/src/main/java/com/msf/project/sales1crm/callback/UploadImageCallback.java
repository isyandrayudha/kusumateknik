package com.msf.project.sales1crm.callback;

import com.msf.project.sales1crm.model.UserModel;

public interface UploadImageCallback {
    void finishUplaod(String result, String response);
    void displayImage(String result, UserModel userModel);
}
