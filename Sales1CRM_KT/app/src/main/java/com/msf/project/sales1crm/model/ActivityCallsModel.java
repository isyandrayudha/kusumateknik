package com.msf.project.sales1crm.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

public class ActivityCallsModel implements Comparable, Parcelable{
	private String date;
	private String durationLabel;
	private String subject;
	private String createdAt;
	private String callPurposeId;
	private String deletedAt;
	private String duration;
	private String accountId;
	private String callTypeId;
	private String updatedAt;
	private String userId;
	private String id;
	private String detail;
	private String status;
	private String account_name;
	private String user_name;
	private String call_purpose_name;
	private String call_type_name;
	private String duration_interval;
	private String date_interval;

	public ActivityCallsModel() {
	    setDate("");
	    setDurationLabel("");
	    setSubject("");
	    setCreatedAt("");
	    setCallPurposeId("");
	    setDeletedAt("");
	    setDuration("");
	    setAccountId("");
	    setCallTypeId("");
	    setUpdatedAt("");
	    setUserId("");
	    setId("");
	    setDetail("");
	    setStatus("");
	    setAccount_name("");
	    setUser_name("");
	    setCall_purpose_name("");
	    setCall_type_name("");
	    setDate_interval("");
	    setDuration_interval("");
    }

	public ActivityCallsModel(Parcel in) {
	    readFromParcel(in);
    }

	public void readFromParcel (Parcel in) {
		date = in.readString();
		durationLabel = in.readString();
		subject = in.readString();
		createdAt = in.readString();
		callPurposeId = in.readString();
		deletedAt = in.readString();
		duration = in.readString();
		accountId = in.readString();
		callTypeId = in.readString();
		updatedAt = in.readString();
		userId = in.readString();
		id = in.readString();
		detail = in.readString();
		status = in.readString();
		user_name = in.readString();
		account_name = in.readString();
		call_purpose_name = in.readString();
		call_type_name = in.readString();
		date_interval = in.readString();
		duration_interval = in.readString();
	}

	public static final Creator<ActivityCallsModel> CREATOR = new Creator<ActivityCallsModel>() {
		@Override
		public ActivityCallsModel createFromParcel(Parcel in) {
			return new ActivityCallsModel(in);
		}

		@Override
		public ActivityCallsModel[] newArray(int size) {
			return new ActivityCallsModel[size];
		}
	};

	public void setDate(String date){
		this.date = date;
	}

    public String getAccount_name() {
        return account_name;
    }

    public void setAccount_name(String account_name) {
        this.account_name = account_name;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getCall_purpose_name() {
        return call_purpose_name;
    }

    public void setCall_purpose_name(String call_purpose_name) {
        this.call_purpose_name = call_purpose_name;
    }

    public String getCall_type_name() {
        return call_type_name;
    }

    public void setCall_type_name(String call_type_name) {
        this.call_type_name = call_type_name;
    }

    public String getDuration_interval() {
        return duration_interval;
    }

    public void setDuration_interval(String duration_interval) {
        this.duration_interval = duration_interval;
    }

    public String getDate_interval() {
        return date_interval;
    }

    public void setDate_interval(String date_interval) {
        this.date_interval = date_interval;
    }

    public String getDate(){
		return date;
	}

	public void setDurationLabel(String durationLabel){
		this.durationLabel = durationLabel;
	}

	public String getDurationLabel(){
		return durationLabel;
	}

	public void setSubject(String subject){
		this.subject = subject;
	}

	public String getSubject(){
		return subject;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setCallPurposeId(String callPurposeId){
		this.callPurposeId = callPurposeId;
	}

	public String getCallPurposeId(){
		return callPurposeId;
	}

	public void setDeletedAt(String deletedAt){
		this.deletedAt = deletedAt;
	}

	public String getDeletedAt(){
		return deletedAt;
	}

	public void setDuration(String duration){
		this.duration = duration;
	}

	public String getDuration(){
		return duration;
	}

	public void setAccountId(String accountId){
		this.accountId = accountId;
	}

	public String getAccountId(){
		return accountId;
	}

	public void setCallTypeId(String callTypeId){
		this.callTypeId = callTypeId;
	}

	public String getCallTypeId(){
		return callTypeId;
	}

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setDetail(String detail){
		this.detail = detail;
	}

	public String getDetail(){
		return detail;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int i) {
		parcel.writeString(date);
		parcel.writeString(durationLabel);
		parcel.writeString(subject);
		parcel.writeString(createdAt);
		parcel.writeString(callPurposeId);
		parcel.writeString(deletedAt);
		parcel.writeString(duration);
		parcel.writeString(accountId);
		parcel.writeString(callTypeId);
		parcel.writeString(updatedAt);
		parcel.writeString(userId);
		parcel.writeString(id);
		parcel.writeString(detail);
		parcel.writeString(status);
		parcel.writeString(account_name);
		parcel.writeString(user_name);
		parcel.writeString(call_purpose_name);
		parcel.writeString(call_type_name);
		parcel.writeString(date_interval);
		parcel.writeString(date_interval);
	}

	@Override
	public int compareTo(@NonNull Object o) {
		return 0;
	}
}
