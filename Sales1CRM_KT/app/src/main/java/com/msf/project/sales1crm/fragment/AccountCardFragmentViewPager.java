package com.msf.project.sales1crm.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.msf.project.sales1crm.AccountAddActivity;
import com.msf.project.sales1crm.ErrorAPIKeyActivity;
import com.msf.project.sales1crm.MenuActivity;
import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.adapter.AccountListCardViewPagerAdapter;
import com.msf.project.sales1crm.callback.AccountListCallback;
import com.msf.project.sales1crm.callback.DashboardCallback;
import com.msf.project.sales1crm.callback.SortDialogCallback;
import com.msf.project.sales1crm.customview.CustomDialog;
import com.msf.project.sales1crm.customview.CustomEditText;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.imageloader.ImageLoader;
import com.msf.project.sales1crm.model.AbsenceModel;
import com.msf.project.sales1crm.model.AccountModel;
import com.msf.project.sales1crm.model.DashboardFirstModel;
import com.msf.project.sales1crm.model.DashboardSecondModel;
import com.msf.project.sales1crm.model.DashboardThirdModel;
import com.msf.project.sales1crm.model.TotalOpportunityModel;
import com.msf.project.sales1crm.presenter.AccountListPresenter;
import com.msf.project.sales1crm.presenter.DashboardPresenter;
import com.msf.project.sales1crm.service.GPSTracker;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class AccountCardFragmentViewPager extends Fragment implements AccountListCallback, DashboardCallback {

    @BindView(R.id.etSearch)
    CustomEditText etSearch;
    @BindView(R.id.ivClear)
    ImageView ivClear;
    @BindView(R.id.tvRefresh)
    CustomTextView tvRefresh;
    @BindView(R.id.rlNoConnection)
    RelativeLayout rlNoConnection;
    @BindView(R.id.llNotFound)
    LinearLayout llNotFound;
    @BindView(R.id.pager)
    ViewPager pager;
    @BindView(R.id.lvAccountSwipe)
    SwipeRefreshLayout lvAccountSwipe;
    @BindView(R.id.faAccountAdd)
    FloatingActionButton faAccountAdd;
    @BindView(R.id.faQuickAdd)
    FloatingActionButton faQuickAdd;
    @BindView(R.id.menu)
    FloatingActionMenu menu;
    @BindView(R.id.circleIv)
    CircleImageView circleIv;
    @BindView(R.id.tvAccountName)
    CustomTextView tvAccountName;
    @BindView(R.id.tvindustry)
    CustomTextView tvindustry;
    @BindView(R.id.view1)
    View view1;
    @BindView(R.id.tvindustryy)
    CustomTextView tvindustryy;
    @BindView(R.id.tvType)
    CustomTextView tvType;
    @BindView(R.id.tvOwnerName)
    CustomTextView tvOwnerName;
    @BindView(R.id.tvtelephone)
    CustomTextView tvtelephone;
    @BindView(R.id.llCard)
    LinearLayout llCard;
    @BindView(R.id.tvCountOpportunity)
    CustomTextView tvCountOpportunity;
    @BindView(R.id.tvCountTask)
    CustomTextView tvCountTask;
    @BindView(R.id.tvCountEvent)
    CustomTextView tvCountEvent;
    @BindView(R.id.shimmerLayout)
    ShimmerFrameLayout shimmerLayout;
    @BindView(R.id.llshim)
    LinearLayout llshim;
    @BindView(R.id.rlAccountList)
    RelativeLayout rlAccountList;
    private View view;
    private Context context;
    private AccountListCardViewPagerAdapter adapter;
    private AccountListPresenter presenter;
    private DashboardPresenter dashboardPresenter;
    private List<AccountModel> list = new ArrayList<>();
    private String stringLatitude, stringLongitude;
    private CustomDialog dialogQuick;
    private String[] sort = {"Name A to Z", "Name Z to A", "Industry A to Z", "Industry Z to A"};
    private Toolbar mtoolbar;
    private boolean isScrolling = false;
    private int currentItems, totalItems, scrollOutItems;


    private int prevSize = 0, sort_id = 0;
    private boolean isLoadMore = true;
    private boolean isMaxSize = true;
    private boolean pause = false;

    public static Fragment newInstance() {
        AccountCardFragmentViewPager fragement = new AccountCardFragmentViewPager();
        return fragement;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_accountcard_vp, container, false);
        setHasOptionsMenu(true);
        ButterKnife.bind(this, this.view);
        return this.view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        this.context = getActivity();
        this.dashboardPresenter = new DashboardPresenter(context, this);
        this.presenter = new AccountListPresenter(context, this);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Account");

        initData();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_list:
                    MenuActivity.getInstance().setFragment(AccountFrangment.newInstance());
                    item.setIcon(R.drawable.ic_list_white_24dp);
                break;
            case R.id.action_cards:
                    MenuActivity.getInstance().setFragment(AccountCardFrangment.newInstance());
                    item.setIcon(R.drawable.ic_cards_black_24dp);
                break;
//            case R.id.action_maps:
//                checkFunction = new CheckFunction(getActivity().getApplicationContext());
//                if (!checkFunction.userAccessControl(1)) {
//                    Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
//                } else {
//                    MenuActivity.getInstance().setFragment(AccountMapStreesFragment.newInstance());
//                    item.setIcon(R.drawable.ic_map_white_24dp);
//                }
//                break;
            case R.id.action_filter:
                SortDialogFragment DialogFragment = new SortDialogFragment(context, new SortDialogCallback() {

                    @Override
                    public void onDialogCallback(Bundle data) {
                        sort_id = Integer.parseInt(data.getString("id"));

                        getDataFromAPI(0, sort_id);
//                            ivSelectSort.setVisibility(View.VISIBLE);
                    }
                }, sort);
                DialogFragment.show(getActivity().getSupportFragmentManager(), "sortDialog");
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    private void initData() {
        Sales1CRMUtils.Utils.imageLoader = new ImageLoader(context);

        etSearch.clearFocus();
//        lvAccount.setOnScrollChangeListener(new EndlessRecycleViewScrollListener());


        lvAccountSwipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                llshim.setVisibility(View.VISIBLE);
                shimmerLayout.setVisibility(View.VISIBLE);
                shimmerLayout.startShimmer();
                getDataFromAPI(0, 0);

            }
        });


        //Set Seach Function
        setTextWatcherForSearch();
        getDataFromAPI(0, 0);

        ivClear.setOnClickListener(click);
        tvRefresh.setOnClickListener(click);
//        rlSort.setOnClickListener(click);
    }

    private void fetchData() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                presenter.setupMoreList(ApiParam.API_010, prevSize, sort_id, etSearch.getText().toString(), stringLatitude, stringLongitude);
                adapter.notifyDataSetChanged();

            }
        }, 5000);
    }

    private void setTextWatcherForSearch() {
        etSearch.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    if (etSearch.getText().length() == 0) {
                        ivClear.setVisibility(View.VISIBLE);
                    }
//                } else {
//                    if (etSearch.getText().length() == 0) {
//                        ivClear.setVisibility(View.GONE);
//                    } else {
//                        ivClear.setVisibility(View.VISIBLE);
//                    }

                }
            }
        });

        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    llshim.setVisibility(View.VISIBLE);
                    shimmerLayout.setVisibility(View.VISIBLE);
                    shimmerLayout.startShimmer();
                    getDataFromAPI(0, 0);
                }

                return true;
            }
        });
    }

    private void getDataFromAPI(int prev, int sort) {
        isMaxSize = true;
        isLoadMore = true;

        llNotFound.setVisibility(View.GONE);

        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(context)) {
            GPSTracker gpsTracker = new GPSTracker(context);
            if (gpsTracker.canGetLocation()) {
                stringLatitude = String.valueOf(gpsTracker.latitude);
                stringLongitude = String.valueOf(gpsTracker.longitude);

                pause = false;
                dashboardPresenter.setupFirst(ApiParam.API_005);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        presenter.setupList(ApiParam.API_010, prev, sort, etSearch.getText().toString(), stringLatitude, stringLongitude);
                    }
                },1000);
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gpsTracker.showSettingsAlert();
                lvAccountSwipe.setRefreshing(false);
            }
        } else {
            lvAccountSwipe.setVisibility(View.GONE);
            rlNoConnection.setVisibility(View.VISIBLE);
        }
    }

    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.ivClear:
                    etSearch.setText("");
                    etSearch.clearFocus();
                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);
                    getDataFromAPI(0, 0);
                    break;
                case R.id.tvRefresh:
                    llshim.setVisibility(View.VISIBLE);
                    rlNoConnection.setVisibility(View.GONE);
                    getDataFromAPI(0, 0);
                    break;
                default:
                    break;
            }
        }
    };

    private void setList() {
        llshim.setVisibility(View.VISIBLE);
        shimmerLayout.setVisibility(View.GONE);
        adapter = new AccountListCardViewPagerAdapter(context, this.list);
        List<AccountListCardViewPagerAdapter.Row> rows = new ArrayList<AccountListCardViewPagerAdapter.Row>();
        for (AccountModel cartModel : list) {
            //Read List by Row to insert into List Adapter
            rows.add(new AccountListCardViewPagerAdapter.Item(cartModel));
        }

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if (isLoadMore == false && isMaxSize == false) {
                    isLoadMore = true;
                    fetchData();
                    Log.d("TAG", "POSITION" + position);
                }

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        adapter.setRows(rows);
        pager.setAdapter(adapter);
        llshim.setVisibility(View.INVISIBLE);
        shimmerLayout.stopShimmer();
        shimmerLayout.setVisibility(View.INVISIBLE);
    }

    private void setMoreList() {
        List<AccountListCardViewPagerAdapter.Row> rows = new ArrayList<AccountListCardViewPagerAdapter.Row>();
        for (AccountModel country : list) {
            // Add the country to the list
            rows.add(new AccountListCardViewPagerAdapter.Item(country));
        }
        adapter.setRows(rows);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void finishAccountList(String result, List<AccountModel> list) {
        lvAccountSwipe.setRefreshing(false);
        if (result.equalsIgnoreCase("OK")) {
            prevSize = list.size();
            this.list = list;

            isMaxSize = false;
            isLoadMore = false;
            setList();
            if (list.size() > 0) {
                llNotFound.setVisibility(View.GONE);
            } else {
                llNotFound.setVisibility(View.VISIBLE);
            }
        } else if (result.equalsIgnoreCase("FAILED_KEY")) {
            startActivity(new Intent(context, ErrorAPIKeyActivity.class));
            MenuActivity.getInstance().finish();
        } else {
            rlNoConnection.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void finishAccountMoreList(String result, List<AccountModel> list) {
        if (result.equals("OK")) {
            for (int i = 0; i < list.size(); i++) {
                this.list.add(list.get(i));
            }

            if (this.prevSize == this.list.size())
                isMaxSize = true;
            this.prevSize = this.list.size();
            setMoreList();
        }
        this.isLoadMore = false;
    }

    @Override
    public void finishQuickCreate(String result) {
        if (result.equalsIgnoreCase("OK")) {
            dialogQuick.dismiss();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onPause() {
        pause = true;
        super.onPause();
    }

    @Override
    public void onResume() {
        if (pause) {
            getDataFromAPI(0, 0);
        }
        super.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void finishFirst(String result, DashboardFirstModel model) {
        if(model.getAccount_create_permission().equalsIgnoreCase("1")){
            faAccountAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(context, AccountAddActivity.class));
                }
            });
            faAccountAdd.setColorNormal(R.color.BlueCRM);
            faAccountAdd.setColorPressed(R.color.colorPrimaryDark);
            faAccountAdd.setColorPressedResId(R.color.colorPrimaryDark);
            faAccountAdd.setColorNormalResId(R.color.BlueCRM);

            faQuickAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialogQuick = CustomDialog.setupDialogAccountAdd(context);
                    final CustomEditText etName = (CustomEditText) dialogQuick.findViewById(R.id.etAccountName);
                    final CustomEditText etPhone = (CustomEditText) dialogQuick.findViewById(R.id.etPhone);
                    CustomTextView tvYa = (CustomTextView) dialogQuick.findViewById(R.id.tvOK);
                    CustomTextView tvTidak = (CustomTextView) dialogQuick.findViewById(R.id.tvBack);

                    tvYa.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            AccountModel model = new AccountModel();
                            model.setAccount_name(etName.getText().toString());
                            model.setPhone(etPhone.getText().toString());

                            presenter.setupCreate(ApiParam.API_013, model);
                        }
                    });
                    tvTidak.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialogQuick.dismiss();
                        }
                    });
                    dialogQuick.show();
                }
            });

            faQuickAdd.setColorNormal(R.color.BlueCRM);
            faQuickAdd.setColorPressed(R.color.colorPrimaryDark);
            faQuickAdd.setColorPressedResId(R.color.colorPrimaryDark);
            faQuickAdd.setColorNormalResId(R.color.BlueCRM);

        } else {
            faAccountAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
                }
            });
            faAccountAdd.setColorNormal(R.color.BlueCRM_light);
            faAccountAdd.setColorPressed(R.color.colorPrimaryDark_light);
            faAccountAdd.setColorPressedResId(R.color.colorPrimaryDark_light);
            faAccountAdd.setColorNormalResId(R.color.BlueCRM_light);

            faQuickAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
                }
            });
            faQuickAdd.setColorNormal(R.color.BlueCRM_light);
            faQuickAdd.setColorPressed(R.color.colorPrimaryDark_light);
            faQuickAdd.setColorPressedResId(R.color.colorPrimaryDark_light);
            faQuickAdd.setColorNormalResId(R.color.BlueCRM_light);
        }
    }

    @Override
    public void finishSecond(String result, DashboardSecondModel model) {

    }

    @Override
    public void finishThird(String result, List<DashboardThirdModel> listLead, List<DashboardThirdModel> listLead1, List<DashboardThirdModel> listOppor, List<DashboardThirdModel> listOppor1) {

    }

    @Override
    public void TotOpportunity(String result, TotalOpportunityModel totOporModel) {

    }

    @Override
    public void absence(String result, AbsenceModel model) {

    }
}
