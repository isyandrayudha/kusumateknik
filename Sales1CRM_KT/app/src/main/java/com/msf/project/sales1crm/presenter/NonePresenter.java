package com.msf.project.sales1crm.presenter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;

import com.msf.project.sales1crm.callback.NoneCallback;
import com.msf.project.sales1crm.model.OpportunityModel;
import com.msf.project.sales1crm.model.TotalNPriceOpporModel;
import com.msf.project.sales1crm.service.NetworkConnection;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.PreferenceUtility;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;
import com.msf.project.sales1crm.utility.Sales1CRMUtilsJSON;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class NonePresenter {
    private final String TAG = NonePresenter.class.getSimpleName();
    private Context context;
    private Messenger messenger;
    private Message message;
    private Bundle bundle;
    private Handler handler;
    private String failureResponse = "";
    private String[] stringResponse = {""};
    private NoneCallback callback;
    private TotalNPriceOpporModel totalNPriceOpporModel;
    private String result = "NG";
    private List<OpportunityModel> list = new ArrayList<>();

    public NonePresenter(Context context, NoneCallback callback) {
        this.context = context;
        this.callback = callback;
    }

    private void doNetworkService(String url, String params, String from) {
        Log.d(TAG, "url " + url);
        Log.d(TAG, "params : " + params);
        Intent networkIntent = new Intent(this.context, NetworkConnection.class);
        this.messenger = new Messenger(this.handler);
        networkIntent.putExtra("messenger", this.messenger);
        networkIntent.putExtra("url", url);
        networkIntent.putExtra("params", params);
        networkIntent.putExtra("from", from);
        this.context.startService(networkIntent);
    }

    public void listNone(int apiIndex, int offset, String stage_id) {
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)) {
            obtainNone(apiIndex, offset, stage_id);
        } else {
            callback.finishNone(this.result, this.list);
        }

    }

    private void obtainNone(int apiIndex, int offset, String stage_id) {
        this.handler = new Handler(this.context.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                parseListNone(msg);
            }
        };
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("api_key", PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.API_KEY));
            jsonObject.put("offset", offset + "");
            jsonObject.put("stage_id", stage_id + "1");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        doNetworkService(ApiParam.urlBuilder(this.context, apiIndex), jsonObject.toString(), "OpportunityListByMQL");
    }


    private void parseListNone(Message message) {
        this.message = message;
        this.bundle = this.message.getData();
        this.stringResponse[0] = this.bundle.getString("network_response");
        this.failureResponse = this.bundle.getString("network_failure");
        Log.d(TAG, "responseString[0] info None " + this.stringResponse[0]);
        if (this.failureResponse.equalsIgnoreCase("yes")) {
            this.callback.finishNone(this.result, this.list);
        } else {
            try {
                this.result = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                if (this.result.equalsIgnoreCase("OK")) {
                    this.list = Sales1CRMUtilsJSON.JSONUtility.getOpportunityListbyMQL(this.stringResponse[0]);
                }
                this.callback.finishNone(this.result, this.list);
            } catch (JSONException e) {
                Log.d(TAG, "EXCEPTION DETAIL RESPONSE " + e.getMessage());
            }
        }
    }

    public void moreListNone(int apiIndex, int offset, String stage_id) {
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)) {
            obtainMoreNone(apiIndex, offset, stage_id);
        } else {
            callback.finishMoreNone(this.result, this.list);
        }
    }

    private void obtainMoreNone(int apiIndex, int offset, String stage_id) {
        this.handler = new Handler(this.context.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                parseListNone(msg);
            }
        };
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("api_key", PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.API_KEY));
            jsonObject.put("offset", offset + "");
            jsonObject.put("stage_id", stage_id + "1");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        doNetworkService(ApiParam.urlBuilder(this.context, apiIndex), jsonObject.toString(), "OpportunityListByMQL");
    }

    private void parseMoreNone(Message message) {
        this.message = message;
        this.bundle = this.message.getData();
        this.stringResponse[0] = this.bundle.getString("network_response");
        this.failureResponse = this.bundle.getString("network_failure");
        Log.d(TAG, "responseString[0] info NONE More " + this.stringResponse[0]);
        if (this.failureResponse.equalsIgnoreCase("yes")) {
            this.callback.finishMoreNone(this.result, this.list);
        } else {
            try {
                this.result = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                if (this.result.equalsIgnoreCase("OK")) {
                    this.list = Sales1CRMUtilsJSON.JSONUtility.getOpportunityListbyMQL(this.stringResponse[0]);
                }
                this.callback.finishMoreNone(this.result, this.list);
            } catch (JSONException e) {
                Log.d(TAG, "Exception detail Response " + e.getMessage());
            }

        }

    }

    public void totNone(int apiIndex, int offset, String stage_id) {
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)){
            obtainTotNone(apiIndex,offset, stage_id);
        } else {
            callback.totNone(this.result,this.totalNPriceOpporModel);
        }
    }

    private void obtainTotNone(int apiIndex, int offset, String stage_id) {
        this.handler = new Handler(this.context.getMainLooper()) {

            @Override
            public void handleMessage(Message msg) {
                parseTotNone(msg);
            }

        };
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("api_key", PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.API_KEY));
            jsonObject.put("offset", offset+"");
            jsonObject.put("stage_id",stage_id+"1");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        doNetworkService(ApiParam.urlBuilder(this.context, apiIndex), jsonObject.toString(), "OpportunityListByMQL");

    }

    private void parseTotNone(Message message) {
        this.message = message;
        this.bundle = this.message.getData();
        this.stringResponse[0] = this.bundle.getString("network_response");
        this.failureResponse = this.bundle.getString("network_failure");
        Log.d(TAG, "responseString[0] info total none " + this.stringResponse[0]);
        if (this.failureResponse.equalsIgnoreCase("yes")) {
            this.callback.totNone(this.result, this.totalNPriceOpporModel);
        } else {
            try{
                this.result = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                if (this.result.equalsIgnoreCase("OK")){
                    this.totalNPriceOpporModel = Sales1CRMUtilsJSON.JSONUtility.getTotOpportunityBystage(this.stringResponse[0]);
                }
                this.callback.totNone(this.result, this.totalNPriceOpporModel);
            } catch (JSONException e){
                Log.d(TAG, "Exception detail Response " + e.getMessage());
            }

        }

    }
}
