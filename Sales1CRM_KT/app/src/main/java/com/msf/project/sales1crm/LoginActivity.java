package com.msf.project.sales1crm;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.msf.project.sales1crm.callback.LoginCallback;
import com.msf.project.sales1crm.customview.CustomEditText;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.model.UserModel;
import com.msf.project.sales1crm.presenter.LoginPresenter;
import com.msf.project.sales1crm.service.TrackingService;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.PermissionCheck;
import com.msf.project.sales1crm.utility.PreferenceUtility;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by christianmacbook on 11/05/18.
 */

public class LoginActivity extends BaseActivity implements LoginCallback {

    @BindView(R.id.etEmail)
    CustomEditText etEmail;

    @BindView(R.id.etPassword)
    CustomEditText etPassword;

    @BindView(R.id.cbVisiblePass)
    CheckBox cbVisiblePass;

    @BindView(R.id.rlForgotPassword)
    RelativeLayout rlForgotPassword;

    @BindView(R.id.tvLogin)
    CustomTextView tvLogin;
    @BindView(R.id.tvForgotPassword)
    CustomTextView tvForgotPassword;

//    @BindView(R.id.tvlogoutDomain)
//    CustomTextView tvlogoutDomain;

    private Context context;
    private LoginPresenter presenter;
    private PermissionCheck permissionCheck;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);
        this.context = this;

//        tvlogoutDomain.setPaintFlags(tvlogoutDomain.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        presenter = new LoginPresenter(context, this);

        this.permissionCheck = new PermissionCheck();
        this.permissionCheck.checkPermission(context);

        initView();
    }

    private void initView() {
        etEmail.setText(PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.KEY_EMAIL));
        etPassword.setText(PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.KEY_PASSWORD));

        etPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    getDataFromAPI();
                }
                return true;
            }
        });

        cbVisiblePass.setOnClickListener(click);
        tvLogin.setOnClickListener(click);
        tvForgotPassword.setOnClickListener(click);
//        tvlogoutDomain.setOnClickListener(click);
    }

    private void getDataFromAPI() {
        UserModel model = new UserModel();
        model.setEmail(etEmail.getText().toString().trim());
        model.setPassword(etPassword.getText().toString().trim());

        showLoadingDialog();
        presenter.setupLoginToServer(ApiParam.API_001, model);
    }

    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.cbVisiblePass:
                    if (cbVisiblePass.isChecked()) {
                        cbVisiblePass.setButtonDrawable(context
                                .getResources().getDrawable(
                                        R.drawable.ico_show_password));
                        etPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    } else {
                        cbVisiblePass.setButtonDrawable(context
                                .getResources().getDrawable(
                                        R.drawable.ico_hide_password));
                        etPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    }

                    break;
                case R.id.tvLogin:
                    getDataFromAPI();
                    break;
                case R.id.tvForgotPassword:
                    AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                    builder.setMessage("Please go to your subdomain in your browser. \nex: companyname.sales1crm.com")
                            .setCancelable(false)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.cancel();
                                }
                            });
                    AlertDialog alertDialog = builder.create();
                    alertDialog.show();
                    break;
//                case R.id.tvlogoutDomain:
//                    AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
//                    builder.setMessage("Are you sure?")
//                            .setCancelable(false)
//                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                                @Override
//                                public void onClick(DialogInterface dialogInterface, int i) {
//                                    Intent intentDomain = new Intent(LoginActivity.this,DomainCheckActivity.class);
//                                    startActivity(intentDomain);
//                                    finish();
//                                }
//                            })
//                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
//                                @Override
//                                public void onClick(DialogInterface dialogInterface, int i) {
//                                    dialogInterface.cancel();
//                                }
//                            });
//                    AlertDialog alertDialog = builder.create();
//                    alertDialog.show();
//                    break;
                default:
                    break;
            }
        }
    };


    @Override
    public void finishedLogin(String result, String response) {
        dismissLoadingDialog();
        if (result.equalsIgnoreCase("OK")) {
            PreferenceUtility.getInstance().saveData(context, PreferenceUtility.KEY_EMAIL, etEmail.getText().toString());
            PreferenceUtility.getInstance().saveData(context, PreferenceUtility.KEY_PASSWORD, etPassword.getText().toString());

            if (PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.KEY_SYNCMASTER).equalsIgnoreCase("1")) {
                startActivity(new Intent(context, MenuActivity.class));
                startTrackerService();
            } else {
                startActivity(new Intent(context, SyncMasterActivity.class));
                startTrackerService();
            }
            finish();
        } else {
            Snackbar snackbar = Snackbar
                    .make(findViewById(R.id.llContainer), response, Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    private void startTrackerService() {
        startService(new Intent(this, TrackingService.class));

//Notify the user that tracking has been enabled//

//        Toast.makeText(this, "GPS tracking enabled", Toast.LENGTH_SHORT).show();

//Close MainActivity//

        finish();
    }
}
