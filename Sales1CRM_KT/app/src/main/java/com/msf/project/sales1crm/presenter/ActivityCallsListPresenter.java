package com.msf.project.sales1crm.presenter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;

import com.msf.project.sales1crm.callback.ActivityListCallback;
import com.msf.project.sales1crm.model.ActivityCallsModel;
import com.msf.project.sales1crm.service.NetworkConnection;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.PreferenceUtility;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;
import com.msf.project.sales1crm.utility.Sales1CRMUtilsJSON;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ActivityCallsListPresenter {
    private final String TAG = ActivityCallsListPresenter.class.getSimpleName();
    private Context context;
    private Messenger messenger;
    private Message message;
    private Bundle bundle;
    private Handler handler;
    private String failureResponse = "";
    private String[] stringResponse = {""};
    private ActivityListCallback callback;
    private String result = "NG";
    private List<ActivityCallsModel> list = new ArrayList<>();

    public ActivityCallsListPresenter(Context context, ActivityListCallback listener) {
        this.context = context;
        this.callback = listener;
    }

    private void doNetworkService(String url, String params, String from) {
        Log.d(TAG, "url activity calls " + url);
        Log.d(TAG, "params : " + params);
        Intent networkIntent = new Intent(this.context, NetworkConnection.class);
        this.messenger = new Messenger(this.handler);
        networkIntent.putExtra("messenger", this.messenger);
        networkIntent.putExtra("url", url);
        networkIntent.putExtra("params", params);
        networkIntent.putExtra("from", from);
        this.context.startService(networkIntent);
    }

    public void setupCallList(int apiIndex, int offset, String account_id,String date) {
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)) {
            obtainListCallItem(apiIndex, offset, account_id, date);
        } else {
            callback.finishActivityCallList(this.result, this.list);
        }
    }

    public void obtainListCallItem(int apiIndex, int offset, String account_id, String date) {
        this.handler = new Handler(this.context.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                parseListCallitem(msg);
            }
        };

        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("api_key", PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.API_KEY));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        doNetworkService(ApiParam.urlBuilder(this.context,apiIndex),jsonObject.toString(),"ActivityCallsList");

    }

    private void parseListCallitem(Message message) {
        this.message = message;
        this.bundle = this.message.getData();
        this.stringResponse[0] = this.bundle.getString("network_response");
        this.failureResponse = this.bundle.getString("network_failure");
        Log.d(TAG, "responseString[0] Activity calls info " + this.stringResponse[0]);
        if (this.failureResponse.equalsIgnoreCase("yes")) {
            this.callback.finishActivityCallList(this.result, this.list);
        } else {
            try {
                this.result = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                if (this.result.equalsIgnoreCase("OK")) {
                    this.list = Sales1CRMUtilsJSON.JSONUtility.getActivityCallsList(this.stringResponse[0]);
                }
//                this.callback.finishActivityCallList(this.result, this.list);
                this.callback.finishActivityCallList(this.result,this.list);
            } catch (JSONException e) {
                Log.d(TAG, "Exception detail Response " + e.getMessage());

            }
        }
    }

    public void setupMoreListCalls(int apiIndex, int offset, String account_id,String date) {
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)) {
        obtainMoreListCallItem(apiIndex,offset,account_id,date);
        } else {
            callback.finishActivityCallMoreList(this.result, this.list);
        }
    }

    public void obtainMoreListCallItem(int apiIndex, int offset, String account_id, String date) {
        this.handler = new Handler(this.context.getMainLooper()){
            @Override
            public void handleMessage(Message msg) {
                parseMoreCallListItem(msg);
            }
        };
        JSONObject jsonObject =  new JSONObject();
        try{
            jsonObject.put("api_key",PreferenceUtility.getInstance().loadDataString(context,PreferenceUtility.API_KEY));

        }catch (JSONException e){
            e.printStackTrace();
        }
        doNetworkService(ApiParam.urlBuilder(this.context,apiIndex),jsonObject.toString(),"ActivityCallsList");
    }


    private void parseMoreCallListItem(Message message) {
        this.message = message;
        this.bundle = this.message.getData();
        this.stringResponse[0] = this.bundle.getString("network_response");
        this.failureResponse = this.bundle.getString("network_failure");
        Log.d(TAG, "responseString[0] login info " + this.stringResponse[0]);
        if (this.failureResponse.equalsIgnoreCase("yes")) {
            this.callback.finishActivityCallMoreList(this.result, this.list);
        } else {
            try {
                this.result = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                if (this.result.equalsIgnoreCase("OK")) {
                    this.list = Sales1CRMUtilsJSON.JSONUtility.getActivityCallsList(this.stringResponse[0]);
                }
                this.callback.finishActivityCallMoreList(this.result, this.list);
            } catch (JSONException e) {
                Log.d(TAG, "Exception detail Response " + e.getMessage());
            }
        }
    }

}

