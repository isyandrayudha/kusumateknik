package com.msf.project.sales1crm.callback;

import com.msf.project.sales1crm.model.ActivityTodoModel;

import java.util.List;

public interface Tcallback {
    void finishATodoList(String result, List<ActivityTodoModel> list);
    void finishATodoMoreList(String result, List<ActivityTodoModel> list);
}
