package com.msf.project.sales1crm.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.msf.project.sales1crm.fragment.OpportunityTabPersonalNewFragment;
import com.msf.project.sales1crm.fragment.OpportunityTabProductNewFragment;

public class OpportunityDetailTabNewAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;
    Bundle bundle;

    public OpportunityDetailTabNewAdapter(FragmentManager fm, int NumOfTabs, Bundle bundle) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
        this.bundle = bundle;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                OpportunityTabPersonalNewFragment tab1 = new OpportunityTabPersonalNewFragment();
                tab1.setArguments(bundle);
                return  tab1;
            case 1:
                OpportunityTabProductNewFragment tab2 = new OpportunityTabProductNewFragment();
                tab2.setArguments(bundle);
                tab2.setArguments(bundle);
               return  tab2;
        }
        return null;
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
