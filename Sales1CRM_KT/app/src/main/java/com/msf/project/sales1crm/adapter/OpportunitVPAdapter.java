package com.msf.project.sales1crm.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.msf.project.sales1crm.OpportunityDetailByStageActivity;
import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.model.OpportunityModel;

import java.util.ArrayList;
import java.util.List;

public class OpportunitVPAdapter extends RecyclerView.Adapter<OpportunitVPAdapter.ViewHolder> {

    private List<OpportunityModel> list = new ArrayList<>();
    private Context context;
    private int size;
    private String iterated;

    public OpportunitVPAdapter(Context contextm, List<OpportunityModel> obj) {
        this.context = contextm;
        this.list = obj;
    }

    public static abstract class Row{

    }

    public static final class Section extends Row {
        public final String text;

        public Section(String text) {
            this.text = text;
        }
    }


    public static final class Item extends Row{
        public final OpportunityModel text;

        public Item(OpportunityModel text) {
            this.text = text;
        }
        public OpportunityModel getItem(){
            return text;
        }
    }


    private  List<Row> rows;

    public void setRows(List<Row> rows){
        this.rows = rows;
        notifyDataSetChanged();
    }



    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_stage_vp, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final  Item item = (Item) rows.get(position);
        Log.d("TAG","POSTION " + position);
        Log.i("TAG",
                "number: "
                        + coolFormat(Double.parseDouble(item.text
                        .getSize()), 0));

        holder.tvOpportunityName.setText(item.text.getName());
        holder.tvAccountName.setText(item.text.getAccount_name());
        holder.tvStage.setText(item.text.getStage_name());
        holder.tvSize.setText(size + iterated);
        holder.tvProgress.setText(item.text.getProgress() + "%");
        holder.tvCloseDate.setText(item.text.getClose_date());
        holder.tvrefNumber.setText(item.text.getComplete_ref_number());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, OpportunityDetailByStageActivity.class);
                intent.putExtra("opportunityModel",item.getItem());
                intent.putExtra("id",item.getItem().getId());

                context.startActivity(intent);
            }
        });


    }

    private char[] c = new char[]{'K', 'M', 'B', 'T'};

    private String coolFormat(double n, int iteration) {
        double d = ((long) n / 100) / 10.0;
        boolean isRound = (d * 10) % 10 == 0;// true if the decimal part is
        // equal to 0 (then it's trimmed
        // anyway)
        Log.i("TAG", "num : " + d);
        size = (int) d;
        iterated = String.valueOf(c[iteration]);
        Log.i("TAG", "iteration : " + c[iteration]);
        return (d < 1000 ? // this determines the class, i.e. 'k', 'm' etc
                ((d > 99.9 || isRound || (!isRound && d > 9.99) ? // this decides
                        // whether to trim
                        // the decimals
                        (int) d * 10 / 10
                        : d + "" // (int) d * 10 / 10 drops the decimal
                ) + "" + c[iteration])
                : coolFormat(d, iteration + 1));
    }


    @Override
    public int getItemCount() {
        return rows.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CustomTextView tvOpportunityName;
        CustomTextView tvAccountName;
        CustomTextView tvProgress;
        CustomTextView tvSize;
        CustomTextView tvStage;
        CustomTextView tvCloseDate;
        CustomTextView tvrefNumber;
        LinearLayout llContainer;


        public ViewHolder(View itemView) {
            super(itemView);
            tvOpportunityName = itemView.findViewById(R.id.tvOpportunityName);
            tvAccountName = itemView.findViewById(R.id.tvAccountName);
            tvProgress = itemView.findViewById(R.id.tvProgress);
            tvSize = itemView.findViewById(R.id.tvSize);
            tvStage = itemView.findViewById(R.id.tvStage);
            tvCloseDate = itemView.findViewById(R.id.tvCloseDate);
            llContainer = itemView.findViewById(R.id.llContainer);
            tvrefNumber = itemView.findViewById(R.id.tvrefNumber);
        }
    }

}
