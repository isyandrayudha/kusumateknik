package com.msf.project.sales1crm.firebase;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.msf.project.sales1crm.utility.PreferenceUtility;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

/**
 * Created by christianmacbook on 11/05/18.
 */

public class FirebaseInstanceServices extends FirebaseInstanceIdService {

        public static final String TAG = FirebaseInstanceServices.class.getSimpleName();

        @Override
        public void onTokenRefresh() {
        super.onTokenRefresh();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        // Saving reg id to shared preferences
        storeRegIdInPref(refreshedToken);

        // sending reg id to your server
        sendRegistrationToServer(refreshedToken);

        // Notify UI that registration has completed, so the progress indicator can be hidden.
        Intent registrationComplete = new Intent(Sales1CRMUtils.REGISTRATION_COMPLETE);
        registrationComplete.putExtra("token", refreshedToken);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);

    }

    private void sendRegistrationToServer(final String token) {
        // sending gcm token to server
        Log.d(TAG, "sendRegistrationToServer: " + token);
    }

    private void storeRegIdInPref(String token) {

        Log.d(TAG, "Device Token : " + token);
        PreferenceUtility.getInstance().saveData(getApplicationContext(),
                PreferenceUtility.PREFERENCES_PROPERTY_REG_ID, token);

    }
}
