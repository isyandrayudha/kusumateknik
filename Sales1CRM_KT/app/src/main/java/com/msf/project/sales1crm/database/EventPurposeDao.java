package com.msf.project.sales1crm.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.msf.project.sales1crm.model.EventPurposeModel;

import java.util.ArrayList;
import java.util.List;

public class EventPurposeDao extends BaseDao<EventPurposeModel> implements DBSchema.EventPurpose {
    public EventPurposeDao(Context c) {
        super(c, TABLE_NAME);
    }

    @Override
    public EventPurposeModel getById(int id) {
        String qry = "SELECT * FROM " + getTable() + " WHERE " + COL_ID + " = " + id;
        Cursor c = getSqliteDb().rawQuery(qry, null);
        EventPurposeModel model = new EventPurposeModel();
        try {
            if(c != null && c.moveToFirst()) {
                model = getByCursor(c);
            }
        } finally {
            c.close();
        }
        return model;
    }

    public List<EventPurposeModel> getEventPurposeList() {
        String query = "SELECT * FROM " + getTable();
        Cursor c = getSqliteDb().rawQuery(query,null);
        List<EventPurposeModel> list = new ArrayList<>();
        try{
            if(c != null && c.moveToFirst()) {
                list.add(getByCursor(c));
                while (c.moveToNext()) {
                    list.add(getByCursor(c));

                }
            }
        } finally {
            c.close();
        }

        return list;
    }

    public String[] getStringArray(){
        String query = "SELECT * FROM " + getTable();

        Cursor c = getSqliteDb().rawQuery(query, null);
        String[] array = new String[c.getCount() + 1];
        try {
            if(c != null && c.moveToFirst()) {
                array[0] = "EventType";
                array[1] = getByCursor(c).getName();
                int i = 2;
                while (c.moveToNext()) {
                    array[i] = getByCursor(c).getName();
                    i++;

                }
            }
        } finally {
            c.close();
        }

        return array;
    }

    @Override
    public EventPurposeModel getByCursor(Cursor c) {
        EventPurposeModel model = new EventPurposeModel();
        model.setId(c.getString(0));
        model.setName(c.getString(1));
        return model;
    }

    @Override
    protected ContentValues upDataValues(EventPurposeModel model, boolean update) {
        ContentValues cv = new ContentValues();
        if(update == true)
            cv.put(COL_ID,model.getId());
        cv.put(KEY_NAME,model.getName());
        return cv;
    }
}
