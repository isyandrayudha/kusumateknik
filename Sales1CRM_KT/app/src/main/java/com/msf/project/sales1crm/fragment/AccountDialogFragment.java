package com.msf.project.sales1crm.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.msf.project.sales1crm.ErrorAPIKeyActivity;
import com.msf.project.sales1crm.MenuActivity;
import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.adapter.AccountListAdapter;
import com.msf.project.sales1crm.callback.AccountDialogCallback;
import com.msf.project.sales1crm.callback.AccountListCallback;
import com.msf.project.sales1crm.customview.CustomEditText;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.imageloader.ImageLoader;
import com.msf.project.sales1crm.model.AccountModel;
import com.msf.project.sales1crm.presenter.AccountListPresenter;
import com.msf.project.sales1crm.service.GPSTracker;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.BindView;

@SuppressLint("ValidFragment")
public class AccountDialogFragment extends BaseDialogFragment implements AccountListCallback {

    @BindView(R.id.rlNoConnection)
    RelativeLayout rlNoConnection;

    @BindView(R.id.tvRefresh)
    CustomTextView tvRefresh;

    @BindView(R.id.etSearch)
    CustomEditText etSearch;

    @BindView(R.id.ivClear)
    ImageView ivClear;

    @BindView(R.id.lvAccount)
    ListView lvAccount;

    @BindView(R.id.lvAccountSwipe)
    SwipeRefreshLayout lvAccountSwipe;

    @BindView(R.id.llNotFound)
    LinearLayout llNotFound;

    @BindView(R.id.rlProgress)
    RelativeLayout rlProgress;

    private View view;
    private Context context;
    private AccountListAdapter adapter;
    private AccountListPresenter presenter;
    private AccountDialogCallback callback;
    private List<AccountModel> list = new ArrayList<>();
    private String stringLatitude, stringLongitude;

    private int prevSize = 0;
    private boolean isLoadMore = true;
    private boolean isMaxSize = true;
    private boolean pause = false;

    @SuppressLint("ValidFragment")
    public AccountDialogFragment(Context context, AccountDialogCallback listener) {
        this.context = context;
        this.callback = listener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, 0);
    }

    @Override
    public void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
        Window window = getDialog().getWindow();
        WindowManager.LayoutParams windowParams = window.getAttributes();
        windowParams.width = (int) (this.context.getResources().getDisplayMetrics().widthPixels * 0.99);
        windowParams.height = (int) (this.context.getResources().getDisplayMetrics().heightPixels * 0.97);
        window.setAttributes(windowParams);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreateView(inflater, container, savedInstanceState);

        this.view = inflater.inflate(R.layout.dialog_accountlist, container, false);

        return this.view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);

        this.presenter = new AccountListPresenter(context, this);

        initView();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
    }

    private void initView() {
        Sales1CRMUtils.Utils.imageLoader = new ImageLoader(context);

        etSearch.clearFocus();
        lvAccount.setOnScrollListener(new AccountDialogFragment.ListLazyLoad());
        lvAccountSwipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getDataFromAPI(0);
            }
        });

        //Set Seach Function
        setTextWatcherForSearch();
        getDataFromAPI(0);

        ivClear.setOnClickListener(click);
        tvRefresh.setOnClickListener(click);
    }

    private void setTextWatcherForSearch() {
        etSearch.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                {
                    if (etSearch.getText().length() == 0){
                        ivClear.setVisibility(View.VISIBLE);
                    }
                } else {
                    if (etSearch.getText().length() == 0)
                    {
                        ivClear.setVisibility(View.GONE);
                    }else{
                        ivClear.setVisibility(View.VISIBLE);
                    }

                }
            }
        });

        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    getDataFromAPI(0);
                }

                return true;
            }
        });
    }

    private void getDataFromAPI(int prev){
        isMaxSize = true;
        isLoadMore = true;

        llNotFound.setVisibility(View.GONE);

        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(context)){
            GPSTracker gpsTracker = new GPSTracker(context);
            if (gpsTracker.canGetLocation()) {
                stringLatitude = String.valueOf(gpsTracker.latitude);
                stringLongitude = String.valueOf(gpsTracker.longitude);

                pause = false;
                presenter.setupList(ApiParam.API_010, prev, 0, etSearch.getText().toString(), stringLatitude, stringLongitude);
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gpsTracker.showSettingsAlert();
                lvAccountSwipe.setRefreshing(false);
            }
        }else{
            lvAccountSwipe.setVisibility(View.GONE);
            rlNoConnection.setVisibility(View.VISIBLE);
        }
    }

    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.ivClear:
                    etSearch.setText("");
                    etSearch.clearFocus();
                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);
                    getDataFromAPI(0);
                    break;
                case R.id.tvRefresh:
                    lvAccountSwipe.setVisibility(View.VISIBLE);
                    rlNoConnection.setVisibility(View.GONE);

                    getDataFromAPI(0);
                    break;
                default:
                    break;
            }
        }
    };

    private void setList() {
        adapter = new AccountListAdapter(context, this.list);

        List<AccountListAdapter.Row> rows = new ArrayList<AccountListAdapter.Row>();
        for (AccountModel cartModel : list){
            //Read List by Row to insert into List Adapter
            rows.add(new AccountListAdapter.Item(cartModel));
        }
        //Set Row Adapter
        adapter.setRows(rows);
        lvAccount.setAdapter(adapter);
        lvAccount.setOnItemClickListener(new AccountDialogFragment.AdapterOnItemClick());
        lvAccount.deferNotifyDataSetChanged();
    }

    private void setMoreList() {
        List<AccountListAdapter.Row> rows = new ArrayList<AccountListAdapter.Row>();

        for (AccountModel country : list) {
            // Add the country to the list
            rows.add(new AccountListAdapter.Item(country));
        }
        adapter.setRows(rows);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void finishAccountList(String result, List<AccountModel> list) {
        lvAccountSwipe.setRefreshing(false);
        rlProgress.setVisibility(View.GONE);
        if (result.equalsIgnoreCase("OK")){
            prevSize = list.size();
            this.list = list;

            isMaxSize = false;
            isLoadMore = false;

            setList();
            if (list.size() > 0){
                llNotFound.setVisibility(View.GONE);
            }else{
                llNotFound.setVisibility(View.VISIBLE);
            }
        }else if (result.equalsIgnoreCase("FAILED_KEY")){
            startActivity(new Intent(context, ErrorAPIKeyActivity.class));
            MenuActivity.getInstance().finish();
        }else{
            lvAccountSwipe.setVisibility(View.GONE);
            rlNoConnection.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void finishAccountMoreList(String result, List<AccountModel> list) {

        if (result.equals("OK")) {
            for (int i = 0; i < list.size(); i++) {
                this.list.add(list.get(i));
            }

            if (this.prevSize == this.list.size())
                isMaxSize = true;
            this.prevSize = this.list.size();
            setMoreList();
        }
        this.isLoadMore = false;
    }

    @Override
    public void finishQuickCreate(String result) {

    }

    private class ListLazyLoad implements AbsListView.OnScrollListener {

        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {

        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            if(firstVisibleItem+visibleItemCount == totalItemCount && totalItemCount!=0)
            {
                Log.d("TAG", "isLoadMore : " + isLoadMore);
                if(isLoadMore == false && isMaxSize == false)
                {
                    isLoadMore = true;
                    prevSize = list.size();

                    presenter.setupMoreList(ApiParam.API_010, prevSize, 0, etSearch.getText().toString(), stringLatitude, stringLongitude);
                }
            }
        }
    }

    private class AdapterOnItemClick implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            // TODO Auto-generated method stub
            final AccountListAdapter.Item item = (AccountListAdapter.Item) lvAccount.getAdapter().getItem(position);
            Bundle bundle = new Bundle();
            Log.d("TAG","ID" + item.text.getId());
            bundle.putString("id", item.text.getId());
            bundle.putString("name", item.text.getAccount_name());
            bundle.putString("pic", item.text.getRelated_people());
            callback.onAccountDialogCallback(bundle);
            dismiss();
        }
    }
}
