package com.msf.project.sales1crm.callback;

import com.msf.project.sales1crm.model.OpportunityModel;

public interface OpportunityDetailCallback {
    void finishDetail(String result, OpportunityModel models);
}
