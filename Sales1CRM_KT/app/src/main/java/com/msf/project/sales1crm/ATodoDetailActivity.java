package com.msf.project.sales1crm;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.msf.project.sales1crm.callback.DashboardCallback;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.model.AbsenceModel;
import com.msf.project.sales1crm.model.ActivityTodoModel;
import com.msf.project.sales1crm.model.DashboardFirstModel;
import com.msf.project.sales1crm.model.DashboardSecondModel;
import com.msf.project.sales1crm.model.DashboardThirdModel;
import com.msf.project.sales1crm.model.TotalOpportunityModel;
import com.msf.project.sales1crm.presenter.DashboardPresenter;
import com.msf.project.sales1crm.utility.ApiParam;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ATodoDetailActivity extends BaseActivity implements DashboardCallback {

    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.tvSubjectTitle)
    CustomTextView tvSubjectTitle;
    @BindView(R.id.ivEdit)
    ImageView ivEdit;
    @BindView(R.id.tvCreateDate)
    CustomTextView tvCreateDate;
    @BindView(R.id.tvAccountName)
    CustomTextView tvAccountName;
    @BindView(R.id.tvSubject)
    CustomTextView tvSubject;
    @BindView(R.id.tvDate)
    CustomTextView tvDate;
    @BindView(R.id.tvAssignTo)
    CustomTextView tvAssignTo;
    @BindView(R.id.tvPriority)
    CustomTextView tvPriority;
    @BindView(R.id.tvTaskStatus)
    CustomTextView tvTaskStatus;
    @BindView(R.id.tvDescription)
    CustomTextView tvDescription;

    private Context context;
    private ActivityTodoModel aTodomodels;
    static ATodoDetailActivity aTodoDetailActivity;
    private DashboardPresenter dashboardPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_atodo_detail);
        ButterKnife.bind(this);
        this.context = this;
        this.dashboardPresenter = new DashboardPresenter(context, this);

        aTodoDetailActivity = this;

        try {
            aTodomodels = getIntent().getExtras().getParcelable("aTodoModels");
        } catch (Exception e) {
        }
        initviewATodo();
    }

    private void initviewATodo(){
        dashboardPresenter.setupFirst(ApiParam.API_005);
        tvSubjectTitle.setText(aTodomodels.getSubject());
        tvSubject.setText(aTodomodels.getSubject());
        tvAccountName.setText(aTodomodels.getAccount_name());
        tvCreateDate.setText("Created at " + aTodomodels.getCreatedAt());
        tvDate.setText(aTodomodels.getDueDate());
        tvAssignTo.setText(aTodomodels.getUser_name());
        tvPriority.setText(aTodomodels.getPriority_name());
        tvTaskStatus.setText(aTodomodels.getTask_status_name());
        tvDescription.setText(aTodomodels.getDescription());

        ivBack.setOnClickListener(click);

    }

    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()){
                case R.id.ivBack:
                    finish();
                    break;

            }
        }
    };

    @Override
    public void finishFirst(String result, DashboardFirstModel model) {
        if(result.equalsIgnoreCase("OK")){
            if(model.getCrm_task_edit_permission().equalsIgnoreCase("1")){
                ivEdit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                            Intent intent = new Intent(context, ATodoAddActivity.class);
                            intent.putExtra("aTodoModels", aTodomodels);
                            intent.putExtra("from_detail_todo", true);
                            startActivity(intent);
                    }
                });
                ivEdit.setVisibility(View.VISIBLE);
            } else {
                ivEdit.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void finishSecond(String result, DashboardSecondModel model) {

    }

    @Override
    public void finishThird(String result, List<DashboardThirdModel> listLead, List<DashboardThirdModel> listLead1, List<DashboardThirdModel> listOppor, List<DashboardThirdModel> listOppor1) {

    }

    @Override
    public void TotOpportunity(String result, TotalOpportunityModel totOporModel) {

    }

    @Override
    public void absence(String result, AbsenceModel model) {

    }
}
