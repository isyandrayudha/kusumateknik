package com.msf.project.sales1crm.presenter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;

import com.msf.project.sales1crm.callback.SyncMasterCallback;
import com.msf.project.sales1crm.service.NetworkConnection;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.PreferenceUtility;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;
import com.msf.project.sales1crm.utility.Sales1CRMUtilsJSON;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by christianmacbook on 24/05/18.
 */

public class SyncMasterPresenter {
    private final String TAG = SyncMasterPresenter.class.getSimpleName();
    private Context context;
    private Messenger messenger;
    private Message message;
    private Bundle bundle;
    private Handler handler;
    private String failureResponse = "";
    private String[] stringResponse = {""};
    private SyncMasterCallback callback;
    private String resultLogin = "NG";
    private String response = "Tolong Periksa Koneksi Anda";

    public SyncMasterPresenter(Context context, SyncMasterCallback listener) {
        this.context = context;
        this.callback = listener;
    }

    private void doNetworkService(String url, String params, String from) {
        Log.d(TAG, "url login " + url);
        Log.d(TAG, "params : " + params);
        Intent networkIntent = new Intent(this.context, NetworkConnection.class);
        this.messenger = new Messenger(this.handler);
        networkIntent.putExtra("messenger", this.messenger);
        networkIntent.putExtra("url", url);
        networkIntent.putExtra("params", params);
        networkIntent.putExtra("from", from);
        this.context.startService(networkIntent);
    }

    public void setupSync(int apiIndex) {
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)) {
            obtainSync(apiIndex);
        }else{
            callback.finishSync(this.resultLogin);
        }
    }

    public void obtainSync(int apiIndex) {
        this.handler = new Handler(this.context.getMainLooper()) {

            @Override
            public void handleMessage(Message msg) {
                parseSyncItem(msg);
            }

        };

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("api_key", PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.API_KEY));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        doNetworkService(ApiParam.urlBuilder(this.context, apiIndex), jsonObject.toString(), "ApiKey");
    }

    private void parseSyncItem(Message message) {
        this.message = message;
        this.bundle = this.message.getData();
        this.stringResponse[0] = this.bundle.getString("network_response");
        this.failureResponse = this.bundle.getString("network_failure");
        Log.d(TAG, "responseString[0] info " + this.stringResponse[0]);
        if (this.failureResponse.equalsIgnoreCase("yes")){
            this.callback.finishSync(this.resultLogin);
        }else{
            try {
                this.resultLogin = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                Sales1CRMUtilsJSON.JSONUtility.getPosition(context, this.stringResponse[0]);
                Sales1CRMUtilsJSON.JSONUtility.getIndustry(context, this.stringResponse[0]);
                Sales1CRMUtilsJSON.JSONUtility.getEventStatus(context, this.stringResponse[0]);
                Sales1CRMUtilsJSON.JSONUtility.getEventPurpose(context,this.stringResponse[0]);
                Sales1CRMUtilsJSON.JSONUtility.getAccountType(context, this.stringResponse[0]);
                Sales1CRMUtilsJSON.JSONUtility.getOpportunityStatus(context, this.stringResponse[0]);
                Sales1CRMUtilsJSON.JSONUtility.getLeadSource(context, this.stringResponse[0]);
                Sales1CRMUtilsJSON.JSONUtility.getLeadStatus(context, this.stringResponse[0]);
                Sales1CRMUtilsJSON.JSONUtility.getNoVisit(context, this.stringResponse[0]);
                Sales1CRMUtilsJSON.JSONUtility.getUseFor(context, this.stringResponse[0]);
                Sales1CRMUtilsJSON.JSONUtility.getNoOrder(context, this.stringResponse[0]);
                Sales1CRMUtilsJSON.JSONUtility.getOpportunityStage(context, this.stringResponse[0]);
                this.callback.finishSync(this.resultLogin);
            } catch (JSONException e) {
                Log.d(TAG, "Exception detail Response " + e.getMessage());
            }
        }
    }

    public void setupMasterUser(int apiIndex) {
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)) {
            obtainMasterUser(apiIndex);
        }else{
            callback.finishUser(this.resultLogin);
        }
    }

    public void obtainMasterUser(int apiIndex) {
        this.handler = new Handler(this.context.getMainLooper()) {

            @Override
            public void handleMessage(Message msg) {
                parseMasterUserItems(msg);
            }

        };

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("api_key", PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.API_KEY));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        doNetworkService(ApiParam.urlBuilder(this.context, apiIndex), jsonObject.toString(), "ApiKey");
    }

    private void parseMasterUserItems(Message message) {
        this.message = message;
        this.bundle = this.message.getData();
        this.stringResponse[0] = this.bundle.getString("network_response");
        this.failureResponse = this.bundle.getString("network_failure");
        Log.d(TAG, "responseString[0] info " + this.stringResponse[0]);
        if (this.failureResponse.equalsIgnoreCase("yes")){
            this.callback.finishUser(this.resultLogin);
        }else{
            try {
                this.resultLogin = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                Sales1CRMUtilsJSON.JSONUtility.getUserList(context, this.stringResponse[0]);
                this.callback.finishUser(this.resultLogin);
            } catch (JSONException e) {
                Log.d(TAG, "Exception detail Response " + e.getMessage());
            }
        }
    }
}
