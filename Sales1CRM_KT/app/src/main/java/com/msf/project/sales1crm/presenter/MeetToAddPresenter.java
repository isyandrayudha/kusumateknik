package com.msf.project.sales1crm.presenter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;

import com.msf.project.sales1crm.callback.MeetToAddCallback;
import com.msf.project.sales1crm.model.ContactModel;
import com.msf.project.sales1crm.service.NetworkConnection;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.PreferenceUtility;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;
import com.msf.project.sales1crm.utility.Sales1CRMUtilsJSON;

import org.json.JSONException;
import org.json.JSONObject;

public class MeetToAddPresenter {
    private final String TAG = MeetToAddPresenter.class.getSimpleName();
    private Context context;
    private Messenger messenger;
    private Message message;
    private Handler handler;
    private Bundle bundle;
    private String failureResponse = "";
    private String[] stringsResponse = {""};
    private MeetToAddCallback callback;
    private String result = "NG";
    private String response = "Tolong Perkisa Koneksi Anda";

    public MeetToAddPresenter(Context context, MeetToAddCallback callback){
        this.context = context;
        this.callback = callback;
    }

    private void doNetworkService(String url, String params, String from) {
        Log.d(TAG, "url " + url);
        Log.d(TAG, "params :" + params);
        Intent intent = new Intent(this.context, NetworkConnection.class);
        this.messenger = new Messenger(this.handler);
        intent.putExtra("messenger", this.messenger);
        intent.putExtra("url", url);
        intent.putExtra("params", params);
        intent.putExtra("from", from);
        this.context.startService(intent);
    }

    public void setupMeetTo(int apiIndex, ContactModel contactModel) {
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)) {
            obtainMeetTo(apiIndex, contactModel);
        } else {
            callback.finishAdd(this.result, this.response);
        }
    }

    public void obtainMeetTo(int apiIndex, ContactModel contactModel) {
        this.handler = new Handler(this.context.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                parseMeetTo(msg);
            }
        };

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("api_key", PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.API_KEY));
            jsonObject.put("title", contactModel.getTitle());
            jsonObject.put("account_id", contactModel.getAccount_id());
            jsonObject.put("first_name", contactModel.getFirst_name());
            jsonObject.put("last_name", contactModel.getLast_name());
            jsonObject.put("position_id", contactModel.getPosition());
            jsonObject.put("email", contactModel.getEmail());
            jsonObject.put("phone", contactModel.getPhone());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        doNetworkService(ApiParam.urlBuilder(this.context, apiIndex), jsonObject.toString(), "contactAdd");
    }

    private void parseMeetTo(Message message) {
        this.message = message;
        this.bundle = this.message.getData();
        this.stringsResponse[0] = this.bundle.getString("network_response");
        this.failureResponse = this.bundle.getString("network_failure");
        Log.d(TAG, "responseString[0] add " + this.stringsResponse[0]);
        if (this.failureResponse.equalsIgnoreCase("ok")) {
            this.callback.finishAdd(this.result, this.response);
        } else {
            try {
                this.result = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringsResponse[0]);
                Log.e("Result ", this.result);
                if (this.result.equalsIgnoreCase("OK")) {
                    this.response = Sales1CRMUtilsJSON.JSONUtility.getContactFromServer(this.stringsResponse[0]);
                } else {
                    this.response = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringsResponse[0]);
                }
                this.callback.finishAdd(this.result, this.response);
            } catch (JSONException e) {
                Log.d(TAG, "Exception detail response " + e.getMessage());
            }
        }
    }

    public void setupEdit(int apiIndex, ContactModel contactModel){
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)) {
            obtainEdit(apiIndex, contactModel);
        } else {
            callback.finishEdit(this.result, this.response);
        }
    }

    public void obtainEdit(int apiIndex, ContactModel contactModel){
        this.handler = new Handler(this.context.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                parseEdit(msg);
            }
        };

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("api_key", PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.API_KEY));
            jsonObject.put("contact_id", contactModel.getId());
            jsonObject.put("title", contactModel.getTitle());
            jsonObject.put("account_id", contactModel.getAccount_id());
            jsonObject.put("first_name", contactModel.getFirst_name());
            jsonObject.put("last_name", contactModel.getLast_name());
            jsonObject.put("position_id", contactModel.getPosition());
            jsonObject.put("email", contactModel.getEmail());
            jsonObject.put("phone", contactModel.getPhone());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        doNetworkService(ApiParam.urlBuilder(this.context, apiIndex), jsonObject.toString(), "contactEdit");
    }

    private void parseEdit(Message message){
        this.message = message;
        this.bundle = this.message.getData();
        this.stringsResponse[0] = this.bundle.getString("network_response");
        this.failureResponse = this.bundle.getString("network_failure");
        Log.d(TAG, "responseString[0] edit " + this.stringsResponse[0]);
        if (this.failureResponse.equalsIgnoreCase("yes")) {
            this.callback.finishEdit(this.result, this.response);
        } else {
            try {
                    this.response = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringsResponse[0]);
                this.callback.finishEdit(this.result, this.response);
            } catch (JSONException e) {
                Log.d(TAG, "Exception detail response " + e.getMessage());
            }
        }
    }

}
