package com.msf.project.sales1crm.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.msf.project.sales1crm.AEventDetailActivity;
import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.database.MeeToSelectDao;
import com.msf.project.sales1crm.model.ActivityEventModel;

import java.util.List;

public class ActivityEventListAdapter extends RecyclerView.Adapter<ActivityEventListAdapter.ActivityEventListHolder> {

    private Context context;
    private Bitmap icon;
    private String url;
    private List<ActivityEventModel> activityEventModelList;

    public ActivityEventListAdapter(Context context, List<ActivityEventModel> objects) {
        this.context = context;
        this.activityEventModelList = objects;
    }

    public static abstract class Row {
    }

    // This is for making Section, ex: A B C D etc
    public static final class Section extends Row {
        public final String text;

        public Section(String text) {
            this.text = text;
        }
    }

    public static final class Item extends Row {
        public final ActivityEventModel text;

        public Item(ActivityEventModel text) {
            this.text = text;
        }

        public ActivityEventModel getItem() {
            return text;
        }
    }

    private List<Row> rows;

    public void setRows(List<Row> rows) {
        this.rows = rows;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return rows.size();
    }


    @Override
    public ActivityEventListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_activity_event, null);
        return new ActivityEventListHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ActivityEventListHolder holder, int position) {
        final Item item = (Item) rows.get(position);
        holder.tvEventName.setText(item.text.getName());
        holder.tvAccountName.setText(item.text.getAccount_name());
        holder.tvAssignTo.setText(item.text.getUser_name());
        holder.tvDate.setText(item.text.getDate());
        holder.tvDescription.setText(item.text.getDescription());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    Intent intent = new Intent(context, AEventDetailActivity.class);
                    intent.putExtra("aEventModels", item.getItem());
                MeeToSelectDao dao = new MeeToSelectDao(context);
                dao.deleteAllRecord();
                    context.startActivity(intent);
            }
        });

    }


    public class ActivityEventListHolder extends RecyclerView.ViewHolder {
        CustomTextView tvEventName;
        CustomTextView tvAccountName;
        CustomTextView tvAssignTo;
        CustomTextView tvDate;
        CustomTextView tvDescription;
        public ActivityEventListHolder(View view) {
            super(view);
            tvEventName = (CustomTextView) view.findViewById(R.id.tvEventName);
            tvAccountName = (CustomTextView) view.findViewById(R.id.tvAccountName);
            tvAssignTo = (CustomTextView) view.findViewById(R.id.tvAssignTo);
            tvDate = (CustomTextView) view.findViewById(R.id.tvDate);
            tvDescription = (CustomTextView) view.findViewById(R.id.tvDescription);
        }
    }
}
