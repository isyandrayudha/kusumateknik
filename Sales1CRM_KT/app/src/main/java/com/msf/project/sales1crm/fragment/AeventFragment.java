package com.msf.project.sales1crm.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.msf.project.sales1crm.ErrorAPIKeyActivity;
import com.msf.project.sales1crm.MenuActivity;
import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.adapter.AEventListAdapter;
import com.msf.project.sales1crm.callback.ActivityListCallback;
import com.msf.project.sales1crm.customview.CustomEditText;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.imageloader.ImageLoader;
import com.msf.project.sales1crm.model.ActivityCallsModel;
import com.msf.project.sales1crm.model.ActivityEventModel;
import com.msf.project.sales1crm.model.ActivityTodoModel;
import com.msf.project.sales1crm.presenter.ActivityPresenter;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AeventFragment extends Fragment implements ActivityListCallback {

    @BindView(R.id.tvEventName)
    CustomTextView tvEventName;
    @BindView(R.id.rcEvent)
    RecyclerView rcEvent;
    @BindView(R.id.shimmerLayout)
    ShimmerFrameLayout shimmerLayout;
    @BindView(R.id.llshim)
    LinearLayout llshim;
    @BindView(R.id.etSearch)
    CustomEditText etSearch;
    @BindView(R.id.ivClear)
    ImageView ivClear;
    @BindView(R.id.llNotFound)
    LinearLayout llNotFound;
    @BindView(R.id.tvRefresh)
    CustomTextView tvRefresh;
    @BindView(R.id.rlNoConnection)
    RelativeLayout rlNoConnection;
    @BindView(R.id.swipeRefresh)
    SwipeRefreshLayout swipeRefresh;
    private View view;
    private Context context;
    private int prevSize = 0, sort_id = 0;
    private boolean isLoadMore = true;
    private boolean isMaxSize = true;
    private boolean pause = false;
    private boolean isScrolling = false;
    private int currentItems, totalItems, scrollOutItems;
    private AEventListAdapter activityEventListAdapter;
    private ActivityPresenter presenter;
    private List<ActivityEventModel> activityEventModelList = new ArrayList<>();

    public static AeventFragment newInstance() {
        AeventFragment fragment = new AeventFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_a_event, container, false);
        ButterKnife.bind(this, this.view);
        return this.view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.context = getActivity();
        this.presenter = new ActivityPresenter(context, this);

        initData();
    }

    private void initData() {
        Sales1CRMUtils.Utils.imageLoader = new ImageLoader(context);
        llNotFound.setVisibility(View.GONE);
        etSearch.clearFocus();
        setTextWatcherForSearch();
        ivClear.setOnClickListener(click);
        tvRefresh.setOnClickListener(click);

        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                llshim.setVisibility(View.VISIBLE);
                shimmerLayout.setVisibility(View.VISIBLE);
                shimmerLayout.startShimmer();
                if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(context)) {

//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
                    presenter.setupListEvent(ApiParam.API_107, etSearch.getText().toString());
//                }
//            }, 1000);


                } else {
                    swipeRefresh.setVisibility(View.GONE);
                    rlNoConnection.setVisibility(View.VISIBLE);
                }
            }
        });
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(context)) {

//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
            presenter.setupListEvent(ApiParam.API_107, etSearch.getText().toString());
//                }
//            }, 1000);


        } else {
            rlNoConnection.setVisibility(View.VISIBLE);
        }

    }

    private void setTextWatcherForSearch() {
        etSearch.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    if (etSearch.getText().length() == 0) {
                        ivClear.setVisibility(View.VISIBLE);
                    }
                }
            }
        });
        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_GO) {
                    initData();
                }

                return true;
            }
        });
    }

    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.ivClear:
                    etSearch.setText("");
                    etSearch.clearFocus();
                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);
                    initData();
                    break;
                case R.id.tvRefresh:
                    llshim.setVisibility(View.VISIBLE);
                    rlNoConnection.setVisibility(View.GONE);
                    getActivity();
                    break;
                default:
                    break;
            }
        }
    };

    private void setEventList() {
        llshim.setVisibility(View.VISIBLE);
        shimmerLayout.startShimmer();
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rcEvent.setLayoutManager(layoutManager);
        rcEvent.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {

                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                currentItems = layoutManager.getChildCount();
                totalItems = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstVisibleItemPosition();

                if (isScrolling && (currentItems + scrollOutItems == totalItems)) {
                    isScrolling = false;
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            presenter.setupMoreListEvent(ApiParam.API_107, etSearch.getText().toString());
                            activityEventListAdapter.notifyDataSetChanged();
                        }
                    }, 5000);

                }
            }
        });

//        activityEventListAdapter = new ActivityEventListAdapter(context, this.activityEventModelList);

        List<AEventListAdapter.Row> rows = new ArrayList<>();
        for (ActivityEventModel aEventModel : activityEventModelList) {
            rows.add(new AEventListAdapter.Item(aEventModel));

        }
        activityEventListAdapter.setRows(rows);
        rcEvent.setAdapter(activityEventListAdapter);
        llshim.setVisibility(View.INVISIBLE);
        shimmerLayout.stopShimmer();
        shimmerLayout.setVisibility(View.INVISIBLE);
    }

    private void setEventMoreList() {
        List<AEventListAdapter.Row> rows = new ArrayList<>();
        for (ActivityEventModel activityEventModel : activityEventModelList) {
            rows.add(new AEventListAdapter.Item(activityEventModel));
        }
        activityEventListAdapter.setRows(rows);
        activityEventListAdapter.notifyDataSetChanged();
    }

    @Override
    public void finishActivityEventList(String result, List<ActivityEventModel> activityEventModel) {
       swipeRefresh.setRefreshing(false);
        if (result.equalsIgnoreCase("OK")) {
            prevSize = activityEventModelList.size();
            this.activityEventModelList = activityEventModel;

            Log.e("EVENT LIST", activityEventModel.toString());

            isMaxSize = false;
            isLoadMore = false;

            activityEventListAdapter = new AEventListAdapter(context, this.activityEventModelList);
            setEventList();
            if (activityEventModel.size() > 0) {
                llNotFound.setVisibility(View.GONE);
            } else {
                llNotFound.setVisibility(View.VISIBLE);
            }
        } else if (result.equalsIgnoreCase("FAILED_KEY")) {
            startActivity(new Intent(context, ErrorAPIKeyActivity.class));
            MenuActivity.getInstance().finish();
        } else {
            rlNoConnection.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void finishActivityEventMoreList(String result, List<ActivityEventModel> activityEventModel) {
        if (result.equals("OK")) {
            for (int i = 0; i < activityEventModel.size(); i++) {
                this.activityEventModelList.add(activityEventModel.get(i));
            }

            if (this.prevSize == this.activityEventModelList.size())
                isMaxSize = true;
            this.prevSize = this.activityEventModelList.size();
            setEventMoreList();

        }
        this.isLoadMore = false;
    }

    @Override
    public void finishActivityTodoList(String result, List<ActivityTodoModel> activityTodoModel) {

    }

    @Override
    public void finishActivityTodoMoreList(String result, List<ActivityTodoModel> activityTodoModel) {

    }

    @Override
    public void finishActivityCallList(String result, List<ActivityCallsModel> activityCallsModel) {

    }

    @Override
    public void finishActivityCallMoreList(String result, List<ActivityCallsModel> activityCallsModel) {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
