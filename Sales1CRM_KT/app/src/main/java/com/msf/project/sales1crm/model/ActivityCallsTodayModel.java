package com.msf.project.sales1crm.model;

public class ActivityCallsTodayModel{
	private String date;
	private String durationLabel;
	private String subject;
	private String createdAt;
	private String callPurposeId;
	private String deletedAt;
	private String duration;
	private String accountId;
	private String callTypeId;
	private String updatedAt;
	private String userId;
	private String id;
	private String detail;
	private String status;

	public void setDate(String date){
		this.date = date;
	}

	public String getDate(){
		return date;
	}

	public void setDurationLabel(String durationLabel){
		this.durationLabel = durationLabel;
	}

	public String getDurationLabel(){
		return durationLabel;
	}

	public void setSubject(String subject){
		this.subject = subject;
	}

	public String getSubject(){
		return subject;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setCallPurposeId(String callPurposeId){
		this.callPurposeId = callPurposeId;
	}

	public String getCallPurposeId(){
		return callPurposeId;
	}

	public void setDeletedAt(String deletedAt){
		this.deletedAt = deletedAt;
	}

	public String getDeletedAt(){
		return deletedAt;
	}

	public void setDuration(String duration){
		this.duration = duration;
	}

	public String getDuration(){
		return duration;
	}

	public void setAccountId(String accountId){
		this.accountId = accountId;
	}

	public String getAccountId(){
		return accountId;
	}

	public void setCallTypeId(String callTypeId){
		this.callTypeId = callTypeId;
	}

	public String getCallTypeId(){
		return callTypeId;
	}

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setDetail(String detail){
		this.detail = detail;
	}

	public String getDetail(){
		return detail;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}
}
