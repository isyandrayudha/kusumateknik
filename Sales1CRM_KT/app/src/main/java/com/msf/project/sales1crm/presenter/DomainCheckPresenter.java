package com.msf.project.sales1crm.presenter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;

import com.msf.project.sales1crm.callback.DomainCheckCallback;
import com.msf.project.sales1crm.model.DomainModel;
import com.msf.project.sales1crm.service.NetworkConnection;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;
import com.msf.project.sales1crm.utility.Sales1CRMUtilsJSON;

import org.json.JSONException;
import org.json.JSONObject;

public class DomainCheckPresenter {
    private final String TAG = DomainCheckPresenter.class.getSimpleName();
    private Context context;
    private Messenger messenger;
    private Message message;
    private Handler handler;
    private Bundle bundle;
    private String failureResponse = "";
    private String[] stringsResponse = {""};
    private DomainCheckCallback domainCheckCallback;
    private String resultDomainCheck = "NG";
    private String response = "Tolong Periksa Koneksi Anda";

    public DomainCheckPresenter(Context context, DomainCheckCallback listener) {
        this.context = context;
        this.domainCheckCallback = listener;
    }

    private void doNetworkService(String url, String params, String from) {
        Log.d(TAG, "url domain check " + url);
        Log.d(TAG, "params :" + params);
        Intent intent = new Intent(this.context, NetworkConnection.class);
        this.messenger = new Messenger(this.handler);
        intent.putExtra("messenger", this.messenger);
        intent.putExtra("url", url);
        intent.putExtra("params", params);
        intent.putExtra("from", from);
        this.context.startService(intent);
    }

    public void setupDomainCheckItems(int apiIndex, DomainModel domainModel) {
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)) {
            obtainDomainCheckItems(apiIndex, domainModel);
        } else {
            domainCheckCallback.finishedDomain(this.resultDomainCheck, this.response);
        }
    }

    public void obtainDomainCheckItems(int apiIndex, DomainModel domainModel) {
        this.handler = new Handler(this.context.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                parseDomainCheckItemsResponse(msg);
            }
        };

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("keyword", domainModel.getKeyword());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        doNetworkService(ApiParam.urlBuilderGlobal(this.context, apiIndex), jsonObject.toString(), "keyword");
    }

    private void parseDomainCheckItemsResponse(Message message) {
        this.message = message;
        this.bundle = this.message.getData();
        this.stringsResponse[0] = this.bundle.getString("network_response");
        this.failureResponse = this.bundle.getString("network_failure");
        Log.d(TAG, "responseString[0] domain info " + this.stringsResponse[0]);
        if (this.failureResponse.equalsIgnoreCase("ok")) {
            this.domainCheckCallback.finishedDomain(this.resultDomainCheck, this.response);
        } else {
            try {
                this.resultDomainCheck = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringsResponse[0]);
                Log.e("Result ", this.resultDomainCheck);
                if (this.resultDomainCheck.equalsIgnoreCase("OK")) {
                    this.response = Sales1CRMUtilsJSON.JSONUtility.getDomainFromServer(this.stringsResponse[0]);
                } else {
                    this.response = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringsResponse[0]);
                }
                this.domainCheckCallback.finishedDomain(this.resultDomainCheck, this.response);
            } catch (JSONException e) {
                Log.d(TAG, "Exception detail response " + e.getMessage());
            }
        }
    }
}
