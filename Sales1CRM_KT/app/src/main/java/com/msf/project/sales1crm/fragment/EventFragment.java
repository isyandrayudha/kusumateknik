package com.msf.project.sales1crm.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.applandeo.materialcalendarview.CalendarView;
import com.applandeo.materialcalendarview.EventDay;
import com.applandeo.materialcalendarview.exceptions.OutOfDateRangeException;
import com.applandeo.materialcalendarview.listeners.OnDayClickListener;
import com.github.clans.fab.FloatingActionButton;
import com.msf.project.sales1crm.ErrorAPIKeyActivity;
import com.msf.project.sales1crm.EventAddActivity;
import com.msf.project.sales1crm.EventDetailActivity;
import com.msf.project.sales1crm.MenuActivity;
import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.adapter.EventListAdapter;
import com.msf.project.sales1crm.callback.DashboardCallback;
import com.msf.project.sales1crm.callback.EventListCallback;
import com.msf.project.sales1crm.customview.CustomEditText;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.database.MeeToSelectDao;
import com.msf.project.sales1crm.database.MeetToDao;
import com.msf.project.sales1crm.database.ProductDao;
import com.msf.project.sales1crm.imageloader.ImageLoader;
import com.msf.project.sales1crm.model.AbsenceModel;
import com.msf.project.sales1crm.model.DashboardFirstModel;
import com.msf.project.sales1crm.model.DashboardSecondModel;
import com.msf.project.sales1crm.model.DashboardThirdModel;
import com.msf.project.sales1crm.model.EventModel;
import com.msf.project.sales1crm.model.TotalOpportunityModel;
import com.msf.project.sales1crm.presenter.DashboardPresenter;
import com.msf.project.sales1crm.presenter.EventListPresenter;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.BindView;

public class EventFragment extends Fragment implements EventListCallback, DashboardCallback {

    @BindView(R.id.calendarView)
    CalendarView calendarView;

    @BindView(R.id.rlNoConnection)
    RelativeLayout rlNoConnection;

    @BindView(R.id.tvRefresh)
    CustomTextView tvRefresh;

    @BindView(R.id.etSearch)
    CustomEditText etSearch;

    @BindView(R.id.ivClear)
    ImageView ivClear;

    @BindView(R.id.lvEvent)
    ListView lvEvent;

    @BindView(R.id.faEventAdd)
    FloatingActionButton faEventAdd;

    @BindView(R.id.llNotFound)
    LinearLayout llNotFound;

    @BindView(R.id.rlProgress)
    RelativeLayout rlProgress;

    private View view;
    private Context context;
    private EventListAdapter adapter;
    private EventListPresenter presenter;
    private List<EventModel> list = new ArrayList<>();
    private String date_selected = "";
    private Calendar calendar;

    private int prevSize = 0;
    private boolean isLoadMore = true;
    private boolean isMaxSize = true;
    private boolean pause = false;
    private DashboardPresenter dashboardPresenter;

    public static EventFragment newInstance() {
        EventFragment fragment = new EventFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_event, container, false);
        this.context = getActivity();
        return this.view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);
        this.context = getActivity();
        this.dashboardPresenter = new DashboardPresenter(context, this);
        this.presenter = new EventListPresenter(context, this);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Event");

        initData();
    }

    private void initData() {
        Sales1CRMUtils.Utils.imageLoader = new ImageLoader(context);


        calendar = Calendar.getInstance();

        try {
            calendarView.setDate(calendar);
        } catch (OutOfDateRangeException e) {
            e.printStackTrace();
        }

        Date date = calendar.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        date_selected = sdf.format(date);

        etSearch.clearFocus();
        lvEvent.setOnScrollListener(new EventFragment.ListLazyLoad());

        //Set Seach Function
        setTextWatcherForSearch();
        getDataFromAPI(0, date_selected);

        calendarView.setOnDayClickListener(new OnDayClickListener() {
            @Override
            public void onDayClick(EventDay eventDay) {
                calendarView.getSelectedDates();
                Date date = eventDay.getCalendar().getTime();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Log.d("TAG", "Date : " + sdf.format(date));

                date_selected = sdf.format(date);

                getDataFromAPI(0, date_selected);
            }
        });

        ivClear.setOnClickListener(click);
        tvRefresh.setOnClickListener(click);
        faEventAdd.setOnClickListener(click);
    }

    private void setTextWatcherForSearch() {
        etSearch.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    if (etSearch.getText().length() == 0) {
                        ivClear.setVisibility(View.VISIBLE);
                    }
                } else {
                    if (etSearch.getText().length() == 0) {
                        ivClear.setVisibility(View.GONE);
                    } else {
                        ivClear.setVisibility(View.VISIBLE);
                    }

                }
            }
        });

        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    getDataFromAPI(0, date_selected);
                }

                return true;
            }
        });
    }

    private void getDataFromAPI(int prev, String date) {
        isMaxSize = true;
        isLoadMore = true;

        llNotFound.setVisibility(View.GONE);

        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(context)) {
            dashboardPresenter.setupFirst(ApiParam.API_005);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    presenter.setupList(ApiParam.API_060, prev, etSearch.getText().toString(), "", date);
                }
            }, 1000);
        } else {
            rlNoConnection.setVisibility(View.VISIBLE);
        }
    }


    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.ivClear:
                    etSearch.setText("");
                    etSearch.clearFocus();
                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);
                    getDataFromAPI(0, date_selected);
                    break;
                case R.id.tvRefresh:
                    rlNoConnection.setVisibility(View.GONE);

                    getDataFromAPI(0, date_selected);
                    break;
                default:
                    break;
            }
        }
    };

    private void setList() {
        adapter = new EventListAdapter(context, this.list);

        List<EventListAdapter.Row> rows = new ArrayList<EventListAdapter.Row>();
        for (EventModel eventModel : list) {
            //Read List by Row to insert into List Adapter
            rows.add(new EventListAdapter.Item(eventModel));
        }
        //Set Row Adapter
        adapter.setRows(rows);
        lvEvent.setAdapter(adapter);
        lvEvent.setSelector(R.drawable.transparent_selector);
        lvEvent.setOverScrollMode(View.OVER_SCROLL_NEVER);
        lvEvent.setOnTouchListener(new View.OnTouchListener() {
            // Setting on Touch Listener for handling the touch inside
            // ScrollView
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // Disallow the touch request for parent scroll on touch of
                // child view
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });
        lvEvent.setOnItemClickListener(new EventFragment.AdapterOnItemClick());
        lvEvent.deferNotifyDataSetChanged();
    }

    private void setMoreList() {
        List<EventListAdapter.Row> rows = new ArrayList<EventListAdapter.Row>();

        for (EventModel country : list) {
            // Add the country to the list
            rows.add(new EventListAdapter.Item(country));
        }
        adapter.setRows(rows);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void finishEventList(String result, List<EventModel> list) {
        rlProgress.setVisibility(View.GONE);
        if (result.equalsIgnoreCase("OK")) {
            prevSize = list.size();
            this.list = list;

            isMaxSize = false;
            isLoadMore = false;

            setList();
            if (list.size() > 0) {
                llNotFound.setVisibility(View.GONE);
            } else {
                llNotFound.setVisibility(View.VISIBLE);
            }
        } else if (result.equalsIgnoreCase("FAILED_KEY")) {
            startActivity(new Intent(context, ErrorAPIKeyActivity.class));
            MenuActivity.getInstance().finish();
        } else {
            rlNoConnection.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void finishEventMoreList(String result, List<EventModel> list) {
        if (result.equals("OK")) {
            for (int i = 0; i < list.size(); i++) {
                this.list.add(list.get(i));
            }

            if (this.prevSize == this.list.size())
                isMaxSize = true;
            this.prevSize = this.list.size();
            setMoreList();
        }
        this.isLoadMore = false;
    }

    @Override
    public void finishFirst(String result, DashboardFirstModel model) {
        if (model.getCrm_event_create_permission().equalsIgnoreCase("1")) {
            faEventAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, EventAddActivity.class);
                    MeeToSelectDao dao = new MeeToSelectDao(context);
                    dao.deleteAllRecord();
                    intent.putExtra("date", date_selected);
                    startActivity(intent);
                }
            });
            faEventAdd.setColorNormal(R.color.BlueCRM);
            faEventAdd.setColorPressed(R.color.colorPrimaryDark);
            faEventAdd.setColorPressedResId(R.color.colorPrimaryDark);
            faEventAdd.setColorNormalResId(R.color.BlueCRM);
        } else {
            faEventAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
                }
            });
            faEventAdd.setColorNormal(R.color.BlueCRM_light);
            faEventAdd.setColorPressed(R.color.colorPrimaryDark_light);
            faEventAdd.setColorPressedResId(R.color.colorPrimaryDark_light);
            faEventAdd.setColorNormalResId(R.color.BlueCRM_light);
        }
    }

    @Override
    public void finishSecond(String result, DashboardSecondModel model) {

    }

    @Override
    public void finishThird(String result, List<DashboardThirdModel> listLead, List<DashboardThirdModel> listLead1, List<DashboardThirdModel> listOppor, List<DashboardThirdModel> listOppor1) {

    }

    @Override
    public void TotOpportunity(String result, TotalOpportunityModel totOporModel) {

    }

    @Override
    public void absence(String result, AbsenceModel model) {

    }

    private class ListLazyLoad implements AbsListView.OnScrollListener {

        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {

        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            if (firstVisibleItem + visibleItemCount == totalItemCount && totalItemCount != 0) {
                Log.d("TAG", "isLoadMore : " + isLoadMore);
                if (isLoadMore == false && isMaxSize == false) {
                    isLoadMore = true;
                    prevSize = list.size();

                    presenter.setupMoreList(ApiParam.API_060, prevSize, etSearch.getText().toString(), "", date_selected);
                }
            }
        }
    }

    private class AdapterOnItemClick implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            // TODO Auto-generated method stub
            EventListAdapter.Item item = (EventListAdapter.Item) lvEvent.getAdapter().getItem(position);
            switch (view.getId()) {
                default:
                    Intent intent = new Intent(context, EventDetailActivity.class);
                    intent.putExtra("eventModel", item.getItem());
                    ProductDao productDao = new ProductDao(context);
                    productDao.deleteAllRecord();

                    MeeToSelectDao meeToSelectDao = new MeeToSelectDao(context);
                    meeToSelectDao.deleteAllRecord();

                    MeetToDao meetToDao = new MeetToDao(context);
                    meetToDao.deleteAllRecord();
                    startActivity(intent);
                    break;
            }
        }
    }

    @Override
    public void onPause() {
//        pause = true;
        super.onPause();
    }

    @Override
    public void onResume() {
//        if (pause){
//            getDataFromAPI(0, date_selected);
//        }
        super.onResume();
    }
}
