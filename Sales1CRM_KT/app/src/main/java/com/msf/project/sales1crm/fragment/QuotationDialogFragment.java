package com.msf.project.sales1crm.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.adapter.QoutationListAdapter;
import com.msf.project.sales1crm.callback.QuotationCallback;
import com.msf.project.sales1crm.callback.QuotationDialogCallback;
import com.msf.project.sales1crm.customview.CustomEditText;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.model.QuotationModel;
import com.msf.project.sales1crm.presenter.QuotationPresenter;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

@SuppressLint("ValidFragment")
public class QuotationDialogFragment extends BaseDialogFragment implements QuotationCallback {

    @BindView(R.id.etSearch)
    CustomEditText etSearch;
    @BindView(R.id.ivClear)
    ImageView ivClear;
    @BindView(R.id.tvRefresh)
    CustomTextView tvRefresh;
    @BindView(R.id.rlNoConnection)
    RelativeLayout rlNoConnection;
    @BindView(R.id.llNotFound)
    LinearLayout llNotFound;
    @BindView(R.id.rlProgress)
    RelativeLayout rlProgress;
    @BindView(R.id.lvQuotation)
    ListView lvQuotation;
    @BindView(R.id.lvSwipe)
    SwipeRefreshLayout lvSwipe;
    @BindView(R.id.rlQoutationList)
    RelativeLayout rlQoutationList;
    private View view;
    private Context context;
    private QoutationListAdapter adapter;
    private QuotationDialogCallback callback;
    private QuotationPresenter presenter;
    private List<QuotationModel> list = new ArrayList<>();
    private String opportunityId;


    @SuppressLint("ValidFragment")
    public QuotationDialogFragment(Context context, QuotationDialogCallback callback) {
        this.context = context;
        this.callback = callback;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, 0);
        opportunityId = getArguments().getString("opportunity_id");
        Log.d("TAG","opportunityId " + opportunityId);
    }

    @Override
    public void onStart() {
        super.onStart();
        Window window = getDialog().getWindow();
        WindowManager.LayoutParams windowParams = window.getAttributes();
        windowParams.width = (int) (this.context.getResources().getDisplayMetrics().widthPixels * 0.99);
        windowParams.height = (int) (this.context.getResources().getDisplayMetrics().heightPixels * 0.97);
        window.setAttributes(windowParams);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onCreateView(inflater, container, savedInstanceState);
        this.view = inflater.inflate(R.layout.fragment_quotation_dialog, container, false);

        return this.view;
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.presenter = new QuotationPresenter(context, this);
        ButterKnife.bind(this, view);
        getDataApi();
    }

    private void getDataApi() {
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(context)) {
            presenter.setupList(ApiParam.API_130, opportunityId);
        } else {
            lvSwipe.setVisibility(View.GONE);
            rlNoConnection.setVisibility(View.VISIBLE);
        }
    }

    private void setList() {
        adapter = new QoutationListAdapter(context, this.list);
        List<QoutationListAdapter.Row> rows = new ArrayList<>();
        for (QuotationModel qm : list) {
            rows.add(new QoutationListAdapter.Item(qm));
        }
        adapter.setRows(rows);
        lvQuotation.setAdapter(adapter);
        lvQuotation.setOnItemClickListener(new QuotationDialogFragment.AdapterOnItemClick());
        lvQuotation.deferNotifyDataSetChanged();
    }

    @Override
    public void finishQuotationList(String result, List<QuotationModel> list) {
        lvSwipe.setRefreshing(false);
        rlProgress.setVisibility(View.GONE);
        if (result.equalsIgnoreCase("OK")) {
            this.list = list;
            setList();
            if (list.size() > 0) {
                llNotFound.setVisibility(View.GONE);
                llNotFound.setVisibility(View.GONE);
            } else {
                llNotFound.setVisibility(View.VISIBLE);
            }
        } else {
            rlNoConnection.setVisibility(View.VISIBLE);
        }
    }

    private class AdapterOnItemClick implements AdapterView.OnItemClickListener{

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final QoutationListAdapter.Item item = (QoutationListAdapter.Item) lvQuotation.getAdapter().getItem(position);
            Bundle bundle = new Bundle();
            bundle.putString("id",item.text.getQuotationId());
            bundle.putString("references", item.text.getReferencesNumber());
            bundle.putString("amount", item.text.getAmount());
            callback.onQuotationDialogCallback(bundle);
            dismiss();
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
