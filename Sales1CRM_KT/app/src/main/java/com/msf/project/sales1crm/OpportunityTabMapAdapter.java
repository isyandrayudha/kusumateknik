package com.msf.project.sales1crm;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.msf.project.sales1crm.fragment.OpportunityActiveMapStreetFragment;
import com.msf.project.sales1crm.fragment.OpportunityCloseMapStreetFragment;

public class OpportunityTabMapAdapter extends FragmentStatePagerAdapter


{
    int mNumOfTabs;
    Bundle bundle;

    public OpportunityTabMapAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                OpportunityActiveMapStreetFragment tab1 = new OpportunityActiveMapStreetFragment();
                return tab1;
            case 1:
                OpportunityCloseMapStreetFragment tab2 = new OpportunityCloseMapStreetFragment();
                return tab2;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
