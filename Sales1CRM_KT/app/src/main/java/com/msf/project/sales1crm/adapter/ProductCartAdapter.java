package com.msf.project.sales1crm.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.model.ProductModel;

import java.util.List;

public class ProductCartAdapter extends BaseAdapter {
    private Context context;
    private LayoutInflater inflater;
    private List<ProductModel> arrModel;

    public ProductCartAdapter(Context context,
                                List<ProductModel> arrModel) {
        this.context = context;
        this.arrModel = arrModel;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        if(arrModel.size()<=0){
            return 1;
        }
        return arrModel.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder holder;

        if (convertView == null) {
            inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            /****** Inflate tabitem.xml file for each row ( Defined below ) *******/
            view = inflater.inflate(R.layout.row_productcart, null);

            /****** View Holder Object to contain tabitem.xml file elements ******/

            holder = new ViewHolder();
            holder.tvProductName = (TextView) view
                    .findViewById(R.id.tvProductName);
            holder.tvQty = (TextView) view
                    .findViewById(R.id.tvQty);
            holder.rlContainer = (RelativeLayout) view
                    .findViewById(R.id.rlContainer);

            /************ Set holder with LayoutInflater ************/
            view.setTag(holder);
        } else
            holder = (ViewHolder) view.getTag();

        if (arrModel.size() <= 0) {
            holder.rlContainer.setBackgroundColor(context.getResources().getColor(R.color.white));
            holder.tvProductName.setText("Tidak Ada Produk");
            holder.tvQty.setText("");

        } else {
            holder.rlContainer.setBackground(context.getResources().getDrawable(R.drawable.container_productcart));
            holder.tvProductName.setText(arrModel.get(position).getName());
            holder.tvQty.setText(arrModel.get(position).getQty() + " Pcs");
        }
        return view;
    }

    /********* Create a holder Class to contain inflated xml file elements *********/
    public static class ViewHolder {

        public TextView tvProductName, tvQty;
        public RelativeLayout rlContainer;

    }
}
