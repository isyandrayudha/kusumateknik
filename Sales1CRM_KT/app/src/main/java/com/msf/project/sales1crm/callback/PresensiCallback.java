package com.msf.project.sales1crm.callback;

public interface PresensiCallback {
    void checkin(String result, String response);
    void checkout(String result, String response);
}
