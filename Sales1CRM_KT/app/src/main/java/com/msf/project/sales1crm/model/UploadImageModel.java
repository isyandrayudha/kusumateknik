package com.msf.project.sales1crm.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

public class UploadImageModel implements Comparable, Parcelable {
    private String photo;
    private String name;

    public UploadImageModel(){
        setPhoto("");
        setName("");
    }

    public UploadImageModel(Parcel in){
        readFromParcel(in);
    }

    public void readFromParcel(Parcel in) {
        photo = in.readString();
        name = in.readString();
    }

    public static final Creator<UploadImageModel> CREATOR = new Creator<UploadImageModel>() {
        @Override
        public UploadImageModel createFromParcel(Parcel in) {
            return new UploadImageModel(in);
        }

        @Override
        public UploadImageModel[] newArray(int size) {
            return new UploadImageModel[size];
        }
    };

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(photo);
        parcel.writeString(name);
    }

    @Override
    public int compareTo(@NonNull Object o) {
        return 0;
    }
}
