package com.msf.project.sales1crm.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.database.MeeToSelectDao;
import com.msf.project.sales1crm.model.ContactModel;

import java.util.List;

import butterknife.BindView;


public class MeetoAdapter extends RecyclerView.Adapter<MeetoAdapter.ViewHolder> {



    private Context context;
    private LayoutInflater layoutInflater;
    private List<ContactModel> listData;

    public MeetoAdapter(Context context, List<ContactModel> objects) {
        this.context = context;
        this.listData = objects;
    }

    public static abstract class Row {
    }

    public static final class Section extends Row {
        public final String text;

        public Section(String text) {
            this.text = text;
        }
    }

    public static final class Item extends Row {
        public final ContactModel text;

        public Item(ContactModel text) {
            this.text = text;
        }

        public ContactModel getItem() {
            return text;
        }
    }

    private List<Row> rows;

    public void setRows(List<Row> rows) {
        this.rows = rows;
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return rows.size();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_meetto, null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Item item = (Item) rows.get(position);
        holder.tvContactName.setText(item.text.getFullname());
        holder.llmeetTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MeeToSelectDao meeToSelectDao = new MeeToSelectDao(context);
                ContactModel contactModel = new ContactModel();
                contactModel.setId(item.text.getId());
                contactModel.setFullname(item.text.getFullname());
                contactModel.setIsCheck("1");
                meeToSelectDao.insertTable(contactModel);
                ((Activity)context).setResult(Activity.RESULT_OK);
                ((Activity)context).finish();

            }
        });

    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvContactName)
        CustomTextView tvContactName;
        @BindView(R.id.llmeetTo)
        RelativeLayout llmeetTo;
        public ViewHolder(View itemView) {
            super(itemView);
            tvContactName = (CustomTextView) itemView.findViewById(R.id.tvContactName);
            llmeetTo = (RelativeLayout) itemView.findViewById(R.id.llmeetTo);
        }
    }


}
