package com.msf.project.sales1crm.model;

public class TaskbyStatusModel{
	private String taskStatus;
	private String totalTasks;
	private String taskStatusId;
	private String tasks;

	public void setTaskStatus(String taskStatus){
		this.taskStatus = taskStatus;
	}

	public String getTaskStatus(){
		return taskStatus;
	}

	public void setTotalTasks(String totalTasks){
		this.totalTasks = totalTasks;
	}

	public String getTotalTasks(){
		return totalTasks;
	}

	public void setTaskStatusId(String taskStatusId){
		this.taskStatusId = taskStatusId;
	}

	public String getTaskStatusId(){
		return taskStatusId;
	}

	public void setTasks(String tasks){
		this.tasks = tasks;
	}

	public String getTasks(){
		return tasks;
	}

	@Override
 	public String toString(){
		return 
			"TaskbyStatusModel{" + 
			"task_status = '" + taskStatus + '\'' + 
			",total_tasks = '" + totalTasks + '\'' + 
			",task_status_id = '" + taskStatusId + '\'' + 
			",tasks = '" + tasks + '\'' + 
			"}";
		}
}
