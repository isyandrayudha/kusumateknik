package com.msf.project.sales1crm.callback;

import android.os.Bundle;

public interface ProductDialogCallback {
    void onProductDialogCallback(Bundle data);
}
