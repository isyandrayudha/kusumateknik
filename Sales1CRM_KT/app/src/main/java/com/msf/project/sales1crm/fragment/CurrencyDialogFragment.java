package com.msf.project.sales1crm.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.adapter.KursListAdapter;
import com.msf.project.sales1crm.callback.CurrencyDialogCallback;
import com.msf.project.sales1crm.callback.CurrencyListCallback;
import com.msf.project.sales1crm.customview.CustomEditText;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.model.KursModel;
import com.msf.project.sales1crm.presenter.KursPresenter;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

@SuppressLint("ValidFragment")
public class CurrencyDialogFragment extends BaseDialogFragment implements CurrencyListCallback {
    @BindView(R.id.etSearch)
    CustomEditText etSearch;
    @BindView(R.id.ivClear)
    ImageView ivClear;
    @BindView(R.id.tvRefresh)
    CustomTextView tvRefresh;
    @BindView(R.id.rlNoConnection)
    RelativeLayout rlNoConnection;
    @BindView(R.id.llNotFound)
    LinearLayout llNotFound;
    @BindView(R.id.rlProgress)
    RelativeLayout rlProgress;
    @BindView(R.id.lvkurs)
    ListView lvkurs;
    @BindView(R.id.lvSwipe)
    SwipeRefreshLayout lvSwipe;
    @BindView(R.id.rlcategoryList)
    RelativeLayout rlcategoryList;
    private View view;
    private Context context;
    private KursListAdapter adapter;
    private CurrencyDialogCallback callback;
    private KursPresenter presenter;
    private List<KursModel> list = new ArrayList<>();

    @SuppressLint("ValidFragment")
    public CurrencyDialogFragment(Context context, CurrencyDialogCallback callback) {
        this.context = context;
        this.callback = callback;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, 0);
    }

    @Override
    public void onStart() {
        super.onStart();
        Window window = getDialog().getWindow();
        WindowManager.LayoutParams windowParams = window.getAttributes();
        windowParams.width = (int) (this.context.getResources().getDisplayMetrics().widthPixels * 0.99);
        windowParams.height = (int) (this.context.getResources().getDisplayMetrics().heightPixels * 0.97);
        window.setAttributes(windowParams);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreateView(inflater, container, savedInstanceState);

        this.view = inflater.inflate(R.layout.dialog_currency_list, container, false);

        ButterKnife.bind(this, this.view);
        return this.view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        this.presenter = new KursPresenter(context, this);
        getDataAPI();
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
    }

    private void getDataAPI() {
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(context)) {
            presenter.setupList(ApiParam.API_128);
        } else {
            lvSwipe.setVisibility(View.GONE);
            rlNoConnection.setVisibility(View.VISIBLE);
        }

    }

    private void setList() {
        adapter = new KursListAdapter(context, this.list);
        List<KursListAdapter.Row> rows = new ArrayList<>();
        for (KursModel cm : list) {
            rows.add(new KursListAdapter.Item(cm));
        }

        adapter.setRows(rows);
        lvkurs.setAdapter(adapter);
        lvkurs.setOnItemClickListener(new CurrencyDialogFragment.AdapterOnItemClick());
        lvkurs.deferNotifyDataSetChanged();
    }

    @Override
    public void finishCurrency(String result, List<KursModel> list) {
        lvSwipe.setRefreshing(false);
        rlProgress.setVisibility(View.GONE);
        if (result.equalsIgnoreCase("OK")) {
            this.list = list;
            setList();
            if (list.size() > 0) {
                llNotFound.setVisibility(View.GONE);
            } else {
                llNotFound.setVisibility(View.VISIBLE);
            }
        } else {
            rlNoConnection.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    private class AdapterOnItemClick implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final KursListAdapter.Item item = (KursListAdapter.Item) lvkurs.getAdapter().getItem(position);
            Bundle bundle = new Bundle();
            Log.d("TAG","name : " + item.text.getCurrencyName());
            bundle.putString("id", item.text.getCurrencyId());
            bundle.putString("name", item.text.getCurrencyName());
            bundle.putString("kurs", item.text.getKurs());
            Log.d("TAG", "ID " + item.text.getCurrencyId());
            callback.onCurrencyDialogCallback(bundle);
            dismiss();
        }
    }
}
