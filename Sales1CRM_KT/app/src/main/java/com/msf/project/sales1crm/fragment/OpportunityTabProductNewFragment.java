package com.msf.project.sales1crm.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.adapter.OpportunityDetailProductAdapter;
import com.msf.project.sales1crm.callback.OpportunityDetailCallback;
import com.msf.project.sales1crm.customview.CustomEditText;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.model.OpportunityModel;
import com.msf.project.sales1crm.model.ProductModel;
import com.msf.project.sales1crm.presenter.OpportunityDetailPresenter;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OpportunityTabProductNewFragment extends Fragment implements OpportunityDetailCallback {
    private final static String TAG = OpportunityTabProductNewFragment.class.getSimpleName();


    @BindView(R.id.etSearch)
    CustomEditText etSearch;
    @BindView(R.id.ivClear)
    ImageView ivClear;
    @BindView(R.id.tvRefresh)
    CustomTextView tvRefresh;
    @BindView(R.id.rlNoConnection)
    RelativeLayout rlNoConnection;
    @BindView(R.id.llNotFound)
    LinearLayout llNotFound;
    @BindView(R.id.lvProduct)
    ListView lvProduct;
    @BindView(R.id.rlProductList)
    RelativeLayout rlProductList;

    private View view;
    private Context context;
    private OpportunityDetailProductAdapter adapter;
    private List<ProductModel> list = new ArrayList<>();
    private List<ProductModel> searchList = new ArrayList<>();
    private OpportunityModel model;
    private OpportunityDetailPresenter presenter;

    public static OpportunityTabProductNewFragment newInstance() {
        OpportunityTabProductNewFragment fragment = new OpportunityTabProductNewFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        this.view = inflater.inflate(R.layout.fragment_opportunity_tab_product_new, container, false);
        ButterKnife.bind(this, view);
        return this.view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        this.context = getActivity();
        this.presenter = new OpportunityDetailPresenter(context,this);
        initView();
    }

    private void initView(){
        try {
            model = getArguments().getParcelable("opportunityModel");
        }catch (Exception e){}

        if(Sales1CRMUtils.ConnectionUtility.isNetworkConnected(context)){
            presenter.setupDetail(ApiParam.API_126,model.getId());
        } else {

        }
//
//        ivClear.setOnClickListener(click);
//
//        setList();
//        setTextWatcherForSearch();
    }






    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void finishDetail(String result, OpportunityModel model) {
        if(result.equalsIgnoreCase("OK")) {
                list.clear();
                try {
                    JSONArray array = new JSONArray(model.getProduct());

                    for (int i = 0; i < array.length(); i++){
                        ProductModel productModel = new ProductModel();
                        productModel.setId(array.getJSONObject(i).getString("id"));
                        productModel.setOppor_id(array.getJSONObject(i).getString("opportunity_id"));
                        productModel.setName(array.getJSONObject(i).getString("product_name"));
                        productModel.setCode(array.getJSONObject(i).getString("product_code"));
                        productModel.setGroup_id(array.getJSONObject(i).getString("product_group_id"));
                        productModel.setDetail_id(array.getJSONObject(i).getString("product_detail_id"));
                        productModel.setCategory_id(array.getJSONObject(i).getString("category_id"));
                        productModel.setCategory_name(array.getJSONObject(i).getString("category_name"));
                        productModel.setBranch_id(array.getJSONObject(i).getString("branch_id"));
                        productModel.setBranch_name(array.getJSONObject(i).getString("branch_name"));
                        productModel.setPrice(array.getJSONObject(i).getString("price"));
                        productModel.setStock(array.getJSONObject(i).getString("stock"));
                        productModel.setQty(array.getJSONObject(i).getString("quantity"));
                        productModel.setUnit(array.getJSONObject(i).getString("unit"));
                        productModel.setNote(array.getJSONObject(i).getString("description_product"));
                        productModel.setShow_price(array.getJSONObject(i).getString("show_price"));
                        productModel.setShow_stock(array.getJSONObject(i).getString("show_stock"));
                        list.add(productModel);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                adapter = new OpportunityDetailProductAdapter(context, list);

                List<OpportunityDetailProductAdapter.Row> rows = new ArrayList<OpportunityDetailProductAdapter.Row>();
                for (ProductModel productModel : list){
                    //Read List by Row to insert into List Adapter
                    rows.add(new OpportunityDetailProductAdapter.Item(productModel));
                }

                //Set Row Adapter
                adapter.setRows(rows);
                lvProduct.setAdapter(adapter);

                ivClear.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        etSearch.setText("");
                        etSearch.clearFocus();
                        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);

                        llNotFound.setVisibility(View.GONE);
                        list.clear();
                        try {
                            JSONArray array = new JSONArray(model.getProduct());

                            for (int i = 0; i < array.length(); i++){
                                ProductModel productModel = new ProductModel();
                                productModel.setId(array.getJSONObject(i).getString("id"));
                                productModel.setOppor_id(array.getJSONObject(i).getString("opportunity_id"));
                                productModel.setName(array.getJSONObject(i).getString("product_name"));
                                productModel.setCode(array.getJSONObject(i).getString("product_code"));
                                productModel.setGroup_id(array.getJSONObject(i).getString("product_group_id"));
                                productModel.setDetail_id(array.getJSONObject(i).getString("product_detail_id"));
                                productModel.setCategory_id(array.getJSONObject(i).getString("category_id"));
                                productModel.setCategory_name(array.getJSONObject(i).getString("category_name"));
                                productModel.setBranch_id(array.getJSONObject(i).getString("branch_id"));
                                productModel.setBranch_name(array.getJSONObject(i).getString("branch_name"));
                                productModel.setPrice(array.getJSONObject(i).getString("price"));
                                productModel.setStock(array.getJSONObject(i).getString("stock"));
                                productModel.setQty(array.getJSONObject(i).getString("quantity"));
                                productModel.setUnit(array.getJSONObject(i).getString("unit"));
                                productModel.setNote(array.getJSONObject(i).getString("description_product"));
                                productModel.setShow_price(array.getJSONObject(i).getString("show_price"));
                                productModel.setShow_stock(array.getJSONObject(i).getString("show_stock"));
                                list.add(productModel);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        adapter = new OpportunityDetailProductAdapter(context, list);

                        List<OpportunityDetailProductAdapter.Row> rows = new ArrayList<OpportunityDetailProductAdapter.Row>();
                        for (ProductModel productModel : list){
                            //Read List by Row to insert into List Adapter
                            rows.add(new OpportunityDetailProductAdapter.Item(productModel));
                        }

                        //Set Row Adapter
                        adapter.setRows(rows);
                        lvProduct.setAdapter(adapter);
                    }
                });


                etSearch.addTextChangedListener(new TextWatcher() {

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before,
                                              int count) {
                        Log.i(TAG, "onTextChanged");

                    }

                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count,
                                                  int after) {
                        Log.i(TAG, "beforeTextChanged");
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        Log.i(TAG, "afterTextChanged");
                        String search = etSearch.getText().toString();
                        searchList.clear();

                        if (!search.equals("")) {
                            if (list.size() > 0) {
                                for (int i = 0; i < list.size(); i++) {
                                    if (list.get(i).getName().toLowerCase().contains(search.toLowerCase())) {
                                        searchList.add(list.get(i));
                                    }
                                }

                                adapter = new OpportunityDetailProductAdapter(context, searchList);

                                List<OpportunityDetailProductAdapter.Row> rows = new ArrayList<OpportunityDetailProductAdapter.Row>();
                                for (ProductModel productModel : searchList){
                                    //Read List by Row to insert into List Adapter
                                    rows.add(new OpportunityDetailProductAdapter.Item(productModel));
                                }

                                //Set Row Adapter
                                adapter.setRows(rows);
                                lvProduct.setAdapter(adapter);
                                if (searchList.size() > 0){
                                    llNotFound.setVisibility(View.GONE);
                                }else{
                                    llNotFound.setVisibility(View.VISIBLE);
                                }
                            }

                            for (int i = 0; i < searchList.size(); i++) {
                                Log.i(TAG, "search data : "
                                        + searchList.get(i).getName());
                            }
                        } else {
                            llNotFound.setVisibility(View.GONE);
                            adapter.notifyDataSetChanged();
                    list.clear();
                try {
                    JSONArray array = new JSONArray(model.getProduct());

                    for (int i = 0; i < array.length(); i++){
                        ProductModel productModel = new ProductModel();
                        productModel.setId(array.getJSONObject(i).getString("id"));
                        productModel.setOppor_id(array.getJSONObject(i).getString("opportunity_id"));
                        productModel.setName(array.getJSONObject(i).getString("product_name"));
                        productModel.setCode(array.getJSONObject(i).getString("product_code"));
                        productModel.setGroup_id(array.getJSONObject(i).getString("product_group_id"));
                        productModel.setDetail_id(array.getJSONObject(i).getString("product_detail_id"));
                        productModel.setCategory_id(array.getJSONObject(i).getString("category_id"));
                        productModel.setCategory_name(array.getJSONObject(i).getString("category_name"));
                        productModel.setBranch_id(array.getJSONObject(i).getString("branch_id"));
                        productModel.setBranch_name(array.getJSONObject(i).getString("branch_name"));
                        productModel.setPrice(array.getJSONObject(i).getString("price"));
                        productModel.setStock(array.getJSONObject(i).getString("stock"));
                        productModel.setQty(array.getJSONObject(i).getString("quantity"));
                        productModel.setUnit(array.getJSONObject(i).getString("unit"));
                        productModel.setNote(array.getJSONObject(i).getString("description_product"));
                        productModel.setShow_price(array.getJSONObject(i).getString("show_price"));
                        productModel.setShow_stock(array.getJSONObject(i).getString("show_stock"));
                        list.add(productModel);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                adapter = new OpportunityDetailProductAdapter(context, list);

                List<OpportunityDetailProductAdapter.Row> rows = new ArrayList<OpportunityDetailProductAdapter.Row>();
                for (ProductModel productModel : list){
                    //Read List by Row to insert into List Adapter
                    rows.add(new OpportunityDetailProductAdapter.Item(productModel));
                }

                //Set Row Adapter
                adapter.setRows(rows);
                lvProduct.setAdapter(adapter);
                        }
                    }
                });
        }
        }
    }

