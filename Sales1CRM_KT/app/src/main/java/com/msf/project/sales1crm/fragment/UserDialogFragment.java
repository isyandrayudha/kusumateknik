package com.msf.project.sales1crm.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.adapter.UserListAdapter;
import com.msf.project.sales1crm.callback.UserDialogCallback;
import com.msf.project.sales1crm.customview.CustomEditText;
import com.msf.project.sales1crm.database.UserDao;
import com.msf.project.sales1crm.imageloader.ImageLoader;
import com.msf.project.sales1crm.model.UserModel;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.BindView;

@SuppressLint("ValidFragment")
public class UserDialogFragment extends BaseDialogFragment {

    @BindView(R.id.etSearch)
    CustomEditText etSearch;

    @BindView(R.id.ivClear)
    ImageView ivClear;

    @BindView(R.id.lvUser)
    ListView lvUser;

    @BindView(R.id.llNotFound)
    LinearLayout llNotFound;

    private View view;
    private Context context;
    private UserListAdapter adapter;
    private UserDialogCallback callback;
    private List<UserModel> list = new ArrayList<>();

    @SuppressLint("ValidFragment")
    public UserDialogFragment(Context context, UserDialogCallback listener) {
        this.context = context;
        this.callback = listener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, 0);
    }

    @Override
    public void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
        Window window = getDialog().getWindow();
        WindowManager.LayoutParams windowParams = window.getAttributes();
        windowParams.width = (int) (this.context.getResources().getDisplayMetrics().widthPixels * 0.99);
        windowParams.height = (int) (this.context.getResources().getDisplayMetrics().heightPixels * 0.97);
        window.setAttributes(windowParams);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreateView(inflater, container, savedInstanceState);

        this.view = inflater.inflate(R.layout.dialog_userlist, container, false);

        return this.view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);

        initView();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
    }

    private void initView() {
        Sales1CRMUtils.Utils.imageLoader = new ImageLoader(context);

        etSearch.clearFocus();
        //Set Seach Function
        setTextWatcherForSearch();
        getDataFromDatabase(etSearch.getText().toString());

        ivClear.setOnClickListener(click);
    }

    private void setTextWatcherForSearch() {
        etSearch.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                {
                    if (etSearch.getText().length() == 0){
                        ivClear.setVisibility(View.VISIBLE);
                    }
                } else {
                    if (etSearch.getText().length() == 0)
                    {
                        ivClear.setVisibility(View.GONE);
                    }else{
                        ivClear.setVisibility(View.VISIBLE);
                    }

                }
            }
        });

        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    getDataFromDatabase(etSearch.getText().toString());
                }

                return true;
            }
        });
    }

    private void getDataFromDatabase(String search){
        llNotFound.setVisibility(View.GONE);

        UserDao userDao = new UserDao(context);
        if (search.length() > 0)
            list = userDao.getUserSearchList(search);
        else
            list = userDao.getUserList();

        setList();
    }

    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.ivClear:
                    etSearch.setText("");
                    etSearch.clearFocus();
                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);
                    getDataFromDatabase(etSearch.getText().toString());
                    break;
                default:
                    break;
            }
        }
    };

    private void setList() {
        adapter = new UserListAdapter(context, this.list);

        List<UserListAdapter.Row> rows = new ArrayList<UserListAdapter.Row>();
        for (UserModel model : list){
            //Read List by Row to insert into List Adapter
            rows.add(new UserListAdapter.Item(model));
        }
        //Set Row Adapter
        adapter.setRows(rows);
        lvUser.setAdapter(adapter);
        lvUser.setOnItemClickListener(new UserDialogFragment.AdapterOnItemClick());
        lvUser.deferNotifyDataSetChanged();
    }

    private class AdapterOnItemClick implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            // TODO Auto-generated method stub
            final UserListAdapter.Item item = (UserListAdapter.Item) lvUser.getAdapter().getItem(position);
            Bundle bundle = new Bundle();
            bundle.putString("name", item.text.getFullname());
            bundle.putString("id", item.text.getId());
            callback.onUserDialogCallback(bundle);
            dismiss();
        }
    }
}
