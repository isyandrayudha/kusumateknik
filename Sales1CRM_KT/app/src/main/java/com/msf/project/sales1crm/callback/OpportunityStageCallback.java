package com.msf.project.sales1crm.callback;

import com.msf.project.sales1crm.model.OpportunityStageModel;

import java.util.List;

public interface OpportunityStageCallback {
    void finishStage(String result, List<OpportunityStageModel> list);
    void finishMoreStage (String result, List<OpportunityStageModel> list);
}
