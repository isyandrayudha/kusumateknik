package com.msf.project.sales1crm.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.model.EventModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by christianmacbook on 22/05/18.
 */

public class EventSimpleAdapter extends RecyclerView.Adapter<EventSimpleAdapter.ViewHolder> {

    //Vars
    private List<EventModel> list = new ArrayList<>();
    private Context context;

    public EventSimpleAdapter(Context context, List<EventModel> object) {
        this.context = context;
        this.list = object;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_simpleevent, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tvName.setText(list.get(position).getName());
        holder.tvDate.setText(list.get(position).getDate());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CustomTextView tvName, tvDate;

        public ViewHolder(View itemView) {
            super(itemView);

            tvName = itemView.findViewById(R.id.tvEventName);
            tvDate = itemView.findViewById(R.id.tvDate);
        }
    }
}
