package com.msf.project.sales1crm;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.msf.project.sales1crm.customview.CustomEditText;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.database.DBSchema;
import com.msf.project.sales1crm.database.ProductDao;
import com.msf.project.sales1crm.model.Product;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ExistingProductAddActivity extends Activity {

    @BindView(R.id.tvTitle)
    CustomTextView tvTitle;
    @BindView(R.id.rlTitle)
    RelativeLayout rlTitle;
    @BindView(R.id.etBranch)
    CustomEditText etBranch;
    @BindView(R.id.llbrand)
    LinearLayout llbrand;
    @BindView(R.id.etType)
    CustomEditText etType;
    @BindView(R.id.llType)
    LinearLayout llType;
    @BindView(R.id.etserialNumber)
    CustomEditText etserialNumber;
    @BindView(R.id.llSerial)
    LinearLayout llSerial;
    @BindView(R.id.etCondition)
    CustomEditText etCondition;
    @BindView(R.id.llcondition)
    LinearLayout llcondition;
    @BindView(R.id.rlForm)
    RelativeLayout rlForm;
    @BindView(R.id.tvCancel)
    CustomTextView tvCancel;
    @BindView(R.id.parentExistingProduct)
    RelativeLayout parentExistingProduct;
    @BindView(R.id.tvSave)
    CustomTextView tvSave;


    private ProductDao dao;
    private Product product;
    private Context context;
    private boolean editProduct = false;
    private String idProduct, brandProduct, typeProduct, serialProduct, conditionProduct, idTemp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        setContentView(R.layout.activity_existing_product_add);
        ButterKnife.bind(this);
        dao = new ProductDao(context);
        product = new Product();

        try {
            editProduct = getIntent().getExtras().getBoolean("editProduct");
            idTemp = getIntent().getExtras().getString("idTemp");
            idProduct = getIntent().getExtras().getString("idProduct");
            brandProduct = getIntent().getExtras().getString("brandProduct");
            typeProduct = getIntent().getExtras().getString("typeProduct");
            serialProduct = getIntent().getExtras().getString("serialProduct");
            conditionProduct = getIntent().getExtras().getString("conditionProduct");
        } catch (Exception e) {

        }

        Log.d("TAG", "idProduct " + idProduct);
        Log.d("TAG", "idProduct " + idTemp);

        initView();

        tvCancel.setOnClickListener(click);
        tvSave.setOnClickListener(click);
    }


    private void initView() {
        if (editProduct) {
            tvTitle.setText("Edit Product");
            etBranch.setText(brandProduct);
            etType.setText(typeProduct);
            etserialNumber.setText(serialProduct);
            etCondition.setText(conditionProduct);
            tvSave.setVisibility(View.VISIBLE);

        }
    }

    private boolean validation() {
        if (etBranch.getText().toString().equalsIgnoreCase("") &&
                etType.getText().toString().equalsIgnoreCase("") &&
                etserialNumber.getText().toString().equalsIgnoreCase("") &&
                etCondition.getText().toString().equalsIgnoreCase("")) {
            dialogErrorHandling("Input Minimum 1 Field !!! ");
            return false;
        }
        return true;
    }

    private void dialogErrorHandling(String info) {
        final Dialog dialog = new Dialog(ExistingProductAddActivity.this,
                android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_dialog);

        TextView text = (TextView) dialog.findViewById(R.id.tvKeterangan);
        text.setText(info);
        TextView tvOK = (TextView) dialog.findViewById(R.id.tvOK);
        tvOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.tvCancel:
                    finish();
                    break;
                case R.id.tvSave:
                    if (validation()) {
                        if (editProduct) {
                            Product pProduct = dao.getProductById(idTemp);
                            pProduct.setBrand(etBranch.getText().toString());
                            pProduct.setType(etType.getText().toString());
                            pProduct.setSerialNumber(etserialNumber.getText().toString());
                            pProduct.setCondition(etCondition.getText().toString());
                            pProduct.setStatus("2");
                            dao.updateTable(pProduct, idTemp, DBSchema.Product.KEY_ID_TEMP);
                            Toast.makeText(getApplicationContext(), "Success update this product!!!", Toast.LENGTH_SHORT).show();
                        } else {
                            product.setBrand(etBranch.getText().toString());
                            product.setType(etType.getText().toString());
                            product.setSerialNumber(etserialNumber.getText().toString());
                            product.setCondition(etCondition.getText().toString());
                            product.setStatus("2");
                            dao.insertTable(product);
                            Toast.makeText(getApplicationContext(), "Success add new product!!!", Toast.LENGTH_SHORT).show();
                        }
                        setResult(RESULT_OK);
                        finish();
                    }
                    break;
            }
        }
    };
}
