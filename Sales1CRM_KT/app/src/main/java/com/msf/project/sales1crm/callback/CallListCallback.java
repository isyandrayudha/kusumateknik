package com.msf.project.sales1crm.callback;

import com.msf.project.sales1crm.model.CallsModel;

import java.util.List;

public interface CallListCallback {
    void finishCallList(String result, List<CallsModel> list);
    void finishCallMoreList(String result, List<CallsModel> list);
}
