package com.msf.project.sales1crm.callback;

import com.msf.project.sales1crm.model.UserModel;

import java.util.List;

public interface UserListCallback {
    void finishUserList(String result, List<UserModel> list);
}
