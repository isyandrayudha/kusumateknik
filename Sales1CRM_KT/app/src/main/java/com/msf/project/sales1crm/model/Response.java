package com.msf.project.sales1crm.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

public class Response implements  Comparable, Parcelable{
	private String calls;
	private String toDoList;
	private String event;

	public Response(){
		setCalls("");
		setEvent("");
		setToDoList("");
	}


	protected Response(Parcel in) {
		readFromParcel(in);
	}

	public void readFromParcel(Parcel in) {
		calls = in.readString();
		toDoList = in.readString();
		event = in.readString();
	}

	public static final Creator<Response> CREATOR = new Creator<Response>() {
		@Override
		public Response createFromParcel(Parcel in) {
			return new Response(in);
		}

		@Override
		public Response[] newArray(int size) {
			return new Response[size];
		}
	};

	public void setCalls(String calls){
		this.calls = calls;
	}

	public String getCalls(){
		return calls;
	}


	public String getToDoList(){
		return toDoList;
	}

	public void setToDoList(String toDoList){
		this.toDoList = toDoList;
	}

	public void setEvent(String event){
		this.event = event;
	}

	public String getEvent(){
		return event;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int i) {
		parcel.writeString(calls);
		parcel.writeString(event);
		parcel.writeString(toDoList);
	}

	@Override
	public int compareTo(@NonNull Object o) {
		return 0;
	}
}
