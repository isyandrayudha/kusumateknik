package com.msf.project.sales1crm;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.msf.project.sales1crm.adapter.SimpleProductAdapter;
import com.msf.project.sales1crm.callback.AccountDialogCallback;
import com.msf.project.sales1crm.callback.EventAddCallback;
import com.msf.project.sales1crm.callback.UserDialogCallback;
import com.msf.project.sales1crm.customview.CustomDialog;
import com.msf.project.sales1crm.customview.CustomEditText;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.customview.DatePickerFragment;
import com.msf.project.sales1crm.customview.SpinnerCustomAdapter;
import com.msf.project.sales1crm.database.DBHelper;
import com.msf.project.sales1crm.database.MeeToSelectDao;
import com.msf.project.sales1crm.database.ProductDao;
import com.msf.project.sales1crm.fragment.AccountDialogFragment;
import com.msf.project.sales1crm.fragment.UserDialogFragment;
import com.msf.project.sales1crm.imageloader.ImageCompression;
import com.msf.project.sales1crm.model.ActivityEventModel;
import com.msf.project.sales1crm.model.ContactModel;
import com.msf.project.sales1crm.model.EventPurposeModel;
import com.msf.project.sales1crm.model.EventStatusModel;
import com.msf.project.sales1crm.model.Product;
import com.msf.project.sales1crm.presenter.AEventAddPresenter;
import com.msf.project.sales1crm.presenter.AccountListPresenter;
import com.msf.project.sales1crm.presenter.MeetToListPresenter;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.PreferenceUtility;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AEventAddActivity extends BaseActivity implements EventAddCallback {


    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.tvTitle)
    CustomTextView tvTitle;
    @BindView(R.id.ivSave)
    ImageView ivSave;
    @BindView(R.id.tvAssignTo)
    CustomTextView tvAssignTo;
    @BindView(R.id.tvAccountName)
    CustomTextView tvAccountName;
    @BindView(R.id.ivAccountName)
    ImageView ivAccountName;
    @BindView(R.id.tvMeetTo)
    CustomTextView tvMeetTo;
    @BindView(R.id.ivMeetToName)
    ImageView ivMeetToName;
    @BindView(R.id.etEventName)
    CustomEditText etEventName;
    @BindView(R.id.tvCloseDate)
    CustomTextView tvCloseDate;
    @BindView(R.id.etDesc)
    CustomEditText etDesc;
    @BindView(R.id.etFollowUp)
    CustomEditText etFollowUp;
    @BindView(R.id.tvEp)
    CustomTextView tvEp;
    @BindView(R.id.ProductAdd)
    ImageView ProductAdd;
    @BindView(R.id.lvProductSave)
    ListView lvProductSave;
    @BindView(R.id.llContainerProduct)
    LinearLayout llContainerProduct;
    @BindView(R.id.foto1)
    ImageView foto1;
    @BindView(R.id.ivCam1)
    ImageView ivCam1;
    @BindView(R.id.llcam1)
    LinearLayout llcam1;
    @BindView(R.id.foto2)
    ImageView foto2;
    @BindView(R.id.ivcam2)
    ImageView ivcam2;
    @BindView(R.id.llcam2)
    LinearLayout llcam2;
    @BindView(R.id.foto3)
    ImageView foto3;
    @BindView(R.id.ivCam3)
    ImageView ivCam3;
    @BindView(R.id.llcam3)
    LinearLayout llcam3;
    private Context context;
    private DatePickerFragment datepicker;
    private AEventAddPresenter presenter;
    private AccountListPresenter accountPresenter;
    private String url = "", contact_json, account_id = "0", assign_id = "0";
    private String open_date = "", string_year, string_month, string_day;
    private int year, month, day;
    private boolean from_detail_event = false;
    private ActivityEventModel model;
    private SimpleProductAdapter simpleProductAdapter;
    private LinearLayout parentLinearLayout;
    private String stringLatitude, stringLongitude;


    private EventStatusModel statusModel;
    private EventPurposeModel purposeModel;
    private List<Product> productList = new ArrayList<>();
    private List<EventStatusModel> statusList = new ArrayList<>();
    private List<EventPurposeModel> purposeList = new ArrayList<>();
    private SpinnerCustomAdapter spStatusAdapter, spPurposeAdapter;
    private ArrayList<String> arrStatus = new ArrayList<>();
    private ArrayList<String> arrPurpose = new ArrayList<>();
    private int indexStatus = 0, indexPurpose = 0;

    private String photoPath1 = "", photoPath2 = "", photoPath3 = "",
            filePath1 = "", filePath2 = "", filePath3 = "", p_1, p_2, p_3,
            photo_1 = "", photo_2 = "", photo_3 = "", eIdProduct, eBrand, eType, eSerial, eCondition, eProductStatus;
    private Bitmap savedBitmap;
    private MeetToListPresenter meetToListPresenter;

    static AEventAddActivity aEventAddActivity;

    public static AEventAddActivity getInstance() {
        return aEventAddActivity;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Sales1CRMUtils.GALLERY:
                if (resultCode == RESULT_OK) {
                    Uri selecctedImageUri = data.getData();
                    BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
                    bmpFactoryOptions.inJustDecodeBounds = false;
                    bmpFactoryOptions.inSampleSize = 4;
                    Bitmap bmp = null;
                    try {
                        bmp = Sales1CRMUtils.Utils.scaleImage(context, selecctedImageUri);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    savedBitmap = bmp.copy(bmp.getConfig(), bmp.isMutable() ? true
                            : false);

                    foto1.setImageBitmap(Sales1CRMUtils.Utils.getroundBitmap(bmp));
                    ivCam1.setVisibility(View.INVISIBLE);
                    llcam1.setVisibility(View.INVISIBLE);
                    photo_1 = Sales1CRMUtils.ImageUtility.encodeToBase64(bmp, Bitmap.CompressFormat.JPEG, 70);

                    bmp.recycle();
                    bmp = null;
                }
                break;
            case Sales1CRMUtils.GALLERY_2:
                if (resultCode == RESULT_OK) {
                    Uri selecctedImageUri = data.getData();
                    BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
                    bmpFactoryOptions.inJustDecodeBounds = false;
                    bmpFactoryOptions.inSampleSize = 4;
                    Bitmap bmp = null;
                    try {
                        bmp = Sales1CRMUtils.Utils.scaleImage(context, selecctedImageUri);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    savedBitmap = bmp.copy(bmp.getConfig(), bmp.isMutable() ? true
                            : false);

                    foto2.setImageBitmap(Sales1CRMUtils.Utils.getroundBitmap(bmp));
                    ivcam2.setVisibility(View.INVISIBLE);
                    llcam2.setVisibility(View.INVISIBLE);
                    photo_2 = Sales1CRMUtils.ImageUtility.encodeToBase64(bmp, Bitmap.CompressFormat.JPEG, 70);

                    bmp.recycle();
                    bmp = null;
                }
                break;
            case Sales1CRMUtils.GALLERY_3:
                if (resultCode == RESULT_OK) {
                    Uri selecctedImageUri = data.getData();
                    BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
                    bmpFactoryOptions.inJustDecodeBounds = false;
                    bmpFactoryOptions.inSampleSize = 4;
                    Bitmap bmp = null;
                    try {
                        bmp = Sales1CRMUtils.Utils.scaleImage(context, selecctedImageUri);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    savedBitmap = bmp.copy(bmp.getConfig(), bmp.isMutable() ? true
                            : false);

                    foto3.setImageBitmap(Sales1CRMUtils.Utils.getroundBitmap(bmp));
                    ivCam3.setVisibility(View.INVISIBLE);
                    llcam3.setVisibility(View.INVISIBLE);
                    photo_3 = Sales1CRMUtils.ImageUtility.encodeToBase64(bmp, Bitmap.CompressFormat.JPEG, 70);

                    bmp.recycle();
                    bmp = null;
                }
                break;
            case Sales1CRMUtils.CAMERA_REQUEST:
                if (resultCode == RESULT_OK) {
                    BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
                    bmpFactoryOptions.inJustDecodeBounds = false;
                    bmpFactoryOptions.inSampleSize = 4;

                    Log.d("TAG", "Photo Path : " + photoPath1);

                    // imageFilePath image path which you pass withintent
                    Bitmap bmp = BitmapFactory.decodeFile(photoPath1,
                            bmpFactoryOptions);
                    // BitmapFactory.Options options = new BitmapFactory.Options();
                    // options.inSampleSize = 4;
                    Log.i("HUAI", "data : " + bmp);
                    // Bitmap photo = (Bitmap) data.getExtras().get("data");
                    if (bmp.getWidth() >= bmp.getHeight()) {

                        bmp = Bitmap.createBitmap(bmp,
                                bmp.getWidth() / 2 - bmp.getHeight() / 2, 0,
                                bmp.getHeight(), bmp.getHeight(), null, true);

                    } else {

                        bmp = Bitmap.createBitmap(bmp, 0,
                                bmp.getHeight() / 2 - bmp.getWidth() / 2,
                                bmp.getWidth(), bmp.getWidth(), null, true);
                    }
                    bmp = Sales1CRMUtils.ImageUtility.scaleDownBitmap(bmp);
                    ExifInterface ei;
                    int orientation = 0;
                    try {
                        ei = new ExifInterface(photoPath1);
                        orientation = ei.getAttributeInt(
                                ExifInterface.TAG_ORIENTATION,
                                ExifInterface.ORIENTATION_NORMAL);

                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                    switch (orientation) {
                        case ExifInterface.ORIENTATION_ROTATE_90:
                            bmp = Sales1CRMUtils.ImageUtility.RotateBitmap(bmp, 90);
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_180:
                            bmp = Sales1CRMUtils.ImageUtility.RotateBitmap(bmp, 180);
                            break;
                        // etc.
                    }
                    savedBitmap = bmp.copy(bmp.getConfig(), bmp.isMutable() ? true
                            : false);
                    foto1.setImageBitmap(Sales1CRMUtils.Utils.getroundBitmap(bmp));
                    ivCam1.setVisibility(View.INVISIBLE);
                    llcam1.setVisibility(View.INVISIBLE);
                    bmp = null;

                    onCaptureImageResult(data);
                }
                break;
            case Sales1CRMUtils.CAMERA_REQUEST_2:
                if (resultCode == RESULT_OK) {
                    BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
                    bmpFactoryOptions.inJustDecodeBounds = false;
                    bmpFactoryOptions.inSampleSize = 4;

                    Log.d("TAG", "Photo Path : " + photoPath2);

                    // imageFilePath image path which you pass withintent
                    Bitmap bmp = BitmapFactory.decodeFile(photoPath2,
                            bmpFactoryOptions);
                    // BitmapFactory.Options options = new BitmapFactory.Options();
                    // options.inSampleSize = 4;
                    Log.i("HUAI", "data : " + bmp);
                    // Bitmap photo = (Bitmap) data.getExtras().get("data");
                    if (bmp.getWidth() >= bmp.getHeight()) {

                        bmp = Bitmap.createBitmap(bmp,
                                bmp.getWidth() / 2 - bmp.getHeight() / 2, 0,
                                bmp.getHeight(), bmp.getHeight(), null, true);

                    } else {

                        bmp = Bitmap.createBitmap(bmp, 0,
                                bmp.getHeight() / 2 - bmp.getWidth() / 2,
                                bmp.getWidth(), bmp.getWidth(), null, true);
                    }
                    bmp = Sales1CRMUtils.ImageUtility.scaleDownBitmap(bmp);
                    ExifInterface ei;
                    int orientation = 0;
                    try {
                        ei = new ExifInterface(photoPath2);
                        orientation = ei.getAttributeInt(
                                ExifInterface.TAG_ORIENTATION,
                                ExifInterface.ORIENTATION_NORMAL);

                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                    switch (orientation) {
                        case ExifInterface.ORIENTATION_ROTATE_90:
                            bmp = Sales1CRMUtils.ImageUtility.RotateBitmap(bmp, 90);
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_180:
                            bmp = Sales1CRMUtils.ImageUtility.RotateBitmap(bmp, 180);
                            break;
                        // etc.
                    }
                    savedBitmap = bmp.copy(bmp.getConfig(), bmp.isMutable() ? true
                            : false);
                    foto2.setImageBitmap(Sales1CRMUtils.Utils.getroundBitmap(bmp));
                    ivcam2.setVisibility(View.INVISIBLE);
                    llcam2.setVisibility(View.INVISIBLE);
                    bmp.recycle();
                    bmp = null;

                    onCaptureImageResult2(data);
                }
                break;
            case Sales1CRMUtils.CAMERA_REQUEST_3:
                if (resultCode == RESULT_OK) {
                    BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
                    bmpFactoryOptions.inJustDecodeBounds = false;
                    bmpFactoryOptions.inSampleSize = 4;

                    Log.d("TAG", "Photo Path : " + photoPath3);

                    // imageFilePath image path which you pass withintent
                    Bitmap bmp = BitmapFactory.decodeFile(photoPath3,
                            bmpFactoryOptions);
                    // BitmapFactory.Options options = new BitmapFactory.Options();
                    // options.inSampleSize = 4;
                    Log.i("HUAI", "data : " + bmp);
                    // Bitmap photo = (Bitmap) data.getExtras().get("data");
                    if (bmp.getWidth() >= bmp.getHeight()) {

                        bmp = Bitmap.createBitmap(bmp,
                                bmp.getWidth() / 2 - bmp.getHeight() / 2, 0,
                                bmp.getHeight(), bmp.getHeight(), null, true);

                    } else {

                        bmp = Bitmap.createBitmap(bmp, 0,
                                bmp.getHeight() / 2 - bmp.getWidth() / 2,
                                bmp.getWidth(), bmp.getWidth(), null, true);
                    }
                    bmp = Sales1CRMUtils.ImageUtility.scaleDownBitmap(bmp);
                    ExifInterface ei;
                    int orientation = 0;
                    try {
                        ei = new ExifInterface(photoPath3);
                        orientation = ei.getAttributeInt(
                                ExifInterface.TAG_ORIENTATION,
                                ExifInterface.ORIENTATION_NORMAL);

                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                    switch (orientation) {
                        case ExifInterface.ORIENTATION_ROTATE_90:
                            bmp = Sales1CRMUtils.ImageUtility.RotateBitmap(bmp, 90);
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_180:
                            bmp = Sales1CRMUtils.ImageUtility.RotateBitmap(bmp, 180);
                            break;
                        // etc.
                    }
                    savedBitmap = bmp.copy(bmp.getConfig(), bmp.isMutable() ? true
                            : false);
                    foto3.setImageBitmap(Sales1CRMUtils.Utils.getroundBitmap(bmp));
                    ivCam3.setVisibility(View.INVISIBLE);
                    llcam3.setVisibility(View.INVISIBLE);
                    bmp.recycle();
                    bmp = null;

                    onCaptureImageResult3(data);
                }
                break;
            case Sales1CRMUtils.LIST_PRODUCT:
                if (resultCode == RESULT_OK) {
                    setProductList();
                    simpleProductAdapter.notifyDataSetChanged();
                }
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case Sales1CRMUtils.KEY_CAMERA_PERMISION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    cameraIntent();
                    cameraIntent2();
                    cameraIntent3();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(context, "Doesn't have permission... ", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void selectImage1() {
        final CharSequence[] items = {"Take Picture", "Galery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Add Picture!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals("Take Picture")) {

                    if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.CAMERA}, Sales1CRMUtils.KEY_CAMERA_PERMISION);
                    } else {
                        cameraIntent();
                    }
                } else if (items[item].equals("Galery")) {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(
                            Intent.createChooser(intent, "Select Picture"),
                            Sales1CRMUtils.GALLERY);
                    dialog.dismiss();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void selectImage2() {
        final CharSequence[] items = {"Take Picture", "Galery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Add Picture!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals("Take Picture")) {

                    if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.CAMERA}, Sales1CRMUtils.KEY_CAMERA_PERMISION);
                    } else {
                        cameraIntent2();
                    }
                } else if (items[item].equals("Galery")) {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(
                            Intent.createChooser(intent, "Select Picture"),
                            Sales1CRMUtils.GALLERY_2);
                    dialog.dismiss();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void selectImage3() {
        final CharSequence[] items = {"Take Picture", "Galery", "Batal"};

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Add Picture!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals("Take Picture")) {

                    if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.CAMERA}, Sales1CRMUtils.KEY_CAMERA_PERMISION);
                    } else {
                        cameraIntent3();
                    }
                } else if (items[item].equals("Galery")) {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(
                            Intent.createChooser(intent, "Select Picture"),
                            Sales1CRMUtils.GALLERY_3);
                    dialog.dismiss();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void cameraIntent() {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        Intent intentCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File photoFile = null;
        try {
            photoFile = createImageFile1();

        } catch (IOException e) {
            e.printStackTrace();
        }

        if (photoFile != null) {
            Uri photoURI = Uri.fromFile(photoFile);
            intentCamera.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
            startActivityForResult(intentCamera, Sales1CRMUtils.CAMERA_REQUEST);
        }
    }

    private void cameraIntent2() {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        Intent intentCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File photoFile = null;
        try {
            photoFile = createImageFile2();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (photoFile != null) {
            Uri photoURI = Uri.fromFile(photoFile);
            intentCamera.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
            startActivityForResult(intentCamera, Sales1CRMUtils.CAMERA_REQUEST_2);
        }
    }

    private void cameraIntent3() {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        Intent intentCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File photoFile = null;
        try {
            photoFile = createImageFile3();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (photoFile != null) {
            Uri photoURI = Uri.fromFile(photoFile);
            intentCamera.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
            startActivityForResult(intentCamera, Sales1CRMUtils.CAMERA_REQUEST_3);
        }
    }


    private File createImageFile1() throws IOException {
        // Create an image file name
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            String Name = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String imageFileName = Name + ".jpg";
            File storageDir = new File(Environment.getExternalStorageDirectory().getPath(), imageFileName);
            photoPath1 = storageDir.getPath();
            return storageDir;
        } else {
            String imageFileName = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            File storageDir = context.getExternalFilesDir("Pictures");
            File image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );
            Log.d("TAG", image.getPath());
            photoPath1 = image.getPath();
            return image;
        }
    }

    private File createImageFile2() throws IOException {
        // Create an image file name
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            String Name = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String imageFileName = Name + ".jpg";
            File storageDir = new File(Environment.getExternalStorageDirectory().getPath(), imageFileName);
            photoPath2 = storageDir.getPath();
            return storageDir;
        } else {
            String imageFileName = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            File storageDir = context.getExternalFilesDir("Pictures");
            File image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );
            Log.d("TAG", image.getPath());
            photoPath2 = image.getPath();
            return image;
        }
    }

    private File createImageFile3() throws IOException {
        // Create an image file name
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            String Name = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String imageFileName = Name + ".jpg";
            File storageDir = new File(Environment.getExternalStorageDirectory().getPath(), imageFileName);
            photoPath3 = storageDir.getPath();
            return storageDir;
        } else {
            String imageFileName = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            File storageDir = context.getExternalFilesDir("Pictures");
            File image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );
            Log.d("TAG", image.getPath());
            photoPath3 = image.getPath();
            return image;
        }
    }


    private void onCaptureImageResult(Intent data) {
        //Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        Log.d("TAG", "Masuk OnCaptureImage");
        try {
            ImageCompression imageCompression = new ImageCompression(context);
            filePath1 = imageCompression.compressImage(photoPath1);
            Log.d("PPP", "Path : " + filePath1);
            Bitmap thumbnail = Sales1CRMUtils.ImageUtility.getThumbnail(filePath1);

            photo_1 = Sales1CRMUtils.ImageUtility.encodeToBase64(thumbnail, Bitmap.CompressFormat.JPEG, 50);


            File imgFile = new File(photoPath1);
            imgFile.delete();
            photoPath1 = "";
        } catch (IOException e) {
            e.printStackTrace();
            Log.d("TAG", e.getMessage());
        }
    }

    private void onCaptureImageResult2(Intent data) {
        //Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        Log.d("TAG", "Masuk OnCaptureImage");
        try {
            ImageCompression imageCompression = new ImageCompression(context);
            filePath2 = imageCompression.compressImage(photoPath2);
            Log.d("PPP", "Path : " + filePath2);
            Bitmap thumbnail = Sales1CRMUtils.ImageUtility.getThumbnail(filePath2);

            photo_2 = Sales1CRMUtils.ImageUtility.encodeToBase64(thumbnail, Bitmap.CompressFormat.JPEG, 50);


            File imgFile = new File(photoPath2);
            imgFile.delete();
            photoPath2 = "";
        } catch (IOException e) {
            e.printStackTrace();
            Log.d("TAG", e.getMessage());
        }
    }

    private void onCaptureImageResult3(Intent data) {
        //Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        Log.d("TAG", "Masuk OnCaptureImage");
        try {
            ImageCompression imageCompression = new ImageCompression(context);
            filePath3 = imageCompression.compressImage(photoPath3);
            Log.d("PPP", "Path : " + filePath3);
            Bitmap thumbnail = Sales1CRMUtils.ImageUtility.getThumbnail(filePath3);

            photo_3 = Sales1CRMUtils.ImageUtility.encodeToBase64(thumbnail, Bitmap.CompressFormat.JPEG, 50);

            File imgFile = new File(photoPath3);
            imgFile.delete();
            photoPath3 = "";
        } catch (IOException e) {
            e.printStackTrace();
            Log.d("TAG", e.getMessage());
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aevent_add);
        ButterKnife.bind(this);
        this.context = this;

        aEventAddActivity = this;

        this.presenter = new AEventAddPresenter(context, this);
        this.url = PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.URL);

        try {
            from_detail_event = getIntent().getExtras().getBoolean("from_detail_event");
        } catch (Exception e) {
        }

        try {
            model = getIntent().getExtras().getParcelable("aEventModel");
        } catch (Exception e) {
        }

        Log.d("TAG", "acc_name" + tvAccountName.getText().toString());

        datepicker = new DatePickerFragment();
        datepicker.setListener(dateListener);

        initDate();
        initView();

    }

    @Override
    protected void onResume() {
        super.onResume();
        MeeToSelectDao dao = new MeeToSelectDao(context);
        List<ContactModel> check = dao.getFullName();
        String item = "";
        String itemId = "";
        for (int i = 0; i < check.size(); i++) {
            item = item + check.get(i).getFullname();
            itemId = itemId + check.get(i).getId();
            if (i != check.size() - 1) {
                item = item + ", ";
                itemId = itemId + ", ";
            }
        }

        tvMeetTo.setText(item);
        setProductList();
        simpleProductAdapter.notifyDataSetChanged();
    }

//    @Override
//    protected void onStart() {
//        super.onStart();
//        MeeToSelectDao dao = new MeeToSelectDao(context);
//        List<ContactModel> check = dao.getFullName();

//        String item = "";
//        for (int i = 0; i < check.size(); i++) {
//            item = item + check.get(i).getFullname();
//            itemId = itemId + check.get(i).getId();
//            if (i != check.size() - 1) {
//                item = item + ", ";
//                itemId = itemId + ", ";
//            }
//        }
//
//        tvMeetTo.setText(item);

//        setProductList();
//        simpleProductAdapter.notifyDataSetChanged();
//    }


    private void initDate() {
        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH) + 1;
        day = c.get(Calendar.DAY_OF_MONTH);
    }

    private void initView() {
        try {
            tvCloseDate.setText(getIntent().getExtras().getString("date"));
        } catch (Exception e) {
            tvCloseDate.setText("Date");
        }

        tvAssignTo.setText(PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.KEY_FULLNAME));
        assign_id = PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.KEY_USERID);

        if (from_detail_event) {
            tvTitle.setText("Edit Event");

            Log.d("TAG", "foto_1 " + model.getPhoto_1());
            Log.d("TAG", "nama_1 " + model.getNama1());

            if (model.getNama1().equalsIgnoreCase("")) {
                ivCam1.setVisibility(View.VISIBLE);
                llcam1.setVisibility(View.VISIBLE);
            } else {
                Sales1CRMUtils.Utils.imageLoader.DisplayImage(url + model.getPhoto_1(),
                        foto1, Bitmap.Config.ARGB_8888, 700, Sales1CRMUtils.TYPE_RECT);
                ivCam1.setVisibility(View.INVISIBLE);
                llcam1.setVisibility(View.INVISIBLE);
            }

            if (model.getNama2().equalsIgnoreCase("")) {
                ivcam2.setVisibility(View.VISIBLE);
                llcam2.setVisibility(View.VISIBLE);
            } else {
                Sales1CRMUtils.Utils.imageLoader.DisplayImage(url + model.getPhoto_2(),
                        foto2, Bitmap.Config.ARGB_8888, 700, Sales1CRMUtils.TYPE_RECT);
                ivcam2.setVisibility(View.INVISIBLE);
                llcam2.setVisibility(View.INVISIBLE);
            }

            if (model.getNama3().equalsIgnoreCase("")) {
                ivCam3.setVisibility(View.VISIBLE);
                llcam3.setVisibility(View.VISIBLE);
            } else {
                Sales1CRMUtils.Utils.imageLoader.DisplayImage(url + model.getPhoto_3(),
                        foto3, Bitmap.Config.ARGB_8888, 700, Sales1CRMUtils.TYPE_RECT);
                ivCam3.setVisibility(View.INVISIBLE);
                llcam3.setVisibility(View.INVISIBLE);
            }


            assign_id = model.getAssignTo();
            account_id = model.getAccountId();
            tvAssignTo.setText(model.getUser_name());
            tvAccountName.setText(model.getAccount_name());
            tvCloseDate.setText(model.getDate());
            etEventName.setText(model.getName());
            etDesc.setText(model.getDescription());
            etFollowUp.setText(model.getFollowUp());

            setProductList();

        }

        ProductDao productDao = new ProductDao(context);
        productDao.getProduc("1");
        Log.d("TAG", "PRD " + productDao.getProduc("1").getBrand());

        MeeToSelectDao dao = new MeeToSelectDao(context);
        List<ContactModel> check = dao.getFullName();

        Log.d("TAG", "TES " + check);

        String item = "";
        for (int i = 0; i < check.size(); i++) {
            item = item + check.get(i).getFullname();
            if (i != check.size() - 1) {
                item = item + ", ";
            }
        }


        tvMeetTo.setText(item);

        ivBack.setOnClickListener(click);
        ivSave.setOnClickListener(click);
        tvAccountName.setOnClickListener(click);
        ivAccountName.setOnClickListener(click);
        tvCloseDate.setOnClickListener(click);
        tvAssignTo.setOnClickListener(click);
        foto1.setOnClickListener(click);
        foto2.setOnClickListener(click);
        foto3.setOnClickListener(click);
        tvMeetTo.setOnClickListener(click);
        ProductAdd.setOnClickListener(click);
        setProductList();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        final CustomDialog dialogback = CustomDialog.setupDialogConfirmation(context, "Anda ingin kembali? Data yang anda masukkan akan hilang?");
        CustomTextView tvCancel = (CustomTextView) dialogback.findViewById(R.id.tvCancel);
        CustomTextView tvYa = (CustomTextView) dialogback.findViewById(R.id.tvOK);

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogback.dismiss();
            }
        });

        tvYa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProductDao productDao = new ProductDao(context);
                productDao.deleteAllRecord();

                MeeToSelectDao meeToSelectDao = new MeeToSelectDao(context);
                meeToSelectDao.deleteAllRecord();

                finish();
                dialogback.dismiss();
            }
        });
        dialogback.show();
    }

    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.ivBack:
                    final CustomDialog dialogback = CustomDialog.setupDialogConfirmation(context, "Anda ingin kembali? Data yang anda masukkan akan hilang?");
                    CustomTextView tvCancel = (CustomTextView) dialogback.findViewById(R.id.tvCancel);
                    CustomTextView tvYa = (CustomTextView) dialogback.findViewById(R.id.tvOK);

                    tvCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialogback.dismiss();
                        }
                    });

                    tvYa.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ProductDao productDao = new ProductDao(context);
                            productDao.deleteAllRecord();

                            MeeToSelectDao meeToSelectDao = new MeeToSelectDao(context);
                            meeToSelectDao.deleteAllRecord();

                            finish();
                            dialogback.dismiss();
                        }
                    });
                    dialogback.show();
                    break;
                case R.id.ivSave:
                    if (checkField()) {
                        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(context)) {
                            if (from_detail_event) {
                                MeeToSelectDao dao = new MeeToSelectDao(context);
                                List<ContactModel> check = dao.getFullName();
                                String item = "";
                                String itemId = "";
                                for (int i = 0; i < check.size(); i++) {
                                    item = item + check.get(i).getFullname();
                                    itemId = itemId + check.get(i).getId();
                                    if (i != check.size() - 1) {
                                        item = item + ", ";
                                        itemId = itemId + ", ";
                                    }
                                }

                                ProductDao productDao = new ProductDao(context);
                                List<Product> brand = productDao.getProduct();
                                String id = "";
                                String bran = "";
                                String type = "";
                                String serialNumber = "";
                                String condition = "";
                                for (int i = 0; i < brand.size(); i++) {
                                    id = brand.get(i).getId();
                                    bran = bran + brand.get(i).getBrand();
                                    type = type + brand.get(i).getType();
                                    serialNumber = serialNumber + brand.get(i).getSerialNumber();
                                    condition = condition + brand.get(i).getCondition();
                                    if (i != brand.size() - 1) {
                                        bran = bran + "\",\"";
                                        type = type + "\",\"";
                                        serialNumber = serialNumber + "\",\"";
                                        condition = condition + "\",\"";
                                    }
                                }


                                Log.d("TAG", "Meet to " + "[" + itemId + "]");
                                model.setId(model.getId());
                                model.setAccountId(account_id);
                                model.setAssignTo(assign_id);
                                model.setName(etEventName.getText().toString());
                                model.setDate(tvCloseDate.getText().toString());
                                model.setDescription(etDesc.getText().toString());
                                model.setFollowUp(etFollowUp.getText().toString());
                                model.setId_product_event(getIdProductEvent());
                                model.setProduct_status(getProductStatus());
                                model.setMeet_to("[" + itemId + "]");
                                model.setCreatedAt(getCurrentDate());
                                model.setBrand(getBrand());
                                model.setType(geteType());
                                model.setSerial_number(geteSerialNumber());
                                model.setCondition(geteCondition());
                                model.setPhoto_1(photo_1);
                                model.setPhoto_2(photo_2);
                                model.setPhoto_3(photo_3);

                                Log.d("TAG", "BAND " + getBrand());
                                Log.d("TAG", "P_1 " + getPhoto_1());
                                Log.d("TAG", "P_2 " + getPhoto_2());
                                Log.d("TAG", "P_3 " + getPhoto_3());

                                showLoadingDialog();

                                presenter.setupEdit(ApiParam.API_062, model);

                            } else {
                                MeeToSelectDao dao = new MeeToSelectDao(context);
                                List<ContactModel> check = dao.getFullName();
                                String item = "";
                                String itemId = "";
                                for (int i = 0; i < check.size(); i++) {
                                    item = item + check.get(i).getFullname();
                                    itemId = itemId + check.get(i).getId();
                                    if (i != check.size() - 1) {
                                        item = item + ", ";
                                        itemId = itemId + ", ";
                                    }
                                }

                                ProductDao productDao = new ProductDao(context);
                                List<Product> brand = productDao.getProduct();
                                String bran = "";
                                String type = "";
                                String serialNumber = "";
                                String condition = "";
                                for (int i = 0; i < brand.size(); i++) {
                                    bran = bran + brand.get(i).getBrand();
                                    type = type + brand.get(i).getType();
                                    serialNumber = serialNumber + brand.get(i).getSerialNumber();
                                    condition = condition + brand.get(i).getCondition();
                                    if (i != brand.size() - 1) {
                                        bran = bran + "\",\"";
                                        type = type + "\",\"";
                                        serialNumber = serialNumber + "\",\"";
                                        condition = condition + "\",\"";
                                    }
                                }


                                Log.d("TAG", "Meet to " + "[" + itemId + "]");

                                ActivityEventModel aeventModel = new ActivityEventModel();
                                aeventModel.setAccountId(account_id);
                                aeventModel.setAssignTo(assign_id);
                                aeventModel.setName(etEventName.getText().toString());
                                aeventModel.setDate(tvCloseDate.getText().toString());
                                aeventModel.setDescription(etDesc.getText().toString());
                                aeventModel.setFollowUp(etFollowUp.getText().toString());
                                aeventModel.setMeet_to("[" + itemId + "]");
                                aeventModel.setCreatedAt(getCurrentDate());
                                aeventModel.setBrand(getBrand());
                                aeventModel.setType(geteType());
                                aeventModel.setSerial_number(geteSerialNumber());
                                aeventModel.setCondition(geteCondition());
                                aeventModel.setPhoto_1(photo_1);
                                aeventModel.setPhoto_2(photo_2);
                                aeventModel.setPhoto_3(photo_3);

                                showLoadingDialog();

                                presenter.setupAdd(ApiParam.API_061, aeventModel);
                            }
                        } else {

                        }
                    }
                    break;
                case R.id.tvAccountName:
                    AccountDialogFragment accountDialogFragment = new AccountDialogFragment(AEventAddActivity.this, new AccountDialogCallback() {

                        @Override
                        public void onAccountDialogCallback(Bundle data) {
                            tvAccountName.setText(data.getString("name"));
                            contact_json = data.getString("pic");
                            account_id = data.getString("id");
                        }
                    });
                    accountDialogFragment.show(getSupportFragmentManager(), "accountDialog");
                    Log.d("TAG", "acc_name" + tvAccountName.getText().toString());
                    break;
                case R.id.ivAccountName:
                    AccountDialogFragment DialogFragment = new AccountDialogFragment(AEventAddActivity.this, new AccountDialogCallback() {

                        @Override
                        public void onAccountDialogCallback(Bundle data) {
                            tvAccountName.setText(data.getString("name"));
                            contact_json = data.getString("pic");
                            account_id = data.getString("id");

                            Log.d("TAG", "Contact JSON OOO : " + contact_json);
                        }
                    });
                    DialogFragment.show(getSupportFragmentManager(), "accountDialog");
                    break;
                case R.id.tvCloseDate:
                    datepicker.setDateFragment(year, month, day);
                    datepicker.show(getFragmentManager(), "datePicker");
                    break;
                case R.id.tvAssignTo:
                    UserDialogFragment userFragment = new UserDialogFragment(AEventAddActivity.this, new UserDialogCallback() {

                        @Override
                        public void onUserDialogCallback(Bundle data) {
                            tvAssignTo.setText(data.getString("name"));
                            assign_id = data.getString("id");
                        }
                    });
                    userFragment.show(getSupportFragmentManager(), "UserFragment");
                    break;
                case R.id.foto1:
                    selectImage1();
                    break;
                case R.id.foto2:
                    selectImage2();
                    break;
                case R.id.foto3:
                    selectImage3();
                    break;
                case R.id.tvMeetTo:
                    if (account_id.equals("")) {
                        tvAccountName.setError("Nama Akun wajib diisi");
                    } else {
                        Intent intent = new Intent(context, MeetTo_activity.class);
                        intent.putExtra("account_id", account_id);
                        startActivity(intent);
                    }
                    break;
                case R.id.ProductAdd:
                    Intent i = new Intent(context, ExistingProductAddActivity.class);
                    startActivityForResult(i, Sales1CRMUtils.PRODUCT_LIST);
                    break;
            }
        }
    };

    private boolean checkField() {
        if (account_id.equalsIgnoreCase("")) {
            tvAccountName.setError("Nama Akun wajib diisi");
            return false;
        } else if (etEventName.getText().toString().equalsIgnoreCase("")) {
            etEventName.setError("Nama Event Harus Diisi !!!");
            return false;
        } else if (tvCloseDate.getText().toString().equalsIgnoreCase("")) {
            tvCloseDate.setError("the Closing Date cannot be Empty !!!");
            return false;
        }
        return true;
    }


    DatePickerDialog.OnDateSetListener dateListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int r_year, int monthOfYear,
                              int dayOfMonth) {
            year = r_year;
            month = monthOfYear + 1;
            day = dayOfMonth;

            string_year = r_year + "";

            if (month < 10) {

                string_month = "0" + month;
            } else {
                string_month = month + "";
            }
            if (day < 10) {

                string_day = "0" + day;
            } else {
                string_day = day + "";
            }
            open_date = year + "-" + string_month + "-" + string_day;

            tvCloseDate.setText(year + "-" + string_month + "-" + string_day);
        }
    };


    @Override
    public void finishAdd(String result, String response) {
        dismissLoadingDialog();
        if (result.equalsIgnoreCase("OK")) {
            finish();
            ProductDao productDao = new ProductDao(context);
            productDao.deleteAllRecord();

            MeeToSelectDao meeToSelectDao = new MeeToSelectDao(context);
            meeToSelectDao.deleteAllRecord();
        }
    }

    @Override
    public void finishEdit(String result, String response) {
        dismissLoadingDialog();
        if (result.equalsIgnoreCase("OK")) {
            finish();
            AEventDetailActivity.getInstance().finish();
            ProductDao productDao = new ProductDao(context);
            productDao.deleteAllRecord();

            MeeToSelectDao meeToSelectDao = new MeeToSelectDao(context);
            meeToSelectDao.deleteAllRecord();
        }
    }

    private String getCurrentDate() {
        Calendar c = Calendar.getInstance();

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        return df.format(c.getTime());
    }

    private String getTimeStamp() {
        Calendar cd = Calendar.getInstance();
        SimpleDateFormat s = new SimpleDateFormat("yyyyMMdd_HHmmss");
        return s.format(cd.getTime());

    }

    private void setProductList() {
        ProductDao dao = new ProductDao(new DBHelper(context), true);
        productList = dao.getProductList();
        Log.d("TAG", "list " + productList);

        simpleProductAdapter = new SimpleProductAdapter(context, this.productList);
        simpleProductAdapter.notifyDataSetChanged();
        lvProductSave.setAdapter(simpleProductAdapter);
        lvProductSave.setSelector(R.drawable.transparent_selector);
        lvProductSave.setOverScrollMode(View.OVER_SCROLL_NEVER);

        lvProductSave.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });
    }

    private String getIdProductEvent() {
        ProductDao productDao = new ProductDao(context);
        List<Product> brand = productDao.getProduct();
        String idProduct = "";
        for (int i = 0; i < brand.size(); i++) {
            idProduct = idProduct + brand.get(i).getId();
            if (i != brand.size() - 1) {
                idProduct = idProduct + ",";
            }
        }
        if (brand.size() <= 0) {
            eIdProduct = "";
        } else {
            eIdProduct = "[" + idProduct + "]";
        }
        return eIdProduct;
    }

    private String getBrand() {
        ProductDao productDao = new ProductDao(context);
        List<Product> brand = productDao.getProduct();
        String bran = "";
        String type = "";
        String serialNumber = "";
        String condition = "";
        for (int i = 0; i < brand.size(); i++) {
            bran = bran + brand.get(i).getBrand();
            type = type + brand.get(i).getType();
            serialNumber = serialNumber + brand.get(i).getSerialNumber();
            condition = condition + brand.get(i).getCondition();
            if (i != brand.size() - 1) {
                bran = bran + "\",\"";
                type = type + "\",\"";
                serialNumber = serialNumber + "\",\"";
                condition = condition + "\",\"";
            }
        }
        if (brand.size() <= 0) {
            eBrand = "";
        } else {
            eBrand = "[" + "\"" + bran + "\"" + "]";
        }
        return eBrand;
    }

    private String geteType() {
        ProductDao productDao = new ProductDao(context);
        List<Product> brand = productDao.getProduct();
        String bran = "";
        String type = "";
        String serialNumber = "";
        String condition = "";
        for (int i = 0; i < brand.size(); i++) {
            bran = bran + brand.get(i).getBrand();
            type = type + brand.get(i).getType();
            serialNumber = serialNumber + brand.get(i).getSerialNumber();
            condition = condition + brand.get(i).getCondition();
            if (i != brand.size() - 1) {
                bran = bran + "\",\"";
                type = type + "\",\"";
                serialNumber = serialNumber + "\",\"";
                condition = condition + "\",\"";
            }
        }
        if (brand.size() <= 0) {
            eType = "";
        } else {
            eType = "[" + "\"" + type + "\"" + "]";
        }
        return eType;
    }

    private String geteSerialNumber() {
        ProductDao productDao = new ProductDao(context);
        List<Product> brand = productDao.getProduct();
        String bran = "";
        String type = "";
        String serialNumber = "";
        String condition = "";
        for (int i = 0; i < brand.size(); i++) {
            bran = bran + brand.get(i).getBrand();
            type = type + brand.get(i).getType();
            serialNumber = serialNumber + brand.get(i).getSerialNumber();
            condition = condition + brand.get(i).getCondition();
            if (i != brand.size() - 1) {
                bran = bran + "\",\"";
                type = type + "\",\"";
                serialNumber = serialNumber + "\",\"";
                condition = condition + "\",\"";
            }
        }
        if (brand.size() <= 0) {
            eSerial = "";
        } else {
            eSerial = "[" + "\"" + serialNumber + "\"" + "]";
        }
        return eSerial;
    }

    private String geteCondition() {
        ProductDao productDao = new ProductDao(context);
        List<Product> brand = productDao.getProduct();
        String bran = "";
        String type = "";
        String serialNumber = "";
        String condition = "";
        for (int i = 0; i < brand.size(); i++) {
            bran = bran + brand.get(i).getBrand();
            type = type + brand.get(i).getType();
            serialNumber = serialNumber + brand.get(i).getSerialNumber();
            condition = condition + brand.get(i).getCondition();
            if (i != brand.size() - 1) {
                bran = bran + "\",\"";
                type = type + "\",\"";
                serialNumber = serialNumber + "\",\"";
                condition = condition + "\",\"";
            }
        }
        if (brand.size() <= 0) {
            eCondition = "";
        } else {
            eCondition = "[" + "\"" + condition + "\"" + "]";
        }
        return eCondition;
    }

    private String getProductStatus() {
        ProductDao productDao = new ProductDao(context);
        List<Product> brand = productDao.getProduct();
        String productStatus = "";
        for (int i = 0; i < brand.size(); i++) {
            productStatus = productStatus + brand.get(i).getStatus();
            if (i != brand.size() - 1) {
                productStatus = productStatus + "\",\"";
            }
        }
        if (brand.size() <= 0) {
            eProductStatus = "";
        } else {
            eProductStatus = "[" + "\"" + productStatus + "\"" + "]";
        }
        return eProductStatus;
    }

    private String getPhoto_1() {
        if (!photo_1.equalsIgnoreCase("")) {
            p_1 = photo_1;
        } else {
            p_1 = "";
        }
        return p_1;
    }

    private String getPhoto_2() {
        if (!photo_2.equalsIgnoreCase("")) {
            p_2 = photo_2;
        } else {
            p_2 = "";
        }
        return p_2;
    }

    private String getPhoto_3() {
        if (!photo_3.equalsIgnoreCase("")) {
            p_3 = photo_3;
        } else {
            p_3 = "";
        }
        return p_3;
    }

}
