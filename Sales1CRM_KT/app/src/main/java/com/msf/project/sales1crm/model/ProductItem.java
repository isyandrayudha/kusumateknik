package com.msf.project.sales1crm.model;

public class ProductItem{
	private String quantity;
	private String categoryName;
	private String showPrice;
	private String productCode;
	private String productName;
	private String showStock;
	private String productGroupId;
	private String descriptionProduct;
	private String sTATUS;
	private String deletedDate;
	private String unit;
	private String opportunityId;
	private String categoryId;
	private String branchId;
	private String price;
	private String branchName;
	private String id;
	private String createdDate;
	private String updatedDate;
	private String stock;
	private String productDetailId;

	public void setQuantity(String quantity){
		this.quantity = quantity;
	}

	public String getQuantity(){
		return quantity;
	}

	public void setCategoryName(String categoryName){
		this.categoryName = categoryName;
	}

	public String getCategoryName(){
		return categoryName;
	}

	public void setShowPrice(String showPrice){
		this.showPrice = showPrice;
	}

	public String getShowPrice(){
		return showPrice;
	}

	public void setProductCode(String productCode){
		this.productCode = productCode;
	}

	public String getProductCode(){
		return productCode;
	}

	public void setProductName(String productName){
		this.productName = productName;
	}

	public String getProductName(){
		return productName;
	}

	public void setShowStock(String showStock){
		this.showStock = showStock;
	}

	public String getShowStock(){
		return showStock;
	}

	public void setProductGroupId(String productGroupId){
		this.productGroupId = productGroupId;
	}

	public String getProductGroupId(){
		return productGroupId;
	}

	public void setDescriptionProduct(String descriptionProduct){
		this.descriptionProduct = descriptionProduct;
	}

	public String getDescriptionProduct(){
		return descriptionProduct;
	}

	public void setSTATUS(String sTATUS){
		this.sTATUS = sTATUS;
	}

	public String getSTATUS(){
		return sTATUS;
	}

	public void setDeletedDate(String deletedDate){
		this.deletedDate = deletedDate;
	}

	public String getDeletedDate(){
		return deletedDate;
	}

	public void setUnit(String unit){
		this.unit = unit;
	}

	public String getUnit(){
		return unit;
	}

	public void setOpportunityId(String opportunityId){
		this.opportunityId = opportunityId;
	}

	public String getOpportunityId(){
		return opportunityId;
	}

	public void setCategoryId(String categoryId){
		this.categoryId = categoryId;
	}

	public String getCategoryId(){
		return categoryId;
	}

	public void setBranchId(String branchId){
		this.branchId = branchId;
	}

	public String getBranchId(){
		return branchId;
	}

	public void setPrice(String price){
		this.price = price;
	}

	public String getPrice(){
		return price;
	}

	public void setBranchName(String branchName){
		this.branchName = branchName;
	}

	public String getBranchName(){
		return branchName;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setCreatedDate(String createdDate){
		this.createdDate = createdDate;
	}

	public String getCreatedDate(){
		return createdDate;
	}

	public void setUpdatedDate(String updatedDate){
		this.updatedDate = updatedDate;
	}

	public String getUpdatedDate(){
		return updatedDate;
	}

	public void setStock(String stock){
		this.stock = stock;
	}

	public String getStock(){
		return stock;
	}

	public void setProductDetailId(String productDetailId){
		this.productDetailId = productDetailId;
	}

	public String getProductDetailId(){
		return productDetailId;
	}

	@Override
 	public String toString(){
		return 
			"ProductItem{" + 
			"quantity = '" + quantity + '\'' + 
			",category_name = '" + categoryName + '\'' + 
			",show_price = '" + showPrice + '\'' + 
			",product_code = '" + productCode + '\'' + 
			",product_name = '" + productName + '\'' + 
			",show_stock = '" + showStock + '\'' + 
			",product_group_id = '" + productGroupId + '\'' + 
			",description_product = '" + descriptionProduct + '\'' + 
			",sTATUS = '" + sTATUS + '\'' + 
			",deleted_date = '" + deletedDate + '\'' + 
			",unit = '" + unit + '\'' + 
			",opportunity_id = '" + opportunityId + '\'' + 
			",category_id = '" + categoryId + '\'' + 
			",branch_id = '" + branchId + '\'' + 
			",price = '" + price + '\'' + 
			",branch_name = '" + branchName + '\'' + 
			",id = '" + id + '\'' + 
			",created_date = '" + createdDate + '\'' + 
			",updated_date = '" + updatedDate + '\'' + 
			",stock = '" + stock + '\'' + 
			",product_detail_id = '" + productDetailId + '\'' + 
			"}";
		}
}
