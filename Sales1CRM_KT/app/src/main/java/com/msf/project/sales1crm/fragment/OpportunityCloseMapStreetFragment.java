package com.msf.project.sales1crm.fragment;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.github.clans.fab.FloatingActionButton;
import com.mapbox.android.core.location.LocationEngine;
import com.mapbox.android.core.location.LocationEngineListener;
import com.mapbox.android.core.location.LocationEnginePriority;
import com.mapbox.android.core.location.LocationEngineProvider;
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.plugins.locationlayer.LocationLayerPlugin;
import com.mapbox.mapboxsdk.plugins.locationlayer.modes.CameraMode;
import com.mapbox.mapboxsdk.plugins.locationlayer.modes.RenderMode;
import com.msf.project.sales1crm.OpportunityDetailActivity;
import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.callback.OpportunityListCloseCallback;
import com.msf.project.sales1crm.customview.CustomEditText;
import com.msf.project.sales1crm.model.OpportunityModel;
import com.msf.project.sales1crm.presenter.OpportunityListClosePresenter;
import com.msf.project.sales1crm.service.GPSTracker;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class OpportunityCloseMapStreetFragment extends android.support.v4.app.Fragment implements OnMapReadyCallback, LocationEngineListener,
        PermissionsListener, MapboxMap.OnMapClickListener, OpportunityListCloseCallback {


    @BindView(R.id.etSearch)
    CustomEditText etSearch;
    @BindView(R.id.ivClear)
    ImageView ivClear;
    @BindView(R.id.rlSearch)
    RelativeLayout rlSearch;

    @BindView(R.id.faMyLocation)
    FloatingActionButton faMyLocation;
    Unbinder unbinder;
    private MapView mapView;
    private MapboxMap mMapboxMap;
    private Context context;
    private OpportunityListClosePresenter opportunityListClosePresenter;
    private PermissionsManager permissionsManager;
    private LocationEngine locationEngine;
    private LocationLayerPlugin locationLayerPlugin;
    private Location originLocatin;
    private List<OpportunityModel> list = new ArrayList<>();
    private String stringLatitude, stringLongitude;


    private int prevSize = 0, sort_id = 0;
    private boolean isLoadMore = true;
    private boolean isMaxSize = true;
    private boolean pause = false;


    public static OpportunityCloseMapStreetFragment newInstance() {
        OpportunityCloseMapStreetFragment fragment = new OpportunityCloseMapStreetFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_opportunity_mapclose_street, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeThisFragment(savedInstanceState);
        this.context = getActivity();
        opportunityListClosePresenter = new OpportunityListClosePresenter(context,this);
        getDataFromAPI(0,sort_id);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("opportunity");

    }

    private void initializeThisFragment(Bundle savedInsanceState) {
        Mapbox.getInstance(getActivity(),getString(R.string.key_access_token_development));
        mapView = getView().findViewById(R.id.mapView);
        mapView.onCreate(savedInsanceState);
        mapView.setStyleUrl(getResources().getString(R.string.layer_streets) + "?auth=" + getResources().getString(R.string.key_access_token_development));
        mapView.getMapAsync(this);
    }

    private void getDataFromAPI(int prev, int sort_id) {
        isMaxSize = true;
        isLoadMore = true;

        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(context)) {
            GPSTracker gpsTracker = new GPSTracker(context);
            if (gpsTracker.canGetLocation()) {
                opportunityListClosePresenter.setupList(ApiParam.API_043,prev,sort_id, etSearch.getText().toString());
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gpsTracker.showSettingsAlert();
            }
        }
    }

    @Override
    public void onMapReady(MapboxMap mapboxMap) {
        mMapboxMap = mapboxMap;
        mapboxMap.addOnMapClickListener(this);
        faMyLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mapboxMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(originLocatin.getLatitude()
                        ,originLocatin.getLongitude()),12.0));
            }
        });
        enabledLocation();

    }

    private void enabledLocation() {
        if (PermissionsManager.areLocationPermissionsGranted(getActivity())) {
            intializedLocationEngine();
            initializedLocationLayer();
        } else  {
            permissionsManager = new PermissionsManager(this);
            permissionsManager.requestLocationPermissions(getActivity());
        }
    }


    @SuppressWarnings("MissingPermission")
    private void intializedLocationEngine() {
        locationEngine = new LocationEngineProvider(getActivity()).obtainBestLocationEngineAvailable();
        locationEngine.setPriority(LocationEnginePriority.HIGH_ACCURACY);
        locationEngine.activate();

        Location lastLocation = locationEngine.getLastLocation();
        if(lastLocation != null) {
            originLocatin = lastLocation;

        } else {
            locationEngine.addLocationEngineListener(this);
        }
    }

    @SuppressWarnings("MissingPermission")
    private void initializedLocationLayer() {
        locationLayerPlugin = new LocationLayerPlugin(mapView, mMapboxMap, locationEngine);
        locationLayerPlugin.setLocationLayerEnabled(true);
        locationLayerPlugin.setCameraMode(CameraMode.TRACKING);
        locationLayerPlugin.setRenderMode(RenderMode.NORMAL);
    }

    private void setCameraPosition(Location location) {
        mMapboxMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude()
        ,location.getLongitude()),8.0));
    }


    @Override
    @SuppressWarnings("MissingPermission")
    public void onConnected() {
    locationEngine.requestLocationUpdates();
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location !=null){
            originLocatin = location;
            setCameraPosition(location);
        }

    }

    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {

    }

    @Override
    public void onPermissionResult(boolean granted) {
        if (granted) {
            enabledLocation();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onMapClick(@NonNull LatLng point) {

    }



    @Override
    public void finishOpporList(String result, List<OpportunityModel> list) {
//        Log.i("LISTCLOSE", list.get(0).getOppotunityBilling().getAlamat());
        mapView.getMapAsync((MapboxMap mapboxMap) -> {
            mapboxMap.setMaxZoomPreference(18);
            LatLng[] latLngs = new LatLng[list.size()];
            OpportunityModel[] opportunityModels = new OpportunityModel[list.size()];
            for (int i=0; i < list.size(); i++) {
               try{
                   JSONObject billingObject = new JSONObject(list.get(i).getAddress_billing());
                   Log.i("count", String.valueOf(billingObject.length()));
                   latLngs[i] = new LatLng(Double.parseDouble(billingObject.getString("latitude")),
                           Double.parseDouble(billingObject.getString("longitude")));
                   opportunityModels[i] = list.get(i);
                   mapboxMap.addMarker(new MarkerOptions()
                           .position(latLngs[i])
                           .title(list.get(i).getName())
                           .snippet(billingObject.getString("alamat")));
               }catch (Exception e){
                   e.printStackTrace();
               }
//               mapboxMap.setOnMarkerClickListener(new MapboxMap.OnMarkerClickListener() {
//                   @Override
//                   public boolean onMarkerClick(@NonNull Marker marker) {
//                       for(int i = 0; i < list.size(); i++) {
//                           if (marker.getPosition().equals(latLngs[i])) {
//                               Intent intent = new Intent(context, OpportunityDetailActivity.class);
//                               intent.putExtra("opportunityModel",opportunityModels[i]);
//                               startActivity(intent);
//                           }
//                       }
//                       return false;
//                   }
//               });
                mapboxMap.setOnInfoWindowClickListener(new MapboxMap.OnInfoWindowClickListener() {
                    @Override
                    public boolean onInfoWindowClick(@NonNull Marker marker) {
                            for (int i = 0; i < list.size(); i++) {
                                if (marker.getPosition().equals(latLngs[i])) {
                                    Intent intent = new Intent(context, OpportunityDetailActivity.class);
                                    intent.putExtra("opportunityModel", opportunityModels[i]);
                                    startActivity(intent);
                                }
                            }
                            return false;

                    }
                });
            }
        });

    }

    @Override
    public void finishOpporMoreList(String result, List<OpportunityModel> list) {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mapView.onDestroy();
        unbinder.unbind();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }
}
