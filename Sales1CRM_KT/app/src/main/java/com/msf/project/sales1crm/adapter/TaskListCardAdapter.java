package com.msf.project.sales1crm.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.TaskDetailActivity;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.model.TaskModel;

import java.util.List;

public class TaskListCardAdapter extends RecyclerView.Adapter<TaskListCardAdapter.TaskListCardHolder> {
    private Context context;
    private Bitmap icon;
    private String url;
    private List<TaskModel> listData;

    public TaskListCardAdapter(Context context, List<TaskModel> objects) {
        this.context = context;
        this.listData = objects;
    }

    public static abstract class Row {
    }

    // This is for making Section, ex: A B C D etc
    public static final class Section extends TaskListCardAdapter.Row {
        public final String text;

        public Section(String text) {
            this.text = text;
        }
    }

    // This is for making Row, ex: A B C D etc
    public static final class Item extends TaskListCardAdapter.Row {
        public final TaskModel text;

        public Item(TaskModel text) {
            this.text = text;
        }

        public TaskModel getItem() {
            return text;
        }
    }

    private List<TaskListCardAdapter.Row> rows;

    public void setRows(List<TaskListCardAdapter.Row> rows) {
        this.rows = rows;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return rows.size();
    }

    @Override
    public TaskListCardAdapter.TaskListCardHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context)
                .inflate(R.layout.card_task, null);
        return new TaskListCardAdapter.TaskListCardHolder(v);
    }

    @Override
    public void onBindViewHolder(TaskListCardAdapter.TaskListCardHolder holder, int position) {
        final TaskListCardAdapter.Item item = (TaskListCardAdapter.Item) rows.get(position);
        holder.tvTaskName.setText(item.text.getSubject());
        holder.tvAssignTo.setText(item.text.getAssignto_name());
        holder.tvDescription.setText(item.text.getDescription());
        holder.tvDate.setText(item.text.getDate());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, TaskDetailActivity.class);
                intent.putExtra("taskModel", item.getItem());
                context.startActivity(intent);
            }
        });
    }

    public class TaskListCardHolder extends RecyclerView.ViewHolder {
        CustomTextView tvTaskName,
                tvAssignTo,
                tvDescription,
                tvDate;
        public TaskListCardHolder(View itemView) {
            super(itemView);
            tvTaskName = (CustomTextView) itemView.findViewById(R.id.tvTaskName);
            tvAssignTo = (CustomTextView) itemView.findViewById(R.id.tvAssignTo);
            tvDescription = (CustomTextView) itemView.findViewById(R.id.tvDescription);
            tvDate = (CustomTextView) itemView.findViewById(R.id.tvDate);
        }
    }
}
