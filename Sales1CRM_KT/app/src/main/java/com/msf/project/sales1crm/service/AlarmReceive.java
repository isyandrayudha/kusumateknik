package com.msf.project.sales1crm.service;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.PowerManager;
import android.util.Log;

import com.msf.project.sales1crm.callback.LocationUserCallback;
import com.msf.project.sales1crm.database.LocationUserDao;
import com.msf.project.sales1crm.model.LocationUserModel;
import com.msf.project.sales1crm.presenter.LocationUserPresenter;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.NetworkChecker;
import com.msf.project.sales1crm.utility.PreferenceUtility;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class AlarmReceive extends BroadcastReceiver implements LocationListener, LocationUserCallback {

    public static final String BROADCAST_ACTION = "Alarm Service";
    private Context context;
    private LocationManager locationManager;
    private LocationUserPresenter presenter;
    private LocationListener listener;
    private String latitude;
    private String longitude;

    private String alamat;
    private String dateNow;
    private LocationUserModel model;

    private LocationUserDao dao;

    Location location;


    @SuppressLint("NewApi")
    @Override
    public void onReceive(Context context, Intent intent) {
        this.context = context;
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "");
        wl.acquire();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        PreferenceUtility.today_date = dateFormat.format(Calendar.getInstance().getTime());
        dateNow = PreferenceUtility.today_date;
        Log.i("TESTT", "date : " + PreferenceUtility.today_date);
        presenter = new LocationUserPresenter(context, this);
        dao = new LocationUserDao(context);

//        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
//        Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
////        if(location == null) {
////            location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
//            Geocoder geocoder;
//            geocoder = new Geocoder(context,Locale.getDefault());
//          try {
//              List<Address> addresses ;
//              addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
//              if (addresses != null && addresses.size() > 0) {
//                  alamat = addresses.get(0).getAddressLine(0);
//                  latitude = String.valueOf( addresses.get(0).getLatitude());
//                  longitude = String.valueOf(addresses.get(0).getLongitude());
//              }
//          } catch (IOException e){
//              e.printStackTrace();
//          }
//          }

////
//
        GPSTracker gpsTracker = new GPSTracker(context);
        Geocoder geocoder;
        geocoder = new Geocoder(context, Locale.getDefault());
        latitude = String.valueOf(gpsTracker.getLatitude());
        longitude = String.valueOf(gpsTracker.getLongitude());
        Log.d("TAG","LAT = " + latitude);
        Log.d("TAG","LNG = " + longitude);
        try{
            List<Address> addresses;
            addresses = geocoder.getFromLocation(gpsTracker.getLatitude(),gpsTracker.getLongitude(),1);
            if(addresses != null && addresses.size() > 0){
                alamat = addresses.get(0).getAddressLine(0);
            } else if ( alamat.isEmpty()) {
                alamat = "-";
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


//        List<String> providers = locationManager.getProviders(true);
//        for (String provider : providers) {
//            Location l = locationManager.getLastKnownLocation(provider);
//
////            Log.i("TAKI-TAKI", "LATITUDE :" + latitude);
//            Geocoder geocoder;
//            geocoder = new Geocoder(context, Locale.getDefault());
//            try {
//                List<Address> addresses;
//                addresses = geocoder.getFromLocation(l.getLatitude(),l.getLongitude(), 1);
//                Log.d("TAG","tes " + addresses);
//                Log.d("TAG","tes " + l.getLatitude());
//                if (addresses != null && addresses.size() > 0) {
//                    alamat = addresses.get(0).getAddressLine(0);
//                 latitude = String.valueOf(addresses.get(0).getLatitude());
//                 longitude = String.valueOf(addresses.get(0).getLongitude());
//                }
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//
//        }


        Log.i("TEST" , "latitude : " + latitude
                + ", longitude : " + longitude
                + ", date : " + PreferenceUtility.today_date
                + ", address : " + alamat);


        if (NetworkChecker.isNetworkConnected(context)) {
            LocationUserModel locationUserModel = new LocationUserModel();
            locationUserModel.setLatitude(String.valueOf(gpsTracker.getLatitude()));
            locationUserModel.setLongitude(String.valueOf(gpsTracker.getLongitude()));
            locationUserModel.setAddress(alamat);
            locationUserModel.setDate(PreferenceUtility.today_date);

            presenter.saveData(ApiParam.API_115,locationUserModel);

        } else {
            LocationUserModel locationUserModel = new LocationUserModel();
            locationUserModel.setLatitude(latitude);
            locationUserModel.setLongitude(longitude);
            locationUserModel.setAddress(alamat);
            locationUserModel.setDate(dateNow);
            dao.insertTable(locationUserModel);
        }

        if(NetworkChecker.isNetworkConnected(context)) {
            LocationUserDao dao = new LocationUserDao(context);
            for (LocationUserModel model : dao.getLocationUserList()) {
                if (model.getAddress() != null) {
                    presenter.saveData(ApiParam.API_115, model);
                }
            }
        }

    }



    public void SetAlarmForGeoService(Context context) {
        Log.i("TAG", "Start Alarm");
        PreferenceUtility.service_running = true;
        AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent i = new Intent(context, LocationSendService.class);
        i.putExtra("send_Location", true);
        PendingIntent pi = PendingIntent.getBroadcast(context,
                PreferenceUtility.PENDINGINTENT_LOCATION, i, PendingIntent.FLAG_CANCEL_CURRENT);
        am.cancel(pi);
        am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(),
                1000 * 60 * 3, pi);
    }


    public void stopSendTaskData(Context context) {
        Intent intent = new Intent(context, LocationSendService.class);
        PendingIntent pendingIntent = PendingIntent.getService(context,
                PreferenceUtility.PENDINGINTENT_LOCATION,intent,PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager am = (AlarmManager) context
                .getSystemService(Context.ALARM_SERVICE);
        am.cancel(pendingIntent);
        am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(),
                1000 * 60 * 3, pendingIntent);
    }

    @Override
    public void onLocationChanged(Location location) {
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

        Log.i("GPS enabled", "GPS ENABLED");

    }

    @Override
    public void finishSave(String result, String response, String id) {
        if (result.equalsIgnoreCase("OK")) {
            dao.delete(id);
        }
    }

}
