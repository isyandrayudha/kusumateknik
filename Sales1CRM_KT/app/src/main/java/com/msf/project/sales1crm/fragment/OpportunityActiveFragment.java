package com.msf.project.sales1crm.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.msf.project.sales1crm.ErrorAPIKeyActivity;
import com.msf.project.sales1crm.MenuActivity;
import com.msf.project.sales1crm.OpportunityAddActivity;
import com.msf.project.sales1crm.OpportunityDetailActivity;
import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.adapter.OpportunityListAdapter;
import com.msf.project.sales1crm.callback.DashboardCallback;
import com.msf.project.sales1crm.callback.OpportunityListActiveCallback;
import com.msf.project.sales1crm.callback.SortDialogCallback;
import com.msf.project.sales1crm.customview.CustomEditText;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.imageloader.ImageLoader;
import com.msf.project.sales1crm.model.AbsenceModel;
import com.msf.project.sales1crm.model.DashboardFirstModel;
import com.msf.project.sales1crm.model.DashboardSecondModel;
import com.msf.project.sales1crm.model.DashboardThirdModel;
import com.msf.project.sales1crm.model.OpportunityModel;
import com.msf.project.sales1crm.model.TotalOpportunityModel;
import com.msf.project.sales1crm.presenter.DashboardPresenter;
import com.msf.project.sales1crm.presenter.OpportunityListActivePresenter;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by christianmacbook on 06/06/18.
 */

public class OpportunityActiveFragment extends Fragment implements OpportunityListActiveCallback, OpportunityTabFragment.ClickActionFilterListener, DashboardCallback {

    @BindView(R.id.rlNoConnection)
    RelativeLayout rlNoConnection;

    @BindView(R.id.tvRefresh)
    CustomTextView tvRefresh;

    @BindView(R.id.etSearch)
    CustomEditText etSearch;

    @BindView(R.id.ivClear)
    ImageView ivClear;

    @BindView(R.id.lvOpportunity)
    ListView lvOpportunity;

    @BindView(R.id.lvOpportunitySwipe)
    SwipeRefreshLayout lvOpportunitySwipe;

    @BindView(R.id.faOpportunityAdd)
    FloatingActionButton faOpportunityAdd;

    @BindView(R.id.llNotFound)
    LinearLayout llNotFound;
    @BindView(R.id.llshim)
    LinearLayout llshim;
    @BindView(R.id.shimmerLayout)
    ShimmerFrameLayout shimmerLayout;
    @BindView(R.id.menu)
    FloatingActionMenu menu;
    @BindView(R.id.rlLeadList)
    RelativeLayout rlLeadList;
    @BindView(R.id.ivSort)
    ImageView ivSort;
    @BindView(R.id.llsort)
    LinearLayout llsort;


    private View view;
    private Context context;
    private OpportunityListActivePresenter presenter;
    private OpportunityListAdapter adapter;
    private DashboardPresenter dashboardPresenter;
    private List<OpportunityModel> list = new ArrayList<>();
    private String[] sort = {"Name A-Z", "Name Z-A", "Opportunity Size Small - Big", "Opportunity Size Big - Small"};

    private int prevSize = 0, sort_id = 0;
    private boolean isLoadMore = true;
    private boolean isMaxSize = true;
    private boolean pause = false;

    public static OpportunityActiveFragment newInstance() {
        OpportunityActiveFragment fragment = new OpportunityActiveFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_opportunityactive, container, false);

        ButterKnife.bind(this, this.view);
        return this.view;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);
        this.context = getActivity();

        this.presenter = new OpportunityListActivePresenter(context, this);
        this.dashboardPresenter = new DashboardPresenter(context, this);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Opportunity");

        initData();
    }


    private void initData() {
        Sales1CRMUtils.Utils.imageLoader = new ImageLoader(context);
        etSearch.clearFocus();
        lvOpportunity.setOnScrollListener(new ListLazyLoad());
        lvOpportunitySwipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                llshim.setVisibility(View.VISIBLE);
                shimmerLayout.setVisibility(View.VISIBLE);
                shimmerLayout.startShimmer();
                getDataFromAPI(0, sort_id);
            }
        });

        //Set Seach Function
        setTextWatcherForSearch();
        getDataFromAPI(0, sort_id);

        ivClear.setOnClickListener(click);
        tvRefresh.setOnClickListener(click);
        llsort.setOnClickListener(click);
    }

    private void setTextWatcherForSearch() {
        etSearch.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    if (etSearch.getText().length() == 0) {
                        ivClear.setVisibility(View.VISIBLE);
                    }
                } else {
                    if (etSearch.getText().length() == 0) {
                        ivClear.setVisibility(View.GONE);
                    } else {
                        ivClear.setVisibility(View.VISIBLE);
                    }

                }
            }
        });

        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    llshim.setVisibility(View.VISIBLE);
                    shimmerLayout.setVisibility(View.VISIBLE);
                    shimmerLayout.startShimmer();
                    getDataFromAPI(0, 0);
                }

                return true;
            }
        });
    }

    public void getDataFromAPI(int prev, int sort_id) {
        isMaxSize = true;
        isLoadMore = true;

        llNotFound.setVisibility(View.GONE);

        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(context)) {
            dashboardPresenter.setupFirst(ApiParam.API_005);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    presenter.setupList(ApiParam.API_040, prev, sort_id, etSearch.getText().toString());
                }
            },1000);

        } else {
            lvOpportunitySwipe.setVisibility(View.GONE);
            rlNoConnection.setVisibility(View.VISIBLE);
            lvOpportunitySwipe.setRefreshing(false);
        }
    }


    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.ivClear:
                    etSearch.setText("");
                    etSearch.clearFocus();
                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);
                    getDataFromAPI(0, sort_id);
                    break;
                case R.id.tvRefresh:
                    llshim.setVisibility(View.VISIBLE);
                    rlNoConnection.setVisibility(View.GONE);
                    getDataFromAPI(0, sort_id);
                    break;
                case R.id.llsort:
                    SortDialogFragment DialogFragment = new SortDialogFragment(context, new SortDialogCallback() {
                        @Override
                        public void onDialogCallback(Bundle data) {
                            llshim.setVisibility(View.VISIBLE);
                            shimmerLayout.setVisibility(View.VISIBLE);
                            shimmerLayout.startShimmer();
                            sort_id = Integer.parseInt(data.getString("id"));
                            getDataFromAPI(0, sort_id);
                        }
                    }, sort);
                    DialogFragment.show(getActivity().getSupportFragmentManager(), "sortDialog");
                    break;
                default:
                    break;
            }
        }
    };

    private void setList() {
        llshim.setVisibility(View.VISIBLE);
        shimmerLayout.setVisibility(View.GONE);
        adapter = new OpportunityListAdapter(context, this.list);
        List<OpportunityListAdapter.Row> rows = new ArrayList<OpportunityListAdapter.Row>();
        for (OpportunityModel leadModel : list) {
            //Read List by Row to insert into List Adapter
            rows.add(new OpportunityListAdapter.Item(leadModel));
        }
        //Set Row Adapter
        adapter.setRows(rows);
        lvOpportunity.setAdapter(adapter);
        lvOpportunity.setOnItemClickListener(new AdapterOnItemClick());
        lvOpportunity.deferNotifyDataSetChanged();
        llshim.setVisibility(View.INVISIBLE);
        shimmerLayout.stopShimmer();
        shimmerLayout.setVisibility(View.INVISIBLE);
    }

    private void setMoreList() {
        List<OpportunityListAdapter.Row> rows = new ArrayList<OpportunityListAdapter.Row>();
        for (OpportunityModel country : list) {
            // Add the country to the list
            rows.add(new OpportunityListAdapter.Item(country));
        }
        adapter.setRows(rows);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void finishOpporList(String result, List<OpportunityModel> list) {
        Log.d("TAG", "RESPONSE" + result);

        lvOpportunitySwipe.setRefreshing(false);
//        rlProgress.setVisibility(View.GONE);
        if (result.equalsIgnoreCase("OK")) {
            prevSize = list.size();
            this.list = list;

            isMaxSize = false;
            isLoadMore = false;

            setList();
            if (list.size() > 0) {
                llNotFound.setVisibility(View.GONE);
            } else {
                llNotFound.setVisibility(View.VISIBLE);
            }
        } else if (result.equalsIgnoreCase("FAILED_KEY")) {
            startActivity(new Intent(context, ErrorAPIKeyActivity.class));
            MenuActivity.getInstance().finish();
        } else {
//            lvOpportunitySwipe.setVisibility(View.GONE);
            rlNoConnection.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void finishOpporMoreList(String result, List<OpportunityModel> list) {
        if (result.equals("OK")) {
            for (int i = 0; i < list.size(); i++) {
                this.list.add(list.get(i));
            }

            if (this.prevSize == this.list.size())
                isMaxSize = true;
            this.prevSize = this.list.size();
            setMoreList();
        }
        this.isLoadMore = false;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onClickItemFilter(int id) {
        new Handler().postDelayed(() -> getDataFromAPI(0, id), 1500);
    }

    @Override
    public void finishFirst(String result, DashboardFirstModel model) {
        if(result.equalsIgnoreCase("OK")){
            if (model.getOpportunity_create_permission().equalsIgnoreCase("1")) {
                faOpportunityAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(context, OpportunityAddActivity.class));
                    }
                });
                faOpportunityAdd.setColorNormal(R.color.BlueCRM);
                faOpportunityAdd.setColorPressed(R.color.colorPrimaryDark);
                faOpportunityAdd.setColorPressedResId(R.color.colorPrimaryDark);
                faOpportunityAdd.setColorNormalResId(R.color.BlueCRM);
            } else {
                faOpportunityAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
                    }
                });
                faOpportunityAdd.setColorNormal(R.color.BlueCRM_light);
                faOpportunityAdd.setColorPressed(R.color.colorPrimaryDark_light);
                faOpportunityAdd.setColorPressedResId(R.color.colorPrimaryDark_light);
                faOpportunityAdd.setColorNormalResId(R.color.BlueCRM_light);
            }
        }
    }

    @Override
    public void finishSecond(String result, DashboardSecondModel model) {

    }

    @Override
    public void finishThird(String result, List<DashboardThirdModel> listLead, List<DashboardThirdModel> listLead1, List<DashboardThirdModel> listOppor, List<DashboardThirdModel> listOppor1) {

    }

    @Override
    public void TotOpportunity(String result, TotalOpportunityModel totOporModel) {

    }

    @Override
    public void absence(String result, AbsenceModel model) {

    }

    private class ListLazyLoad implements AbsListView.OnScrollListener {

        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {

        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            if (firstVisibleItem + visibleItemCount == totalItemCount && totalItemCount != 0) {
                Log.d("TAG", "isLoadMore : " + isLoadMore);
                if (isLoadMore == false && isMaxSize == false) {
                    isLoadMore = true;
                    prevSize = list.size();

                    presenter.setupMoreList(ApiParam.API_040, prevSize, sort_id, etSearch.getText().toString());
                }
            }
        }
    }

    private class AdapterOnItemClick implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            // TODO Auto-generated method stub
            OpportunityListAdapter.Item item = (OpportunityListAdapter.Item) lvOpportunity.getAdapter().getItem(position);
            switch (view.getId()) {
                default:
                    Intent intent = new Intent(context, OpportunityDetailActivity.class);
                    intent.putExtra("opportunityModel", item.getItem());
                    startActivity(intent);
                    break;
            }
        }
    }

    @Override
    public void onPause() {
        pause = true;
        super.onPause();
    }

    @Override
    public void onResume() {
        if (pause) {
            getDataFromAPI(0, sort_id);
            initData();
        }
        super.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
//        shimmerLayout.stopShimmer();
    }
}
