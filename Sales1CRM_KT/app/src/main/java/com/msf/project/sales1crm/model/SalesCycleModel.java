package com.msf.project.sales1crm.model;

public class SalesCycleModel {
    private String total_oppor;
    private String total_lead;
    private String total_closewon;
    private String total_closelose;
    private String total_conversion;
    private String total_conversionrate;
    private String total_sales;
    private String avg_sales;

    public String getTotal_oppor() {
        return total_oppor;
    }

    public void setTotal_oppor(String total_oppor) {
        this.total_oppor = total_oppor;
    }

    public String getTotal_lead() {
        return total_lead;
    }

    public void setTotal_lead(String total_lead) {
        this.total_lead = total_lead;
    }

    public String getTotal_closewon() {
        return total_closewon;
    }

    public void setTotal_closewon(String total_closewon) {
        this.total_closewon = total_closewon;
    }

    public String getTotal_closelose() {
        return total_closelose;
    }

    public void setTotal_closelose(String total_closelose) {
        this.total_closelose = total_closelose;
    }

    public String getTotal_conversion() {
        return total_conversion;
    }

    public void setTotal_conversion(String total_conversion) {
        this.total_conversion = total_conversion;
    }

    public String getTotal_conversionrate() {
        return total_conversionrate;
    }

    public void setTotal_conversionrate(String total_conversionrate) {
        this.total_conversionrate = total_conversionrate;
    }

    public String getTotal_sales() {
        return total_sales;
    }

    public void setTotal_sales(String total_sales) {
        this.total_sales = total_sales;
    }

    public String getAvg_sales() {
        return avg_sales;
    }

    public void setAvg_sales(String avg_sales) {
        this.avg_sales = avg_sales;
    }
}
