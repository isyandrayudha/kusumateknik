package com.msf.project.sales1crm.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.callback.VpCallback;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.model.OpportunityModel;
import com.msf.project.sales1crm.model.VpModel;
import com.msf.project.sales1crm.presenter.VpPresenter;
import com.msf.project.sales1crm.utility.PreferenceUtility;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class Vp_OpportunityStage extends PagerAdapter {

    private Context context;
    private String url;
    private List<VpModel> list;
    private List<OpportunityModel> listData = new ArrayList<>();
    private OpportunitVPAdapter opportunityListAdapter;
    private boolean isScrolling = false;
    private int currentItems, totalItems, scrollOutItems;
    private int prevSize = 0, sort_id = 0;
    private VpPresenter vpPresenter;
    private VpCallback callback;
    private boolean isLoadMore = true;
    private boolean isMaxSize = true;

    public Vp_OpportunityStage(Context context, List<VpModel> object, List<OpportunityModel> listData) {
        this.context = context;
        this.list = object;
        this.listData = listData;
        this.url = PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.URL);
    }

    public static abstract class Row {

    }

    public static final class Section extends Row {
        public final String text;

        public Section(String text) {
            this.text = text;
        }
    }

    public static final class Item extends Row {
        public final VpModel text;

        public Item(VpModel text) {
            this.text = text;
        }

        public VpModel getText() {
            return text;
        }
    }

    private List<Row> rows;

    public void setRows(List<Row> rows) {
        this.rows = rows;
        notifyDataSetChanged();
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return super.getItemPosition(object);
    }

    @Override
    public int getCount() {
        return rows.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        this.vpPresenter = new VpPresenter(context, callback);
        final int pos = position;
        final ViewHolde holder;
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.row_tab_vp_opportunity_by_stage, container, false);
        holder = new ViewHolde();
        holder.tvRefresh = (CustomTextView) view.findViewById(R.id.tvRefresh);
        holder.rlNoConnection = (RelativeLayout) view.findViewById(R.id.rlNoConnection);
        holder.stageName = (CustomTextView) view.findViewById(R.id.stageName);
        holder.tvCount = (CustomTextView) view.findViewById(R.id.tvCount);
        holder.tvPrice = (CustomTextView) view.findViewById(R.id.tvPrice);
        holder.llNotFound = (LinearLayout) view.findViewById(R.id.llNotFound);
        holder.rcOpportunityStage = (RecyclerView) view.findViewById(R.id.rcOpportunityStage);
        holder.tvSizeData = (CustomTextView) view.findViewById(R.id.tvSizeData);

        holder.stageName.setText(list.get(pos).getStageName());
        holder.tvPrice.setText(list.get(pos).getTotal_opportunity_size());
        holder.tvSizeData.setText(list.get(pos).getTotalOpportunity());
        holder.tvCount.setText(list.get(pos).getPercentage() + "%");

        try {
            listData.clear();
            JSONArray array = new JSONArray(list.get(pos).getOpportunities());
            for (int i = 0; i < array.length(); i++) {
                OpportunityModel model = new OpportunityModel();
                model.setId(array.getJSONObject(i).getString("id"));
                model.setName(array.getJSONObject(i).getString("opportunity_name"));
                model.setClose_date(array.getJSONObject(i).getString("close_date"));
                model.setSize(array.getJSONObject(i).getString("opportunity_size"));
                model.setStage_id(array.getJSONObject(i).getString("stage_id"));
                model.setStage_name(array.getJSONObject(i).getString("stage_name"));
                model.setProgress(array.getJSONObject(i).getString("stage_percentage"));
                model.setAccount_name(array.getJSONObject(i).getString("account_name"));
                model.setComplete_ref_number(array.getJSONObject(i).getString("complete_ref_number"));
                listData.add(model);
            }

            opportunityListAdapter = new OpportunitVPAdapter(context, this.listData);
            List<OpportunitVPAdapter.Row> rows = new ArrayList<>();
            for (OpportunityModel vp : listData) {
                rows.add(new OpportunitVPAdapter.Item(vp));
            }
            LinearLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
            holder.rcOpportunityStage.setLayoutManager(layoutManager);
            opportunityListAdapter.setRows(rows);
            holder.rcOpportunityStage.setAdapter(opportunityListAdapter);
            opportunityListAdapter.notifyDataSetChanged();


            if (array.length() <= 0) {
                holder.llNotFound.setVisibility(View.VISIBLE);
            } else {
                holder.llNotFound.setVisibility(View.GONE);
            }

            Log.d("TAG", "tag" + array);

        } catch (JSONException e) {
            e.printStackTrace();
        }


        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    private static class ViewHolde {
        CustomTextView tvRefresh;
        RelativeLayout rlNoConnection;
        CustomTextView stageName;
        CustomTextView tvCount;
        CustomTextView tvPrice;
        LinearLayout llNotFound;
        RecyclerView rcOpportunityStage;
        CustomTextView tvSizeData;
    }
}
