package com.msf.project.sales1crm.firebase;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.msf.project.sales1crm.SplashActivity;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by christianmacbook on 11/05/18.
 */

public class FirebaseMessagingServices extends FirebaseMessagingService {

    private static final String TAG = FirebaseMessagingServices.class.getSimpleName();

    private FirebaseNotification firebaseNotification;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e(TAG, "From: " + remoteMessage.getFrom());

        if (remoteMessage == null)
            return;

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.e(TAG, "Notification Body: " + remoteMessage.getNotification().getBody());
            handleNotification(remoteMessage.getNotification().getBody());
        }

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "Data Payload: " + remoteMessage.getData().toString());

            try {
                JSONObject json = new JSONObject(remoteMessage.getData().toString());
                handleDataMessage(json);
            } catch (Exception e) {
                Log.e(TAG, "Exception: " + e.getMessage());
            }
        }
    }

    private void handleNotification(String message) {
        if (!FirebaseNotification.isAppIsInBackground(getApplicationContext())) {
            // app is in foreground, broadcast the push message
            Intent pushNotification = new Intent(Sales1CRMUtils.PUSH_NOTIFICATION);
            pushNotification.putExtra("message", message);
            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

            // play notification sound
            FirebaseNotification firebaseNotification = new FirebaseNotification(getApplicationContext());
            firebaseNotification.playNotificationSound();
        }else{
            // If the app is in background, firebase itself handles the notification
        }
    }

    private void handleDataMessage(JSONObject json) {
        Log.d(TAG, "push json: " + json.toString());

        try {
            JSONObject data = json.getJSONObject("data");

            String title = data.getString("title");
            String message = data.getString("message");
            boolean isBackground = data.getBoolean("is_background");
            String imageUrl = data.getString("image");
            String timestamp = data.getString("timestamp");
            int id = data.getInt("id");

            Log.d(TAG, "title: " + title);
            Log.d(TAG, "message: " + message);
            Log.d(TAG, "isBackground: " + isBackground);
            Log.d(TAG, "imageUrl: " + imageUrl);
            Log.d(TAG, "timestamp: " + timestamp);


            if (!FirebaseNotification.isAppIsInBackground(getApplicationContext())) {
                // app is in foreground, broadcast the push message
                Intent pushNotification = new Intent(Sales1CRMUtils.PUSH_NOTIFICATION);
                pushNotification.putExtra("message", message);
                LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);

                // play notification sound
                FirebaseNotification notificationUtils = new FirebaseNotification(getApplicationContext());
                notificationUtils.playNotificationSound();

                Intent intent = new Intent();
                intent.setAction(Sales1CRMUtils.KEY_REALTIMECART);
                // We should use LocalBroadcastManager when we want INTRA app
                // communication
                LocalBroadcastManager.getInstance(getBaseContext()).sendBroadcast(intent);
            } else {
                // app is in background, show the notification in notification tray
                Intent resultIntent = new Intent(getApplicationContext(), SplashActivity.class);
                resultIntent.putExtra("message", message);

                // check for image attachment
                if (TextUtils.isEmpty(imageUrl)) {
                    showNotificationMessage(getApplicationContext(), title, message, timestamp, resultIntent, id);
                } else {
                    // image is present, show notification with image
                    showNotificationMessageWithBigImage(getApplicationContext(), title, message, timestamp, resultIntent, imageUrl, id);
                }
            }
        } catch (JSONException e) {
            Log.d(TAG, "Json Exception: " + e.getMessage());
        } catch (Exception e) {
            Log.d(TAG, "Exception: " + e.getMessage());
        }
    }

    /**
     * Showing notification with text only
     */
    private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent, int id) {
        firebaseNotification = new FirebaseNotification(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        firebaseNotification.showNotificationMessage(title, message, timeStamp, intent, id);
    }

    /**
     * Showing notification with text and image
     */
    private void showNotificationMessageWithBigImage(Context context, String title, String message, String timeStamp, Intent intent, String imageUrl, int id) {
        firebaseNotification = new FirebaseNotification(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        firebaseNotification.showNotificationMessage(title, message, timeStamp, intent, imageUrl, id);
    }
}
