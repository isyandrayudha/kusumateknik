package com.msf.project.sales1crm.presenter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;

import com.msf.project.sales1crm.callback.OpportunityAddCallback;
import com.msf.project.sales1crm.model.OpportunityModel;
import com.msf.project.sales1crm.service.NetworkConnection;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.PreferenceUtility;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;
import com.msf.project.sales1crm.utility.Sales1CRMUtilsJSON;

import org.json.JSONException;
import org.json.JSONObject;

public class OpportunityAddPresenter {
    private final String TAG = OpportunityAddPresenter.class.getSimpleName();
    private Context context;
    private Messenger messenger;
    private Message message;
    private Bundle bundle;
    private Handler handler;
    private String failureResponse = "";
    private String[] stringResponse = {""};
    private OpportunityAddCallback callback;
    private String resultLogin = "NG";
    private String response = "Tolong Periksa Koneksi Anda";

    public OpportunityAddPresenter(Context context, OpportunityAddCallback listener) {
        this.context = context;
        this.callback = listener;
    }

    private void doNetworkService(String url, String params, String from) {
        Log.d(TAG, "url login " + url);
        Log.d(TAG, "params : " + params);
        Intent networkIntent = new Intent(this.context, NetworkConnection.class);
        this.messenger = new Messenger(this.handler);
        networkIntent.putExtra("messenger", this.messenger);
        networkIntent.putExtra("url", url);
        networkIntent.putExtra("params", params);
        networkIntent.putExtra("from", from);
        this.context.startService(networkIntent);
    }

    public void setupAdd(int apiIndex, OpportunityModel opportunityModel) {
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)) {
            obtainAddAccount(apiIndex, opportunityModel);
        }else{
            callback.finishAdd(this.resultLogin, this.response);
        }
    }

    public void obtainAddAccount(int apiIndex, OpportunityModel opportunityModel) {
        this.handler = new Handler(this.context.getMainLooper()) {

            @Override
            public void handleMessage(Message msg) {
                parseAddAccount(msg);
            }

        };

        JSONObject jsonObject = new JSONObject();
        try {
            //Umum
            jsonObject.put("api_key", PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.API_KEY));
            jsonObject.put("account_id", opportunityModel.getAccount_id());
            jsonObject.put("name", opportunityModel.getName());
            jsonObject.put("type", opportunityModel.getType());
            jsonObject.put("stage", opportunityModel.getStage_id());
            jsonObject.put("probability", opportunityModel.getProgress());
            jsonObject.put("opportunity_size", opportunityModel.getSize());
            jsonObject.put("next_step", opportunityModel.getNext_step());
            jsonObject.put("lead_source", opportunityModel.getSource_id());
            jsonObject.put("contact_id", opportunityModel.getPic());
            jsonObject.put("description", opportunityModel.getDescription());
            jsonObject.put("opportunity_status", opportunityModel.getOppor_status_id());
            jsonObject.put("create_date", opportunityModel.getOpen_date());
            jsonObject.put("close_date", opportunityModel.getClose_date());
            jsonObject.put("product", opportunityModel.getProduct());
            jsonObject.put("industry_id", opportunityModel.getIndustry());
            jsonObject.put("category", opportunityModel.getCategory_id());
            jsonObject.put("assign_to", opportunityModel.getAssign_to());
            jsonObject.put("reason_not_deal", opportunityModel.getReason_not_deal());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        doNetworkService(ApiParam.urlBuilder(this.context, apiIndex), jsonObject.toString(), "OpportunityAdd");
    }

    private void parseAddAccount(Message message) {
        this.message = message;
        this.bundle = this.message.getData();
        this.stringResponse[0] = this.bundle.getString("network_response");
        this.failureResponse = this.bundle.getString("network_failure");
        Log.d(TAG, "responseString[0] info " + this.stringResponse[0]);
        if (this.failureResponse.equalsIgnoreCase("yes")){
            this.callback.finishAdd(this.resultLogin, this.response);
        }else{
            try {
                this.resultLogin = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                this.callback.finishAdd(this.resultLogin, this.response);
            } catch (JSONException e) {
                Log.d(TAG, "Exception detail Response " + e.getMessage());
            }
        }
    }

    public void setupEditOpportunity(int apIndex, OpportunityModel opportunityModel){
        if(Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)){
            obtainEditOpportunity(apIndex,opportunityModel);
        } else {
            callback.finishAdd(this.resultLogin,this.response);
        }
    }

    public void obtainEditOpportunity(int apiIndex, OpportunityModel opportunityModel){
        this.handler = new Handler(this.context.getMainLooper()){
            @Override
            public void handleMessage(Message msg) {
               parseEditOpportunity(msg);
            }
        };
        JSONObject jsonObject = new JSONObject();

        try{
            jsonObject.put("api_key", PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.API_KEY));
            jsonObject.put("opportunity_id", opportunityModel.getId());
            jsonObject.put("account_id", opportunityModel.getAccount_id());
            jsonObject.put("name", opportunityModel.getName());
            jsonObject.put("type", opportunityModel.getType());
            jsonObject.put("stage", opportunityModel.getStage_id());
            jsonObject.put("probability", opportunityModel.getProgress());
            jsonObject.put("opportunity_size", opportunityModel.getSize());
            jsonObject.put("next_step", opportunityModel.getNext_step());
            jsonObject.put("lead_source", opportunityModel.getSource_id());
            jsonObject.put("contact_id", opportunityModel.getPic());
            jsonObject.put("description", opportunityModel.getDescription());
            jsonObject.put("opportunity_status", opportunityModel.getOppor_status_id());
            jsonObject.put("update_date", opportunityModel.getUpdate_date());
            jsonObject.put("close_date", opportunityModel.getClose_date());
            jsonObject.put("product", opportunityModel.getProduct());
            jsonObject.put("industry_id", opportunityModel.getIndustry());
            jsonObject.put("category", opportunityModel.getCategory_id());
            jsonObject.put("assign_to", opportunityModel.getAssign_to());
            jsonObject.put("actual_opportunity_size", opportunityModel.getActual_opportunity_size());
            jsonObject.put("final_quotation_id",opportunityModel.getFinal_quotation_id());
            jsonObject.put("reason_not_deal", opportunityModel.getReason_not_deal());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        doNetworkService(ApiParam.urlBuilder(this.context, apiIndex), jsonObject.toString(),"OpportunityEdit");
    }

    private void parseEditOpportunity(Message message){
        this.message = message;
        this.bundle = this.message.getData();
        this.stringResponse[0] = this.bundle.getString("network_response");
        this.failureResponse = this.bundle.getString("network_failure");
        Log.d(TAG, "responseString[0] info " + this.stringResponse[0]);
        Log.d(TAG,"failureResponse info " + this.failureResponse);
        if(this.failureResponse.equalsIgnoreCase("yes")){
            this.callback.finishEdit(this.resultLogin,this.response);
        }else {
            try{
                this.resultLogin = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                this.callback.finishEdit(this.resultLogin, this.response);
            } catch (JSONException e) {
                Log.d(TAG,"Exception detail response " + e.getMessage());
            }
        }
    }

//    public void setupEdit(int apiIndex, OpportunityModel opportunityModel) {
//        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)) {
//            obtainEditAccount(apiIndex, opportunityModel);
//        }else{
//            callback.finishEdit(this.resultLogin, this.response);
//        }
//    }
//
//    public void obtainEditAccount(int apiIndex, OpportunityModel opportunityModel) {
//        this.handler = new Handler(this.context.getMainLooper()) {
//
//            @Override
//            public void handleMessage(Message msg) {
//                parseEditAccount(msg);
//            }
//
//        };
//
//        JSONObject jsonObject = new JSONObject();
//        try {
//            //Umum
//            jsonObject.put("api_key", PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.API_KEY));
//            jsonObject.put("opportunity_id", opportunityModel.getId());
//            jsonObject.put("account_id", opportunityModel.getAccount_id());
//            jsonObject.put("name", opportunityModel.getName());
//            jsonObject.put("type", opportunityModel.getType());
//            jsonObject.put("stage", opportunityModel.getStage_id());
//            jsonObject.put("probability", opportunityModel.getProgress());
//            jsonObject.put("opportunity_size", opportunityModel.getSize());
//            jsonObject.put("next_step", opportunityModel.getNext_step());
//            jsonObject.put("lead_source", opportunityModel.getSource_id());
//            jsonObject.put("contact_id", opportunityModel.getPic());
//            jsonObject.put("description", opportunityModel.getDescription());
//            jsonObject.put("opportunity_status", opportunityModel.getOppor_status_id());
//            jsonObject.put("update_date", opportunityModel.getUpdate_date());
//            jsonObject.put("close_date", opportunityModel.getClose_date());
//            jsonObject.put("product", opportunityModel.getProduct());
//            jsonObject.put("industry_id", opportunityModel.getIndustry());
//            jsonObject.put("category", opportunityModel.getCategory_id());
//            jsonObject.put("assign_to", opportunityModel.getAssign_to());
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        doNetworkService(ApiParam.urlBuilder(this.context, apiIndex), jsonObject.toString(), "OpportunityEdit");
//    }
//
//    private void parseEditAccount(Message message) {
//        this.message = message;
//        this.bundle = this.message.getData();
//        this.stringResponse[0] = this.bundle.getString("network_response");
//        this.failureResponse = this.bundle.getString("network_failure");
//        Log.d(TAG, "responseString[0] info " + this.stringResponse[0]);
//        if (this.failureResponse.equalsIgnoreCase("yes")){
//            this.callback.finishEdit(this.resultLogin, this.response);
//        }else{
//            try {
//                this.resultLogin = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
//                this.callback.finishEdit(this.resultLogin, this.response);
//            } catch (JSONException e) {
//                Log.d(TAG, "Exception detail Response " + e.getMessage());
//            }
//        }
//    }
}
