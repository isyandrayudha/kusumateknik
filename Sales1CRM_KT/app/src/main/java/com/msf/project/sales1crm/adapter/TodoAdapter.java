package com.msf.project.sales1crm.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.msf.project.sales1crm.ATodoDetailActivity;
import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.model.ActivityTodoModel;

import java.util.List;


public class TodoAdapter extends RecyclerView.Adapter<TodoAdapter.TodoHolder> {



    private Context context;
    private Bitmap icon;
    private String url;
    private List<ActivityTodoModel> activityTodoModelList;

    public TodoAdapter(Context context, List<ActivityTodoModel> object) {
        this.context = context;
        this.activityTodoModelList = object;
    }

    public static abstract class Row {
        public static final class Section extends Row {
            public final String text;

            public Section(String text) {
                this.text = text;
            }
        }
    }

    public static final class Item extends Row {
        public final ActivityTodoModel text;

        public Item(ActivityTodoModel text) {
            this.text = text;
        }

        public ActivityTodoModel getItem() {
            return text;
        }
    }

    private List<Row> rows;

    public void setGaris(List<Row> rows) {
        this.rows = rows;
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return rows.size();
    }

    @NonNull
    @Override
    public TodoHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_activity_todo, null);
        return new TodoHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TodoHolder holder, int position) {
        final Item item = (Item) rows.get(position);
        holder.tvTodoName.setText(item.text.getSubject());
        holder.tvAssignTo.setText(item.text.getUser_name());
        holder.tvDescription.setText(item.text.getDescription());
        holder.tvDate.setText(item.text.getDueDate());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    Intent intent = new Intent(context, ATodoDetailActivity.class);
                    intent.putExtra("aTodoModels", item.getItem());
                    context.startActivity(intent);
            }
        });
    }


    public class TodoHolder extends RecyclerView.ViewHolder {
        CustomTextView tvTodoName;
        CustomTextView tvAssignTo;
        CustomTextView tvDescription;
        CustomTextView tvDate;


        public TodoHolder(View view) {
            super(view);
            tvTodoName = (CustomTextView) view.findViewById(R.id.tvTodoName);
            tvAssignTo = (CustomTextView) view.findViewById(R.id.tvAssignTo);
            tvDescription = (CustomTextView) view.findViewById(R.id.tvDescription);
            tvDate = (CustomTextView) view.findViewById(R.id.tvDate);

        }
    }
}
