package com.msf.project.sales1crm.model;

public class MenuModel{
        public String menuName, fragment;
        public boolean hasChildren, isGroup;

        public MenuModel(String menuName, boolean isGroup, boolean hasChildren, String fragment) {

            this.menuName = menuName;
            this.fragment = fragment;
            this.isGroup = isGroup;
            this.hasChildren = hasChildren;
        }
}
