package com.msf.project.sales1crm;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;

import com.msf.project.sales1crm.callback.AccountDialogCallback;
import com.msf.project.sales1crm.callback.CategoryDialogCallback;
import com.msf.project.sales1crm.callback.DatabaseMachineAddCallback;
import com.msf.project.sales1crm.customview.CustomEditText;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.customview.DatePickerFragment;
import com.msf.project.sales1crm.fragment.AccountDialogFragment;
import com.msf.project.sales1crm.fragment.CategoryDialogFragment;
import com.msf.project.sales1crm.model.DatabaseMachineModel;
import com.msf.project.sales1crm.presenter.DatabaseAddPresenter;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DatabaseMachineCreate extends BaseActivity implements DatabaseMachineAddCallback {

    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.tvTitle)
    CustomTextView tvTitle;
    @BindView(R.id.ivSave)
    ImageView ivSave;
    @BindView(R.id.tvAccountName)
    CustomEditText tvAccountName;
    @BindView(R.id.ivAccountName)
    ImageView ivAccountName;
    @BindView(R.id.tvBrand)
    CustomTextView tvBrand;
    @BindView(R.id.ivBrand)
    ImageView ivBrand;
    @BindView(R.id.etApplication)
    CustomEditText etApplication;
    @BindView(R.id.tvUnitNoInCustomer)
    CustomEditText tvUnitNoInCustomer;
    @BindView(R.id.etType)
    CustomEditText etType;
    @BindView(R.id.etModel)
    CustomEditText etModel;
    @BindView(R.id.etserialNumber)
    CustomEditText etserialNumber;
    @BindView(R.id.etyearOfInstall)
    CustomEditText etyearOfInstall;
    @BindView(R.id.etpressureVacum)
    CustomEditText etpressureVacum;
    @BindView(R.id.etcapacity)
    CustomEditText etcapacity;
    @BindView(R.id.etDiffPressure)
    CustomEditText etDiffPressure;
    @BindView(R.id.etspeed)
    CustomEditText etspeed;
    @BindView(R.id.etMotorspecs)
    CustomEditText etMotorspecs;
    @BindView(R.id.tvDateLast)
    CustomTextView tvDateLast;
    @BindView(R.id.ivDataLast)
    ImageView ivDataLast;
    @BindView(R.id.etTypeOut)
    CustomEditText etTypeOut;
    @BindView(R.id.tvlatestInspection)
    CustomTextView tvlatestInspection;
    @BindView(R.id.ivLatestInspection)
    ImageView ivLatestInspection;
    @BindView(R.id.etRunning)
    CustomEditText etRunning;
    @BindView(R.id.etCondition)
    CustomEditText etCondition;
    private Context context;
    private String url = "", contact_json, account_id = "", assign_id = "0", string_year, string_month, string_day, open_date = "", brand_id = "";
    private DatePickerFragment datepicker_lastOverhaul, datepicker_latesInspectionDate ;
    private int year, month, day;
    private DatabaseAddPresenter presenter;
    private DatabaseMachineModel model;
private boolean from_detail = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_database_machine_create);
        ButterKnife.bind(this);
        this.context = this;
        this.presenter = new DatabaseAddPresenter(context, this);
        datepicker_lastOverhaul = new DatePickerFragment();
        datepicker_lastOverhaul.setListener(dateListener_lastOverhaoul);
        datepicker_latesInspectionDate = new DatePickerFragment();
        datepicker_latesInspectionDate.setListener(dateListener_latestInspectionDate);

        try{
            from_detail = getIntent().getExtras().getBoolean("from_detail");
        } catch (Exception e){

        }

        try{
            model = getIntent().getExtras().getParcelable("model");
        } catch (Exception e){

        }

        initView();
        initDate();
    }

    private void initView() {
        if(from_detail){
            account_id = model.getId();
            brand_id = model.getBrandId();
            tvTitle.setText("Edit Database Machine");
            tvAccountName.setText(model.getAccountName());
            tvBrand.setText(model.getBrandName());
            etApplication.setText(model.getAccountName());
            tvUnitNoInCustomer.setText(model.getUnitNoInCustomerSite());
            etType.setText(model.getType());
            etModel.setText(model.getModel());
            etserialNumber.setText(model.getSerialNumber());
            etyearOfInstall.setText(model.getYearOfInstall());
            etpressureVacum.setText(model.getPressureVacuum());
            etcapacity.setText(model.getCapacity());
            etDiffPressure.setText(model.getDiffPressure());
            etspeed.setText(model.getSpeed());
            etMotorspecs.setText(model.getMotorSpecs());
            tvDateLast.setText(model.getDateLastOverhaul());
            etTypeOut.setText(model.getTypeOfServiceDone());
            tvlatestInspection.setText(model.getLatestInspectionDate());
            etRunning.setText(model.getRunningHours());
            etCondition.setText(model.getCondition());

            Log.d("TAG","account_id :" + account_id);
            Log.d("TAG", "brand_id : " + brand_id);
            Log.d("TAG","databse_id : " + model.getId());
        }
        ivBack.setOnClickListener(click);
        tvAccountName.setOnClickListener(click);
        ivAccountName.setOnClickListener(click);
        tvBrand.setOnClickListener(click);
        tvDateLast.setOnClickListener(click);
        tvlatestInspection.setOnClickListener(click);
        ivSave.setOnClickListener(click);
    }

    private void initDate() {
        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH) + 1;
        day = c.get(Calendar.DAY_OF_MONTH);
    }


    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.ivBack:
                    finish();
                    break;
                case R.id.tvAccountName:
                    AccountDialogFragment accountDialogFragment = new AccountDialogFragment(DatabaseMachineCreate.this, new AccountDialogCallback() {

                        @Override
                        public void onAccountDialogCallback(Bundle data) {
                            tvAccountName.setText(data.getString("name"));
                            contact_json = data.getString("pic");
                            account_id = data.getString("id");
                        }
                    });
                    accountDialogFragment.show(getSupportFragmentManager(), "accountDialog");
                    Log.d("TAG", "acc_name" + tvAccountName.getText().toString());
                    break;
                case R.id.ivAccountName:
                    AccountDialogFragment DialogFragment = new AccountDialogFragment(DatabaseMachineCreate.this, new AccountDialogCallback() {

                        @Override
                        public void onAccountDialogCallback(Bundle data) {
                            tvAccountName.setText(data.getString("name"));
                            contact_json = data.getString("pic");
                            account_id = data.getString("id");

                            Log.d("TAG", "Contact JSON OOO : " + contact_json);
                        }
                    });
                    DialogFragment.show(getSupportFragmentManager(), "accountDialog");
                    break;
                case R.id.tvBrand:
                    CategoryDialogFragment categoryDialogFragment = new CategoryDialogFragment(DatabaseMachineCreate.this, new CategoryDialogCallback() {
                        @Override
                        public void onAccountDialogCallback(Bundle data) {
                            tvBrand.setText(data.getString("name"));
                            brand_id = data.getString("id");
                            Log.d("TAG", " NAMA : " + tvBrand.getText().toString());
                            Log.d("TAG", " NAMA data : " + data.getString("name"));
                        }
                    });
                    categoryDialogFragment.show(getSupportFragmentManager(), "categoryDialog");
                    break;
                case R.id.ivBrand:
                    CategoryDialogFragment categoryDialog = new CategoryDialogFragment(DatabaseMachineCreate.this, new CategoryDialogCallback() {
                        @Override
                        public void onAccountDialogCallback(Bundle data) {
                            tvBrand.setText(data.getString("name"));
                            brand_id = data.getString("id");
                        }
                    });
                    categoryDialog.show(getSupportFragmentManager(), "categoryDialog");
                    break;
                case R.id.tvDateLast:
                    datepicker_lastOverhaul.setDateFragment(year, month, day);
                    datepicker_lastOverhaul.show(getFragmentManager(), "datePicker");
                    break;
                case R.id.tvlatestInspection:
                    datepicker_latesInspectionDate.setDateFragment(year, month, day);
                    datepicker_latesInspectionDate.show(getFragmentManager(), "datePicker");
                    break;
                case R.id.ivSave:
                    if(Sales1CRMUtils.ConnectionUtility.isNetworkConnected(context)){
                        if(from_detail){
                            model.setAccountId(account_id);
                            model.setBrandId(brand_id);
                            model.setApplication(etApplication.getText().toString());
                            model.setUnitNoInCustomerSite(tvUnitNoInCustomer.getText().toString());
                            model.setType(etType.getText().toString());
                            model.setModel(etModel.getText().toString());
                            model.setSerialNumber(etserialNumber.getText().toString());
                            model.setYearOfInstall(etyearOfInstall.getText().toString());
                            model.setPressureVacuum(etpressureVacum.getText().toString());
                            model.setCapacity(etcapacity.getText().toString());
                            model.setDiffPressure(etDiffPressure.getText().toString());
                            model.setSpeed(etspeed.getText().toString());
                            model.setMotorSpecs(etMotorspecs.getText().toString());
                            model.setDateLastOverhaul(tvDateLast.getText().toString());
                            model.setTypeOfServiceDone(etTypeOut.getText().toString());
                            model.setLatestInspectionDate(tvlatestInspection.getText().toString());
                            model.setRunningHours(etRunning.getText().toString());
                            model.setCondition(etCondition.getText().toString());

                            Log.d("TAG","account_id :" + account_id);
                            Log.d("TAG", "brand_id : " + brand_id);
                            Log.d("TAG","databse_id : " + model.getId());

                            showLoadingDialog();
                            presenter.setupEdit(ApiParam.API_121, model);
                         } else {
                            DatabaseMachineModel model = new DatabaseMachineModel();
                            model.setAccountId(account_id);
                            model.setBrandId(brand_id);
                            model.setId(model.getId());
                            model.setApplication(etApplication.getText().toString());
                            model.setUnitNoInCustomerSite(tvUnitNoInCustomer.getText().toString());
                            model.setType(etType.getText().toString());
                            model.setModel(etModel.getText().toString());
                            model.setSerialNumber(etserialNumber.getText().toString());
                            model.setYearOfInstall(etyearOfInstall.getText().toString());
                            model.setPressureVacuum(etpressureVacum.getText().toString());
                            model.setCapacity(etcapacity.getText().toString());
                            model.setDiffPressure(etDiffPressure.getText().toString());
                            model.setSpeed(etspeed.getText().toString());
                            model.setMotorSpecs(etMotorspecs.getText().toString());
                            model.setDateLastOverhaul(tvDateLast.getText().toString());
                            model.setTypeOfServiceDone(etTypeOut.getText().toString());
                            model.setLatestInspectionDate(tvlatestInspection.getText().toString());
                            model.setRunningHours(etRunning.getText().toString());
                            model.setCondition(etCondition.getText().toString());



                            showLoadingDialog();

                            presenter.setupAdd(ApiParam.API_120,model);
                        }
                    }



                    break;
            }
        }
    };

    DatePickerDialog.OnDateSetListener dateListener_lastOverhaoul = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int r_year, int monthOfYear,
                              int dayOfMonth) {
            year = r_year;
            month = monthOfYear + 1;
            day = dayOfMonth;

            string_year = r_year + "";

            if (month < 10) {

                string_month = "0" + month;
            } else {
                string_month = month + "";
            }
            if (day < 10) {

                string_day = "0" + day;
            } else {
                string_day = day + "";
            }
            open_date = year + "-" + string_month + "-" + string_day;

            tvDateLast.setText(year + "-" + string_month + "-" + string_day);
        }
    };

    DatePickerDialog.OnDateSetListener dateListener_latestInspectionDate = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int r_year, int monthOfYear,
                              int dayOfMonth) {
            year = r_year;
            month = monthOfYear + 1;
            day = dayOfMonth;

            string_year = r_year + "";

            if (month < 10) {

                string_month = "0" + month;
            } else {
                string_month = month + "";
            }
            if (day < 10) {

                string_day = "0" + day;
            } else {
                string_day = day + "";
            }
            open_date = year + "-" + string_month + "-" + string_day;

            tvlatestInspection.setText(year + "-" + string_month + "-" + string_day);
        }
    };


    @Override
    public void finishAdd(String result, String response) {
        dismissLoadingDialog();
        if(result.equalsIgnoreCase("OK")){
            finish();
        }

    }

    @Override
    public void finishEdit(String result, String response) {
        dismissLoadingDialog();
        if(result.equalsIgnoreCase("OK")){
            finish();
        }
    }
}
