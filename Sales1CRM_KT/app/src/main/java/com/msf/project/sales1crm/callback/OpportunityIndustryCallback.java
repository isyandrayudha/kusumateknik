package com.msf.project.sales1crm.callback;

import com.msf.project.sales1crm.model.PieChartModel;

import java.util.List;

public interface OpportunityIndustryCallback {
    void finished(String result, List<PieChartModel> listOppor, List<PieChartModel> listOppor1);
}
