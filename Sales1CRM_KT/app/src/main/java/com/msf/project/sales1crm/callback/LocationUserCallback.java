package com.msf.project.sales1crm.callback;

public interface LocationUserCallback {
    void finishSave(String result, String response, String id);
}
