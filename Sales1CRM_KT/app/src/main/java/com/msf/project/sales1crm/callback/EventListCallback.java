package com.msf.project.sales1crm.callback;

import com.msf.project.sales1crm.model.EventModel;

import java.util.List;

public interface EventListCallback {
    void finishEventList(String result, List<EventModel> list);
    void finishEventMoreList(String result, List<EventModel> list);
}
