package com.msf.project.sales1crm.callback;

import com.msf.project.sales1crm.model.OpportunityModel;

import java.util.List;

public interface OpportunityListByStageCallback {
    void finishMQL(String result, List<OpportunityModel> list);
    void finishMoreMQL(String result,List<OpportunityModel> list);
}
