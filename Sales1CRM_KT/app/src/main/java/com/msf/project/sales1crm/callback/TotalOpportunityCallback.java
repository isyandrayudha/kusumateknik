package com.msf.project.sales1crm.callback;

import com.msf.project.sales1crm.model.TotalModel;

import java.util.List;

public interface TotalOpportunityCallback {
    void finished(String result, List<TotalModel> listOppor, List<TotalModel> listOppor1);
}
