package com.msf.project.sales1crm.callback;

import android.os.Bundle;

/**
 * Created by christianmacbook on 25/05/18.
 */

public interface LocationDialogCallback {
    void onLocationDialogCallback(Bundle data);
}
