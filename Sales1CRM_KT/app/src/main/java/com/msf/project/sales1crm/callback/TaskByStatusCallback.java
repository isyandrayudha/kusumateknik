package com.msf.project.sales1crm.callback;

import com.msf.project.sales1crm.model.TaskModel;

import java.util.List;

public interface TaskByStatusCallback {
    void notStarted(String result, List<TaskModel> list);
    void notstartedMore(String result,List<TaskModel> list);
    void pending(String result, List<TaskModel> list);
    void pendingMore(String result, List<TaskModel> list);
    void inProgress(String result, List<TaskModel> list);
    void inProgressMore(String result, List<TaskModel> list);
    void completed(String result, List<TaskModel>list);
    void completedMore(String result,List<TaskModel> list);
    void waiting(String result, List<TaskModel> list);
    void waitingMore(String result, List<TaskModel> list);
}
