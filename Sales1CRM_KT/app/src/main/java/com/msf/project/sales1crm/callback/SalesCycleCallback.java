package com.msf.project.sales1crm.callback;

import com.msf.project.sales1crm.model.SalesCycleModel;

public interface SalesCycleCallback {
    void finished(String result, SalesCycleModel model);
}
