package com.msf.project.sales1crm.callback;

import com.msf.project.sales1crm.model.LeadConvertModel;

public interface ConvertCallback {
    void finishConvert(String result, LeadConvertModel model);
}
