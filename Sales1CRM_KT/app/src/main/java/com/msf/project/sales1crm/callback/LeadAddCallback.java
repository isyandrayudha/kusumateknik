package com.msf.project.sales1crm.callback;

/**
 * Created by christianmacbook on 04/06/18.
 */

public interface LeadAddCallback {
    void finishAdd(String result, String response);
    void finishEdit(String result, String response);
}
