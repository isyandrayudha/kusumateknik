package com.msf.project.sales1crm.callback;

import com.msf.project.sales1crm.model.TotalModel;

import java.util.List;

public interface TotalStatsCallback {
    void finished(String result, List<TotalModel> list, List<TotalModel> list1);
}
