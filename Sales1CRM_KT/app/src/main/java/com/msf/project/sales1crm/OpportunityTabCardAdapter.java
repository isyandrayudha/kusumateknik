package com.msf.project.sales1crm;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.msf.project.sales1crm.fragment.OpportunityActiveCardFragment;
import com.msf.project.sales1crm.fragment.OpportunityCloseCardFragment;
import com.msf.project.sales1crm.fragment.OpportunityTabCardFragment;

public class OpportunityTabCardAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;
    Bundle bundle;
    private OpportunityTabCardFragment mFragment;

    public OpportunityTabCardAdapter(FragmentManager fm, int NumOfTabs, OpportunityTabCardFragment fragment) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
        this.mFragment = fragment;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                OpportunityActiveCardFragment tab1 = new OpportunityActiveCardFragment();
                mFragment.setClickActionFilterLisner(tab1);
                return tab1;
            case 1:
                OpportunityCloseCardFragment tab2 = new OpportunityCloseCardFragment();
                mFragment.setClickActionFilterLisner(tab2);
                return tab2;
            default:
        }
        return null;
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
