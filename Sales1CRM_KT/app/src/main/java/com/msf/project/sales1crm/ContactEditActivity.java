package com.msf.project.sales1crm;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.msf.project.sales1crm.callback.MeetToAddCallback;
import com.msf.project.sales1crm.customview.CustomEditText;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.customview.SpinnerCustomAdapter;
import com.msf.project.sales1crm.database.PositionDao;
import com.msf.project.sales1crm.model.ContactModel;
import com.msf.project.sales1crm.model.PositionModel;
import com.msf.project.sales1crm.presenter.MeetToAddPresenter;
import com.msf.project.sales1crm.utility.ApiParam;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ContactEditActivity extends Activity implements MeetToAddCallback {
    @BindView(R.id.tvTitle)
    CustomTextView tvTitle;
    @BindView(R.id.rl)
    RelativeLayout rl;
    @BindView(R.id.spTitle)
    Spinner spTitle;
    @BindView(R.id.v)
    View v;
    @BindView(R.id.ivSpin)
    ImageView ivSpin;
    @BindView(R.id.tvErrorTitle)
    CustomTextView tvErrorTitle;
    @BindView(R.id.etFirstName)
    CustomEditText etFirstName;
    @BindView(R.id.etLastName)
    CustomEditText etLastName;
    @BindView(R.id.spPosition)
    Spinner spPosition;
    @BindView(R.id.ivspinn)
    ImageView ivspinn;
    @BindView(R.id.vv)
    View vv;
    @BindView(R.id.tvErrorPosition)
    CustomTextView tvErrorPosition;
    @BindView(R.id.etPhone)
    CustomEditText etPhone;
    @BindView(R.id.etEmail)
    CustomEditText etEmail;
    @BindView(R.id.tvCancel)
    CustomTextView tvCancel;
    @BindView(R.id.tvSave)
    CustomTextView tvSave;
    private View view;
    private Context context;
    private SpinnerCustomAdapter spTitleAdapter, spPositionAdapter;
    private int indexTitle = 0, indexPosition = 0;
    private String[] title = {"Select Title", "Mr.", "Ms.", "Mrs.", "Dr.",
            "Prof."};
    private PositionModel positionModel;
    private MeetToAddPresenter presenter;
    private ContactModel model;
    private String account_id, titles, firtsName, lastName, pPosition, telp, email, contactId;
    private List<PositionModel> positionList = new ArrayList<>();
    private ArrayList<String> arrPosition = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        this.context = this;
        this.presenter = new MeetToAddPresenter(context, this);
        setContentView(R.layout.activity_create_edit);
        ButterKnife.bind(this);

//        initData();
        tvCancel.setOnClickListener(click);
        tvSave.setOnClickListener(click);

        try{
            account_id = getIntent().getExtras().getString("account_id");
            contactId = getIntent().getExtras().getString("id");
            titles = getIntent().getExtras().getString("title");
            firtsName = getIntent().getExtras().getString("firtsName");
            lastName = getIntent().getExtras().getString("LastName");
            pPosition = getIntent().getExtras().getString("position");
            telp = getIntent().getExtras().getString("telp");
            email = getIntent().getExtras().getString("email");
        } catch (Exception e){

        }

        getDataSpinner();


        spTitle.setSelection(Integer.parseInt(titles));
        etFirstName.setText(firtsName);
        etLastName.setText(lastName);
        spPosition.setSelection(indexPosition);
        etPhone.setText(telp);
        etEmail.setText(email);
    }


    private void getDataSpinner() {


        //Title
        spTitleAdapter = new SpinnerCustomAdapter(context,
                R.layout.spinner_default, title);
        spTitleAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spTitle.setAdapter(spTitleAdapter);
        spTitle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                if (position != 0) {
                    indexTitle = position;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        positionList = new PositionDao(context).getPositionList();

        for (int i = 0; i < positionList.size(); i++) {
            if (i == 0) {
                arrPosition.add("Select Position");
            }
            arrPosition.add(positionList.get(i).getName());
        }
        String[] position = new String[arrPosition.size()];
        position = arrPosition.toArray(position);
        spPositionAdapter = new SpinnerCustomAdapter(context,
                R.layout.spinner_default, position);
        spPositionAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spPosition.setAdapter(spPositionAdapter);
        spPosition.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                if (position != 0) {
                    indexPosition = position;
                    positionModel = positionList.get(position - 1);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }


    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.tvCancel:
                    finish();
                    break;
                case R.id.tvSave:
                    if (validation()) {
                        model = new ContactModel();
                        model.setTitle(indexTitle + "");
                        model.setAccount_id(account_id);
                        model.setId(contactId);
                        if(etFirstName.getText().toString().equalsIgnoreCase("")){
                            etFirstName.setError("First Name is not Empty!!!");
                            model.setFirst_name("");
                        }else {
                            model.setFirst_name(etFirstName.getText().toString());
                        }
                        model.setLast_name(etLastName.getText().toString());
                        if (indexPosition != 0) {
                            model.setPosition(positionModel.getId());
                            tvErrorPosition.setVisibility(View.GONE);
                        } else if (indexPosition == 0){
                            tvErrorPosition.setText("Position is not Empty");
                            tvErrorPosition.setVisibility(View.VISIBLE);
                            model.setPosition("0");
                        }
                        if(etPhone.getText().toString().equalsIgnoreCase("")){
                            etPhone.setError("Phone is not Empty!!!");
                            model.setPhone("");
                        } else {
                            model.setPhone(etPhone.getText().toString());
                        }
                        model.setEmail(etEmail.getText().toString());

                        presenter.setupEdit(ApiParam.API_116, model);
                    }
                    Toast.makeText(context, "Success Edit Contact!!!", Toast.LENGTH_SHORT).show();
                    finish();
                    AccountDetailActivity.getInstance().finish();
                    break;
            }
        }
    };

    private boolean validation() {
        if(indexTitle==0) {
            tvErrorTitle.setText("Title must select");
            tvErrorTitle.setVisibility(View.VISIBLE);
            return false;
        }else if(indexTitle!=0){
            tvErrorTitle.setVisibility(View.GONE);
            return true;
        } else if (etFirstName.getText().toString().equalsIgnoreCase("")) {
            etFirstName.setError("First Name is not Empty!!!");
            return false;
        } else if (etPhone.getText().toString().equalsIgnoreCase("")) {
            etPhone.setError("Phone is not Empty!!!");
            return false;
        }
        return true;
    }

    @Override
    public void finishAdd(String result, String response) {
        if (result.equals("OK")) {
            Toast.makeText(context, "Success Edit Contact!!!", Toast.LENGTH_SHORT).show();
            finish();
            AccountDetailActivity.getInstance().finish();
        }
    }

    @Override
    public void finishEdit(String result, String response) {
        if (result.equalsIgnoreCase("OK")) {
            Toast.makeText(context, "Success Edit Contact!!!", Toast.LENGTH_SHORT).show();
            finish();
            AccountDetailActivity.getInstance().finish();
        }
    }
}
