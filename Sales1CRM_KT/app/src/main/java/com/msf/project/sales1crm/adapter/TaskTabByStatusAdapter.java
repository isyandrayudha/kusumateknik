package com.msf.project.sales1crm.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.msf.project.sales1crm.fragment.TCompletedFragment;
import com.msf.project.sales1crm.fragment.TInProgressFragment;
import com.msf.project.sales1crm.fragment.TNotStartedFragment;
import com.msf.project.sales1crm.fragment.TPendingFragment;
import com.msf.project.sales1crm.fragment.TWaitingforFragment;
import com.msf.project.sales1crm.fragment.TaskTabByStatusFragment;

public class TaskTabByStatusAdapter extends FragmentStatePagerAdapter{
    int mNumOfTabs;
    private TaskTabByStatusFragment mFragment;

    public TaskTabByStatusAdapter(FragmentManager supportFragmentManager, int NumOfTabs, TaskTabByStatusFragment fragment) {
        super(supportFragmentManager);
        this.mNumOfTabs = NumOfTabs;
        this.mFragment = fragment;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                TNotStartedFragment tNotStartedFragment = new TNotStartedFragment();
                mFragment.setClickFilterListener(tNotStartedFragment);
                return tNotStartedFragment;
            case 1:
                TPendingFragment tPendingFragment = new TPendingFragment();
                mFragment.setClickFilterListener(tPendingFragment);
                return  tPendingFragment;
            case 2:
                TInProgressFragment tInProgressFragment = new TInProgressFragment();
                mFragment.setClickFilterListener(tInProgressFragment);
                return tInProgressFragment;
            case 3:
                TCompletedFragment tCompletedFragment = new TCompletedFragment();
                mFragment.setClickFilterListener(tCompletedFragment);
                return tCompletedFragment;
            case 4:
                TWaitingforFragment tWaitingforFragment = new TWaitingforFragment();
                mFragment.setClickFilterListener(tWaitingforFragment);
                return tWaitingforFragment;

        }
        return null;
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
