package com.msf.project.sales1crm;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.msf.project.sales1crm.adapter.OpportunityDetailTabAdapter;
import com.msf.project.sales1crm.callback.DashboardCallback;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.model.AbsenceModel;
import com.msf.project.sales1crm.model.DashboardFirstModel;
import com.msf.project.sales1crm.model.DashboardSecondModel;
import com.msf.project.sales1crm.model.DashboardThirdModel;
import com.msf.project.sales1crm.model.OpportunityModel;
import com.msf.project.sales1crm.model.TotalOpportunityModel;
import com.msf.project.sales1crm.presenter.DashboardPresenter;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.PreferenceUtility;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OpportunityDetailActivity extends BaseActivity implements DashboardCallback {

    @BindView(R.id.ivBack)
    ImageView ivBack;

    @BindView(R.id.ivEdit)
    ImageView ivEdit;

    @BindView(R.id.tvOpportunityNameTitle)
    CustomTextView tvOpportunityNameTitle;

    @BindView(R.id.tvOpportunityName)
    CustomTextView tvOpportunityName;

    @BindView(R.id.tvAccountName)
    CustomTextView tvAccountName;

    @BindView(R.id.tvOpportunityType)
    CustomTextView tvOpportunityType;

    @BindView(R.id.tvOpportunityStatus)
    CustomTextView tvOpportunityStatus;

    @BindView(R.id.tvLastUpdated)
    CustomTextView tvLastUpdated;

    @BindView(R.id.tvProgress)
    CustomTextView tvProgress;

    @BindView(R.id.tvSize)
    CustomTextView tvSize;

    @BindView(R.id.tvCloseDate)
    CustomTextView tvCloseDate;

    @BindView(R.id.tabOpporDetail)
    TabLayout tabOpporDetail;

    @BindView(R.id.vpOpporDetail)
    ViewPager vpOpporDetail;
    @BindView(R.id.tvrefNumber)
    CustomTextView tvrefNumber;

    private Context context;
    private String url, iterated;
    private int size;
    private boolean close_oppor = false;
    private OpportunityModel opportunityModel;
    private String[] type = {"opportunity Type", "None", "New Business", "Ext Business"};
    private OpportunityDetailTabAdapter adapter;
    private DashboardPresenter dashboardPresenter;
    static OpportunityDetailActivity opportunityDetailActivity;

    public static OpportunityDetailActivity getInstance() {
        return opportunityDetailActivity;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_opportunitydetail);

        ButterKnife.bind(this);
        this.context = this;
        this.dashboardPresenter = new DashboardPresenter(context, this);
        opportunityDetailActivity = this;

        this.url = PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.URL);

        try {
            opportunityModel = getIntent().getExtras().getParcelable("opportunityModel");
        } catch (Exception e) {
        }

        try {
            close_oppor = getIntent().getExtras().getBoolean("close_oppor");
        } catch (Exception e) {
        }

        Log.i("TAG",
                "number: "
                        + coolFormat(Double.parseDouble(opportunityModel
                        .getSize()), 0));

        initView();
        initData();
    }

    private void initView() {
        dashboardPresenter.setupFirst(ApiParam.API_005);
        Log.d("TAG", "CLose : " + close_oppor);

        if (close_oppor) {
            ivEdit.setVisibility(View.GONE);
        }
//        if(opportunityModel.getProbability().equalsIgnoreCase("100") ||
//                ( opportunityModel.getProbability().equalsIgnoreCase("0") && !opportunityModel.getStage_id().equalsIgnoreCase("1"))) {
//            ivEdit.setVisibility(View.GONE);
//        } else {
//            ivEdit.setVisibility(View.VISIBLE);
//        }

        Log.d("TAG", "PRBAV " + opportunityModel.getProbability());

        tvOpportunityNameTitle.setText(opportunityModel.getName());
        tvOpportunityName.setText(opportunityModel.getName());
        tvAccountName.setText("account Name : " + opportunityModel.getAccount_name());
        tvOpportunityStatus.setText(opportunityModel.getOppor_status_name());
        tvOpportunityType.setText(type[Integer.parseInt(opportunityModel.getType())]);
        tvLastUpdated.setText("Added by " + opportunityModel.getOwner_name());
        tvSize.setText(size + iterated);
        tvProgress.setText(opportunityModel.getProgress() + "%");
        tvCloseDate.setText(opportunityModel.getClose_date_interval());
        tvrefNumber.setText(opportunityModel.getComplete_ref_number());

        ivBack.setOnClickListener(click);
    }

    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.ivBack:
                    finish();
                    break;
            }
        }
    };

    private void initData() {
        Bundle bundle = new Bundle();
        bundle.putParcelable("opportunityModel", opportunityModel);

        CustomTextView tabOne = (CustomTextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabOne.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ico_information, 0, 0);
        tabOpporDetail.addTab(tabOpporDetail.newTab().setCustomView(tabOne));

        CustomTextView tabTwo = (CustomTextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabTwo.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ico_product_white, 0, 0);
        tabOpporDetail.addTab(tabOpporDetail.newTab().setCustomView(tabTwo));

        tabOpporDetail.setTabGravity(tabOpporDetail.GRAVITY_FILL);

        adapter = new OpportunityDetailTabAdapter
                (getSupportFragmentManager(), tabOpporDetail.getTabCount(), bundle);
        vpOpporDetail.setOffscreenPageLimit(1);
        vpOpporDetail.setAdapter(adapter);
        vpOpporDetail.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabOpporDetail));
        tabOpporDetail.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                vpOpporDetail.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    private char[] c = new char[]{'K', 'M', 'B', 'T'};

    private String coolFormat(double n, int iteration) {
        double d = ((long) n / 100) / 10.0;
        boolean isRound = (d * 10) % 10 == 0;// true if the decimal part is
        // equal to 0 (then it's trimmed
        // anyway)
        Log.i("TAG", "num : " + d);
        size = (int) d;
        iterated = String.valueOf(c[iteration]);
        Log.i("TAG", "iteration : " + c[iteration]);
        return (d < 1000 ? // this determines the class, i.e. 'k', 'm' etc
                ((d > 99.9 || isRound || (!isRound && d > 9.99) ? // this decides
                        // whether to trim
                        // the decimals
                        (int) d * 10 / 10
                        : d + "" // (int) d * 10 / 10 drops the decimal
                ) + "" + c[iteration])
                : coolFormat(d, iteration + 1));
    }

    @Override
    public void finishFirst(String result, DashboardFirstModel model) {
        if (model.getOpportunity_edit_permission().equalsIgnoreCase("1")) {
            if (opportunityModel.getProbability().equalsIgnoreCase("100") ||
                    (opportunityModel.getProbability().equalsIgnoreCase("0") &&
                            !opportunityModel.getStage_id().equalsIgnoreCase("1"))) {
                ivEdit.setVisibility(View.GONE);
            } else {
                ivEdit.setVisibility(View.VISIBLE);
            }
            ivEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(context, OpportunityAddActivity.class);
                    i.putExtra("from_detail", true);
                    i.putExtra("opportunityModel", opportunityModel);
                    Log.d("TAG", "op_model " + opportunityModel);
                    startActivity(i);
                }
            });
//            ivEdit.setVisibility(View.VISIBLE);

        } else {
            ivEdit.setVisibility(View.GONE);
            ivEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
                }
            });

        }
    }

    @Override
    public void finishSecond(String result, DashboardSecondModel model) {

    }

    @Override
    public void finishThird(String result, List<DashboardThirdModel> listLead, List<DashboardThirdModel> listLead1, List<DashboardThirdModel> listOppor, List<DashboardThirdModel> listOppor1) {

    }

    @Override
    public void TotOpportunity(String result, TotalOpportunityModel totOporModel) {

    }

    @Override
    public void absence(String result, AbsenceModel model) {

    }
}
