package com.msf.project.sales1crm.presenter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;

import com.msf.project.sales1crm.callback.EventAddCallback;
import com.msf.project.sales1crm.model.EventModel;
import com.msf.project.sales1crm.service.NetworkConnection;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.PreferenceUtility;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;
import com.msf.project.sales1crm.utility.Sales1CRMUtilsJSON;

import org.json.JSONException;
import org.json.JSONObject;

public class EventAddPresenter{
    private final String TAG = EventAddPresenter.class.getSimpleName();
    private Context context;
    private Messenger messenger;
    private Message message;
    private Bundle bundle;
    private Handler handler;
    private String failureResponse = "";
    private String[] stringResponse = {""};
    private EventAddCallback callback;
    private String resultLogin = "NG";
    private String response = "Tolong Periksa Koneksi Anda";

    public EventAddPresenter(Context context, EventAddCallback listener) {
        this.context = context;
        this.callback = listener;
    }

    private void doNetworkService(String url, String params, String from) {
        Log.d(TAG, "url login " + url);
        Log.d(TAG, "params : " + params );
        Intent networkIntent = new Intent(this.context, NetworkConnection.class);
        this.messenger = new Messenger(this.handler);
        networkIntent.putExtra("messenger", this.messenger);
        networkIntent.putExtra("url", url);
        networkIntent.putExtra("params", params);
        networkIntent.putExtra("from", from);
        this.context.startService(networkIntent);
    }

    public void setupAdd(int apiIndex, EventModel eventModel) {
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)) {
            obtainAddAccount(apiIndex, eventModel);
        }else{
            callback.finishAdd(this.resultLogin, this.response);
        }
    }

    public void obtainAddAccount(int apiIndex, EventModel eventModel) {
        this.handler = new Handler(this.context.getMainLooper()) {

            @Override
            public void handleMessage(Message msg) {
                parseAddAccount(msg);
            }

        };

        JSONObject jsonObject = new JSONObject();
        try {
            //Umum
            jsonObject.put("api_key", PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.API_KEY));
            jsonObject.put("event_id", eventModel.getId());
            jsonObject.put("id_product_event", eventModel.getId_product_event());
            jsonObject.put("product_status", eventModel.getProduct_status());
            jsonObject.put("account_id", eventModel.getAccount_id());
            jsonObject.put("assign_to", eventModel.getAssignto());
            jsonObject.put("name", eventModel.getName());
            jsonObject.put("date", eventModel.getDate());
            jsonObject.put("description", eventModel.getDescription());
            jsonObject.put("follow_up_action", eventModel.getFollow_up());
            jsonObject.put("meet_to", eventModel.getMeet_to());
            jsonObject.put("created_at", eventModel.getCreated_at());
            jsonObject.put("brand", eventModel.getBrand());
            jsonObject.put("type", eventModel.getType());
            jsonObject.put("serial_number", eventModel.getSerial_number());
            jsonObject.put("condition", eventModel.getCondition());
            jsonObject.put("photo_1",eventModel.getPhoto_1());
            jsonObject.put("photo_2",eventModel.getPhoto_2());
            jsonObject.put("photo_3",eventModel.getPhoto_3());
            if(!eventModel.getPhoto_1().equalsIgnoreCase("")){
                jsonObject.put("nama_photo_satu","photoSatu" +".jpg");
            }
            if(!eventModel.getPhoto_2().equalsIgnoreCase("")){
                jsonObject.put("nama_photo_dua","photoDua" + ".jpg");
            }
            if(!eventModel.getPhoto_3().equalsIgnoreCase("")){
                jsonObject.put("nama_photo_tiga","photoTiga" + ".jpg");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        doNetworkService(ApiParam.urlBuilder(this.context, apiIndex), jsonObject.toString(), "EventAdd");
    }

    private void parseAddAccount(Message message) {
        this.message = message;
        this.bundle = this.message.getData();
        this.stringResponse[0] = this.bundle.getString("network_response");
        this.failureResponse = this.bundle.getString("network_failure");
        Log.d(TAG, "responseString[0] info " + this.stringResponse[0]);
        if (this.failureResponse.equalsIgnoreCase("yes")){
            this.callback.finishAdd(this.resultLogin, this.response);
        }else{
            try {
                this.resultLogin = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                this.callback.finishAdd(this.resultLogin, this.response);
            } catch (JSONException e) {
                Log.d(TAG, "Exception detail Response " + e.getMessage());
            }
        }
    }

    public void setupEdit(int apiIndex, EventModel eventModel) {
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)) {
            obtainEditAccount(apiIndex, eventModel);
        }else{
            callback.finishEdit(this.resultLogin, this.response);
        }
    }

    public void obtainEditAccount(int apiIndex, EventModel eventModel) {
        this.handler = new Handler(this.context.getMainLooper()) {

            @Override
            public void handleMessage(Message msg) {
                parseEditAccount(msg);
            }

        };

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("api_key", PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.API_KEY));
            jsonObject.put("event_id", eventModel.getId());
            jsonObject.put("id_product_event", eventModel.getId_product_event());
            jsonObject.put("product_status", eventModel.getProduct_status());
            jsonObject.put("account_id", eventModel.getAccount_id());
            jsonObject.put("assign_to", eventModel.getAssignto());
            jsonObject.put("name", eventModel.getName());
            jsonObject.put("date", eventModel.getDate());
            jsonObject.put("description", eventModel.getDescription());
            jsonObject.put("follow_up_action", eventModel.getFollow_up());
            jsonObject.put("meet_to", eventModel.getMeet_to());
            jsonObject.put("created_at", eventModel.getCreated_at());
            jsonObject.put("brand", eventModel.getBrand());
            jsonObject.put("type", eventModel.getType());
            jsonObject.put("serial_number", eventModel.getSerial_number());
            jsonObject.put("condition", eventModel.getCondition());
            jsonObject.put("photo_1",eventModel.getPhoto_1());
            jsonObject.put("photo_2",eventModel.getPhoto_2());
            jsonObject.put("photo_3",eventModel.getPhoto_3());

            if(!eventModel.getPhoto_1().equalsIgnoreCase("")){
                jsonObject.put("nama_photo_satu","photoSatu" +".jpg");
            } else {
                jsonObject.put("nama_photo_satu","");
            }
            if(!eventModel.getPhoto_2().equalsIgnoreCase("")){
                jsonObject.put("nama_photo_dua","photoDua" + ".jpg");
            } else {
                jsonObject.put("nama_photo_dua","");
            }
            if(!eventModel.getPhoto_3().equalsIgnoreCase("")){
                jsonObject.put("nama_photo_tiga","photoTiga" + ".jpg");
            } else {
                jsonObject.put("nama_photo_dua","");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        doNetworkService(ApiParam.urlBuilder(this.context, apiIndex), jsonObject.toString(), "EventEdit");
    }

    private void parseEditAccount(Message message) {
        this.message = message;
        this.bundle = this.message.getData();
        this.stringResponse[0] = this.bundle.getString("network_response");
        this.failureResponse = this.bundle.getString("network_failure");
        Log.d(TAG, "responseString[0] info " + this.stringResponse[0]);
        if (this.failureResponse.equalsIgnoreCase("yes")){
            this.callback.finishEdit(this.resultLogin, this.response);
        }else{
            try {
                this.resultLogin = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                this.callback.finishEdit(this.resultLogin, this.response);
            } catch (JSONException e) {
                Log.d(TAG, "Exception detail Response " + e.getMessage());
            }
        }
    }

}
