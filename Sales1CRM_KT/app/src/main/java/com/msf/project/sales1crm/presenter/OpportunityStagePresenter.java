package com.msf.project.sales1crm.presenter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;

import com.msf.project.sales1crm.callback.OpportunityStageCallback;
import com.msf.project.sales1crm.model.OpportunityStageModel;
import com.msf.project.sales1crm.service.NetworkConnection;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.PreferenceUtility;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;
import com.msf.project.sales1crm.utility.Sales1CRMUtilsJSON;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class OpportunityStagePresenter {
    private final String TAG = OpportunityStagePresenter.class.getSimpleName();
    private Context context;
    private Messenger messenger;
    private Message message;
    private Bundle bundle;
    private Handler handler;
    private String failureResponse = "";
    private String[] stringResponse = {""};
    private OpportunityStageCallback callback;
    private String result = "NG";
    private List<OpportunityStageModel> list = new ArrayList<>();

    public OpportunityStagePresenter(Context context, OpportunityStageCallback callback) {
        this.context = context;
        this.callback = callback;
    }

    private void doNetworkService(String url, String params, String from) {
        Log.d(TAG, "url " + url);
        Log.d(TAG,"params " + params);
        Intent networkIntent = new Intent(this.context, NetworkConnection.class);
        this.messenger = new Messenger(this.handler);
        networkIntent.putExtra("messenger", this.messenger);
        networkIntent.putExtra("url", url);
        networkIntent.putExtra("params", params);
        networkIntent.putExtra("from", from);
        this.context.startService(networkIntent);
    }

    public void setupOS(int apiIndex){
        if(Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)){
            obtainListOS(apiIndex);
        }else {
            callback.finishStage(this.result, this.list);
        }
    }

    public void obtainListOS(int apiIndex){
        this.handler = new Handler(this.context.getMainLooper()){
            @Override
            public void handleMessage(Message msg) {
                parseOS(msg);
            }
        };

        JSONObject jsonObject = new JSONObject();
        try{
            jsonObject.put("api_key", PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.API_KEY));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        doNetworkService(ApiParam.urlBuilder(this.context, apiIndex), jsonObject.toString(),"ApiKey");
    }

    private void parseOS(Message message){
        this.message = message;
        this.bundle = this.message.getData();
        this.stringResponse[0] = this.bundle.getString("network_response");
        this.failureResponse = this.bundle.getString("network_failur");
        Log.d(TAG,"responseString[0] info " + this.stringResponse[0]);
        if(this.failureResponse.equalsIgnoreCase("yes")){
            this.callback.finishStage(this.result, this.list);
        } else {
            try{
                this.result = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                if(this.result.equalsIgnoreCase("OK")){
                    this.list = Sales1CRMUtilsJSON.JSONUtility.getOpportunityStageList(this.stringResponse[0]);
                }
                this.callback.finishStage(this.result, this.list);
            } catch (JSONException e) {
                Log.d(TAG,"Exception detail response " + e.getMessage());
            }
        }
    }

    public void setupMoreOS(int apiIndex){
        if(Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)){
            obtainMoreOS(apiIndex);
        } else {
            callback.finishMoreStage(this.result, this.list);
        }
    }

    public void obtainMoreOS(int apiIndex){
        this.handler = new Handler(this.context.getMainLooper()){
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
            }
        };

        JSONObject jsonObject = new JSONObject();
        try{
            jsonObject.put("api_key", PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.API_KEY));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        doNetworkService(ApiParam.urlBuilder(this.context, apiIndex), jsonObject.toString(),"ApiKey");
    }

    private void parseMoreOS(Message message){
        this.message = message;
        this.bundle = this.message.getData();
        this.stringResponse[0] = this.bundle.getString("network_response");
        this.failureResponse = this.bundle.getString("network_failure");
        Log.d(TAG,"responseString[0] info " + this.stringResponse[0]);
        if(this.failureResponse.equalsIgnoreCase("yes")){
            this.callback.finishMoreStage(this.result, this.list);
        } else {
            try{
                this.result = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                if (this.result.equalsIgnoreCase("OK")){
                    this.list = Sales1CRMUtilsJSON.JSONUtility.getOpportunityStageList(this.stringResponse[0]);
                }
            } catch (JSONException e) {
                Log.d(TAG,"Exception detail response " + e.getMessage());
            }
        }
    }
}
