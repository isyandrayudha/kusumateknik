package com.msf.project.sales1crm.utility;

import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;

import com.msf.project.sales1crm.imageloader.ImageLoader;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static android.content.Context.CONNECTIVITY_SERVICE;

/**
 * Created by christianmacbook on 11/05/18.
 */

public class Sales1CRMUtils {
    public static int POST_WITH_JSON_OBJECT = 1;
    public static int POST_WITH_JSON_ARRAY = 1;

    public static int KEY_REGISTER = 5;
    public static int PRODUCT_LIST = 5;
    public static int ACCOUNT_ACTIVITY = 11;

    public static Date today_date;
    public static boolean service_running = false;
    public static boolean service_device_failure = false;

    //Permission
        public static final int REQUEST_PHONE_CALL = 0;
        public static final int END_CALL = 9;

    // Broadcast Receiver value
    public static final String KEY_DEVICE_PRODUCT = "com.tombol.project.tombol.Device";
    public static final int PENDINGINTENT_STARTALARM = 99993;
    public static final int PENDINGINTENT_ENDALARM = 99991;
    public static final int PENDINGINTENT_LOCATION = 99999;
    public static final int PENDINGINTENT_TASKDATA = 99992;
    public static final int DEFAULT_VALUEFORCHECK_IN = 1;
    public static final int CHECKOUT_ALREADY = 0;

    // DIALOG
    public static final int TYPE_OK = 1;
    public static final int TYPE_YESNO = 2;

    public static final int GALLERY = 0;

    public static final int CAMERA_REQUEST = 1;
    public static final int KEY_ADDRESS_LIST = 2;
    public static final int KEY_ADDRESS_UPDATE = 3;
    public static final int KEY_DEVICE_ADD = 4;
    public static final int KEY_DEVICE_DELETE = 5;
    public static final int KEY_GOOGLEMAP_ADDRESS = 6;
    public static final int KEY_DEVICE_DETAIL = 7;
    public static final int GALLERY_1 = 8;
    public static final int GALLERY_2 = 9;
    public static final int GALLERY_3 = 10;

    public static final int CAMERA_REQUEST_1 = 22;
    public static final int CAMERA_REQUEST_2 = 23;
    public static final int CAMERA_REQUEST_3 = 24;
    public static final int LIST_PRODUCT = 25;
    public static final int LIST_MEET_TO = 26;


    //Location
    public static final int KEY_PROVINSI = 11;
    public static final int KEY_KABUPATEN = 12;
    public static final int KEY_KECAMATAN = 13;
    public static final int KEY_KELURAHAN = 14;

    //Permission
    public static final int KEY_CAMERA_PERMISION = 101;

    public static final int TYPE_RECT = 0;
    public static final int TYPE_ROUND = 1;

    //	FireBase
    public static final String TOPIC_GLOBAL = "global";
    //	broadcast receiver intent filter
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";

    public static final String KEY_REALTIMECART = "com.tombol.project.tombol.Cart";
    //  id to handle the notification in the notification tray
    public static final int BADGE_NOTIF = 99;
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;

    public static class ConnectionUtility {

        public static boolean isNetworkConnected(Context context) {
            ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected()) {
                return true;
            }
            return false;
        }

        public static boolean isMyServiceRunning(Context context, Class<?> serviceClass) {
            ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
            for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
                if (serviceClass.getName().equals(service.service.getClassName())) {
                    return true;
                }
            }
            return false;
        }
    }

    public static class PreferenceShared {
        public static void LogoutUser(Context context) {
            PreferenceUtility.getInstance().saveData(context, PreferenceUtility.API_KEY, "");
            PreferenceUtility.getInstance().saveData(context, PreferenceUtility.DEVICE_TOKEN, "");
            PreferenceUtility.getInstance().saveData(context, PreferenceUtility.KEY_FULLNAME, "");
            PreferenceUtility.getInstance().saveData(context, PreferenceUtility.KEY_COMPANY, "");
            PreferenceUtility.getInstance().saveData(context, PreferenceUtility.KEY_POSITION, "");
            PreferenceUtility.getInstance().saveData(context, PreferenceUtility.KEY_IMAGES_MASTER,"");
        }
    }

    public static class MapUtility {

        public static String getCompleteAddressString(Context context, double LATITUDE, double LONGITUDE) {
            Log.d("TAG", "Longitude : " + LONGITUDE);
            Log.d("TAG", "Latitude : " + LATITUDE);

            String strAdd = "Alamat Tidak Tersedia";

            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            try {
                List<Address> addresses = geocoder.getFromLocation(LATITUDE,
                        LONGITUDE, 1);

                if (addresses != null) {
                    String address = addresses.get(0).getAddressLine(0); //0 to obtain first possible address
                    String city = addresses.get(0).getLocality();
                    String state = addresses.get(0).getAdminArea();
                    String country = addresses.get(0).getCountryName();
                    String postalCode = addresses.get(0).getPostalCode();

                    //create your custom title
                    strAdd = address;
                } else {
                    strAdd = "Alamat Tidak Tersedia";
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return strAdd;
        }
    }

    public static class ImageUtility {

        public static String encodeToBase64(Bitmap image, Bitmap.CompressFormat compressFormat, int quality) {
            ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
            image.compress(compressFormat, quality, byteArrayOS);
            return Base64.encodeToString(byteArrayOS.toByteArray(), Base64.NO_WRAP);
        }

        public static Bitmap getThumbnail(String path) throws FileNotFoundException, IOException {
            File imgFile = new File(path);
            long sizeInBytes = imgFile.length() / (1024 * 1024);
            Log.d("TAG", "size image file " + sizeInBytes);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.RGB_565;
            if (sizeInBytes < 1)
                options.inSampleSize = 1;
            else if (sizeInBytes > 1 && sizeInBytes < 2)
                options.inSampleSize = 2;
            else if (sizeInBytes > 2 && sizeInBytes < 3)
                options.inSampleSize = 3;
            else if (sizeInBytes > 3 && sizeInBytes < 4)
                options.inSampleSize = 4;
            else if (sizeInBytes > 4 && sizeInBytes < 5)
                options.inSampleSize = 5;
            else if (sizeInBytes > 5 && sizeInBytes < 6)
                options.inSampleSize = 6;
            else if (sizeInBytes > 6 && sizeInBytes < 7)
                options.inSampleSize = 7;
            else if (sizeInBytes < 7 && sizeInBytes < 8)
                options.inSampleSize = 8;
            options.inJustDecodeBounds = false;
            options.inDither = true;
            options.inPurgeable = true;
            Bitmap bitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath(), options);
            return bitmap;
        }

        public static Bitmap RotateBitmap(Bitmap source, float angle) {
            Matrix matrix = new Matrix();
            matrix.postRotate(angle);
            return Bitmap.createBitmap(source, 0, 0, source.getWidth(),
                    source.getHeight(), matrix, true);
        }
        public static Bitmap scaleDownBitmap(Bitmap photo/* , int newHeight, Context context */) {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            photo.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            byte[] byteArray = stream.toByteArray();

            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 2;
            options.inPurgeable = true;
            options.inInputShareable = true;
            options.inJustDecodeBounds = false;
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;

            photo = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length,
                    options);
            return photo;
        }
    }

    public static class Utils {
        public static ImageLoader imageLoader;
        public static DecimalFormat formatter;

        public static float convertDpToPixel(float dp, Context context) {
            Resources resources = context.getResources();
            DisplayMetrics metrics = resources.getDisplayMetrics();
            float px = dp * (metrics.densityDpi / 160f);
            return px;
        }

        public static String formatNumber(int number) {
            formatter = new DecimalFormat("#,###,###");
            return formatter.format(number);
        }

        public static String formatNumberLong(long number) {
            formatter = new DecimalFormat("#,###,###");
            return formatter.format(number);
        }

        public static boolean isNetworkAvailable(Context context) {
            ConnectivityManager connectivityManager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager
                    .getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnected();
        }

        public final static boolean isValidEmail(CharSequence target) {
            if (target == null) {
                return false;
            } else {
                return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
            }
        }

        public final static boolean appInstalledOrNot(Context context, String uri) {
            PackageManager pm = context.getPackageManager();
            boolean app_installed;
            try {
                pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
                app_installed = true;
            }
            catch (PackageManager.NameNotFoundException e) {
                app_installed = false;
            }
            return app_installed;
        }

        /**
         * This method converts device specific pixels to density independent
         * pixels.
         *
         * @param px
         *            A value in px (pixels) unit. Which we need to convert into db
         * @param context
         *            Context to get resources and device specific display metrics
         * @return A float value to represent dp equivalent to px value
         */
        public static float convertPixelsToDp(float px, Context context) {
            Resources resources = context.getResources();
            DisplayMetrics metrics = resources.getDisplayMetrics();
            float dp = px / (metrics.densityDpi / 160f);
            return dp;
        }

        public static Bitmap getroundBitmap(Bitmap bitmap) {
            Bitmap output = null;
            // if (bitmap.getWidth() < bitmap.getHeight()) {
            // output = Bitmap.createBitmap(bitmap.getWidth(),
            // bitmap.getWidth(), Config.ARGB_8888);
            // } else if(bitmap.getWidth() > bitmap.getHeight()) {
            // output = Bitmap.createBitmap(bitmap.getHeight(),
            // bitmap.getHeight(), Config.ARGB_8888);
            // }
            if (bitmap.getWidth() >= bitmap.getHeight()) {
                output = Bitmap.createBitmap(bitmap,
                        bitmap.getWidth() / 2 - bitmap.getHeight() / 2, 0,
                        bitmap.getHeight(), bitmap.getHeight());
            } else {

                output = Bitmap.createBitmap(bitmap, 0, bitmap.getHeight() / 2
                                - bitmap.getWidth() / 2, bitmap.getWidth(),
                        bitmap.getWidth());
            }
            Bitmap drawableBitmap = output.copy(Bitmap.Config.ARGB_8888, true);
            Canvas canvas = new Canvas(drawableBitmap);

            final int color = 0xff424242;
            final Paint paint = new Paint();
            final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
            final RectF rectF = new RectF(rect);
            float roundPx = 0;
            if (bitmap.getWidth() < bitmap.getHeight()) {
                roundPx = bitmap.getHeight() / 2;
            } else {
                roundPx = bitmap.getWidth() / 2;
            }

            paint.setAntiAlias(true);
            canvas.drawARGB(0, 0, 0, 0);
            paint.setColor(color);
            canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
            canvas.drawBitmap(bitmap, rect, rect, paint);

            return output;
        }

        public static Bitmap getrectBitmap(Bitmap bitmap) {
            Bitmap output = null;
            // if (bitmap.getWidth() < bitmap.getHeight()) {
            // output = Bitmap.createBitmap(bitmap.getWidth(),
            // bitmap.getWidth(), Config.ARGB_8888);
            // } else if(bitmap.getWidth() > bitmap.getHeight()) {
            // output = Bitmap.createBitmap(bitmap.getHeight(),
            // bitmap.getHeight(), Config.ARGB_8888);
            // }
            if (bitmap.getWidth() >= bitmap.getHeight()) {
                output = Bitmap.createBitmap(bitmap,
                        bitmap.getWidth() / 2 - bitmap.getHeight() / 2, 0,
                        bitmap.getHeight(), bitmap.getHeight());
            } else {

                output = Bitmap.createBitmap(bitmap, 0, bitmap.getHeight() / 2
                                - bitmap.getWidth() / 2, bitmap.getWidth(),
                        bitmap.getWidth());
            }
            Bitmap drawableBitmap = output.copy(Bitmap.Config.ARGB_8888, true);
            Canvas canvas = new Canvas(drawableBitmap);

            final int color = 0xff424242;
            final Paint paint = new Paint();
            final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
            final RectF rectF = new RectF(rect);
            float roundPx = 0;
            if (bitmap.getWidth() < bitmap.getHeight()) {
                roundPx = bitmap.getHeight() / 10;
            } else {
                roundPx = bitmap.getWidth() / 10;
            }

            paint.setAntiAlias(true);
            canvas.drawARGB(0, 0, 0, 0);
            paint.setColor(color);
            canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
            canvas.drawBitmap(bitmap, rect, rect, paint);

            return output;
        }

        public static android.graphics.BitmapFactory.Options getSize(Context c,
                                                                     int resId) {
            android.graphics.BitmapFactory.Options o = new android.graphics.BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeResource(c.getResources(), resId, o);
            return o;
        }

        public static Bitmap scaleImage(Context context, Uri photoUri)
                throws IOException {
            InputStream is = context.getContentResolver().openInputStream(photoUri);
            BitmapFactory.Options dbo = new BitmapFactory.Options();
            dbo.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(is, null, dbo);
            is.close();

            int orientation = getOrientation(context, photoUri);

            Bitmap srcBitmap;
            is = context.getContentResolver().openInputStream(photoUri);

            // // Create the bitmap from file
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 4;
            // srcBitmap = BitmapFactory.decodeStream(is, null, options);
            // } else {
            srcBitmap = BitmapFactory.decodeStream(is, null, options);
            // }
            is.close();

		/*
		 * if the orientation is not 0 (or -1, which means we don't know), we
		 * have to do a rotation.
		 */
            if (orientation > 0) {
                Matrix matrix = new Matrix();
                matrix.postRotate(orientation);

                srcBitmap = Bitmap.createBitmap(srcBitmap, 0, 0,
                        srcBitmap.getWidth(), srcBitmap.getHeight(), matrix, true);
            }

            String type = context.getContentResolver().getType(photoUri);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            if (type.equals("image/png")) {
                srcBitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
            } else if (type.equals("image/jpg") || type.equals("image/jpeg")) {
                srcBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            }
            byte[] bMapArray = baos.toByteArray();
            baos.close();
            return BitmapFactory.decodeByteArray(bMapArray, 0, bMapArray.length);
        }

        public static int getOrientation(Context context, Uri photoUri) {
		/* it's on the external media. */
            Cursor cursor = context.getContentResolver().query(photoUri,
                    new String[] { MediaStore.Images.ImageColumns.ORIENTATION },
                    null, null, null);

            if (cursor.getCount() != 1) {
                return -1;
            }

            cursor.moveToFirst();
            return cursor.getInt(0);
        }

        /**
         * ------------ Helper Methods ----------------------
         * */

        /**
         * Creating file uri to store image/video
         */
        public static Uri getOutputMediaFileUri(int type) {
            return Uri.fromFile(getOutputMediaFile(type));
        }

        /**
         * returning image / video
         */
        private static String IMAGE_DIRECTORY_NAME = "MSF";

        private static File getOutputMediaFile(int type) {

            // External sdcard location
            File mediaStorageDir = new File(
                    Environment
                            .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                    IMAGE_DIRECTORY_NAME);

            // Create the storage directory if it does not exist
            if (!mediaStorageDir.exists()) {
                if (!mediaStorageDir.mkdirs()) {
                    Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
                            + IMAGE_DIRECTORY_NAME + " directory");
                    return null;
                }
            }

            // Create a media file name
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                    Locale.getDefault()).format(new Date());
            File mediaFile;
            mediaFile = new File(mediaStorageDir.getPath() + File.separator
                    + "IMG_" + timeStamp + ".jpg");

            return mediaFile;
        }

        /**
         * returning folder where file will be saved
         */
        private static String FILE_DIRECTORY_NAME = "MSF";
        public static String FILE_LOCATION_NAME = "location.txt";

        public static File createFileFolder() {

            // External sdcard location
            File mediaStorageDir = new File(
                    Environment.getExternalStorageDirectory(), FILE_DIRECTORY_NAME);

            Log.i("AAA", "mediaStorageDir : " + mediaStorageDir.getPath());

            // Create the storage directory if it does not exist
            if (!mediaStorageDir.exists()) {
                if (!mediaStorageDir.mkdirs()) {
                    Log.d(IMAGE_DIRECTORY_NAME, "Oops! Failed create "
                            + IMAGE_DIRECTORY_NAME + " directory");
                    return null;
                }
            }

            return mediaStorageDir;
        }

        public static File createFile(File filepath) {
            File file = new File(filepath.getPath() + File.separator
                    + FILE_LOCATION_NAME);
            Log.i("AAA", "FILE_LOCATION_NAME : "+file.getPath());

            return file;
        }
    }
}
