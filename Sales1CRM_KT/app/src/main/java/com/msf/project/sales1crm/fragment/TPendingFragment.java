package com.msf.project.sales1crm.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.msf.project.sales1crm.ErrorAPIKeyActivity;
import com.msf.project.sales1crm.MenuActivity;
import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.TaskDetailActivity;
import com.msf.project.sales1crm.adapter.TaskListByStatusAdapter;
import com.msf.project.sales1crm.callback.TaskByStatusCallback;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.imageloader.ImageLoader;
import com.msf.project.sales1crm.model.TaskModel;
import com.msf.project.sales1crm.presenter.TaskbyStatusPresenter;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class TPendingFragment extends Fragment implements TaskByStatusCallback, TaskTabByStatusFragment.ClickActionFilterListner {

    //    @BindView(R.id.etSearch)
//    CustomEditText etSearch;
//    @BindView(R.id.ivClear)
//    ImageView ivClear;
    @BindView(R.id.rcPending)
    ListView rcPending;
    Unbinder unbinder;
    @BindView(R.id.tvRefresh)
    CustomTextView tvRefresh;
    @BindView(R.id.rlNoConnection)
    RelativeLayout rlNoConnection;
    @BindView(R.id.llNotFound)
    LinearLayout llNotFound;
    @BindView(R.id.lvTaskSwipe)
    SwipeRefreshLayout lvTaskSwipe;
    private View view;
    private Context context;

    private int prevSize = 0, sort_id = 0;
    private String date_selected = "";
    private boolean isLoadMore = true;
    private boolean isMaxSize = true;
    private boolean pause = false;
    private boolean isScrolling = false;
    private int currentItems, totalItems, scrollOutItems;

    private TaskListByStatusAdapter adapter;
    private TaskbyStatusPresenter presenter;
    private List<TaskModel> list = new ArrayList<>();

    public static TPendingFragment newInstance() {
        TPendingFragment fragment = new TPendingFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_t_pending, container, false);
        ButterKnife.bind(this, this.view);
        this.context = getActivity();
        return this.view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.context = getActivity();
        this.presenter = new TaskbyStatusPresenter(context, this);
        initView();
    }

    private void initView() {
        Sales1CRMUtils.Utils.imageLoader = new ImageLoader(context);
//        etSearch.clearFocus();
        rcPending.setOnScrollListener(new ListLazyLoad());
        getDataFromAPI(0, date_selected, sort_id);
        lvTaskSwipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getDataFromAPI(0,date_selected,sort_id);
            }
        });
//        setTextWatcherForSearch();
//        ivClear.setOnClickListener(click);
        tvRefresh.setOnClickListener(click);
    }

//    private void setTextWatcherForSearch(){
//        etSearch.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (hasFocus) {
//                    if (etSearch.getText().length() == 0) {
//                        ivClear.setVisibility(View.VISIBLE);
//                    }
////                } else {
////                    if (etSearch.getText().length() == 0) {
////                        ivClear.setVisibility(View.GONE);
////                    } else {
////                        ivClear.setVisibility(View.VISIBLE);
////                    }
//
//                }
//            }
//        });
//
//        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            @Override
//            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                if (actionId == EditorInfo.IME_ACTION_GO) {
//                    getDataFromAPI(0, date_selected, sort_id);
//                }
//
//                return true;
//            }
//        });
//    }

    private void getDataFromAPI(int prev, String date, int sort) {
        isMaxSize = true;
        isLoadMore = true;
        llNotFound.setVisibility(View.GONE);

        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(context)) {
            presenter.setupPending(ApiParam.API_070, prev, sort, "", date, "");
        } else {
            rlNoConnection.setVisibility(View.VISIBLE);
            lvTaskSwipe.setVisibility(View.GONE);
        }
    }

    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.ivClear:
//                    etSearch.setText("");
//                    etSearch.clearFocus();
//                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
//                    imm.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);
//                    getDataFromAPI(0, date_selected, sort_id);
                    break;
                case R.id.tvRefresh:
                    getDataFromAPI(0,date_selected, sort_id);
                    break;
                default:
                    break;
            }
        }
    };

    private void setPending() {
        adapter = new TaskListByStatusAdapter(context, this.list);
        List<TaskListByStatusAdapter.Row> rows = new ArrayList<TaskListByStatusAdapter.Row>();
        for (TaskModel taskModel : list) {
            //Read List by Row to insert into List Adapter
            rows.add(new TaskListByStatusAdapter.Item(taskModel));
        }
        //Set Row Adapter
        adapter.setRows(rows);
        rcPending.setAdapter(adapter);
        rcPending.setSelector(R.drawable.transparent_selector);
        rcPending.setOnItemClickListener(new AdapterOnItemClick());
        rcPending.deferNotifyDataSetChanged();

    }

    private void setPendingMore() {
        List<TaskListByStatusAdapter.Row> rows = new ArrayList<TaskListByStatusAdapter.Row>();
        for (TaskModel country : list) {
            // Add the country to the list
            rows.add(new TaskListByStatusAdapter.Item(country));
        }
        adapter.setRows(rows);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void notStarted(String result, List<TaskModel> list) {

    }

    @Override
    public void notstartedMore(String result, List<TaskModel> list) {

    }

    @Override
    public void pending(String result, List<TaskModel> list) {
        lvTaskSwipe.setRefreshing(false);
        if (result.equalsIgnoreCase("OK")) {
            prevSize = list.size();
            this.list = list;

            isMaxSize = false;
            isLoadMore = false;

            setPending();
            if (list.size() > 0) {
                llNotFound.setVisibility(View.GONE);
            } else {
                llNotFound.setVisibility(View.VISIBLE);
            }
        } else if (result.equalsIgnoreCase("FAILED_KEY")) {
            startActivity(new Intent(context, ErrorAPIKeyActivity.class));
            MenuActivity.getInstance().finish();
        } else {
            lvTaskSwipe.setVisibility(View.GONE);
            rlNoConnection.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void pendingMore(String result, List<TaskModel> list) {
        if (result.equals("OK")) {
            for (int i = 0; i < list.size(); i++) {
                this.list.add(list.get(i));
            }

            if (this.prevSize == this.list.size())
                isMaxSize = true;
            this.prevSize = this.list.size();
            setPendingMore();
        }
        this.isLoadMore = false;
    }

    @Override
    public void onClickItemFilter(Integer id) {
        getDataFromAPI(0, date_selected, sort_id);
    }

    private class ListLazyLoad implements AbsListView.OnScrollListener {

        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {

        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            if (firstVisibleItem + visibleItemCount == totalItemCount && totalItemCount != 0) {
                Log.d("TAG", "isLoadMore : " + isLoadMore);
                if (isLoadMore == false && isMaxSize == false) {
                    isLoadMore = true;
                    prevSize = list.size();

                    presenter.setupPendingMore(ApiParam.API_070, prevSize, sort_id, "", date_selected, "");
                }
            }
        }
    }

    private class AdapterOnItemClick implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            // TODO Auto-generated method stub
            TaskListByStatusAdapter.Item item = (TaskListByStatusAdapter.Item) rcPending.getAdapter().getItem(position);
            switch (view.getId()) {
                default:
                        Intent intent = new Intent(context, TaskDetailActivity.class);
                        intent.putExtra("taskModel", item.getItem());
                        startActivity(intent);
                    break;
            }
        }
    }

    @Override
    public void inProgress(String result, List<TaskModel> list) {

    }

    @Override
    public void inProgressMore(String result, List<TaskModel> list) {

    }

    @Override
    public void completed(String result, List<TaskModel> list) {

    }

    @Override
    public void completedMore(String result, List<TaskModel> list) {

    }

    @Override
    public void waiting(String result, List<TaskModel> list) {

    }

    @Override
    public void waitingMore(String result, List<TaskModel> list) {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
