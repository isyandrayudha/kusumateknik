package com.msf.project.sales1crm.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextClock;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.msf.project.sales1crm.AccountAddActivity;
import com.msf.project.sales1crm.CallsAddActivity;
import com.msf.project.sales1crm.DatabaseMachineCreate;
import com.msf.project.sales1crm.ErrorAPIKeyActivity;
import com.msf.project.sales1crm.EventAddActivity;
import com.msf.project.sales1crm.LeadAddActivity;
import com.msf.project.sales1crm.MenuActivity;
import com.msf.project.sales1crm.OpportunityAddActivity;
import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.TaskAddActivity;
import com.msf.project.sales1crm.adapter.ActivityCallsTodayAdapter;
import com.msf.project.sales1crm.adapter.ActivityEventTodayAdapter;
import com.msf.project.sales1crm.adapter.ActivityTodoTodayAdapter;
import com.msf.project.sales1crm.callback.ActivityTodayCallback;
import com.msf.project.sales1crm.callback.DashboardCallback;
import com.msf.project.sales1crm.callback.LocationUserCallback;
import com.msf.project.sales1crm.callback.LoginCallback;
import com.msf.project.sales1crm.callback.PresensiCallback;
import com.msf.project.sales1crm.customview.CustomDialog;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.customview.SheetBottomDialog;
import com.msf.project.sales1crm.database.LocationUserDao;
import com.msf.project.sales1crm.database.MeeToSelectDao;
import com.msf.project.sales1crm.database.ProductDao;
import com.msf.project.sales1crm.imageloader.ImageLoader;
import com.msf.project.sales1crm.model.AbsenceModel;
import com.msf.project.sales1crm.model.ActivityCallsModel;
import com.msf.project.sales1crm.model.ActivityEventModel;
import com.msf.project.sales1crm.model.ActivityTodoModel;
import com.msf.project.sales1crm.model.DashboardFirstModel;
import com.msf.project.sales1crm.model.DashboardSecondModel;
import com.msf.project.sales1crm.model.DashboardThirdModel;
import com.msf.project.sales1crm.model.FunctionModel;
import com.msf.project.sales1crm.model.TotalOpportunityModel;
import com.msf.project.sales1crm.presenter.ActivityTodayPresenter;
import com.msf.project.sales1crm.presenter.DashboardPresenter;
import com.msf.project.sales1crm.presenter.LocationUserPresenter;
import com.msf.project.sales1crm.presenter.LoginPresenter;
import com.msf.project.sales1crm.presenter.PresensiPresenter;
import com.msf.project.sales1crm.service.AlarmReceive;
import com.msf.project.sales1crm.service.GPSTracker;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.PreferenceUtility;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by christianmacbook on 09/05/18.
 */

public class DashboardFragment extends Fragment implements DashboardCallback, ActivityTodayCallback, LoginCallback, LocationUserCallback, PresensiCallback {


    @BindView(R.id.rlNoConnection)
    RelativeLayout rlNoConnection;
    @BindView(R.id.tvFullname)
    CustomTextView tvFullname;
    @BindView(R.id.textAlert)
    CustomTextView textAlert;
    @BindView(R.id.llcheckIn)
    RelativeLayout llcheckIn;
    @BindView(R.id.btnCheckIn)
    CustomTextView btnCheckIn;
    @BindView(R.id.btnCheckOut)
    CustomTextView btnCheckOut;
    @BindView(R.id.tvMessage)
    CustomTextView tvMessage;
    @BindView(R.id.tvCountAccount)
    CustomTextView tvCountAccount;
    @BindView(R.id.rlAccount)
    RelativeLayout rlAccount;
    @BindView(R.id.tvCountOpportunity_2)
    CustomTextView tvCountOpportunity2;
    @BindView(R.id.rlOpportunity)
    RelativeLayout rlOpportunity;
    @BindView(R.id.tvCountLeads)
    CustomTextView tvCountLeads;
    @BindView(R.id.rlLead)
    RelativeLayout rlLead;
    @BindView(R.id.tvCountActivity)
    CustomTextView tvCountActivity;
    @BindView(R.id.rlAcctivity)
    RelativeLayout rlAcctivity;
    @BindView(R.id.tvOppor)
    CustomTextView tvOppor;
    @BindView(R.id.tvCountOpportunity)
    CustomTextView tvCountOpportunity;
    @BindView(R.id.tvTotalValueOpportunity)
    CustomTextView tvTotalValueOpportunity;
    @BindView(R.id.viewPipeline)
    CustomTextView viewPipeline;
    @BindView(R.id.llnotToday)
    LinearLayout llnotToday;
    @BindView(R.id.tvRefresh)
    CustomTextView tvRefresh;
    @BindView(R.id.llTodayNoInternet)
    LinearLayout llTodayNoInternet;
    @BindView(R.id.rcEventToday)
    RecyclerView rcEventToday;
    @BindView(R.id.rcTodoToday)
    RecyclerView rcTodoToday;
    @BindView(R.id.rcCallsToday)
    RecyclerView rcCallsToday;
    @BindView(R.id.swipeRefresh)
    SwipeRefreshLayout swipeRefresh;
    @BindView(R.id.faAccountAdd)
    FloatingActionButton faAccountAdd;
    @BindView(R.id.faLeadAdd)
    FloatingActionButton faLeadAdd;
    @BindView(R.id.faOpportunityAdd)
    FloatingActionButton faOpportunityAdd;
    @BindView(R.id.faEventAdd)
    FloatingActionButton faEventAdd;
    @BindView(R.id.faTaskAdd)
    FloatingActionButton faTaskAdd;
    @BindView(R.id.faCallAdd)
    FloatingActionButton faCallAdd;
    @BindView(R.id.faDatabaseAdd)
    FloatingActionButton faDatabaseAdd;
    @BindView(R.id.menu)
    FloatingActionMenu menu;
    @BindView(R.id.tvAccountName)
    CustomTextView tvAccountName;
    @BindView(R.id.tvAssignTo)
    CustomTextView tvAssignTo;
    @BindView(R.id.shimmerLayout)
    ShimmerFrameLayout shimmerLayout;
    @BindView(R.id.llshim)
    LinearLayout llshim;
    Unbinder unbinder;
    @BindView(R.id.llcheck)
    RelativeLayout llcheck;
    @BindView(R.id.rlcheckin)
    RelativeLayout rlcheckin;
    @BindView(R.id.rlcheckout)
    RelativeLayout rlcheckout;
    private View view;
    private Context context;
    private DashboardPresenter presenter;
    private PresensiPresenter presensiPresenter;
    private LoginPresenter loginPresenter;
    private SheetBottomDialog sheetBottomDialog;
    private TotalOpportunityModel totOportunity;


    private ActivityEventTodayAdapter aEvenAdapter;
    private ActivityTodoTodayAdapter aTodoAdapter;
    private ActivityCallsTodayAdapter aCallsAdapter;
    private ActivityTodayPresenter todayPresenter;
    private FunctionModel functionModel;
    private List<ActivityEventModel> aEventList = new ArrayList<>();
    private List<ActivityTodoModel> aTodoList = new ArrayList<>();
    private List<ActivityCallsModel> aCallsList = new ArrayList<>();


    private int prevSize = 0;
    private boolean isLoadMore = true;
    private boolean isMaxSize = true;
    private boolean pause = false;
    private String url = "";
    private boolean isScrolling = false;
    private int currentItems, totalItems, scrollOutItems;
    private String alamat, timeNow;
    private AlarmReceive location_alarm;
    private LocationUserDao dao;
    private LocationUserPresenter locationPresenter;
    private char[] c = new char[]{'K', 'M', 'B', 'T'};

    public static DashboardFragment newInstance() {
        DashboardFragment fragment = new DashboardFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_dashboard, container, false);

        this.context = getActivity();
        location_alarm = new AlarmReceive();
        context.registerReceiver(location_alarm, new IntentFilter(AlarmReceive.BROADCAST_ACTION));
        dao = new LocationUserDao(context);
        locationPresenter = new LocationUserPresenter(context, this);
        presensiPresenter = new PresensiPresenter(context, this);

        unbinder = ButterKnife.bind(this, this.view);
        return this.view;


    }

//    private void sendLocationUser() {
//        LocationUserDao dao = new LocationUserDao(context);
//        if (dao.getLastLocationUser().getLatitude() != null) {
//            locationPresenter.saveData(ApiParam.API_115, dao.getLastLocationUser());
//        }
//    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        context.unregisterReceiver(location_alarm);
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        this.context = getActivity();
        this.presenter = new DashboardPresenter(context, this);
        this.todayPresenter = new ActivityTodayPresenter(context, this);
        this.presensiPresenter = new PresensiPresenter(context, this);

//        sendLocationUser();
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Dashboard");

//        checkToday();
//        getDataTodoToday();
        initView();


    }


    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.tvRefresh:
                    getDataDashboard();
                    getDataTodoToday();
                    break;
                case R.id.btnCheckIn:
                    final CustomDialog dialogCheckIn = CustomDialog.setupDialogAbsence(context);
                    TextClock tcClock = (TextClock) dialogCheckIn.findViewById(R.id.tcClock);
                    CustomTextView tvCancel = (CustomTextView) dialogCheckIn.findViewById(R.id.tvCancel);
                    CustomTextView tvCheckIn = (CustomTextView) dialogCheckIn.findViewById(R.id.tvCheckIn);
                    CustomTextView tvTitle = (CustomTextView) dialogCheckIn.findViewById(R.id.tvTitle);

                    tvTitle.setText("Checkin");
                    tvCheckIn.setText("CheckIn");

                    tvCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialogCheckIn.dismiss();

                        }
                    });

                    tvCheckIn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            presensiPresenter.Checkin(ApiParam.API_123);
                            dialogCheckIn.dismiss();
                        }
                    });
                    dialogCheckIn.show();

                    break;
                case R.id.btnCheckOut:
                    final CustomDialog dialogCheckOut = CustomDialog.setupDialogAbsence(context);
                    TextClock ntcClock = (TextClock) dialogCheckOut.findViewById(R.id.tcClock);
                    CustomTextView ntvCancel = (CustomTextView) dialogCheckOut.findViewById(R.id.tvCancel);
                    CustomTextView ntvCheckIn = (CustomTextView) dialogCheckOut.findViewById(R.id.tvCheckIn);
                    CustomTextView ntvTitle = (CustomTextView) dialogCheckOut.findViewById(R.id.tvTitle);

                    ntvTitle.setText("Checkout");
                    ntvCheckIn.setText("Checkout");

                    ntvCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialogCheckOut.dismiss();

                        }
                    });

                    ntvCheckIn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            presensiPresenter.Checkout(ApiParam.API_124);
                            dialogCheckOut.dismiss();
                        }
                    });
                    dialogCheckOut.show();
                    break;
            }
        }
    };

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void initView() {
        Sales1CRMUtils.Utils.imageLoader = new ImageLoader(context);
        tvFullname.setText(PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.KEY_FULLNAME));
//        tvTotalValueOpportunity.setText(totOportunity.getTotal());
//        faAccountAdd.setOnClickListener(click);
        btnCheckIn.setOnClickListener(click);
        btnCheckOut.setOnClickListener(click);
        getDataDashboard();
        getDataTodoToday();

        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                llshim.setVisibility(View.VISIBLE);
                shimmerLayout.setVisibility(View.VISIBLE);
                shimmerLayout.startShimmer();
                getDataDashboard();
                getDataTodoToday();
            }
        });
    }

    private void getDataDashboard() {
        GPSTracker gpsTracker = new GPSTracker(context);
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(context)) {
            if (gpsTracker.canGetLocation()) {
                if (!PreferenceUtility.service_running) {

                    location_alarm.SetAlarmForGeoService(context);
                }

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        presenter.setupAbsen(ApiParam.API_122);
                    }
                }, 1000);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        presenter.setupFirst(ApiParam.API_005);
                    }
                }, 2000);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        presenter.setupTotOpor(ApiParam.API_110);
                    }
                }, 3000);


            } else {

                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gpsTracker.showSettingsAlert();
            }


        } else {
            swipeRefresh.setVisibility(View.GONE);
        }
    }

    private void getDataTodoToday() {
        isMaxSize = true;
        isLoadMore = true;

        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(context)) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    todayPresenter.setupTodoToday(ApiParam.API_108);
                }
            }, 4000);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    todayPresenter.setupEventToday(ApiParam.API_107);
                }
            }, 5000);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    todayPresenter.setupCallsToday(ApiParam.API_109);
                }
            }, 6000);
        } else {
            swipeRefresh.setVisibility(View.GONE);
//            llnotToday.setVisibility(View.VISIBLE);
//            rlNoConnection.setVisibility(View.VISIBLE);
//            Toast.makeText(context, "no connection ", Toast.LENGTH_SHORT).show();
        }
    }

    private void checkToday() {
        if (aCallsList.size() == 0 && aTodoList.size() == 0 && aEventList.size() == 0) {
            llnotToday.setVisibility(View.VISIBLE);

        } else {
            llnotToday.setVisibility(View.GONE);
        }
    }


    @Override
    public void finishFirst(String result, DashboardFirstModel model) {
//        if (swipeRefresh != null) {
//            swipeRefresh.setRefreshing(false);
//        }
        if (result.equalsIgnoreCase("OK")) {
            if (model.getAccount_list_permission().equalsIgnoreCase("1")) {
                tvCountAccount.setText(model.getCount_account());
                tvCountAccount.setVisibility(View.VISIBLE);
                rlAccount.setBackground(getResources().getDrawable(R.drawable.custom_panel_dashboard_purple));
                rlAccount.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.frame_layout, AccountFrangment.newInstance());
                        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                        transaction.commit();
                    }
                });

            } else {
                tvCountAccount.setVisibility(View.INVISIBLE);
                rlAccount.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
                    }
                });
                rlAccount.setBackground(getResources().getDrawable(R.drawable.custom_panel_purple_gray));
            }

            Log.d("TAG", "OLP " + model.getOpportunity_list_permission());
            if (model.getOpportunity_list_permission().equalsIgnoreCase("1")) {
                tvCountOpportunity.setText(model.getCount_opportunity());
                tvCountOpportunity2.setText(model.getCount_opportunity());
                tvCountOpportunity.setVisibility(View.VISIBLE);
                tvCountOpportunity2.setVisibility(View.VISIBLE);
                rlOpportunity.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.frame_layout, OpportunityTabFragment.newInstance());
                        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                        transaction.commit();
                    }
                });
                rlOpportunity.setBackground(getResources().getDrawable(R.drawable.custom_panel_dashboard_lightblue));
            } else {
                tvCountOpportunity.setVisibility(View.INVISIBLE);
                tvCountOpportunity2.setVisibility(View.INVISIBLE);
                rlOpportunity.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
                    }
                });
                rlOpportunity.setBackground(getResources().getDrawable(R.drawable.custom_panel_lightblue_gray));
            }

            if (model.getOpportunity_list_permission().equalsIgnoreCase("1")) {
                viewPipeline.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.frame_layout, Vp_opportunityTab.newInstance());
                        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                        transaction.commit();
                    }
                });
                viewPipeline.setVisibility(View.VISIBLE);
            } else {
                viewPipeline.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
                    }
                });
                viewPipeline.setVisibility(View.INVISIBLE);
            }

            if (model.getLead_list_permission().equalsIgnoreCase("1")) {
                tvCountLeads.setText(model.getCount_lead());
                tvCountLeads.setVisibility(View.VISIBLE);
                rlLead.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.frame_layout, LeadFragment.newInstance());
                        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                        transaction.commit();
                    }
                });
                rlLead.setBackground(getResources().getDrawable(R.drawable.custom_panel_dashboard_darkblue));
            } else {
                tvCountLeads.setVisibility(View.INVISIBLE);
                rlLead.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
                    }
                });
                rlLead.setBackground(getResources().getDrawable(R.drawable.custome_panel_darkblue_gray));
            }

            if (model.getActivity_list_permission().equalsIgnoreCase("1")) {
                tvCountActivity.setText(model.getCount_activity());
                tvCountActivity.setVisibility(View.VISIBLE);
                rlAcctivity.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.frame_layout, ActivityListFragment.newInstance());
                        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                        transaction.commit();
                    }
                });
                rlAcctivity.setBackground(getResources().getDrawable(R.drawable.custom_panel_dashboard_greend));
            } else {
                tvCountActivity.setVisibility(View.INVISIBLE);
                rlAcctivity.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
                    }
                });
                rlAcctivity.setBackground(getResources().getDrawable(R.drawable.custom_panel_pink_gray));
            }


            if (model.getAccount_create_permission().equalsIgnoreCase("1")) {
                faAccountAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(context, AccountAddActivity.class));
                    }
                });
                faAccountAdd.setColorNormal(R.color.BlueCRM);
                faAccountAdd.setColorPressed(R.color.colorPrimaryDark);
                faAccountAdd.setColorPressedResId(R.color.colorPrimaryDark);
                faAccountAdd.setColorNormalResId(R.color.BlueCRM);
            } else {
                faAccountAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
                    }
                });
                faAccountAdd.setColorNormal(R.color.BlueCRM_light);
                faAccountAdd.setColorPressed(R.color.colorPrimaryDark_light);
                faAccountAdd.setColorPressedResId(R.color.colorPrimaryDark_light);
                faAccountAdd.setColorNormalResId(R.color.BlueCRM_light);
            }

            if (model.getLead_create_permission().equalsIgnoreCase("1")) {
                faLeadAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(context, LeadAddActivity.class));
                    }
                });

                faLeadAdd.setColorNormal(R.color.BlueCRM);
                faLeadAdd.setColorPressed(R.color.colorPrimaryDark);
                faLeadAdd.setColorPressedResId(R.color.colorPrimaryDark);
                faLeadAdd.setColorNormalResId(R.color.BlueCRM);

            } else {
                faLeadAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
                    }
                });
                faLeadAdd.setColorNormal(R.color.BlueCRM_light);
                faLeadAdd.setColorPressed(R.color.colorPrimaryDark_light);
                faLeadAdd.setColorPressedResId(R.color.colorPrimaryDark_light);
                faLeadAdd.setColorNormalResId(R.color.BlueCRM_light);
            }

            if (model.getOpportunity_create_permission().equalsIgnoreCase("1")) {
                faOpportunityAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(context, OpportunityAddActivity.class));
                    }
                });
                faOpportunityAdd.setColorNormal(R.color.BlueCRM);
                faOpportunityAdd.setColorPressed(R.color.colorPrimaryDark);
                faOpportunityAdd.setColorPressedResId(R.color.colorPrimaryDark);
                faOpportunityAdd.setColorNormalResId(R.color.BlueCRM);
            } else {
                faOpportunityAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
                    }
                });
                faOpportunityAdd.setColorNormal(R.color.BlueCRM_light);
                faOpportunityAdd.setColorPressed(R.color.colorPrimaryDark_light);
                faOpportunityAdd.setColorPressedResId(R.color.colorPrimaryDark_light);
                faOpportunityAdd.setColorNormalResId(R.color.BlueCRM_light);
            }

            if (model.getCrm_event_create_permission().equalsIgnoreCase("1")) {
                faEventAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        MeeToSelectDao meeToSelectDao = new MeeToSelectDao(context);
                        meeToSelectDao.deleteAllRecord();

                        ProductDao productDao = new ProductDao(context);
                        productDao.deleteAllRecord();
                        startActivity(new Intent(context, EventAddActivity.class));
                    }
                });
                faEventAdd.setColorNormal(R.color.BlueCRM);
                faEventAdd.setColorPressed(R.color.colorPrimaryDark);
                faEventAdd.setColorPressedResId(R.color.colorPrimaryDark);
                faEventAdd.setColorNormalResId(R.color.BlueCRM);
            } else {
                faEventAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
                    }
                });
                faEventAdd.setColorNormal(R.color.BlueCRM_light);
                faEventAdd.setColorPressed(R.color.colorPrimaryDark_light);
                faEventAdd.setColorPressedResId(R.color.colorPrimaryDark_light);
                faEventAdd.setColorNormalResId(R.color.BlueCRM_light);
            }

            if (model.getCrm_task_create_permission().equalsIgnoreCase("1")) {
                faTaskAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(context, TaskAddActivity.class));
                    }
                });
                faTaskAdd.setColorNormal(R.color.BlueCRM);
                faTaskAdd.setColorPressed(R.color.colorPrimaryDark);
                faTaskAdd.setColorPressedResId(R.color.colorPrimaryDark);
                faTaskAdd.setColorNormalResId(R.color.BlueCRM);
            } else {
                faTaskAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
                    }
                });
                faTaskAdd.setColorNormal(R.color.BlueCRM_light);
                faTaskAdd.setColorPressed(R.color.colorPrimaryDark_light);
                faTaskAdd.setColorPressedResId(R.color.colorPrimaryDark_light);
                faTaskAdd.setColorNormalResId(R.color.BlueCRM_light);
            }

            if (model.getCrm_call_create_permission().equalsIgnoreCase("1")) {
                faCallAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(context, CallsAddActivity.class));
                    }
                });
                faCallAdd.setColorNormal(R.color.BlueCRM);
                faCallAdd.setColorPressed(R.color.colorPrimaryDark);
                faCallAdd.setColorPressedResId(R.color.colorPrimaryDark);
                faCallAdd.setColorNormalResId(R.color.BlueCRM);
            } else {
                faCallAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
                    }
                });
                faCallAdd.setColorNormal(R.color.BlueCRM_light);
                faCallAdd.setColorPressed(R.color.colorPrimaryDark_light);
                faCallAdd.setColorPressedResId(R.color.colorPrimaryDark_light);
                faCallAdd.setColorNormalResId(R.color.BlueCRM_light);
            }

            if (model.getDatabase_machine_create_permission().equalsIgnoreCase("1")) {
                faDatabaseAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(context, DatabaseMachineCreate.class));
                    }
                });
                faDatabaseAdd.setColorNormal(R.color.BlueCRM);
                faDatabaseAdd.setColorPressed(R.color.colorPrimaryDark);
                faDatabaseAdd.setColorPressedResId(R.color.colorPrimaryDark);
                faDatabaseAdd.setColorNormalResId(R.color.BlueCRM);
            } else {
                faDatabaseAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
                    }
                });
                faDatabaseAdd.setColorNormal(R.color.BlueCRM_light);
                faDatabaseAdd.setColorPressed(R.color.colorPrimaryDark_light);
                faDatabaseAdd.setColorPressedResId(R.color.colorPrimaryDark_light);
                faDatabaseAdd.setColorNormalResId(R.color.BlueCRM_light);
            }
        } else if (result.equalsIgnoreCase("FAILED_KEY")) {
            startActivity(new Intent(context, ErrorAPIKeyActivity.class));
            MenuActivity.getInstance().finish();
        } else {
        }
    }

    @Override
    public void finishSecond(String result, DashboardSecondModel model) {
    }


    @Override
    public void finishThird(String result, List<DashboardThirdModel> listLead, List<DashboardThirdModel> listLead1, List<DashboardThirdModel> listOppor, List<DashboardThirdModel> listOppor1) {

    }

    @Override
    public void TotOpportunity(String result, TotalOpportunityModel totOporModel) {
//        if (swipeRefresh != null) {
//            swipeRefresh.setRefreshing(false);
//        }
        if (result.equalsIgnoreCase("OK")) {
            tvTotalValueOpportunity.setText(totOporModel.getTotal());
        } else if (result.equalsIgnoreCase("FAILED_KEY")) {
            startActivity(new Intent(context, ErrorAPIKeyActivity.class));
            MenuActivity.getInstance().finish();
        }
    }


    @Override
    public void absence(String result, AbsenceModel model) {
        if (swipeRefresh != null) {
            swipeRefresh.setRefreshing(false);
        }
        if (result.equalsIgnoreCase("OK")) {
            if (model.getUserType().equalsIgnoreCase("2")) {
                if (model.getCheckIn().equalsIgnoreCase("0") && model.getCheckOut().equalsIgnoreCase("0")) {
                    if (model.getStatus().equalsIgnoreCase("late")) {
                        textAlert.setText(model.getMessage());
//                        btnCheckIn.setVisibility(View.VISIBLE);
//                        btnCheckOut.setVisibility(View.GONE);
                        rlcheckin.setVisibility(View.VISIBLE);
                        tvMessage.setVisibility(View.GONE);
                        llcheckIn.setVisibility(View.VISIBLE);
                    } else if (model.getStatus().equalsIgnoreCase("notcheckin")) {
                        textAlert.setText("You are not checkin! Please");
//                        btnCheckIn.setVisibility(View.VISIBLE);
//                        btnCheckOut.setVisibility(View.GONE);
                        rlcheckin.setVisibility(View.VISIBLE);
                        tvMessage.setVisibility(View.GONE);
                        llcheckIn.setVisibility(View.VISIBLE);
                    }
                } else if (model.getCheckIn().equalsIgnoreCase("1") && model.getCheckOut().equalsIgnoreCase("0")) {
                    if (model.getStatus().equalsIgnoreCase("notcheckout")) {
//                        btnCheckIn.setVisibility(View.GONE);
//                        btnCheckOut.setVisibility(View.VISIBLE);
                        textAlert.setText("Please Checkout!!!");
                        tvMessage.setVisibility(View.GONE);
                        rlcheckout.setVisibility(View.VISIBLE);
                        llcheckIn.setVisibility(View.VISIBLE);
                    } else if (model.getStatus().equalsIgnoreCase("checkin")) {
                        tvMessage.setText(model.getMessage());
                        tvMessage.setVisibility(View.VISIBLE);
                        llcheckIn.setVisibility(View.GONE);
//                        btnCheckOut.setVisibility(View.GONE);
//                        btnCheckIn.setVisibility(View.GONE);
                    }
                } else if (model.getCheckIn().equalsIgnoreCase("1") && model.getCheckOut().equalsIgnoreCase("1")) {
                    llcheckIn.setVisibility(View.GONE);
                    tvMessage.setText(model.getMessage());
                    btnCheckIn.setVisibility(View.GONE);
                    btnCheckOut.setVisibility(View.GONE);
                }
            } else {
                llcheckIn.setVisibility(View.GONE);
                tvMessage.setText(model.getMessage());
            }
        }
    }


    private String getDate() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
        return df.format(c.getTime());
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }


    private void setEventToday() {
//        llshim.setVisibility(View.VISIBLE);
//        shimmerLayout.setVisibility(View.GONE);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rcEventToday.setLayoutManager(layoutManager);
        rcEventToday.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {

                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                currentItems = layoutManager.getChildCount();
                totalItems = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstVisibleItemPosition();

                if (isScrolling && (currentItems + scrollOutItems == totalItems)) {
                    isScrolling = false;
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            todayPresenter.setupMoreEventToday(ApiParam.API_107);
                            aEvenAdapter.notifyDataSetChanged();
                        }
                    }, 1000);
                }
            }
        });

        aEvenAdapter = new ActivityEventTodayAdapter(context, this.aEventList);
        List<ActivityEventTodayAdapter.Row> rows = new ArrayList<>();
        for (ActivityEventModel activityEventModel : aEventList) {
            rows.add(new ActivityEventTodayAdapter.Item(activityEventModel));
        }
        aEvenAdapter.setRows(rows);
        rcEventToday.setAdapter(aEvenAdapter);
        llshim.setVisibility(View.INVISIBLE);
        shimmerLayout.stopShimmer();
        shimmerLayout.setVisibility(View.INVISIBLE);
    }

    private void setEventTodayMore() {
        List<ActivityEventTodayAdapter.Row> rows = new ArrayList<>();
        for (ActivityEventModel atm : aEventList) {
            rows.add(new ActivityEventTodayAdapter.Item(atm));
        }
        aEvenAdapter.setRows(rows);
        aEvenAdapter.notifyDataSetChanged();
    }


    @Override
    public void finishAEventToday(String result, List<ActivityEventModel> aEventModels) {
//        if (swipeRefresh != null) {
//            swipeRefresh.setRefreshing(false);
//        }
        if (result.equalsIgnoreCase("OK")) {
            prevSize = aEventModels.size();
            this.aEventList = aEventModels;

            isMaxSize = false;
            isLoadMore = false;
//            llnotToday.setVisibility(View.GONE);
            setEventToday();
            if (aEventModels.size() > 0) {
            } else if (result.equalsIgnoreCase("FAILED_KEY")) {
                startActivity(new Intent(context, ErrorAPIKeyActivity.class));
                MenuActivity.getInstance().finish();
            } else {
                checkToday();
//                llnotToday.setVisibility(View.VISIBLE);
            }
        } else {
        }
    }

    @Override
    public void finishAEventMoreToday(String result, List<ActivityEventModel> aEventModels) {
        if (result.equals("OK")) {
            for (int i = 0; i < aEventModels.size(); i++) {
                this.aEventList.add(aEventModels.get(i));
            }

            if (this.prevSize == this.aEventList.size())
                isMaxSize = true;
            this.prevSize = this.aEventList.size();
            setEventTodayMore();

        }
        this.isLoadMore = false;
    }


    private void setTodoToday() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rcTodoToday.setLayoutManager(layoutManager);
        rcTodoToday.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {

                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                currentItems = layoutManager.getChildCount();
                totalItems = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstVisibleItemPosition();

                if (isScrolling && (currentItems + scrollOutItems == totalItems)) {
                    isScrolling = false;
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            todayPresenter.setupMoreTodoToday(ApiParam.API_108);
                            aEvenAdapter.notifyDataSetChanged();
                        }
                    }, 2000);
                }
            }
        });

        aTodoAdapter = new ActivityTodoTodayAdapter(context, this.aTodoList);
        List<ActivityTodoTodayAdapter.Row> rows = new ArrayList<>();
        for (ActivityTodoModel atta : aTodoList) {
            rows.add(new ActivityTodoTodayAdapter.Item(atta));
        }
        aTodoAdapter.setGaris(rows);
        rcTodoToday.setAdapter(aTodoAdapter);
    }

    private void setTodoTodayMore() {
        List<ActivityTodoTodayAdapter.Row> rows = new ArrayList<>();
        for (ActivityTodoModel atm : aTodoList) {
            rows.add(new ActivityTodoTodayAdapter.Item(atm));
        }

        aTodoAdapter.setGaris(rows);
        aTodoAdapter.notifyDataSetChanged();

    }

    @Override
    public void finishATodoToday(String result, List<ActivityTodoModel> aTodoModels) {
//        if (swipeRefresh != null) {
//            swipeRefresh.setRefreshing(false);
//        }
        if (result.equalsIgnoreCase("OK")) {
            prevSize = aTodoModels.size();
            this.aTodoList = aTodoModels;

            isMaxSize = false;
            isLoadMore = false;

//            llnotToday.setVisibility(View.GONE);
            setTodoToday();
            if (aTodoModels.size() > 0) {
            } else if (result.equalsIgnoreCase("FAILED_KEY")) {
                startActivity(new Intent(context, ErrorAPIKeyActivity.class));
                MenuActivity.getInstance().finish();
            } else {
                checkToday();
//                llnotToday.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void finishATodoMoreToday(String result, List<ActivityTodoModel> aTodoModels) {
        if (result.equals("OK")) {
            for (int i = 0; i < aTodoModels.size(); i++) {
                this.aTodoList.add(aTodoModels.get(i));
            }

            if (this.prevSize == this.aTodoList.size())
                isMaxSize = true;
            this.prevSize = this.aTodoList.size();
            setTodoTodayMore();


        }
        this.isLoadMore = false;
    }

    private void setCallsToday() {
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rcCallsToday.setLayoutManager(layoutManager);
        aCallsAdapter = new ActivityCallsTodayAdapter(context, this.aCallsList);
        List<ActivityCallsTodayAdapter.Row> rows = new ArrayList<>();
        for (ActivityCallsModel aCallsM : aCallsList) {
            rows.add(new ActivityCallsTodayAdapter.Item(aCallsM));
        }
        aCallsAdapter.setRows(rows);
        rcCallsToday.setAdapter(aCallsAdapter);
    }

    private void setCallsTodayMore() {
        List<ActivityCallsTodayAdapter.Row> rows = new ArrayList<>();
        for (ActivityCallsModel aCalls : aCallsList) {
            rows.add(new ActivityCallsTodayAdapter.Item(aCalls));
        }
        aCallsAdapter.setRows(rows);
        aCallsAdapter.notifyDataSetChanged();
    }

    @Override
    public void finishACallsToday(String result, List<ActivityCallsModel> aCallsModels) {
//        if (swipeRefresh != null) {
//            swipeRefresh.setRefreshing(false);
//        }
        if (result.equalsIgnoreCase("OK")) {
            prevSize = aCallsModels.size();
            this.aCallsList = aCallsModels;

            isMaxSize = false;
            isLoadMore = false;
//            llnotToday.setVisibility(View.GONE);
            setCallsToday();
            if (aCallsModels.size() > 0) {
            } else if (result.equalsIgnoreCase("FAILED_KEY")) {
                startActivity(new Intent(context, ErrorAPIKeyActivity.class));
                MenuActivity.getInstance().finish();
            } else {
                checkToday();
//                llnotToday.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void finishACallsMoreToday(String result, List<ActivityCallsModel> aCallsModels) {
        if (result.equals("OK")) {
            for (int i = 0; i < aCallsModels.size(); i++) {
                this.aCallsList.add(aCallsModels.get(i));
            }

            if (this.prevSize == this.aCallsList.size())
                isMaxSize = true;
            this.prevSize = this.aCallsList.size();
            setCallsTodayMore();

        }
        this.isLoadMore = false;
    }


    @Override
    public void onPause() {
        pause = true;
        super.onPause();
    }

    @Override
    public void onResume() {
        if (pause) {
            getDataDashboard();
            getDataTodoToday();
        }
        super.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
        shimmerLayout.stopShimmer();
    }


    @Override
    public void finishedLogin(String result, String response) {

    }


    @Override
    public void finishSave(String result, String response, String id) {
        if (result.equalsIgnoreCase("OK")) {
//            Log.e("ID LOC", id+" ");
//            dao.delete(id);
        }
    }

    @Override
    public void checkin(String result, String response) {
        if (result.equalsIgnoreCase("OK")) {
            llcheckIn.setVisibility(View.GONE);
            tvMessage.setText("your already check-in at " + getDate());
            tvMessage.setVisibility(View.VISIBLE);
            Toast.makeText(context, "Check-in Success", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void checkout(String result, String response) {
        if (result.equalsIgnoreCase("OK")) {
            llcheckIn.setVisibility(View.GONE);
            tvMessage.setText("You already checkout at " + getDate());
            tvMessage.setVisibility(View.VISIBLE);
            Toast.makeText(context, "Check-out Success", Toast.LENGTH_SHORT).show();
        }
    }
}


