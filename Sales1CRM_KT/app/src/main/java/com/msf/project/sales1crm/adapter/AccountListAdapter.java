package com.msf.project.sales1crm.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;

import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.model.AccountModel;
import com.msf.project.sales1crm.utility.PreferenceUtility;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by christianmacbook on 11/05/18.
 */

public class AccountListAdapter extends BaseAdapter {
    private Context context;
    private Bitmap icon;
    private String url;
    private List<AccountModel> listData;

    public AccountListAdapter(Context context, List<AccountModel> objects) {
        this.context = context;
        this.listData = objects;

        this.url = PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.URL);
    }

    public static abstract class Row {
    }

    // This is for making Section, ex: A B C D etc
    public static final class Section extends Row {
        public final String text;

        public Section(String text) {
            this.text = text;
        }
    }

    // This is for making Row, ex: A B C D etc
    public static final class Item extends Row {
        public final AccountModel text;

        public Item(AccountModel text) {
            this.text = text;
        }

        public AccountModel getItem() {
            return text;
        }
    }

    private List<Row> rows;

    public void setRows(List<Row> rows) {
        this.rows = rows;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return rows.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return rows.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return rows.indexOf(getItem(position));
    }

    @Override
    public int getItemViewType(int position) {
        if (getItem(position) instanceof Section) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public View getView(final int position, View converView, ViewGroup parent) {
        View view = converView;
        final ViewHolder holder;

        final Item item = (Item) getItem(position);
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) parent.getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = (LinearLayout) inflater.inflate(
                    R.layout.row_accountlist, parent, false);
            holder = new ViewHolder();
            holder.tvAccountName = (CustomTextView) view
                    .findViewById(R.id.tvAccountName);
            holder.tvIndustry = (CustomTextView) view
                    .findViewById(R.id.tvIndustry);
            holder.tvPhone = (CustomTextView) view
                    .findViewById(R.id.tvPhone);
            holder.tvCountOpportunity = (CustomTextView) view
                    .findViewById(R.id.tvCountOpportunity);
            holder.tvCountTask = (CustomTextView) view
                    .findViewById(R.id.tvCountTask);
            holder.tvCountEvent = (CustomTextView) view
                    .findViewById(R.id.tvCountEvent);
            holder.ivImage = (CircleImageView) view
                    .findViewById(R.id.ivImage);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        Sales1CRMUtils.Utils.imageLoader.DisplayImage(url + item.text.getImageUrl(),
                holder.ivImage, Bitmap.Config.ARGB_8888, 500, Sales1CRMUtils.TYPE_RECT);

        holder.tvAccountName.setText(item.text.getAccount_name());
        holder.tvIndustry.setText(item.text.getIndustry_name());
        holder.tvPhone.setText(item.text.getPhone());
        holder.tvCountOpportunity.setText(item.text.getCount_opportunity());
        holder.tvCountTask.setText(item.text.getCount_task());
        holder.tvCountEvent.setText(item.text.getCount_event());
        return view;
    }

    private static class ViewHolder {
        CustomTextView tvAccountName, tvIndustry, tvPhone,
                tvCountOpportunity, tvCountTask, tvCountEvent;
        CircleImageView ivImage;

    }
}