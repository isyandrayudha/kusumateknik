package com.msf.project.sales1crm;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.msf.project.sales1crm.adapter.AccountDetailTabAdapter;
import com.msf.project.sales1crm.callback.DashboardCallback;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.model.AbsenceModel;
import com.msf.project.sales1crm.model.AccountModel;
import com.msf.project.sales1crm.model.DashboardFirstModel;
import com.msf.project.sales1crm.model.DashboardSecondModel;
import com.msf.project.sales1crm.model.DashboardThirdModel;
import com.msf.project.sales1crm.model.TotalOpportunityModel;
import com.msf.project.sales1crm.presenter.DashboardPresenter;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.PreferenceUtility;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by christianmacbook on 14/05/18.
 */

public class AccountDetailActivity extends BaseActivity implements DashboardCallback {

    @BindView(R.id.ivBack)
    ImageView ivBack;

    @BindView(R.id.ivEdit)
    ImageView ivEdit;


    @BindView(R.id.tvAccountTitle)
    CustomTextView tvAccountTitle;

    @BindView(R.id.tvLastUpdated)
    CustomTextView tvLastUpdated;

    @BindView(R.id.tvIndustry)
    CustomTextView tvIndustry;

    @BindView(R.id.tvAccountName)
    CustomTextView tvAccountName;

    @BindView(R.id.tvAccountType)
    CustomTextView tvAccountType;

    @BindView(R.id.tvCountOpportunity)
    CustomTextView tvCountOpportunity;

    @BindView(R.id.tvCountTask)
    CustomTextView tvCountTask;

    @BindView(R.id.tvCountEvent)
    CustomTextView tvCountEvent;

    @BindView(R.id.vpAccountDetail)
    ViewPager vpAccountDetail;

    @BindView(R.id.tabAccountDetail)
    TabLayout tabAccountDetail;
    @BindView(R.id.ivDetailPict)
    ImageView ivDetailPict;

    private Context context;
    private AccountDetailTabAdapter adapter;
    private AccountModel accountModel;
    private String url;
    private DashboardPresenter dashboardPresenter;

    static AccountDetailActivity accountDetailActivity;

    public static AccountDetailActivity getInstance() {
        return accountDetailActivity;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accountdetail);

        ButterKnife.bind(this);
        this.context = this;
        this.dashboardPresenter = new DashboardPresenter(context, this);

        accountDetailActivity = this;

        this.url = PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.URL);

        try {
            accountModel = getIntent().getExtras().getParcelable("accountModel");
        } catch (Exception e) {
        }

        initView();
        initData();
    }

    private void initView() {
        dashboardPresenter.setupFirst(ApiParam.API_005);

        tvAccountTitle.setText(accountModel.getAccount_name());
        tvLastUpdated.setText("Dibuat Oleh " + accountModel.getLast_update_name());
        tvAccountName.setText(accountModel.getAccount_name());
        if (accountModel.getType_id().equalsIgnoreCase("0")) {
            tvAccountType.setVisibility(View.GONE);
        } else {
            tvAccountType.setVisibility(View.VISIBLE);
            tvAccountType.setText(accountModel.getType_name());
        }
        tvIndustry.setText(accountModel.getIndustry_name());
        tvCountOpportunity.setText(accountModel.getCount_opportunity());
        tvCountTask.setText(accountModel.getCount_task());
        tvCountEvent.setText(accountModel.getCount_event());

        Log.d("TAG", "URL : " + url + accountModel.getImageUrl());

        Sales1CRMUtils.Utils.imageLoader.DisplayImage(url + accountModel.getImageUrl(),
                ivDetailPict, Bitmap.Config.ARGB_8888, 500, Sales1CRMUtils.TYPE_RECT);

        ivBack.setOnClickListener(click);
//        ivEdit.setOnClickListener(click);


    }

    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.ivBack:
                    finish();
                    break;
//                case R.id.ivEdit:
//                    if(!checkFunction.userAccessControl(4)){
//
//                    } else {
//                        Intent intent = new Intent(context, AccountAddActivity.class);
//                        intent.putExtra("from_detail", true);
//                        intent.putExtra("accountModel", model);
//                        startActivity(intent);
//                    }
//                    break;
                default:
                    break;
            }
        }
    };


    private void initData() {
        Bundle bundle = new Bundle();
        bundle.putParcelable("accountUmum", accountModel);

        CustomTextView tabOne = (CustomTextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabOne.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ico_information, 0, 0);
        tabOne.setGravity(Gravity.CENTER_VERTICAL);
        tabAccountDetail.addTab(tabAccountDetail.newTab().setCustomView(tabOne));

        CustomTextView tabTwo = (CustomTextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabTwo.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ico_address_white, 0, 0);
        tabAccountDetail.addTab(tabAccountDetail.newTab().setCustomView(tabTwo));


//        checkFunction = new CheckFunction(getApplicationContext());
//       if (!checkFunction.userAccessControl(11)){
//            Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
//            // TODO KASIH WARNA
//        } else{
            CustomTextView tabThree = (CustomTextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
            tabThree.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ico_contact_white, 0, 0);
            tabAccountDetail.addTab(tabAccountDetail.newTab().setCustomView(tabThree));
//        }
        tabAccountDetail.setTabGravity(tabAccountDetail.GRAVITY_FILL);

        adapter = new AccountDetailTabAdapter
                (getSupportFragmentManager(), tabAccountDetail.getTabCount(), bundle);
        vpAccountDetail.setOffscreenPageLimit(1);
        vpAccountDetail.setAdapter(adapter);
        vpAccountDetail.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabAccountDetail));
        tabAccountDetail.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                vpAccountDetail.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void finishFirst(String result, DashboardFirstModel model) {
        if(model.getAccount_edit_permission().equalsIgnoreCase("1")){
            ivEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, AccountAddActivity.class);
                    intent.putExtra("from_detail", true);
                    intent.putExtra("accountModel", accountModel);
                    startActivity(intent);
                }
            });
            ivEdit.setVisibility(View.VISIBLE);
        } else {
        ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
            }
        });
        ivEdit.setVisibility(View.GONE);
        }
    }

    @Override
    public void finishSecond(String result, DashboardSecondModel model) {

    }

    @Override
    public void finishThird(String result, List<DashboardThirdModel> listLead, List<DashboardThirdModel> listLead1, List<DashboardThirdModel> listOppor, List<DashboardThirdModel> listOppor1) {

    }

    @Override
    public void TotOpportunity(String result, TotalOpportunityModel totOporModel) {

    }

    @Override
    public void absence(String result, AbsenceModel model) {

    }
}
