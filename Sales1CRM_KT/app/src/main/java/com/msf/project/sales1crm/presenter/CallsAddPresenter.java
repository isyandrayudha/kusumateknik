package com.msf.project.sales1crm.presenter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;

import com.msf.project.sales1crm.callback.CallsAddCallback;
import com.msf.project.sales1crm.model.CallsModel;
import com.msf.project.sales1crm.service.NetworkConnection;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.PreferenceUtility;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;
import com.msf.project.sales1crm.utility.Sales1CRMUtilsJSON;

import org.json.JSONException;
import org.json.JSONObject;

public class CallsAddPresenter {
    private final String TAG = CallsAddPresenter.class.getSimpleName();
    private Context context;
    private Messenger messenger;
    private Message message;
    private Bundle bundle;
    private Handler handler;
    private String failureResponse = "";
    private String[] stringResponse = {""};
    private CallsAddCallback callback;
    private String resultLogin = "NG";
    private String response = "Tolong Periksa Koneksi Anda";

    public CallsAddPresenter(Context context, CallsAddCallback listener) {
        this.context = context;
        this.callback = listener;
    }

    private void doNetworkService(String url, String params, String from) {
        Log.d(TAG, "url login " + url);
        Log.d(TAG, "params : " + params);
        Intent networkIntent = new Intent(this.context, NetworkConnection.class);
        this.messenger = new Messenger(this.handler);
        networkIntent.putExtra("messenger", this.messenger);
        networkIntent.putExtra("url", url);
        networkIntent.putExtra("params", params);
        networkIntent.putExtra("from", from);
        this.context.startService(networkIntent);
    }

    public void setupAdd(int apiIndex, CallsModel callsModel) {
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)) {
            obtainAddAccount(apiIndex, callsModel);
        }else{
            callback.finishAdd(this.resultLogin, this.response);
        }
    }

    public void obtainAddAccount(int apiIndex, CallsModel callsModel) {
        this.handler = new Handler(this.context.getMainLooper()) {

            @Override
            public void handleMessage(Message msg) {
                parseAddAccount(msg);
            }

        };

        JSONObject jsonObject = new JSONObject();
        try {
            //Umum
            jsonObject.put("api_key", PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.API_KEY));
            jsonObject.put("account_id", callsModel.getAccount_id());
            jsonObject.put("call_type_id", callsModel.getCall_type_id());
            jsonObject.put("call_purpose_id", callsModel.getCall_purpose_id());
            jsonObject.put("date", callsModel.getDate());
            jsonObject.put("duration", callsModel.getDuration());
            jsonObject.put("subject", callsModel.getSubject());
            jsonObject.put("detail", callsModel.getDetail());
            jsonObject.put("created_at", callsModel.getCreated_at());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        doNetworkService(ApiParam.urlBuilder(this.context, apiIndex), jsonObject.toString(), "CallAdd");
    }

    private void parseAddAccount(Message message) {
        this.message = message;
        this.bundle = this.message.getData();
        this.stringResponse[0] = this.bundle.getString("network_response");
        this.failureResponse = this.bundle.getString("network_failure");
        Log.d(TAG, "responseString[0] info " + this.stringResponse[0]);
        if (this.failureResponse.equalsIgnoreCase("yes")){
            this.callback.finishAdd(this.resultLogin, this.response);
        }else{
            try {
                this.resultLogin = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                this.callback.finishAdd(this.resultLogin, this.response);
            } catch (JSONException e) {
                Log.d(TAG, "Exception detail Response " + e.getMessage());
            }
        }
    }

    public void setupEdit(int apiIndex, CallsModel callsModel) {
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)) {
            obtainEditAccount(apiIndex, callsModel);
        }else{
            callback.finishEdit(this.resultLogin, this.response);
        }
    }

    public void obtainEditAccount(int apiIndex, CallsModel callsModel) {
        this.handler = new Handler(this.context.getMainLooper()) {

            @Override
            public void handleMessage(Message msg) {
                parseEditAccount(msg);
            }

        };

        JSONObject jsonObject = new JSONObject();
        try {
            //Umum
            jsonObject.put("api_key", PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.API_KEY));
            jsonObject.put("call_id", callsModel.getId());
            jsonObject.put("account_id", callsModel.getAccount_id());
            jsonObject.put("call_type_id", callsModel.getCall_type_id());
            jsonObject.put("call_purpose_id", callsModel.getCall_purpose_id());
            jsonObject.put("date", callsModel.getDate());
            jsonObject.put("duration", callsModel.getDuration());
            jsonObject.put("subject", callsModel.getSubject());
            jsonObject.put("detail", callsModel.getDetail());
            jsonObject.put("created_at", callsModel.getCreated_at());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        doNetworkService(ApiParam.urlBuilder(this.context, apiIndex), jsonObject.toString(), "CallEdit");
    }

    private void parseEditAccount(Message message) {
        this.message = message;
        this.bundle = this.message.getData();
        this.stringResponse[0] = this.bundle.getString("network_response");
        this.failureResponse = this.bundle.getString("network_failure");
        Log.d(TAG, "responseString[0] info " + this.stringResponse[0]);
        if (this.failureResponse.equalsIgnoreCase("yes")){
            this.callback.finishEdit(this.resultLogin, this.response);
        }else{
            try {
                this.resultLogin = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                this.callback.finishEdit(this.resultLogin, this.response);
            } catch (JSONException e) {
                Log.d(TAG, "Exception detail Response " + e.getMessage());
            }
        }
    }
}
