package com.msf.project.sales1crm.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.msf.project.sales1crm.MenuActivity;
import com.msf.project.sales1crm.OpportunityTabListAdapter;
import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.callback.SortDialogCallback;
import com.msf.project.sales1crm.customview.CustomEditText;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.presenter.DashboardPresenter;
import com.msf.project.sales1crm.presenter.OpportunityListActivePresenter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OpportunityTabFragment extends Fragment {


    interface ClickActionFilterListener {
        void onClickItemFilter(int id);
    }

    private ClickActionFilterListener mFilterListener;

    public void setClickActionFilterListener(ClickActionFilterListener listener) {
        this.mFilterListener = listener;
    }

    @BindView(R.id.tabOppor)
    TabLayout tabOppor;
    @BindView(R.id.vpOpportunity)
    ViewPager vpOpportunity;
    @BindView(R.id.etSearch)
    CustomEditText etSearch;


    private View view;
    private Context context;
    private OpportunityTabListAdapter adapter;
    private OpportunityListActivePresenter presenter;
    private MenuActivity myContext;
    private String[] sort = {"Name A to Z", "Name Z to A", "Opportunity Size A to Z", "Opportunity Size Z to A" ,
            "Create Date Ascending", "Create Date Descending", "Due Date Ascending", "Due Date Descending"};

    private int prevSize = 0, sort_id = 0;
    private boolean isLoadMore = true;
    private boolean isMaxSize = true;
    private boolean pause = false;
    private OpportunityActiveFragment opportunityActiveFragment;
    private DashboardPresenter dashboardPresenter;

    public static OpportunityTabFragment newInstance() {
        OpportunityTabFragment fragment = new OpportunityTabFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onAttach(Activity activity) {
        myContext = (MenuActivity) activity;
        super.onAttach(activity);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_opportunitytab, container, false);
        setHasOptionsMenu(true);
        ButterKnife.bind(this.view);
        return this.view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);
        this.context = getActivity();

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Opportunity");

        initData();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_oppor, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_list:
                    MenuActivity.getInstance().setFragment(OpportunityTabFragment.newInstance());
                    item.setIcon(R.drawable.ic_list_white_24dp);
                break;
            case R.id.action_cards:
                    MenuActivity.getInstance().setFragment(OpportunityTabCardFragment.newInstance());
                    item.setIcon(R.drawable.ic_cards_black_24dp);
                break;
//            case R.id.action_maps:
//                    MenuActivity.getInstance().setFragment(OpportunityTabMapFragment.newInstance());
//                    item.setIcon(R.drawable.ic_map_white_24dp);
//                break;
            case R.id.action_viewpipeline:
                    MenuActivity.getInstance().setFragment(Vp_opportunityTab.newInstance());
                break;
            case R.id.action_filter:
                SortDialogFragment dialogFragment = new SortDialogFragment(context, new SortDialogCallback() {
                    @Override
                    public void onDialogCallback(Bundle data) {
                        sort_id = Integer.parseInt(data.getString("id"));
                        mFilterListener.onClickItemFilter(sort_id);
                    }
                }, sort);
                dialogFragment.show(getActivity().getSupportFragmentManager(), "sortDialog");
                break;
            default:
                return super.onOptionsItemSelected(item);

        }
        return true;
    }



    private void initData() {
        CustomTextView tabOne = (CustomTextView) LayoutInflater.from(context).inflate(R.layout.custom_tab, null);
        tabOne.setText("Active");
        tabOppor.addTab(tabOppor.newTab().setCustomView(tabOne));

        CustomTextView tabTwo = (CustomTextView) LayoutInflater.from(context).inflate(R.layout.custom_tab, null);
        tabTwo.setText("Close");
        tabOppor.addTab(tabOppor.newTab().setCustomView(tabTwo));

        tabOppor.setTabGravity(tabOppor.GRAVITY_FILL);

        adapter = new OpportunityTabListAdapter(myContext.getSupportFragmentManager(), tabOppor.getTabCount(), this);
        vpOpportunity.setAdapter(adapter);
        vpOpportunity.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabOppor));
        tabOppor.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                vpOpportunity.setCurrentItem(tab.getPosition(), true);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
//                vpOpportunity.setCurrentItem(tab.getPosition());

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }


    //    private void addTabs(ViewPager viewPager) {
//        ViewPagerAdapter adapter = new ViewPagerAdapter(myContext.getSupportFragmentManager());
//        adapter.addFrag(new OpportunityActiveFragment(), "Active");
//        adapter.addFrag(new OpportunityCloseFragment(), "Close");
//        viewPager.setAdapter(adapter);
//    }
//
//    class ViewPagerAdapter extends FragmentPagerAdapter {
//        private final List<Fragment> mFragmentList = new ArrayList<>();
//        private final List<String> mFragmentTitleList = new ArrayList<>();
//
//        public ViewPagerAdapter(FragmentManager manager) {
//            super(manager);
//        }
//
//        @Override
//        public Fragment getItem(int position) {
//            return mFragmentList.get(position);
//        }
//
//        @Override
//        public int getCount() {
//            return mFragmentList.size();
//        }
//
//        public void addFrag(Fragment fragment, String title) {
//            mFragmentList.add(fragment);
//            mFragmentTitleList.add(title);
//        }
//
//        @Override
//        public CharSequence getPageTitle(int position) {
//            return mFragmentTitleList.get(position);
//        }
//    }
}
