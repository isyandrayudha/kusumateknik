package com.msf.project.sales1crm.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextClock;
import android.widget.Toast;

import com.msf.project.sales1crm.ChangeFotoProfileActivity;
import com.msf.project.sales1crm.LoginActivity;
import com.msf.project.sales1crm.MenuActivity;
import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.SyncMasterActivity;
import com.msf.project.sales1crm.callback.DashboardCallback;
import com.msf.project.sales1crm.callback.LogoutCallback;
import com.msf.project.sales1crm.callback.PresensiCallback;
import com.msf.project.sales1crm.callback.UploadImageCallback;
import com.msf.project.sales1crm.customview.CustomDialog;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.customview.RoundedImageView;
import com.msf.project.sales1crm.model.AbsenceModel;
import com.msf.project.sales1crm.model.DashboardFirstModel;
import com.msf.project.sales1crm.model.DashboardSecondModel;
import com.msf.project.sales1crm.model.DashboardThirdModel;
import com.msf.project.sales1crm.model.TotalOpportunityModel;
import com.msf.project.sales1crm.model.UserModel;
import com.msf.project.sales1crm.presenter.DashboardPresenter;
import com.msf.project.sales1crm.presenter.LogoutPresenter;
import com.msf.project.sales1crm.presenter.PresensiPresenter;
import com.msf.project.sales1crm.presenter.UploadImageProfilePresenter;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.PreferenceUtility;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;
import com.msf.project.sales1crm.utility.Sales1CRMUtilsJSON;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by christianmacbook on 14/05/18.
 */

public class SettingFragment extends Fragment implements UploadImageCallback, PresensiCallback, DashboardCallback,LogoutCallback {

    @BindView(R.id.tvEmail)
    CustomTextView tvEmail;

    @BindView(R.id.tvKeluar)
    CustomTextView tvKeluar;

    @BindView(R.id.llDataMaster)
    LinearLayout llDataMaster;
    @BindView(R.id.ivAccountPicture)
    RoundedImageView ivAccountPicture;
    @BindView(R.id.btn_changeFoto)
    FrameLayout btnChangeFoto;
    @BindView(R.id.tvMessage)
    CustomTextView tvMessage;
    @BindView(R.id.tvbtnCheckIn)
    CustomTextView tvbtnCheckIn;
    @BindView(R.id.tvbtnCheckout)
    CustomTextView tvbtnCheckout;
    @BindView(R.id.llAbsence)
    LinearLayout llAbsence;

    private View view;
    private Context context;
    private UserModel userModel;
    public static boolean shouldRestart = true;
    private DashboardPresenter presenter;
    private PresensiPresenter presensiPresenter;
    private LogoutPresenter logoutPresenter;
    private UploadImageProfilePresenter uPpresenter;
    private String url = "";

    public static SettingFragment newInstance() {
        SettingFragment fragment = new SettingFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_setting, container, false);

        ButterKnife.bind(this, this.view);
        this.uPpresenter = new UploadImageProfilePresenter(context, this);
        this.presenter = new DashboardPresenter(context, this);
        this.presensiPresenter = new PresensiPresenter(context, this);
        this.logoutPresenter = new LogoutPresenter(context, (LogoutCallback) this);
        return this.view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);
        this.context = getActivity();
        this.presenter = new DashboardPresenter(context, this);
        this.presensiPresenter = new PresensiPresenter(context, this);
        this.url = PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.URL);
        this.logoutPresenter = new LogoutPresenter(context, (LogoutCallback) this);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("settings");

        initView();

    }

    private void initView() {
        tvEmail.setText(PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.KEY_EMAIL));
//        Sales1CRMUtils.Utils.imageLoader.DisplayImage(url + PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.KEY_IMAGES_MASTER),
//                ivAccountPicture, Bitmap.Config.ARGB_8888, 200, Sales1CRMUtils.TYPE_RECT);
        Picasso.get().load(url + PreferenceUtility.getInstance()
                .loadDataString(context, PreferenceUtility.KEY_IMAGES_MASTER))
                .error(R.drawable.avatar_placeholder)
                .into(ivAccountPicture);

        tvKeluar.setOnClickListener(click);
        llDataMaster.setOnClickListener(click);
        btnChangeFoto.setOnClickListener(click);
        tvbtnCheckIn.setOnClickListener(click);
        tvbtnCheckout.setOnClickListener(click);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                presenter.setupAbsen(ApiParam.API_122);
            }
        },1000);
    }

//    private void getImageMaster(){
//        if(Sales1CRMUtils.ConnectionUtility.isNetworkConnected(context)) {
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    presenter.setupDisplayImage(ApiParam.API_003);
//                }
//            },1000);
//
//        }
//    }

    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.tvKeluar:
                    final CustomDialog dialogLogout = CustomDialog.setupDialogConfirmation(context, "Apakah Anda ingin keluar?");
                    CustomTextView tvCancel = (CustomTextView) dialogLogout.findViewById(R.id.tvCancel);
                    CustomTextView tvYa = (CustomTextView) dialogLogout.findViewById(R.id.tvOK);

                    tvCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialogLogout.dismiss();
                        }
                    });

                    tvYa.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(Sales1CRMUtils.ConnectionUtility.isNetworkConnected(context)){
                                logoutPresenter.setupLogout(ApiParam.API_133);
                                PreferenceUtility.getInstance().saveData(context, PreferenceUtility.KEY_SYNCMASTER, "0");
                                PreferenceUtility.getInstance().saveData(context, PreferenceUtility.API_KEY, "");
                                Sales1CRMUtilsJSON.JSONUtility.getClearDataFunction(context);
                                shouldRestart = false;
                                dialogLogout.dismiss();
                            }

                        }
                    });
                    dialogLogout.show();
                    break;
                case R.id.llDataMaster:
                    final CustomDialog dialogMaster = CustomDialog.setupDialogConfirmation(context, "Anda ingin melakukan sinkron master data kembali?");
                    CustomTextView tvCancel1 = (CustomTextView) dialogMaster.findViewById(R.id.tvCancel);
                    CustomTextView tvYa1 = (CustomTextView) dialogMaster.findViewById(R.id.tvOK);

                    tvCancel1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialogMaster.dismiss();
                        }
                    });

                    tvYa1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            startActivity(new Intent(context, SyncMasterActivity.class));
                            MenuActivity.getInstance().finish();

                            dialogMaster.dismiss();
                        }
                    });
                    dialogMaster.show();
                    break;
                case R.id.btn_changeFoto:
                    startActivity(new Intent(context, ChangeFotoProfileActivity.class));
                    break;
                case R.id.tvbtnCheckIn:
                    final CustomDialog dialogCheckIn = CustomDialog.setupDialogAbsence(context);
                    TextClock tcClock = (TextClock) dialogCheckIn.findViewById(R.id.tcClock);
                    CustomTextView mtvCancel = (CustomTextView) dialogCheckIn.findViewById(R.id.tvCancel);
                    CustomTextView tvCheckIn = (CustomTextView) dialogCheckIn.findViewById(R.id.tvCheckIn);
                    CustomTextView tvTitle = (CustomTextView) dialogCheckIn.findViewById(R.id.tvTitle);

                    tvTitle.setText("Checkin");
                    tvCheckIn.setText("Checkin");

                    mtvCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialogCheckIn.dismiss();

                        }
                    });

                    tvCheckIn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            presensiPresenter.Checkin(ApiParam.API_123);
                            dialogCheckIn.dismiss();
                        }
                    });
                    dialogCheckIn.show();

                    break;
                case R.id.tvbtnCheckout:
                    final CustomDialog dialogCheckOut = CustomDialog.setupDialogAbsence(context);
                    TextClock ntcClock = (TextClock) dialogCheckOut.findViewById(R.id.tcClock);
                    CustomTextView ntvCancel = (CustomTextView) dialogCheckOut.findViewById(R.id.tvCancel);
                    CustomTextView ntvCheckIn = (CustomTextView) dialogCheckOut.findViewById(R.id.tvCheckIn);
                    CustomTextView ntvTitle = (CustomTextView) dialogCheckOut.findViewById(R.id.tvTitle);

                    ntvTitle.setText("Checkout");
                    ntvCheckIn.setText("Checkout");

                    ntvCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialogCheckOut.dismiss();

                        }
                    });

                    ntvCheckIn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            presensiPresenter.Checkout(ApiParam.API_124);
                            dialogCheckOut.dismiss();
                        }
                    });
                    dialogCheckOut.show();
                    break;
                default:
                    break;
            }
        }
    };

    @Override
    public void onDestroyView() {

        super.onDestroyView();

    }

    @Override
    public void finishUplaod(String result, String response) {

    }

    @Override
    public void displayImage(String result, UserModel userModel) {
//        if(result.equalsIgnoreCase("OK")){
//            Sales1CRMUtils.Utils.imageLoader.DisplayImage(url + userModel.getImage_master(),
//                    ivAccountPicture, Bitmap.Config.ARGB_8888, 200, Sales1CRMUtils.TYPE_RECT);
//            presenter.setupDisplayImage(ApiParam.API_003);
//        } else if(result.equalsIgnoreCase("FAILDE_KEY")) {
//            startActivity(new Intent(context, ErrorAPIKeyActivity.class));
//            MenuActivity.getInstance().finish();
//        }
    }

    @Override
    public void finishFirst(String result, DashboardFirstModel model) {

    }

    @Override
    public void finishSecond(String result, DashboardSecondModel model) {

    }

    @Override
    public void finishThird(String result, List<DashboardThirdModel> listLead, List<DashboardThirdModel> listLead1, List<DashboardThirdModel> listOppor, List<DashboardThirdModel> listOppor1) {

    }

    @Override
    public void TotOpportunity(String result, TotalOpportunityModel totOporModel) {

    }

    @Override
    public void absence(String result, AbsenceModel model) {
        if (result.equalsIgnoreCase("OK")) {
            if (model.getUserType().equalsIgnoreCase("2")) {
                if (model.getCheckIn().equalsIgnoreCase("0") && model.getCheckOut().equalsIgnoreCase("0")) {
                    if (model.getStatus().equalsIgnoreCase("late")) {
                        tvbtnCheckIn.setVisibility(View.VISIBLE);
                        tvbtnCheckout.setVisibility(View.GONE);
                        tvMessage.setVisibility(View.GONE);
                        llAbsence.setVisibility(View.VISIBLE);
                    } else if (model.getStatus().equalsIgnoreCase("notcheckin")) {
                        tvbtnCheckIn.setVisibility(View.VISIBLE);
                        tvbtnCheckout.setVisibility(View.GONE);
                        tvMessage.setText("You are not checkin! Please Checkin!!!");
                        llAbsence.setVisibility(View.VISIBLE);
                    }
                } else if (model.getCheckIn().equalsIgnoreCase("1") && model.getCheckOut().equalsIgnoreCase("0")) {
                    if (model.getStatus().equalsIgnoreCase("notcheckout")) {
                        llAbsence.setVisibility(View.VISIBLE);
                        tvbtnCheckIn.setVisibility(View.GONE);
                        tvbtnCheckout.setVisibility(View.VISIBLE);
                        tvMessage.setText("Please Checkout!!!");
                    } else if (model.getStatus().equalsIgnoreCase("checkin")) {
                        tvMessage.setText(model.getMessage());
                        tvMessage.setVisibility(View.VISIBLE);
                        llAbsence.setVisibility(View.VISIBLE);
                        tvbtnCheckout.setVisibility(View.VISIBLE);
                        tvbtnCheckIn.setVisibility(View.GONE);
                    }
                } else if (model.getCheckIn().equalsIgnoreCase("1") && model.getCheckOut().equalsIgnoreCase("1")) {
                    llAbsence.setVisibility(View.GONE);
                    tvMessage.setText(model.getMessage());
                    tvbtnCheckIn.setVisibility(View.GONE);
                    tvbtnCheckout.setVisibility(View.GONE);
                }
            } else {
                llAbsence.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void checkin(String result, String response) {
        if(result.equalsIgnoreCase("OK")){
            tvMessage.setText("your already check-in at "+getDate());
            Toast.makeText(context, "Check-in Success", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void checkout(String result, String response) {
        if(result.equalsIgnoreCase("OK")){
            tvMessage.setText("You already checkout at " + getDate());
            Toast.makeText(context, "Check-out Success", Toast.LENGTH_SHORT).show();
        }
    }
    private String getDate() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
        return df.format(c.getTime());
    }

    @Override
    public void finishLogout(String result, String response) {
    if (result.equalsIgnoreCase("OK")){
        startActivity(new Intent(context, LoginActivity.class));
    } else {
        Toast.makeText(context, "Logout Failed", Toast.LENGTH_SHORT).show();
    }
    }
}
