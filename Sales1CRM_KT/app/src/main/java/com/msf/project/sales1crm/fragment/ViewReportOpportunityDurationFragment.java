package com.msf.project.sales1crm.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Spinner;

import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.adapter.OpportunityDurationAdapter;
import com.msf.project.sales1crm.callback.OpportunityDurationCallback;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.customview.SpinnerCustomAdapter;
import com.msf.project.sales1crm.model.TotalModel;
import com.msf.project.sales1crm.presenter.ViewReportOpportunityDurationPresenter;
import com.msf.project.sales1crm.service.GPSTracker;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ViewReportOpportunityDurationFragment extends Fragment implements OpportunityDurationCallback {

    @BindView(R.id.spMonth1)
    Spinner spMonth1;

    @BindView(R.id.spYear1)
    Spinner spYear1;

    @BindView(R.id.lvCloseWonIndustry)
    ListView lvCloseWonIndustry;

    @BindView(R.id.lvCloseLoseIndustry)
    ListView lvCloseLoseIndustry;

    @BindView(R.id.lvCloseWonSource)
    ListView lvCloseWonSource;

    @BindView(R.id.lvCloseLoseSource)
    ListView lvCloseLoseSource;

    @BindView(R.id.tvSubmit)
    CustomTextView tvSubmit;

    private View view;
    private Context context;
    private ViewReportOpportunityDurationPresenter presenter;
    private SpinnerCustomAdapter spinnerCustomAdapter;
    private OpportunityDurationAdapter adapter, adapter1, adapter2, adapter3;
    private List<TotalModel> list = new ArrayList<>();
    private List<TotalModel> list1 = new ArrayList<>();
    private String[] months = {"Choose Month", "January", "February", "March", "April", "May", "Juni", "July", "August", "September", "October", "November", "Desember"};
    private String[] years = {"Choose Year", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019"};
    private String month_1="", year_1="";

    private DecimalFormat df = (DecimalFormat) DecimalFormat.getCurrencyInstance();
    private DecimalFormatSymbols dfs = new DecimalFormatSymbols();

    public static ViewReportTotalCallsFragment newInstance() {
        ViewReportTotalCallsFragment fragment = new ViewReportTotalCallsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_opportunityduration, container, false);

        return this.view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);
        this.context = getActivity();

        this.presenter = new ViewReportOpportunityDurationPresenter(context, this);

        initData();
    }

    private void initData(){
        spinnerCustomAdapter = new SpinnerCustomAdapter(context,
                R.layout.spinner_default, years);
        spinnerCustomAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spYear1.setAdapter(spinnerCustomAdapter);
        spYear1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                if (position != 0){
                    year_1 = years[position];
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerCustomAdapter = new SpinnerCustomAdapter(context,
                R.layout.spinner_default, months);
        spinnerCustomAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spMonth1.setAdapter(spinnerCustomAdapter);
        spMonth1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                if (position != 0){
                    month_1 = String.valueOf(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        tvSubmit.setOnClickListener(click);
    }

    public void getDataFromAPI(){
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(context)){
            GPSTracker gpsTracker = new GPSTracker(context);
            if (gpsTracker.canGetLocation()) {
                presenter.setupTotal(ApiParam.API_103, month_1, year_1);
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gpsTracker.showSettingsAlert();
            }
        }
    }

    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.tvSubmit:
                    getDataFromAPI();
                    break;
            }
        }
    };

    @Override
    public void finished(String result, List<TotalModel> wonIndustry, List<TotalModel> loseIndustry, List<TotalModel> wonSource, List<TotalModel> loseSource) {
        if (result.equalsIgnoreCase("OK")){
            if (wonIndustry.size() > 0){
                adapter = new OpportunityDurationAdapter(context, wonIndustry);
                lvCloseWonIndustry.setAdapter(adapter);
                lvCloseWonIndustry.setSelector(R.drawable.transparent_selector);
                lvCloseWonIndustry.setOverScrollMode(View.OVER_SCROLL_NEVER);
                lvCloseWonIndustry.setOnTouchListener(new View.OnTouchListener() {
                    // Setting on Touch Listener for handling the touch inside
                    // ScrollView
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        // Disallow the touch request for parent scroll on touch of
                        // child view
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        return false;
                    }
                });
                lvCloseWonIndustry.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                    }
                });
            }

            if (loseIndustry.size() > 0){
                adapter1 = new OpportunityDurationAdapter(context, loseIndustry);
                lvCloseLoseIndustry.setAdapter(adapter1);
                lvCloseLoseIndustry.setSelector(R.drawable.transparent_selector);
                lvCloseLoseIndustry.setOverScrollMode(View.OVER_SCROLL_NEVER);
                lvCloseLoseIndustry.setOnTouchListener(new View.OnTouchListener() {
                    // Setting on Touch Listener for handling the touch inside
                    // ScrollView
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        // Disallow the touch request for parent scroll on touch of
                        // child view
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        return false;
                    }
                });
                lvCloseLoseIndustry.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                    }
                });
            }

            if (wonSource.size() > 0){
                adapter2 = new OpportunityDurationAdapter(context, wonSource);
                lvCloseWonSource.setAdapter(adapter2);
                lvCloseWonSource.setSelector(R.drawable.transparent_selector);
                lvCloseWonSource.setOverScrollMode(View.OVER_SCROLL_NEVER);
                lvCloseWonSource.setOnTouchListener(new View.OnTouchListener() {
                    // Setting on Touch Listener for handling the touch inside
                    // ScrollView
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        // Disallow the touch request for parent scroll on touch of
                        // child view
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        return false;
                    }
                });
                lvCloseWonSource.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                    }
                });
            }

            if (loseSource.size() > 0){
                adapter3 = new OpportunityDurationAdapter(context, loseSource);
                lvCloseLoseSource.setAdapter(adapter3);
                lvCloseLoseSource.setSelector(R.drawable.transparent_selector);
                lvCloseLoseSource.setOverScrollMode(View.OVER_SCROLL_NEVER);
                lvCloseLoseSource.setOnTouchListener(new View.OnTouchListener() {
                    // Setting on Touch Listener for handling the touch inside
                    // ScrollView
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        // Disallow the touch request for parent scroll on touch of
                        // child view
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        return false;
                    }
                });
                lvCloseLoseSource.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                    }
                });
            }
        }
    }
}
