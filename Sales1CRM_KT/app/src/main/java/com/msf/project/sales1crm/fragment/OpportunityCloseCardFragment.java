package com.msf.project.sales1crm.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.msf.project.sales1crm.ErrorAPIKeyActivity;
import com.msf.project.sales1crm.MenuActivity;
import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.adapter.OpportunityListCardAdapter;
import com.msf.project.sales1crm.callback.OpportunityListCloseCallback;
import com.msf.project.sales1crm.customview.CustomEditText;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.imageloader.ImageLoader;
import com.msf.project.sales1crm.model.OpportunityModel;
import com.msf.project.sales1crm.presenter.OpportunityListClosePresenter;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class OpportunityCloseCardFragment extends Fragment implements OpportunityListCloseCallback, OpportunityTabCardFragment.ClickActionFilterListener {

    @BindView(R.id.etSearch)
    CustomEditText etSearch;
    @BindView(R.id.ivClear)
    ImageView ivClear;
    @BindView(R.id.tvRefresh)
    CustomTextView tvRefresh;
    @BindView(R.id.rlNoConnection)
    RelativeLayout rlNoConnection;
    @BindView(R.id.llNotFound)
    LinearLayout llNotFound;
    //    @BindView(R.id.rlProgress)
//    RelativeLayout rlProgress;
    @BindView(R.id.lvOpportunity)
    RecyclerView lvOpportunity;
        @BindView(R.id.lvOpportunitySwipe)
        SwipeRefreshLayout lvOpportunitySwipe;
    @BindView(R.id.rlLeadList)
    RelativeLayout rlLeadList;
    Unbinder unbinder;
    @BindView(R.id.shimmerLayout)
    ShimmerFrameLayout shimmerLayout;
    @BindView(R.id.llshim)
    LinearLayout llshim;
    private View view;
    private Context context;
    private OpportunityListClosePresenter presenter;
    private OpportunityListCardAdapter adapter;
    private List<OpportunityModel> list = new ArrayList<>();

    private int prevSize = 0, sort_id = 0;
    private boolean isLoadMore = true;
    private boolean isMaxSize = true;
    private boolean pause = false;

    public static OpportunityCloseCardFragment newInstance() {
        OpportunityCloseCardFragment fragment = new OpportunityCloseCardFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_opportunityclosecard, container, false);
        ButterKnife.bind(this, this.view);
        return this.view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        this.context = getActivity();
        this.presenter = new OpportunityListClosePresenter(context, this);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("opportunity");

        initData();
    }

    private void initData() {
        Sales1CRMUtils.Utils.imageLoader = new ImageLoader(context);
        etSearch.clearFocus();
//        lvOpportunity.setOnScrollListener(new OpportunityCloseCardFragment.ListLaztLoad());
        lvOpportunitySwipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                llshim.setVisibility(View.VISIBLE);
                shimmerLayout.setVisibility(View.VISIBLE);
                shimmerLayout.startShimmer();
                getDataFromAPI(0, sort_id);
            }
        });
        setTextWatcherForSearch();
        getDataFromAPI(0,0);

        ivClear.setOnClickListener(click);
        tvRefresh.setOnClickListener(click);
    }

    private void setTextWatcherForSearch() {
        etSearch.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    if (etSearch.getText().length() == 0) {
                        ivClear.setVisibility(View.VISIBLE);
                    }
                } else {
                    if (etSearch.getText().length() == 0) {
                        ivClear.setVisibility(View.GONE);
                    } else {
                        ivClear.setVisibility(View.VISIBLE);
                    }

                }
            }
        });


        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_GO) {
                    llshim.setVisibility(View.VISIBLE);
                    shimmerLayout.setVisibility(View.VISIBLE);
                    shimmerLayout.startShimmer();
                    getDataFromAPI(0,0);
                }
                return false;
            }
        });

    }

    public void getDataFromAPI(int prev, int sort_id) {
        isMaxSize = true;
        isLoadMore = true;

        llNotFound.setVisibility(View.GONE);

        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(context)) {
            presenter.setupList(ApiParam.API_043, prev,sort_id, etSearch.getText().toString());
        } else {
            lvOpportunitySwipe.setVisibility(View.GONE);
            rlNoConnection.setVisibility(View.VISIBLE);
        }
    }

    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.ivClear:
                    etSearch.setText("");
                    etSearch.clearFocus();
                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);
                    getDataFromAPI(0,0);
                    break;
                case R.id.tvRefresh:
//                    lvOpportunitySwipe.setVisibility(View.VISIBLE);
                    rlNoConnection.setVisibility(View.GONE);
                    getDataFromAPI(0,0);
                    break;
                default:
                    break;
            }
        }
    };

    public void setList() {
        llshim.setVisibility(View.VISIBLE);
        shimmerLayout.setVisibility(View.GONE);
        adapter = new OpportunityListCardAdapter(context, this.list);
        List<OpportunityListCardAdapter.Row> rows = new ArrayList<OpportunityListCardAdapter.Row>();
        for (OpportunityModel cardModel : list) {
            rows.add(new OpportunityListCardAdapter.Item(cardModel));
        }
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        lvOpportunity.setLayoutManager(layoutManager);
        adapter.setRows(rows);
        lvOpportunity.setAdapter(adapter);
        llshim.setVisibility(View.INVISIBLE);
        shimmerLayout.stopShimmer();
        shimmerLayout.setVisibility(View.INVISIBLE);
    }

    public void setMoreList() {
        List<OpportunityListCardAdapter.Row> rows = new ArrayList<OpportunityListCardAdapter.Row>();
        for (OpportunityModel country : list) {
            rows.add(new OpportunityListCardAdapter.Item(country));
        }
        adapter.setRows(rows);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void finishOpporList(String result, List<OpportunityModel> list) {
        lvOpportunitySwipe.setRefreshing(false);
//        rlProgress.setVisibility(View.GONE);
        if (result.equalsIgnoreCase("OK")) {
            prevSize = list.size();
            this.list = list;

            isMaxSize = false;
            isLoadMore = false;

            setList();
            if (list.size() > 0) {
                llNotFound.setVisibility(View.GONE);
            } else {
                llNotFound.setVisibility(View.VISIBLE);
            }
        } else if (result.equalsIgnoreCase("FAILED_KEY")) {
            startActivity(new Intent(context, ErrorAPIKeyActivity.class));
            MenuActivity.getInstance().finish();
        } else {
//            lvOpportunitySwipe.setVisibility(View.GONE);
            rlNoConnection.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void finishOpporMoreList(String result, List<OpportunityModel> list) {
        if (result.equalsIgnoreCase("OK")) {
            for (int i = 0; i < list.size(); i++) {
                this.list.add(list.get(i));
            }
            if (this.prevSize == this.list.size())
                isMaxSize = true;
            this.prevSize = this.list.size();
            setMoreList();
        }
        this.isLoadMore = false;
    }

    @Override
    public void onClickItemFilter(Integer id) {
        getDataFromAPI(0,0);
    }

    private class ListLaztLoad implements AbsListView.OnScrollListener {

        @Override
        public void onScrollStateChanged(AbsListView absListView, int i) {

        }

        @Override
        public void onScroll(AbsListView absListView, int i, int i1, int i2) {
            if (isLoadMore == false && isMaxSize == false) {
                isLoadMore = true;
                prevSize = list.size();

                presenter.setupMoreList(ApiParam.API_043, prevSize,sort_id, etSearch.getText().toString());
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onPause() {
        pause = true;
        super.onPause();
    }

    @Override
    public void onResume() {
        if (pause)
            getDataFromAPI(0,0);
        super.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
        shimmerLayout.stopShimmer();
    }
}
