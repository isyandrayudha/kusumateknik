package com.msf.project.sales1crm.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.msf.project.sales1crm.model.NoVisitModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by christianmacbook on 24/05/18.
 */

public class NoVisitDao extends BaseDao<NoVisitModel> implements DBSchema.NoVisitReason {

    public NoVisitDao(Context c) {
        super(c, TABLE_NAME);
    }

    public NoVisitDao(Context c, boolean willWrite) {
        super(c, TABLE_NAME, willWrite);
    }

    public NoVisitDao(DBHelper db) {
        super(db, TABLE_NAME);
    }

    public NoVisitDao(DBHelper db, boolean willWrite) {
        super(db, TABLE_NAME, willWrite);
    }

    public NoVisitModel getById(int id) {
        String qry = "SELECT * FROM " + getTable() + " WHERE " + COL_ID + " = " + id;
        Cursor c = getSqliteDb().rawQuery(qry, null);
        NoVisitModel model = new NoVisitModel();
        try {
            if(c != null && c.moveToFirst()) {
                model = getByCursor(c);
            }
        } finally {
            c.close();
        }
        return model;
    }

    public List<NoVisitModel> getNoVisitList() {
        String query = "SELECT * FROM " + getTable();

        Cursor c = getSqliteDb().rawQuery(query, null);
        List<NoVisitModel> list = new ArrayList<>();
        try {
            if(c != null && c.moveToFirst()) {
                list.add(getByCursor(c));
                while (c.moveToNext()) {
                    list.add(getByCursor(c));

                }
            }
        } finally {
            c.close();
        }
        return list;
    }

    @Override
    public NoVisitModel getByCursor(Cursor c) {
        NoVisitModel model = new NoVisitModel();
        model.setId(c.getString(0));
        model.setName(c.getString(1));
        return model;
    }

    @Override
    protected ContentValues upDataValues(NoVisitModel model, boolean update) {
        ContentValues cv = new ContentValues();
        if (update == true)
            cv.put(COL_ID, model.getId());
        cv.put(KEY_NAME, model.getName());
        return cv;
    }
}
