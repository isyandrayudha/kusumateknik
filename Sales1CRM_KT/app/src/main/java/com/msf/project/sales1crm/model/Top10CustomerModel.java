package com.msf.project.sales1crm.model;

public class Top10CustomerModel {
    private String account_id;
    private String account_name;
    private String account_revenue;
    private String stage_id;

    public String getAccount_id() {
        return account_id;
    }

    public void setAccount_id(String account_id) {
        this.account_id = account_id;
    }

    public String getAccount_name() {
        return account_name;
    }

    public void setAccount_name(String account_name) {
        this.account_name = account_name;
    }

    public String getAccount_revenue() {
        return account_revenue;
    }

    public void setAccount_revenue(String account_revenue) {
        this.account_revenue = account_revenue;
    }

    public String getStage_id() {
        return stage_id;
    }

    public void setStage_id(String stage_id) {
        this.stage_id = stage_id;
    }
}
