package com.msf.project.sales1crm.model;

public class AbsenceModel{
	private String checkOut;
	private String userType;
	private String checkInTime;
	private String checkIn;
	private String checkOutTime;
	private String workingTimeEnd;
	private String buttonCheckOut;
	private String buttonCheckIn;
	private String message;
	private String workingTimeStart;
	private String status;

	public void setCheckOut(String checkOut){
		this.checkOut = checkOut;
	}

	public String getCheckOut(){
		return checkOut;
	}

	public void setUserType(String userType){
		this.userType = userType;
	}

	public String getUserType(){
		return userType;
	}

	public void setCheckInTime(String checkInTime){
		this.checkInTime = checkInTime;
	}

	public String getCheckInTime(){
		return checkInTime;
	}

	public void setCheckIn(String checkIn){
		this.checkIn = checkIn;
	}

	public String getCheckIn(){
		return checkIn;
	}

	public void setCheckOutTime(String checkOutTime){
		this.checkOutTime = checkOutTime;
	}

	public String getCheckOutTime(){
		return checkOutTime;
	}

	public void setWorkingTimeEnd(String workingTimeEnd){
		this.workingTimeEnd = workingTimeEnd;
	}

	public String getWorkingTimeEnd(){
		return workingTimeEnd;
	}

	public void setButtonCheckOut(String buttonCheckOut){
		this.buttonCheckOut = buttonCheckOut;
	}

	public String getButtonCheckOut(){
		return buttonCheckOut;
	}

	public void setButtonCheckIn(String buttonCheckIn){
		this.buttonCheckIn = buttonCheckIn;
	}

	public String getButtonCheckIn(){
		return buttonCheckIn;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setWorkingTimeStart(String workingTimeStart){
		this.workingTimeStart = workingTimeStart;
	}

	public String getWorkingTimeStart(){
		return workingTimeStart;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
