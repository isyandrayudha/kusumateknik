package com.msf.project.sales1crm;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.msf.project.sales1crm.adapter.OpportunityDetailTabNewAdapter;
import com.msf.project.sales1crm.callback.DashboardCallback;
import com.msf.project.sales1crm.callback.OpportunityDetailCallback;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.model.AbsenceModel;
import com.msf.project.sales1crm.model.DashboardFirstModel;
import com.msf.project.sales1crm.model.DashboardSecondModel;
import com.msf.project.sales1crm.model.DashboardThirdModel;
import com.msf.project.sales1crm.model.OpportunityModel;
import com.msf.project.sales1crm.model.TotalOpportunityModel;
import com.msf.project.sales1crm.presenter.DashboardPresenter;
import com.msf.project.sales1crm.presenter.OpportunityDetailPresenter;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OpportunityDetailByStageActivity extends BaseActivity implements OpportunityDetailCallback, DashboardCallback {

    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.tvOpportunityNameTitle)
    CustomTextView tvOpportunityNameTitle;
    @BindView(R.id.ivEdit)
    ImageView ivEdit;
    @BindView(R.id.tvOpportunityName)
    CustomTextView tvOpportunityName;
    @BindView(R.id.tvAccountName)
    CustomTextView tvAccountName;
    @BindView(R.id.tvOpportunityType)
    CustomTextView tvOpportunityType;
    @BindView(R.id.tvOpportunityStatus)
    CustomTextView tvOpportunityStatus;
    @BindView(R.id.tvLastUpdated)
    CustomTextView tvLastUpdated;
    @BindView(R.id.tvProgress)
    CustomTextView tvProgress;
    @BindView(R.id.tvSize)
    CustomTextView tvSize;
    @BindView(R.id.tvCloseDate)
    CustomTextView tvCloseDate;
    @BindView(R.id.tabOpporDetail)
    TabLayout tabOpporDetail;
    @BindView(R.id.vpOpporDetail)
    ViewPager vpOpporDetail;
    @BindView(R.id.tvrefNumber)
    CustomTextView tvrefNumber;
    private OpportunityModel models;
    private Context context;
    private String url, iterated;
    private int size;
    private String[] type = {"opportunity Type", "None", "New Business", "Ext Business"};
    private OpportunityDetailTabNewAdapter adapter;
    private OpportunityDetailPresenter presenter;
    private DashboardPresenter dashboardPresenter;
    private String opportunityId, progress, stageID;
    static OpportunityDetailByStageActivity opportunityDetailByStageActivity;

    public static OpportunityDetailByStageActivity getInstance() {
        return opportunityDetailByStageActivity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_opportunity_detail_by_stage);
        ButterKnife.bind(this);
        this.context = this;
        this.presenter = new OpportunityDetailPresenter(context, this);
        this.dashboardPresenter = new DashboardPresenter(context, this);
        opportunityDetailByStageActivity = this;
        try {
            models = getIntent().getExtras().getParcelable("opportunityModel");
            opportunityId = getIntent().getExtras().getString("id");
        } catch (Exception e) {

        }

        Log.i("TAG",
                "number: "
                        + coolFormat(Double.parseDouble(models
                        .getSize()), 0));
        Log.d("TAG", "model " + models.getId());
        Log.d("TAG", "PROGRESS_ : " + progress);
        Log.d("TAG", "STAGEID_ : " + stageID);

        intiview();
        initData();
    }

    private void intiview() {
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(context)) {
            dashboardPresenter.setupFirst(ApiParam.API_005);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    presenter.setupDetail(ApiParam.API_126, opportunityId);
                }
            }, 1000);
        } else {
            Toast.makeText(context, "No connection", Toast.LENGTH_SHORT).show();
        }
    }

    private void initData() {
        Bundle bundle = new Bundle();
        bundle.putParcelable("opportunityModel", models);
        CustomTextView tabOne = (CustomTextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabOne.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ico_information, 0, 0);
        tabOpporDetail.addTab(tabOpporDetail.newTab().setCustomView(tabOne));

        CustomTextView tabTwo = (CustomTextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabTwo.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ico_product_white, 0, 0);
        tabOpporDetail.addTab(tabOpporDetail.newTab().setCustomView(tabTwo));

        tabOpporDetail.setTabGravity(tabOpporDetail.GRAVITY_FILL);

        adapter = new OpportunityDetailTabNewAdapter
                (getSupportFragmentManager(), tabOpporDetail.getTabCount(), bundle);
        vpOpporDetail.setOffscreenPageLimit(1);
        vpOpporDetail.setAdapter(adapter);
        vpOpporDetail.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabOpporDetail));
        tabOpporDetail.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                vpOpporDetail.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }


    @Override
    public void finishDetail(String result, OpportunityModel models) {
        if (result.equalsIgnoreCase("OK")) {
            tvOpportunityNameTitle.setText(models.getName());
            tvOpportunityName.setText(models.getName());
            tvAccountName.setText("account Name : " + models.getAccount_name());
            tvOpportunityStatus.setText(models.getOppor_status_name());
            tvOpportunityType.setText(type[Integer.parseInt(models.getType())]);
            tvLastUpdated.setText("Added by " + models.getOwner_name());
            tvSize.setText(size + iterated);
            tvProgress.setText(models.getProgress() + "%");
            tvCloseDate.setText(models.getClose_date_interval());
            tvrefNumber.setText(models.getComplete_ref_number());


            progress = models.getProgress();
            stageID = models.getStage_id();
            Log.d("TAG","oppor_status_id  == " + models.getOppor_status_id());
//            Toast.makeText(context, "progress " + progress, Toast.LENGTH_SHORT).show();


            ivBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });


            ivEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(context, OpportunityAddActivity.class);
                    i.putExtra("from_detail", true);
                    i.putExtra("byStatgeActivity", true);
                    i.putExtra("opportunityModel", models);
                    startActivity(i);
                }
            });

//            if (models.getProgress().equalsIgnoreCase("100") ||
//                    (models.getProgress().equalsIgnoreCase("0"))) {
//                ivEdit.setVisibility(View.GONE);
//            } else {
//                ivEdit.setVisibility(View.VISIBLE);
//                ivEdit.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        Intent i = new Intent(context, OpportunityAddActivity.class);
//                        i.putExtra("from_detail", true);
//                        i.putExtra("byStatgeActivity", true);
//                        i.putExtra("opportunityModel", models);
//                        startActivity(i);
//                    }
//                });
//            }


        }
    }


    private char[] c = new char[]{'K', 'M', 'B', 'T'};

    private String coolFormat(double n, int iteration) {
        double d = ((long) n / 100) / 10.0;
        boolean isRound = (d * 10) % 10 == 0;// true if the decimal part is
        // equal to 0 (then it's trimmed
        // anyway)
        Log.i("TAG", "num : " + d);
        size = (int) d;
        iterated = String.valueOf(c[iteration]);
        Log.i("TAG", "iteration : " + c[iteration]);
        return (d < 1000 ? // this determines the class, i.e. 'k', 'm' etc
                ((d > 99.9 || isRound || (!isRound && d > 9.99) ? // this decides
                        // whether to trim
                        // the decimals
                        (int) d * 10 / 10
                        : d + "" // (int) d * 10 / 10 drops the decimal
                ) + "" + c[iteration])
                : coolFormat(d, iteration + 1));
    }

    @Override
    public void finishFirst(String result, DashboardFirstModel model) {
        Log.d("TAG", "stage : " + models.getProgress());
        Log.d("TAG","oppor_status_id : " + models.getOppor_status_id());
        if (model.getOpportunity_edit_permission().equalsIgnoreCase("1")) {
            if (models.getProgress().equalsIgnoreCase("100") ||
                    models.getProgress().equalsIgnoreCase("0") &&
                            !models.getStage_id().equalsIgnoreCase("1")) {
                ivEdit.setVisibility(View.GONE);
            } else {
                ivEdit.setVisibility(View.VISIBLE);
            }

        } else {
            ivEdit.setVisibility(View.GONE);
            ivEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    @Override
    public void finishSecond(String result, DashboardSecondModel model) {

    }

    @Override
    public void finishThird(String result, List<DashboardThirdModel> listLead, List<DashboardThirdModel> listLead1, List<DashboardThirdModel> listOppor, List<DashboardThirdModel> listOppor1) {

    }

    @Override
    public void TotOpportunity(String result, TotalOpportunityModel totOporModel) {

    }

    @Override
    public void absence(String result, AbsenceModel model) {

    }
}
