package com.msf.project.sales1crm.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.callback.OpportunityDetailCallback;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.model.OpportunityModel;
import com.msf.project.sales1crm.presenter.OpportunityDetailPresenter;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OpportunityTabPersonalNewFragment extends Fragment implements OpportunityDetailCallback {


    @BindView(R.id.tvAssignTo)
    CustomTextView tvAssignTo;
    @BindView(R.id.tvNextStep)
    CustomTextView tvNextStep;
    @BindView(R.id.tvLead)
    CustomTextView tvLead;
    @BindView(R.id.tvLeadSource)
    CustomTextView tvLeadSource;
    @BindView(R.id.tvStage)
    CustomTextView tvStage;
    @BindView(R.id.tvProgress)
    CustomTextView tvProgress;
    @BindView(R.id.tvDescription)
    CustomTextView tvDescription;
    @BindView(R.id.tvEmail)
    CustomTextView tvEmail;
    @BindView(R.id.llEmail)
    LinearLayout llEmail;
    @BindView(R.id.tvPhone)
    CustomTextView tvPhone;
    @BindView(R.id.llPhone)
    LinearLayout llPhone;
    @BindView(R.id.tvReason)
    CustomTextView tvReason;
    @BindView(R.id.llReason)
    LinearLayout llReason;

    private View view;
    private Context context;
    private OpportunityModel model;
    private OpportunityDetailPresenter presenter;
    private LinearLayout llCall, llSMS, llWhatsapp;

    public OpportunityTabPersonalNewFragment() {
        // Required empty public constructor
    }


    public static OpportunityTabPersonalNewFragment newInstance() {
        OpportunityTabPersonalNewFragment fragment = new OpportunityTabPersonalNewFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_opportunity_tab_personal_new, container, false);
        ButterKnife.bind(this, this.view);
        return this.view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, this.view);
        this.context = getActivity();
        this.presenter = new OpportunityDetailPresenter(context, this);
        initView();
    }

    private void initView() {
        try {
            model = getArguments().getParcelable("opportunityModel");
        } catch (Exception e) {
        }
        Log.d("TAG", "ID :" + model.getId());

        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(context)) {
            presenter.setupDetail(ApiParam.API_126, model.getId());
        } else {

        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void finishDetail(String result, OpportunityModel model) {
        if (result.equalsIgnoreCase("OK")) {
            tvAssignTo.setText(model.getAssign_to_name());
            tvNextStep.setText(model.getNext_step());
//        tvPosition.setText(model.getPosition_name());
            tvLeadSource.setText(model.getSource_name());
            tvStage.setText(model.getStage_name());
            tvDescription.setText(model.getDescription());
            tvProgress.setText(model.getProgress() + "%");

            if (!model.getEmail().equalsIgnoreCase("")) {
                tvEmail.setText(model.getEmail());
            } else {
                llEmail.setVisibility(View.GONE);
            }

            if (!model.getPhone().equalsIgnoreCase("")) {
                tvPhone.setText(model.getPhone());
            } else {
                llPhone.setVisibility(View.GONE);
            }

            if(model.getStage_id().equalsIgnoreCase("8")){
                tvReason.setText(model.getReason_not_deal());
                llReason.setVisibility(View.VISIBLE);
            } else {
                llReason.setVisibility(View.GONE);
            }

            llEmail.setOnClickListener(click);
            llPhone.setOnClickListener(click);

        }

    }

    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.llEmail:
                    if (!model.getEmail().equalsIgnoreCase("")) {
                        String mailto = "mailto:" + model.getEmail();

                        Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
                        emailIntent.setData(Uri.parse(mailto));

                        try {
                            startActivity(Intent.createChooser(emailIntent, "Pilih Email"));
                        } catch (ActivityNotFoundException e) {
                            //TODO: Handle case where no email app is available
                        }
                    } else {
                        llEmail.setVisibility(View.GONE);
                    }
                    break;
                case R.id.llPhone:
                    final Dialog dialog = new Dialog(context);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.dialog_call);

                    llCall = (LinearLayout) dialog
                            .findViewById(R.id.llCall);
                    llSMS = (LinearLayout) dialog
                            .findViewById(R.id.llSMS);
                    llWhatsapp = (LinearLayout) dialog
                            .findViewById(R.id.llWhatsapp);

                    llCall.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (ContextCompat.checkSelfPermission(context,
                                    Manifest.permission.CALL_PHONE)
                                    != PackageManager.PERMISSION_GRANTED) {

                                ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.CALL_PHONE}, Sales1CRMUtils.REQUEST_PHONE_CALL);
                            } else {
                                callNumber(model.getPhone());
                            }
                            dialog.dismiss();
                        }
                    });

                    llSMS.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            sendSMS(model.getPhone());
                            dialog.dismiss();
                        }
                    });

                    llWhatsapp.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent();
                            openWhatsApp(model.getPhone());
                        }
                    });
                    dialog.show();
                    break;
            }
        }
    };

    private void openWhatsApp(String number) {
        final String appPackageName = "com.whatsapp";

        if (Sales1CRMUtils.Utils.appInstalledOrNot(context, appPackageName)) {
            number = PhoneNumberUtils.formatNumber(number).replace("-", "");
            if (number.substring(0, 2).equalsIgnoreCase("08")) {
                number = "62" + number.substring(1);
            } else {
                number = number.replace(" ", "").replace("+", "");
            }

            Log.d("TAG", "Number : " + number);
            try {
                Intent sendIntent = new Intent("android.intent.action.MAIN");
                sendIntent.setComponent(new ComponentName("com.whatsapp", "com.whatsapp.Conversation"));
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.setType("text/plain");
                sendIntent.putExtra(Intent.EXTRA_TEXT, "");
                sendIntent.putExtra("jid", number + "@s.whatsapp.net");
                sendIntent.setPackage("com.whatsapp");
                startActivity(sendIntent);
            } catch (Exception e) {
                Toast.makeText(context, "Error/n" + e.toString(), Toast.LENGTH_SHORT).show();
            }
        } else {
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
            } catch (ActivityNotFoundException anfe) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
            }
        }
    }

    private void callNumber(String number) {
        PackageManager packageManager = context.getPackageManager();
        if (packageManager.hasSystemFeature(PackageManager.FEATURE_TELEPHONY)) {
            // call phone
            Intent intent = new Intent(Intent.ACTION_CALL);

            intent.setData(Uri.parse("tel:" + number));

            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            startActivity(intent);
        }
    }

    private void sendSMS(String number) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("smsto:" + Uri.encode(number)));
        startActivity(intent);
    }
}

