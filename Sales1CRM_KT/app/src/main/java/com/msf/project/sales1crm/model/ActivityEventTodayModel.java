package com.msf.project.sales1crm.model;

public class ActivityEventTodayModel{
	private String date;
	private String microsoftCalendarId;
	private String assignTo;
	private String description;
	private String createdAt;
	private String deletedBy;
	private String createdBy;
	private String deletedAt;
	private String googleCalendarId;
	private String accountId;
	private String updatedAt;
	private String name;
	private String updatedBy;
	private String id;
	private String status;
	private String user_name;
	private String account_name;

	public void setDate(String date){
		this.date = date;
	}

	public String getDate(){
		return date;
	}

	public void setMicrosoftCalendarId(String microsoftCalendarId){
		this.microsoftCalendarId = microsoftCalendarId;
	}

	public String getMicrosoftCalendarId(){
		return microsoftCalendarId;
	}

	public void setAssignTo(String assignTo){
		this.assignTo = assignTo;
	}

	public String getAssignTo(){
		return assignTo;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setDeletedBy(String deletedBy){
		this.deletedBy = deletedBy;
	}

	public String getDeletedBy(){
		return deletedBy;
	}

	public void setCreatedBy(String createdBy){
		this.createdBy = createdBy;
	}

	public String getCreatedBy(){
		return createdBy;
	}

	public void setDeletedAt(String deletedAt){
		this.deletedAt = deletedAt;
	}

	public String getDeletedAt(){
		return deletedAt;
	}

	public void setGoogleCalendarId(String googleCalendarId){
		this.googleCalendarId = googleCalendarId;
	}

	public String getGoogleCalendarId(){
		return googleCalendarId;
	}

	public void setAccountId(String accountId){
		this.accountId = accountId;
	}

	public String getAccountId(){
		return accountId;
	}

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setUpdatedBy(String updatedBy){
		this.updatedBy = updatedBy;
	}

	public String getUpdatedBy(){
		return updatedBy;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}
}
