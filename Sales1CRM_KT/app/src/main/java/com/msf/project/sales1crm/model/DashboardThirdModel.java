package com.msf.project.sales1crm.model;

public class DashboardThirdModel {
    private String total;
    private String date;

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
