package com.msf.project.sales1crm.utility;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.msf.project.sales1crm.R;

/**
 * Created by christianmacbook on 11/05/18.
 */

public class AnimationImageView extends ImageView {

    public AnimationImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public AnimationImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public AnimationImageView(Context context) {
        super(context);
        init();
    }

    private void init() {
        setBackgroundResource(R.drawable.dialog_loading);
        final AnimationDrawable frameAnimation = (AnimationDrawable) getBackground();
        post(new Runnable(){
            public void run(){
                frameAnimation.start();
            }
        });
    }
}
