package com.msf.project.sales1crm.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.model.OpportunityStageModel;

import java.util.List;
import java.util.Locale;

public class ViewPipelineOpportunityStageAdapter extends PagerAdapter {


    private LayoutInflater layoutInflater;
    private Context context;
    private List<OpportunityStageModel> models;
    Locale localeID = new Locale("in","ID");


    public ViewPipelineOpportunityStageAdapter(List<OpportunityStageModel> models, Context context) {
        this.models = models;
        this.context = context;
    }


    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.row_stage, container, false);
        CustomTextView tvName,tvPercent;

        tvName = (CustomTextView) view.findViewById(R.id.tvName);
//        tvPercent = (CustomTextView) view.findViewById(R.id.tvPercent);

        tvName.setText(models.get(position).getName());
//        tvPercent.setText(models.get(position).getPercent());

        container.addView(view,0);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return models.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }


}
