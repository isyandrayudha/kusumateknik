package com.msf.project.sales1crm.utility;

import android.content.Context;
import android.net.ConnectivityManager;

public class NetworkChecker {

    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        assert cm != null;
        return cm.getActiveNetworkInfo() != null;
    }
}
