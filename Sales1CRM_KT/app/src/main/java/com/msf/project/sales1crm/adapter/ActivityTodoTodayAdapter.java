package com.msf.project.sales1crm.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.msf.project.sales1crm.ATodoDetailActivity;
import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.database.MeeToSelectDao;
import com.msf.project.sales1crm.model.ActivityTodoModel;

import java.util.List;

import butterknife.BindView;

public class ActivityTodoTodayAdapter extends RecyclerView.Adapter<ActivityTodoTodayAdapter.TodoTodayHolder> {


    private Context context;
    private Bitmap icon;
    private String url;
    private List<ActivityTodoModel> aTodoList;

    public ActivityTodoTodayAdapter(Context context, List<ActivityTodoModel> object) {
        this.context = context;
        this.aTodoList = object;
    }

    public static abstract class Row {
        public static final class Section extends Row {
            public final String text;

            public Section(String text) {
                this.text = text;
            }
        }
    }

    public static final class Item extends Row {
        public final ActivityTodoModel text;

        public Item(ActivityTodoModel text) {
            this.text = text;
        }

        public ActivityTodoModel getItem() {
            return text;
        }
    }

    private List<Row> rows;

    public void setGaris(List<Row> rows) {
        this.rows = rows;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return rows.size();
    }

    @NonNull
    @Override
    public TodoTodayHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_activity_todo_today, null);
        return new TodoTodayHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TodoTodayHolder holder, int position) {
        final Item item = (Item) rows.get(position);
        holder.tvTodoName.setText(item.text.getSubject());
        holder.tvAssignTo.setText(item.text.getUser_name());
        holder.tvDescription.setText(item.text.getDescription());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    Intent intent = new Intent(context, ATodoDetailActivity.class);
                MeeToSelectDao dao = new MeeToSelectDao(context);
                dao.deleteAllRecord();
                    intent.putExtra("aTodoModels", item.getItem());
                    context.startActivity(intent);
            }
        });
    }


    public class TodoTodayHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvTodoName)
        CustomTextView tvTodoName;
        @BindView(R.id.tvAssignTo)
        CustomTextView tvAssignTo;
        @BindView(R.id.tvDescription)
        CustomTextView tvDescription;
        public TodoTodayHolder(View view) {
            super(view);
            tvTodoName = (CustomTextView) view.findViewById(R.id.tvTodoName);
            tvAssignTo = (CustomTextView) view.findViewById(R.id.tvAssignTo);
            tvDescription = (CustomTextView) view.findViewById(R.id.tvDescription);
        }
    }
}
