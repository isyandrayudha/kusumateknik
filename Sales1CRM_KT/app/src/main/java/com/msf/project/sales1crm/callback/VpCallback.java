package com.msf.project.sales1crm.callback;

import com.msf.project.sales1crm.model.VpModel;

import java.util.List;

public interface VpCallback {
    void finishVp(String result, List<VpModel> list);
    void finishMoreVp(String result, List<VpModel>list);
}
