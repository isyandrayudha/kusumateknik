package com.msf.project.sales1crm.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.msf.project.sales1crm.ErrorAPIKeyActivity;
import com.msf.project.sales1crm.MenuActivity;
import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.TaskAddActivity;
import com.msf.project.sales1crm.TaskDetailActivity;
import com.msf.project.sales1crm.adapter.TaskListAdapter;
import com.msf.project.sales1crm.callback.DashboardCallback;
import com.msf.project.sales1crm.callback.SortDialogCallback;
import com.msf.project.sales1crm.callback.TaskListCallback;
import com.msf.project.sales1crm.customview.CustomEditText;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.imageloader.ImageLoader;
import com.msf.project.sales1crm.model.AbsenceModel;
import com.msf.project.sales1crm.model.DashboardFirstModel;
import com.msf.project.sales1crm.model.DashboardSecondModel;
import com.msf.project.sales1crm.model.DashboardThirdModel;
import com.msf.project.sales1crm.model.TaskModel;
import com.msf.project.sales1crm.model.TotalOpportunityModel;
import com.msf.project.sales1crm.presenter.DashboardPresenter;
import com.msf.project.sales1crm.presenter.TaskListPresenter;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class TaskFragment extends Fragment implements TaskListCallback, DashboardCallback {

    @BindView(R.id.rlNoConnection)
    RelativeLayout rlNoConnection;

    @BindView(R.id.tvRefresh)
    CustomTextView tvRefresh;

    @BindView(R.id.etSearch)
    CustomEditText etSearch;

    @BindView(R.id.ivClear)
    ImageView ivClear;

    @BindView(R.id.lvTaskSwipe)
    SwipeRefreshLayout lvTaskSwipe;

    @BindView(R.id.lvTask)
    ListView lvTask;

    @BindView(R.id.faTaskAdd)
    FloatingActionButton faTaskAdd;

    @BindView(R.id.llNotFound)
    LinearLayout llNotFound;

    //    @BindView(R.id.rlProgress)
//    RelativeLayout rlProgress;
    @BindView(R.id.shimmerLayout)
    ShimmerFrameLayout shimmerLayout;
    Unbinder unbinder;
    @BindView(R.id.llshim)
    LinearLayout llshim;

    @BindView(R.id.tvNow)
    CustomTextView tvNow;
    @BindView(R.id.menu)
    FloatingActionMenu menu;
    @BindView(R.id.rlTaskList)
    RelativeLayout rlTaskList;

//    @BindView(R.id.rlSort)
//    RelativeLayout rlSort;
//
//    @BindView(R.id.ivSelectSort)
//    ImageView ivSelectSort;

//    @BindView(R.id.rlFilter)
//    RelativeLayout rlFilter;

    private View view;
    private Context context;
    private TaskListAdapter adapter;
    private TaskListPresenter presenter;
    private List<TaskModel> list = new ArrayList<>();
    private String date_selected = "";
    private Calendar calendar;
    private DashboardPresenter dashboardPresenter;
    private String[] sort = {"Subject A to Z", "Sub Z to A", "account Name A to Z", "account Name Z to A", "Due Date A to Z", "Due Date Z to A"};
    private int prevSize = 0, sort_id = 0;
    private boolean isLoadMore = true;
    private boolean isMaxSize = true;
    private boolean pause = false;

    public static TaskFragment newInstance() {
        TaskFragment fragment = new TaskFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_task, container, false);
        setHasOptionsMenu(true);
        ButterKnife.bind(this, this.view);
        this.context = getActivity();
        return this.view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);
        this.context = getActivity();

        this.dashboardPresenter = new DashboardPresenter(context, this);
        this.presenter = new TaskListPresenter(context, this);
        tvNow.setPaintFlags(tvNow.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Task");

        initData();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_item, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_cards:
                    MenuActivity.getInstance().setFragment(TaskTabByStatusFragment.newInstance());
                    item.setIcon(R.drawable.ic_list_white_24dp);
                break;
            case R.id.action_list:
                    MenuActivity.getInstance().setFragment(TaskFragment.newInstance());
                    item.setIcon(R.drawable.ic_cards_black_24dp);
                break;
            case R.id.action_filter:
                SortDialogFragment DialogFragment = new SortDialogFragment(context, new SortDialogCallback() {
                    @Override
                    public void onDialogCallback(Bundle data) {
                        sort_id = Integer.parseInt(data.getString("id"));

                        getDataFromAPI(0, date_selected, sort_id);
//                        ivSelectSort.setVisibility(View.VISIBLE);
                    }
                }, sort);
                DialogFragment.show(getActivity().getSupportFragmentManager(), "sortDialog");
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    private void initData() {
        Sales1CRMUtils.Utils.imageLoader = new ImageLoader(context);

        etSearch.clearFocus();
        lvTask.setOnScrollListener(new ListLazyLoad());
        lvTaskSwipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                llshim.setVisibility(View.VISIBLE);
                shimmerLayout.setVisibility(View.VISIBLE);
                shimmerLayout.startShimmer();
                getDataFromAPI(0, date_selected, sort_id);
            }
        });

        //Set Seach Function
        setTextWatcherForSearch();
        getDataFromAPI(0, date_selected, sort_id);

        ivClear.setOnClickListener(click);
        tvRefresh.setOnClickListener(click);
        faTaskAdd.setOnClickListener(click);
        tvNow.setOnClickListener(click);
//        rlSort.setOnClickListener(click);
    }

    private void setTextWatcherForSearch() {
        etSearch.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    if (etSearch.getText().length() == 0) {
                        ivClear.setVisibility(View.VISIBLE);
                    }
//                } else {
//                    if (etSearch.getText().length() == 0) {
//                        ivClear.setVisibility(View.GONE);
//                    } else {
//                        ivClear.setVisibility(View.VISIBLE);
//                    }

                }
            }
        });

        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    getDataFromAPI(0, date_selected, sort_id);
                }

                return true;
            }
        });
    }

    private void getDataFromAPI(int prev, String date, int sort) {
        isMaxSize = true;
        isLoadMore = true;

        llNotFound.setVisibility(View.GONE);

        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(context)) {
            presenter.setupList(ApiParam.API_070, prev, sort, etSearch.getText().toString(), "", date);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    dashboardPresenter.setupFirst(ApiParam.API_005);
                }
            },1000);
        }
        else {
            rlNoConnection.setVisibility(View.VISIBLE);
            lvTaskSwipe.setVisibility(View.GONE);
            lvTaskSwipe.setRefreshing(false);
        }
    }


    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.ivClear:
                    etSearch.setText("");
                    etSearch.clearFocus();
                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);
                    getDataFromAPI(0, date_selected, sort_id);
                    break;
                case R.id.tvRefresh:
//                    lvTaskSwipe.setVisibility(View.VISIBLE);
                    rlNoConnection.setVisibility(View.GONE);

                    getDataFromAPI(0, date_selected, sort_id);
                    break;

//                case R.id.rlSort:
//                    SortDialogFragment DialogFragment = new SortDialogFragment(context, new SortDialogCallback() {
//
//                        @Override
//                        public void onDialogCallback(Bundle data) {
//                            sort_id = Integer.parseInt(data.getString("id"));
//
//                            getDataFromAPI(0, date_selected, sort_id);
//                            ivSelectSort.setVisibility(View.VISIBLE);
//                        }
//                    }, sort);
//                    DialogFragment.show(getActivity().getSupportFragmentManager(), "sortDialog");
//                    break;
                default:
                    break;
            }
        }
    };

    private void setList() {
        llshim.setVisibility(View.VISIBLE);
        shimmerLayout.setVisibility(View.GONE);
        adapter = new TaskListAdapter(context, this.list);
        List<TaskListAdapter.Row> rows = new ArrayList<TaskListAdapter.Row>();
        for (TaskModel taskModel : list) {
            //Read List by Row to insert into List Adapter
            rows.add(new TaskListAdapter.Item(taskModel));
        }
        //Set Row Adapter
        adapter.setRows(rows);
        lvTask.setAdapter(adapter);
        lvTask.setSelector(R.drawable.transparent_selector);
        lvTask.setOnItemClickListener(new AdapterOnItemClick());
        lvTask.deferNotifyDataSetChanged();
        llshim.setVisibility(View.INVISIBLE);
        shimmerLayout.stopShimmer();
        shimmerLayout.setVisibility(View.INVISIBLE);
    }

    private void setMoreList() {
        List<TaskListAdapter.Row> rows = new ArrayList<TaskListAdapter.Row>();
        for (TaskModel country : list) {
            // Add the country to the list
            rows.add(new TaskListAdapter.Item(country));
        }
        adapter.setRows(rows);
        adapter.notifyDataSetChanged();

    }

    @Override
    public void finishTaskList(String result, List<TaskModel> list) {
        lvTaskSwipe.setRefreshing(false);
//        rlProgress.setVisibility(View.GONE);
        if (result.equalsIgnoreCase("OK")) {
            prevSize = list.size();
            this.list = list;

            isMaxSize = false;
            isLoadMore = false;

            setList();
            if (list.size() > 0) {
                llNotFound.setVisibility(View.GONE);
            } else {
                llNotFound.setVisibility(View.VISIBLE);
            }
        } else if (result.equalsIgnoreCase("FAILED_KEY")) {
            startActivity(new Intent(context, ErrorAPIKeyActivity.class));
            MenuActivity.getInstance().finish();
        } else {
//            lvTaskSwipe.setVisibility(View.GONE);
            rlNoConnection.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void finishTaskMoreList(String result, List<TaskModel> list) {
        if (result.equals("OK")) {
            for (int i = 0; i < list.size(); i++) {
                this.list.add(list.get(i));
            }

            if (this.prevSize == this.list.size())
                isMaxSize = true;
            this.prevSize = this.list.size();
            setMoreList();
        }
        this.isLoadMore = false;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void finishFirst(String result, DashboardFirstModel model) {
        if(model.getCrm_task_create_permission().equalsIgnoreCase("1")){
            faTaskAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, TaskAddActivity.class);
                    intent.putExtra("date", date_selected);
                    startActivity(intent);
                }
            });
            faTaskAdd.setColorNormal(R.color.BlueCRM);
            faTaskAdd.setColorPressed(R.color.colorPrimaryDark);
            faTaskAdd.setColorPressedResId(R.color.colorPrimaryDark);
            faTaskAdd.setColorNormalResId(R.color.BlueCRM);

            tvNow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intentt = new Intent(context, TaskAddActivity.class);
                    intentt.putExtra("date", date_selected);
                    startActivity(intentt);
                }
            });
            tvNow.setVisibility(View.VISIBLE);

        } else {
            faTaskAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
                }
            });
            faTaskAdd.setColorNormal(R.color.BlueCRM_light);
            faTaskAdd.setColorPressed(R.color.colorPrimaryDark_light);
            faTaskAdd.setColorPressedResId(R.color.colorPrimaryDark_light);
            faTaskAdd.setColorNormalResId(R.color.BlueCRM_light);
        }

        tvNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
            }
        });
        tvNow.setBackgroundColor(getResources().getColor(R.color.gray));
    }

    @Override
    public void finishSecond(String result, DashboardSecondModel model) {

    }

    @Override
    public void finishThird(String result, List<DashboardThirdModel> listLead, List<DashboardThirdModel> listLead1, List<DashboardThirdModel> listOppor, List<DashboardThirdModel> listOppor1) {

    }

    @Override
    public void TotOpportunity(String result, TotalOpportunityModel totOporModel) {

    }

    @Override
    public void absence(String result, AbsenceModel model) {

    }

    private class ListLazyLoad implements AbsListView.OnScrollListener {

        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {

        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            if (firstVisibleItem + visibleItemCount == totalItemCount && totalItemCount != 0) {
                Log.d("TAG", "isLoadMore : " + isLoadMore);
                if (isLoadMore == false && isMaxSize == false) {
                    isLoadMore = true;
                    prevSize = list.size();

                    presenter.setupMoreList(ApiParam.API_070, prevSize, sort_id, etSearch.getText().toString(), "", date_selected);
                }
            }
        }
    }

    private class AdapterOnItemClick implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            // TODO Auto-generated method stub
            TaskListAdapter.Item item = (TaskListAdapter.Item) lvTask.getAdapter().getItem(position);
            switch (view.getId()) {
                default:
                        Intent intent = new Intent(context, TaskDetailActivity.class);
                        intent.putExtra("taskModel", item.getItem());
                        startActivity(intent);
                    break;
            }
        }
    }

    @Override
    public void onPause() {
        pause = true;
        super.onPause();
    }

    @Override
    public void onResume() {
        if (pause) {
            getDataFromAPI(0, date_selected, sort_id);
        }
        super.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();

    }
}
