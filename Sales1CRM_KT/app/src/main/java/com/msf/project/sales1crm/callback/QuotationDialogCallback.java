package com.msf.project.sales1crm.callback;

import android.os.Bundle;

public interface QuotationDialogCallback {
    void onQuotationDialogCallback(Bundle data);
}
