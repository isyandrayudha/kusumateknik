package com.msf.project.sales1crm.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.msf.project.sales1crm.model.LeadStatusModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by christianmacbook on 25/05/18.
 */

public class LeadStatusDao extends BaseDao<LeadStatusModel> implements DBSchema.LeadStatus {

    public LeadStatusDao(Context c) {
        super(c, TABLE_NAME);
    }

    public LeadStatusDao(Context c, boolean willWrite) {
        super(c, TABLE_NAME, willWrite);
    }

    public LeadStatusDao(DBHelper db) {
        super(db, TABLE_NAME);
    }

    public LeadStatusDao(DBHelper db, boolean willWrite) {
        super(db, TABLE_NAME, willWrite);
    }

    public LeadStatusModel getById(int id) {
        String qry = "SELECT * FROM " + getTable() + " WHERE " + COL_ID + " = " + id;
        Cursor c = getSqliteDb().rawQuery(qry, null);
        LeadStatusModel model = new LeadStatusModel();
        try {
            if(c != null && c.moveToFirst()) {
                model = getByCursor(c);
            }
        } finally {
            c.close();
        }
        return model;
    }

    public List<LeadStatusModel> getStatusList() {
        String query = "SELECT * FROM " + getTable();

        Cursor c = getSqliteDb().rawQuery(query, null);
        List<LeadStatusModel> list = new ArrayList<>();
        try {
            if(c != null && c.moveToFirst()) {
                list.add(getByCursor(c));
                while (c.moveToNext()) {
                    list.add(getByCursor(c));

                }
            }
        } finally {
            c.close();
        }
        return list;
    }

    @Override
    public LeadStatusModel getByCursor(Cursor c) {
        LeadStatusModel model = new LeadStatusModel();
        model.setId(c.getString(0));
        model.setName(c.getString(1));
        return model;
    }

    @Override
    protected ContentValues upDataValues(LeadStatusModel model, boolean update) {
        ContentValues cv = new ContentValues();
        if (update == true)
            cv.put(COL_ID, model.getId());
        cv.put(KEY_NAME, model.getName());
        return cv;
    }
}
