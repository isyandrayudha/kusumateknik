package com.msf.project.sales1crm.fragment;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.callback.TotalStatsCallback;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.customview.SpinnerCustomAdapter;
import com.msf.project.sales1crm.model.TotalModel;
import com.msf.project.sales1crm.presenter.ViewReportLeadStatusPresenter;
import com.msf.project.sales1crm.service.GPSTracker;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ViewReportLeadStatusFragment extends Fragment implements TotalStatsCallback {

    @BindView(R.id.pieChart)
    PieChart pieChart;

    @BindView(R.id.pieChart1)
    PieChart pieChart1;

    @BindView(R.id.spMonth1)
    Spinner spMonth1;

    @BindView(R.id.spYear1)
    Spinner spYear1;

    @BindView(R.id.spMonth2)
    Spinner spMonth2;

    @BindView(R.id.spYear2)
    Spinner spYear2;

    @BindView(R.id.tvSubmit)
    CustomTextView tvSubmit;

    private View view;
    private Context context;
    private ViewReportLeadStatusPresenter presenter;
    private SpinnerCustomAdapter spinnerCustomAdapter;
    private List<TotalModel> list = new ArrayList<>();
    private List<TotalModel> list1 = new ArrayList<>();
    private String[] months = {"Choose Month", "January", "February", "March", "April", "May", "Juni", "July", "August", "September", "October", "November", "Desember"};
    private String[] years = {"Choose Year", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019"};
    private String month_1="", year_1="", month_2="", year_2="";

    public static ViewReportLeadStatusFragment newInstance() {
        ViewReportLeadStatusFragment fragment = new ViewReportLeadStatusFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_leadbystatus, container, false);

        return this.view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);
        this.context = getActivity();

        this.presenter = new ViewReportLeadStatusPresenter(context, this);

        initData();
    }

    private void initData(){
        spinnerCustomAdapter = new SpinnerCustomAdapter(context,
                R.layout.spinner_default, years);
        spinnerCustomAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spYear1.setAdapter(spinnerCustomAdapter);
        spYear1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                if (position != 0){
                    year_1 = years[position];
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerCustomAdapter = new SpinnerCustomAdapter(context,
                R.layout.spinner_default, months);
        spinnerCustomAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spMonth1.setAdapter(spinnerCustomAdapter);
        spMonth1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                if (position != 0){
                    month_1 = String.valueOf(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerCustomAdapter = new SpinnerCustomAdapter(context,
                R.layout.spinner_default, years);
        spinnerCustomAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spYear2.setAdapter(spinnerCustomAdapter);
        spYear2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                if (position != 0){
                    year_2 = years[position];
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerCustomAdapter = new SpinnerCustomAdapter(context,
                R.layout.spinner_default, months);
        spinnerCustomAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spMonth2.setAdapter(spinnerCustomAdapter);
        spMonth2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                if (position != 0){
                    month_2 = String.valueOf(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        tvSubmit.setOnClickListener(click);
    }

    public void getDataFromAPI(){
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(context)){
            GPSTracker gpsTracker = new GPSTracker(context);
            if (gpsTracker.canGetLocation()) {
                presenter.setupTotal(ApiParam.API_097, month_1, year_1, month_2, year_2);
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gpsTracker.showSettingsAlert();
            }
        }
    }

    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.tvSubmit:
                    getDataFromAPI();
                    break;
            }
        }
    };

    private void setPieCurrent(ArrayList<PieEntry> current){
        PieDataSet dataSet = new PieDataSet(current, "");
        dataSet.setColors(ColorTemplate.COLORFUL_COLORS);

        PieData data = new PieData(dataSet);
        // In Percentage
        data.setValueTextSize(13f);
        data.setValueTextColor(Color.DKGRAY);

        // Default value
        //data.setValueFormatter(new DefaultValueFormatter(0));
        pieChart.setData(data);
        pieChart.setEntryLabelColor(Color.DKGRAY);
        pieChart.getDescription().setEnabled(false);
        pieChart.setDrawHoleEnabled(true);
        pieChart.setTransparentCircleRadius(58f);
        pieChart.setHoleRadius(58f);
        pieChart.spin( 500,0,-360f, Easing.EasingOption.EaseInOutQuad);
        pieChart.invalidate(); // refresh
    }

    private void setPieCurrent1(ArrayList<PieEntry> current){
        PieDataSet dataSet = new PieDataSet(current, "");
        dataSet.setColors(ColorTemplate.COLORFUL_COLORS);

        PieData data = new PieData(dataSet);
        // In Percentage
        data.setValueTextSize(13f);
        data.setValueTextColor(Color.DKGRAY);

        // Default value
        //data.setValueFormatter(new DefaultValueFormatter(0));
        pieChart1.setData(data);
        pieChart1.setEntryLabelColor(Color.DKGRAY);
        pieChart1.getDescription().setEnabled(false);
        pieChart1.setDrawHoleEnabled(true);
        pieChart1.setTransparentCircleRadius(58f);
        pieChart1.setHoleRadius(58f);
        pieChart1.spin( 500,0,-360f, Easing.EasingOption.EaseInOutQuad);
        pieChart1.invalidate(); // refresh
    }

    @Override
    public void finished(String result, List<TotalModel> list, List<TotalModel> list1) {
        if (result.equalsIgnoreCase("OK")){
            ArrayList<PieEntry> data1 = new ArrayList<PieEntry>();
            for (int x = 0; x < list.size(); x++){
                Log.d("TAG", list.get(x).getTotal() + " - " + list.get(x).getName());

                data1.add(new PieEntry(Integer.parseInt(list.get(x).getTotal()), list.get(x).getName()));
            }

            ArrayList<PieEntry> data2 = new ArrayList<PieEntry>();
            for (int x = 0; x < list1.size(); x++){
                data2.add(new PieEntry(Integer.parseInt(list1.get(x).getTotal()), list1.get(x).getName()));
            }

            setPieCurrent(data1);
            setPieCurrent1(data2);
        }
    }
}
