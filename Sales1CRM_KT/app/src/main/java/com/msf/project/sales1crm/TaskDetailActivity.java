package com.msf.project.sales1crm;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.msf.project.sales1crm.callback.DashboardCallback;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.model.AbsenceModel;
import com.msf.project.sales1crm.model.DashboardFirstModel;
import com.msf.project.sales1crm.model.DashboardSecondModel;
import com.msf.project.sales1crm.model.DashboardThirdModel;
import com.msf.project.sales1crm.model.TaskModel;
import com.msf.project.sales1crm.model.TotalOpportunityModel;
import com.msf.project.sales1crm.presenter.DashboardPresenter;
import com.msf.project.sales1crm.utility.ApiParam;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TaskDetailActivity extends BaseActivity implements DashboardCallback {

    @BindView(R.id.ivBack)
    ImageView ivBack;

    @BindView(R.id.ivEdit)
    ImageView ivEdit;

    @BindView(R.id.tvSubjectTitle)
    CustomTextView tvSubjectTitle;

    @BindView(R.id.tvSubject)
    CustomTextView tvSubject;

    @BindView(R.id.tvAccountName)
    CustomTextView tvAccountName;

    @BindView(R.id.tvPriority)
    CustomTextView tvPriority;

    @BindView(R.id.tvTaskStatus)
    CustomTextView tvTaskStatus;

    @BindView(R.id.tvDate)
    CustomTextView tvDate;

    @BindView(R.id.tvCreateDate)
    CustomTextView tvCreateDate;

    @BindView(R.id.tvDescription)
    CustomTextView tvDesc;

    @BindView(R.id.tvAssignTo)
    CustomTextView tvAssignTo;

    private Context context;
    private TaskModel tmodel;
    private DashboardPresenter dashboardPresenter;
    static TaskDetailActivity taskDetailActivity;

    public static TaskDetailActivity getInstance(){
        return taskDetailActivity;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_taskdetail);

        ButterKnife.bind(this);
        this.context = this;
        this.dashboardPresenter = new DashboardPresenter(context, this);

        taskDetailActivity = this;

        try {
            tmodel = getIntent().getExtras().getParcelable("taskModel");
        }catch (Exception e){}

        initView();
    }

    private void initView(){
        dashboardPresenter.setupFirst(ApiParam.API_005);
        tvSubjectTitle.setText(tmodel.getSubject());
        tvSubject.setText(tmodel.getSubject());
        tvAccountName.setText(tmodel.getAccount_name());
        tvCreateDate.setText("Created at " + tmodel.getCreated_at());
        tvDate.setText(tmodel.getDate_interval());
        tvAssignTo.setText(tmodel.getAssignto_name());
        tvPriority.setText(tmodel.getPriority_name());
        tvTaskStatus.setText(tmodel.getTask_status_name());
        tvDesc.setText(tmodel.getDescription());

        ivBack.setOnClickListener(click);
    }

    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.ivBack:
                    finish();
                    break;

            }
        }
    };

    @Override
    public void finishFirst(String result, DashboardFirstModel model) {
if(model.getCrm_task_edit_permission().equalsIgnoreCase("1")){
    ivEdit.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(context, TaskAddActivity.class);
            intent.putExtra("taskModel", tmodel);
            intent.putExtra("from_detail", true);
            startActivity(intent);
        }
    });
    ivEdit.setVisibility(View.VISIBLE);
} else {
    ivEdit.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
        }
    });
    ivEdit.setVisibility(View.GONE);
}
    }

    @Override
    public void finishSecond(String result, DashboardSecondModel model) {

    }

    @Override
    public void finishThird(String result, List<DashboardThirdModel> listLead, List<DashboardThirdModel> listLead1, List<DashboardThirdModel> listOppor, List<DashboardThirdModel> listOppor1) {

    }

    @Override
    public void TotOpportunity(String result, TotalOpportunityModel totOporModel) {

    }

    @Override
    public void absence(String result, AbsenceModel model) {

    }
}
