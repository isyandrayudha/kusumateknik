package com.msf.project.sales1crm;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.msf.project.sales1crm.callback.SyncMasterCallback;
import com.msf.project.sales1crm.customview.CustomDialog;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.presenter.SyncMasterPresenter;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.PreferenceUtility;
import com.msf.project.sales1crm.utility.Sales1CRMUtilsJSON;

import butterknife.ButterKnife;
import butterknife.BindView;

/**
 * Created by christianmacbook on 24/05/18.
 */

public class SyncMasterActivity extends BaseActivity implements SyncMasterCallback {

    @BindView(R.id.tvKeterangan)
    CustomTextView tvKeterangan;
    
    @BindView(R.id.tvCobaKembali)
    CustomTextView tvCobaKembali;
    
    @BindView(R.id.tvBack)
    CustomTextView tvBack;
    
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    
    private Context context;
    private boolean config = true, user = true;
    private SyncMasterPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_syncmaster);

        ButterKnife.bind(this);
        this.context = this;
        this.presenter = new SyncMasterPresenter(context, this);
        
        initView();
    }
    
    private void initView(){
        Sales1CRMUtilsJSON.JSONUtility.getClearDataMaster(context);

        getDataFromAPI();

        tvBack.setOnClickListener(click);
        tvCobaKembali.setOnClickListener(click);
    }

    private void getDataFromAPI(){
        if (config)
            presenter.setupSync(ApiParam.API_002);
        else if (user)
            presenter.setupMasterUser(ApiParam.API_003);
    }
    
    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.tvBack:
                    final CustomDialog dialogBack = CustomDialog.setupDialogConfirmation(context, "Anda ingin membatalkan sinkron data?");
                    CustomTextView tvCancel = (CustomTextView) dialogBack.findViewById(R.id.tvCancel);
                    CustomTextView tvYa = (CustomTextView) dialogBack.findViewById(R.id.tvOK);

                    tvCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialogBack.dismiss();
                        }
                    });

                    tvYa.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            startActivity(new Intent(context, MenuActivity.class));
                            finish();

                            dialogBack.dismiss();
                        }
                    });
                    dialogBack.show();
                    break;
                case R.id.tvCobaKembali:
                    final CustomDialog dialogRetry = CustomDialog.setupDialogConfirmation(context, "Pastikan Koneksi anda dalam kondisi baik, Sinkron data sekaran?");
                    CustomTextView tvCancel1 = (CustomTextView) dialogRetry.findViewById(R.id.tvCancel);
                    CustomTextView tvYa1 = (CustomTextView) dialogRetry.findViewById(R.id.tvOK);

                    tvCancel1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialogRetry.dismiss();
                        }
                    });

                    tvYa1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Sales1CRMUtilsJSON.JSONUtility.getClearDataMaster(context);


                            tvKeterangan.setText("Tunggu Sedang Sinkron Data..");

                            progressBar.setVisibility(View.VISIBLE);
                            tvCobaKembali.setVisibility(View.GONE);

                            config = true;
                            user = true;

                            getDataFromAPI();
                            
                            dialogRetry.dismiss();
                        }
                    });
                    dialogRetry.show();
                    break;
            }
        }
    };

    @Override
    public void finishSync(String result) {
        if (result.equalsIgnoreCase("OK")){
            config = false;

            if (!config && !user){
                PreferenceUtility.getInstance().saveData(context, PreferenceUtility.KEY_SYNCMASTER, "1");
                dialogInformation("Sinkron Data Berhasil!");
            }else{
                getDataFromAPI();
            }
        }else{
            tvKeterangan.setText("Terjadi kendala pada server, Coba Sinkron data kembali.");
            progressBar.setVisibility(View.GONE);
            tvCobaKembali.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void finishUser(String result) {
        if (result.equalsIgnoreCase("OK")){
            user = false;

            if (!config && !user){
                progressBar.setVisibility(View.GONE);
                PreferenceUtility.getInstance().saveData(context, PreferenceUtility.KEY_SYNCMASTER, "1");
                dialogInformation("Sinkron Data Berhasil!");
            }else{
                user = false;
            }
        }else{
            tvKeterangan.setText("Terjadi kendala pada server, Coba Sinkron data kembali.");
            progressBar.setVisibility(View.GONE);
            tvCobaKembali.setVisibility(View.VISIBLE);
        }
    }

    private void dialogInformation(String info){
        final Dialog dialog = new Dialog(SyncMasterActivity.this,
                android.R.style.Theme_Translucent_NoTitleBar);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_result);

        TextView text = (TextView) dialog.findViewById(R.id.tvDialog);
        text.setText(info);
        TextView tvOK = (TextView) dialog.findViewById(R.id.tvOK);
        tvOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, MenuActivity.class));
                finish();

                dialog.dismiss();
            }
        });

        dialog.show();
    }
}
