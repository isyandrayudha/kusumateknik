package com.msf.project.sales1crm;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.msf.project.sales1crm.callback.AccountDialogCallback;
import com.msf.project.sales1crm.callback.QuotationDialogCallback;
import com.msf.project.sales1crm.callback.ReasonCallback;
import com.msf.project.sales1crm.callback.ReasonDialogCallback;
import com.msf.project.sales1crm.callback.UserDialogCallback;
import com.msf.project.sales1crm.customview.CurrencyEditText;
import com.msf.project.sales1crm.customview.CustomDialog;
import com.msf.project.sales1crm.customview.CustomEditText;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.customview.DatePickerFragment;
import com.msf.project.sales1crm.customview.SpinnerCustomAdapter;
import com.msf.project.sales1crm.database.IndustryDao;
import com.msf.project.sales1crm.database.OpportunityStageDao;
import com.msf.project.sales1crm.database.OpportunityStatusDao;
import com.msf.project.sales1crm.database.UseForDao;
import com.msf.project.sales1crm.fragment.AccountDialogFragment;
import com.msf.project.sales1crm.fragment.QuotationDialogFragment;
import com.msf.project.sales1crm.fragment.ReasonNotDealFragment;
import com.msf.project.sales1crm.fragment.UserDialogFragment;
import com.msf.project.sales1crm.model.AccountModel;
import com.msf.project.sales1crm.model.IndustryModel;
import com.msf.project.sales1crm.model.OpportunityModel;
import com.msf.project.sales1crm.model.OpportunityStageModel;
import com.msf.project.sales1crm.model.ReasonModel;
import com.msf.project.sales1crm.utility.PreferenceUtility;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OpportunityAddActivity extends BaseActivity implements ReasonCallback {

    @BindView(R.id.ivNext)
    ImageView ivNext;

    @BindView(R.id.ivBack)
    ImageView ivBack;

    @BindView(R.id.tvTitle)
    CustomTextView tvTitle;

    @BindView(R.id.tvNext)
    CustomTextView tvNext;

    @BindView(R.id.ivAccountName)
    ImageView ivAccountName;


    @BindView(R.id.etOpportunityName)
    CustomEditText etOpportunityName;

//    @BindView(R.id.tilOpportunityName)
//    TextInputLayout tilOpportunityName;

    @BindView(R.id.etSizeProject)
    CurrencyEditText etSizeProject;

    @BindView(R.id.spStatus)
    Spinner spStatus;

    @BindView(R.id.spType)
    Spinner spType;

    @BindView(R.id.tvCloseDate)
    CustomTextView tvCloseDate;

    @BindView(R.id.spStage)
    Spinner spStage;

    @BindView(R.id.tvProgress)
    CustomTextView tvProgress;

    @BindView(R.id.spIndustry)
    Spinner spIndustry;

    @BindView(R.id.tvAssignTo)
    CustomTextView tvAssignTo;
    @BindView(R.id.tvAccountName)
    CustomTextView tvAccountName;
    @BindView(R.id.tvQoutation)
    CustomTextView tvQoutation;
    @BindView(R.id.ivQuotation)
    ImageView ivQuotation;
    @BindView(R.id.rlQuotation)
    RelativeLayout rlQuotation;
    @BindView(R.id.tvActualSize)
    CustomTextView tvActualSize;
    @BindView(R.id.llactualSize)
    LinearLayout llactualSize;

    @BindView(R.id.llReason)
    LinearLayout llReason;
    @BindView(R.id.tvReasonNotDeal)
    CustomTextView tvReasonNotDeal;
    @BindView(R.id.ivReason)
    ImageView ivReason;
//    @BindView(R.id.tilSizeProject)
//    TextInputLayout tilSizeProject;

    private Context context;
    private DatePickerFragment datepicker;
    private String url = "", contact_json, account_id = "";
    private String open_date = "", string_year, string_month, string_day,
            oppor_status = "0", oppor_type = "0", oppor_stage = "0", assign_id = "0", oppor_industry = "0", oppor_category = "0";
    private int year, month, day;
    private OpportunityModel model;
    private AccountModel accountModel;
    private List<IndustryModel> industryList = new ArrayList<>();
    private ArrayList<String> arrIndustry = new ArrayList<>();
    private List<OpportunityStageModel> stageModels = new ArrayList<>();
    private List<ReasonModel> reasonModels = new ArrayList<>();
    private String[] stageArr, stageArrID, statusArr, statusArrID, categoryArr, categoryArrID, industryArr, industryArrID,
            typeArr = {"Choose Type", "None", "New Business", "Existing Business"};
    static OpportunityAddActivity opportunityAddActivity;
    private boolean from_detail;
    private boolean byStatgeActivity;
    private boolean convert;
    private String accountId, accountName, contact, qoutationId = "", reasonId= "";

    public static OpportunityAddActivity getInstance() {
        return opportunityAddActivity;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_opportunityadd);

        ButterKnife.bind(this);
        this.context = this;
        opportunityAddActivity = this;

        try {
            from_detail = getIntent().getExtras().getBoolean("from_detail");
            model = getIntent().getExtras().getParcelable("opportunityModel");
            Log.d("TAG", "op_model " + model);
            byStatgeActivity = getIntent().getExtras().getBoolean("byStatgeActivity");
        } catch (Exception e) {
        }

        try {
            convert = getIntent().getExtras().getBoolean("convert");
            account_id = getIntent().getExtras().getString("account_id");
            accountName = getIntent().getExtras().getString("account_name");
            contact_json = getIntent().getExtras().getString("contact");
        } catch (Exception e) {

        }

        Log.d("TAG", "acc_name " + accountName);

        this.url = PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.URL);

        datepicker = new DatePickerFragment();
        datepicker.setListener(dateListener);

        getDataMaster();
        initDate();
        initView();
    }

    private void getDataMaster() {
        OpportunityStageDao opportunityStageDao = new OpportunityStageDao(context);
        stageModels = opportunityStageDao.getFromList();
        stageArr = new String[stageModels.size() + 1];
        stageArrID = new String[stageModels.size() + 1];
        for (int i = 0; i < stageModels.size(); i++) {
            if (i == 0) {
                stageArr[i] = "Choose Stage";
                stageArrID[i] = "0";
            }

            stageArr[i + 1] = stageModels.get(i).getName();
            stageArrID[i + 1] = stageModels.get(i).getId();
        }

        OpportunityStatusDao statusDao = new OpportunityStatusDao(context);
        statusArr = statusDao.getStringArray();
        statusArrID = statusDao.getStringArrayID();

        UseForDao useForDao = new UseForDao(context);
        categoryArr = useForDao.getStringArray();
        categoryArrID = useForDao.getStringArrayID();

        IndustryDao industryDao = new IndustryDao(context);
        industryArr = industryDao.getStringArray();
        industryArrID = industryDao.getStringArrayID();

        if (from_detail) {
            for (int i = 0; i < stageArrID.length; i++) {
                if (stageArrID[i].equalsIgnoreCase(model.getStage_id())) {
                    model.setStage_id(String.valueOf(i));
                }
            }

            for (int i = 0; i < statusArrID.length; i++) {
                if (statusArrID[i].equalsIgnoreCase(model.getOppor_status_id())) {
                    model.setOppor_status_id(String.valueOf(i));
                }
            }

            for (int i = 0; i < categoryArrID.length; i++) {
                if (categoryArrID[i].equalsIgnoreCase(model.getCategory_id())) {
                    model.setCategory_id(String.valueOf(i));
                }
            }

            for (int i = 0; i < industryArrID.length; i++) {
                if (industryArrID[i].equalsIgnoreCase(model.getIndustry())) {
                    model.setIndustry(String.valueOf(i));
                }
            }
        }
    }

    private void initDate() {
        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH) + 1;
        day = c.get(Calendar.DAY_OF_MONTH);
    }

    private void initView() {
        SpinnerCustomAdapter spIndustryAdapter = new SpinnerCustomAdapter(context,
                R.layout.spinner_default, industryArr);
        spIndustryAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spIndustry.setAdapter(spIndustryAdapter);
        spIndustry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                if (position != 0) {
                    oppor_industry = industryArrID[position];
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        SpinnerCustomAdapter stageAdapter = new SpinnerCustomAdapter(context,
                R.layout.spinner_default, stageArr);
        stageAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spStage.setAdapter(stageAdapter);
        spStage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                if (position != 0) {
                    if (from_detail) {
                        oppor_stage = stageArrID[position];
                        tvProgress.setText(stageModels.get(position - 1).getPercent() + "%");
                        if (tvProgress.getText().toString().equals("100%")) {
                            Log.d("TAG", "OK " + tvProgress.getText().toString());
                            rlQuotation.setVisibility(View.VISIBLE);
                            llactualSize.setVisibility(View.VISIBLE);
                        } else {
                            rlQuotation.setVisibility(View.GONE);
                            llactualSize.setVisibility(View.GONE);
                        }

                        if (tvProgress.getText().toString().equalsIgnoreCase("0%") && !oppor_stage.equalsIgnoreCase("1")){
                            llReason.setVisibility(View.VISIBLE);
                        } else {
                            llReason.setVisibility(View.GONE);
                        }
                    } else {
                        oppor_stage = stageArrID[position];
                        tvProgress.setText(stageModels.get(position - 1).getPercent() + "%");
                        if(tvProgress.getText().toString().equalsIgnoreCase("0%") && !oppor_stage.equalsIgnoreCase("1")){
                            llReason.setVisibility(View.VISIBLE);
                        } else {
                            llReason.setVisibility(View.GONE);
                        }
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        SpinnerCustomAdapter statusAdapter = new SpinnerCustomAdapter(context,
                R.layout.spinner_default, statusArr);
        statusAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spStatus.setAdapter(statusAdapter);
        spStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                if (position != 0) {
                    oppor_status = statusArrID[position];
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        SpinnerCustomAdapter typeAdapter = new SpinnerCustomAdapter(context,
                R.layout.spinner_default, typeArr);
        typeAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spType.setAdapter(typeAdapter);
        spType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                if (position != 0) {
                    oppor_type = String.valueOf(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        tvAssignTo.setText(PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.KEY_FULLNAME));
        assign_id = PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.KEY_USERID);

        if (from_detail) {
            tvTitle.setText("Edit Opportunity");
            rlQuotation.setVisibility(View.VISIBLE);
            llactualSize.setVisibility(View.VISIBLE);
            ivAccountName.setEnabled(false);
            tvAccountName.setEnabled(false);
            ivAccountName.setVisibility(View.GONE);

            assign_id = model.getAssign_to();
            account_id = model.getAccount_id();

            spStatus.setSelection(Integer.parseInt(model.getOppor_status_id()));
            spType.setSelection(Integer.parseInt(model.getType()));
            spStage.setSelection(Integer.parseInt(model.getStage_id()));
            spIndustry.setSelection(Integer.parseInt(model.getIndustry()));


            oppor_status = model.getOppor_status_id();
            oppor_type = model.getType();
            oppor_stage = model.getStage_id();
            oppor_industry = model.getIndustry();
            oppor_category = model.getCategory_id();

            tvAssignTo.setText(model.getAssign_to_name());
            etOpportunityName.setText(model.getName());
            tvAccountName.setText(model.getAccount_name());
            etSizeProject.setText(model.getSize());
            tvCloseDate.setText(model.getClose_date());
            tvProgress.setText(model.getProgress() + "%");


        }


        if (convert) {
            tvAccountName.setText(accountName);
        }

        etSizeProject.setLocale(new Locale("in", "ID"));

        ivBack.setOnClickListener(click);
        ivNext.setOnClickListener(click);
        tvNext.setOnClickListener(click);
        tvAccountName.setOnClickListener(click);
        ivAccountName.setOnClickListener(click);
        tvCloseDate.setOnClickListener(click);
        tvAssignTo.setOnClickListener(click);
        tvQoutation.setOnClickListener(click);
        tvReasonNotDeal.setOnClickListener(click);
    }

    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.tvAssignTo:
                    UserDialogFragment userFragment = new UserDialogFragment(OpportunityAddActivity.this, new UserDialogCallback() {

                        @Override
                        public void onUserDialogCallback(Bundle data) {
                            tvAssignTo.setText(data.getString("name"));
                            assign_id = data.getString("id");
                        }
                    });
                    userFragment.show(getSupportFragmentManager(), "UserFragment");
                    break;
                case R.id.ivBack:
                    final CustomDialog dialogback = CustomDialog.setupDialogConfirmation(context, "Anda ingin kembali? Data yang anda masukkan akan hilang?");
                    CustomTextView tvCancel = (CustomTextView) dialogback.findViewById(R.id.tvCancel);
                    CustomTextView tvYa = (CustomTextView) dialogback.findViewById(R.id.tvOK);

                    tvCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialogback.dismiss();
                        }
                    });

                    tvYa.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            finish();
                            dialogback.dismiss();
                        }
                    });
                    dialogback.show();
                    break;
                case R.id.ivNext:
                    if (checkField()) {
                        if (from_detail) {
                            if (oppor_stage.equalsIgnoreCase("7")) {
                                model.setAccount_name(tvAccountName.getText().toString());
                                model.setAccount_id(account_id);
                                model.setName(etOpportunityName.getText().toString());
                                model.setSize(String.valueOf(etSizeProject.getRawValue()));
                                model.setOppor_status_id(oppor_status);
                                model.setType(oppor_type);
                                model.setClose_date(tvCloseDate.getText().toString());
                                model.setStage_id(oppor_stage);
                                model.setProgress(tvProgress.getText().toString().replace("%", ""));
                                model.setAssign_to(assign_id);
                                model.setIndustry(oppor_industry);
                                model.setCategory_id(oppor_category);
                                if (tvActualSize.getText().toString().equalsIgnoreCase("")) {
                                    tvActualSize.setError("Actual Size is is required");
                                    return;
                                } else {
                                    model.setActual_opportunity_size(tvActualSize.getText().toString());
                                }
                                model.setFinal_quotation_id(qoutationId);
                                model.setReason_not_deal(reasonId);
                                Log.d("TAG", "ACTUAL_SIZE : " + tvActualSize.getText().toString());

                                Intent i = new Intent(context, OpportunityAddExtActivity.class);
                                i.putExtra("opportunityModel", model);
                                i.putExtra("pic", model.getAccount_contact());
                                i.putExtra("from_detail", from_detail);
                                startActivity(i);
                            } else if(oppor_stage.equalsIgnoreCase("8")){
                                model.setAccount_name(tvAccountName.getText().toString());
                                model.setAccount_id(account_id);
                                model.setName(etOpportunityName.getText().toString());
                                model.setSize(String.valueOf(etSizeProject.getRawValue()));
                                model.setOppor_status_id(oppor_status);
                                model.setType(oppor_type);
                                model.setClose_date(tvCloseDate.getText().toString());
                                model.setStage_id(oppor_stage);
                                model.setProgress(tvProgress.getText().toString().replace("%", ""));
                                model.setAssign_to(assign_id);
                                model.setIndustry(oppor_industry);
                                model.setCategory_id(oppor_category);
                                if (reasonId.equals("0") || reasonId.equalsIgnoreCase("")) {
                                    tvReasonNotDeal.setError("Actual Size is is required");
                                    return;
                                } else {
                                    model.setReason_not_deal(reasonId);
                                }
                                model.setFinal_quotation_id(qoutationId);
                                model.setActual_opportunity_size(tvActualSize.getText().toString());

                                Log.d("TAG", "ACTUAL_SIZE : " + tvActualSize.getText().toString());

                                Intent i = new Intent(context, OpportunityAddExtActivity.class);
                                i.putExtra("opportunityModel", model);
                                i.putExtra("pic", model.getAccount_contact());
                                i.putExtra("from_detail", from_detail);
                                startActivity(i);
                            } else {
                                model.setAccount_name(tvAccountName.getText().toString());
                                model.setAccount_id(account_id);
                                model.setName(etOpportunityName.getText().toString());
                                model.setSize(String.valueOf(etSizeProject.getRawValue()));
                                model.setOppor_status_id(oppor_status);
                                model.setType(oppor_type);
                                model.setClose_date(tvCloseDate.getText().toString());
                                model.setStage_id(oppor_stage);
                                model.setProgress(tvProgress.getText().toString().replace("%", ""));
                                model.setAssign_to(assign_id);
                                model.setIndustry(oppor_industry);
                                model.setCategory_id(oppor_category);
                                model.setActual_opportunity_size(tvActualSize.getText().toString());
                                model.setFinal_quotation_id(qoutationId);
                                model.setReason_not_deal(reasonId);
                                Log.d("TAG", "ACTUAL_SIZE : " + tvActualSize.getText().toString());

                                Intent i = new Intent(context, OpportunityAddExtActivity.class);
                                i.putExtra("opportunityModel", model);
                                i.putExtra("pic", model.getAccount_contact());
                                i.putExtra("from_detail", from_detail);
                                startActivity(i);
                            }
                        } else if (convert) {
                            if (oppor_stage.equalsIgnoreCase("7")) {
                                model = new OpportunityModel();
                                model.setAccount_name(tvAccountName.getText().toString());
                                model.setAccount_id(account_id);
                                model.setAccount_id(account_id);
                                model.setName(etOpportunityName.getText().toString());
                                model.setSize(String.valueOf(etSizeProject.getRawValue()));
                                model.setOppor_status_id(oppor_status);
                                model.setType(oppor_type);
                                model.setClose_date(tvCloseDate.getText().toString());
                                model.setStage_id(oppor_stage);
                                model.setProgress(tvProgress.getText().toString().replace("%", ""));
                                model.setAssign_to(assign_id);
                                model.setIndustry(oppor_industry);
                                model.setCategory_id(oppor_category);
                                model.setReason_not_deal(reasonId);
                                if (tvActualSize.getText().toString().equalsIgnoreCase("")) {
                                    tvActualSize.setError("Actual Size is is required");
                                    return;
                                } else {
                                    model.setActual_opportunity_size(tvActualSize.getText().toString());
                                }
                                model.setFinal_quotation_id(qoutationId);


                                Log.d("TAG", "ACTUAL_SIZE : " + tvActualSize.getText().toString());
                                Intent i = new Intent(context, OpportunityAddExtActivity.class);
                                i.putExtra("opportunityModel", model);
                                i.putExtra("pic", contact_json);
                                i.putExtra("convert", convert);
                                startActivity(i);
                            } else if(oppor_stage.equalsIgnoreCase("8")){
                                model.setAccount_name(tvAccountName.getText().toString());
                                model.setAccount_id(account_id);
                                model.setName(etOpportunityName.getText().toString());
                                model.setSize(String.valueOf(etSizeProject.getRawValue()));
                                model.setOppor_status_id(oppor_status);
                                model.setType(oppor_type);
                                model.setClose_date(tvCloseDate.getText().toString());
                                model.setStage_id(oppor_stage);
                                model.setProgress(tvProgress.getText().toString().replace("%", ""));
                                model.setAssign_to(assign_id);
                                model.setIndustry(oppor_industry);
                                model.setCategory_id(oppor_category);
                                if (reasonId.equals("0") || reasonId.equalsIgnoreCase("")) {
                                    tvReasonNotDeal.setError("Actual Size is is required");
                                    return;
                                } else {
                                    model.setReason_not_deal(reasonId);
                                }
                                model.setFinal_quotation_id(qoutationId);
                                model.setActual_opportunity_size(tvActualSize.getText().toString());

                                Log.d("TAG", "ACTUAL_SIZE : " + tvActualSize.getText().toString());

                                Intent i = new Intent(context, OpportunityAddExtActivity.class);
                                i.putExtra("opportunityModel", model);
                                i.putExtra("pic", model.getAccount_contact());
                                i.putExtra("from_detail", from_detail);
                                startActivity(i);
                            } else {
                                model = new OpportunityModel();
                                model.setAccount_name(tvAccountName.getText().toString());
                                model.setAccount_id(account_id);
                                model.setAccount_id(account_id);
                                model.setName(etOpportunityName.getText().toString());
                                model.setSize(String.valueOf(etSizeProject.getRawValue()));
                                model.setOppor_status_id(oppor_status);
                                model.setType(oppor_type);
                                model.setClose_date(tvCloseDate.getText().toString());
                                model.setStage_id(oppor_stage);
                                model.setProgress(tvProgress.getText().toString().replace("%", ""));
                                model.setAssign_to(assign_id);
                                model.setIndustry(oppor_industry);
                                model.setCategory_id(oppor_category);
                                model.setActual_opportunity_size(tvActualSize.getText().toString());
                                model.setFinal_quotation_id(qoutationId);
                                model.setReason_not_deal(reasonId);

                                Log.d("TAG", "ACTUAL_SIZE : " + tvActualSize.getText().toString());
                                Intent i = new Intent(context, OpportunityAddExtActivity.class);
                                i.putExtra("opportunityModel", model);
                                i.putExtra("pic", contact_json);
                                i.putExtra("convert", convert);
                                startActivity(i);
                            }
                        } else {
                            model = new OpportunityModel();
                            model.setAccount_name(tvAccountName.getText().toString());
                            model.setAccount_id(account_id);
                            model.setName(etOpportunityName.getText().toString());
                            model.setSize(String.valueOf(etSizeProject.getRawValue()));
                            model.setOppor_status_id(oppor_status);
                            model.setType(oppor_type);
                            model.setClose_date(tvCloseDate.getText().toString());
                            model.setStage_id(oppor_stage);
                            model.setProgress(tvProgress.getText().toString().replace("%", ""));
                            model.setAssign_to(assign_id);
                            model.setIndustry(oppor_industry);
                            model.setCategory_id(oppor_category);
                            model.setReason_not_deal(reasonId);

                            Intent i = new Intent(context, OpportunityAddExtActivity.class);
                            i.putExtra("opportunityModel", model);
                            i.putExtra("pic", contact_json);
                            startActivity(i);
                        }
                    }
                    break;
                case R.id.tvNext:
                    if (checkField()) {
                        if (from_detail) {
                            if (oppor_stage.equalsIgnoreCase("7")) {
                                model.setAccount_name(tvAccountName.getText().toString());
                                model.setAccount_id(account_id);
                                model.setName(etOpportunityName.getText().toString());
                                model.setSize(String.valueOf(etSizeProject.getRawValue()));
                                model.setOppor_status_id(oppor_status);
                                model.setType(oppor_type);
                                model.setClose_date(tvCloseDate.getText().toString());
                                model.setStage_id(oppor_stage);
                                model.setProgress(tvProgress.getText().toString().replace("%", ""));
                                model.setAssign_to(assign_id);
                                model.setIndustry(oppor_industry);
                                model.setCategory_id(oppor_category);
                                model.setFinal_quotation_id(qoutationId);
                                model.setReason_not_deal(reasonId);

                                if (tvActualSize.getText().toString().equalsIgnoreCase("")) {
                                    tvActualSize.setError("Actual Size is is required");
                                    return;
                                } else {
                                    model.setActual_opportunity_size(tvActualSize.getText().toString());
                                }

                                Log.d("TAG", "opportunity_stage : " + oppor_stage);
                                Log.d("TAG", "progress : " + tvProgress.getText().toString());

                                Intent i = new Intent(context, OpportunityAddExtActivity.class);
                                i.putExtra("opportunityModel", model);
                                i.putExtra("pic", model.getAccount_contact());
                                i.putExtra("from_detail", from_detail);
                                i.putExtra("byStatgeActivity", byStatgeActivity);
                                startActivity(i);
                            } else if(oppor_stage.equalsIgnoreCase("8")){
                                model.setAccount_name(tvAccountName.getText().toString());
                                model.setAccount_id(account_id);
                                model.setName(etOpportunityName.getText().toString());
                                model.setSize(String.valueOf(etSizeProject.getRawValue()));
                                model.setOppor_status_id(oppor_status);
                                model.setType(oppor_type);
                                model.setClose_date(tvCloseDate.getText().toString());
                                model.setStage_id(oppor_stage);
                                model.setProgress(tvProgress.getText().toString().replace("%", ""));
                                model.setAssign_to(assign_id);
                                model.setIndustry(oppor_industry);
                                model.setCategory_id(oppor_category);
                                if (reasonId.equals("0") || reasonId.equalsIgnoreCase("")) {
                                    tvReasonNotDeal.setError("Actual Size is is required");
                                    return;
                                } else {
                                    model.setReason_not_deal(reasonId);
                                }
                                model.setFinal_quotation_id(qoutationId);
                                model.setActual_opportunity_size(tvActualSize.getText().toString());

                                Log.d("TAG", "ACTUAL_SIZE : " + tvActualSize.getText().toString());

                                Intent i = new Intent(context, OpportunityAddExtActivity.class);
                                i.putExtra("opportunityModel", model);
                                i.putExtra("pic", model.getAccount_contact());
                                i.putExtra("from_detail", from_detail);
                                startActivity(i);
                            } else {
                                model.setAccount_name(tvAccountName.getText().toString());
                                model.setAccount_id(account_id);
                                model.setName(etOpportunityName.getText().toString());
                                model.setSize(String.valueOf(etSizeProject.getRawValue()));
                                model.setOppor_status_id(oppor_status);
                                model.setType(oppor_type);
                                model.setClose_date(tvCloseDate.getText().toString());
                                model.setStage_id(oppor_stage);
                                model.setProgress(tvProgress.getText().toString().replace("%", ""));
                                model.setAssign_to(assign_id);
                                model.setIndustry(oppor_industry);
                                model.setCategory_id(oppor_category);
                                model.setFinal_quotation_id(qoutationId);
                                model.setActual_opportunity_size(tvActualSize.getText().toString());
                                model.setReason_not_deal(reasonId);
                                Intent i = new Intent(context, OpportunityAddExtActivity.class);
                                i.putExtra("opportunityModel", model);
                                i.putExtra("pic", model.getAccount_contact());
                                i.putExtra("from_detail", from_detail);
                                i.putExtra("byStatgeActivity", byStatgeActivity);
                                startActivity(i);
                            }
                        } else if (convert) {
                            if (oppor_stage.equalsIgnoreCase("7")) {
                                model = new OpportunityModel();
                                model.setAccount_name(tvAccountName.getText().toString());
                                model.setAccount_id(account_id);
                                model.setAccount_id(account_id);
                                model.setName(etOpportunityName.getText().toString());
                                model.setSize(String.valueOf(etSizeProject.getRawValue()));
                                model.setOppor_status_id(oppor_status);
                                model.setType(oppor_type);
                                model.setClose_date(tvCloseDate.getText().toString());
                                model.setStage_id(oppor_stage);
                                model.setProgress(tvProgress.getText().toString().replace("%", ""));
                                model.setAssign_to(assign_id);
                                model.setIndustry(oppor_industry);
                                model.setCategory_id(oppor_category);
                                model.setFinal_quotation_id(qoutationId);
                                model.setReason_not_deal(reasonId);
                                if (tvActualSize.getText().toString().equalsIgnoreCase("")) {
                                    tvActualSize.setError("Actual Size is is required");
                                    return;
                                } else {
                                    model.setActual_opportunity_size(tvActualSize.getText().toString());
                                }
                                Intent i = new Intent(context, OpportunityAddExtActivity.class);
                                i.putExtra("opportunityModel", model);
                                i.putExtra("pic", contact_json);
                                i.putExtra("convert", convert);
                                startActivity(i);
                            } else if(oppor_stage.equalsIgnoreCase("8")){
                                model.setAccount_name(tvAccountName.getText().toString());
                                model.setAccount_id(account_id);
                                model.setName(etOpportunityName.getText().toString());
                                model.setSize(String.valueOf(etSizeProject.getRawValue()));
                                model.setOppor_status_id(oppor_status);
                                model.setType(oppor_type);
                                model.setClose_date(tvCloseDate.getText().toString());
                                model.setStage_id(oppor_stage);
                                model.setProgress(tvProgress.getText().toString().replace("%", ""));
                                model.setAssign_to(assign_id);
                                model.setIndustry(oppor_industry);
                                model.setCategory_id(oppor_category);
                                if (reasonId.equals("0") || reasonId.equalsIgnoreCase("")) {
                                    tvReasonNotDeal.setError("Actual Size is is required");
                                    return;
                                } else {
                                    model.setReason_not_deal(reasonId);
                                }
                                model.setFinal_quotation_id(qoutationId);
                                model.setActual_opportunity_size(tvActualSize.getText().toString());

                                Log.d("TAG", "ACTUAL_SIZE : " + tvActualSize.getText().toString());

                                Intent i = new Intent(context, OpportunityAddExtActivity.class);
                                i.putExtra("opportunityModel", model);
                                i.putExtra("pic", model.getAccount_contact());
                                i.putExtra("from_detail", from_detail);
                                startActivity(i);
                            } else {
                                model = new OpportunityModel();
                                model.setAccount_name(tvAccountName.getText().toString());
                                model.setAccount_id(account_id);
                                model.setAccount_id(account_id);
                                model.setName(etOpportunityName.getText().toString());
                                model.setSize(String.valueOf(etSizeProject.getRawValue()));
                                model.setOppor_status_id(oppor_status);
                                model.setType(oppor_type);
                                model.setClose_date(tvCloseDate.getText().toString());
                                model.setStage_id(oppor_stage);
                                model.setProgress(tvProgress.getText().toString().replace("%", ""));
                                model.setAssign_to(assign_id);
                                model.setIndustry(oppor_industry);
                                model.setCategory_id(oppor_category);
                                model.setFinal_quotation_id(qoutationId);
                                model.setActual_opportunity_size(tvActualSize.getText().toString());
                                model.setReason_not_deal(reasonId);
                                Intent i = new Intent(context, OpportunityAddExtActivity.class);
                                i.putExtra("opportunityModel", model);
                                i.putExtra("pic", contact_json);
                                i.putExtra("convert", convert);
                                startActivity(i);
                            }
                        } else {
                            model = new OpportunityModel();
                            model.setAccount_name(tvAccountName.getText().toString());
                            model.setAccount_id(account_id);
                            model.setName(etOpportunityName.getText().toString());
                            model.setSize(String.valueOf(etSizeProject.getRawValue()));
                            model.setOppor_status_id(oppor_status);
                            model.setType(oppor_type);
                            model.setClose_date(tvCloseDate.getText().toString());
                            model.setStage_id(oppor_stage);
                            model.setProgress(tvProgress.getText().toString().replace("%", ""));
                            model.setAssign_to(assign_id);
                            model.setIndustry(oppor_industry);
                            model.setCategory_id(oppor_category);
                            model.setReason_not_deal(reasonId);
                            Intent i = new Intent(context, OpportunityAddExtActivity.class);
                            i.putExtra("opportunityModel", model);
                            i.putExtra("pic", contact_json);
                            startActivity(i);
                        }
                    }
                    break;
                case R.id.tvAccountName:
                    AccountDialogFragment accountDialogFragment = new AccountDialogFragment(OpportunityAddActivity.this, new AccountDialogCallback() {

                        @Override
                        public void onAccountDialogCallback(Bundle data) {
                            tvAccountName.setText(data.getString("name"));
                            contact_json = data.getString("pic");
                            account_id = data.getString("id");

                            Log.d("TAG", "Contact JSON : " + contact_json);
                        }
                    });
                    accountDialogFragment.show(getSupportFragmentManager(), "accountDialog");
                    break;
                case R.id.ivAccountName:
                    AccountDialogFragment DialogFragment = new AccountDialogFragment(OpportunityAddActivity.this, new AccountDialogCallback() {

                        @Override
                        public void onAccountDialogCallback(Bundle data) {
                            tvAccountName.setText(data.getString("name"));
                            contact_json = data.getString("pic");
                            account_id = data.getString("id");

                            Log.d("TAG", "Contact JSON : " + contact_json);
                        }
                    });
                    DialogFragment.show(getSupportFragmentManager(), "accountDialog");
                    break;
                case R.id.tvCloseDate:
                    datepicker.setDateFragment(year, month, day);
                    datepicker.show(getFragmentManager(), "datePicker");
                    break;
                case R.id.tvQoutation:
                    QuotationDialogFragment quotationDialogFragment = new QuotationDialogFragment(OpportunityAddActivity.this, new QuotationDialogCallback() {
                        @Override
                        public void onQuotationDialogCallback(Bundle data) {
                            tvQoutation.setText(data.getString("references"));
                            qoutationId = data.getString("id");
                            tvActualSize.setText(data.getString("amount"));
                            Log.d("TAG", "Quotation_id " + qoutationId);
                        }
                    });
                    Bundle args = new Bundle();
                    args.putString("opportunity_id", model.getId());
                    Log.d("TAG", "OPPORTUNITY_ID " + model.getId());
                    quotationDialogFragment.setArguments(args);
                    quotationDialogFragment.show(getSupportFragmentManager(), "qoutationDialog");
                    break;
                case R.id.tvReasonNotDeal:
                    ReasonNotDealFragment reasonNotDealFragment = new ReasonNotDealFragment(OpportunityAddActivity.this, new ReasonDialogCallback() {
                        @Override
                        public void onReason(Bundle data) {
                            tvReasonNotDeal.setText(data.getString("reason"));
                            reasonId = data.getString("id");
                            Log.d("TAG","idReason = " + reasonId);
                        }
                    });
                    reasonNotDealFragment.show(getSupportFragmentManager(),"reasonDialog");
                default:
                    break;
            }
        }
    };

    private boolean checkField() {
        if (account_id.equalsIgnoreCase("")) {
            tvAccountName.setError("Account name is required");
            return false;
        } else if (etOpportunityName.getText().toString().equalsIgnoreCase("")) {
            etOpportunityName.setError("Opportunity name is required !!!");
            return false;
        } else if (tvCloseDate.getText().toString().equalsIgnoreCase("")) {
            tvCloseDate.setError("the Closing Date cannot be Empty !!!");
            return false;
//        } else if(tvActualSize.getText().toString().equalsIgnoreCase("")) {
//            tvActualSize.setError("Actual Size is is required");
//            return false;
        }
        return true;
    }

    DatePickerDialog.OnDateSetListener dateListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int r_year, int monthOfYear,
                              int dayOfMonth) {
            year = r_year;
            month = monthOfYear + 1;
            day = dayOfMonth;

            string_year = r_year + "";

            if (month < 10) {

                string_month = "0" + month;
            } else {
                string_month = month + "";
            }
            if (day < 10) {

                string_day = "0" + day;
            } else {
                string_day = day + "";
            }
            open_date = year + "-" + string_month + "-" + string_day;

            tvCloseDate.setText(year + "-" + string_month + "-" + string_day);
        }
    };

    @Override
    public void finishReason(String result, List<ReasonModel> list) {

    }
}
