package com.msf.project.sales1crm.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.msf.project.sales1crm.ErrorAPIKeyActivity;
import com.msf.project.sales1crm.MenuActivity;
import com.msf.project.sales1crm.OpportunityAddActivity;
import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.adapter.Vp_OpportunityStage;
import com.msf.project.sales1crm.callback.DashboardCallback;
import com.msf.project.sales1crm.callback.SortDialogCallback;
import com.msf.project.sales1crm.callback.VpCallback;
import com.msf.project.sales1crm.customview.CustomEditText;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.model.AbsenceModel;
import com.msf.project.sales1crm.model.DashboardFirstModel;
import com.msf.project.sales1crm.model.DashboardSecondModel;
import com.msf.project.sales1crm.model.DashboardThirdModel;
import com.msf.project.sales1crm.model.OpportunityModel;
import com.msf.project.sales1crm.model.TotalOpportunityModel;
import com.msf.project.sales1crm.model.VpModel;
import com.msf.project.sales1crm.presenter.DashboardPresenter;
import com.msf.project.sales1crm.presenter.VpPresenter;
import com.msf.project.sales1crm.service.GPSTracker;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Vp_opportunityTab extends Fragment implements VpCallback, DashboardCallback {
    @BindView(R.id.etSearch)
    CustomEditText etSearch;
    @BindView(R.id.ivClear)
    ImageView ivClear;
    @BindView(R.id.tvRefresh)
    CustomTextView tvRefresh;
    @BindView(R.id.rlNoConnection)
    RelativeLayout rlNoConnection;
    @BindView(R.id.llNotFound)
    LinearLayout llNotFound;
    @BindView(R.id.pagerVp)
    ViewPager pagerVp;
    @BindView(R.id.faOpportunityAdd)
    FloatingActionButton faOpportunityAdd;
    @BindView(R.id.menu)
    FloatingActionMenu menu;
    @BindView(R.id.rlOpportunity)
    RelativeLayout rlOpportunity;
    @BindView(R.id.shimmerLayout)
    ShimmerFrameLayout shimmerLayout;
    @BindView(R.id.llshim)
    LinearLayout llshim;
    private View view;
    private Context context;
    private Vp_OpportunityStage adapter;
    private VpPresenter presenter;
    private List<VpModel> list = new ArrayList<>();
    private List<OpportunityModel> opporList = new ArrayList<>();
    private DashboardPresenter dashboardPresenter;
    private String[] sort = {"Name A to Z", "Name Z to A","Size Low to High","Size High to Low",};
    private int prevSize = 0, sort_id = 0;
    private boolean isLoadMore = true;
    private boolean isMaxSize = true;
    private boolean pause = false;

    public static Fragment newInstance() {
        Vp_opportunityTab fragment = new Vp_opportunityTab();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.vp_opportunity_tab, container, false);
        ButterKnife.bind(this, this.view);
        this.context = getActivity();
        return this.view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, this.view);
        this.context = getActivity();
        this.presenter = new VpPresenter(context, this);
        this.dashboardPresenter = new DashboardPresenter(context, this);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Opportunity");
        initData();
    }



    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.ivClear:
                    etSearch.setText("");
                    etSearch.clearFocus();
                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);
                    getDataFromApi(0, 0);
                    break;
                case R.id.tvRefresh:
                    llshim.setVisibility(View.VISIBLE);
                    rlNoConnection.setVisibility(View.GONE);
                    getDataFromApi(0, 0);
                    break;
            }
        }
    };



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_opportunity2, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_list:
                MenuActivity.getInstance().setFragment(OpportunityTabFragment.newInstance());
                item.setIcon(R.drawable.ic_list_white_24dp);
                break;
            case R.id.action_cards:
                MenuActivity.getInstance().setFragment(OpportunityTabCardFragment.newInstance());
                item.setIcon(R.drawable.ic_cards_black_24dp);
                break;
            case R.id.action_filter:
                SortDialogFragment dialogFragment = new SortDialogFragment(context, new SortDialogCallback() {
                    @Override
                    public void onDialogCallback(Bundle data) {
                        llshim.setVisibility(View.VISIBLE);
                        shimmerLayout.setVisibility(View.VISIBLE);
                        shimmerLayout.startShimmer();
                        sort_id = Integer.parseInt(data.getString("id"));
                        getDataFromApi(0,sort_id);
                    }
                },sort);
                dialogFragment.show(getActivity().getSupportFragmentManager(),"sortDialog");
                break;
            case R.id.action_refresh:
                llshim.setVisibility(View.VISIBLE);
                shimmerLayout.setVisibility(View.VISIBLE);
                shimmerLayout.startShimmer();
                if(Sales1CRMUtils.ConnectionUtility.isNetworkConnected(context)){
                    presenter.listVp(ApiParam.API_111, 0, 0,etSearch.getText().toString());
                } else {
                    rlNoConnection.setVisibility(view.VISIBLE);
                }
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }


    private void initData() {
        etSearch.clearFocus();
        setTextWatcherForASearch();
        getDataFromApi(0, 0);

        ivClear.setOnClickListener(click);
        tvRefresh.setOnClickListener(click);
    }

    private void setTextWatcherForASearch(){
        etSearch.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus) {
                    if(etSearch.getText().length() == 0){
                        ivClear.setVisibility(View.VISIBLE);
                    }
                }
            }
        });
        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    llshim.setVisibility(View.VISIBLE);
                    shimmerLayout.setVisibility(View.VISIBLE);
                    shimmerLayout.startShimmer();
                    getDataFromApi(0, 0);
                }

                return true;
            }
        });
    }

    private void getDataFromApi(int offset, int sort) {
        llNotFound.setVisibility(View.GONE);
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(context)) {
            GPSTracker gpsTracker = new GPSTracker(context);
            dashboardPresenter.setupFirst(ApiParam.API_005);
          new Handler().postDelayed(new Runnable() {
              @Override
              public void run() {
                  presenter.listVp(ApiParam.API_111, offset, sort, etSearch.getText().toString());
              }
          },1000);

        } else {
            rlNoConnection.setVisibility(View.VISIBLE);
        }
    }

    private void setList() {
        llshim.setVisibility(View.VISIBLE);
        shimmerLayout.setVisibility(View.GONE);
        adapter = new Vp_OpportunityStage(context, this.list, this.opporList);
        List<Vp_OpportunityStage.Row> rows = new ArrayList<>();
        for (VpModel vp : list) {
            rows.add(new Vp_OpportunityStage.Item(vp));
        }

        pagerVp.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        adapter.setRows(rows);
        pagerVp.setAdapter(adapter);
        llshim.setVisibility(View.GONE);
        shimmerLayout.stopShimmer();
        shimmerLayout.setVisibility(View.INVISIBLE);
    }


    @Override
    public void finishVp(String result, List<VpModel> list) {
        if (result.equalsIgnoreCase("OK")) {
            prevSize = list.size();
            this.list = list;

            isMaxSize = false;
            isLoadMore = false;
            setList();
            if (list.size() > 0) {
                llNotFound.setVisibility(View.GONE);
            } else {
                llNotFound.setVisibility(View.VISIBLE);
            }
        } else if (result.equalsIgnoreCase("FAILED_KEY")) {
            startActivity(new Intent(context, ErrorAPIKeyActivity.class));
            MenuActivity.getInstance().finish();
        } else {
            rlNoConnection.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void finishMoreVp(String result, List<VpModel> list) {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void finishFirst(String result, DashboardFirstModel model) {
        if(result.equalsIgnoreCase("OK")){
            if(model.getOpportunity_create_permission().equalsIgnoreCase("1")){
                faOpportunityAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                            startActivity(new Intent(context, OpportunityAddActivity.class));
                    }
                });
                faOpportunityAdd.setColorNormal(R.color.BlueCRM);
                faOpportunityAdd.setColorPressed(R.color.colorPrimaryDark);
                faOpportunityAdd.setColorPressedResId(R.color.colorPrimaryDark);
                faOpportunityAdd.setColorNormalResId(R.color.BlueCRM);
            } else {
                faOpportunityAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
                    }
                });
                faOpportunityAdd.setColorNormal(R.color.BlueCRM_light);
                faOpportunityAdd.setColorPressed(R.color.colorPrimaryDark_light);
                faOpportunityAdd.setColorPressedResId(R.color.colorPrimaryDark_light);
                faOpportunityAdd.setColorNormalResId(R.color.BlueCRM_light);
            }
        }
    }

    @Override
    public void onResume() {
        getDataFromApi(0,0);
        initData();
        super.onResume();

    }

    @Override
    public void finishSecond(String result, DashboardSecondModel model) {

    }


    @Override
    public void finishThird(String result, List<DashboardThirdModel> listLead, List<DashboardThirdModel> listLead1, List<DashboardThirdModel> listOppor, List<DashboardThirdModel> listOppor1) {

    }

    @Override
    public void TotOpportunity(String result, TotalOpportunityModel totOporModel) {

    }

    @Override
    public void absence(String result, AbsenceModel model) {

    }
}
