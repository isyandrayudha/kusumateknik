package com.msf.project.sales1crm.presenter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;

import com.msf.project.sales1crm.callback.GeneralDashboardCallback;
import com.msf.project.sales1crm.model.SalesCycleModel;
import com.msf.project.sales1crm.model.Top10CustomerModel;
import com.msf.project.sales1crm.model.TotalModel;
import com.msf.project.sales1crm.service.NetworkConnection;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.PreferenceUtility;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;
import com.msf.project.sales1crm.utility.Sales1CRMUtilsJSON;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ViewDashboardGeneralDashboardPresenter {
    private final String TAG = ViewDashboardGeneralDashboardPresenter.class.getSimpleName();
    private Context context;
    private Messenger messenger;
    private Message message;
    private Bundle bundle;
    private Handler handler;
    private String failureResponse = "";
    private String[] stringResponse = {""};
    private GeneralDashboardCallback callback;
    private String result = "NG";
    private SalesCycleModel model;
    private List<TotalModel> wonIndustry = new ArrayList<>();
    private List<TotalModel> loseIndustry = new ArrayList<>();
    private List<TotalModel> wonSource = new ArrayList<>();
    private List<TotalModel> loseSource = new ArrayList<>();
    private List<Top10CustomerModel> top10Account = new ArrayList<>();

    public ViewDashboardGeneralDashboardPresenter(Context context, GeneralDashboardCallback listener) {
        this.context = context;
        this.callback = listener;
    }

    private void doNetworkService(String url, String params, String from) {
        Log.d(TAG, "url login " + url);
        Log.d(TAG, "params : " + params);
        Intent networkIntent = new Intent(this.context, NetworkConnection.class);
        this.messenger = new Messenger(this.handler);
        networkIntent.putExtra("messenger", this.messenger);
        networkIntent.putExtra("url", url);
        networkIntent.putExtra("params", params);
        networkIntent.putExtra("from", from);
        this.context.startService(networkIntent);
    }

    public void setupTotal(int apiIndex, String startDate, String endDate) {
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)) {
            obtainItem(apiIndex, startDate, endDate);
        }else{
            callback.finished(this.result, this.model, this.wonIndustry, this.loseIndustry, this.wonSource, this.loseSource, this.top10Account);
        }
    }

    public void obtainItem(int apiIndex, String startDate, String endDate) {
        this.handler = new Handler(this.context.getMainLooper()) {

            @Override
            public void handleMessage(Message msg) {
                parseItem(msg);
            }

        };

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("api_key", PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.API_KEY));
            jsonObject.put("start_date", startDate);
            jsonObject.put("end_date", endDate);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        doNetworkService(ApiParam.urlBuilder(this.context, apiIndex), jsonObject.toString(), "LeadConversion");
    }

    private void parseItem(Message message) {
        this.message = message;
        this.bundle = this.message.getData();
        this.stringResponse[0] = this.bundle.getString("network_response");
        this.failureResponse = this.bundle.getString("network_failure");
        Log.d(TAG, "responseString[0] login info " + this.stringResponse[0]);
        if (this.failureResponse.equalsIgnoreCase("yes")){
            callback.finished(this.result, this.model, this.wonIndustry, this.loseIndustry, this.wonSource, this.loseSource, this.top10Account);
        }else{
            try {
                this.result = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                if (this.result.equalsIgnoreCase("OK")){
                    this.model = Sales1CRMUtilsJSON.JSONUtility.getGeneralDashboard1(this.stringResponse[0]);
                    this.wonIndustry = Sales1CRMUtilsJSON.JSONUtility.getGeneralDashboard2(this.stringResponse[0]);
                    this.loseIndustry = Sales1CRMUtilsJSON.JSONUtility.getGeneralDashboard3(this.stringResponse[0]);
                    this.wonSource = Sales1CRMUtilsJSON.JSONUtility.getGeneralDashboard4(this.stringResponse[0]);
                    this.loseSource = Sales1CRMUtilsJSON.JSONUtility.getGeneralDashboard5(this.stringResponse[0]);
                    this.top10Account = Sales1CRMUtilsJSON.JSONUtility.getGeneralDashboard6(this.stringResponse[0]);
                }
                callback.finished(this.result, this.model, this.wonIndustry, this.loseIndustry, this.wonSource, this.loseSource, this.top10Account);
            } catch (JSONException e) {
                Log.d(TAG, "Exception detail Response " + e.getMessage());
            }
        }
    }
}
