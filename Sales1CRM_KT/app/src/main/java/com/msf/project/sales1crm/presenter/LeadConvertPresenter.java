package com.msf.project.sales1crm.presenter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;

import com.msf.project.sales1crm.callback.ConvertCallback;
import com.msf.project.sales1crm.model.LeadConvertModel;
import com.msf.project.sales1crm.service.NetworkConnection;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.PreferenceUtility;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;
import com.msf.project.sales1crm.utility.Sales1CRMUtilsJSON;

import org.json.JSONException;
import org.json.JSONObject;

public class LeadConvertPresenter {
private final String TAG = LeadConvertPresenter.class.getSimpleName();
    private Context context;
    private Messenger messenger;
    private Message message;
    private Bundle bundle;
    private Handler handler;
    private String failureResponse = "";
    private String[] stringResponse = {""};
    private ConvertCallback callback;
    private String result = "NG";
    private LeadConvertModel model;
    private String response = "Tolong Periksa Koneksi Anda";

    public LeadConvertPresenter(Context context, ConvertCallback callback){
        this.context = context;
        this.callback = callback;
    }
    private void doNetworkService(String url, String params, String from) {
        Log.d(TAG, "url login " + url);
        Log.d(TAG, "params : " + params);
        Intent networkIntent = new Intent(this.context, NetworkConnection.class);
        this.messenger = new Messenger(this.handler);
        networkIntent.putExtra("messenger", this.messenger);
        networkIntent.putExtra("url", url);
        networkIntent.putExtra("params", params);
        networkIntent.putExtra("from", from);
        this.context.startService(networkIntent);
    }

    public void setupConvert(int apiIndex, String id,String convertStatus){
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)) {
obtainConvert(apiIndex,id, convertStatus);
        } else {
            callback.finishConvert(this.result, this.model);
        }
    }

    public void obtainConvert(int apiIndex, String id, String convertStatus){
        this.handler = new Handler(this.context.getMainLooper()){
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                parseConvert(msg);
            }
        };
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("api_key", PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.API_KEY));
            jsonObject.put("id",id);
            jsonObject.put("convert_status",convertStatus);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        doNetworkService(ApiParam.urlBuilder(this.context,apiIndex),jsonObject.toString(),"convert");
    }
private void parseConvert(Message message){
    this.message = message;
    this.bundle = this.message.getData();
    this.stringResponse[0] = this.bundle.getString("network_response");
    this.failureResponse = this.bundle.getString("network_failure");
    Log.d(TAG, "responseString[0] convert " + this.stringResponse[0]);
    if (this.failureResponse.equalsIgnoreCase("yes")) {
        this.callback.finishConvert(this.result,this.model);
    } else {
        try{
            this.result = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
            if (this.result.equalsIgnoreCase("OK")){
                this.model = Sales1CRMUtilsJSON.JSONUtility.getConvert(this.stringResponse[0]);
            }
            this.callback.finishConvert(this.result, this.model);
        } catch (JSONException e){
            Log.d(TAG, "Exception detail Response " + e.getMessage());
        }

    }
}
}
