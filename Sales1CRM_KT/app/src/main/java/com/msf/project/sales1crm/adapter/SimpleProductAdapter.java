package com.msf.project.sales1crm.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.msf.project.sales1crm.ExistingProductAddActivity;
import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.database.DBSchema;
import com.msf.project.sales1crm.database.ProductDao;
import com.msf.project.sales1crm.model.Product;


import java.util.List;


public class SimpleProductAdapter extends BaseAdapter {
    private Context context;
    private LayoutInflater layoutInflater;
    private List<Product> products;


    public SimpleProductAdapter(Context context, List<Product> obj) {
        this.context = context;
        this.products = obj;
    }

    @Override
    public int getCount() {
        if (products.size() <= 0) {
            return 1;
        }
        return products.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder holder;
        if (convertView == null) {
            layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.row_product, null);

            holder = new ViewHolder();
            holder.brandName = (CustomTextView) view.findViewById(R.id.brandName);
            holder.typeName = (CustomTextView) view.findViewById(R.id.typeName);
            holder.serialNumber = (CustomTextView) view.findViewById(R.id.serialNumber);
            holder.condition = (CustomTextView) view.findViewById(R.id.condition);
            holder.llContainer = (LinearLayout) view.findViewById(R.id.llContainer);
            holder.tvNoData = (CustomTextView) view.findViewById(R.id.tvNoData);
            holder.deleteProduct = (RelativeLayout) view.findViewById(R.id.deleteProduct);
            holder.editProduct = (RelativeLayout) view.findViewById(R.id.editProduct);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        if (products.size() <= 0) {
            holder.tvNoData.setText("No Data");
            holder.llContainer.setVisibility(View.GONE);
        } else {
            holder.brandName.setText(products.get(position).getBrand());
            holder.typeName.setText(products.get(position).getType());
            holder.serialNumber.setText(products.get(position).getSerialNumber());
            holder.condition.setText(products.get(position).getCondition());
            holder.deleteProduct.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(context);
                    alert.setTitle("Warning !!");
                    alert.setMessage("Delete this product?");
                    alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ProductDao productDao = new ProductDao(context);
                            Product pProduct = productDao.getProductById(products.get(position).getId_temp());
                            pProduct.setStatus("1");
                            productDao.updateTable(pProduct,products.get(position).getId_temp(),DBSchema.Product.KEY_ID_TEMP);
                            notifyDataSetChanged();
                            ((Activity) context).setResult(Activity.RESULT_OK);
                            holder.llContainer.setVisibility(View.GONE);
                            holder.tvNoData.setVisibility(View.GONE);
                            dialog.dismiss();
                        }
                    });

                    alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    alert.show();
                }
            });

            holder.editProduct.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(context, ExistingProductAddActivity.class);
                    i.putExtra("idTemp",products.get(position).getId_temp());
                    i.putExtra("idProduct",products.get(position).getId());
                    i.putExtra("brandProduct", products.get(position).getBrand());
                    i.putExtra("typeProduct", products.get(position).getType());
                    i.putExtra("serialProduct", products.get(position).getSerialNumber());
                    i.putExtra("conditionProduct", products.get(position).getCondition());
                    i.putExtra("editProduct",true);
                    context.startActivity(i);
                }
            });


        }
        return view;
    }


    public class ViewHolder {
        CustomTextView tvNoData;
        CustomTextView brandName;
        CustomTextView typeName;
        CustomTextView serialNumber;
        CustomTextView condition;
        RelativeLayout deleteProduct;
        RelativeLayout editProduct;
        LinearLayout llContainer;
    }

}
