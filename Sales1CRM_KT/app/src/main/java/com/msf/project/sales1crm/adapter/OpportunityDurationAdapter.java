package com.msf.project.sales1crm.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;

import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.model.TotalModel;

import java.util.List;

public class OpportunityDurationAdapter extends BaseAdapter {
    private Context context;
    private LayoutInflater inflater;
    private List<TotalModel> arrModel;

    public OpportunityDurationAdapter(Context context,
                              List<TotalModel> arrModel) {
        this.context = context;
        this.arrModel = arrModel;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        if(arrModel.size()<=0){
            return 1;
        }
        return arrModel.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(final int position, View converView, ViewGroup parent) {
        View view = converView;
        final ViewHolder holder;

        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) parent.getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = (LinearLayout) inflater.inflate(
                    R.layout.row_opportunityduration, parent, false);
            holder = new ViewHolder();
            holder.tvName = (CustomTextView) view
                    .findViewById(R.id.tvName);
            holder.tvTotal = (CustomTextView) view
                    .findViewById(R.id.tvTotal);
            holder.tvAvg = (CustomTextView) view
                    .findViewById(R.id.tvAvg);
            holder.llContainer = (LinearLayout) view
                    .findViewById(R.id.llContainer);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        if (getCount() > 0){
            holder.llContainer.setVisibility(View.VISIBLE);

            holder.tvName.setText(arrModel.get(position).getName());
            holder.tvTotal.setText(arrModel.get(position).getTotal());
            holder.tvAvg.setText(arrModel.get(position).getAvg());
        }else{
            holder.llContainer.setVisibility(View.GONE);
        }

        return view;
    }

    private static class ViewHolder {
        CustomTextView tvName, tvTotal, tvAvg;
        LinearLayout llContainer;
    }
}
