package com.msf.project.sales1crm;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;

import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.maps.MapView;
import com.msf.project.sales1crm.callback.DashboardCallback;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.fragment.AccountFrangment;
import com.msf.project.sales1crm.fragment.ActivityListFragment;
import com.msf.project.sales1crm.fragment.CallsFragment;
import com.msf.project.sales1crm.fragment.DashboardFragment;
import com.msf.project.sales1crm.fragment.DatabaseMachineFragment;
import com.msf.project.sales1crm.fragment.EventFragment;
import com.msf.project.sales1crm.fragment.LeadFragment;
import com.msf.project.sales1crm.fragment.OpportunityTabFragment;
import com.msf.project.sales1crm.fragment.SettingFragment;
import com.msf.project.sales1crm.fragment.TaskFragment;
import com.msf.project.sales1crm.model.AbsenceModel;
import com.msf.project.sales1crm.model.DashboardFirstModel;
import com.msf.project.sales1crm.model.DashboardSecondModel;
import com.msf.project.sales1crm.model.DashboardThirdModel;
import com.msf.project.sales1crm.model.TotalOpportunityModel;
import com.msf.project.sales1crm.presenter.DashboardPresenter;
import com.msf.project.sales1crm.service.AlarmReceive;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.BottomNavigationViewHelper;
import com.msf.project.sales1crm.utility.PreferenceUtility;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class MenuActivity extends AppCompatActivity
        implements  DashboardCallback {


    //    @BindView(R.id.AccountAdd)
//    LinearLayout AccountAdd;
//    @BindView(R.id.AccountQuickAdd)
//    LinearLayout AccountQuickAdd;
//    @BindView(R.id.AccountList)
//    LinearLayout AccountList;
//    @BindView(R.id.bottomSheet)
//    LinearLayout bottomSheet;
    private Context context;
    static MenuActivity menuActivity;
    private NavigationView navigationView;
    private BottomNavigationViewEx bottomNavigationViewEx;
    CoordinatorLayout coordinatorLayout;
    BottomSheetDialog dialog;
    AlarmManager alarmManager;
    PendingIntent pendingIntent;
    //    private CheckFunction checkFunction;
    private DashboardPresenter dashboardPresenter;

    static MenuActivity instance;
    LocationRequest locationRequest;
    FusedLocationProviderClient fusedLocationProviderClient;

    private MapView mapView;


    public static MenuActivity getMenuActivity() {
        return instance;
    }

    private CustomTextView tvCompany, tvFullname, tvVersion;
    private CircleImageView ivAccountPicture;
    private String url = "";


    public static MenuActivity getInstance() {
        return menuActivity;
    }

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Mapbox.getInstance(this, getString(R.string.key_access_token_production));

        this.context = this;
        this.dashboardPresenter = new DashboardPresenter(context, this);
//        checkFunction = new CheckFunction(getApplicationContext());
        menuActivity = this;
        instance = this;
        this.url = PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.URL);


//        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinator);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();


        initView();
//        init_modal();


        Dexter.withActivity(this).withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
//                        updateLocation();
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        Toast.makeText(MenuActivity.this, "You Must accept this location", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {

                    }
                }).check();


        if (alarmManager == null) {
            alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            Intent intent = new Intent(this, AlarmReceive.class);
            pendingIntent = PendingIntent.getBroadcast(this, 0, intent, 0);
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 1000 * 60 * 3,
                    pendingIntent);
        }

    }



    //    private void updateLocation() {
//        buildLocationRequest();
//
//        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
//        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//
//            return;
//        }
//        fusedLocationProviderClient.requestLocationUpdates(locationRequest, getPendingIntent());
//    }
//
//    private PendingIntent getPendingIntent() {
//        Intent intent = new Intent(this, MyServiceLocation.class);
//        intent.setAction(MyServiceLocation.ACTION_PROCESS_UPDATE);
//        return PendingIntent.getBroadcast(this,0,intent,PendingIntent.FLAG_UPDATE_CURRENT);
//    }
//
//    private void buildLocationRequest() {
//        locationRequest = new LocationRequest();
//        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
//        locationRequest.setInterval(5000);
//        locationRequest.setFastestInterval(10000);
//        locationRequest.setSmallestDisplacement(10f);
//
//    }


    /**
     * BottomNavigationView Setup
     */


    private void switchFragment(Fragment selectedFragment, MenuItem item) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//        transaction.add(new DashboardFragment(),"Home");
//        transaction.addToBackStack("Home");
        transaction.replace(R.id.frame_layout, selectedFragment);
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.commit();
        item.setChecked(true);
    }


    public void init_modal() {
        View modalbottomsheet = getLayoutInflater().inflate(R.layout.fragment_bottom_sheet_dialog_account, null);
        dialog = new BottomSheetDialog(this);
        dialog.setContentView(modalbottomsheet);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);

    }


    private void initView() {
        dashboardPresenter.setupFirst(ApiParam.API_005);
        navigationView = (NavigationView) findViewById(R.id.nvMenu);
//        navigationView.setNavigationItemSelectedListener(this);
        navigationView.getMenu().getItem(0).setChecked(true);


        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout, DashboardFragment.newInstance());
        transaction.commit();


        View headerView = navigationView.getHeaderView(0);
        tvCompany = (CustomTextView) headerView.findViewById(R.id.tvCompany);
        tvFullname = (CustomTextView) headerView.findViewById(R.id.tvFullname);
        tvVersion = (CustomTextView) headerView.findViewById(R.id.tvVersion);
        ivAccountPicture = (CircleImageView) headerView.findViewById(R.id.ivAccountPicture);

        tvCompany.setText("Sales1CRM " + PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.KEY_COMPANY));
        tvFullname.setText(PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.KEY_FULLNAME));
        Log.i("tag", PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.KEY_IMAGES_MASTER));
        Log.i("tag url", PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.URL));
        Picasso.get().load(url + PreferenceUtility.getInstance()
                .loadDataString(context, PreferenceUtility.KEY_IMAGES_MASTER))
                .error(R.drawable.avatar_placeholder)
                .into(ivAccountPicture);


        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            tvVersion.setText("Ver" + packageInfo.versionName.toString());
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
            return;
        }

        super.onBackPressed();
    }




//    @SuppressWarnings("StatementWithEmptyBody")
//    @Override
//    public boolean onNavigationItemSelected(MenuItem item) {
//        // Handle navigation view item clicks here.
//        final int[] selectPosition = {0};
//        int size = navigationView.getMenu().size();
//        for (int i = 0; i < size; i++) {
//            navigationView.getMenu().getItem(i).setChecked(false);
//        }
//
//        Menu menu = bottomNavigationViewEx.getMenu();
//
//        Fragment selectedFragment = null;
//        switch (item.getItemId()) {
//            case R.id.nvDashboard:
//                selectedFragment = DashboardFragment.newInstance();
//                item.setIcon(R.drawable.dashboard);
//                item.setChecked(true);
//                menu.getItem(0).setChecked(true);
//                selectPosition[0] = 0;
//                swt(selectedFragment, item);
//                break;
//            case R.id.nvAccount:
////                if (!checkFunction.userAccessControl(1)) {
////                    Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
////                    item.setIcon(R.drawable.account);
////                } else {
////                    item.setIcon(R.drawable.account);
////                    selectedFragment = AccountFrangment.newInstance();
////                    item.setIcon(R.drawable.account);
////                    item.setCheckable(true);
////                    menu.getItem(1).setChecked(true);
////                    selectPosition[0] = 1;
////                    swt(selectedFragment, item);
////                }
//                break;
//            case R.id.nvOpportunity:
////                if (!checkFunction.userAccessControl(55)) {
////                    Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
////                    item.setIcon(R.drawable.opportunity);
////                } else {
////                    item.setIcon(R.drawable.opportunity);
////                    selectedFragment = OpportunityTabFragment.newInstance();
////                    item.setIcon(R.drawable.opportunity);
////                    item.setCheckable(true);
////                    menu.getItem(2).setChecked(true);
////                    selectPosition[0] = 2;
////                    swt(selectedFragment, item);
////                }
//                break;
//            case R.id.nvLead:
////                if (!checkFunction.userAccessControl(44)) {
////                    Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
////                    item.setIcon(R.drawable.leads);
////                } else {
////                    item.setIcon(R.drawable.leads);
////                    selectedFragment = LeadFragment.newInstance();
////                    item.setIcon(R.drawable.leads);
////                    item.setCheckable(true);
////                    menu.getItem(3).setChecked(true);
////                    selectPosition[0] = 3;
////                    swt(selectedFragment, item);
////                }
//                break;
//            case R.id.nvEvent:
////                if (!checkFunction.userAccessControl(103)) {
////                    Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
////                    item.setIcon(R.drawable.events);
////                } else {
////                    item.setIcon(R.drawable.events);
////                    selectedFragment = EventFragment.newInstance();
////                    item.setIcon(R.drawable.events);
////                    item.setCheckable(true);
////                    menu.getItem(4).setChecked(true);
////                    selectPosition[0] = 4;
////                    swt(selectedFragment, item);
////                }
//                break;
//            case R.id.nvTask:
////                if (!checkFunction.userAccessControl(97)) {
////                    Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
////                    item.setIcon(R.drawable.to_do_list_task);
////                } else {
////                    item.setIcon(R.drawable.to_do_list_task);
////                    selectedFragment = TaskFragment.newInstance();
////                    item.setIcon(R.drawable.to_do_list_task);
////                    item.setCheckable(true);
////                    menu.getItem(4).setChecked(true);
////                    selectPosition[0] = 4;
////                    swt(selectedFragment, item);
////                }
//                break;
//            case R.id.nvCalls:
////                if (!checkFunction.userAccessControl(109)) {
////                    Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
////                    item.setIcon(R.drawable.calls);
////                } else {
////                    item.setIcon(R.drawable.calls);
////                    selectedFragment = CallsFragment.newInstance();
////                    item.setIcon(R.drawable.calls);
////                    item.setCheckable(true);
////                    menu.getItem(4).setChecked(true);
////                    selectPosition[0] = 4;
////                    swt(selectedFragment, item);
////                }
//                break;
//            case R.id.nvDatabaseMachine:
//                item.setIcon(R.drawable.calls);
//                selectedFragment = DatabaseMachineFragment.newInstance();
//                item.setCheckable(true);
//                swt(selectedFragment, item);
//                break;
////            case R.id.nvStatsReport:
////                selectedFragment = StatsReportFragment.newInstance();
////                break;
//            case R.id.nvSetting:
//                selectedFragment = SettingFragment.newInstance();
//                item.setCheckable(true);
//                swt(selectedFragment, item);
//                break;
//        }
//
//
//        return false;
//    }

    public void swt(Fragment selectedFragment, MenuItem item) {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//        transaction.add(new DashboardFragment(),"Home");
//        transaction.addToBackStack("Home");
        transaction.replace(R.id.frame_layout, selectedFragment);
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.commit();
        item.setChecked(true);
    }


    public void setFragment(Fragment selectedFragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout, selectedFragment);
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.commit();
    }

    @Override
    public void finishFirst(String result, DashboardFirstModel model) {
        navigationView = (NavigationView) findViewById(R.id.nvMenu);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                final int[] selectPosition = {0};
                int size = navigationView.getMenu().size();
                for (int i = 0; i < size; i++) {
                    navigationView.getMenu().getItem(i).setChecked(false);
                }

                Menu menu = bottomNavigationViewEx.getMenu();
//                getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

                Fragment selectedFragment = null;
                switch (item.getItemId()) {
                    case R.id.nvDashboard:
                        selectedFragment = DashboardFragment.newInstance();
                        item.setIcon(R.drawable.dashboard);
                        item.setChecked(true);
                        menu.getItem(0).setChecked(true);
                        selectPosition[0] = 0;
                        swt(selectedFragment, item);
                        break;
                    case R.id.nvAccount:
                        if (!model.getAccount_list_permission().equalsIgnoreCase("1")) {
                            Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
                            item.setIcon(R.drawable.account);
                        } else {
                            item.setIcon(R.drawable.account);
                            selectedFragment = AccountFrangment.newInstance();
                            item.setIcon(R.drawable.account);
                            item.setCheckable(true);
                            menu.getItem(1).setChecked(true);
                            selectPosition[0] = 1;
                            swt(selectedFragment, item);
                        }
                        break;
                    case R.id.nvOpportunity:
                        if (!model.getOpportunity_list_permission().equalsIgnoreCase("1")) {
                            Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
                            item.setIcon(R.drawable.opportunity);
                        } else {
                            item.setIcon(R.drawable.opportunity);
                            selectedFragment = OpportunityTabFragment.newInstance();
                            item.setIcon(R.drawable.opportunity);
                            item.setCheckable(true);
                            menu.getItem(2).setChecked(true);
                            selectPosition[0] = 2;
                            swt(selectedFragment, item);
                        }
                        break;
                    case R.id.nvLead:
                        if (!model.getLead_list_permission().equalsIgnoreCase("1")) {
                            Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
                            item.setIcon(R.drawable.leads);
                        } else {
                            item.setIcon(R.drawable.leads);
                            selectedFragment = LeadFragment.newInstance();
                            item.setIcon(R.drawable.leads);
                            item.setCheckable(true);
                            menu.getItem(3).setChecked(true);
                            selectPosition[0] = 3;
                            swt(selectedFragment, item);
                        }
                        break;
                    case R.id.nvEvent:
                        if (!model.getCrm_event_list_permission().equalsIgnoreCase("1")) {
                            Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
                            item.setIcon(R.drawable.events);
                        } else {
                            item.setIcon(R.drawable.events);
                            selectedFragment = EventFragment.newInstance();
                            item.setIcon(R.drawable.events);
                            item.setCheckable(true);
                            menu.getItem(4).setChecked(true);
                            selectPosition[0] = 4;
                            swt(selectedFragment, item);
                        }
                        break;
                    case R.id.nvTask:
                        if (!model.getCrm_task_list_permission().equalsIgnoreCase("1")) {
                            Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
                            item.setIcon(R.drawable.to_do_list_task);
                        } else {
                            item.setIcon(R.drawable.to_do_list_task);
                            selectedFragment = TaskFragment.newInstance();
                            item.setIcon(R.drawable.to_do_list_task);
                            item.setCheckable(true);
                            menu.getItem(4).setChecked(true);
                            selectPosition[0] = 4;
                            swt(selectedFragment, item);
                        }
                        break;
                    case R.id.nvCalls:
                        if (!model.getCrm_call_list_permission().equalsIgnoreCase("1")) {
                            Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
                            item.setIcon(R.drawable.calls);
                        } else {
                            item.setIcon(R.drawable.calls);
                            selectedFragment = CallsFragment.newInstance();
                            item.setIcon(R.drawable.calls);
                            item.setCheckable(true);
                            menu.getItem(4).setChecked(true);
                            selectPosition[0] = 4;
                            swt(selectedFragment, item);
                        }
                        break;
                    case R.id.nvDatabaseMachine:
                        if(!model.getDatabase_machine_list_permission().equalsIgnoreCase("1")){
                            Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
                            item.setIcon(R.drawable.database_machine_32);
                        } else {
                            item.setIcon(R.drawable.database_machine_32);
                            selectedFragment = DatabaseMachineFragment.newInstance();
                            item.setCheckable(true);
                            swt(selectedFragment, item);
                        }
                        break;
//            case R.id.nvStatsReport:
//                selectedFragment = StatsReportFragment.newInstance();
//                break;
                    case R.id.nvSetting:
                        selectedFragment = SettingFragment.newInstance();
                        item.setCheckable(true);
                        swt(selectedFragment, item);
                        break;
                }


                return false;
            }
        });


        bottomNavigationViewEx = (BottomNavigationViewEx) findViewById(R.id.bnNavigation);
        BottomNavigationViewHelper.setupBottomNavigationView(bottomNavigationViewEx);
        final int[] selectPosition = {0};

        bottomNavigationViewEx.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                int size = bottomNavigationViewEx.getMenu().size();
                for (int i = 0; i < size; i++) {
                    bottomNavigationViewEx.getMenu().getItem(i).setChecked(false);
                }

                Menu menu = navigationView.getMenu();

                Fragment selectedFragment = null;
                switch (item.getItemId()) {
                    case R.id.bnDashboard:
                        selectedFragment = DashboardFragment.newInstance();
                        item.setIcon(R.drawable.dashboard);
                        item.setChecked(true);
                        menu.getItem(0).setChecked(true);
                        selectPosition[0] = 0;
                        switchFragment(selectedFragment, item);
                        break;
                    case R.id.bnAccount:
                        if (!model.getAccount_list_permission().equalsIgnoreCase("1")) {
                            Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
                            item.setIcon(R.drawable.account);
                        } else {
                            item.setChecked(true);
                            selectedFragment = AccountFrangment.newInstance();
                            item.setIcon(R.drawable.account);
                            menu.getItem(1).setChecked(true);
                            selectPosition[0] = 1;
                            switchFragment(selectedFragment, item);
                        }
                        break;
                    case R.id.bnOpportunity:
                        if (!model.getOpportunity_list_permission().equalsIgnoreCase("1")) {
                            Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
                            item.setIcon(R.drawable.opportunity);
                        } else {
                            item.setChecked(true);
                            selectedFragment = OpportunityTabFragment.newInstance();
                            item.setIcon(R.drawable.opportunity);
                            menu.getItem(2).setChecked(true);
                            selectPosition[0] = 2;
                            switchFragment(selectedFragment, item);
                        }
                        break;
                    case R.id.bnLead:
                        if (!model.getLead_list_permission().equalsIgnoreCase("1")) {
                            Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
                            item.setIcon(R.drawable.leads);
                        } else {
                            item.setIcon(R.drawable.leads);
                            selectedFragment = LeadFragment.newInstance();
                            item.setIcon(R.drawable.leads);
                            menu.getItem(3).setChecked(true);
                            selectPosition[0] = 3;
                            switchFragment(selectedFragment, item);
                        }
                        break;
                    case R.id.bnActivity:
                        if (!model.getActivity_list_permission().equalsIgnoreCase("1")) {
                            Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
                        } else {
                            selectedFragment = ActivityListFragment.newInstance();
                            menu.getItem(4).setChecked(true);
                            item.setIcon(R.drawable.folder_64);
                            item.setCheckable(true);
                            selectPosition[0] = 4;
                            switchFragment(selectedFragment, item);
                        }

//                        if (!model.getActivity_list_permission().equalsIgnoreCase("1")) {
//                            Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
//                        } else {
//                            selectedFragment = ActivityListFragment.newInstance();
//                            menu.getItem(4).setCheckable(true);
//                            item.setIcon(R.drawable.folder_64);
//                            item.setCheckable(true);
//                            selectPosition[0] = 4;
//                            switchFragment(selectedFragment, item);
//                        }
//
//                        if (!model.getActivity_list_permission().equalsIgnoreCase("1")) {
//                            Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
//                        } else {
//                            selectedFragment = ActivityListFragment.newInstance();
//                            menu.getItem(4).setCheckable(true);
//                            item.setIcon(R.drawable.folder_64);
//                            item.setCheckable(true);
//                            selectPosition[0] = 4;
//                            switchFragment(selectedFragment, item);
//                        }

                        break;
                }

                return false;


            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        dashboardPresenter.setupFirst(ApiParam.API_005);
    }

    @Override
    public void finishSecond(String result, DashboardSecondModel model) {

    }

    @Override
    public void finishThird(String result, List<DashboardThirdModel> listLead, List<DashboardThirdModel> listLead1, List<DashboardThirdModel> listOppor, List<DashboardThirdModel> listOppor1) {

    }

    @Override
    public void TotOpportunity(String result, TotalOpportunityModel totOporModel) {

    }

    @Override
    public void absence(String result, AbsenceModel model) {

    }
}
