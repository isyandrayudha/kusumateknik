package com.msf.project.sales1crm.callback;

import com.msf.project.sales1crm.model.AccountModel;

import java.util.List;

/**
 * Created by christianmacbook on 14/05/18.
 */

public interface AccountListCallback {
    void finishAccountList(String result, List<AccountModel> list);
    void finishAccountMoreList(String result, List<AccountModel> list);
    void finishQuickCreate(String result);
}
