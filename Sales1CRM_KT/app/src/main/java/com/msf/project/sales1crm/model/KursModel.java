package com.msf.project.sales1crm.model;

public class KursModel{
	private String currencyName;
	private String kurs;
	private String currencyId;

	public void setCurrencyName(String currencyName){
		this.currencyName = currencyName;
	}

	public String getCurrencyName(){
		return currencyName;
	}

	public void setKurs(String kurs){
		this.kurs = kurs;
	}

	public String getKurs(){
		return kurs;
	}

	public void setCurrencyId(String currencyId){
		this.currencyId = currencyId;
	}

	public String getCurrencyId(){
		return currencyId;
	}
}
