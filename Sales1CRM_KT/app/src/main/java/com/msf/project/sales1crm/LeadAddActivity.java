package com.msf.project.sales1crm;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.mapbox.android.core.location.LocationEngine;
import com.mapbox.android.core.location.LocationEngineListener;
import com.mapbox.android.core.location.LocationEnginePriority;
import com.mapbox.android.core.location.LocationEngineProvider;
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.mapboxsdk.annotations.Icon;
import com.mapbox.mapboxsdk.annotations.IconFactory;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.plugins.locationlayer.LocationLayerPlugin;
import com.mapbox.mapboxsdk.plugins.locationlayer.modes.CameraMode;
import com.mapbox.mapboxsdk.plugins.locationlayer.modes.RenderMode;
import com.msf.project.sales1crm.callback.ConvertCallback;
import com.msf.project.sales1crm.callback.LeadAddCallback;
import com.msf.project.sales1crm.callback.LocationDialogCallback;
import com.msf.project.sales1crm.callback.UserDialogCallback;
import com.msf.project.sales1crm.customview.CustomDialog;
import com.msf.project.sales1crm.customview.CustomEditText;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.customview.SpinnerCustomAdapter;
import com.msf.project.sales1crm.database.IndustryDao;
import com.msf.project.sales1crm.database.LeadSourceDao;
import com.msf.project.sales1crm.database.LeadStatusDao;
import com.msf.project.sales1crm.database.PositionDao;
import com.msf.project.sales1crm.fragment.LocationFragment;
import com.msf.project.sales1crm.fragment.UserDialogFragment;
import com.msf.project.sales1crm.imageloader.ImageCompression;
import com.msf.project.sales1crm.model.IndustryModel;
import com.msf.project.sales1crm.model.LeadConvertModel;
import com.msf.project.sales1crm.model.LeadModel;
import com.msf.project.sales1crm.model.LeadSourceModel;
import com.msf.project.sales1crm.model.LeadStatusModel;
import com.msf.project.sales1crm.model.PositionModel;
import com.msf.project.sales1crm.presenter.LeadAddPresenter;
import com.msf.project.sales1crm.presenter.LeadConvertPresenter;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.PreferenceUtility;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by christianmacbook on 31/05/18.
 */

public class LeadAddActivity extends BaseActivity implements OnMapReadyCallback, LeadAddCallback, ConvertCallback,
        LocationEngineListener, PermissionsListener, MapboxMap.OnMapClickListener{

    @BindView(R.id.ivBack)
    ImageView ivBack;

    @BindView(R.id.ivSave)
    ImageView ivSave;

    @BindView(R.id.tvTitle)
    CustomTextView tvTitle;

    @BindView(R.id.tvAssignTo)
    CustomEditText tvAssignTo;

    @BindView(R.id.ivLeadPic)
    ImageView ivLeadPic;

    @BindView(R.id.spTitle)
    Spinner spTitle;


    @BindView(R.id.etFirstName)
    CustomEditText etFirstName;

    @BindView(R.id.etLastName)
    CustomEditText etLastName;

    @BindView(R.id.spPosition)
    Spinner spPosition;


    @BindView(R.id.etPhone)
    CustomEditText etPhone;

    @BindView(R.id.etEmail)
    CustomEditText etEmail;

    @BindView(R.id.etAddress)
    CustomEditText etAddress;

    @BindView(R.id.tvProvinsi)
    CustomTextView tvProvinsi;

    @BindView(R.id.tvKabupaten)
    CustomTextView tvKabupaten;

    @BindView(R.id.tvKecamatan)
    CustomTextView tvKecamatan;

    @BindView(R.id.tvKelurahan)
    CustomTextView tvKelurahan;

    @BindView(R.id.tvKodepos)
    CustomTextView tvKodepos;

    @BindView(R.id.etCompany)
    CustomEditText etCompany;

    @BindView(R.id.spIndustry)
    Spinner spIndustry;

    @BindView(R.id.etTotalEmployee)
    CustomEditText etTotalEmployee;

    @BindView(R.id.spStatus)
    Spinner spStatus;

    @BindView(R.id.spSource)
    Spinner spSource;

    @BindView(R.id.etDesc)
    CustomEditText etDesc;
    @BindView(R.id.tvLatitude)
    CustomTextView tvLatitude;
    @BindView(R.id.tvLongitude)
    CustomTextView tvLongitude;
//    @BindView(R.id.tilLastName)
//    TextInputLayout tilLastName;
//    @BindView(R.id.tilAddress)
//    TextInputLayout tilAddress;

    private Context context;

    private LocationFragment locationFragment;
    private String provinsiID = "0", kabupatenID = "0", kecamatanID = "0", kelurahanID = "0", assign_id = "0";
    private LeadAddPresenter presenter;
    private boolean from_detail = false;

    private LeadModel model;
    private List<IndustryModel> industryList = new ArrayList<>();
    private List<PositionModel> positionList = new ArrayList<>();
    private List<LeadStatusModel> leadStatusList = new ArrayList<>();
    private List<LeadSourceModel> leadSourceList = new ArrayList<>();
    private IndustryModel industryModel;
    private PositionModel positionModel;
    private LeadStatusModel leadStatusModel;
    private LeadSourceModel leadSourceModel;


    private MapboxMap mMapboxMap;
    private MapView mapView;
    private Marker markerView;
    private LocationEngine locationEngine;
    private LocationLayerPlugin locationLayerPlugin;
    private PermissionsManager permissionsManager;
    private LeadConvertPresenter leadConvertPresenter;
    private Location originLocation;

    private SpinnerCustomAdapter spIndustryAdapter, spPositionAdapter, spStatusAdapter, spSourceAdapter, spTitleAdapter;
    private ArrayList<String> arrIndustry = new ArrayList<>();
    private ArrayList<String> arrPosition = new ArrayList<>();
    private ArrayList<String> arrStatus = new ArrayList<>();
    private ArrayList<String> arrSource = new ArrayList<>();
    private int indexIndustry = 0, indexPosition = 0, indexStatus = 0, indexSource = 0, indexTitle = 0;
    private String kecamatan, kabupaten, provinsi, kelurahan, alamat, jalan, no, latitude, longitude, postalCode;

    private String[] title = {"Select Title", "Mr.", "Ms.", "Mrs.", "Dr.",
            "Prof."};

    //Image Utils
    private String photoPath = "", filePath = "", myBase64Image = "", url = "";
    private Bitmap savedBitmap;

    static LeadAddActivity leadAddActivity;

    public static LeadAddActivity getInstance() {
        return leadAddActivity;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Sales1CRMUtils.GALLERY:
                if (resultCode == RESULT_OK) {
                    Uri selectedImageUri = data.getData();
                    BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
                    bmpFactoryOptions.inJustDecodeBounds = false;
                    bmpFactoryOptions.inSampleSize = 4;

                    // imageFilePath image path which you pass with intent
                    Bitmap bmp = null;
                    try {
                        bmp = Sales1CRMUtils.Utils.scaleImage(context, selectedImageUri);
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    savedBitmap = bmp.copy(bmp.getConfig(), bmp.isMutable() ? true
                            : false);
                    ivLeadPic.setImageBitmap(Sales1CRMUtils.Utils.getroundBitmap(bmp));

                    myBase64Image = Sales1CRMUtils.ImageUtility.encodeToBase64(bmp, Bitmap.CompressFormat.JPEG, 50);

                    bmp.recycle();
                    bmp = null;
                }
                break;
            case Sales1CRMUtils.CAMERA_REQUEST:
                if (resultCode == RESULT_OK) {
                    BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
                    bmpFactoryOptions.inJustDecodeBounds = false;
                    bmpFactoryOptions.inSampleSize = 4;

                    Log.d("TAG", "Photo Path : " + photoPath);

                    // imageFilePath image path which you pass withintent
                    Bitmap bmp = BitmapFactory.decodeFile(photoPath,
                            bmpFactoryOptions);
                    // BitmapFactory.Options options = new BitmapFactory.Options();
                    // options.inSampleSize = 4;
                    Log.i("HUAI", "data : " + bmp);
                    // Bitmap photo = (Bitmap) data.getExtras().get("data");
                    if (bmp.getWidth() >= bmp.getHeight()) {

                        bmp = Bitmap.createBitmap(bmp,
                                bmp.getWidth() / 2 - bmp.getHeight() / 2, 0,
                                bmp.getHeight(), bmp.getHeight(), null, true);

                    } else {

                        bmp = Bitmap.createBitmap(bmp, 0,
                                bmp.getHeight() / 2 - bmp.getWidth() / 2,
                                bmp.getWidth(), bmp.getWidth(), null, true);
                    }
                    bmp = Sales1CRMUtils.ImageUtility.scaleDownBitmap(bmp);
                    ExifInterface ei;
                    int orientation = 0;
                    try {
                        ei = new ExifInterface(photoPath);
                        orientation = ei.getAttributeInt(
                                ExifInterface.TAG_ORIENTATION,
                                ExifInterface.ORIENTATION_NORMAL);

                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                    switch (orientation) {
                        case ExifInterface.ORIENTATION_ROTATE_90:
                            bmp = Sales1CRMUtils.ImageUtility.RotateBitmap(bmp, 90);
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_180:
                            bmp = Sales1CRMUtils.ImageUtility.RotateBitmap(bmp, 180);
                            break;
                        // etc.
                    }
                    savedBitmap = bmp.copy(bmp.getConfig(), bmp.isMutable() ? true
                            : false);
                    ivLeadPic.setImageBitmap(Sales1CRMUtils.Utils.getrectBitmap(savedBitmap));
                    bmp.recycle();
                    bmp = null;

                    onCaptureImageResult(data);
                }
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case Sales1CRMUtils.KEY_CAMERA_PERMISION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    cameraIntent();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(context, "Doesn't have permission... ", Toast.LENGTH_SHORT).show();
                }
                break;
        }

        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leadadd);

//        Mapbox.getInstance(this, getString(R.string.key_access_token_production));
//        mapView = findViewById(R.id.mapView);
//        mapView.setStyleUrl(getResources().getString(R.string.layer_streets) + "?auth=" + getResources().getString(R.string.key_access_token_production));
//        mapView.getMapAsync(this);

        ButterKnife.bind(this);
        this.context = this;

        leadAddActivity = this;

        this.presenter = new LeadAddPresenter(context, this);
        this.leadConvertPresenter = new LeadConvertPresenter(context, (ConvertCallback) this);
        this.url = PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.URL);

        initView();
    }

    private void initView() {
        try {
            model = getIntent().getExtras().getParcelable("leadModel");
        } catch (Exception e) {
        }

        try {
            from_detail = getIntent().getExtras().getBoolean("from_detail");
        } catch (Exception e) {
        }

        tvAssignTo.setText(PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.KEY_FULLNAME));
        assign_id = PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.KEY_USERID);

        getDataSpinner();

        getDataIntent();

        getDataFromDetail();

        tvAssignTo.setOnClickListener(click);
        ivLeadPic.setOnClickListener(click);
        tvProvinsi.setOnClickListener(click);
        tvKabupaten.setOnClickListener(click);
        tvKecamatan.setOnClickListener(click);
        tvKelurahan.setOnClickListener(click);
        ivBack.setOnClickListener(click);
        ivSave.setOnClickListener(click);
    }

    private void getDataFromDetail() {
        if (from_detail) {
            tvTitle.setText("Edit Lead");

            Sales1CRMUtils.Utils.imageLoader.DisplayImage(url + model.getImage_url(),
                    ivLeadPic, Bitmap.Config.ARGB_8888, 150, Sales1CRMUtils.TYPE_ROUND);


            assign_id = model.getAssign_to();

            spTitle.setSelection(Integer.parseInt(model.getTitle_id()));
            spIndustry.setSelection(indexIndustry);
            spPosition.setSelection(indexPosition);
            spStatus.setSelection(indexStatus);
            spSource.setSelection(indexSource);

            tvAssignTo.setText(model.getAssign_to_name());
            etFirstName.setText(model.getFirst_name());
            etLastName.setText(model.getLast_name());
            etCompany.setText(model.getCompany());
            etDesc.setText(model.getDescription());
            etTotalEmployee.setText(model.getTotal_employee());
            etEmail.setText(model.getEmail());
            etPhone.setText(model.getPhone());
            etDesc.setText(model.getDescription());
            etAddress.setText(model.getAddress());
            tvProvinsi.setText(model.getProvinsi_name());
            tvKabupaten.setText(model.getKabupaten_name());
            tvKecamatan.setText(model.getKecamatan_name());
            tvKelurahan.setText(model.getKelurahan_name());
            tvKodepos.setText(model.getKodepos());
            tvLatitude.setText(model.getLatitude());
            tvLongitude.setText(model.getLongitude());
//            mapView.setVisibility(View.VISIBLE);

            provinsiID = model.getProvinsi_id();
            kabupatenID = model.getKabupaten_id();
            kecamatanID = model.getKecamatan_id();
            kelurahanID = model.getKelurahan_id();



//            mapView = findViewById(R.id.mapView);
//            mapView.setStyleUrl(getResources().getString(R.string.layer_streets) + "?auth=" + getResources().getString(R.string.key_access_token_development));
//            mapView.getMapAsync((MapboxMap mapboxmap) -> {
//                mapboxmap.setMaxZoomPreference(18);
//                mapboxmap.addMarker(new MarkerOptions()
//                        .position(new LatLng(Double.parseDouble(model.getLatitude()),
//                                Double.parseDouble(model.getLongitude()))));
//
//                LatLng latLng = new LatLng(Double.parseDouble(model.getLatitude()),
//                        Double.parseDouble(model.getLongitude()));
//
//                mapboxmap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng,20));
//            });

        }
    }

    private void getDataIntent() {
        Intent data = getIntent();
        if (data != null) {
            alamat = data.getStringExtra("alamat");
            jalan = data.getStringExtra("jalan");
            no = data.getStringExtra("no");
            provinsi = data.getStringExtra("provinsi");
            kabupaten = data.getStringExtra("kabupaten");
            kecamatan = data.getStringExtra("kecamatan");
            kelurahan = data.getStringExtra("kelurahan");
            postalCode = data.getStringExtra("postalCode");
            latitude = data.getStringExtra("latitude");
            longitude = data.getStringExtra("longitude");

//            etAddress.setText(jalan + " " + no);
            tvProvinsi.setText(provinsi);
            tvKabupaten.setText(kabupaten);
            tvKecamatan.setText(kecamatan);
            tvKelurahan.setText(kelurahan);
            tvKodepos.setText(postalCode);
            tvLatitude.setText(latitude);
            tvLongitude.setText(longitude);

        }
    }

    private void getDataSpinner() {
        // industry
        industryList = new IndustryDao(context).getIndustryList();

        if (from_detail && !model.getIndustry_id().equalsIgnoreCase("0")) {
            for (int i = 0; i < industryList.size(); i++) {
                if (i == 0) {
                    arrIndustry.add("Select Status");
                }

                if (model.getIndustry_id().equalsIgnoreCase(industryList.get(i).getId()))
                    indexIndustry = i + 1;
                arrIndustry.add(industryList.get(i).getName());
            }
        } else {
            for (int i = 0; i < industryList.size(); i++) {
                if (i == 0) {
                    arrIndustry.add("Select Industry");
                }
                arrIndustry.add(industryList.get(i).getName());
            }
        }
        String[] industry = new String[arrIndustry.size()];
        industry = arrIndustry.toArray(industry);


        spIndustryAdapter = new SpinnerCustomAdapter(context,
                R.layout.spinner_default, industry);
        spIndustryAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spIndustry.setAdapter(spIndustryAdapter);
        spIndustry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                if (position != 0) {
                    indexIndustry = position;
                    industryModel = industryList.get(position - 1);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Position
        positionList = new PositionDao(context).getPositionList();

        if (from_detail && !model.getPosition_id().equalsIgnoreCase("0")) {
            for (int i = 0; i < positionList.size(); i++) {
                if (i == 0) {
                    arrPosition.add("Select Position");
                }

                if (model.getPosition_id().equalsIgnoreCase(positionList.get(i).getId()))
                    indexPosition = i + 1;
                arrPosition.add(positionList.get(i).getName());
            }
        } else {
            for (int i = 0; i < positionList.size(); i++) {
                if (i == 0) {
                    arrPosition.add("Select Position");
                }
                arrPosition.add(positionList.get(i).getName());
            }
        }
        String[] position = new String[arrPosition.size()];
        position = arrPosition.toArray(position);


        spPositionAdapter = new SpinnerCustomAdapter(context,
                R.layout.spinner_default, position);
        spPositionAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spPosition.setAdapter(spPositionAdapter);
        spPosition.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                if (position != 0) {
                    indexPosition = position;
                    positionModel = positionList.get(position - 1);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Lead Status
        leadStatusList = new LeadStatusDao(context).getStatusList();

        if (from_detail && !model.getLead_status_id().equalsIgnoreCase("0")) {
            for (int i = 0; i < leadStatusList.size(); i++) {
                if (i == 0) {
                    arrStatus.add("Plih Status");
                }

                if (model.getLead_status_id().equalsIgnoreCase(leadStatusList.get(i).getId()))
                    indexStatus = i + 1;
                arrStatus.add(leadStatusList.get(i).getName());
            }
        } else {
            for (int i = 0; i < leadStatusList.size(); i++) {
                if (i == 0) {
                    arrStatus.add("Select Status");
                }
                arrStatus.add(leadStatusList.get(i).getName());
            }
        }
        String[] status = new String[arrStatus.size()];
        status = arrStatus.toArray(status);


        spStatusAdapter = new SpinnerCustomAdapter(context,
                R.layout.spinner_default, status);
        spStatusAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spStatus.setAdapter(spStatusAdapter);
        spStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                if (position != 0) {
                    indexStatus = position;
                    leadStatusModel = leadStatusList.get(position - 1);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Lead Source
        leadSourceList = new LeadSourceDao(context).getSourceList();

        if (from_detail && !model.getLead_source_id().equalsIgnoreCase("0")) {
            for (int i = 0; i < leadSourceList.size(); i++) {
                if (i == 0) {
                    arrSource.add("Select Lead Source");
                }

                if (model.getLead_source_id().equalsIgnoreCase(leadSourceList.get(i).getId()))
                    indexSource = i + 1;
                arrSource.add(leadSourceList.get(i).getName());
            }
        } else {
            for (int i = 0; i < leadSourceList.size(); i++) {
                if (i == 0) {
                    arrSource.add("Select Lead Source");
                }
                arrSource.add(leadSourceList.get(i).getName());
            }
        }
        String[] source = new String[arrSource.size()];
        source = arrSource.toArray(source);


        spSourceAdapter = new SpinnerCustomAdapter(context,
                R.layout.spinner_default, source);
        spSourceAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spSource.setAdapter(spSourceAdapter);
        spSource.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                if (position != 0) {
                    indexSource = position;
                    leadSourceModel = leadSourceList.get(position - 1);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //Title
        spTitleAdapter = new SpinnerCustomAdapter(context,
                R.layout.spinner_default, title);
        spTitleAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spTitle.setAdapter(spTitleAdapter);
        spTitle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                if (position != 0) {
                    indexTitle = position;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.tvAssignTo:
                    UserDialogFragment userFragment = new UserDialogFragment(LeadAddActivity.this, new UserDialogCallback() {

                        @Override
                        public void onUserDialogCallback(Bundle data) {
                            tvAssignTo.setText(data.getString("name"));
                            assign_id = data.getString("id");
                        }
                    });
                    userFragment.show(getSupportFragmentManager(), "UserFragment");
                    break;
                case R.id.ivLeadPic:
                    if (from_detail) {
                            selectImage();
                    } else {
                            selectImage();
                    }

                    break;
                case R.id.ivBack:
                    final CustomDialog dialogback = CustomDialog.setupDialogConfirmation(context, "Anda ingin kembali? Data yang anda masukkan akan hilang?");
                    CustomTextView tvCancel = (CustomTextView) dialogback.findViewById(R.id.tvCancel);
                    CustomTextView tvYa = (CustomTextView) dialogback.findViewById(R.id.tvOK);

                    tvCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialogback.dismiss();
                        }
                    });

                    tvYa.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            finish();
                            dialogback.dismiss();
                        }
                    });
                    dialogback.show();
                    break;
                case R.id.ivSave:
                    if (checkField()) {
                        if (!from_detail) {
                            //Input Into Model
                            model = new LeadModel();
                            model.setTitle_id(indexTitle + "");
                            model.setFirst_name(etFirstName.getText().toString());
                            model.setLast_name(etLastName.getText().toString());
                            model.setCompany(etCompany.getText().toString());
                            model.setPhone(etPhone.getText().toString());
                            model.setEmail(etEmail.getText().toString());
                            model.setTotal_employee(etTotalEmployee.getText().toString());
                            model.setDescription(etDesc.getText().toString());
                            model.setBase64Image(myBase64Image);
                            model.setCreate_date(getCurrentDate());
                            model.setAssign_to(assign_id);

                            model.setAddress(etAddress.getText().toString());
                            model.setLatitude(tvLatitude.getText().toString());
                            model.setLongitude(tvLongitude.getText().toString());
                            model.setProvinsi_id(provinsiID);
                            model.setKabupaten_id(kabupatenID);
                            model.setKecamatan_id(kecamatanID);
                            model.setKelurahan_id(kelurahanID);


//                            mapView = findViewById(R.id.mapView);
//                            mapView.setStyleUrl(getResources().getString(R.string.layer_streets) + "?auth=" + getResources().getString(R.string.key_access_token_production));



                            if (indexIndustry != 0) {
                                model.setIndustry_id(industryModel.getId());
                                model.setIndustry_name(industryModel.getName());
                            }

                            if (indexPosition != 0) {
                                model.setPosition_id(positionModel.getId());
                                model.setPosition_name(positionModel.getName());
                            }

                            if (indexStatus != 0) {
                                model.setLead_status_id(leadStatusModel.getId());
                                model.setLead_status_name(leadStatusModel.getName());
                            }

                            if (indexSource != 0) {
                                model.setLead_source_id(leadSourceModel.getId());
                                model.setLead_source_name(leadSourceModel.getName());
                            }

                            //Enable Error
                            etFirstName.setError(null);
                            etPhone.setError(null);

                            showLoadingDialog();
                            presenter.setupAdd(ApiParam.API_031, model);
                        } else {
                            //Input Into Model
                            model.setTitle_id(indexTitle + "");
                            model.setFirst_name(etFirstName.getText().toString());
                            model.setLast_name(etLastName.getText().toString());
                            model.setCompany(etCompany.getText().toString());
                            model.setPhone(etPhone.getText().toString());
                            model.setEmail(etEmail.getText().toString());
                            model.setTotal_employee(etTotalEmployee.getText().toString());
                            model.setDescription(etDesc.getText().toString());
                            model.setBase64Image(myBase64Image);
                            model.setCreate_date(getCurrentDate());
                            model.setAssign_to(assign_id);

                            model.setAddress(etAddress.getText().toString());
                            model.setLongitude(tvLongitude.getText().toString());
                            model.setLatitude(tvLatitude.getText().toString());
                            model.setProvinsi_id(provinsiID);
                            model.setKabupaten_id(kabupatenID);
                            model.setKecamatan_id(kecamatanID);
                            model.setKelurahan_id(kelurahanID);

//                            mapView = findViewById(R.id.mapView);
//                            mapView.setStyleUrl(getResources().getString(R.string.layer_streets) + "?auth=" + getResources().getString(R.string.key_access_token_production));


                            if (indexIndustry != 0) {
                                model.setIndustry_id(industryModel.getId());
                                model.setIndustry_name(industryModel.getName());
                            }

                            if (indexPosition != 0) {
                                model.setPosition_id(positionModel.getId());
                                model.setPosition_name(positionModel.getName());
                            }

                            if (indexStatus != 0) {
                                model.setLead_status_id(leadStatusModel.getId());
                                model.setLead_status_name(leadStatusModel.getName());
                            }

                            if (indexSource != 0) {
                                model.setLead_source_id(leadSourceModel.getId());
                                model.setLead_source_name(leadSourceModel.getName());
                            }

                            //Enable Error
                            etFirstName.setError(null);
                            etPhone.setError(null);

                            showLoadingDialog();
                            presenter.setupEdit(ApiParam.API_032, model);
                        }
                    }
                    break;
                case R.id.tvProvinsi:
                    locationFragment = new LocationFragment(LeadAddActivity.this, "provinsi", "0", new LocationDialogCallback() {

                        @Override
                        public void onLocationDialogCallback(Bundle data) {
                            tvProvinsi.setText(data.getString("name"));
                            provinsiID = data.getString("id");

                            tvKabupaten.setText("Kabupaten");
                            tvKecamatan.setText("Kecamatan");
                            tvKelurahan.setText("Kelurahan");
                            tvKodepos.setText("Kodepos");

                            kabupatenID = "0";
                            kecamatanID = "0";
                            kelurahanID = "0";
                        }
                    });
                    locationFragment.show(getSupportFragmentManager(), "locationDialog");
                    break;
                case R.id.tvKabupaten:
                    if (!provinsiID.equalsIgnoreCase("0")) {
                        locationFragment = new LocationFragment(LeadAddActivity.this, "kabupaten", provinsiID, new LocationDialogCallback() {

                            @Override
                            public void onLocationDialogCallback(Bundle data) {
                                tvKabupaten.setText(data.getString("name"));
                                kabupatenID = data.getString("id");

                                tvKecamatan.setText("Kecamatan");
                                tvKelurahan.setText("Kelurahan");
                                tvKodepos.setText("Kodepos");

                                kecamatanID = "0";
                                kelurahanID = "0";
                            }
                        });
                        locationFragment.show(getSupportFragmentManager(), "locationDialog");
                    }
                    break;
                case R.id.tvKecamatan:
                    if (!kabupatenID.equalsIgnoreCase("0")) {
                        locationFragment = new LocationFragment(LeadAddActivity.this, "kecamatan", kabupatenID, new LocationDialogCallback() {

                            @Override
                            public void onLocationDialogCallback(Bundle data) {
                                tvKecamatan.setText(data.getString("name"));
                                kecamatanID = data.getString("id");

                                tvKelurahan.setText("Kelurahan");
                                tvKodepos.setText("Kodepos");

                                kelurahanID = "0";
                            }
                        });
                        locationFragment.show(getSupportFragmentManager(), "locationDialog");
                    }
                    break;
                case R.id.tvKelurahan:
                    if (!kecamatanID.equalsIgnoreCase("0")) {
                        locationFragment = new LocationFragment(LeadAddActivity.this, "kelurahan", kecamatanID, new LocationDialogCallback() {

                            @Override
                            public void onLocationDialogCallback(Bundle data) {
                                tvKelurahan.setText(data.getString("name"));
                                tvKodepos.setText(data.getString("kodepos"));
                                kelurahanID = data.getString("id");
                            }
                        });
                        locationFragment.show(getSupportFragmentManager(), "locationDialog");
                    }
                    break;
                default:
                    break;
            }
        }
    };

    private void selectImage() {
        final CharSequence[] items = {"Ambil Foto", "Galery", "Batal"};

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Tambah Foto!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals("Ambil Foto")) {

                    if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.CAMERA}, Sales1CRMUtils.KEY_CAMERA_PERMISION);
                    } else {
                        cameraIntent();
                    }
                } else if (items[item].equals("Galery")) {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(
                            Intent.createChooser(intent, "Select Picture"),
                            Sales1CRMUtils.GALLERY);
                    dialog.dismiss();
                } else if (items[item].equals("Batal")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void cameraIntent() {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        Intent intentCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File photoFile = null;
        try {
            photoFile = createImageFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (photoFile != null) {
            Uri photoURI = Uri.fromFile(photoFile);
            intentCamera.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
            startActivityForResult(intentCamera, Sales1CRMUtils.CAMERA_REQUEST);
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            String Name = new SimpleDateFormat("yyyyMMdd_HHmm").format(new Date());
            String imageFileName = Name + ".jpg";
            File storageDir = new File(Environment.getExternalStorageDirectory().getPath(), imageFileName);
            photoPath = storageDir.getPath();
            return storageDir;
        } else {
            String imageFileName = new SimpleDateFormat("yyyyMMdd_HHmm").format(new Date());
            File storageDir = context.getExternalFilesDir("Pictures");
            File image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );
            Log.d("TAG", image.getPath());
            photoPath = image.getPath();
            return image;
        }
    }

    private void onCaptureImageResult(Intent data) {
        //Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        Log.d("TAG", "Masuk OnCaptureImage");
        try {
            ImageCompression imageCompression = new ImageCompression(context);
            filePath = imageCompression.compressImage(photoPath);
            Log.d("PPP", "Path : " + filePath);
            Bitmap thumbnail = Sales1CRMUtils.ImageUtility.getThumbnail(filePath);

            myBase64Image = Sales1CRMUtils.ImageUtility.encodeToBase64(thumbnail, Bitmap.CompressFormat.JPEG, 50);

            File imgFile = new File(photoPath);
            imgFile.delete();
            photoPath = "";
        } catch (IOException e) {
            e.printStackTrace();
            Log.d("TAG", e.getMessage());
        }
    }

    @Override
    public void finishAdd(String result, String response) {
        dismissLoadingDialog();
        Log.d("TAG","result : " + result);
        Log.d("TAG","response : " + response);
        if (result.equalsIgnoreCase("OK")) {
            if(leadStatusModel.getId().equalsIgnoreCase("3")){
                Log.d("TAG","indexStatus  :" + indexStatus);
                Log.d("TAG","leadstatusId  : " + leadStatusModel.getId());
                Log.d("TAG","leadstatusName  : " + leadStatusModel.getName());
                final CustomDialog  dialogConvert = CustomDialog.setupDialogConvert(context);
                RadioGroup radioGroup = (RadioGroup) dialogConvert.findViewById(R.id.radioGroup);
                RadioButton rb_1 = (RadioButton) dialogConvert.findViewById(R.id.rb_1);
                RadioButton rb_2 = (RadioButton) dialogConvert.findViewById(R.id.rb_2);
                CustomTextView btnCancel = (CustomTextView) dialogConvert.findViewById(R.id.tvCancel);
                CustomTextView btnSave = (CustomTextView) dialogConvert.findViewById(R.id.tvSave);

                btnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogConvert.dismiss();
                    }
                });

                btnSave.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(rb_1.isChecked()) {
                            leadConvertPresenter.setupConvert(ApiParam.API_127,response,"1");
                            dialogConvert.dismiss();
                            finish();
                        } else {
                            leadConvertPresenter.setupConvert(ApiParam.API_127, response,"2");
                            dialogConvert.dismiss();
                        }
                    }
                });
                dialogConvert.show();
            } else {
                finish();
            }
        } else {

        }
    }

    @Override
    public void finishEdit(String result, String response) {
        dismissLoadingDialog();
        if (result.equalsIgnoreCase("OK")) {
            if(leadStatusModel.getId().equalsIgnoreCase("3")) {
                Log.d("TAG", "result : " + result);
                Log.d("TAG", "response : " + response);
                final CustomDialog dialogConvert = CustomDialog.setupDialogConvert(context);
                RadioGroup radioGroup = (RadioGroup) dialogConvert.findViewById(R.id.radioGroup);
                RadioButton rb_1 = (RadioButton) dialogConvert.findViewById(R.id.rb_1);
                RadioButton rb_2 = (RadioButton) dialogConvert.findViewById(R.id.rb_2);
                CustomTextView btnCancel = (CustomTextView) dialogConvert.findViewById(R.id.tvCancel);
                CustomTextView btnSave = (CustomTextView) dialogConvert.findViewById(R.id.tvSave);

                btnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogConvert.dismiss();
                        finish();
                    }
                });

                btnSave.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (rb_1.isChecked()) {
                            leadConvertPresenter.setupConvert(ApiParam.API_127, model.getId(),"1");
                            dialogConvert.dismiss();
                        } else {
                            leadConvertPresenter.setupConvert(ApiParam.API_127, model.getId(),"2");
                            dialogConvert.dismiss();
                        }
                    }
                });
                dialogConvert.show();
            } else {
                finish();
            LeadDetailActivity.getInstance().finish();
            }
        } else {

        }
    }

    private boolean checkField() {
        if (etFirstName.getText().toString().equalsIgnoreCase("")) {
            etFirstName.setError("Nama Depan wajib diisi");
            return false;
        } else if (etPhone.getText().toString().equalsIgnoreCase("")) {
            etPhone.setError("Telepon wajib diisi");
            return false;
        }
        return true;
    }

    private String getCurrentDate() {
        Calendar c = Calendar.getInstance();

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        return df.format(c.getTime());
    }

    @Override
    public void onConnected() {
        locationEngine.requestLocationUpdates();

    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            originLocation = location;

        }

    }

    @SuppressWarnings("MissingPermission")
    private void initializedLocationEngine() {
        locationEngine = new LocationEngineProvider(this).obtainBestLocationEngineAvailable();
        locationEngine.setPriority(LocationEnginePriority.HIGH_ACCURACY);
        locationEngine.activate();

        Location lastLocation = locationEngine.getLastLocation();
        if (lastLocation != null) {
            originLocation = lastLocation;
//            setCameraPosition(lastLocation);
        } else {
            locationEngine.addLocationEngineListener(this);
        }
    }

    @SuppressWarnings("MissingPermission")
   private void intializedLocationLayer(){
        locationLayerPlugin = new LocationLayerPlugin(mapView,mMapboxMap,locationEngine);
        locationLayerPlugin.setLocationLayerEnabled(true);
        locationLayerPlugin.setCameraMode(CameraMode.TRACKING);
        locationLayerPlugin.setRenderMode(RenderMode.NORMAL);
   }

    private void enableLocation() {
        if (PermissionsManager.areLocationPermissionsGranted(this)) {
            initializedLocationEngine();
            intializedLocationLayer();
        } else {
            permissionsManager = new PermissionsManager(this);
            permissionsManager.requestLocationPermissions(this);
        }
    }

    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {

    }

    @Override
    public void onPermissionResult(boolean granted) {
        if (granted) {
            enableLocation();
        }

    }



    @Override
    public void onMapClick(@NonNull LatLng point) {

        if (markerView != null) {
            mMapboxMap.removeMarker(markerView);
        }

        IconFactory iconFactory = IconFactory.getInstance(context);
        Icon icon = iconFactory.fromResource(R.drawable.mapicon);
        markerView = mMapboxMap.addMarker(new MarkerOptions()
                .position(point)
                .icon(icon));


        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(point.getLatitude(), point.getLongitude(), 1);
            if (addresses != null && addresses.size() > 0) {
                alamat = addresses.get(0).getAddressLine(0);
                jalan = addresses.get(0).getThoroughfare();
                no = addresses.get(0).getSubThoroughfare();
                kecamatan = addresses.get(0).getLocality();
                String country = addresses.get(0).getCountryName();
                postalCode = addresses.get(0).getPostalCode();
                String knowName = addresses.get(0).getFeatureName();
                kelurahan = addresses.get(0).getSubLocality();
                provinsi = addresses.get(0).getAdminArea();
                kabupaten = addresses.get(0).getSubAdminArea();
                latitude = String.valueOf(addresses.get(0).getLatitude());
                longitude = String.valueOf(addresses.get(0).getLongitude());


//                etAddress.setText(alamat);
                tvLatitude.setText(latitude);
                tvLongitude.setText(longitude);

            }

        } catch (IOException e) {
            e.printStackTrace();
        }



    }

    @Override
    public void onMapReady(MapboxMap mapboxMap) {
        mMapboxMap = mapboxMap;
        mMapboxMap.addOnMapClickListener(this);

    }

    @Override
    protected void onStart() {
        super.onStart();
//        mapView.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
//        mapView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
//        mapView.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
//        mapView.onStop();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
//        mapView.onLowMemory();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        mapView.onLowMemory();
    }

    @Override
    public void finishConvert(String result, LeadConvertModel model) {
        if(result.equalsIgnoreCase("OK")){
            if(model.getConvert_status().equalsIgnoreCase("1")){
                finish();
                LeadAddActivity.getInstance().finish();
            }else {
                Log.d("TAG","CONVERT_STATUS : " + model.getConvert_status());
                finish();
                Intent intent = new Intent(context, OpportunityAddActivity.class);

                intent.putExtra("account_id", model.getAccount_id());
                intent.putExtra("account_name", model.getAccount_name());
                intent.putExtra("contact", model.getContact());
                intent.putExtra("lead_id", model.getLead_id());
                intent.putExtra("convert",true);
                Log.d("TAG","ID " + model.getAccount_id());
                Log.d("TAG","NAMA " + model.getAccount_name());
                startActivity(intent);
            }
        }


    }
}
