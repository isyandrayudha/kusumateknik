package com.msf.project.sales1crm.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.callback.SalesCycleCallback;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.customview.SpinnerCustomAdapter;
import com.msf.project.sales1crm.model.SalesCycleModel;
import com.msf.project.sales1crm.model.TotalModel;
import com.msf.project.sales1crm.presenter.ViewReportSalesCyclePresenter;
import com.msf.project.sales1crm.service.GPSTracker;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ViewReportSalesCycleFragment extends Fragment implements SalesCycleCallback {

    @BindView(R.id.spMonth1)
    Spinner spMonth1;

    @BindView(R.id.spYear1)
    Spinner spYear1;

    @BindView(R.id.tvTotalLeads)
    CustomTextView tvTotalLeads;

    @BindView(R.id.tvTotalOppor)
    CustomTextView tvTotalOppor;

    @BindView(R.id.tvCloseWon)
    CustomTextView tvCloseWon;

    @BindView(R.id.tvCloseLose)
    CustomTextView tvCloseLose;

    @BindView(R.id.tvTotalSales)
    CustomTextView tvTotalSales;

    @BindView(R.id.tvTotalAvgSales)
    CustomTextView tvTotalAvgSales;

    @BindView(R.id.tvSubmit)
    CustomTextView tvSubmit;

    private View view;
    private Context context;
    private ViewReportSalesCyclePresenter presenter;
    private SpinnerCustomAdapter spinnerCustomAdapter;
    private List<TotalModel> list = new ArrayList<>();
    private List<TotalModel> list1 = new ArrayList<>();
    private String[] months = {"Choose Month", "January", "February", "March", "April", "May", "Juni", "July", "August", "September", "October", "November", "Desember"};
    private String[] years = {"Choose Year", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019"};
    private String month_1="", year_1="";

    private DecimalFormat df = (DecimalFormat) DecimalFormat.getCurrencyInstance();
    private DecimalFormatSymbols dfs = new DecimalFormatSymbols();

    public static ViewReportTotalCallsFragment newInstance() {
        ViewReportTotalCallsFragment fragment = new ViewReportTotalCallsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_salescycle, container, false);

        return this.view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);
        this.context = getActivity();

        this.presenter = new ViewReportSalesCyclePresenter(context, this);

        initData();
    }

    private void initData(){
        spinnerCustomAdapter = new SpinnerCustomAdapter(context,
                R.layout.spinner_default, years);
        spinnerCustomAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spYear1.setAdapter(spinnerCustomAdapter);
        spYear1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                if (position != 0){
                    year_1 = years[position];
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerCustomAdapter = new SpinnerCustomAdapter(context,
                R.layout.spinner_default, months);
        spinnerCustomAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spMonth1.setAdapter(spinnerCustomAdapter);
        spMonth1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                if (position != 0){
                    month_1 = String.valueOf(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        tvSubmit.setOnClickListener(click);
    }

    public void getDataFromAPI(){
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(context)){
            GPSTracker gpsTracker = new GPSTracker(context);
            if (gpsTracker.canGetLocation()) {
                presenter.setupTotal(ApiParam.API_096, month_1, year_1);
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gpsTracker.showSettingsAlert();
            }
        }
    }

    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.tvSubmit:
                    getDataFromAPI();
                    break;
            }
        }
    };

    @Override
    public void finished(String result, SalesCycleModel model) {
        if (result.equalsIgnoreCase("OK")){
            df.setDecimalFormatSymbols(dfs);

            tvTotalOppor.setText(model.getTotal_oppor());
            tvTotalLeads.setText(model.getTotal_lead());
            tvCloseWon.setText(model.getTotal_closewon());
            tvCloseLose.setText(model.getTotal_closelose());
            tvTotalSales.setText(df.format(Double.parseDouble(model.getTotal_sales())));
            tvTotalAvgSales.setText(df.format(Double.parseDouble(model.getAvg_sales())));
        }
    }
}