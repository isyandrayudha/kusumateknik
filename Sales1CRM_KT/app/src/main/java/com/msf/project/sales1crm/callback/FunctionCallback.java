package com.msf.project.sales1crm.callback;

public interface FunctionCallback {
    void finishFunction(String result, String response);
}
