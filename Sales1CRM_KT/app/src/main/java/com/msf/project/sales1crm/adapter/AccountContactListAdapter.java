package com.msf.project.sales1crm.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;

import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.model.ContactModel;

import java.util.List;

/**
 * Created by christianmacbook on 16/05/18.
 */

public class AccountContactListAdapter extends BaseAdapter {
    private Context context;
    private Bitmap icon;
    private String url;
    private List<ContactModel> listData;

    public AccountContactListAdapter(Context context, List<ContactModel> objects) {
        this.context = context;
        this.listData = objects;
    }

    public static abstract class Row {
    }

    // This is for making Section, ex: A B C D etc
    public static final class Section extends Row {
        public final String text;

        public Section(String text) {
            this.text = text;
        }
    }

    // This is for making Row, ex: A B C D etc
    public static final class Item extends Row {
        public final ContactModel text;

        public Item(ContactModel text) {
            this.text = text;
        }

        public ContactModel getItem() {
            return text;
        }
    }

    private List<Row> rows;

    public void setRows(List<Row> rows) {
        this.rows = rows;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return rows.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return rows.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        if (getItem(position) instanceof Section) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public View getView(final int position, View converView, ViewGroup parent) {
        View view = converView;
        final ViewHolder holder;

        final Item item = (Item) getItem(position);
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) parent.getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = (LinearLayout) inflater.inflate(
                    R.layout.row_accountcontactlist, parent, false);
            holder = new ViewHolder();
            holder.tvContactName = (CustomTextView) view
                    .findViewById(R.id.tvContactName);
            holder.tvPosition = (CustomTextView) view
                    .findViewById(R.id.tvPosition);
            holder.tvPhone = (CustomTextView) view
                    .findViewById(R.id.tvPhone);
            holder.tvEmail = (CustomTextView) view
                    .findViewById(R.id.tvEmail);
            holder.llEmail = (LinearLayout) view
                    .findViewById(R.id.llEmail);
            
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        holder.tvContactName.setText(item.text.getFullname());
        holder.tvPosition.setText(item.text.getPosition());
        holder.tvPhone.setText(item.text.getPhone());
        if (!item.text.getEmail().equalsIgnoreCase("")){
            holder.tvEmail.setText(item.text.getEmail());
            holder.llEmail.setVisibility(View.VISIBLE);
        } else {
            holder.llEmail.setVisibility(View.GONE);
        }
        return view;
    }

    private static class ViewHolder {
        CustomTextView tvContactName, tvPosition, tvPhone, tvEmail;
        LinearLayout llEmail;
    }
}
