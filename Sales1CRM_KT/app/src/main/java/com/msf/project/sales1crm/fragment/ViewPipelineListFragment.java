package com.msf.project.sales1crm.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.msf.project.sales1crm.MenuActivity;
import com.msf.project.sales1crm.OpportunityAddActivity;
import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.adapter.OpportunityListByStageAdapter;
import com.msf.project.sales1crm.callback.OpportunityListByStageCallback;
import com.msf.project.sales1crm.callback.VpCallback;
import com.msf.project.sales1crm.customview.CustomEditText;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.imageloader.ImageLoader;
import com.msf.project.sales1crm.model.OpportunityModel;
import com.msf.project.sales1crm.model.VpModel;
import com.msf.project.sales1crm.presenter.OpportunitylListByStagePresenter;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ViewPipelineListFragment extends Fragment implements VpCallback {

    @BindView(R.id.etSearch)
    CustomEditText etSearch;
    @BindView(R.id.ivClear)
    ImageView ivClear;
    @BindView(R.id.tvRefresh)
    CustomTextView tvRefresh;
    @BindView(R.id.rlNoConnection)
    RelativeLayout rlNoConnection;
    @BindView(R.id.llNotFound)
    LinearLayout llNotFound;
    @BindView(R.id.rcVpList)
    RecyclerView rcVpList;
    @BindView(R.id.faOpportunityAdd)
    FloatingActionButton faOpportunityAdd;
    @BindView(R.id.menu)
    FloatingActionMenu menu;
    private View view;
    private Context context;
    private OpportunityListByStageAdapter adapter;
    private OpportunityListByStageCallback callback;
    private OpportunitylListByStagePresenter presenter;
    private List<OpportunityModel> list = new ArrayList<>();

    private int prevSize = 0, sort_id = 0;
    private int offset = 0;
    private boolean isLoadMore = true;
    private boolean isMaxSize = true;
    private boolean pause = false;

    private boolean isScrolling = false;
    private int currentItems, totalItems, scrollOutItems;


    public static ViewPipelineListFragment newInstance() {
        ViewPipelineListFragment fragment = new ViewPipelineListFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_viewpipeline_list, container, false);
        ButterKnife.bind(this, this.view);
        this.context = getActivity();
        return this.view;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.context = getActivity();
//        this.presenter = new OpportunitylListByStagePresenter(context, this);
//        rcTotMQL.setNestedScrollingEnabled(false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Opportunity");
        inidata();
    }




    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.faOpportunityAdd:
                        startActivity(new Intent(context, OpportunityAddActivity.class));
                    break;
                case R.id.tvRefresh:
//                    llshim.setVisibility(View.VISIBLE);
                    rlNoConnection.setVisibility(View.GONE);
                    getDataViewPipeline();
                    break;
            }
        }
    };

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_item, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_list:
                    MenuActivity.getInstance().setFragment(ViewPipelineListFragment.newInstance());
                break;
            case R.id.action_cards:
                    MenuActivity.getInstance().setFragment(ViewpipelineTabFragment.newInstance());
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void inidata() {
        faOpportunityAdd.setOnClickListener(click);
        tvRefresh.setOnClickListener(click);
        Sales1CRMUtils.Utils.imageLoader = new ImageLoader(context);
        etSearch.clearFocus();
        getDataViewPipeline();
    }


    public void getDataViewPipeline() {
        isMaxSize = true;
        isLoadMore = true;
        llNotFound.setVisibility(View.GONE);
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(context)) {

        } else {
            rlNoConnection.setVisibility(View.VISIBLE);
        }
    }


//    private void setListMQL() {
//        llshim.setVisibility(View.VISIBLE);
//        shimmerLayout.startShimmer();
//        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
//        rcTotMQL.setLayoutManager(layoutManager);
//        rcTotMQL.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
//                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
//
//                }
//            }
//
//            @Override
//            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//                super.onScrolled(recyclerView, dx, dy);
//                currentItems = layoutManager.getChildCount();
//                totalItems = layoutManager.getItemCount();
//                scrollOutItems = layoutManager.findFirstVisibleItemPosition();
//
//                if (isScrolling && (currentItems + scrollOutItems == totalItems)) {
//                    isScrolling = false;
//                    new Handler().postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            presenter.moreListMQL(ApiParam.API_111, offset, "", sort_id);
//                            adapter.notifyDataSetChanged();
//                        }
//                    }, 10000);
//
//                }
//            }
//        });
//
//        List<OpportunityListByStageAdapter.Row> rows = new ArrayList<>();
//        for (OpportunityModel mql : list) {
//            rows.add(new OpportunityListByStageAdapter.Item(mql));
//        }
//        adapter.setRows(rows);
//        rcTotMQL.setAdapter(adapter);
//        llshim.setVisibility(View.INVISIBLE);
//        shimmerLayout.stopShimmer();
//        shimmerLayout.setVisibility(View.INVISIBLE);
//    }
//
//    private void setMoreListMQL() {
//        List<OpportunityListByStageAdapter.Row> rows = new ArrayList<>();
//        for (OpportunityModel mqlmore : list) {
//            rows.add(new OpportunityListByStageAdapter.Item(mqlmore));
//        }
//        adapter.setRows(rows);
//        adapter.notifyDataSetChanged();
//    }
//
//    @Override
//    public void finishMQL(String result, List<OpportunityModel> list) {
//        if (result.equalsIgnoreCase("OK")) {
//            prevSize = list.size();
//            this.list = list;
//            Log.e("MQL", list.toString());
//
//            isMaxSize = false;
//            isLoadMore = false;
//
//            adapter = new OpportunityListByStageAdapter(context, this.list);
//            setListMQL();
//            if (list.size() > 0) {
//            } else if (result.equalsIgnoreCase("FAILDE_KEY")) {
//                startActivity(new Intent(context, ErrorAPIKeyActivity.class));
//                MenuActivity.getInstance().finish();
//            } else {
////                rlNoConnection.setVisibility(View.VISIBLE);
//            }
//        } else {
//            Log.e("MQL", "KOSONG");
//        }
//
//    }
//
//    @Override
//    public void finishMoreMQL(String result, List<OpportunityModel> list) {
//        if (result.equals("OK")) {
//            for (int i = 0; i < list.size(); i++) {
//                this.list.add(list.get(i));
//            }
//            if (this.prevSize == this.list.size())
//                isMaxSize = true;
//            this.prevSize = this.list.size();
//            setMoreListMQL();
//        }
//        this.isLoadMore = false;
//    }

//    private void setListIDM() {
//        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
//        rcIDM.setLayoutManager(layoutManager);
//        rcIDM.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
//                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
//
//                }
//            }
//
//            @Override
//            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//                super.onScrolled(recyclerView, dx, dy);
//                currentItems = layoutManager.getChildCount();
//                totalItems = layoutManager.getItemCount();
//                scrollOutItems = layoutManager.findFirstVisibleItemPosition();
//
//                if (isScrolling && (currentItems + scrollOutItems == totalItems)) {
//                    isScrolling = false;
//                    new Handler().postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            presenter.moreListIDM(ApiParam.API_111, offset, "", sort_id);
//                            adapter.notifyDataSetChanged();
//                        }
//                    }, 5000);
//
//                }
//            }
//        });
//
//        List<OpportunityListByStageAdapter.Row> rows = new ArrayList<>();
//        for (OpportunityModel idm : list) {
//            rows.add(new OpportunityListByStageAdapter.Item(idm));
//        }
//        adapter.setRows(rows);
//        rcIDM.setAdapter(adapter);
//
//    }
//
//    private void setMoreListIDM() {
//        List<OpportunityListByStageAdapter.Row> rows = new ArrayList<>();
//        for (OpportunityModel idmmore : list) {
//            rows.add(new OpportunityListByStageAdapter.Item(idmmore));
//        }
//        adapter.setRows(rows);
//        adapter.notifyDataSetChanged();
//
//    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    @Override
    public void onResume() {
        if (pause) {
//            getDataNone();
//            getDataMQL();
        }
        super.onResume();
    }

    @Override
    public void onPause() {
        pause = true;
        super.onPause();
    }

    @Override
    public void finishVp(String result, List<VpModel> list) {

    }

    @Override
    public void finishMoreVp(String result, List<VpModel> list) {

    }
}
