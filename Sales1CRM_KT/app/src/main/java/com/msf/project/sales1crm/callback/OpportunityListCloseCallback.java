package com.msf.project.sales1crm.callback;

import com.msf.project.sales1crm.model.OpportunityModel;

import java.util.List;

public interface OpportunityListCloseCallback {
    void finishOpporList(String result, List<OpportunityModel> list);
    void finishOpporMoreList(String result, List<OpportunityModel> list);
}
