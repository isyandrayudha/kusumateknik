package com.msf.project.sales1crm.customview;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.msf.project.sales1crm.AccountAddActivity;
import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.fragment.AccountFrangment;
import com.msf.project.sales1crm.model.AccountModel;
import com.msf.project.sales1crm.presenter.AccountListPresenter;
import com.msf.project.sales1crm.utility.ApiParam;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class SheetBottomDialog extends BottomSheetDialogFragment {


    @BindView(R.id.AccountAdd)
    LinearLayout AccountAdd;
    @BindView(R.id.AccountQuickAdd)
    LinearLayout AccountQuickAdd;
    @BindView(R.id.AccountList)
    LinearLayout AccountList;
    @BindView(R.id.bottomSheet)
    LinearLayout bottomSheet;
    Unbinder unbinder;

    private CustomDialog dialogQuick;
    private AccountListPresenter presenter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_bottom_sheet_dialog_account, container, false);
        unbinder = ButterKnife.bind(this, v);



        AccountAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), AccountAddActivity.class);
                startActivity(intent);
            }
        });

        AccountQuickAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogQuick = CustomDialog.setupDialogAccountAdd(getActivity());
                final CustomEditText etName = (CustomEditText) dialogQuick.findViewById(R.id.etAccountName);
                final CustomEditText etPhone = (CustomEditText) dialogQuick.findViewById(R.id.etPhone);
                CustomTextView tvYa = (CustomTextView) dialogQuick.findViewById(R.id.tvOK);
                CustomTextView tvTidak = (CustomTextView) dialogQuick.findViewById(R.id.tvBack);

                tvYa.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        AccountModel model = new AccountModel();
                        model.setAccount_name(etName.getText().toString());
                        model.setPhone(etPhone.getText().toString());

                        presenter.setupCreate(ApiParam.API_013, model);
                    }
                });

                tvTidak.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialogQuick.dismiss();
                    }
                });
                dialogQuick.show();
            }
        });

        AccountList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_layout, AccountFrangment.newInstance());
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                transaction.commit();
                dismiss();
            }
        });

        return v;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


}
