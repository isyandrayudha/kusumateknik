package com.msf.project.sales1crm.model;

public class Shipping{
	private String provinsi;
	private String latitude;
	private String kodeposName;
	private String provinsiName;
	private String type;
	private String kabupaten;
	private String kelurahan;
	private String alamat;
	private String kecamatanName;
	private String accountId;
	private String kabupatenName;
	private String kecamatan;
	private String kodepos;
	private String id;
	private String longitude;
	private String kelurahanName;

	public void setProvinsi(String provinsi){
		this.provinsi = provinsi;
	}

	public String getProvinsi(){
		return provinsi;
	}

	public void setLatitude(String latitude){
		this.latitude = latitude;
	}

	public String getLatitude(){
		return latitude;
	}

	public void setKodeposName(String kodeposName){
		this.kodeposName = kodeposName;
	}

	public String getKodeposName(){
		return kodeposName;
	}

	public void setProvinsiName(String provinsiName){
		this.provinsiName = provinsiName;
	}

	public String getProvinsiName(){
		return provinsiName;
	}

	public void setType(String type){
		this.type = type;
	}

	public String getType(){
		return type;
	}

	public void setKabupaten(String kabupaten){
		this.kabupaten = kabupaten;
	}

	public String getKabupaten(){
		return kabupaten;
	}

	public void setKelurahan(String kelurahan){
		this.kelurahan = kelurahan;
	}

	public String getKelurahan(){
		return kelurahan;
	}

	public void setAlamat(String alamat){
		this.alamat = alamat;
	}

	public String getAlamat(){
		return alamat;
	}

	public void setKecamatanName(String kecamatanName){
		this.kecamatanName = kecamatanName;
	}

	public String getKecamatanName(){
		return kecamatanName;
	}

	public void setAccountId(String accountId){
		this.accountId = accountId;
	}

	public String getAccountId(){
		return accountId;
	}

	public void setKabupatenName(String kabupatenName){
		this.kabupatenName = kabupatenName;
	}

	public String getKabupatenName(){
		return kabupatenName;
	}

	public void setKecamatan(String kecamatan){
		this.kecamatan = kecamatan;
	}

	public String getKecamatan(){
		return kecamatan;
	}

	public void setKodepos(String kodepos){
		this.kodepos = kodepos;
	}

	public String getKodepos(){
		return kodepos;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setLongitude(String longitude){
		this.longitude = longitude;
	}

	public String getLongitude(){
		return longitude;
	}

	public void setKelurahanName(String kelurahanName){
		this.kelurahanName = kelurahanName;
	}

	public String getKelurahanName(){
		return kelurahanName;
	}

	@Override
 	public String toString(){
		return 
			"Shipping{" + 
			"provinsi = '" + provinsi + '\'' + 
			",latitude = '" + latitude + '\'' + 
			",kodepos_name = '" + kodeposName + '\'' + 
			",provinsi_name = '" + provinsiName + '\'' + 
			",type = '" + type + '\'' + 
			",kabupaten = '" + kabupaten + '\'' + 
			",kelurahan = '" + kelurahan + '\'' + 
			",alamat = '" + alamat + '\'' + 
			",kecamatan_name = '" + kecamatanName + '\'' + 
			",account_id = '" + accountId + '\'' + 
			",kabupaten_name = '" + kabupatenName + '\'' + 
			",kecamatan = '" + kecamatan + '\'' + 
			",kodepos = '" + kodepos + '\'' + 
			",id = '" + id + '\'' + 
			",longitude = '" + longitude + '\'' + 
			",kelurahan_name = '" + kelurahanName + '\'' + 
			"}";
		}
}
