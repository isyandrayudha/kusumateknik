package com.msf.project.sales1crm.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toolbar;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.github.clans.fab.FloatingActionButton;
import com.msf.project.sales1crm.AccountAddActivity;
import com.msf.project.sales1crm.ErrorAPIKeyActivity;
import com.msf.project.sales1crm.MenuActivity;
import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.adapter.AccountListCardAdapter;
import com.msf.project.sales1crm.callback.AccountListCallback;
import com.msf.project.sales1crm.callback.SortDialogCallback;
import com.msf.project.sales1crm.customview.CustomDialog;
import com.msf.project.sales1crm.customview.CustomEditText;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.imageloader.ImageLoader;
import com.msf.project.sales1crm.model.AccountModel;
import com.msf.project.sales1crm.presenter.AccountListPresenter;
import com.msf.project.sales1crm.service.GPSTracker;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by christianmacbook on 09/05/18.
 */

public class AccountCardFrangment extends Fragment implements AccountListCallback {

    @BindView(R.id.rlNoConnection)
    RelativeLayout rlNoConnection;

    @BindView(R.id.tvRefresh)
    CustomTextView tvRefresh;

    @BindView(R.id.etSearch)
    CustomEditText etSearch;

    @BindView(R.id.ivClear)
    ImageView ivClear;

    @BindView(R.id.lvAccount)
    RecyclerView lvAccount;

    @BindView(R.id.lvAccountSwipe)
    SwipeRefreshLayout lvAccountSwipe;

    @BindView(R.id.faAccountAdd)
    FloatingActionButton faAccountAdd;

    @BindView(R.id.faQuickAdd)
    FloatingActionButton faQuickAdd;

    @BindView(R.id.llNotFound)
    LinearLayout llNotFound;

//    @BindView(R.id.rlProgress)
//    RelativeLayout rlProgress;
    //    @BindView(R.id.viewpagerr)
//    ViewPager viewpager;
    Unbinder unbinder;
    @BindView(R.id.shimmerLayout)
    ShimmerFrameLayout shimmerLayout;
    @BindView(R.id.llshim)
    LinearLayout llshim;


//    @BindView(R.id.rlSort)
//    RelativeLayout rlSort;

//    @BindView(R.id.ivSelectSort)
//    ImageView ivSelectSort;

//    @BindView(R.id.rlFilter)
//    RelativeLayout rlFilter;

    private View view;
    private Context context;
    private AccountListCardAdapter adapter;
    private AccountListPresenter presenter;
    private List<AccountModel> list = new ArrayList<>();
    private String stringLatitude, stringLongitude;
    private CustomDialog dialogQuick;
    private String[] sort = {"Name A to Z", "Name Z to A", "Industry A to Z", "Industry Z to A"};
    private Toolbar mtoolbar;
    private boolean isScrolling = false;
    private int currentItems, totalItems, scrollOutItems;


    private int prevSize = 0, sort_id = 0;
    private boolean isLoadMore = true;
    private boolean isMaxSize = true;
    private boolean pause = false;

    public static AccountCardFrangment newInstance() {
        AccountCardFrangment fragment = new AccountCardFrangment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_accountcard, container, false);
        setHasOptionsMenu(true);
        ButterKnife.bind(this, this.view);
        return this.view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);
        this.context = getActivity();

        this.presenter = new AccountListPresenter(context, this);


        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Account");

        initData();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_list:
                    MenuActivity.getInstance().setFragment(AccountFrangment.newInstance());
                    item.setIcon(R.drawable.ic_list_white_24dp);
                break;
            case R.id.action_cards:
                    MenuActivity.getInstance().setFragment(AccountCardFrangment.newInstance());
                    item.setIcon(R.drawable.ic_cards_black_24dp);
                break;
            case R.id.action_maps:
                    MenuActivity.getInstance().setFragment(AccountMapStreesFragment.newInstance());
                    item.setIcon(R.drawable.ic_map_white_24dp);
                break;
            case R.id.action_filter:
                SortDialogFragment DialogFragment = new SortDialogFragment(context, new SortDialogCallback() {

                    @Override
                    public void onDialogCallback(Bundle data) {
                        sort_id = Integer.parseInt(data.getString("id"));

                        getDataFromAPI(0, sort_id);
//                            ivSelectSort.setVisibility(View.VISIBLE);
                    }
                }, sort);
                DialogFragment.show(getActivity().getSupportFragmentManager(), "sortDialog");
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    private void initData() {
        Sales1CRMUtils.Utils.imageLoader = new ImageLoader(context);

        etSearch.clearFocus();
//        lvAccount.setOnScrollChangeListener(new EndlessRecycleViewScrollListener());


        lvAccountSwipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                llshim.setVisibility(View.VISIBLE);
                shimmerLayout.setVisibility(View.VISIBLE);
                shimmerLayout.startShimmer();
                getDataFromAPI(0, 0);

            }
        });


        //Set Seach Function
        setTextWatcherForSearch();
        getDataFromAPI(0, 0);

        ivClear.setOnClickListener(click);
        tvRefresh.setOnClickListener(click);
        faAccountAdd.setOnClickListener(click);
        faQuickAdd.setOnClickListener(click);
//        rlSort.setOnClickListener(click);
    }

    private void fetchData() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                presenter.setupMoreList(ApiParam.API_010, prevSize, sort_id, etSearch.getText().toString(), stringLatitude, stringLongitude);
                adapter.notifyDataSetChanged();

            }
        }, 5000);
    }

    private void setTextWatcherForSearch() {
        etSearch.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    if (etSearch.getText().length() == 0) {
                        ivClear.setVisibility(View.VISIBLE);
                    }
//                } else {
//                    if (etSearch.getText().length() == 0) {
//                        ivClear.setVisibility(View.GONE);
//                    } else {
//                        ivClear.setVisibility(View.VISIBLE);
//                    }

                }
            }
        });

        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    llshim.setVisibility(View.VISIBLE);
                    shimmerLayout.setVisibility(View.VISIBLE);
                    shimmerLayout.startShimmer();
                    getDataFromAPI(0, 0);
                }

                return true;
            }
        });
    }

    private void getDataFromAPI(int prev, int sort) {
        isMaxSize = true;
        isLoadMore = true;

        llNotFound.setVisibility(View.GONE);

        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(context)) {
            GPSTracker gpsTracker = new GPSTracker(context);
            if (gpsTracker.canGetLocation()) {
                stringLatitude = String.valueOf(gpsTracker.latitude);
                stringLongitude = String.valueOf(gpsTracker.longitude);

                pause = false;
                presenter.setupList(ApiParam.API_010, prev, sort, etSearch.getText().toString(), stringLatitude, stringLongitude);
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gpsTracker.showSettingsAlert();
                lvAccountSwipe.setRefreshing(false);
            }
        } else {
            lvAccountSwipe.setVisibility(View.GONE);
            rlNoConnection.setVisibility(View.VISIBLE);
        }
    }

    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.ivClear:
                    etSearch.setText("");
                    etSearch.clearFocus();
                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);
                    getDataFromAPI(0, 0);
                    break;
                case R.id.tvRefresh:
                    llshim.setVisibility(View.VISIBLE);
                    rlNoConnection.setVisibility(View.GONE);
                    getDataFromAPI(0, 0);
                    break;
                case R.id.faAccountAdd:
                    startActivity(new Intent(context, AccountAddActivity.class));
                    break;
                case R.id.faQuickAdd:
                    dialogQuick = CustomDialog.setupDialogAccountAdd(context);
                    final CustomEditText etName = (CustomEditText) dialogQuick.findViewById(R.id.etAccountName);
                    final CustomEditText etPhone = (CustomEditText) dialogQuick.findViewById(R.id.etPhone);
                    CustomTextView tvYa = (CustomTextView) dialogQuick.findViewById(R.id.tvOK);
                    CustomTextView tvTidak = (CustomTextView) dialogQuick.findViewById(R.id.tvBack);

                    tvYa.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            AccountModel model = new AccountModel();
                            model.setAccount_name(etName.getText().toString());
                            model.setPhone(etPhone.getText().toString());

                            presenter.setupCreate(ApiParam.API_013, model);
                        }
                    });
                    tvTidak.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialogQuick.dismiss();
                        }
                    });
                    dialogQuick.show();
                    break;
//                case R.id.rlSort:
//                    SortDialogFragment DialogFragment = new SortDialogFragment(context, new SortDialogCallback() {
//
//                        @Override
//                        public void onDialogCallback(Bundle data) {
//                            sort_id = Integer.parseInt(data.getString("id"));
//
//                            getDataFromAPI(0, sort_id);
////                            ivSelectSort.setVisibility(View.VISIBLE);
//                        }
//                    }, sort);
//                    DialogFragment.show(getActivity().getSupportFragmentManager(), "sortDialog");
//                    break;
                default:
                    break;
            }
        }
    };

    private void setList() {
        llshim.setVisibility(View.VISIBLE);
        shimmerLayout.setVisibility(View.GONE);
        adapter = new AccountListCardAdapter(context, this.list);
        List<AccountListCardAdapter.Row> rows = new ArrayList<AccountListCardAdapter.Row>();
        for (AccountModel cartModel : list) {
            //Read List by Row to insert into List Adapter
            rows.add(new AccountListCardAdapter.Item(cartModel));
        }
        //Set Row Adapter

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        lvAccount.setLayoutManager(layoutManager);

//       final SnapHelper snapHelper = new LinearSnapHelper();
//        if (lvAccount.getOnFlingListener() == null){
//            snapHelper.attachToRecyclerView(lvAccount);
//        }
//
//
//
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                RecyclerView.ViewHolder viewHolder = lvAccount.findViewHolderForAdapterPosition(0);
//                RelativeLayout rrl = viewHolder.itemView.findViewById(R.id.rrl);
//                rrl.animate().scaleY(1).scaleX(1).setDuration(350).setInterpolator(new AccelerateInterpolator()).start();
//            }
//        },1000);

        lvAccount.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
//                View v = snapHelper.findSnapView(layoutManager);
//                int pos = layoutManager.getPosition(v);
//                RecyclerView.ViewHolder viewHolder = lvAccount.findViewHolderForAdapterPosition(pos);
//                RelativeLayout rrl = viewHolder.itemView.findViewById(R.id.rrl);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE) {
                    isScrolling = true;
                }
//                if (newState == RecyclerView.SCROLL_STATE_IDLE) {
////                    isScrolling = true;
//                    rrl.animate().setDuration(350).scaleX(1).scaleY(1).setInterpolator(new AccelerateInterpolator()).start();
//                } else {
//                    rrl.animate().setDuration(350).scaleX(0.95f).scaleY(0.95f).setInterpolator(new AccelerateInterpolator()).start();
//                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                currentItems = layoutManager.getChildCount();
                totalItems = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstVisibleItemPosition();

                if (isScrolling && (currentItems + scrollOutItems == totalItems)) {
                    isScrolling = false;
                    fetchData();

                }
            }
        });
        adapter.setRows(rows);
        lvAccount.setAdapter(adapter);
        llshim.setVisibility(View.INVISIBLE);
        shimmerLayout.stopShimmer();
        shimmerLayout.setVisibility(View.INVISIBLE);
    }

    private void setMoreList() {
        List<AccountListCardAdapter.Row> rows = new ArrayList<AccountListCardAdapter.Row>();
        for (AccountModel country : list) {
            // Add the country to the list
            rows.add(new AccountListCardAdapter.Item(country));
        }
        adapter.setRows(rows);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void finishAccountList(String result, List<AccountModel> list) {
        lvAccountSwipe.setRefreshing(false);
        if (result.equalsIgnoreCase("OK")) {
            prevSize = list.size();
            this.list = list;

            isMaxSize = false;
            isLoadMore = false;
            setList();
            if (list.size() > 0) {
                llNotFound.setVisibility(View.GONE);
            } else {
                llNotFound.setVisibility(View.VISIBLE);
            }
        } else if (result.equalsIgnoreCase("FAILED_KEY")) {
            startActivity(new Intent(context, ErrorAPIKeyActivity.class));
            MenuActivity.getInstance().finish();
        } else {
            rlNoConnection.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void finishAccountMoreList(String result, List<AccountModel> list) {
        if (result.equals("OK")) {
            for (int i = 0; i < list.size(); i++) {
                this.list.add(list.get(i));
            }

            if (this.prevSize == this.list.size())
                isMaxSize = true;
            this.prevSize = this.list.size();
            setMoreList();
        }
        this.isLoadMore = false;
    }

    @Override
    public void finishQuickCreate(String result) {
        if (result.equalsIgnoreCase("OK")) {
            dialogQuick.dismiss();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }


    @Override
    public void onPause() {
        pause = true;
        super.onPause();
    }

    @Override
    public void onResume() {
        if (pause) {
            getDataFromAPI(0, 0);
        }
        super.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
