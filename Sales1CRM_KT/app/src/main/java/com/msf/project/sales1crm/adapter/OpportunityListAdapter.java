package com.msf.project.sales1crm.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;

import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.model.OpportunityModel;
import com.msf.project.sales1crm.utility.PreferenceUtility;

import java.util.List;

/**
 * Created by christianmacbook on 06/06/18.
 */

public class OpportunityListAdapter extends BaseAdapter {
    private Context context;
    private Bitmap icon;
    private String url, iterated;
    private int size;
    private List<OpportunityModel> listData;

    public OpportunityListAdapter(Context context, List<OpportunityModel> objects) {
        this.context = context;
        this.listData = objects;

        this.url = PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.URL);
    }

    public static abstract class Row {
    }

    // This is for making Section, ex: A B C D etc
    public static final class Section extends Row {
        public final String text;

        public Section(String text) {
            this.text = text;
        }
    }

    // This is for making Row, ex: A B C D etc
    public static final class Item extends Row {
        public final OpportunityModel text;

        public Item(OpportunityModel text) {
            this.text = text;
        }

        public OpportunityModel getItem() {
            return text;
        }
    }

    private List<Row> rows;

    public void setRows(List<Row> rows) {
        this.rows = rows;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return rows.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return rows.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        if (getItem(position) instanceof Section) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public View getView(final int position, View converView, ViewGroup parent) {
        View view = converView;
        final ViewHolder holder;

        final Item item = (Item) getItem(position);
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) parent.getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = (LinearLayout) inflater.inflate(
                    R.layout.row_opportunitylist, parent, false);
            holder = new ViewHolder();
            holder.tvOpportunityName = (CustomTextView) view
                    .findViewById(R.id.tvOpportunityName);
            holder.tvAccountName = (CustomTextView) view
                    .findViewById(R.id.tvAccountName);
            holder.tvStage = (CustomTextView) view
                    .findViewById(R.id.tvStage);
            holder.tvCloseDate = (CustomTextView) view
                    .findViewById(R.id.tvCloseDate);
            holder.tvProgress = (CustomTextView) view
                    .findViewById(R.id.tvProgress);
            holder.tvSize = (CustomTextView) view
                    .findViewById(R.id.tvSize);
            holder.tvrefNumber = (CustomTextView) view
                    .findViewById(R.id.tvrefNumber);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        Log.i("TAG",
                "number: "
                        + coolFormat(Double.parseDouble(item.getItem()
                        .getSize()), 0));

        holder.tvOpportunityName.setText(item.text.getName());
        holder.tvAccountName.setText(item.text.getAccount_name());
        holder.tvStage.setText(item.text.getStage_name());
        holder.tvCloseDate.setText(item.text.getClose_date());
        holder.tvProgress.setText(item.text.getProgress() + "%");
        holder.tvSize.setText(size + iterated);
        holder.tvrefNumber.setText(item.text.getComplete_ref_number());

        return view;
    }


    /**
     * Recursive implementation, invokes itself for each factor of a thousand,
     * increasing the class on each invokation.
     *
     * @param n
     *            the number to format
     * @param iteration
     *            in fact this is the class from the array c
     * @return a String representing the number n formatted in a cool looking
     *         way.
     */
    private char[] c = new char[] { 'K', 'M', 'B', 'T' };

    private String coolFormat(double n, int iteration) {
        double d = ((long) n / 100) / 10.0;
        boolean isRound = (d * 10) % 10 == 0;// true if the decimal part is
        // equal to 0 (then it's trimmed
        // anyway)
        Log.i("TAG", "num : " + d);
        size = (int) d;
        iterated = String.valueOf(c[iteration]);
        Log.i("TAG", "iteration : " + c[iteration]);
        return (d < 1000 ? // this determines the class, i.e. 'k', 'm' etc
                ((d > 99.9 || isRound || (!isRound && d > 9.99) ? // this decides
                        // whether to trim
                        // the decimals
                        (int) d * 10 / 10
                        : d + "" // (int) d * 10 / 10 drops the decimal
                ) + "" + c[iteration])
                : coolFormat(d, iteration + 1));
    }

    private static class ViewHolder {
        CustomTextView tvOpportunityName, tvAccountName, tvStage, tvCloseDate, tvProgress, tvSize, tvrefNumber;
    }
}
