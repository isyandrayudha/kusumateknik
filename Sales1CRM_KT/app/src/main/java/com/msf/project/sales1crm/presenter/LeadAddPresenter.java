package com.msf.project.sales1crm.presenter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;

import com.msf.project.sales1crm.callback.LeadAddCallback;
import com.msf.project.sales1crm.model.LeadModel;
import com.msf.project.sales1crm.service.NetworkConnection;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.PreferenceUtility;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;
import com.msf.project.sales1crm.utility.Sales1CRMUtilsJSON;

import org.json.JSONException;
import org.json.JSONObject;

public class LeadAddPresenter {
    private final String TAG = LeadAddPresenter.class.getSimpleName();
    private Context context;
    private Messenger messenger;
    private Message message;
    private Bundle bundle;
    private Handler handler;
    private String failureResponse = "";
    private String[] stringResponse = {""};
    private LeadAddCallback callback;
    private String resultLogin = "NG";
    private String response = "Tolong Periksa Koneksi Anda";

    public LeadAddPresenter(Context context, LeadAddCallback listener) {
        this.context = context;
        this.callback = listener;
    }

    private void doNetworkService(String url, String params, String from) {
        Log.d(TAG, "url login " + url);
        Log.d(TAG, "params : " + params);
        Intent networkIntent = new Intent(this.context, NetworkConnection.class);
        this.messenger = new Messenger(this.handler);
        networkIntent.putExtra("messenger", this.messenger);
        networkIntent.putExtra("url", url);
        networkIntent.putExtra("params", params);
        networkIntent.putExtra("from", from);
        this.context.startService(networkIntent);
    }

    public void setupAdd(int apiIndex, LeadModel leadModel) {
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)) {
            obtainAddAccount(apiIndex, leadModel);
        }else{
            callback.finishAdd(this.resultLogin, this.response);
        }
    }

    public void obtainAddAccount(int apiIndex, LeadModel leadModel) {
        this.handler = new Handler(this.context.getMainLooper()) {

            @Override
            public void handleMessage(Message msg) {
                parseAddAccount(msg);
            }

        };

        JSONObject jsonObject = new JSONObject();
        try {
            //Umum
            jsonObject.put("api_key", PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.API_KEY));
            jsonObject.put("title", leadModel.getTitle_id());
            jsonObject.put("first_name", leadModel.getFirst_name());
            jsonObject.put("last_name", leadModel.getLast_name());
            jsonObject.put("company", leadModel.getCompany());
            jsonObject.put("position", leadModel.getPosition_id());
            jsonObject.put("email", leadModel.getEmail());
            jsonObject.put("total_employee", leadModel.getTotal_employee());
            jsonObject.put("phone", leadModel.getPhone());
            jsonObject.put("industry", leadModel.getIndustry_id());
            jsonObject.put("lead_stage", leadModel.getLead_status_id());
            jsonObject.put("lead_source", leadModel.getLead_source_id());
            jsonObject.put("description", leadModel.getDescription());
            jsonObject.put("create_date", leadModel.getCreate_date());
            jsonObject.put("image_master", leadModel.getBase64Image());
            jsonObject.put("assign_to", leadModel.getAssign_to());
            //Alamat
            jsonObject.put("street", leadModel.getAddress());
            jsonObject.put("provinsi", leadModel.getProvinsi_id());
            jsonObject.put("kabupaten", leadModel.getKabupaten_id());
            jsonObject.put("kecamatan", leadModel.getKecamatan_id());
            jsonObject.put("kelurahan", leadModel.getKelurahan_id());
            jsonObject.put("latitude",leadModel.getLatitude());
            jsonObject.put("longitude",leadModel.getLongitude());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        doNetworkService(ApiParam.urlBuilder(this.context, apiIndex), jsonObject.toString(), "LeadAdd");
    }

    private void parseAddAccount(Message message) {
        this.message = message;
        this.bundle = this.message.getData();
        this.stringResponse[0] = this.bundle.getString("network_response");
        this.failureResponse = this.bundle.getString("network_failure");
        Log.d(TAG, "responseString[0] info " + this.stringResponse[0]);
        if (this.failureResponse.equalsIgnoreCase("yes")){
            this.callback.finishAdd(this.resultLogin, this.response);
        }else{
            try {
                this.resultLogin = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                if(this.resultLogin.equalsIgnoreCase("OK")){
                    this.response = Sales1CRMUtilsJSON.JSONUtility.getLeadIdFromServer(this.stringResponse[0]);
                } else {
                    this.response = Sales1CRMUtilsJSON.JSONUtility.getResponseFromServer(this.stringResponse[0]);
                }
                this.callback.finishAdd(this.resultLogin, this.response);
            } catch (JSONException e) {
                Log.d(TAG, "Exception detail Response " + e.getMessage());
            }
        }
    }

    public void setupEdit(int apiIndex, LeadModel leadModel) {
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)) {
            obtainEditAccount(apiIndex, leadModel);
        }else{
            callback.finishEdit(this.resultLogin, this.response);
        }
    }

    public void obtainEditAccount(int apiIndex, LeadModel leadModel) {
        this.handler = new Handler(this.context.getMainLooper()) {

            @Override
            public void handleMessage(Message msg) {
                parseEditAccount(msg);
            }

        };

        JSONObject jsonObject = new JSONObject();
        try {
            //Umum
            jsonObject.put("api_key", PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.API_KEY));
            jsonObject.put("lead_id", leadModel.getId());
            jsonObject.put("title", leadModel.getTitle_id());
            jsonObject.put("first_name", leadModel.getFirst_name());
            jsonObject.put("last_name", leadModel.getLast_name());
            jsonObject.put("company", leadModel.getCompany());
            jsonObject.put("position", leadModel.getPosition_id());
            jsonObject.put("email", leadModel.getEmail());
            jsonObject.put("total_employee", leadModel.getTotal_employee());
            jsonObject.put("phone", leadModel.getPhone());
            jsonObject.put("industry", leadModel.getIndustry_id());
            jsonObject.put("lead_stage", leadModel.getLead_status_id());
            jsonObject.put("lead_source", leadModel.getLead_source_id());
            jsonObject.put("description", leadModel.getDescription());
            jsonObject.put("create_date", leadModel.getCreate_date());
            jsonObject.put("image_master", leadModel.getBase64Image());
            jsonObject.put("assign_to", leadModel.getAssign_to());
            //Alamat
            jsonObject.put("street", leadModel.getAddress());
            jsonObject.put("provinsi", leadModel.getProvinsi_id());
            jsonObject.put("kabupaten", leadModel.getKabupaten_id());
            jsonObject.put("kecamatan", leadModel.getKecamatan_id());
            jsonObject.put("kelurahan", leadModel.getKelurahan_id());
            jsonObject.put("latitude",leadModel.getLatitude());
            jsonObject.put("longitude",leadModel.getLongitude());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        doNetworkService(ApiParam.urlBuilder(this.context, apiIndex), jsonObject.toString(), "LeadEdit");
    }

    private void parseEditAccount(Message message) {
        this.message = message;
        this.bundle = this.message.getData();
        this.stringResponse[0] = this.bundle.getString("network_response");
        this.failureResponse = this.bundle.getString("network_failure");
        Log.d(TAG, "responseString[0] info " + this.stringResponse[0]);
        if (this.failureResponse.equalsIgnoreCase("yes")){
            this.callback.finishEdit(this.resultLogin, this.response);
        }else{
            try {
                this.resultLogin = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                this.callback.finishEdit(this.resultLogin, this.response);
            } catch (JSONException e) {
                Log.d(TAG, "Exception detail Response " + e.getMessage());
            }
        }
    }
}
