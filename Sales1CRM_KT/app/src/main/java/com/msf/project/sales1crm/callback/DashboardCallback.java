package com.msf.project.sales1crm.callback;

import com.msf.project.sales1crm.model.AbsenceModel;
import com.msf.project.sales1crm.model.DashboardFirstModel;
import com.msf.project.sales1crm.model.DashboardSecondModel;
import com.msf.project.sales1crm.model.DashboardThirdModel;
import com.msf.project.sales1crm.model.TotalOpportunityModel;

import java.util.List;

public interface DashboardCallback {
    void finishFirst(String result, DashboardFirstModel model);
    void finishSecond(String result, DashboardSecondModel model);
    void finishThird(String result, List<DashboardThirdModel> listLead, List<DashboardThirdModel> listLead1, List<DashboardThirdModel> listOppor, List<DashboardThirdModel> listOppor1);
    void TotOpportunity(String result, TotalOpportunityModel totOporModel);
    void absence(String result, AbsenceModel model);
}
