package com.msf.project.sales1crm.presenter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;

import com.msf.project.sales1crm.callback.DashboardCallback;
import com.msf.project.sales1crm.model.AbsenceModel;
import com.msf.project.sales1crm.model.DashboardFirstModel;
import com.msf.project.sales1crm.model.DashboardSecondModel;
import com.msf.project.sales1crm.model.DashboardThirdModel;
import com.msf.project.sales1crm.model.TotalOpportunityModel;
import com.msf.project.sales1crm.service.NetworkConnection;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.PreferenceUtility;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;
import com.msf.project.sales1crm.utility.Sales1CRMUtilsJSON;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class DashboardPresenter {
    private final String TAG = DashboardPresenter.class.getSimpleName();
    private Context context;
    private Messenger messenger;
    private Message message;
    private Bundle bundle;
    private Handler handler;
    private String failureResponse = "";
    private String[] stringResponse = {""};
    private DashboardCallback callback;
    private DashboardFirstModel firstModel;
    private DashboardSecondModel secondModel;
    private TotalOpportunityModel totOpor;
    private AbsenceModel absenceModel;
    private List<DashboardThirdModel> listLead, listLead1;
    private List<DashboardThirdModel> listOppor, listOppor1;
    private String resultLogin = "NG";
    private String response = "Tolong Periksa Koneksi Anda";

    public DashboardPresenter(Context context, DashboardCallback listener) {
        this.context = context;
        this.callback = listener;
    }

    private void doNetworkService(String url, String params, String from) {
        Log.d(TAG, "url login " + url);
        Log.d(TAG, "params : " + params);
        Intent networkIntent = new Intent(this.context, NetworkConnection.class);
        this.messenger = new Messenger(this.handler);
        networkIntent.putExtra("messenger", this.messenger);
        networkIntent.putExtra("url", url);
        networkIntent.putExtra("params", params);
        networkIntent.putExtra("from", from);
        this.context.startService(networkIntent);
    }

    public void setupFirst(int apiIndex) {
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)) {
            obtainFirst(apiIndex);
        }else{
            callback.finishFirst(this.resultLogin, this.firstModel);
        }
    }

    public void obtainFirst(int apiIndex) {
        this.handler = new Handler(this.context.getMainLooper()) {

            @Override
            public void handleMessage(Message msg) {
                parseFirstItem(msg);
            }

        };

        JSONObject jsonObject = new JSONObject();
        try {
            //Umum
            jsonObject.put("api_key", PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.API_KEY));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        doNetworkService(ApiParam.urlBuilder(this.context, apiIndex), jsonObject.toString(), "ApiKey");
    }

    private void parseFirstItem(Message message) {
        this.message = message;
        this.bundle = this.message.getData();
        this.stringResponse[0] = this.bundle.getString("network_response");
        this.failureResponse = this.bundle.getString("network_failure");
        Log.d(TAG, "responseString[0] info " + this.stringResponse[0]);
        if (this.failureResponse.equalsIgnoreCase("yes")){
            this.callback.finishFirst(this.resultLogin, this.firstModel);
        }else{
            try {
                this.resultLogin = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                if (this.resultLogin.equalsIgnoreCase("OK")){
                    this.firstModel = Sales1CRMUtilsJSON.JSONUtility.getFirstDashboard(this.stringResponse[0]);
                }
                this.callback.finishFirst(this.resultLogin, this.firstModel);
            } catch (JSONException e) {
                Log.d(TAG, "Exception detail Response " + e.getMessage());
            }
        }
    }

    public void setupSecond(int apiIndex) {
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)) {
            obtainSecond(apiIndex);
        }else{
            this.callback.finishSecond(this.resultLogin, this.secondModel);
        }
    }

    public void obtainSecond(int apiIndex) {
        this.handler = new Handler(this.context.getMainLooper()) {

            @Override
            public void handleMessage(Message msg) {
                parseSecondItem(msg);
            }

        };

        JSONObject jsonObject = new JSONObject();
        try {
            //Umum
            jsonObject.put("api_key", PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.API_KEY));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        doNetworkService(ApiParam.urlBuilder(this.context, apiIndex), jsonObject.toString(), "ApiKey");
    }

    private void parseSecondItem(Message message) {
        this.message = message;
        this.bundle = this.message.getData();
        this.stringResponse[0] = this.bundle.getString("network_response");
        this.failureResponse = this.bundle.getString("network_failure");
        Log.d(TAG, "responseString[0] info " + this.stringResponse[0]);
        if (this.failureResponse.equalsIgnoreCase("yes")){
            this.callback.finishSecond(this.resultLogin, this.secondModel);
        }else{
            try {
                this.resultLogin = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                if (this.resultLogin.equalsIgnoreCase("OK")){
                    this.secondModel = Sales1CRMUtilsJSON.JSONUtility.getSecondDashboard(this.stringResponse[0]);
                }
                this.callback.finishSecond(this.resultLogin, this.secondModel);
            } catch (JSONException e) {
                Log.d(TAG, "Exception detail Response " + e.getMessage());
            }
        }
    }

    public void setupThird(int apiIndex) {
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)) {
            obtainThird(apiIndex);
        }else{
            this.callback.finishThird(this.resultLogin, this.listLead, this.listLead1, this.listOppor, this.listOppor1);
        }
    }

    public void obtainThird(int apiIndex) {
        this.handler = new Handler(this.context.getMainLooper()) {

            @Override
            public void handleMessage(Message msg) {
                parseThirdItem(msg);
            }

        };

        JSONObject jsonObject = new JSONObject();
        try {
            //Umum
            jsonObject.put("api_key", PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.API_KEY));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        doNetworkService(ApiParam.urlBuilder(this.context, apiIndex), jsonObject.toString(), "ApiKey");
    }

    private void parseThirdItem(Message message) {
        this.message = message;
        this.bundle = this.message.getData();
        this.stringResponse[0] = this.bundle.getString("network_response");
        this.failureResponse = this.bundle.getString("network_failure");
        Log.d(TAG, "responseString[0] info " + this.stringResponse[0]);
        if (this.failureResponse.equalsIgnoreCase("yes")){
            this.callback.finishThird(this.resultLogin, this.listLead, this.listLead1, this.listOppor, this.listOppor1);
        }else{
            try {
                this.resultLogin = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                if (this.resultLogin.equalsIgnoreCase("OK")){
                    this.listLead = Sales1CRMUtilsJSON.JSONUtility.getDashboardLeads(this.stringResponse[0]);
                    this.listLead1 = Sales1CRMUtilsJSON.JSONUtility.getDashboardLeads1(this.stringResponse[0]);
                    this.listOppor = Sales1CRMUtilsJSON.JSONUtility.getDashboardOppor(this.stringResponse[0]);
                    this.listOppor1 = Sales1CRMUtilsJSON.JSONUtility.getDashboardOppor1(this.stringResponse[0]);
                }
                this.callback.finishThird(this.resultLogin, this.listLead, this.listLead1, this.listOppor, this.listOppor1);
            } catch (JSONException e) {
                Log.d(TAG, "Exception detail Response " + e.getMessage());
            }
        }
    }

    public void setupTotOpor(int apiIndex) {
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)) {
            obtainTotOpor(apiIndex);
        }  else {
            callback.TotOpportunity(this.resultLogin,this.totOpor);
        }
    }

    public void obtainTotOpor(int apiIndex) {
        this.handler = new Handler(this.context.getMainLooper()) {

            @Override
            public void handleMessage(Message msg) {
                parseTotOpor(msg);
            }
        };

        JSONObject jsonObject = new JSONObject();
        try{
            jsonObject.put("api_key",PreferenceUtility.getInstance().loadDataString(context,PreferenceUtility.API_KEY));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        doNetworkService(ApiParam.urlBuilder(this.context,apiIndex),jsonObject.toString(),"ApiKey");

    }

    private void parseTotOpor(Message message) {
        this.message = message;
        this.bundle = this.message.getData();
        this.stringResponse[0] = this.bundle.getString("network_response");
        this.failureResponse = this.bundle.getString("network_failure");
        Log.d(TAG, "responseString[0] info total oportunity " + this.stringResponse[0]);
        if (this.failureResponse.equalsIgnoreCase("yes")) {
            this.callback.TotOpportunity(this.resultLogin, this.totOpor);
        } else {
            try{
                this.resultLogin = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                if (this.resultLogin.equalsIgnoreCase("OK")){
                    this.totOpor = Sales1CRMUtilsJSON.JSONUtility.getTotalOportunity(this.stringResponse[0]);
//                    Sales1CRMUtilsJSON.JSONUtility.getUserList(context, this.stringResponse[0]);
                }
                this.callback.TotOpportunity(this.resultLogin, this.totOpor);
            } catch (JSONException e){
                Log.d(TAG, "Exception detail Response  " + e.getMessage());
            }

        }
    }

    public void setupAbsen(int apiIndex){
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)) {
            obtainAbsen(apiIndex);
        }else{
            this.callback.finishThird(this.resultLogin, this.listLead, this.listLead1, this.listOppor, this.listOppor1);
        }
    }
    public void obtainAbsen(int apiIndex) {
        this.handler = new Handler(this.context.getMainLooper()) {

            @Override
            public void handleMessage(Message msg) {
                parseAbsen(msg);
            }
        };

        JSONObject jsonObject = new JSONObject();
        try{
            jsonObject.put("api_key",PreferenceUtility.getInstance().loadDataString(context,PreferenceUtility.API_KEY));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        doNetworkService(ApiParam.urlBuilder(this.context,apiIndex),jsonObject.toString(),"ApiKey");

    }

    private void parseAbsen(Message message) {
        this.message = message;
        this.bundle = this.message.getData();
        this.stringResponse[0] = this.bundle.getString("network_response");
        this.failureResponse = this.bundle.getString("network_failure");
        Log.d(TAG, "responseString[0] info Absence  " + this.stringResponse[0]);
        if (this.failureResponse.equalsIgnoreCase("yes")) {
            this.callback.absence(this.resultLogin, this.absenceModel);
        } else {
            try{
                this.resultLogin = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                if (this.resultLogin.equalsIgnoreCase("OK")){
                    this.absenceModel = Sales1CRMUtilsJSON.JSONUtility.getAbsence(this.stringResponse[0]);
                }
                this.callback.absence(this.resultLogin, this.absenceModel);
            } catch (JSONException e){
                Log.d(TAG, "Exception detail Response " + e.getMessage());
            }

        }
    }
}
