package com.msf.project.sales1crm.presenter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;

import com.msf.project.sales1crm.callback.OpportunityListByStageCallback;
import com.msf.project.sales1crm.model.OpportunityModel;
import com.msf.project.sales1crm.model.TotalNPriceOpporModel;
import com.msf.project.sales1crm.service.NetworkConnection;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.PreferenceUtility;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;
import com.msf.project.sales1crm.utility.Sales1CRMUtilsJSON;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class OpportunitylListByStagePresenter {
    private final String TAG = OpportunitylListByStagePresenter.class.getSimpleName();
    private Context context;
    private Messenger messenger;
    private Message message;
    private Bundle bundle;
    private Handler handler;
    private String failureResponse = "";
    private String[] stringResponse = {""};
    private OpportunityListByStageCallback callback;
    private TotalNPriceOpporModel totalNPriceOpporModel;
    private String result = "NG";
    private List<OpportunityModel> list = new ArrayList<>();



    public OpportunitylListByStagePresenter(Context context, OpportunityListByStageCallback callback){
        this.context = context;
        this.callback = callback;
    }


    private void doNetworkService(String url, String params, String from) {
        Log.d(TAG, "url " + url);
        Log.d(TAG, "params : " + params);
        Intent networkIntent = new Intent(this.context, NetworkConnection.class);
        this.messenger = new Messenger(this.handler);
        networkIntent.putExtra("messenger", this.messenger);
        networkIntent.putExtra("url", url);
        networkIntent.putExtra("params", params);
        networkIntent.putExtra("from", from);
        this.context.startService(networkIntent);
    }



    public void listMQL (int apiIndex, int offset, String stage_id, int sort){
        if(Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)) {
            obtainListMQL(apiIndex,offset,stage_id,sort);
        }else {
            callback.finishMQL(this.result,this.list);
        }

    }
    private void obtainListMQL (int apiIndex, int offset, String stage_id, int sort){
        this.handler = new Handler(this.context.getMainLooper()){
            @Override
            public void handleMessage(Message msg) {
                parseListMQL(msg);
            }
        };
        JSONObject jsonObject = new JSONObject();
        try{
            jsonObject.put("api_key", PreferenceUtility.getInstance().loadDataString(context,PreferenceUtility.API_KEY));
            jsonObject.put("offset",offset+"");
            jsonObject.put("stage_id", stage_id+"2");
            jsonObject.put("sort",sort+"");
//            jsonObject.put("search",search);
        } catch (JSONException e){
            e.printStackTrace();
        }
        doNetworkService(ApiParam.urlBuilder(this.context, apiIndex), jsonObject.toString(),"OpportunityListByMQL");
    }

    private void parseListMQL (Message message){
        this.message = message;
        this.bundle = this.message.getData();
        this.stringResponse[0] = this.bundle.getString("network_response");
        this.failureResponse = this.bundle.getString("network_failure");
        Log.d(TAG, "responseString[0] info MQL " + this.stringResponse[0]);
        if (this.failureResponse.equalsIgnoreCase("yes")){
            this.callback.finishMQL(this.result, this.list);
        }else{
            try {
                this.result = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                if (this.result.equalsIgnoreCase("OK")){
                    this.list = Sales1CRMUtilsJSON.JSONUtility.getOpportunityListbyMQL(this.stringResponse[0]);
                }
                this.callback.finishMQL(this.result, this.list);
            } catch (JSONException e) {
                Log.d(TAG, "Exception detail Response " + e.getMessage());
            }
        }

    }

    public void moreListMQL (int apiIndex, int offset, String stage_id, int sort){
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)) {
            obtainMoreListMQL(apiIndex, offset, stage_id, sort);
        }else{
            callback.finishMoreMQL(this.result, this.list);
        }

    }

    private void obtainMoreListMQL (int apiIndex, int offset, String stage_id, int sort){
        this.handler = new Handler(this.context.getMainLooper()){
            @Override
            public void handleMessage(Message msg) {
                parseMoreListMQL(msg);
            }
        };
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("api_key", PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.API_KEY));
            jsonObject.put("offset", offset+"");
            jsonObject.put("stage_id",stage_id+"2");
            jsonObject.put("sort",sort+"");
//            jsonObject.put("search", search);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        doNetworkService(ApiParam.urlBuilder(this.context, apiIndex), jsonObject.toString(), "OpportunityListByMQL");
    }

    private void parseMoreListMQL (Message message){
        this.message = message;
        this.bundle = this.message.getData();
        this.stringResponse[0] = this.bundle.getString("network_response");
        this.failureResponse = this.bundle.getString("network_failure");
        Log.d(TAG, "responseString[0] info MQL More " + this.stringResponse[0]);
        if (this.failureResponse.equalsIgnoreCase("yes")){
            this.callback.finishMoreMQL(this.result, this.list);
        }else{
            try {
                this.result = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                if (this.result.equalsIgnoreCase("OK")){
                    this.list = Sales1CRMUtilsJSON.JSONUtility.getOpportunityListbyMQL(this.stringResponse[0]);
                }
                this.callback.finishMoreMQL(this.result, this.list);
            } catch (JSONException e) {
                Log.d(TAG, "Exception detail Response " + e.getMessage());
            }
        }
    }

}
