package com.msf.project.sales1crm.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

/**
 * Created by christianmacbook on 23/05/18.
 */

public class TaskModel implements Comparable, Parcelable {
    private String id;
    private String subject;
    private String account_id;
    private String account_name;
    private String assignto;
    private String assignto_name;
    private String date;
    private String date_interval;
    private String description;
    private String priority_id;
    private String priority_name;
    private String task_status_id;
    private String task_status_name;
    private String created_at;

    public TaskModel() {
        setId("");
        setSubject("");
        setAccount_id("");
        setAccount_name("");
        setAssignto("");
        setAssignto_name("");
        setDate("");
        setDate_interval("");
        setDescription("");
        setPriority_id("");
        setPriority_id("");
        setTask_status_id("");
        setTask_status_name("");
        setCreated_at("");
    }

    public TaskModel(Parcel in) {
        readFromParcel(in);
    }

    public void readFromParcel(Parcel in) {
        id = in.readString();
        subject = in.readString();
        account_id = in.readString();
        account_name = in.readString();
        assignto = in.readString();
        assignto_name = in.readString();
        date = in.readString();
        date_interval = in.readString();
        description = in.readString();
        priority_id = in.readString();
        priority_name = in.readString();
        task_status_id = in.readString();
        task_status_name = in.readString();
        created_at = in.readString();
    }

    public static final Creator<TaskModel> CREATOR = new Creator<TaskModel>() {
        @Override
        public TaskModel createFromParcel(Parcel in) {
            return new TaskModel(in);
        }

        @Override
        public TaskModel[] newArray(int size) {
            return new TaskModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getAccount_id() {
        return account_id;
    }

    public void setAccount_id(String account_id) {
        this.account_id = account_id;
    }

    public String getAccount_name() {
        return account_name;
    }

    public void setAccount_name(String account_name) {
        this.account_name = account_name;
    }

    public String getAssignto() {
        return assignto;
    }

    public void setAssignto(String assignto) {
        this.assignto = assignto;
    }

    public String getAssignto_name() {
        return assignto_name;
    }

    public void setAssignto_name(String assignto_name) {
        this.assignto_name = assignto_name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate_interval() {
        return date_interval;
    }

    public void setDate_interval(String date_interval) {
        this.date_interval = date_interval;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPriority_id() {
        return priority_id;
    }

    public void setPriority_id(String priority_id) {
        this.priority_id = priority_id;
    }

    public String getPriority_name() {
        return priority_name;
    }

    public void setPriority_name(String priority_name) {
        this.priority_name = priority_name;
    }

    public String getTask_status_id() {
        return task_status_id;
    }

    public void setTask_status_id(String task_status_id) {
        this.task_status_id = task_status_id;
    }

    public String getTask_status_name() {
        return task_status_name;
    }

    public void setTask_status_name(String task_status_name) {
        this.task_status_name = task_status_name;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(subject);
        dest.writeString(account_id);
        dest.writeString(account_name);
        dest.writeString(assignto);
        dest.writeString(assignto_name);
        dest.writeString(date);
        dest.writeString(date_interval);
        dest.writeString(description);
        dest.writeString(priority_id);
        dest.writeString(priority_name);
        dest.writeString(task_status_id);
        dest.writeString(task_status_name);
        dest.writeString(created_at);
    }

    @Override
    public int compareTo(@NonNull Object o) {
        return 0;
    }
}
