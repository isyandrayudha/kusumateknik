package com.msf.project.sales1crm.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.util.Log;

public class MyServiceLocation extends BroadcastReceiver {

    public static final String ACTION_PROCESS_UPDATE = "com.msf.project.sales1crm.service.UPDATE_LOCATION";
    public static final String TAG = "LocationReceiver";


    protected LocationManager locationManager;

    @Override
    public void onReceive(Context context, Intent intent) {
//        if(intent != null){
//            final String action = intent.getAction();
//            if (ACTION_PROCESS_UPDATE.equals(action)){
//                LocationResult result = LocationResult.extractResult(intent);
//                if (result != null){
//                    Location location = result.getLastLocation();
//                    String location_string = new StringBuilder(""+location.getLatitude())
//                            .append("/")
//                            .append(location.getLongitude())
//                            .toString();
//                    try{
//                        Log.d("AAAA", location_string);
//                    }catch (Exception ex){
//                        Toast.makeText(context, location_string, Toast.LENGTH_SHORT).show();
//                    }
//                }
//            }
//
//        }
        Location location = (Location)intent.getParcelableExtra(LocationManager.KEY_LOCATION_CHANGED);
        if(location != null){
            onLocationReceiver(context, location);
            return;
        }

        if (intent.hasExtra(LocationManager.KEY_PROVIDER_ENABLED)){
            boolean enabled = intent.getBooleanExtra(LocationManager.KEY_PROVIDER_ENABLED, false);
            onProviderEnableChanged(enabled);
        }

    }

    private void onProviderEnableChanged(boolean enabled) {
        Log.i(TAG,"PROVIDER "  + (enabled ? "enabled" : " disabled" ));
    }

    private void onLocationReceiver(Context context, Location location) {
        Log.d(TAG, this + " Got location from " + location.getProvider() + ": " + location.getLatitude() + ", " + location.getLongitude());
    }
}
