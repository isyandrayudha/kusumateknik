package com.msf.project.sales1crm.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;

import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.model.ReasonModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;


public class ReasonListAdapter extends BaseAdapter {
    private List<ReasonModel> list = new ArrayList<>();
    private Context context;

    public ReasonListAdapter(Context context, List<ReasonModel> obj) {
        this.context = context;
        this.list = obj;
    }

    public static abstract class Row {

    }

    public static final class Section extends Row {
        public final String text;

        public Section(String text) {
            this.text = text;
        }
    }

    public static final class Item extends Row {
        public final ReasonModel text;

        public Item(ReasonModel text) {
            this.text = text;
        }

        public ReasonModel getItem() {
            return text;
        }
    }

    private List<Row> rows;

    public void setRows(List<Row> rows) {
        this.rows = rows;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return rows.size();
    }

    @Override
    public Object getItem(int position) {
        return rows.get(position);
    }

    @Override
    public long getItemId(int position) {
        return rows.indexOf(getItem(position));
    }

    @Override
    public int getItemViewType(int position) {
        if (getItem(position) instanceof Section) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        final ViewHolder holder;
        final Item item = (Item) getItem(position);
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = (LinearLayout) inflater.inflate(R.layout.row_reason, parent, false);
            holder = new ViewHolder();
            holder.tvReason = (CustomTextView) view.findViewById(R.id.tvReason);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.tvReason.setText(item.text.getReason());
        Log.d("TAG","Reason = " + item.text.getReason());
        return view;
    }


    public static class ViewHolder {
        @BindView(R.id.tvReason)
        CustomTextView tvReason;
    }
}
