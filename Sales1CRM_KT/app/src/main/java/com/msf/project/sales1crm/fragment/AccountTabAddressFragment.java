package com.msf.project.sales1crm.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.model.AccountAddressModel;
import com.msf.project.sales1crm.model.AccountModel;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.ButterKnife;
import butterknife.BindView;

/**
 * Created by christianmacbook on 15/05/18.
 */

public class AccountTabAddressFragment extends Fragment {

    @BindView(R.id.tvAlamatBilling)
    CustomTextView tvAlamatBilling;

    @BindView(R.id.tvProvinsiBilling)
    CustomTextView tvProvinsiBilling;

    @BindView(R.id.tvKabupatenBilling)
    CustomTextView tvKabupatenBilling;

    @BindView(R.id.tvKecamatanBilling)
    CustomTextView tvKecamatanBilling;

    @BindView(R.id.tvKelurahanBilling)
    CustomTextView tvKelurahanBilling;

    @BindView(R.id.tvKodeposBilling)
    CustomTextView tvKodeposBilling;

    @BindView(R.id.tvAlamatShipping)
    CustomTextView tvAlamatShipping;

    @BindView(R.id.tvProvinsiShipping)
    CustomTextView tvProvinsiShipping;

    @BindView(R.id.tvKabupatenShipping)
    CustomTextView tvKabupatenShipping;

    @BindView(R.id.tvKecamatanShipping)
    CustomTextView tvKecamatanShipping;

    @BindView(R.id.tvKelurahanShipping)
    CustomTextView tvKelurahanShipping;

    @BindView(R.id.tvKodeposShipping)
    CustomTextView tvKodeposShipping;

    @BindView(R.id.tvDistanceBilling)
    CustomTextView tvDistanceBilling;

    @BindView(R.id.tvDistanceShipping)
    CustomTextView tvDistanceShipping;

    @BindView(R.id.llBilling)
    LinearLayout llBilling;

    @BindView(R.id.llShipping)
    LinearLayout llShipping;

    private View view;
    private Context context;
    private AccountModel model;
    private AccountAddressModel billingModel, shippingModel;

    public static AccountTabPersonalFragment newInstance() {
        AccountTabPersonalFragment fragment = new AccountTabPersonalFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_accountaddress, container, false);

        return this.view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);
        this.context = getActivity();

        initView();
    }

    private void initView(){
        try {
            model = getArguments().getParcelable("accountUmum");
        }catch (Exception e){}

        try {
            JSONObject billingObject = new JSONObject(model.getAddress_billing());
            billingModel = new AccountAddressModel();
            billingModel.setId(billingObject.getString("id"));
            billingModel.setAddress(billingObject.getString("alamat"));
            billingModel.setProvinsi_id(billingObject.getString("provinsi"));
            billingModel.setProvinsi_name(billingObject.getString("provinsi_name"));
            billingModel.setKabupaten_id(billingObject.getString("kabupaten"));
            billingModel.setKabupaten_name(billingObject.getString("kabupaten_name"));
            billingModel.setKecamatan_id(billingObject.getString("kecamatan"));
            billingModel.setKecamatan_name(billingObject.getString("kecamatan_name"));
            billingModel.setKelurahan_id(billingObject.getString("kelurahan"));
            billingModel.setKelurahan_name(billingObject.getString("kelurahan_name"));
            billingModel.setKodepos(billingObject.getString("kodepos_name"));
            billingModel.setLatitude(billingObject.getString("latitude"));
            billingModel.setLongitude(billingObject.getString("longitude"));
            billingModel.setDistance(billingObject.getString("distance"));

            JSONObject shippingObject = new JSONObject(model.getAddress_shipping());
            shippingModel = new AccountAddressModel();
            shippingModel.setId(shippingObject.getString("id"));
            shippingModel.setAddress(shippingObject.getString("alamat"));
            shippingModel.setProvinsi_id(shippingObject.getString("provinsi"));
            shippingModel.setProvinsi_name(shippingObject.getString("provinsi_name"));
            shippingModel.setKabupaten_id(shippingObject.getString("kabupaten"));
            shippingModel.setKabupaten_name(shippingObject.getString("kabupaten_name"));
            shippingModel.setKecamatan_id(shippingObject.getString("kecamatan"));
            shippingModel.setKecamatan_name(shippingObject.getString("kecamatan_name"));
            shippingModel.setKelurahan_id(shippingObject.getString("kelurahan"));
            shippingModel.setKelurahan_name(shippingObject.getString("kelurahan_name"));
            shippingModel.setKodepos(shippingObject.getString("kodepos_name"));
            shippingModel.setLatitude(shippingObject.getString("latitude"));
            shippingModel.setLongitude(shippingObject.getString("longitude"));
            shippingModel.setDistance(shippingObject.getString("distance"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        tvAlamatBilling.setText(billingModel.getAddress());
        tvProvinsiBilling.setText(billingModel.getProvinsi_name());
        tvKabupatenBilling.setText(billingModel.getKabupaten_name());
        tvKecamatanBilling.setText(billingModel.getKecamatan_name());
        tvKelurahanBilling.setText(billingModel.getKelurahan_name());
        tvKodeposBilling.setText(billingModel.getKodepos());
        tvDistanceBilling.setText(billingModel.getDistance());

        tvAlamatShipping.setText(shippingModel.getAddress());
        tvProvinsiShipping.setText(shippingModel.getProvinsi_name());
        tvKabupatenShipping.setText(shippingModel.getKabupaten_name());
        tvKecamatanShipping.setText(shippingModel.getKecamatan_name());
        tvKelurahanShipping.setText(shippingModel.getKelurahan_name());
        tvKodeposShipping.setText(shippingModel.getKodepos());
        tvDistanceShipping.setText(shippingModel.getDistance());

        llBilling.setOnClickListener(click);
        llShipping.setOnClickListener(click);
    }

    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.llBilling:
                    String strUri = "http://maps.google.com/maps?q=loc:"+billingModel.getLatitude()+","+billingModel.getLongitude();
                    Intent intent = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(strUri));
                    intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                    startActivity(intent);
                    break;
                case R.id.llShipping:
                    String strUri1 = "http://maps.google.com/maps?q=loc:"+shippingModel.getLatitude()+","+shippingModel.getLongitude();
                    Intent intent1 = new Intent(android.content.Intent.ACTION_VIEW, Uri.parse(strUri1));
                    intent1.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                    startActivity(intent1);
                    break;
            }
        }
    };
}
