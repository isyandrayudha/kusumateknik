package com.msf.project.sales1crm.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.msf.project.sales1crm.ErrorAPIKeyActivity;
import com.msf.project.sales1crm.MenuActivity;
import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.adapter.OpportunityProductAdapter;
import com.msf.project.sales1crm.callback.KursCalculateCallback;
import com.msf.project.sales1crm.callback.ProductDialogCallback;
import com.msf.project.sales1crm.callback.ProductListCallback;
import com.msf.project.sales1crm.customview.CustomDialog;
import com.msf.project.sales1crm.customview.CustomEditText;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.imageloader.ImageLoader;
import com.msf.project.sales1crm.model.PriceModel;
import com.msf.project.sales1crm.model.ProductModel;
import com.msf.project.sales1crm.presenter.KursCalculatorPresenter;
import com.msf.project.sales1crm.presenter.ProductListPresenter;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.BindView;

@SuppressLint("ValidFragment")
public class ProductDialogFragment extends BaseDialogFragment implements ProductListCallback, KursCalculateCallback {

    @BindView(R.id.rlNoConnection)
    RelativeLayout rlNoConnection;

    @BindView(R.id.tvRefresh)
    CustomTextView tvRefresh;

    @BindView(R.id.etSearch)
    CustomEditText etSearch;

    @BindView(R.id.ivClear)
    ImageView ivClear;

    @BindView(R.id.lvProduct)
    ListView lvProduct;

    @BindView(R.id.lvProductSwipe)
    SwipeRefreshLayout lvProductSwipe;

    @BindView(R.id.llNotFound)
    LinearLayout llNotFound;

    @BindView(R.id.rlProgress)
    RelativeLayout rlProgress;

    private View view;
    private Context context;
    private OpportunityProductAdapter adapter;
    private ProductListPresenter presenter;
    private KursCalculatorPresenter kursCalculatorPresenter;
    private ProductDialogCallback callback;
    private List<ProductModel> list = new ArrayList<>();
    private String stringLatitude, stringLongitude;

    private int prevSize = 0;
    private boolean isLoadMore = true;
    private boolean isMaxSize = true;
    private boolean pause = false;

    private String currencyName, Currency_id,nilainya;

    @SuppressLint("ValidFragment")
    public ProductDialogFragment(Context context, ProductDialogCallback listener) {
        this.context = context;
        this.callback = listener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, 0);
       Currency_id = getArguments().getString("currencyNameid");
    }

    @Override
    public void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
        Window window = getDialog().getWindow();
        WindowManager.LayoutParams windowParams = window.getAttributes();
        windowParams.width = (int) (this.context.getResources().getDisplayMetrics().widthPixels * 0.99);
        windowParams.height = (int) (this.context.getResources().getDisplayMetrics().heightPixels * 0.97);
        window.setAttributes(windowParams);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreateView(inflater, container, savedInstanceState);

        this.view = inflater.inflate(R.layout.dialog_productlist, container, false);

        return this.view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);

        this.presenter = new ProductListPresenter(context, this);
        this.kursCalculatorPresenter = new KursCalculatorPresenter(context, this);

        initView();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
    }

    private void initView() {
        Sales1CRMUtils.Utils.imageLoader = new ImageLoader(context);

        etSearch.clearFocus();
        lvProduct.setOnScrollListener(new ProductDialogFragment.ListLazyLoad());
        lvProductSwipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getDataFromAPI(0,Currency_id);
            }
        });

        //Set Seach Function
        setTextWatcherForSearch();
        getDataFromAPI(0,Currency_id);

        ivClear.setOnClickListener(click);
        tvRefresh.setOnClickListener(click);
    }

    private void setTextWatcherForSearch() {
        etSearch.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                {
                    if (etSearch.getText().length() == 0){
                        ivClear.setVisibility(View.VISIBLE);
                    }
                } else {
                    if (etSearch.getText().length() == 0)
                    {
                        ivClear.setVisibility(View.GONE);
                    }else{
                        ivClear.setVisibility(View.VISIBLE);
                    }

                }
            }
        });

        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    getDataFromAPI(0,Currency_id);
                }

                return true;
            }
        });
    }

    private void getDataFromAPI(int prev, String Currency_id){
        isMaxSize = true;
        isLoadMore = true;

        llNotFound.setVisibility(View.GONE);

        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(context)){
            pause = false;
            presenter.setupList(ApiParam.API_050, prev, etSearch.getText().toString(),Currency_id);
        }else{
            lvProductSwipe.setVisibility(View.GONE);
            rlNoConnection.setVisibility(View.VISIBLE);
        }
    }

    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.ivClear:
                    etSearch.setText("");
                    etSearch.clearFocus();
                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);
                    getDataFromAPI(0,Currency_id);
                    break;
                case R.id.tvRefresh:
                    lvProductSwipe.setVisibility(View.VISIBLE);
                    rlNoConnection.setVisibility(View.GONE);

                    getDataFromAPI(0, Currency_id);
                    break;
                default:
                    break;
            }
        }
    };

    private void setList() {
        adapter = new OpportunityProductAdapter(context, this.list);

        List<OpportunityProductAdapter.Row> rows = new ArrayList<OpportunityProductAdapter.Row>();
        for (ProductModel model : list){
            //Read List by Row to insert into List Adapter
            rows.add(new OpportunityProductAdapter.Item(model));
        }
        
        //Set Row Adapter
        adapter.setRows(rows);
        lvProduct.setAdapter(adapter);
        lvProduct.setOnItemClickListener(new ProductDialogFragment.AdapterOnItemClick());
        lvProduct.deferNotifyDataSetChanged();
    }

    private void setMoreList() {
        List<OpportunityProductAdapter.Row> rows = new ArrayList<OpportunityProductAdapter.Row>();

        for (ProductModel country : list) {
            // Add the country to the list
            rows.add(new OpportunityProductAdapter.Item(country));
        }
        adapter.setRows(rows);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void finishProductList(String result, List<ProductModel> list) {
        lvProductSwipe.setRefreshing(false);
        rlProgress.setVisibility(View.GONE);
        if (result.equalsIgnoreCase("OK")){
            prevSize = list.size();
            this.list = list;

            isMaxSize = false;
            isLoadMore = false;

            setList();
            if (list.size() > 0){
                llNotFound.setVisibility(View.GONE);
            }else{
                llNotFound.setVisibility(View.VISIBLE);
            }
        }else if (result.equalsIgnoreCase("FAILED_KEY")){
            startActivity(new Intent(context, ErrorAPIKeyActivity.class));
            MenuActivity.getInstance().finish();
        }else{
            lvProductSwipe.setVisibility(View.GONE);
            rlNoConnection.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void finishProductMoreList(String result, List<ProductModel> list) {
        if (result.equals("OK")) {
            for (int i = 0; i < list.size(); i++) {
                this.list.add(list.get(i));
            }

            if (this.prevSize == this.list.size())
                isMaxSize = true;
            this.prevSize = this.list.size();
            setMoreList();
        }
        this.isLoadMore = false;
    }

    @Override
    public void finishCalculate(String result, PriceModel priceModel) {
        if(result.equalsIgnoreCase("OK")){
           nilainya = priceModel.getPrice();
        }
    }

    private class ListLazyLoad implements AbsListView.OnScrollListener {

        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {

        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            if(firstVisibleItem+visibleItemCount == totalItemCount && totalItemCount!=0)
            {
                Log.d("TAG", "isLoadMore : " + isLoadMore);
                if(isLoadMore == false && isMaxSize == false)
                {
                    isLoadMore = true;

                    prevSize = list.size();

                    presenter.setupMoreList(ApiParam.API_050, prevSize, etSearch.getText().toString(),Currency_id);
                }
            }
        }
    }

    private class AdapterOnItemClick implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            // TODO Auto-generated method stub
            final OpportunityProductAdapter.Item item = (OpportunityProductAdapter.Item) lvProduct.getAdapter().getItem(position);
            Log.d("TAG","PDI " + item.text.getDetail_id());
//                    kursCalculatorPresenter.setupPrice(ApiParam.API_129,item.text.getDetail_id(), Currency_id);

            final CustomDialog dialog = CustomDialog.setupDialogProduct(context, item.text.getName());
            CustomTextView tvYa = (CustomTextView) dialog.findViewById(R.id.tvOK);
            CustomTextView tvTidak = (CustomTextView) dialog.findViewById(R.id.tvBack);
            final CustomEditText etQty = (CustomEditText) dialog.findViewById(R.id.etQty);
            final CustomEditText etPrice = (CustomEditText) dialog.findViewById(R.id.etPrice);
            final CustomEditText etDesc = (CustomEditText) dialog.findViewById(R.id.etDesc);



//            }
            etPrice.setText(item.text.getPrice());
            Log.d("TAG","nilai " + nilainya);

            etDesc.setText(item.text.getDescription());

            Log.d("TAG","cn " + currencyName);

            tvYa.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
                    bundle.putString("name", item.text.getName());
                    bundle.putString("id", item.text.getDetail_id());
                    bundle.putString("qty", etQty.getText().toString());
                    bundle.putString("unit", item.text.getUnit());
                    bundle.putString("price", etPrice.getText().toString());
                    bundle.putString("desc", etDesc.getText().toString());
                    callback.onProductDialogCallback(bundle);

                    dialog.dismiss();
                    dismiss();
                }
            });
            tvTidak.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    dialog.dismiss();
                }
            });
            dialog.show();
        }
    }
}
