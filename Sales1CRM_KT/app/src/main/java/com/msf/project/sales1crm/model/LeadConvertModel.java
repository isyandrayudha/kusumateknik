package com.msf.project.sales1crm.model;

public class LeadConvertModel {
    private String account_id;
    private String account_name;
    private String contact;
    private String lead_id;
    private String convert_status;

    public String getAccount_id() {
        return account_id;
    }

    public void setAccount_id(String account_id) {
        this.account_id = account_id;
    }

    public String getAccount_name() {
        return account_name;
    }

    public void setAccount_name(String account_name) {
        this.account_name = account_name;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getLead_id() {
        return lead_id;
    }

    public void setLead_id(String lead_id) {
        this.lead_id = lead_id;
    }

    public String getConvert_status() {
        return convert_status;
    }

    public void setConvert_status(String convert_status) {
        this.convert_status = convert_status;
    }
}
