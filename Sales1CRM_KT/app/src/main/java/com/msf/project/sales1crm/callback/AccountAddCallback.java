package com.msf.project.sales1crm.callback;

/**
 * Created by christianmacbook on 26/05/18.
 */

public interface AccountAddCallback {
    void finishAdd(String result, String response);
    void finishEdit(String result, String response);
}
