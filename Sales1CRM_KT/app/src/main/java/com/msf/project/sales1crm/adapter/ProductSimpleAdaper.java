package com.msf.project.sales1crm.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.model.Product;

import java.util.ArrayList;
import java.util.List;

public class ProductSimpleAdaper extends RecyclerView.Adapter<ProductSimpleAdaper.ViewHolder> {

    private List<Product> list = new ArrayList<>();
    private Context context;

    public ProductSimpleAdaper(Context context, List<Product> obj) {
        this.context = context;
        this.list = obj;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_simple_product, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.brandName.setText(list.get(position).getBrand());
        holder.typeName.setText(list.get(position).getType());
        holder.serialNumber.setText(list.get(position).getSerialNumber());
        holder.condition.setText(list.get(position).getCondition());
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CustomTextView brandName;
        CustomTextView typeName;
        CustomTextView serialNumber;
        CustomTextView condition;
        LinearLayout llCon;

        public ViewHolder(View itemView) {
            super(itemView);
            brandName = itemView.findViewById(R.id.brandName);
            typeName = itemView.findViewById(R.id.typeName);
            serialNumber = itemView.findViewById(R.id.serialNumber);
            condition = itemView.findViewById(R.id.condition);


        }
    }
}
