package com.msf.project.sales1crm.callback;

import android.os.Bundle;

public interface AccountDialogCallback {
    void onAccountDialogCallback(Bundle data);
}
