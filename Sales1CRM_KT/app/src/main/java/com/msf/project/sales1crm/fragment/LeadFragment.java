package com.msf.project.sales1crm.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.github.clans.fab.FloatingActionButton;
import com.msf.project.sales1crm.ErrorAPIKeyActivity;
import com.msf.project.sales1crm.LeadAddActivity;
import com.msf.project.sales1crm.LeadDetailActivity;
import com.msf.project.sales1crm.MenuActivity;
import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.adapter.LeadListAdapter;
import com.msf.project.sales1crm.callback.DashboardCallback;
import com.msf.project.sales1crm.callback.LeadListCallback;
import com.msf.project.sales1crm.callback.SortDialogCallback;
import com.msf.project.sales1crm.customview.CustomEditText;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.imageloader.ImageLoader;
import com.msf.project.sales1crm.model.AbsenceModel;
import com.msf.project.sales1crm.model.DashboardFirstModel;
import com.msf.project.sales1crm.model.DashboardSecondModel;
import com.msf.project.sales1crm.model.DashboardThirdModel;
import com.msf.project.sales1crm.model.LeadModel;
import com.msf.project.sales1crm.model.TotalOpportunityModel;
import com.msf.project.sales1crm.presenter.DashboardPresenter;
import com.msf.project.sales1crm.presenter.LeadListPresenter;
import com.msf.project.sales1crm.service.GPSTracker;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by christianmacbook on 30/05/18.
 */

public class LeadFragment extends Fragment implements LeadListCallback, DashboardCallback {

    @BindView(R.id.rlNoConnection)
    RelativeLayout rlNoConnection;

    @BindView(R.id.tvRefresh)
    CustomTextView tvRefresh;

    @BindView(R.id.etSearch)
    CustomEditText etSearch;

    @BindView(R.id.ivClear)
    ImageView ivClear;

    @BindView(R.id.lvLead)
    ListView lvLead;

    @BindView(R.id.lvLeadSwipe)
    SwipeRefreshLayout lvLeadSwipe;

    @BindView(R.id.faLeadAdd)
    FloatingActionButton faLeadAdd;

    @BindView(R.id.llNotFound)
    LinearLayout llNotFound;

    //    @BindView(R.id.rlProgress)
//    RelativeLayout rlProgress;
    @BindView(R.id.shimmerLayout)
    ShimmerFrameLayout shimmerLayout;
    Unbinder unbinder;
    @BindView(R.id.llshim)
    LinearLayout llshim;

//    @BindView(R.id.rlSort)
//    RelativeLayout rlSort;
//
//    @BindView(R.id.ivSelectSort)
//    ImageView ivSelectSort;
//
//    @BindView(R.id.rlFilter)
//    RelativeLayout rlFilter;

    private View view;
    private Context context;
    private LeadListAdapter adapter;
    private LeadListPresenter presenter;
    private List<LeadModel> list = new ArrayList<>();
    private String stringLatitude, stringLongitude;
    private DashboardPresenter dashboardPresenter;
    private String[] sort = {"Name A to Z", "Name Z to A", "Company A to Z", "Company Z to A"};

    private int prevSize = 0, sort_id = 0;
    private boolean isLoadMore = true;
    private boolean isMaxSize = true;
    private boolean pause = false;

    public static LeadFragment newInstance() {
        LeadFragment fragment = new LeadFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_lead, container, false);

        setHasOptionsMenu(true);
        this.context= getActivity();
        ButterKnife.bind(this, this.view);
        this.context = getActivity();
        return this.view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        ButterKnife.bind(this, view);
        this.context = getActivity();
        this.presenter = new LeadListPresenter(context, this);
        this.dashboardPresenter = new DashboardPresenter(context, this);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Leads");

        initData();

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
//            case R.id.action_list:
//                    MenuActivity.getInstance().setFragment(LeadFragment.newInstance());
//                    item.setIcon(R.drawable.ic_list_white_24dp);
//                break;
//            case R.id.action_cards:
//                    MenuActivity.getInstance().setFragment(LeadCardFragment.newInstance());
//                    item.setIcon(R.drawable.ic_cards_black_24dp);
//                break;
//            case R.id.action_maps:
//                    MenuActivity.getInstance().setFragment(LeadMapStreetFragment.newInstance());
//                    item.setIcon(R.drawable.ic_map_white_24dp);
//                break;
            case R.id.action_filter:
                SortDialogFragment DialogFragment = new SortDialogFragment(context, new SortDialogCallback() {

                    @Override
                    public void onDialogCallback(Bundle data) {
                        llshim.setVisibility(View.VISIBLE);
                        shimmerLayout.setVisibility(View.VISIBLE);
                        shimmerLayout.startShimmer();
                        sort_id = Integer.parseInt(data.getString("id"));

                        getDataFromAPI(0, sort_id);
//                            ivSelectSort.setVisibility(View.VISIBLE);
                    }
                }, sort);
                DialogFragment.show(getActivity().getSupportFragmentManager(), "sortDialog");
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initData() {
        Sales1CRMUtils.Utils.imageLoader = new ImageLoader(context);
        etSearch.clearFocus();
        lvLead.setOnScrollListener(new ListLazyLoad());
        lvLeadSwipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                llshim.setVisibility(View.VISIBLE);
                shimmerLayout.setVisibility(View.VISIBLE);
                shimmerLayout.startShimmer();
                getDataFromAPI(0, sort_id);
            }
        });

        //Set Seach Function
        setTextWatcherForSearch();
        getDataFromAPI(0, sort_id);

        ivClear.setOnClickListener(click);
        tvRefresh.setOnClickListener(click);

    }

    private void setTextWatcherForSearch() {
        etSearch.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    if (etSearch.getText().length() == 0) {
                        ivClear.setVisibility(View.VISIBLE);
                    }
//                } else {
//                    if (etSearch.getText().length() == 0) {
//                        ivClear.setVisibility(View.GONE);
//                    } else {
//                        ivClear.setVisibility(View.VISIBLE);
//                    }

                }
            }
        });

        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    llshim.setVisibility(View.VISIBLE);
                    shimmerLayout.setVisibility(View.VISIBLE);
                    shimmerLayout.startShimmer();
                    getDataFromAPI(0, sort_id);
                }

                return true;
            }
        });
    }

    private void getDataFromAPI(int prev, int sort_id) {
        isMaxSize = true;
        isLoadMore = true;

        llNotFound.setVisibility(View.GONE);

        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(context)) {
            GPSTracker gpsTracker = new GPSTracker(context);
            if (gpsTracker.canGetLocation()) {
                stringLatitude = String.valueOf(gpsTracker.latitude);
                stringLongitude = String.valueOf(gpsTracker.longitude);

                pause = false;
                dashboardPresenter.setupFirst(ApiParam.API_005);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        presenter.setupList(ApiParam.API_030, prev, sort_id, etSearch.getText().toString(), stringLatitude, stringLongitude);
                    }
                },1000);
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gpsTracker.showSettingsAlert();
                lvLeadSwipe.setRefreshing(false);
            }
        } else {
            lvLeadSwipe.setVisibility(View.GONE);
            rlNoConnection.setVisibility(View.VISIBLE);
        }
    }


    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.ivClear:
                    etSearch.setText("");
                    etSearch.clearFocus();
                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);
                    getDataFromAPI(0, sort_id);
                    break;
                case R.id.tvRefresh:
                    llshim.setVisibility(View.VISIBLE);
                    rlNoConnection.setVisibility(View.GONE);
                    getDataFromAPI(0, sort_id);
                    break;
                default:
                    break;
            }
        }
    };

    private void setList() {
        llshim.setVisibility(View.VISIBLE);
        shimmerLayout.setVisibility(View.GONE);
        adapter = new LeadListAdapter(context, this.list);
        List<LeadListAdapter.Row> rows = new ArrayList<LeadListAdapter.Row>();
        for (LeadModel leadModel : list) {
            //Read List by Row to insert into List Adapter
            rows.add(new LeadListAdapter.Item(leadModel));
        }
        //Set Row Adapter
        adapter.setRows(rows);
        lvLead.setAdapter(adapter);
        lvLead.setOnItemClickListener(new AdapterOnItemClick());
        lvLead.deferNotifyDataSetChanged();
        llshim.setVisibility(View.INVISIBLE);
        shimmerLayout.stopShimmer();
        shimmerLayout.setVisibility(View.INVISIBLE);
    }

    private void setMoreList() {
        List<LeadListAdapter.Row> rows = new ArrayList<LeadListAdapter.Row>();
        for (LeadModel country : list) {
            // Add the country to the list
            rows.add(new LeadListAdapter.Item(country));
        }
        adapter.setRows(rows);
        adapter.notifyDataSetChanged();

    }

    @Override
    public void finishLeadList(String result, List<LeadModel> list) {
        lvLeadSwipe.setRefreshing(false);
        if (result.equalsIgnoreCase("OK")) {
            prevSize = list.size();
            this.list = list;

            isMaxSize = false;
            isLoadMore = false;

            setList();
            if (list.size() > 0) {
                llNotFound.setVisibility(View.GONE);
            } else {
                llNotFound.setVisibility(View.VISIBLE);
            }
        } else if (result.equalsIgnoreCase("FAILED_KEY")) {
            startActivity(new Intent(context, ErrorAPIKeyActivity.class));
            MenuActivity.getInstance().finish();
        } else {
//            lvLeadSwipe.setVisibility(View.GONE);
            rlNoConnection.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void finishLeadMoreList(String result, List<LeadModel> list) {
        if (result.equals("OK")) {
            for (int i = 0; i < list.size(); i++) {
                this.list.add(list.get(i));
            }

            if (this.prevSize == this.list.size())
                isMaxSize = true;
            this.prevSize = this.list.size();
            setMoreList();
        }
        this.isLoadMore = false;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void finishFirst(String result, DashboardFirstModel model) {
        if(model.getLead_create_permission().equalsIgnoreCase("1")){
            faLeadAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(context, LeadAddActivity.class));
                }
            });
            faLeadAdd.setColorNormal(R.color.BlueCRM);
            faLeadAdd.setColorPressed(R.color.colorPrimaryDark);
            faLeadAdd.setColorPressedResId(R.color.colorPrimaryDark);
            faLeadAdd.setColorNormalResId(R.color.BlueCRM);
        } else {
            faLeadAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
                }
            });
            faLeadAdd.setColorNormal(R.color.BlueCRM_light);
            faLeadAdd.setColorPressed(R.color.colorPrimaryDark_light);
            faLeadAdd.setColorPressedResId(R.color.colorPrimaryDark_light);
            faLeadAdd.setColorNormalResId(R.color.BlueCRM_light);
        }
    }

    @Override
    public void finishSecond(String result, DashboardSecondModel model) {

    }

    @Override
    public void finishThird(String result, List<DashboardThirdModel> listLead, List<DashboardThirdModel> listLead1, List<DashboardThirdModel> listOppor, List<DashboardThirdModel> listOppor1) {

    }

    @Override
    public void TotOpportunity(String result, TotalOpportunityModel totOporModel) {

    }

    @Override
    public void absence(String result, AbsenceModel model) {

    }

    private class ListLazyLoad implements AbsListView.OnScrollListener {

        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {

        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            if (firstVisibleItem + visibleItemCount == totalItemCount && totalItemCount != 0) {
                Log.d("TAG", "isLoadMore : " + isLoadMore);
                if (isLoadMore == false && isMaxSize == false) {
                    isLoadMore = true;
                    prevSize = list.size();

                    presenter.setupMoreList(ApiParam.API_030, prevSize, sort_id, etSearch.getText().toString(), stringLatitude, stringLongitude);
                }
            }
        }
    }

    private class AdapterOnItemClick implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            // TODO Auto-generated method stub
            LeadListAdapter.Item item = (LeadListAdapter.Item) lvLead.getAdapter().getItem(position);
            switch (view.getId()) {
                default:
                        Intent intent = new Intent(context, LeadDetailActivity.class);
                        intent.putExtra("leadModel", item.getItem());
                        startActivity(intent);
                        break;
            }
        }
    }

    @Override
    public void onPause() {
        pause = true;
        super.onPause();
    }

    @Override
    public void onResume() {
        if (pause) {
            getDataFromAPI(0, sort_id);
        }
        super.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
