package com.msf.project.sales1crm.callback;

import com.msf.project.sales1crm.model.SalesCycleModel;
import com.msf.project.sales1crm.model.Top10CustomerModel;
import com.msf.project.sales1crm.model.TotalModel;

import java.util.List;

public interface GeneralDashboardCallback {
    void finished(String result, SalesCycleModel model, List<TotalModel> wonIndustry, List<TotalModel> loseIndustry, List<TotalModel> wonSource, List<TotalModel> loseSource, List<Top10CustomerModel> listTop10);
}
