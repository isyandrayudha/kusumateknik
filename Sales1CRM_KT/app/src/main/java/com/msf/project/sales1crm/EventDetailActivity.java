package com.msf.project.sales1crm;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.msf.project.sales1crm.adapter.ProductSimpleAdaper;
import com.msf.project.sales1crm.callback.DashboardCallback;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.database.MeeToSelectDao;
import com.msf.project.sales1crm.database.ProductDao;
import com.msf.project.sales1crm.model.AbsenceModel;
import com.msf.project.sales1crm.model.ContactModel;
import com.msf.project.sales1crm.model.DashboardFirstModel;
import com.msf.project.sales1crm.model.DashboardSecondModel;
import com.msf.project.sales1crm.model.DashboardThirdModel;
import com.msf.project.sales1crm.model.EventModel;
import com.msf.project.sales1crm.model.Product;
import com.msf.project.sales1crm.model.TotalOpportunityModel;
import com.msf.project.sales1crm.presenter.DashboardPresenter;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.PreferenceUtility;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EventDetailActivity extends BaseActivity implements DashboardCallback {

    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.tvEventNameTitle)
    CustomTextView tvEventNameTitle;
    @BindView(R.id.ivEdit)
    ImageView ivEdit;
    @BindView(R.id.tvCreateDate)
    CustomTextView tvCreateDate;
    @BindView(R.id.tvEventName)
    CustomTextView tvEventName;
    @BindView(R.id.tvAccountName)
    CustomTextView tvAccountName;
    @BindView(R.id.tvDate)
    CustomTextView tvDate;
    @BindView(R.id.tvAssignTo)
    CustomTextView tvAssignTo;
    @BindView(R.id.tvMeetTo)
    CustomTextView tvMeetTo;
    @BindView(R.id.tvFollowUp)
    CustomTextView tvFollowUp;
    @BindView(R.id.tvDescription)
    CustomTextView tvDesc;
    @BindView(R.id.image_1)
    ImageView image1;
    @BindView(R.id.image_2)
    ImageView image2;
    @BindView(R.id.image_3)
    ImageView image3;
    @BindView(R.id.RcProduct)
    RecyclerView RcProduct;
    @BindView(R.id.tvNoDataProduct)
    CustomTextView tvNoDataProduct;
    @BindView(R.id.llcam1)
    LinearLayout llcam1;
    @BindView(R.id.llcam2)
    LinearLayout llcam2;
    @BindView(R.id.llcam3)
    LinearLayout llcam3;
    @BindView(R.id.parentCam)
    LinearLayout parentCam;
    private Context context;
    private EventModel eModel;
    static EventDetailActivity eventDetailActivity;
    private String url = "";
    private List<Product> productList = new ArrayList<>();
    private ProductSimpleAdaper productSimpleAdaper;
    private DashboardPresenter dashboardPresenter;

    public static EventDetailActivity getInstance() {
        return eventDetailActivity;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eventdetail);

        ButterKnife.bind(this);
        this.context = this;
        this.dashboardPresenter = new DashboardPresenter(context, this);
        eventDetailActivity = this;
        this.url = PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.URL);

        try {
            eModel = getIntent().getExtras().getParcelable("eventModel");
        } catch (Exception e) {
        }

        initView();
    }

    private void initView() {
        dashboardPresenter.setupFirst(ApiParam.API_005);
        tvEventNameTitle.setText(eModel.getName());
        tvEventName.setText(eModel.getName());
        tvAccountName.setText(eModel.getAccount_name());
        tvCreateDate.setText("Created at " + eModel.getCreated_at());
        tvDate.setText(eModel.getDate_interval());
        tvAssignTo.setText(eModel.getAssignto_name());
        tvDesc.setText(eModel.getDescription());
        tvFollowUp.setText(eModel.getFollow_up());

        try {
            JSONArray array = new JSONArray(eModel.getProduct());

            for (int i = 0; i < array.length(); i++) {
                Product product = new Product();
                product.setId(array.getJSONObject(i).getString("id"));
                product.setBrand(array.getJSONObject(i).getString("brand"));
                product.setType(array.getJSONObject(i).getString("type"));
                product.setSerialNumber(array.getJSONObject(i).getString("serial_number"));
                product.setCondition(array.getJSONObject(i).getString("condition"));
                product.setStatus(array.getJSONObject(i).getString("status"));
                productList.add(product);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        try {
            JSONArray array = new JSONArray(eModel.getMeet_to());
            String meetTo = "";
            for (int i = 0; i < array.length(); i++) {

                meetTo = meetTo + array.getJSONObject(i).getString("name");
                if (i != array.length() - 1) {
                    meetTo = meetTo + ", ";

                }
            }
            tvMeetTo.setText(meetTo);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        if (productList.size() > 0) {
            setProductList();
        } else {
            RcProduct.setVisibility(View.GONE);
            tvNoDataProduct.setVisibility(View.VISIBLE);
        }

        if (eModel.getNama1().equalsIgnoreCase("")) {
            llcam1.setVisibility(View.GONE);
        } else {
            Sales1CRMUtils.Utils.imageLoader.DisplayImage(url + eModel.getPhoto_1(),
                    image1, Bitmap.Config.ARGB_8888, 700, Sales1CRMUtils.TYPE_RECT);
        }

        if (eModel.getNama2().equalsIgnoreCase("")) {
            llcam2.setVisibility(View.GONE);
        } else {
            Sales1CRMUtils.Utils.imageLoader.DisplayImage(url + eModel.getPhoto_2(),
                    image2, Bitmap.Config.ARGB_8888, 700, Sales1CRMUtils.TYPE_RECT);
        }

        if (eModel.getNama3().equalsIgnoreCase("")) {
            llcam3.setVisibility(View.GONE);
        } else {
            Sales1CRMUtils.Utils.imageLoader.DisplayImage(url + eModel.getPhoto_3(),
                    image3, Bitmap.Config.ARGB_8888, 700, Sales1CRMUtils.TYPE_RECT);
        }

        if(eModel.getNama1().equalsIgnoreCase("") && eModel.getNama2().equalsIgnoreCase("") && eModel.getNama3().equalsIgnoreCase("")){
            parentCam.setVisibility(View.GONE);
        }

        ivBack.setOnClickListener(click);
        ivEdit.setOnClickListener(click);
    }

    private void setProductList() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        RcProduct.setLayoutManager(layoutManager);
        productSimpleAdaper = new ProductSimpleAdaper(context, this.productList);
        RcProduct.setAdapter(productSimpleAdaper);
    }

    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.ivBack:
                    finish();
                    break;
            }
        }
    };

    @Override
    public void finishFirst(String result, DashboardFirstModel model) {
        if(model.getLead_edit_permission().equalsIgnoreCase("1")){
            ivEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, EventAddActivity.class);

                    ProductDao productDao = new ProductDao(context);
                    MeeToSelectDao meeToSelectDao = new MeeToSelectDao(context);
//                    meeToSelectDao.deleteAllRecord();
//                    MeetToDao meetToDao = new MeetToDao(context);
//                    meetToDao.deleteAllRecord();

                    try {
                        JSONArray array = new JSONArray(eModel.getProduct());

                        for (int i = 0; i < array.length(); i++) {
                            Product product = new Product();
                            product.setId_temp(array.getJSONObject(i).getString("id"));
                            product.setId(array.getJSONObject(i).getString("id"));
                            product.setBrand(array.getJSONObject(i).getString("brand"));
                            product.setType(array.getJSONObject(i).getString("type"));
                            product.setSerialNumber(array.getJSONObject(i).getString("serial_number"));
                            product.setCondition(array.getJSONObject(i).getString("condition"));
                            product.setStatus(array.getJSONObject(i).getString("status"));
                            productDao.insertTable(product);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    try {
                        JSONArray array = new JSONArray(eModel.getMeet_to());
                        for (int i = 0; i < array.length(); i++) {
                            ContactModel contactModel = new ContactModel();
                            contactModel.setAccount_id(eModel.getAccount_id());
                            contactModel.setId(array.getJSONObject(i).getString("contact_id"));
                            contactModel.setFullname(array.getJSONObject(i).getString("name"));
                            contactModel.setIsCheck("1");
                            meeToSelectDao.insertTable(contactModel);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    intent.putExtra("eventModel", eModel);
                    intent.putExtra("from_detail", true);
                    startActivity(intent);
                }
            });
            ivEdit.setVisibility(View.VISIBLE);
        } else {
            ivEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
                }
            });
            ivEdit.setVisibility(View.GONE);
        }
    }

    @Override
    public void finishSecond(String result, DashboardSecondModel model) {

    }

    @Override
    public void finishThird(String result, List<DashboardThirdModel> listLead, List<DashboardThirdModel> listLead1, List<DashboardThirdModel> listOppor, List<DashboardThirdModel> listOppor1) {

    }

    @Override
    public void TotOpportunity(String result, TotalOpportunityModel totOporModel) {

    }

    @Override
    public void absence(String result, AbsenceModel model) {

    }
}
