package com.msf.project.sales1crm.presenter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;

import com.msf.project.sales1crm.callback.AccountAddCallback;
import com.msf.project.sales1crm.model.AccountAddressModel;
import com.msf.project.sales1crm.model.AccountModel;
import com.msf.project.sales1crm.service.NetworkConnection;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.PreferenceUtility;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;
import com.msf.project.sales1crm.utility.Sales1CRMUtilsJSON;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by christianmacbook on 26/05/18.
 */

public class AccountAddPresenter {
    private final String TAG = AccountAddPresenter.class.getSimpleName();
    private Context context;
    private Messenger messenger;
    private Message message;
    private Bundle bundle;
    private Handler handler;
    private String failureResponse = "";
    private String[] stringResponse = {""};
    private AccountAddCallback callback;
    private String resultLogin = "NG";
    private String response = "Tolong Periksa Koneksi Anda";

    public AccountAddPresenter(Context context, AccountAddCallback listener) {
        this.context = context;
        this.callback = listener;
    }

    private void doNetworkService(String url, String params, String from) {
        Log.d(TAG, "url login " + url);
        Log.d(TAG, "params : " + params);
        Intent networkIntent = new Intent(this.context, NetworkConnection.class);
        this.messenger = new Messenger(this.handler);
        networkIntent.putExtra("messenger", this.messenger);
        networkIntent.putExtra("url", url);
        networkIntent.putExtra("params", params);
        networkIntent.putExtra("from", from);
        this.context.startService(networkIntent);
    }

    public void setupAdd(int apiIndex, AccountModel accountModel, AccountAddressModel billingModel, AccountAddressModel shippingModel) {
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)) {
            obtainAddAccount(apiIndex, accountModel, billingModel, shippingModel);
        }else{
            callback.finishAdd(this.resultLogin, this.response);
        }
    }

    public void obtainAddAccount(int apiIndex, AccountModel accountModel, AccountAddressModel billingModel, AccountAddressModel shippingModel) {
        this.handler = new Handler(this.context.getMainLooper()) {

            @Override
            public void handleMessage(Message msg) {
                parseAddAccount(msg);
            }

        };

        JSONObject jsonObject = new JSONObject();
        try {
            //Umum
            jsonObject.put("api_key", PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.API_KEY));
            jsonObject.put("assign_to", accountModel.getAssign_to());
            jsonObject.put("type", accountModel.getType_id());
            jsonObject.put("industry", accountModel.getIndustry_id());
            jsonObject.put("create_date", accountModel.getCreated_date());
            jsonObject.put("name", accountModel.getAccount_name());
            jsonObject.put("description", accountModel.getDescription());
            jsonObject.put("email", accountModel.getEmail());
            jsonObject.put("website", accountModel.getWebsite());
            jsonObject.put("total_employee", accountModel.getTotal_employee());
            jsonObject.put("phone", accountModel.getPhone());
            jsonObject.put("image_master", accountModel.getBase64Image());
            //Alamat Penagihan
            jsonObject.put("billing_alamat", billingModel.getAddress());
            jsonObject.put("billing_provinsi", billingModel.getProvinsi_id());
            jsonObject.put("billing_kabupaten", billingModel.getKabupaten_id());
            jsonObject.put("billing_kecamatan", billingModel.getKecamatan_id());
            jsonObject.put("billing_kelurahan", billingModel.getKelurahan_id());
            jsonObject.put("billing_latitude",billingModel.getLatitude());
            jsonObject.put("billing_longitude",billingModel.getLongitude());
            //Alamat Pengiriman
            jsonObject.put("shipping_alamat", shippingModel.getAddress());
            jsonObject.put("shipping_provinsi", shippingModel.getProvinsi_id());
            jsonObject.put("shipping_kabupaten", shippingModel.getKabupaten_id());
            jsonObject.put("shipping_kecamatan", shippingModel.getKecamatan_id());
            jsonObject.put("shipping_kelurahan", shippingModel.getKelurahan_id());
            jsonObject.put("shipping_latitude",shippingModel.getLatitude());
            jsonObject.put("shipping_longitude",shippingModel.getLongitude());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        doNetworkService(ApiParam.urlBuilder(this.context, apiIndex), jsonObject.toString(), "AccountAdd");
    }

    private void parseAddAccount(Message message) {
        this.message = message;
        this.bundle = this.message.getData();
        this.stringResponse[0] = this.bundle.getString("network_response");
        this.failureResponse = this.bundle.getString("network_failure");
        Log.d(TAG, "responseString[0] info " + this.stringResponse[0]);
        if (this.failureResponse.equalsIgnoreCase("yes")){
            this.callback.finishAdd(this.resultLogin, this.response);
        }else{
            try {
                this.resultLogin = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                this.callback.finishAdd(this.resultLogin, this.response);
            } catch (JSONException e) {
                Log.d(TAG, "Exception detail Response " + e.getMessage());
            }
        }
    }

    public void setupEdit(int apiIndex, AccountModel accountModel, AccountAddressModel billingModel, AccountAddressModel shippingModel) {
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)) {
            obtainEditAccount(apiIndex, accountModel, billingModel, shippingModel);
        }else{
            callback.finishEdit(this.resultLogin, this.response);
        }
    }

    public void obtainEditAccount(int apiIndex, AccountModel accountModel, AccountAddressModel billingModel, AccountAddressModel shippingModel) {
        this.handler = new Handler(this.context.getMainLooper()) {

            @Override
            public void handleMessage(Message msg) {
                parseEditAccount(msg);
            }

        };

        JSONObject jsonObject = new JSONObject();
        try {
            //Umum
            jsonObject.put("api_key", PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.API_KEY));
            jsonObject.put("assign_to", accountModel.getAssign_to());
            jsonObject.put("account_id", accountModel.getId());
            jsonObject.put("type", accountModel.getType_id());
            jsonObject.put("industry", accountModel.getIndustry_id());
            jsonObject.put("create_date", accountModel.getCreated_date());
            jsonObject.put("name", accountModel.getAccount_name());
            jsonObject.put("description", accountModel.getDescription());
            jsonObject.put("email", accountModel.getEmail());
            jsonObject.put("website", accountModel.getWebsite());
            jsonObject.put("total_employee", accountModel.getTotal_employee());
            jsonObject.put("phone", accountModel.getPhone());
            jsonObject.put("image_master", accountModel.getBase64Image());
            //Alamat Penagihan
            jsonObject.put("billing_alamat", billingModel.getAddress());
            jsonObject.put("billing_provinsi", billingModel.getProvinsi_id());
            jsonObject.put("billing_kabupaten", billingModel.getKabupaten_id());
            jsonObject.put("billing_kecamatan", billingModel.getKecamatan_id());
            jsonObject.put("billing_kelurahan", billingModel.getKelurahan_id());
            jsonObject.put("billing_latitude",billingModel.getLatitude());
            jsonObject.put("billing_longitude",billingModel.getLongitude());
            //Alamat Pengiriman
            jsonObject.put("shipping_alamat", shippingModel.getAddress());
            jsonObject.put("shipping_provinsi", shippingModel.getProvinsi_id());
            jsonObject.put("shipping_kabupaten", shippingModel.getKabupaten_id());
            jsonObject.put("shipping_kecamatan", shippingModel.getKecamatan_id());
            jsonObject.put("shipping_kelurahan", shippingModel.getKelurahan_id());
            jsonObject.put("shipping_latitude",shippingModel.getLatitude());
            jsonObject.put("shipping_longitude",shippingModel.getLongitude());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        doNetworkService(ApiParam.urlBuilder(this.context, apiIndex), jsonObject.toString(), "AccountEdit");
    }

    private void parseEditAccount(Message message) {
        this.message = message;
        this.bundle = this.message.getData();
        this.stringResponse[0] = this.bundle.getString("network_response");
        this.failureResponse = this.bundle.getString("network_failure");
        Log.d(TAG, "responseString[0] info " + this.stringResponse[0]);
        if (this.failureResponse.equalsIgnoreCase("yes")){
            this.callback.finishEdit(this.resultLogin, this.response);
        }else{
            try {
                this.resultLogin = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                this.callback.finishEdit(this.resultLogin, this.response);
            } catch (JSONException e) {
                Log.d(TAG, "Exception detail Response " + e.getMessage());
            }
        }
    }
}
