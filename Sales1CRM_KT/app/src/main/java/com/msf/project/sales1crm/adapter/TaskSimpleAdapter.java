package com.msf.project.sales1crm.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.model.TaskModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by christianmacbook on 23/05/18.
 */

public class TaskSimpleAdapter extends RecyclerView.Adapter<TaskSimpleAdapter.ViewHolder> {

    //Vars
    private List<TaskModel> list = new ArrayList<>();
    private Context context;

    public TaskSimpleAdapter(Context context, List<TaskModel> object) {
        this.context = context;
        this.list = object;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_simpletask, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tvName.setText(list.get(position).getSubject());
        holder.tvDate.setText(list.get(position).getDate());
        holder.tvDesc.setText(list.get(position).getDescription());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CustomTextView tvName, tvDate, tvDesc;

        public ViewHolder(View itemView) {
            super(itemView);

            tvName = itemView.findViewById(R.id.tvTaskName);
            tvDate = itemView.findViewById(R.id.tvDate);
            tvDesc = itemView.findViewById(R.id.tvDescription);
        }
    }
}
