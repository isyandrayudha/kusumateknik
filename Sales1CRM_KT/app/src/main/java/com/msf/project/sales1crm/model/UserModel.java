package com.msf.project.sales1crm.model;

/**
 * Created by christianmacbook on 11/05/18.
 */

public class UserModel {
    private String id;
    private String fullname;
    private String email;
    private String password;
    private String position;
    private String company;
    private String image_master;



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getImage_master() {
        return image_master;
    }

    public void setImage_master(String image_master) {
        this.image_master = image_master;
    }
}
