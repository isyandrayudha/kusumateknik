package com.msf.project.sales1crm.model;

public class DashboardFirstModel {
    private String count_account;
    private String count_lead;
    private String count_opportunity;
    private String count_activity;
    private String account_list_permission;
    private String lead_list_permission;
    private String opportunity_list_permission;
    private String activity_list_permission;
    private String account_create_permission;
    private String lead_create_permission ;
    private String opportunity_create_permission;
    private String crm_event_create_permission;
    private String crm_task_create_permission;
    private String crm_call_create_permission;
    private String database_machine_create_permission;
    private String account_edit_permission;
    private String lead_edit_permission;
    private String opportunity_edit_permission;
    private String database_machine_edit_permission;
    private String crm_event_edit_permission;
    private String crm_task_edit_permission;
    private String crm_call_edit_permission;
    private String lead_convert_to_account_permission;
    private String lead_convert_to_opportunity_permission;
    private String crm_task_list_permission;
    private String crm_event_list_permission;
    private String crm_call_list_permission;
    private String database_machine_list_permission;

    public String getCount_account() {
        return count_account;
    }

    public void setCount_account(String count_account) {
        this.count_account = count_account;
    }

    public String getCount_lead() {
        return count_lead;
    }

    public void setCount_lead(String count_lead) {
        this.count_lead = count_lead;
    }

    public String getCount_opportunity() {
        return count_opportunity;
    }

    public void setCount_opportunity(String count_opportunity) {
        this.count_opportunity = count_opportunity;
    }

    public String getCount_activity() {
        return count_activity;
    }

    public void setCount_activity(String count_activity) {
        this.count_activity = count_activity;
    }

    public String getAccount_list_permission() {
        return account_list_permission;
    }

    public void setAccount_list_permission(String account_list_permission) {
        this.account_list_permission = account_list_permission;
    }

    public String getLead_list_permission() {
        return lead_list_permission;
    }

    public void setLead_list_permission(String lead_list_permission) {
        this.lead_list_permission = lead_list_permission;
    }

    public String getOpportunity_list_permission() {
        return opportunity_list_permission;
    }

    public void setOpportunity_list_permission(String opportunity_list_permission) {
        this.opportunity_list_permission = opportunity_list_permission;
    }

    public String getActivity_list_permission() {
        return activity_list_permission;
    }

    public void setActivity_list_permission(String activity_list_permission) {
        this.activity_list_permission = activity_list_permission;
    }

    public String getAccount_create_permission() {
        return account_create_permission;
    }

    public void setAccount_create_permission(String account_create_permission) {
        this.account_create_permission = account_create_permission;
    }

    public String getLead_create_permission() {
        return lead_create_permission;
    }

    public void setLead_create_permission(String lead_create_permission) {
        this.lead_create_permission = lead_create_permission;
    }

    public String getOpportunity_create_permission() {
        return opportunity_create_permission;
    }

    public void setOpportunity_create_permission(String opportunity_create_permission) {
        this.opportunity_create_permission = opportunity_create_permission;
    }

    public String getCrm_event_create_permission() {
        return crm_event_create_permission;
    }

    public void setCrm_event_create_permission(String crm_event_create_permission) {
        this.crm_event_create_permission = crm_event_create_permission;
    }

    public String getCrm_task_create_permission() {
        return crm_task_create_permission;
    }

    public void setCrm_task_create_permission(String crm_task_create_permission) {
        this.crm_task_create_permission = crm_task_create_permission;
    }

    public String getCrm_call_create_permission() {
        return crm_call_create_permission;
    }

    public void setCrm_call_create_permission(String crm_call_create_permission) {
        this.crm_call_create_permission = crm_call_create_permission;
    }

    public String getDatabase_machine_create_permission() {
        return database_machine_create_permission;
    }

    public void setDatabase_machine_create_permission(String database_machine_create_permission) {
        this.database_machine_create_permission = database_machine_create_permission;
    }

    public String getAccount_edit_permission() {
        return account_edit_permission;
    }

    public void setAccount_edit_permission(String account_edit_permission) {
        this.account_edit_permission = account_edit_permission;
    }

    public String getLead_edit_permission() {
        return lead_edit_permission;
    }

    public void setLead_edit_permission(String lead_edit_permission) {
        this.lead_edit_permission = lead_edit_permission;
    }

    public String getOpportunity_edit_permission() {
        return opportunity_edit_permission;
    }

    public void setOpportunity_edit_permission(String opportunity_edit_permission) {
        this.opportunity_edit_permission = opportunity_edit_permission;
    }

    public String getDatabase_machine_edit_permission() {
        return database_machine_edit_permission;
    }

    public void setDatabase_machine_edit_permission(String database_machine_edit_permission) {
        this.database_machine_edit_permission = database_machine_edit_permission;
    }

    public String getCrm_event_edit_permission() {
        return crm_event_edit_permission;
    }

    public void setCrm_event_edit_permission(String crm_event_edit_permission) {
        this.crm_event_edit_permission = crm_event_edit_permission;
    }

    public String getCrm_task_edit_permission() {
        return crm_task_edit_permission;
    }

    public void setCrm_task_edit_permission(String crm_task_edit_permission) {
        this.crm_task_edit_permission = crm_task_edit_permission;
    }

    public String getCrm_call_edit_permission() {
        return crm_call_edit_permission;
    }

    public void setCrm_call_edit_permission(String crm_call_edit_permission) {
        this.crm_call_edit_permission = crm_call_edit_permission;
    }

    public String getLead_convert_to_account_permission() {
        return lead_convert_to_account_permission;
    }

    public void setLead_convert_to_account_permission(String lead_convert_to_account_permission) {
        this.lead_convert_to_account_permission = lead_convert_to_account_permission;
    }

    public String getLead_convert_to_opportunity_permission() {
        return lead_convert_to_opportunity_permission;
    }

    public void setLead_convert_to_opportunity_permission(String lead_convert_to_opportunity_permission) {
        this.lead_convert_to_opportunity_permission = lead_convert_to_opportunity_permission;
    }

    public String getCrm_task_list_permission() {
        return crm_task_list_permission;
    }

    public void setCrm_task_list_permission(String crm_task_list_permission) {
        this.crm_task_list_permission = crm_task_list_permission;
    }

    public String getCrm_event_list_permission() {
        return crm_event_list_permission;
    }

    public void setCrm_event_list_permission(String crm_event_list_permission) {
        this.crm_event_list_permission = crm_event_list_permission;
    }

    public String getCrm_call_list_permission() {
        return crm_call_list_permission;
    }

    public void setCrm_call_list_permission(String crm_call_list_permission) {
        this.crm_call_list_permission = crm_call_list_permission;
    }

    public String getDatabase_machine_list_permission() {
        return database_machine_list_permission;
    }

    public void setDatabase_machine_list_permission(String database_machine_list_permission) {
        this.database_machine_list_permission = database_machine_list_permission;
    }
}
