package com.msf.project.sales1crm.callback;

public interface DatabaseMachineAddCallback {
    void finishAdd (String result, String response);
    void finishEdit (String result, String response);
}
