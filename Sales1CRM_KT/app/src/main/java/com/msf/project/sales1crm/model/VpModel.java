package com.msf.project.sales1crm.model;

public class VpModel{
	private String totalOpportunity;
	private String sizeOpportunity;
	private String stageId;
	private String stageName;
	private String percentage;
	private String opportunities;
	private String total_opportunity_size;


	public String getTotalOpportunity() {
		return totalOpportunity;
	}

	public void setTotalOpportunity(String totalOpportunity) {
		this.totalOpportunity = totalOpportunity;
	}

	public String getSizeOpportunity() {
		return sizeOpportunity;
	}

	public void setSizeOpportunity(String sizeOpportunity) {
		this.sizeOpportunity = sizeOpportunity;
	}

	public String getStageId() {
		return stageId;
	}

	public void setStageId(String stageId) {
		this.stageId = stageId;
	}

	public String getStageName() {
		return stageName;
	}

	public void setStageName(String stageName) {
		this.stageName = stageName;
	}

	public String getPercentage() {
		return percentage;
	}

	public void setPercentage(String percentage) {
		this.percentage = percentage;
	}

	public String getOpportunities() {
		return opportunities;
	}

	public void setOpportunities(String opportunities) {
		this.opportunities = opportunities;
	}

	public String getTotal_opportunity_size() {
		return total_opportunity_size;
	}

	public void setTotal_opportunity_size(String total_opportunity_size) {
		this.total_opportunity_size = total_opportunity_size;
	}
}