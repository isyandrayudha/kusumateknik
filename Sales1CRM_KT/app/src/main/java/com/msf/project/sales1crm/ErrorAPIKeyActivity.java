package com.msf.project.sales1crm;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import butterknife.ButterKnife;
import butterknife.BindView;


public class ErrorAPIKeyActivity extends BaseActivity {

    @BindView(R.id.tvOK)
    CustomTextView tvOK;

    private Context context;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_errorapikey);

        ButterKnife.bind(this);
        this.context = this;

        initView();
    }

    private void initView(){
        Sales1CRMUtils.PreferenceShared.LogoutUser(context);

        tvOK.setOnClickListener(click);
    }

    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.tvOK:
                    Intent intent = new Intent(context, LoginActivity.class);
                    startActivity(intent);
                    finish();
                    break;
                default:
                    break;
            }
        }
    };
}
