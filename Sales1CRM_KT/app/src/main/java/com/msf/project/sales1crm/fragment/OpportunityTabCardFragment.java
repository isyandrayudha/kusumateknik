package com.msf.project.sales1crm.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.msf.project.sales1crm.MenuActivity;
import com.msf.project.sales1crm.OpportunityTabCardAdapter;
import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.callback.SortDialogCallback;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.customview.NonSwipeableViewPager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class OpportunityTabCardFragment extends Fragment {

    interface ClickActionFilterListener{
        void onClickItemFilter(Integer id);
    }

    private ClickActionFilterListener mFilterLisner;

    public void setClickActionFilterLisner(ClickActionFilterListener lisner) {
        this.mFilterLisner =lisner;
    }


    @BindView(R.id.tabOppor)
    TabLayout tabOppor;
    @BindView(R.id.vpOpportunity)
    NonSwipeableViewPager vpOpportunity;
    Unbinder unbinder;


    private View view;
    private Context context;
    private OpportunityTabCardAdapter adapter;
    private MenuActivity myContext;
    private String[] sort = {"Name A to Z", "Name Z to A", "Opportunity Size A to Z", "Opportunity Size Z to A" ,
            "Create Date Ascending", "Create Date Descending", "Due Date Ascending", "Due Date Descending"};

    private int prevSize = 0, sort_id = 0;

    public static OpportunityTabCardFragment newInstance() {
        OpportunityTabCardFragment fragment = new OpportunityTabCardFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onAttach(Activity activity) {
        myContext = (MenuActivity) activity;
        super.onAttach(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_opportunitytabcard, container, false);
        setHasOptionsMenu(true);
        unbinder = ButterKnife.bind(this, this.view);
        this.context = getActivity();
        return this.view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);
        this.context = getActivity();

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Opportunity");

        initData();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_oppor, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_list:
                    MenuActivity.getInstance().setFragment(OpportunityTabFragment.newInstance());
                    item.setIcon(R.drawable.ic_list_white_24dp);
                break;
            case R.id.action_cards:
                    MenuActivity.getInstance().setFragment(OpportunityTabCardFragment.newInstance());
                    item.setIcon(R.drawable.ic_cards_black_24dp);
                break;
            case R.id.action_maps:
                    MenuActivity.getInstance().setFragment(OpportunityTabMapFragment.newInstance());
                    item.setIcon(R.drawable.ic_map_white_24dp);
                break;
            case R.id.action_viewpipeline:
                    MenuActivity.getInstance().setFragment(Vp_opportunityTab.newInstance());
                break;
            case R.id.action_filter:
                SortDialogFragment dialogFragment = new SortDialogFragment(context, new SortDialogCallback() {
                    @Override
                    public void onDialogCallback(Bundle data) {
                        sort_id = Integer.parseInt(data.getString("id"));
                       mFilterLisner.onClickItemFilter(sort_id);
                    }
                }, sort);
                dialogFragment.show(getActivity().getSupportFragmentManager(), "sortDialog");
                break;
            default:
                return super.onOptionsItemSelected(item);

        }
        return true;
    }

    private void initData() {
        CustomTextView tabOne = (CustomTextView) LayoutInflater.from(context).inflate(R.layout.custom_tab, null);
        tabOne.setText("Active");
        tabOppor.addTab(tabOppor.newTab().setCustomView(tabOne));

        CustomTextView tabTwo = (CustomTextView) LayoutInflater.from(context).inflate(R.layout.custom_tab, null);
        tabTwo.setText("Close");
        tabOppor.addTab(tabOppor.newTab().setCustomView(tabTwo));

        tabOppor.setTabGravity(tabOppor.GRAVITY_FILL);

        adapter = new OpportunityTabCardAdapter(myContext.getSupportFragmentManager(), tabOppor.getTabCount(), this);
        vpOpportunity.setAdapter(adapter);
        vpOpportunity.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabOppor));
        tabOppor.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                vpOpportunity.setCurrentItem(tab.getPosition(), true);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
//                vpOpportunity.setCurrentItem(tab.getPosition());

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
