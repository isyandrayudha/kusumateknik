package com.msf.project.sales1crm.callback;

import com.msf.project.sales1crm.model.LocationUserModel;

import java.util.List;

public interface OfflineLocationCallback {
    void finishLocationUserDB(List<LocationUserModel> locationUser);
    void finishLocationUserList(String result, int count, int total);
}
