package com.msf.project.sales1crm;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.msf.project.sales1crm.callback.UploadImageCallback;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.customview.RoundedImageView;
import com.msf.project.sales1crm.imageloader.ImageCompression;
import com.msf.project.sales1crm.model.UploadImageModel;
import com.msf.project.sales1crm.model.UserModel;
import com.msf.project.sales1crm.presenter.LoginPresenter;
import com.msf.project.sales1crm.presenter.UploadImageProfilePresenter;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.PreferenceUtility;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChangeFotoProfileActivity extends BaseActivity implements UploadImageCallback{

    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.ivAccountPicture)
    RoundedImageView ivAccountPicture;
    @BindView(R.id.tvFullname)
    CustomTextView tvFullname;
    @BindView(R.id.Position)
    CustomTextView Position;
    @BindView(R.id.tvCompany)
    CustomTextView tvCompany;
    @BindView(R.id.tvSubmit)
    CustomTextView tvSubmit;

    private Context context;
    private UploadImageProfilePresenter presenter;
    private LoginPresenter loginPresenter;
    private String url= "";
    private UploadImageModel model;
    private String photoPath= "";
    private String filePath= "";
    private String photo= "";
    private Bitmap savedBitmap;

    static ChangeFotoProfileActivity changeFotoProfileActivity;

    public static ChangeFotoProfileActivity getInstance(){
        return changeFotoProfileActivity;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Sales1CRMUtils.GALLERY:
                if (resultCode == RESULT_OK) {
                    Uri selecctedImageUri = data.getData();
                    BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
                    bmpFactoryOptions.inJustDecodeBounds = false;
                    bmpFactoryOptions.inSampleSize = 4;
                    Bitmap bmp = null;
                    try {
                        bmp = Sales1CRMUtils.Utils.scaleImage(context, selecctedImageUri);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    savedBitmap = bmp.copy(bmp.getConfig(), bmp.isMutable() ? true
                            : false);

                    ivAccountPicture.setImageBitmap(Sales1CRMUtils.Utils.getroundBitmap(bmp));
                    photo = Sales1CRMUtils.ImageUtility.encodeToBase64(bmp, Bitmap.CompressFormat.JPEG, 70);

                    bmp.recycle();
                    bmp = null;
                }
                break;
            case Sales1CRMUtils.CAMERA_REQUEST:
                if (resultCode == RESULT_OK) {
                    BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
                    bmpFactoryOptions.inJustDecodeBounds = false;
                    bmpFactoryOptions.inSampleSize = 4;

                    Log.d("TAG", "Photo Path : " + photoPath);

                    // imageFilePath image path which you pass withintent
                    Bitmap bmp = BitmapFactory.decodeFile(photoPath,
                            bmpFactoryOptions);
                    // BitmapFactory.Options options = new BitmapFactory.Options();
                    // options.inSampleSize = 4;
                    Log.i("HUAI", "data : " + bmp);
                    // Bitmap photo = (Bitmap) data.getExtras().get("data");
                    if (bmp.getWidth() >= bmp.getHeight()) {

                        bmp = Bitmap.createBitmap(bmp,
                                bmp.getWidth() / 2 - bmp.getHeight() / 2, 0,
                                bmp.getHeight(), bmp.getHeight(), null, true);

                    } else {

                        bmp = Bitmap.createBitmap(bmp, 0,
                                bmp.getHeight() / 2 - bmp.getWidth() / 2,
                                bmp.getWidth(), bmp.getWidth(), null, true);
                    }
                    bmp = Sales1CRMUtils.ImageUtility.scaleDownBitmap(bmp);
                    ExifInterface ei;
                    int orientation = 0;
                    try {
                        ei = new ExifInterface(photoPath);
                        orientation = ei.getAttributeInt(
                                ExifInterface.TAG_ORIENTATION,
                                ExifInterface.ORIENTATION_NORMAL);

                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                    switch (orientation) {
                        case ExifInterface.ORIENTATION_ROTATE_90:
                            bmp = Sales1CRMUtils.ImageUtility.RotateBitmap(bmp, 90);
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_180:
                            bmp = Sales1CRMUtils.ImageUtility.RotateBitmap(bmp, 180);
                            break;
                        // etc.
                    }
                    savedBitmap = bmp.copy(bmp.getConfig(), bmp.isMutable() ? true
                            : false);
                    ivAccountPicture.setImageBitmap(Sales1CRMUtils.Utils.getroundBitmap(bmp));
                    bmp = null;

                    onCaptureImageResult(data);
                }
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case Sales1CRMUtils.KEY_CAMERA_PERMISION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    cameraIntent();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(context, "Doesn't have permission... ", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }


    private void selectImage() {
        final CharSequence[] items = {"Take Picture", "Galery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Add Picture!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals("Take Picture")) {

                    if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions((Activity) context, new String[]{android.Manifest.permission.CAMERA}, Sales1CRMUtils.KEY_CAMERA_PERMISION);
                    } else {
                        cameraIntent();
                    }
                } else if (items[item].equals("Galery")) {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(
                            Intent.createChooser(intent, "Select Picture"),
                            Sales1CRMUtils.GALLERY);
                    dialog.dismiss();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void cameraIntent() {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        Intent intentCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File photoFile = null;
        try {
            photoFile = createImageFile();

        } catch (IOException e) {
            e.printStackTrace();
        }

        if (photoFile != null) {
            Uri photoURI = Uri.fromFile(photoFile);
            intentCamera.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
            startActivityForResult(intentCamera, Sales1CRMUtils.CAMERA_REQUEST);
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            String Name = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String imageFileName = Name + ".jpg";
            File storageDir = new File(Environment.getExternalStorageDirectory().getPath(), imageFileName);
            photoPath = storageDir.getPath();
            return storageDir;
        } else {
            String imageFileName = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            File storageDir = context.getExternalFilesDir("Pictures");
            File image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );
            Log.d("TAG", image.getPath());
            photoPath = image.getPath();
            return image;
        }
    }

    private void onCaptureImageResult(Intent data) {
        //Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        Log.d("TAG", "Masuk OnCaptureImage");
        try {
            ImageCompression imageCompression = new ImageCompression(context);
            filePath = imageCompression.compressImage(photoPath);
            Log.d("PPP", "Path : " + filePath);
            Bitmap thumbnail = Sales1CRMUtils.ImageUtility.getThumbnail(filePath);

            photo = Sales1CRMUtils.ImageUtility.encodeToBase64(thumbnail, Bitmap.CompressFormat.JPEG, 50);


            File imgFile = new File(photoPath);
            imgFile.delete();
            photoPath = "";
        } catch (IOException e) {
            e.printStackTrace();
            Log.d("TAG", e.getMessage());
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_foto_profile);
        ButterKnife.bind(this);
        this.context = this;
        changeFotoProfileActivity = this;
        this.presenter = new UploadImageProfilePresenter(context, this);
        this.url = PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.URL);

        initView();
    }

    private  void initView() {
//        Sales1CRMUtils.Utils.imageLoader.DisplayImage(url + PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.KEY_IMAGES_MASTER),
//                ivAccountPicture, Bitmap.Config.ARGB_8888, 300, Sales1CRMUtils.TYPE_RECT);
        back.setOnClickListener(click);
        ivAccountPicture.setOnClickListener(click);
        tvSubmit.setOnClickListener(click);
        tvFullname.setText(PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.KEY_FULLNAME));
        tvCompany.setText(PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.KEY_COMPANY));
        Position.setText(PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.KEY_POSITION));
        Picasso.get().load(url + PreferenceUtility.getInstance()
                .loadDataString(context, PreferenceUtility.KEY_IMAGES_MASTER))
                .error(R.drawable.avatar_placeholder)
                .into(ivAccountPicture);

    }

    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.back:
                    onBackPressed();
                    break;
                case R.id.ivAccountPicture:
                    selectImage();
                    break;
                case R.id.tvSubmit:
//                    if(Sales1CRMUtils.ConnectionUtility.isNetworkConnected(context)) {
                        UploadImageModel uploadImageModel = new UploadImageModel();
                        uploadImageModel.setPhoto(photo);
                        uploadImageModel.setName(getTimeStamp() + ".jpg");
                        showLoadingDialog();
                        presenter.uploadImage(ApiParam.API_114, uploadImageModel);
//                    }
                    break;
            }
        }
    };

    private String getTimeStamp() {
        Calendar cd = Calendar.getInstance();
        SimpleDateFormat s = new SimpleDateFormat("yyyyMMdd_HHmmss");
        return s.format(cd.getTime());

    }



    @Override
    public void finishUplaod(String result, String response) {
        dismissLoadingDialog();
        if(result.equalsIgnoreCase("OK")){
            finish();
        }
    }

    @Override
    public void displayImage(String result, UserModel userModel) {

    }

}
