package com.msf.project.sales1crm;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.msf.project.sales1crm.callback.DomainCheckCallback;
import com.msf.project.sales1crm.customview.CustomEditText;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.model.DomainModel;
import com.msf.project.sales1crm.presenter.DomainCheckPresenter;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.PermissionCheck;
import com.msf.project.sales1crm.utility.PreferenceUtility;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DomainCheckActivity extends BaseActivity implements DomainCheckCallback {

    @BindView(R.id.etDomain)
    CustomEditText etDomain;
    @BindView(R.id.btnNext)
    CustomTextView btnNext;

    private Context context;
    private PermissionCheck permissionCheck;
    private DomainCheckPresenter domainCheckPresenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_domain_check);

        ButterKnife.bind(this);
        this.context = this;

        domainCheckPresenter = new DomainCheckPresenter(context,this);

        this.permissionCheck = new PermissionCheck();
        this.permissionCheck.checkPermission(context);
        initView();
    }

    private void initView(){
        etDomain.setText(PreferenceUtility.getInstance().loadDataString(context,PreferenceUtility.KEY_DOMAIN));

        btnNext.setOnClickListener(click);

    }

    private void getDataFromApi(){
        DomainModel domainModel = new DomainModel();
        domainModel.setKeyword(etDomain.getText().toString());
        showLoadingDialog();
        domainCheckPresenter.setupDomainCheckItems(ApiParam.API_000,domainModel);
    }

    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()){
                case R.id.btnNext:
                    getDataFromApi();
                    break;
                default:
                    break;
            }
        }
    };

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

    @Override
    public void finishedDomain(String result, String response) {
        dismissLoadingDialog();
        Log.e("Result domain ", result);
        Log.e("Domain ", response);
        if (result.equalsIgnoreCase("OK")){
            PreferenceUtility.getInstance().saveData(context,PreferenceUtility.URL, "https://" + response);
            PreferenceUtility.getInstance().saveData(context,PreferenceUtility.KEY_DOMAIN, "1");
            startActivity(new Intent(context, LoginActivity.class));
        } else {
            Toast.makeText(context, "Domain Not Found", Toast.LENGTH_SHORT).show();
        }
//        finish();
    }
}
