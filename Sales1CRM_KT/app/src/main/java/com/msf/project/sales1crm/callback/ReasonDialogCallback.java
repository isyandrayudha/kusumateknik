package com.msf.project.sales1crm.callback;

import android.os.Bundle;

public interface ReasonDialogCallback {
    void onReason(Bundle data);
}
