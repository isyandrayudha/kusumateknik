package com.msf.project.sales1crm.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;

import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.model.TaskModel;

import java.util.List;

public class TaskListAdapter extends BaseAdapter {
    private Context context;
    private Bitmap icon;
    private String url;
    private List<TaskModel> listData;

    public TaskListAdapter(Context context, List<TaskModel> objects) {
        this.context = context;
        this.listData = objects;
    }

    public static abstract class Row {
    }

    // This is for making Section, ex: A B C D etc
    public static final class Section extends Row {
        public final String text;

        public Section(String text) {
            this.text = text;
        }
    }

    // This is for making Row, ex: A B C D etc
    public static final class Item extends Row {
        public final TaskModel text;

        public Item(TaskModel text) {
            this.text = text;
        }

        public TaskModel getItem() {
            return text;
        }
    }

    private List<Row> rows;

    public void setRows(List<Row> rows) {
        this.rows = rows;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return rows.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return rows.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        if (getItem(position) instanceof Section) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public View getView(final int position, View converView, ViewGroup parent) {
        View view = converView;
        final ViewHolder holder;

        final Item item = (Item) getItem(position);
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) parent.getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = (LinearLayout) inflater.inflate(
                    R.layout.row_todolist, parent, false);
            holder = new ViewHolder();
            holder.tvTaskName = (CustomTextView) view
                    .findViewById(R.id.tvTaskName);
            holder.tvDesc = (CustomTextView) view
                    .findViewById(R.id.tvDescription);
            holder.tvDate = (CustomTextView) view
                    .findViewById(R.id.tvDate);
            holder.tvAssignto = (CustomTextView) view
                    .findViewById(R.id.tvAssignTo);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        holder.tvTaskName.setText(item.text.getSubject());
        holder.tvDesc.setText(item.text.getDescription());
        holder.tvDate.setText(item.text.getDate());
        holder.tvAssignto.setText(item.text.getAssignto_name());

        return view;
    }

    private static class ViewHolder {
        CustomTextView tvTaskName, tvDesc, tvDate, tvAssignto;
    }
}
