package com.msf.project.sales1crm.model;

import android.os.Parcel;
import android.os.Parcelable;

public class DatabaseMachineModel implements Comparable, Parcelable {
	private String pressureVacuum;
	private String latestInspectionDate;
	private String motorSpecs;
	private String typeOfServiceDone;
	private String createdAt;
	private String serialNumber;
	private String type;
	private String deletedAt;
	private String speed;
	private String brandId;
	private String capacity;
	private String unitNoInCustomerSite;
	private String condition;
	private String accountId;
	private String application;
	private String runningHours;
	private String updatedAt;
	private String model;
	private String yearOfInstall;
	private String id;
	private String dateLastOverhaul;
	private String diffPressure;
	private String status;
	private String accountName;
	private String brandName;

	public DatabaseMachineModel(){
		setPressureVacuum("");
		setLatestInspectionDate("");
		setMotorSpecs("");
		setTypeOfServiceDone("");
		setCreatedAt("");
		setSerialNumber("");
		setType("");
		setDeletedAt("");
		setSpeed("");
		setBrandId("");
		setCapacity("");
		setUnitNoInCustomerSite("");
		setCondition("");
		setAccountId("");
		setApplication("");
		setRunningHours("");
		setUpdatedAt("");
		setModel("");
		setYearOfInstall("");
		setId("");
		setDateLastOverhaul("");
		setDiffPressure("");
		setStatus("");
		setAccountName("");
		setBrandName("");
	}

	public DatabaseMachineModel(Parcel in){
		read(in);
	}

	public void read(Parcel in) {
		pressureVacuum = in.readString();
		latestInspectionDate = in.readString();
		motorSpecs = in.readString();
		typeOfServiceDone = in.readString();
		createdAt = in.readString();
		serialNumber = in.readString();
		type = in.readString();
		deletedAt = in.readString();
		speed = in.readString();
		brandId = in.readString();
		capacity = in.readString();
		unitNoInCustomerSite = in.readString();
		condition = in.readString();
		accountId = in.readString();
		application = in.readString();
		runningHours = in.readString();
		updatedAt = in.readString();
		model = in.readString();
		yearOfInstall = in.readString();
		id = in.readString();
		dateLastOverhaul = in.readString();
		diffPressure = in.readString();
		status = in.readString();
		accountName = in.readString();
		brandName = in.readString();
	}

	public static final Creator<DatabaseMachineModel> CREATOR = new Creator<DatabaseMachineModel>() {
		@Override
		public DatabaseMachineModel createFromParcel(Parcel in) {
			return new DatabaseMachineModel(in);
		}

		@Override
		public DatabaseMachineModel[] newArray(int size) {
			return new DatabaseMachineModel[size];
		}
	};

	public void readFromParcel(Parcel in){

	}

	public void setPressureVacuum(String pressureVacuum){
		this.pressureVacuum = pressureVacuum;
	}

	public String getPressureVacuum(){
		return pressureVacuum;
	}

	public void setLatestInspectionDate(String latestInspectionDate){
		this.latestInspectionDate = latestInspectionDate;
	}

	public String getLatestInspectionDate(){
		return latestInspectionDate;
	}

	public void setMotorSpecs(String motorSpecs){
		this.motorSpecs = motorSpecs;
	}

	public String getMotorSpecs(){
		return motorSpecs;
	}

	public void setTypeOfServiceDone(String typeOfServiceDone){
		this.typeOfServiceDone = typeOfServiceDone;
	}

	public String getTypeOfServiceDone(){
		return typeOfServiceDone;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setSerialNumber(String serialNumber){
		this.serialNumber = serialNumber;
	}

	public String getSerialNumber(){
		return serialNumber;
	}

	public void setType(String type){
		this.type = type;
	}

	public String getType(){
		return type;
	}

	public void setDeletedAt(String deletedAt){
		this.deletedAt = deletedAt;
	}

	public String getDeletedAt(){
		return deletedAt;
	}

	public void setSpeed(String speed){
		this.speed = speed;
	}

	public String getSpeed(){
		return speed;
	}

	public void setBrandId(String brandId){
		this.brandId = brandId;
	}

	public String getBrandId(){
		return brandId;
	}

	public void setCapacity(String capacity){
		this.capacity = capacity;
	}

	public String getCapacity(){
		return capacity;
	}

	public void setUnitNoInCustomerSite(String unitNoInCustomerSite){
		this.unitNoInCustomerSite = unitNoInCustomerSite;
	}

	public String getUnitNoInCustomerSite(){
		return unitNoInCustomerSite;
	}

	public void setCondition(String condition){
		this.condition = condition;
	}

	public String getCondition(){
		return condition;
	}

	public void setAccountId(String accountId){
		this.accountId = accountId;
	}

	public String getAccountId(){
		return accountId;
	}

	public void setApplication(String application){
		this.application = application;
	}

	public String getApplication(){
		return application;
	}

	public void setRunningHours(String runningHours){
		this.runningHours = runningHours;
	}

	public String getRunningHours(){
		return runningHours;
	}

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public void setModel(String model){
		this.model = model;
	}

	public String getModel(){
		return model;
	}

	public void setYearOfInstall(String yearOfInstall){
		this.yearOfInstall = yearOfInstall;
	}

	public String getYearOfInstall(){
		return yearOfInstall;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setDateLastOverhaul(String dateLastOverhaul){
		this.dateLastOverhaul = dateLastOverhaul;
	}

	public String getDateLastOverhaul(){
		return dateLastOverhaul;
	}

	public void setDiffPressure(String diffPressure){
		this.diffPressure = diffPressure;
	}

	public String getDiffPressure(){
		return diffPressure;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	@Override
 	public String toString(){
		return 
			"DatabaseMachineModel{" + 
			"pressure_vacuum = '" + pressureVacuum + '\'' + 
			",latest_inspection_date = '" + latestInspectionDate + '\'' + 
			",motor_specs = '" + motorSpecs + '\'' + 
			",type_of_service_done = '" + typeOfServiceDone + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",serial_number = '" + serialNumber + '\'' + 
			",type = '" + type + '\'' + 
			",deleted_at = '" + deletedAt + '\'' + 
			",speed = '" + speed + '\'' + 
			",brand_id = '" + brandId + '\'' + 
			",capacity = '" + capacity + '\'' + 
			",unit_no_in_customer_site = '" + unitNoInCustomerSite + '\'' + 
			",condition = '" + condition + '\'' + 
			",account_id = '" + accountId + '\'' + 
			",application = '" + application + '\'' + 
			",running_hours = '" + runningHours + '\'' + 
			",updated_at = '" + updatedAt + '\'' + 
			",model = '" + model + '\'' + 
			",year_of_install = '" + yearOfInstall + '\'' + 
			",id = '" + id + '\'' + 
			",date_last_overhaul = '" + dateLastOverhaul + '\'' + 
			",diff_pressure = '" + diffPressure + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(pressureVacuum);
		dest.writeString(latestInspectionDate);
		dest.writeString(motorSpecs);
		dest.writeString(typeOfServiceDone);
		dest.writeString(createdAt);
		dest.writeString(serialNumber);
		dest.writeString(type);
		dest.writeString(deletedAt);
		dest.writeString(speed);
		dest.writeString(brandId);
		dest.writeString(capacity);
		dest.writeString(unitNoInCustomerSite);
		dest.writeString(condition);
		dest.writeString(accountId);
		dest.writeString(application);
		dest.writeString(runningHours);
		dest.writeString(updatedAt);
		dest.writeString(model);
		dest.writeString(yearOfInstall);
		dest.writeString(id);
		dest.writeString(dateLastOverhaul);
		dest.writeString(diffPressure);
		dest.writeString(status);
		dest.writeString(accountName);
		dest.writeString(brandName);
	}

	@Override
	public int compareTo(Object o) {
		return 0;
	}
}
