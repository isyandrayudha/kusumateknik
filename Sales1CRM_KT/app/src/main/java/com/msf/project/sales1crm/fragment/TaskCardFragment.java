package com.msf.project.sales1crm.fragment;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.msf.project.sales1crm.ErrorAPIKeyActivity;
import com.msf.project.sales1crm.MenuActivity;
import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.TaskAddActivity;
import com.msf.project.sales1crm.adapter.TaskListCardAdapter;
import com.msf.project.sales1crm.callback.SortDialogCallback;
import com.msf.project.sales1crm.callback.TaskListCallback;
import com.msf.project.sales1crm.customview.CustomEditText;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.imageloader.ImageLoader;
import com.msf.project.sales1crm.model.TaskModel;
import com.msf.project.sales1crm.presenter.TaskListPresenter;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class TaskCardFragment extends Fragment implements TaskListCallback {


    @BindView(R.id.etSearch)
    CustomEditText etSearch;
    @BindView(R.id.ivClear)
    ImageView ivClear;
    @BindView(R.id.tvRefresh)
    CustomTextView tvRefresh;
    @BindView(R.id.rlNoConnection)
    RelativeLayout rlNoConnection;
    @BindView(R.id.llNotFound)
    LinearLayout llNotFound;
    @BindView(R.id.rlProgress)
    RelativeLayout rlProgress;
    @BindView(R.id.rvTask)
    RecyclerView rvTask;
    @BindView(R.id.lvTaskSwipe)
    SwipeRefreshLayout lvTaskSwipe;
    @BindView(R.id.faTaskAdd)
    FloatingActionButton faTaskAdd;
    @BindView(R.id.menu)
    FloatingActionMenu menu;
    @BindView(R.id.rlTaskList)
    RelativeLayout rlTaskList;
    Unbinder unbind;
    @BindView(R.id.shimmerLayout)
    ShimmerFrameLayout shimmerLayout;
    private View view;
    private Context context;
    private TaskListCardAdapter adapter;
    private TaskListPresenter presenter;
    private List<TaskModel> list = new ArrayList<>();
    private String date_selected = "";
    private Calendar calendar;
    private String[] sort = {"Subject Asc", "Sub Desc", "account Name Asc", "account Name Desc", "Due Date Asc", "Due Date Desc"};

    private int prevSize = 0, sort_id = 0;
    private boolean isLoadMore = true;
    private boolean isMaxSize = true;
    private boolean pause = false;

    public static TaskCardFragment newInstance() {
        TaskCardFragment fragment = new TaskCardFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_taskcard, container, false);
        setHasOptionsMenu(true);
        ButterKnife.bind(this, this.view);
        return this.view;
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        this.context = getActivity();
        this.presenter = new TaskListPresenter(context, this);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Task");

        initData();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_list:
                MenuActivity.getInstance().setFragment(TaskFragment.newInstance());
                item.setIcon(R.drawable.ic_list_white_24dp);
                break;
            case R.id.action_cards:
                MenuActivity.getInstance().setFragment(TaskCardFragment.newInstance());
                item.setIcon(R.drawable.ic_cards_black_24dp);
                break;
            case R.id.action_maps:
                break;
            case R.id.action_filter:
                SortDialogFragment DialogFragment = new SortDialogFragment(context, new SortDialogCallback() {

                    @Override
                    public void onDialogCallback(Bundle data) {
                        sort_id = Integer.parseInt(data.getString("id"));

                        getDataFromAPI(0, date_selected, sort_id);
//                        ivSelectSort.setVisibility(View.VISIBLE);
                    }
                }, sort);
                DialogFragment.show(getActivity().getSupportFragmentManager(), "sortDialog");
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @TargetApi(Build.VERSION_CODES.M)
    @RequiresApi(api = Build.VERSION_CODES.M)
    private void initData() {
        Sales1CRMUtils.Utils.imageLoader = new ImageLoader(context);

        etSearch.clearFocus();
//        rvTask.setOnScrollChangeListener((View.OnScrollChangeListener) new ListLazyLoad());
        lvTaskSwipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getDataFromAPI(0, date_selected, sort_id);
            }
        });

        setTextWatcherForSearch();
        getDataFromAPI(0, date_selected, sort_id);
        ivClear.setOnClickListener(click);
        tvRefresh.setOnClickListener(click);
        faTaskAdd.setOnClickListener(click);


    }

    private void setTextWatcherForSearch() {
        etSearch.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    if (etSearch.getText().length() == 0) {
                        ivClear.setVisibility(View.VISIBLE);
                    }
//                } else {
//                    if (etSearch.getText().length() == 0) {
//                        ivClear.setVisibility(View.GONE);
//                    } else {
//                        ivClear.setVisibility(View.VISIBLE);
//                    }

                }
            }
        });

        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_GO) {
                    getDataFromAPI(0, date_selected, sort_id);
                }
                return false;
            }
        });
    }

    private void getDataFromAPI(int prev, String date, int sort) {
        isMaxSize = true;
        isLoadMore = true;

        llNotFound.setVisibility(View.GONE);

        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(context)) {
            presenter.setupList(ApiParam.API_070, prev, sort, etSearch.getText().toString(), "", date);
        } else {
            rlNoConnection.setVisibility(View.VISIBLE);
            lvTaskSwipe.setVisibility(View.GONE);
        }

    }

    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.ivClear:
                    etSearch.setText("");
                    etSearch.clearFocus();
                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);
                    getDataFromAPI(0, date_selected, sort_id);
                    break;
                case R.id.tvRefresh:
                    lvTaskSwipe.setVisibility(View.VISIBLE);
                    rlNoConnection.setVisibility(View.GONE);
                    break;
                case R.id.faTaskAdd:
                    Intent intent = new Intent(context, TaskAddActivity.class);
                    intent.putExtra("date", date_selected);
                    startActivity(intent);
                    break;
            }
        }
    };


    private void setList() {
        adapter = new TaskListCardAdapter(context, this.list);

        List<TaskListCardAdapter.Row> rows = new ArrayList<TaskListCardAdapter.Row>();
        shimmerLayout.startShimmer();
        shimmerLayout.setVisibility(View.GONE);
        for (TaskModel lc : list) {
            rows.add(new TaskListCardAdapter.Item(lc));
        }
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rvTask.setLayoutManager(layoutManager);
        adapter.setRows(rows);
        rvTask.setAdapter(adapter);

    }

    private void setMoreList() {
        List<TaskListCardAdapter.Row> rows = new ArrayList<TaskListCardAdapter.Row>();
        shimmerLayout.stopShimmer();
        shimmerLayout.setVisibility(View.GONE);
        for (TaskModel tm : list) {
            rows.add(new TaskListCardAdapter.Item(tm));
        }
        adapter.setRows(rows);
        adapter.notifyDataSetChanged();
        shimmerLayout.setVisibility(View.GONE);
        shimmerLayout.stopShimmer();
    }


    @Override
    public void finishTaskList(String result, List<TaskModel> list) {
        lvTaskSwipe.setRefreshing(false);
        rlProgress.setVisibility(View.GONE);
        if (result.equalsIgnoreCase("OK")) {
            prevSize = list.size();
            this.list = list;

            isMaxSize = false;
            isLoadMore = false;

            setList();
            if (list.size() > 0) {
                llNotFound.setVisibility(View.GONE);
            } else {
                llNotFound.setVisibility(View.VISIBLE);
            }
        } else if (result.equalsIgnoreCase("FAILDES_KEY")) {
            startActivity(new Intent(context, ErrorAPIKeyActivity.class));
            MenuActivity.getInstance().finish();
        } else {
            lvTaskSwipe.setVisibility(View.GONE);
            rvTask.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void finishTaskMoreList(String result, List<TaskModel> list) {
        if (result.equals("OK")) {
            for (int i = 0; i < list.size(); i++) {
                this.list.add(list.get(i));
            }

            if (this.prevSize == this.list.size())
                isMaxSize = true;
            this.prevSize = this.list.size();
            setMoreList();
        }
        this.isLoadMore = false;
    }

    private class ListLazyLoad implements AbsListView.OnScrollListener {

        @Override
        public void onScrollStateChanged(AbsListView absListView, int i) {

        }

        @Override
        public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            if (firstVisibleItem + visibleItemCount == totalItemCount && totalItemCount != 0) {
                Log.d("TAG", "isLoadMore : " + isLoadMore);
                if (isLoadMore == false && isMaxSize == false) {
                    isLoadMore = true;
                    prevSize = list.size();

                    presenter.setupMoreList(ApiParam.API_070, prevSize, sort_id, etSearch.getText().toString(), "", date_selected);
                }
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    @Override
    public void onPause() {
        pause = true;
        super.onPause();
    }

    @Override
    public void onResume() {
        if (pause) {
            getDataFromAPI(0, date_selected, sort_id);
        }
        super.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
        shimmerLayout.stopShimmer();
    }
}
