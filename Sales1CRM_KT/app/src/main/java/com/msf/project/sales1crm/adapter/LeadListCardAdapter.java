package com.msf.project.sales1crm.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.msf.project.sales1crm.LeadDetailActivity;
import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.model.LeadModel;
import com.msf.project.sales1crm.utility.PreferenceUtility;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class LeadListCardAdapter extends RecyclerView.Adapter<LeadListCardAdapter.LeadListCardHolder>{

    private Context context;
    private Bitmap icon;
    private String url;
    private List<LeadModel> listData;

    public LeadListCardAdapter(Context context,List<LeadModel> objects) {
        this.context = context;
        this.listData = objects;

        this.url = PreferenceUtility.getInstance().loadDataString(context,PreferenceUtility.URL);
    }

    public static abstract class Row {
    }

    public static final class Section extends Row {
        public final String text;

        public Section(String text) {
            this.text = text;
        }
    }

    public static final class Item extends Row {
        public final LeadModel text;

        public Item(LeadModel text) {
            this.text = text;
        }

        public LeadModel getItem(){
            return text;
        }
    }

    private List<Row> rows;

    public void setRows(List<Row> rows) {
        this.rows = rows;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return rows.size();
    }

    @Override
    public LeadListCardAdapter.LeadListCardHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).
                inflate(R.layout.card_lead,null);
        return new LeadListCardHolder(view);
    }

    @Override
    public void onBindViewHolder(LeadListCardHolder holder, int position) {
        final Item item = (Item) rows.get(position);
        holder.tvLeadName.setText(item.text.getFirst_name() + " " + item.text.getLast_name());
        holder.tvCompany.setText(item.text.getCompany());
        holder.tvCompanyy.setText(item.text.getCompany());
        holder.tvPosition.setText(item.text.getPosition_name());
        holder.tvOwnerName.setText(item.text.getOwner_name());
        holder.tvtelephone.setText(item.text.getPhone());

        Sales1CRMUtils.Utils.imageLoader.DisplayImage(url+item.text.getImage_url(),
                holder.ivImage,Bitmap.Config.ARGB_8888,100,Sales1CRMUtils.TYPE_RECT);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    Intent intent = new Intent(context, LeadDetailActivity.class);
                    intent.putExtra("leadModel", item.getItem());
                    context.startActivity(intent);

            }
        });
    }



    public class LeadListCardHolder extends RecyclerView.ViewHolder{
        CircleImageView ivImage;
        CustomTextView tvLeadName,tvCompany,tvCompanyy,tvPosition,tvOwnerName, tvtelephone;
        public LeadListCardHolder(View itemView) {
            super(itemView);
            tvLeadName = (CustomTextView) itemView.findViewById(R.id.tvLeadName);
            tvCompany = (CustomTextView) itemView.findViewById(R.id.tvCompany);
            tvCompanyy = (CustomTextView) itemView.findViewById(R.id.tvCompanyy);
            tvPosition = (CustomTextView) itemView.findViewById(R.id.tvPosition);
            ivImage = (CircleImageView) itemView.findViewById(R.id.ivImage);
            tvOwnerName = (CustomTextView) itemView.findViewById(R.id.tvOwnerName);
            tvtelephone = (CustomTextView) itemView.findViewById(R.id.tvtelephone);

        }
    }
}
