package com.msf.project.sales1crm.presenter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;

import com.msf.project.sales1crm.callback.PresensiCallback;
import com.msf.project.sales1crm.service.NetworkConnection;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.PreferenceUtility;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;
import com.msf.project.sales1crm.utility.Sales1CRMUtilsJSON;

import org.json.JSONException;
import org.json.JSONObject;


public class PresensiPresenter {
    private final String TAG = AccountAddPresenter.class.getSimpleName();
    private Context context;
    private Messenger messenger;
    private Message message;
    private Bundle bundle;
    private Handler handler;
    private String failureResponse = "";
    private String[] stringResponse = {""};
    private PresensiCallback callback;
    private String result = "NG";
    private String response = "Tolong Periksa Koneksi Anda";

    public PresensiPresenter(Context context, PresensiCallback callback){
        this.context = context;
        this.callback = callback;
    }

    private void doNetworkService(String url, String params, String from) {
        Log.d(TAG, "url login " + url);
        Log.d(TAG, "params : " + params);
        Intent networkIntent = new Intent(this.context, NetworkConnection.class);
        this.messenger = new Messenger(this.handler);
        networkIntent.putExtra("messenger", this.messenger);
        networkIntent.putExtra("url", url);
        networkIntent.putExtra("params", params);
        networkIntent.putExtra("from", from);
        this.context.startService(networkIntent);
    }

    public void Checkin(int apiIndex){
        if(Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)){
            obtainCheckin(apiIndex);
        }else {
            callback.checkin(this.result, this.response);
        }
    }

    public void obtainCheckin(int apiIndex){
        this.handler = new Handler(this.context.getMainLooper()){
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                parseCheckin(msg);
            }
        };

        JSONObject jsonObject = new JSONObject();
        try{
            jsonObject.put("api_key", PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.API_KEY));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        doNetworkService(ApiParam.urlBuilder(this.context, apiIndex), jsonObject.toString(),"ApiKey");
    }

    private void parseCheckin(Message message){
        this.message = message;
        this.bundle = this.message.getData();
        this.stringResponse[0] = this.bundle.getString("network_response");
        this.failureResponse = this.bundle.getString("network_failure");
        Log.d(TAG, "responseString[0] info checkin  " + this.stringResponse[0]);
        if (this.failureResponse.equalsIgnoreCase("yes")){
            this.callback.checkin(this.result, this.response);
        }else{
            try {
                this.result = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                this.callback.checkin(this.result, this.response);
            } catch (JSONException e) {
                Log.d(TAG, "Exception detail Response " + e.getMessage());
            }
        }
    }

    public void Checkout(int apiIndex){
        if(Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)){
            obtainCheckout(apiIndex);
        }else {
            callback.checkout(this.result, this.response);
        }
    }

    public void obtainCheckout(int apiIndex){
        this.handler = new Handler(this.context.getMainLooper()){
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                parseCheckout(msg);
            }
        };

        JSONObject jsonObject = new JSONObject();
        try{
            jsonObject.put("api_key", PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.API_KEY));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        doNetworkService(ApiParam.urlBuilder(this.context, apiIndex), jsonObject.toString(),"ApiKey");
    }

    private void parseCheckout(Message message){
        this.message = message;
        this.bundle = this.message.getData();
        this.stringResponse[0] = this.bundle.getString("network_response");
        this.failureResponse = this.bundle.getString("network_failure");
        Log.d(TAG, "responseString[0] info checkout " + this.stringResponse[0]);
        if (this.failureResponse.equalsIgnoreCase("yes")){
            this.callback.checkout(this.result, this.response);
        }else{
            try {
                this.result = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                this.callback.checkout(this.result, this.response);
            } catch (JSONException e) {
                Log.d(TAG, "Exception detail Response " + e.getMessage());
            }
        }
    }
}
