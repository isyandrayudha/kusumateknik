package com.msf.project.sales1crm.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.msf.project.sales1crm.DatabaseMachineCreate;
import com.msf.project.sales1crm.ErrorAPIKeyActivity;
import com.msf.project.sales1crm.MenuActivity;
import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.adapter.DatabaseMachineAdapter;
import com.msf.project.sales1crm.callback.DashboardCallback;
import com.msf.project.sales1crm.callback.DatabaseMachineListCallback;
import com.msf.project.sales1crm.callback.SortDialogCallback;
import com.msf.project.sales1crm.customview.CustomEditText;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.model.AbsenceModel;
import com.msf.project.sales1crm.model.DashboardFirstModel;
import com.msf.project.sales1crm.model.DashboardSecondModel;
import com.msf.project.sales1crm.model.DashboardThirdModel;
import com.msf.project.sales1crm.model.DatabaseMachineModel;
import com.msf.project.sales1crm.model.TotalOpportunityModel;
import com.msf.project.sales1crm.presenter.DashboardPresenter;
import com.msf.project.sales1crm.presenter.DatabaseMachineListPresenter;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DatabaseMachineFragment extends Fragment implements DatabaseMachineListCallback, DashboardCallback {
    @BindView(R.id.etSearch)
    CustomEditText etSearch;
    @BindView(R.id.ivClear)
    ImageView ivClear;
    @BindView(R.id.tvRefresh)
    CustomTextView tvRefresh;
    @BindView(R.id.rlNoConnection)
    RelativeLayout rlNoConnection;
    @BindView(R.id.llNotFound)
    LinearLayout llNotFound;
    @BindView(R.id.RvDatabaseMachine)
    RecyclerView RvDatabaseMachine;
    @BindView(R.id.rvSwipe)
    SwipeRefreshLayout rvSwipe;
    @BindView(R.id.faDatabaseAdd)
    FloatingActionButton faDatabaseAdd;
    @BindView(R.id.menu)
    FloatingActionMenu menu;
    @BindView(R.id.rlDMList)
    RelativeLayout rlDMList;
    @BindView(R.id.shimmerLayout)
    ShimmerFrameLayout shimmerLayout;
    @BindView(R.id.llshim)
    LinearLayout llshim;

    private View view;
    private Context context;
    private boolean isScrolling = false;
    private int currentItems, totalItems, scrollOutItems;
    private int prevSize = 0, sort_id = 0;
    private boolean isLoadMore = true;
    private boolean isMaxSize = true;
    private boolean pause = false;
    private DatabaseMachineAdapter adapter;
    private DatabaseMachineListPresenter presenter;
    private DashboardPresenter dashboardPresenter;
    private List<DatabaseMachineModel> list = new ArrayList<>();
    private String[] sort = {"Name A to Z", "Name Z to A"};


    public static DatabaseMachineFragment newInstance() {
        DatabaseMachineFragment fragment = new DatabaseMachineFragment();
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_database_machine, container, false);
        setHasOptionsMenu(true);
        this.context = getActivity();
        ButterKnife.bind(this, this.view);
        return this.view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.context = getActivity();
        this.presenter = new DatabaseMachineListPresenter(context, this);
        this.dashboardPresenter = new DashboardPresenter(context, this);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Database Machine");
        initData();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_item, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_cards:
//                if (!checkFunction.userAccessControl(97)) {
//                    Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
//                } else {
//                    MenuActivity.getInstance().setFragment(TaskTabByStatusFragment.newInstance());
//                    item.setIcon(R.drawable.ic_list_white_24dp);
//                }
                break;
            case R.id.action_list:
//                if (!checkFunction.userAccessControl(97)) {
//                    Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
//                } else {
//                    MenuActivity.getInstance().setFragment(TaskFragment.newInstance());
//                    item.setIcon(R.drawable.ic_cards_black_24dp);
//                }
                break;
            case R.id.action_filter:
                SortDialogFragment DialogFragment = new SortDialogFragment(context, new SortDialogCallback() {
                    @Override
                    public void onDialogCallback(Bundle data) {
                        sort_id = Integer.parseInt(data.getString("id"));

                        getDataFromApi(0, sort_id);
//                        ivSelectSort.setVisibility(View.VISIBLE);
                    }
                }, sort);
                DialogFragment.show(getActivity().getSupportFragmentManager(), "sortDialog");
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    private void initData() {
        etSearch.clearFocus();

        rvSwipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                llshim.setVisibility(view.VISIBLE);
                shimmerLayout.setVisibility(View.VISIBLE);
                shimmerLayout.startShimmer();
                if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(context)) {
                    presenter.setupList(ApiParam.API_119, 0, etSearch.getText().toString(), 0);
                } else {
                    rvSwipe.setVisibility(View.GONE);
                    rlNoConnection.setVisibility(View.VISIBLE);
                }
            }
        });

        setTextWatcherForSearch();
        getDataFromApi(0, 0);
        ivClear.setOnClickListener(click);
        tvRefresh.setOnClickListener(click);
    }

    private void getDataFromApi(int prevSize, int sort_id) {
        isMaxSize = true;
        isLoadMore = true;
        llNotFound.setVisibility(View.GONE);
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(context)) {
            presenter.setupList(ApiParam.API_119, prevSize, etSearch.getText().toString(), sort_id);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    dashboardPresenter.setupFirst(ApiParam.API_005);
                }
            }, 1000);
        } else {
            rlNoConnection.setVisibility(View.VISIBLE);
            rvSwipe.setVisibility(View.GONE);
            rvSwipe.setRefreshing(false);
        }
    }

    private void setTextWatcherForSearch() {
        etSearch.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    if (etSearch.getText().length() == 0) {
                        ivClear.setVisibility(View.VISIBLE);
                    }
                }
            }
        });

        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    getDataFromApi(0, sort_id);
                }
                return true;
            }
        });
    }


    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.tvRefresh:
                    llshim.setVisibility(View.VISIBLE);
                    rlNoConnection.setVisibility(View.GONE);
                    getDataFromApi(0, 0);
                    break;
                case R.id.ivClear:
                    etSearch.setText("");
                    etSearch.clearFocus();
                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);
                    getDataFromApi(0, 0);
                    break;
            }
        }
    };

    private void setList() {
        llshim.setVisibility(View.VISIBLE);
        shimmerLayout.setVisibility(View.GONE);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        RvDatabaseMachine.setLayoutManager(layoutManager);
        RvDatabaseMachine.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                currentItems = layoutManager.getChildCount();
                totalItems = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstVisibleItemPosition();
                if (isScrolling && (currentItems + scrollOutItems == totalItems)) {
                    isScrolling = false;
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            presenter.setupListMore(ApiParam.API_119, prevSize, etSearch.getText().toString(), 0);
                        }
                    }, 3000);
                }
            }
        });
        List<DatabaseMachineAdapter.Row> rows = new ArrayList<>();
        for (DatabaseMachineModel dbMachine : list) {
            rows.add(new DatabaseMachineAdapter.Item(dbMachine));
        }
        adapter.setRows(rows);
        RvDatabaseMachine.setAdapter(adapter);
        llshim.setVisibility(View.INVISIBLE);
        shimmerLayout.stopShimmer();
        shimmerLayout.setVisibility(View.INVISIBLE);
    }

    private void setMoreList() {
        List<DatabaseMachineAdapter.Row> rows = new ArrayList<>();
        for (DatabaseMachineModel dbm : list) {
            rows.add(new DatabaseMachineAdapter.Item(dbm));
        }
        adapter.setRows(rows);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void finishDBmachine(String result, List<DatabaseMachineModel> list) {
        rvSwipe.setRefreshing(false);
        if (result.equalsIgnoreCase("OK")) {
            prevSize = list.size();
            this.list = list;
            isMaxSize = false;
            isLoadMore = false;

            adapter = new DatabaseMachineAdapter(context, this.list);
            setList();
            if (list.size() > 0) {
                llNotFound.setVisibility(View.GONE);
            } else {
                llNotFound.setVisibility(View.VISIBLE);
            }
        } else if (result.equalsIgnoreCase("success")) {
            prevSize = list.size();
            this.list = list;
            isMaxSize = false;
            isLoadMore = false;

            adapter = new DatabaseMachineAdapter(context, this.list);
            setList();
            if (list.size() > 0) {
                llNotFound.setVisibility(View.GONE);
            } else {
                llNotFound.setVisibility(View.VISIBLE);
            }
        } else if (result.equalsIgnoreCase("FAILED_KEY")) {
            startActivity(new Intent(context, ErrorAPIKeyActivity.class));
            MenuActivity.getInstance().finish();
        } else {
            rlNoConnection.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void finishDBmachineMore(String result, List<DatabaseMachineModel> list) {
        if (result.equalsIgnoreCase("OK")) {
            for (int i = 0; i < list.size(); i++) {
                this.list.add(list.get(i));
            }
            if (this.prevSize == this.list.size())
                isMaxSize = true;
            this.prevSize = this.list.size();
            setMoreList();
        } else {
            for (int i = 0; i < list.size(); i++) {
                this.list.add(list.get(i));
            }
            if (this.prevSize == this.list.size())
                isMaxSize = true;
            this.prevSize = this.list.size();
            setMoreList();
        }
        this.isLoadMore = false;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void finishFirst(String result, DashboardFirstModel model) {
        if (model.getDatabase_machine_create_permission().equalsIgnoreCase("1")) {
            faDatabaseAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(context, DatabaseMachineCreate.class));
                }
            });
            faDatabaseAdd.setColorNormal(R.color.BlueCRM);
            faDatabaseAdd.setColorPressed(R.color.colorPrimaryDark);
            faDatabaseAdd.setColorPressedResId(R.color.colorPrimaryDark);
            faDatabaseAdd.setColorNormalResId(R.color.BlueCRM);
        } else {
            faDatabaseAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
                }
            });
            faDatabaseAdd.setColorNormal(R.color.BlueCRM_light);
            faDatabaseAdd.setColorPressed(R.color.colorPrimaryDark_light);
            faDatabaseAdd.setColorPressedResId(R.color.colorPrimaryDark_light);
            faDatabaseAdd.setColorNormalResId(R.color.BlueCRM_light);
        }
    }

    @Override
    public void finishSecond(String result, DashboardSecondModel model) {

    }

    @Override
    public void finishThird(String result, List<DashboardThirdModel> listLead, List<DashboardThirdModel> listLead1, List<DashboardThirdModel> listOppor, List<DashboardThirdModel> listOppor1) {

    }

    @Override
    public void TotOpportunity(String result, TotalOpportunityModel totOporModel) {

    }

    @Override
    public void absence(String result, AbsenceModel model) {

    }

    @Override
    public void onPause() {
        pause = true;
        super.onPause();
    }

}
