package com.msf.project.sales1crm.callback;

import com.msf.project.sales1crm.model.PriceModel;

public interface KursCalculateCallback {
    void finishCalculate(String result, PriceModel priceModel);
}
