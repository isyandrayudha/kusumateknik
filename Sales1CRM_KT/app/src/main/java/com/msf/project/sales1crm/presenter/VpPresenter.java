package com.msf.project.sales1crm.presenter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;

import com.msf.project.sales1crm.callback.VpCallback;
import com.msf.project.sales1crm.model.VpModel;
import com.msf.project.sales1crm.model.VpOppor;
import com.msf.project.sales1crm.service.NetworkConnection;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.PreferenceUtility;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;
import com.msf.project.sales1crm.utility.Sales1CRMUtilsJSON;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class VpPresenter {
    private final String TAG = VpPresenter.class.getSimpleName();
    private Context context;
    private Messenger messenger;
    private Message message;
    private Bundle bundle;
    private Handler handler;
    private String failureResponse = "";
    private String[] stringResponse = {""};
    private VpCallback callback;
    private VpModel vpModel;
    private String result = "NG";
    private List<VpModel> list = new ArrayList<>();
    private List<VpOppor> listData = new ArrayList<>();

    public VpPresenter(Context context, VpCallback callback){
        this.context = context;
        this.callback = callback;
    }

    private void doNetworkService(String url, String params, String from) {
        Log.d(TAG, "url " + url);
        Log.d(TAG, "params : " + params);
        Intent networkIntent = new Intent(this.context, NetworkConnection.class);
        this.messenger = new Messenger(this.handler);
        networkIntent.putExtra("messenger", this.messenger);
        networkIntent.putExtra("url", url);
        networkIntent.putExtra("params", params);
        networkIntent.putExtra("from", from);
        this.context.startService(networkIntent);
    }

    public void listVp(int apiIndex,int offset,int sort, String search){
        if(Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)){
            obtainList(apiIndex,offset,sort,search);
        } else {
            callback.finishVp(this.result, this.list);
        }
    }

    private void obtainList(int apiIndex, int offset, int sort,String search){
        this.handler = new Handler(this.context.getMainLooper()){
            @Override
            public void handleMessage(Message msg) {
                parseList(msg);
            }
        };
        JSONObject jsonObject = new JSONObject();
        try{
            jsonObject.put("api_key", PreferenceUtility.getInstance().loadDataString(context,PreferenceUtility.API_KEY));
            jsonObject.put("offset",offset+"");
            jsonObject.put("sort", sort+"");
            jsonObject.put("search",search);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        doNetworkService(ApiParam.urlBuilder(this.context, apiIndex), jsonObject.toString(), "Vp");
    }

    public void parseList(Message message){
        this.message = message;
        this.bundle = this.message.getData();
        this.stringResponse[0] = this.bundle.getString("network_response");
        this.failureResponse = this.bundle.getString("network_failure");
        Log.d(TAG, "responseString[0] vp more " + this.stringResponse[0]);
        if (this.failureResponse.equalsIgnoreCase("yes")){
            this.callback.finishVp(this.result, this.list);
        }else{
            try {
                this.result = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                if (this.result.equalsIgnoreCase("OK")){
                    this.list = Sales1CRMUtilsJSON.JSONUtility.getViewPipeline(this.stringResponse[0]);
                }
                this.callback.finishVp(this.result, this.list);
            } catch (JSONException e) {
                Log.d(TAG, "Exception detail Response " + e.getMessage());
            }
        }
    }

    public void listMoreVp(int apiIndex, int offset, int sort,String search){
        if(Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)){
            obtainMorelistVp(apiIndex,offset, sort, search);
        } else {
            callback.finishMoreVp(this.result, this.list);
        }
    }

    public void obtainMorelistVp(int apiIndex, int offset, int sort, String search){
        this.handler = new Handler(this.context.getMainLooper()){
            @Override
            public void handleMessage(Message msg) {
                parseMoreListVp(msg);
            }
        };
        JSONObject jsonObject = new JSONObject();
        try{
            jsonObject.put("api_key", PreferenceUtility.getInstance().loadDataString(context,PreferenceUtility.API_KEY));
            jsonObject.put("offset",offset+"");
            jsonObject.put("sort", sort+"");
//            jsonObject.put("search",search);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        doNetworkService(ApiParam.urlBuilder(this.context, apiIndex), jsonObject.toString(), "Vp");
    }

    private void parseMoreListVp(Message message){
        this.message = message;
        this.bundle = this.message.getData();
        this.stringResponse[0] = this.bundle.getString("network_response");
        this.failureResponse = this.bundle.getString("network_failure");
        Log.d(TAG, "responseString[0] info vp more " + this.stringResponse[0]);
        if (this.failureResponse.equalsIgnoreCase("yes")){
            this.callback.finishMoreVp(this.result, this.list);
        }else{
            try {
                this.result = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                if (this.result.equalsIgnoreCase("OK")){
                    this.list = Sales1CRMUtilsJSON.JSONUtility.getViewPipeline(this.stringResponse[0]);
                }
                this.callback.finishMoreVp(this.result, this.list);
            } catch (JSONException e) {
                Log.d(TAG, "Exception detail Response " + e.getMessage());
            }
        }
    }



}
