package com.msf.project.sales1crm.callback;

import android.os.Bundle;

public interface UserDialogCallback {
    void onUserDialogCallback(Bundle data);
}
