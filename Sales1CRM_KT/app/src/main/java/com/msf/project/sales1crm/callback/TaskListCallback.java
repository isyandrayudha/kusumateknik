package com.msf.project.sales1crm.callback;

import com.msf.project.sales1crm.model.TaskModel;

import java.util.List;

public interface TaskListCallback {
    void finishTaskList(String result, List<TaskModel> list);
    void finishTaskMoreList(String result, List<TaskModel> list);
}
