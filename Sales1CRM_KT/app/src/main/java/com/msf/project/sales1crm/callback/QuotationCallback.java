package com.msf.project.sales1crm.callback;

import com.msf.project.sales1crm.model.QuotationModel;

import java.util.List;

public interface QuotationCallback {
    void finishQuotationList(String result, List<QuotationModel> list);
}
