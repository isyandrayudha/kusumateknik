package com.msf.project.sales1crm.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

public class LocationUserModel implements Comparable, Parcelable {
    private String id;
    private String latitude;
    private String longitude;
    private String address;
    private String date;

    public LocationUserModel(){
        setLatitude("");
        setLongitude("");
        setAddress("");
        setDate("");
    }

    public LocationUserModel(Parcel in) {
        readFromParcel(in);
    }

    public void readFromParcel (Parcel in) {
        latitude = in.readString();
        longitude = in.readString();
        address = in.readString();
        date = in.readString();
    }

    public static final Creator<LocationUserModel> CREATOR = new Creator<LocationUserModel>() {
        @Override
        public LocationUserModel createFromParcel(Parcel in) {
            return new LocationUserModel(in);
        }

        @Override
        public LocationUserModel[] newArray(int size) {
            return new LocationUserModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(latitude);
        parcel.writeString(longitude);
        parcel.writeString(address);
        parcel.writeString(date);
    }

    @Override
    public int compareTo(@NonNull Object o) {
        return 0;
    }
}
