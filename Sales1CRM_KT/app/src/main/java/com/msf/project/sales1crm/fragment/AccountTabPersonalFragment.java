package com.msf.project.sales1crm.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.msf.project.sales1crm.AccountDetailActivity;
import com.msf.project.sales1crm.CallsAddActivity;
import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.adapter.EventSimpleAdapter;
import com.msf.project.sales1crm.adapter.OpportunitySimpleAdapter;
import com.msf.project.sales1crm.adapter.ProductSimpleAdapter;
import com.msf.project.sales1crm.adapter.TaskSimpleAdapter;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.model.AccountModel;
import com.msf.project.sales1crm.model.EventModel;
import com.msf.project.sales1crm.model.OpportunityModel;
import com.msf.project.sales1crm.model.ProductModel;
import com.msf.project.sales1crm.model.TaskModel;
import com.msf.project.sales1crm.service.PhonecallReceiver;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by christianmacbook on 14/05/18.
 */

public class AccountTabPersonalFragment extends Fragment {

    @BindView(R.id.tvAccountType)
    CustomTextView tvAccountType;

    @BindView(R.id.tvIndustry)
    CustomTextView tvIndustry;

    @BindView(R.id.tvAssignTo)
    CustomTextView tvAssignTo;

    @BindView(R.id.tvEmail)
    CustomTextView tvEmail;

    @BindView(R.id.tvTotalEmployee)
    CustomTextView tvTotalEmployee;

    @BindView(R.id.tvDescription)
    CustomTextView tvDescription;

    @BindView(R.id.tvWebsite)
    CustomTextView tvWebsite;

    @BindView(R.id.tvPhone)
    CustomTextView tvPhone;

    @BindView(R.id.llEmail)
    LinearLayout llEmail;

    @BindView(R.id.llWebsite)
    LinearLayout llWebsite;

    @BindView(R.id.llPhone)
    LinearLayout llPhone;

    @BindView(R.id.llOpportunity)
    LinearLayout llOpportunity;

    @BindView(R.id.rvOpportunity)
    RecyclerView rvOpportunity;

    @BindView(R.id.llEvent)
    LinearLayout llEvent;

    @BindView(R.id.rvEvent)
    RecyclerView rvEvent;

    @BindView(R.id.llTask)
    LinearLayout llTask;

    @BindView(R.id.rvTask)
    RecyclerView rvTask;

    @BindView(R.id.llProduct)
    LinearLayout llProduct;

    @BindView(R.id.rvProduct)
    RecyclerView rvProduct;

    @BindView(R.id.tvNoDataProduct)
    CustomTextView tvNoDataProduct;

    @BindView(R.id.tvNoDataOpportunity)
    CustomTextView tvNoDataOpportunity;

    @BindView(R.id.tvNoDataTask)
    CustomTextView tvNoDataTask;

    @BindView(R.id.tvNoDataEvent)
    CustomTextView tvNoDataEvent;
//    @BindView(R.id.tvOpportunityMore)
//    CustomTextView tvOpportunityMore;
//    @BindView(R.id.tvProductMore)
//    CustomTextView tvProductMore;
//    @BindView(R.id.tvLeadMore)
//    CustomTextView tvLeadMore;
//    @BindView(R.id.tvTaskMore)
//    CustomTextView tvTaskMore;
    Unbinder unbinder;

    private View view;
    private Context context;
    private AccountModel model;
    private OpportunityModel opportunityModel;
    private EventModel eventModel;
    private ProductModel productModel;
    private TaskModel taskModel;
    private LinearLayout llCall, llSMS, llWhatsapp;
    private PhonecallReceiver phonecallReceiver;

    private List<OpportunityModel> opportunityList = new ArrayList<>();
    private List<EventModel> eventList = new ArrayList<>();
    private List<TaskModel> taskList = new ArrayList<>();
    private List<ProductModel> productList = new ArrayList<>();
    private OpportunitySimpleAdapter opportunitySimpleAdapter;
    private EventSimpleAdapter eventSimpleAdapter;
    private TaskSimpleAdapter taskSimpleAdapter;
    private ProductSimpleAdapter productSimpleAdapter;

    public static final int RESULT_OK = -1;

    public static AccountTabPersonalFragment newInstance() {
        AccountTabPersonalFragment fragment = new AccountTabPersonalFragment();
        return fragment;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case Sales1CRMUtils.REQUEST_PHONE_CALL: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    callNumber(model.getPhone());
                }
                return;
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Sales1CRMUtils.END_CALL:
                if (resultCode == RESULT_OK) {
                    Intent intent = new Intent(context, CallsAddActivity.class);
                    intent.putExtra("accountModel", model);
                    intent.putExtra("from_account", true);
                    startActivity(intent);
                }
            default:
                break;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_accountpersonal, container, false);

        unbinder = ButterKnife.bind(this, this.view);
        return this.view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);
        this.context = getActivity();

        initView();
    }

    private void initView() {
        try {
            if (getArguments() != null) {
                model = getArguments().getParcelable("accountUmum");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            JSONArray array = new JSONArray(model.getNew_event());

            for (int i = 0; i < array.length(); i++) {
                eventModel = new EventModel();
                eventModel.setId(array.getJSONObject(i).getString("id"));
                eventModel.setName(array.getJSONObject(i).getString("name"));
                eventModel.setDate(array.getJSONObject(i).getString("event_date"));
                eventList.add(eventModel);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            JSONArray array = new JSONArray(model.getNew_task());

            Log.d("TAG", "Task : " + model.getNew_task());

            for (int i = 0; i < array.length(); i++) {
                taskModel = new TaskModel();
                taskModel.setId(array.getJSONObject(i).getString("id"));
                taskModel.setSubject(array.getJSONObject(i).getString("subject"));
                taskModel.setDescription(array.getJSONObject(i).getString("description"));
                taskModel.setDate(array.getJSONObject(i).getString("task_date"));
                taskList.add(taskModel);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            JSONArray array = new JSONArray(model.getTop_5_product());

            for (int i = 0; i < array.length(); i++) {
                productModel = new ProductModel();
                productModel.setId(array.getJSONObject(i).getString("id_product"));
                productModel.setName(array.getJSONObject(i).getString("product_name"));
                productModel.setNote(array.getJSONObject(i).getString("note"));
                productList.add(productModel);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            JSONArray array = new JSONArray(model.getTop_opportunity());
            Log.d("TAG", "opportunity : " + model.getTop_opportunity());
            Log.d("TAG", "opportunity array : " + array.length());
            for (int i = 0; i < array.length(); i++) {
                opportunityModel = new OpportunityModel();
                opportunityModel.setId(array.getJSONObject(i).getString("id"));
                opportunityModel.setName(array.getJSONObject(i).getString("NAME"));
                opportunityModel.setSize(array.getJSONObject(i).getString("opportunity_size"));
                opportunityModel.setClose_date(array.getJSONObject(i).getString("close_date"));
                opportunityList.add(opportunityModel);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        tvAccountType.setText(model.getType_name());
        tvIndustry.setText(model.getIndustry_name());
        tvAssignTo.setText(model.getAssign_to_name());
        tvTotalEmployee.setText(model.getTotal_employee() + " People");
        tvDescription.setText(model.getDescription());

        if (!model.getWebsite().equalsIgnoreCase(""))
            tvWebsite.setText(model.getWebsite());
        else
            llWebsite.setVisibility(View.GONE);

        if (!model.getPhone().equalsIgnoreCase("")) {
            tvPhone.setText(model.getPhone());
        } else {
            llPhone.setVisibility(View.GONE);
        }

        if (!model.getEmail().equalsIgnoreCase("")) {
            tvEmail.setText(model.getEmail());
        } else {
            llEmail.setVisibility(View.GONE);
        }

        if (taskList.size() > 0) {
            setTaskList();
        } else {
            rvTask.setVisibility(View.GONE);
            tvNoDataTask.setVisibility(View.VISIBLE);
        }

        if (eventList.size() > 0) {
            setEventList();
        } else {
            rvEvent.setVisibility(View.GONE);
            tvNoDataEvent.setVisibility(View.VISIBLE);
        }

        if (productList.size() > 0) {
            setProductList();
        } else {
            rvProduct.setVisibility(View.GONE);
            tvNoDataProduct.setVisibility(View.VISIBLE);
        }
        if (opportunityList.size() > 0) {
            setOpportunityList();
        } else {
            rvOpportunity.setVisibility(View.GONE);
            tvNoDataOpportunity.setVisibility(View.VISIBLE);
        }

        llEmail.setOnClickListener(click);
        llWebsite.setOnClickListener(click);
        llPhone.setOnClickListener(click);
    }

    private void setOpportunityList() {
        //Make Listview Horizontal
        LinearLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL,false);
        rvOpportunity.setLayoutManager(layoutManager);

        opportunitySimpleAdapter = new OpportunitySimpleAdapter(context, this.opportunityList);
        rvOpportunity.setAdapter(opportunitySimpleAdapter);

    }

    private void setEventList() {
        //Make Listview Horizontal
        LinearLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        rvEvent.setLayoutManager(layoutManager);

        eventSimpleAdapter = new EventSimpleAdapter(context, this.eventList);
        rvEvent.setAdapter(eventSimpleAdapter);
    }

    private void setTaskList() {
        //Make Listview Horizontal
        LinearLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        rvTask.setLayoutManager(layoutManager);

        taskSimpleAdapter = new TaskSimpleAdapter(context, this.taskList);
        rvTask.setAdapter(taskSimpleAdapter);
    }

    private void setProductList() {
        //Make Listview Horizontal
        LinearLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        rvProduct.setLayoutManager(layoutManager);

        productSimpleAdapter = new ProductSimpleAdapter(context, this.productList);
        rvProduct.setAdapter(productSimpleAdapter);
    }


    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.llEmail:
                    if (!model.getEmail().equalsIgnoreCase("")) {
                        String mailto = "mailto:" + model.getEmail();

                        Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
                        emailIntent.setData(Uri.parse(mailto));

                        try {
                            startActivity(Intent.createChooser(emailIntent, "Pilih Email"));
                        } catch (ActivityNotFoundException e) {
                            //TODO: Handle case where no email app is available
                        }
                    } else {
                        llEmail.setVisibility(View.GONE);
                    }
                    break;
                case R.id.llWebsite:
                    String url = model.getWebsite();
                    Log.i("AAA", "url : " + url);
                    if (!url.equals("")) {
                        if (!url.startsWith("http://") && !url.startsWith("https://")) {
                            url = "http://" + url;
                        }
                        Log.i("AAA", "new url : " + url);
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                        startActivity(Intent.createChooser(browserIntent, "Pilih Browser"));
                    } else {
                        llWebsite.setVisibility(View.GONE);
                    }
                    break;
                case R.id.llPhone:
                    final Dialog dialog = new Dialog(context);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.dialog_call);

                    llCall = (LinearLayout) dialog
                            .findViewById(R.id.llCall);
                    llSMS = (LinearLayout) dialog
                            .findViewById(R.id.llSMS);
                    llWhatsapp = (LinearLayout) dialog
                            .findViewById(R.id.llWhatsapp);

                    llCall.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (ContextCompat.checkSelfPermission(context,
                                    Manifest.permission.CALL_PHONE)
                                    != PackageManager.PERMISSION_GRANTED) {

                                ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.CALL_PHONE}, Sales1CRMUtils.REQUEST_PHONE_CALL);
                            } else {
                                callNumber(model.getPhone());
                            }
                            dialog.dismiss();
                        }
                    });

                    llSMS.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            sendSMS(model.getPhone());
                            dialog.dismiss();
                        }
                    });

                    llWhatsapp.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent();
                            openWhatsApp(model.getPhone());
                        }
                    });
                    dialog.show();
                    break;
            }
        }
    };

    private void openWhatsApp(String number) {
        final String appPackageName = "com.whatsapp";

        if (Sales1CRMUtils.Utils.appInstalledOrNot(context, appPackageName)) {
            number = PhoneNumberUtils.formatNumber(number).replace("-", "");
            if (number.substring(0, 2).equalsIgnoreCase("08")) {
                number = "62" + number.substring(1);
            } else {
                number = number.replace(" ", "").replace("+", "");
            }

            Log.d("TAG", "Number : " + number);
            try {
                Intent sendIntent = new Intent("android.intent.action.MAIN");
                sendIntent.setComponent(new ComponentName("com.whatsapp", "com.whatsapp.Conversation"));
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.setType("text/plain");
                sendIntent.putExtra(Intent.EXTRA_TEXT, "");
                sendIntent.putExtra("jid", number + "@s.whatsapp.net");
                sendIntent.setPackage("com.whatsapp");
                startActivity(sendIntent);
            } catch (Exception e) {
                Toast.makeText(context, "Error/n" + e.toString(), Toast.LENGTH_SHORT).show();
            }
        } else {
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
            } catch (ActivityNotFoundException anfe) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
            }
        }
    }

    private void callNumber(String number) {
        PackageManager packageManager = context.getPackageManager();
        if (packageManager.hasSystemFeature(PackageManager.FEATURE_TELEPHONY)) {
            // call phone
            Intent intent = new Intent(Intent.ACTION_CALL);

            intent.setData(Uri.parse("tel:" + number));

            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            startActivityForResult(intent, Sales1CRMUtils.END_CALL);
//            startActivity(intent);
        }
    }

    private void sendSMS(String number) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("smsto:" + Uri.encode(number)));
        startActivity(intent);
    }

    @Override
    public void onStart() {
        IntentFilter intent = new IntentFilter("android.intent.action.PHONE");

        phonecallReceiver = new PhonecallReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d("TAGG", "MASUK >>>");
                Intent i = new Intent(context, AccountDetailActivity.class);
                i.putExtra("accountModel", model);
                i.putExtra("from_account", true);
                startActivity(i);
            }
        };
        context.registerReceiver(phonecallReceiver, intent);
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();

//        BroadcastListener receiver = new BroadcastListener();
//
//        Intent intent = new Intent();
//        intent.setAction("android.intent.action.NEW_OUTGOING_CALL");
//        intent.putExtra("android.intent.extra.PHONE_NUMBER", model.getPhone());
//        receiver.onReceive(context, intent);
    }

    @Override
    public void onDestroy() {
        context.unregisterReceiver(phonecallReceiver);
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    //    private final BroadcastReceiver CallReceiver = new BroadcastReceiver() {
//        public void onReceive(Context context, Intent intent) {
//            AudioManager amanager=(AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
//            amanager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
//
//            String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
//
//            if (TelephonyManager.EXTRA_STATE_IDLE.equals(state))
//            {
//                try {
//                    Thread.sleep(1000);
//                } catch (InterruptedException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                }
//                amanager.setRingerMode(0);  //Ringer Silent
//            }
//            if (TelephonyManager.EXTRA_STATE_OFFHOOK.equals(state))
//            {
//                try {
//                    Thread.sleep(1000);
//                } catch (InterruptedException e) {
//                    // TODO Auto-generated catch block
//                    e.printStackTrace();
//                }
//                amanager.setRingerMode(0);  ////Ringer Silent
//            }
//            if (TelephonyManager.EXTRA_STATE_RINGING.equals(state))
//            {
//                amanager.setRingerMode(2);  //Ringer ON
//            }
//        }
//    };


//    PhonecallReceiver phonecallReceiver = new PhonecallReceiver() {
//
//        @Override
//        public void onIncomingCallStarted(String number, Date start) {
//            Log.d("TAG", "OutGoing Call1");
//
//        }
//
//        @Override
//        public void onOutgoingCallStarted(String number, Date start) {
//            Log.d("TAG", "OutGoing Call2");
//
//        }
//
//        @Override
//        public void onIncomingCallEnded(String number, Date start, Date end) {
//            Log.d("TAG", "OutGoing Call3");
//
//        }
//
//        @Override
//        public void onOutgoingCallEnded(String number, Date start, Date end) {
//            Log.d("TAG", "OutGoing Call4");
//            Intent intent = new Intent(context, CallsAddActivity.class);
//            intent.putExtra("accountModel", model);
//            intent.putExtra("from_account", true);
//            startActivity(intent);
//        }
//
//        @Override
//        public void onMissedCall(String number, Date start) {
//            Log.d("TAG", "OutGoing Call5");
//
//        }
//    };
}

