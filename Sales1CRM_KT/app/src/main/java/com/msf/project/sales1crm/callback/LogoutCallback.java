package com.msf.project.sales1crm.callback;

public interface LogoutCallback {
    void finishLogout(String result, String response);
}
