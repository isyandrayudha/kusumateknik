package com.msf.project.sales1crm.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

public class ActivityEventModel implements Comparable, Parcelable{
	private String date;
	private String microsoftCalendarId;
	private String assignTo;
	private String description;
	private String createdAt;
	private String deletedBy;
	private String createdBy;
	private String deletedAt;
	private String googleCalendarId;
	private String accountId;
	private String updatedAt;
	private String name;
	private String updatedBy;
	private String id;
	private String status;
	private String user_name;
	private String account_name;
	private String photo_1;
	private String photo_2;
	private String photo_3;
	private String nama1;
	private String nama2;
	private String nama3;
	private String event_type_id;
	private String event_purpose_id;
	private String event_type_name;
	private String event_purpose_name;
	private String followUp;
	private String id_product_event;
	private String product_status;
	private String meet_to;
	private String brand;
	private String type;
	private String serial_number;
	private String condition;
	private String assigntTo_name;
	private String product;


	public ActivityEventModel() {
		setDate("");
		setMicrosoftCalendarId("");
		setAssignTo("");
		setDescription("");
		setCreatedAt("");
		setDeletedBy("");
		setCreatedBy("");
		setDeletedAt("");
		setGoogleCalendarId("");
		setAccountId("");
		setUpdatedAt("");
		setName("");
		setUpdatedBy("");
		setId("");
		setStatus("");
		setUser_name("");
		setAccount_name("");
		setPhoto_1("");
		setPhoto_2("");
		setPhoto_3("");
		setNama1("");
		setNama2("");
		setNama2("");
		setNama3("");
		setEvent_type_id("");
		setEvent_purpose_id("");
		setEvent_type_id("");
		setEvent_purpose_name("");
		setFollowUp("");
		setId_product_event("");
		setProduct_status("");
		setMeet_to("");
		setBrand("");
		setType("");
		setSerial_number("");
		setCondition("");
		setProduct("");

	}

	public ActivityEventModel(Parcel in) {
		readFromParcel(in);
	}

	public void readFromParcel(Parcel in) {
		date = in.readString();
		microsoftCalendarId = in.readString();
		assignTo = in.readString();
		description = in.readString();
		createdAt = in.readString();
		deletedBy = in.readString();
		createdBy = in.readString();
		deletedAt = in.readString();
		googleCalendarId = in.readString();
		accountId = in.readString();
		updatedAt = in.readString();
		name = in.readString();
		updatedBy = in.readString();
		id = in.readString();
		status = in.readString();
		user_name = in.readString();
		account_name = in.readString();
		photo_1 = in.readString();
		photo_2 = in.readString();
		photo_3 = in.readString();
		nama1 = in.readString();
		nama2 = in.readString();
		nama3 = in.readString();
		event_purpose_id = in.readString();
		event_purpose_name = in.readString();
		event_type_id = in.readString();
		event_type_name = in.readString();
		followUp = in.readString();
		id_product_event = in.readString();
		product_status = in.readString();
		meet_to = in.readString();
		brand = in.readString();
		type = in.readString();
		serial_number = in.readString();
		condition = in.readString();
		product = in.readString();
	}

	public static final Creator<ActivityEventModel> CREATOR = new Creator<ActivityEventModel>() {
		@Override
		public ActivityEventModel createFromParcel(Parcel parcel) {
			return new ActivityEventModel(parcel);
		}

		@Override
		public ActivityEventModel[] newArray(int i) {
			return new ActivityEventModel[i];
		}
	};

	public void setDate(String date){
		this.date = date;
	}

	public String getDate(){
		return date;
	}

	public void setMicrosoftCalendarId(String microsoftCalendarId){
		this.microsoftCalendarId = microsoftCalendarId;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public String getAccount_name() {
		return account_name;
	}

	public void setAccount_name(String account_name) {
		this.account_name = account_name;
	}

	public String getMicrosoftCalendarId(){
		return microsoftCalendarId;
	}

	public void setAssignTo(String assignTo){
		this.assignTo = assignTo;
	}

	public String getAssignTo(){
		return assignTo;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setDeletedBy(String deletedBy){
		this.deletedBy = deletedBy;
	}

	public String getDeletedBy(){
		return deletedBy;
	}

	public void setCreatedBy(String createdBy){
		this.createdBy = createdBy;
	}

	public String getCreatedBy(){
		return createdBy;
	}

	public void setDeletedAt(String deletedAt){
		this.deletedAt = deletedAt;
	}

	public String getDeletedAt(){
		return deletedAt;
	}

	public void setGoogleCalendarId(String googleCalendarId){
		this.googleCalendarId = googleCalendarId;
	}

	public String getGoogleCalendarId(){
		return googleCalendarId;
	}

	public void setAccountId(String accountId){
		this.accountId = accountId;
	}

	public String getAccountId(){
		return accountId;
	}

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setUpdatedBy(String updatedBy){
		this.updatedBy = updatedBy;
	}

	public String getUpdatedBy(){
		return updatedBy;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	public String getPhoto_1() {
		return photo_1;
	}

	public void setPhoto_1(String photo_1) {
		this.photo_1 = photo_1;
	}

	public String getPhoto_2() {
		return photo_2;
	}

	public void setPhoto_2(String photo_2) {
		this.photo_2 = photo_2;
	}

	public String getPhoto_3() {
		return photo_3;
	}

	public void setPhoto_3(String photo_3) {
		this.photo_3 = photo_3;
	}

	public String getNama1() {
		return nama1;
	}

	public void setNama1(String nama1) {
		this.nama1 = nama1;
	}

	public String getNama2() {
		return nama2;
	}

	public void setNama2(String nama2) {
		this.nama2 = nama2;
	}

	public String getNama3() {
		return nama3;
	}

	public void setNama3(String nama3) {
		this.nama3 = nama3;
	}

	public String getEvent_type_id() {
		return event_type_id;
	}

	public void setEvent_type_id(String event_type_id) {
		this.event_type_id = event_type_id;
	}

	public String getEvent_purpose_id() {
		return event_purpose_id;
	}

	public void setEvent_purpose_id(String event_purpose_id) {
		this.event_purpose_id = event_purpose_id;
	}

	public String getEvent_type_name() {
		return event_type_name;
	}

	public void setEvent_type_name(String event_type_name) {
		this.event_type_name = event_type_name;
	}

	public String getEvent_purpose_name() {
		return event_purpose_name;
	}

	public void setEvent_purpose_name(String event_purpose_name) {
		this.event_purpose_name = event_purpose_name;
	}

	public String getFollowUp() {
		return followUp;
	}

	public void setFollowUp(String followUp) {
		this.followUp = followUp;
	}

	public String getId_product_event() {
		return id_product_event;
	}

	public void setId_product_event(String id_product_event) {
		this.id_product_event = id_product_event;
	}

	public String getProduct_status() {
		return product_status;
	}

	public void setProduct_status(String product_status) {
		this.product_status = product_status;
	}

	public String getMeet_to() {
		return meet_to;
	}

	public void setMeet_to(String meet_to) {
		this.meet_to = meet_to;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSerial_number() {
		return serial_number;
	}

	public void setSerial_number(String serial_number) {
		this.serial_number = serial_number;
	}

	public String getCondition() {
		return condition;
	}

	public void setCondition(String condition) {
		this.condition = condition;
	}

	public String getAssigntTo_name() {
		return assigntTo_name;
	}

	public void setAssigntTo_name(String assigntTo_name) {
		this.assigntTo_name = assigntTo_name;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int i) {
		parcel.writeString(date);
		parcel.writeString(microsoftCalendarId);
		parcel.writeString(assignTo);
		parcel.writeString(description);
		parcel.writeString(createdAt);
		parcel.writeString(deletedBy);
		parcel.writeString(createdBy);
		parcel.writeString(deletedAt);
		parcel.writeString(googleCalendarId);
		parcel.writeString(accountId);
		parcel.writeString(updatedAt);
		parcel.writeString(name);
		parcel.writeString(updatedBy);
		parcel.writeString(id);
		parcel.writeString(status);
		parcel.writeString(user_name);
		parcel.writeString(account_name);
		parcel.writeString(photo_1);
		parcel.writeString(photo_2);
		parcel.writeString(photo_3);
		parcel.writeString(nama1);
		parcel.writeString(nama2);
		parcel.writeString(nama3);
		parcel.writeString(event_purpose_id);
		parcel.writeString(event_purpose_name);
		parcel.writeString(event_type_id);
		parcel.writeString(event_type_name);
		parcel.writeString(followUp);
		parcel.writeString(id_product_event);
		parcel.writeString(product_status);
		parcel.writeString(meet_to);
		parcel.writeString(brand);
		parcel.writeString(type);
		parcel.writeString(serial_number);
		parcel.writeString(condition);
		parcel.writeString(product);
	}

	@Override
	public int compareTo(@NonNull Object o) {
		return 0;
	}
}
