package com.msf.project.sales1crm.utility;

import android.content.Context;

import com.google.gson.JsonIOException;
import com.msf.project.sales1crm.database.AccountTypeDao;
import com.msf.project.sales1crm.database.DBHelper;
import com.msf.project.sales1crm.database.EventPurposeDao;
import com.msf.project.sales1crm.database.EventStatusDao;
import com.msf.project.sales1crm.database.FunctionDao;
import com.msf.project.sales1crm.database.IndustryDao;
import com.msf.project.sales1crm.database.LeadSourceDao;
import com.msf.project.sales1crm.database.LeadStatusDao;
import com.msf.project.sales1crm.database.LocationUserDao;
import com.msf.project.sales1crm.database.NoOrderDao;
import com.msf.project.sales1crm.database.NoVisitDao;
import com.msf.project.sales1crm.database.OpportunityStageDao;
import com.msf.project.sales1crm.database.OpportunityStatusDao;
import com.msf.project.sales1crm.database.PositionDao;
import com.msf.project.sales1crm.database.UseForDao;
import com.msf.project.sales1crm.database.UserDao;
import com.msf.project.sales1crm.model.AbsenceModel;
import com.msf.project.sales1crm.model.AccountAddressModel;
import com.msf.project.sales1crm.model.AccountModel;
import com.msf.project.sales1crm.model.AccountTypeModel;
import com.msf.project.sales1crm.model.ActivityCallsModel;
import com.msf.project.sales1crm.model.ActivityEventModel;
import com.msf.project.sales1crm.model.ActivityTodoModel;
import com.msf.project.sales1crm.model.CallsModel;
import com.msf.project.sales1crm.model.CategoryModel;
import com.msf.project.sales1crm.model.ContactModel;
import com.msf.project.sales1crm.model.DashboardFirstModel;
import com.msf.project.sales1crm.model.DashboardSecondModel;
import com.msf.project.sales1crm.model.DashboardThirdModel;
import com.msf.project.sales1crm.model.DatabaseMachineModel;
import com.msf.project.sales1crm.model.EventModel;
import com.msf.project.sales1crm.model.EventPurposeModel;
import com.msf.project.sales1crm.model.EventStatusModel;
import com.msf.project.sales1crm.model.FunctionModel;
import com.msf.project.sales1crm.model.IndustryModel;
import com.msf.project.sales1crm.model.KursModel;
import com.msf.project.sales1crm.model.LeadConvertModel;
import com.msf.project.sales1crm.model.LeadModel;
import com.msf.project.sales1crm.model.LeadSourceModel;
import com.msf.project.sales1crm.model.LeadStatusModel;
import com.msf.project.sales1crm.model.LocationModel;
import com.msf.project.sales1crm.model.LocationUserModel;
import com.msf.project.sales1crm.model.NoOrderModel;
import com.msf.project.sales1crm.model.NoVisitModel;
import com.msf.project.sales1crm.model.OpportunityBillingModel;
import com.msf.project.sales1crm.model.OpportunityModel;
import com.msf.project.sales1crm.model.OpportunityStageModel;
import com.msf.project.sales1crm.model.OpportunityStatusModel;
import com.msf.project.sales1crm.model.PieChartModel;
import com.msf.project.sales1crm.model.PositionModel;
import com.msf.project.sales1crm.model.PriceModel;
import com.msf.project.sales1crm.model.ProductModel;
import com.msf.project.sales1crm.model.QuotationModel;
import com.msf.project.sales1crm.model.ReasonModel;
import com.msf.project.sales1crm.model.SalesCycleModel;
import com.msf.project.sales1crm.model.TaskModel;
import com.msf.project.sales1crm.model.Top10CustomerModel;
import com.msf.project.sales1crm.model.TotalModel;
import com.msf.project.sales1crm.model.TotalNPriceOpporModel;
import com.msf.project.sales1crm.model.TotalOpportunityModel;
import com.msf.project.sales1crm.model.UseForModel;
import com.msf.project.sales1crm.model.UserModel;
import com.msf.project.sales1crm.model.VpModel;
import com.msf.project.sales1crm.model.VpOppor;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by christianmacbook on 11/05/18.
 */

public class Sales1CRMUtilsJSON {
    public static class JSONUtility {

        public static String getResultFromServer(String json) throws JSONException {
            String result = "";

            JSONObject jsonObject = new JSONObject(json);
            result = jsonObject.getString("result");

            return result;
        }

        public static String getDomainFromServer(String json) throws JSONException {
            String domain = "";

            JSONObject jsonObject = new JSONObject(json);
            domain = jsonObject.getJSONObject("datas").
                    getJSONArray("list").getJSONObject(0).
                    getString("domain");

            return domain;
        }

        public static UserModel getDisplayImage(String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);
            UserModel model = new UserModel();
            model.setImage_master(jsonObject.getJSONObject("datas").getJSONArray("list").
                    getJSONObject(0).getString("image_master"));

            return model;
        }

        public static String getContactFromServer(String json) throws JSONException {
            String result = "";
            JSONObject jsonObject = new JSONObject(json);
            result = jsonObject.getJSONObject("datas").getString("account_contact_id");
            return result;
        }


        public static String getPermissionService(String json) throws JSONException {
            String permission = "";

            JSONObject jsonObject = new JSONObject(json);
            permission = jsonObject.getJSONObject("datas").
                    getJSONArray("list").getJSONObject(0).
                    getString("permission");
            return permission;
        }

        public static String getLeadIdFromServer(String json) throws JSONException {
            String result ="";
            JSONObject  jsonObject = new JSONObject(json);
            result = jsonObject.getJSONObject("datas").getString("lead_id");
            return result;
        }

        public static String getIdFromServer(String json) throws JSONException {
            String result = "";

            JSONObject jsonObject = new JSONObject(json);
            result = jsonObject.getString("id");

            return result;
        }

        public static String getResponseFromServer(String json) throws JSONException {
            String result = "";

            JSONObject jsonObject = new JSONObject(json);
            result = jsonObject.getString("error_message");

            return result;
        }

        public static List<KursModel> getKursList(String json) throws JSONException {
            List<KursModel> list = new ArrayList<>();

            JSONObject jsonObject = new JSONObject(json);
            try{
                JSONArray jsonArray = jsonObject.getJSONArray("datas");
                for(int i = 0; i < jsonArray.length(); i++ ){
                    KursModel model = new KursModel();
                    model.setCurrencyId(jsonArray.getJSONObject(i).getString("currency_id"));
                    model.setCurrencyName(jsonArray.getJSONObject(i).getString("currency_name"));
                    model.setKurs(jsonArray.getJSONObject(i).getString("kurs"));
                    list.add(model);
                }
            } catch (JSONException e){
                e.printStackTrace();
            }
            return list;
        }

        public static List<ReasonModel> getReason(String json) throws JSONException {
            List<ReasonModel> list = new ArrayList<>();
            JSONObject jsonObject = new JSONObject(json);
            try {
                JSONArray jsonArray = jsonObject.getJSONArray("datas");
                for(int i = 0; i < jsonArray.length(); i++){
                    ReasonModel model = new ReasonModel();
                    model.setId(jsonArray.getJSONObject(i).getString("id"));
                    model.setReason(jsonArray.getJSONObject(i).getString("reason"));
                    list.add(model);
                }
            } catch (JSONException e){
                e.printStackTrace();
            }
            return list;
        }

//        public static void getReason(Context context, String json) throws JSONException {
//            JSONObject jsonObject = new JSONObject(json);
//                JSONArray jsonArray = jsonObject.getJSONArray("datas");
//                for(int i = 0; i < jsonArray.length(); i++){
//                    ReasonModel model = new ReasonModel();
//                    model.setId(jsonArray.getJSONObject(i).getString("id"));
//                    model.setReason(jsonArray.getJSONObject(i).getString("reason"));
//                    model.setStat("1");
//                    new ReasonDao(context).insertTable(model);
//                }
//        }

        public static List<QuotationModel> getQuotationList(String json) throws JSONException {
            List<QuotationModel> list = new ArrayList<>();

            JSONObject jsonObject = new JSONObject(json);
            JSONArray array = new JSONArray(jsonObject.getJSONObject("datas").getString("quotation"));
            for(int i = 0; i < array.length(); i++){
                QuotationModel model = new QuotationModel();
                model.setQuotationId(array.getJSONObject(i).getString("quotation_id"));
                model.setReferencesNumber(array.getJSONObject(i).getString("references_number"));
                model.setAmount(array.getJSONObject(i).getString("amount"));
                model.setCreateDate(array.getJSONObject(i).getString("create_date"));
                model.setProductCount(array.getJSONObject(i).getString("product_count"));
                list.add(model);
            }
            return list;
        }

        public static List<AccountModel> getAccountList(String json) throws JSONException {
            List<AccountModel> list = new ArrayList<>();

            JSONObject jsonObject = new JSONObject(json);
            JSONArray array = new JSONArray(jsonObject.getJSONObject("datas").getString("accounts"));
            for (int i = 0; i < array.length(); i++) {
                AccountModel model = new AccountModel();
                model.setId(array.getJSONObject(i).getString("id"));
                model.setAccount_code(array.getJSONObject(i).getString("account_code"));
                model.setAccount_name(array.getJSONObject(i).getString("name"));
                model.setImageUrl(array.getJSONObject(i).getString("image_master"));
                model.setEmail(array.getJSONObject(i).getString("email"));
                model.setPhone(array.getJSONObject(i).getString("phone"));
                model.setWebsite(array.getJSONObject(i).getString("website"));
                model.setDescription(array.getJSONObject(i).getString("description"));
                model.setType_id(array.getJSONObject(i).getString("type_id"));
                model.setType_name(array.getJSONObject(i).getString("type_name"));
                model.setIndustry_id(array.getJSONObject(i).getString("industry_id"));
                model.setIndustry_name(array.getJSONObject(i).getString("industry"));
                model.setOwner_name(array.getJSONObject(i).getString("owner_name"));
                model.setTotal_employee(array.getJSONObject(i).getString("total_employee"));
                model.setLast_update_name(array.getJSONObject(i).getString("last_updated_name"));
                model.setAddress_billing(array.getJSONObject(i).getString("billing"));
                model.setAddress_shipping(array.getJSONObject(i).getString("shipping"));
                model.setRelated_people(array.getJSONObject(i).getString("related_people"));
                model.setTop_opportunity(array.getJSONObject(i).getString("opportunity"));
                model.setTop_5_product(array.getJSONObject(i).getString("top_5_product"));
                model.setNew_event(array.getJSONObject(i).getString("crm_event"));
                model.setNew_task(array.getJSONObject(i).getString("crm_task"));
                model.setCount_opportunity(array.getJSONObject(i).getString("opportunity_count"));
                model.setCount_task(array.getJSONObject(i).getString("crm_task_count"));
                model.setCount_event(array.getJSONObject(i).getString("crm_event_count"));
                model.setAssign_to(array.getJSONObject(i).getString("assign_to"));
                model.setAssign_to_name(array.getJSONObject(i).getString("assign_to_name"));
                model.setBilling(new AccountAddressModel(
                        array.getJSONObject(i).getJSONObject("billing").getString("id"),
                        array.getJSONObject(i).getJSONObject("billing").getString("alamat"),
                        array.getJSONObject(i).getJSONObject("billing").getString("latitude"),
                        array.getJSONObject(i).getJSONObject("billing").getString("longitude")));
                list.add(model);
            }

            return list;
        }

        public static List<ProductModel> getProductList(String json) throws JSONException {
            List<ProductModel> list = new ArrayList<>();

            JSONObject jsonObject = new JSONObject(json);
            JSONArray array = new JSONArray(jsonObject.getJSONObject("datas").getString("detail"));
            for (int i = 0; i < array.length(); i++) {
                ProductModel model = new ProductModel();
                model.setId(array.getJSONObject(i).getString("product_id"));
                model.setDetail_id(array.getJSONObject(i).getString("id"));
                model.setName(array.getJSONObject(i).getString("product_name"));
                model.setCategory_name(array.getJSONObject(i).getString("category_name"));
                model.setCategory_id(array.getJSONObject(i).getString("category_id"));
                model.setGroup_id(array.getJSONObject(i).getString("product_group_id"));
                model.setGroup_name(array.getJSONObject(i).getString("product_group_name"));
                model.setBranch_name(array.getJSONObject(i).getString("branch_name"));
                model.setBranch_id(array.getJSONObject(i).getString("branch"));
                model.setPrice(array.getJSONObject(i).getString("price"));
                model.setStock(array.getJSONObject(i).getString("stock"));
                model.setUnit(array.getJSONObject(i).getString("unit"));
                model.setNote(array.getJSONObject(i).getString("description"));
                model.setShow_stock(array.getJSONObject(i).getString("show_stock"));
                model.setShow_price(array.getJSONObject(i).getString("show_price"));
                model.setCurrency_name(array.getJSONObject(i).getString("currency_name"));
                model.setDescription(array.getJSONObject(i).getString("description"));
                list.add(model);
            }

            return list;
        }

        public static List<CategoryModel> getCategoryList(String json) throws JSONException {
            List<CategoryModel> list = new ArrayList<>();
            JSONObject jsonObject = new JSONObject(json);
            JSONArray array = new JSONArray(jsonObject.getJSONObject("datas").getString("category"));
            for(int i = 0; i< array.length(); i++){
                CategoryModel model = new CategoryModel();
                model.setId(array.getJSONObject(i).getString("id"));
                model.setBranch(array.getJSONObject(i).getString("branch"));
                model.setName(array.getJSONObject(i).getString("name"));
                model.setDescription(array.getJSONObject(i).getString("description"));
                model.setStatus(array.getJSONObject(i).getString("status"));
                list.add(model);
            }
            return list;
        }

        public static List<ContactModel> getContactList(String json) throws JSONException {
            List<ContactModel> list = new ArrayList<>();

            JSONObject jsonObject = new JSONObject(json);
            JSONArray array = new JSONArray(jsonObject.getJSONObject("datas").getString("related_people"));
            for (int i = 0; i < array.length(); i++) {
                ContactModel model = new ContactModel();
                model.setId(array.getJSONObject(i).getString("id"));
                model.setFullname(array.getJSONObject(i).getString("first_name") + " " + array.getJSONObject(i).getString("last_name"));
                model.setFirst_name(array.getJSONObject(i).getString("first_name"));
                model.setLast_name(array.getJSONObject(i).getString("last_name"));
//                model.setTitle(array.getJSONObject(i).getString("title"));
                model.setPosition(array.getJSONObject(i).getString("position"));
                model.setPhone(array.getJSONObject(i).getString("phone"));
                model.setAccount_id(array.getJSONObject(i).getString("account_id"));
                model.setEmail(array.getJSONObject(i).getString("email"));
                list.add(model);
            }
            return list;
        }

        public static List<LeadModel> getLeadList(String json) throws JSONException {
            List<LeadModel> list = new ArrayList<>();

            JSONObject jsonObject = new JSONObject(json);
            JSONArray array = new JSONArray(jsonObject.getJSONObject("datas").getString("leads"));
            for (int i = 0; i < array.length(); i++) {
                LeadModel model = new LeadModel();
                model.setId(array.getJSONObject(i).getString("id"));
                model.setFirst_name(array.getJSONObject(i).getString("first_name"));
                model.setLast_name(array.getJSONObject(i).getString("last_name"));
                model.setTitle_id(array.getJSONObject(i).getString("title"));
                model.setCompany(array.getJSONObject(i).getString("company"));
                model.setPosition_id(array.getJSONObject(i).getString("position_id"));
                model.setPosition_name(array.getJSONObject(i).getString("position"));
                model.setImage_url(array.getJSONObject(i).getString("image_master"));
                model.setEmail(array.getJSONObject(i).getString("email"));
                model.setPhone(array.getJSONObject(i).getString("phone"));
                model.setDescription(array.getJSONObject(i).getString("description"));
                model.setIndustry_id(array.getJSONObject(i).getString("industry_id"));
                model.setIndustry_name(array.getJSONObject(i).getString("industry"));
                model.setTotal_employee(array.getJSONObject(i).getString("total_employee"));
                model.setLead_source_id(array.getJSONObject(i).getString("lead_source_id"));
                model.setLead_source_name(array.getJSONObject(i).getString("lead_source"));
                model.setLead_status_id(array.getJSONObject(i).getString("lead_status_id"));
                model.setLead_status_name(array.getJSONObject(i).getString("lead_status"));
                model.setCreate_date(array.getJSONObject(i).getString("create_date"));
                model.setOwner_name(array.getJSONObject(i).getString("owner_name"));
                model.setDistance(array.getJSONObject(i).getString("distance"));
                model.setCount_date(array.getJSONObject(i).getString("count_date"));
                model.setAssign_to(array.getJSONObject(i).getString("assign_to"));
                model.setAssign_to_name(array.getJSONObject(i).getString("assign_to_name"));
                //Address
                model.setAddress(array.getJSONObject(i).getString("street"));
                model.setProvinsi_id(array.getJSONObject(i).getString("provinsi"));
                model.setProvinsi_name(array.getJSONObject(i).getString("provinsi_name"));
                model.setKabupaten_id(array.getJSONObject(i).getString("kabupaten"));
                model.setKabupaten_name(array.getJSONObject(i).getString("kabupaten_name"));
                model.setKecamatan_id(array.getJSONObject(i).getString("kecamatan"));
                model.setKecamatan_name(array.getJSONObject(i).getString("kecamatan_name"));
                model.setKelurahan_id(array.getJSONObject(i).getString("kelurahan"));
                model.setKelurahan_name(array.getJSONObject(i).getString("kelurahan_name"));
                model.setKodepos(array.getJSONObject(i).getString("kodepos_name"));
                model.setLongitude(array.getJSONObject(i).getString("longitude"));
                model.setLatitude(array.getJSONObject(i).getString("latitude"));
                list.add(model);
            }

            return list;
        }

        public static List<EventModel> getEventList(String json) throws JSONException {
            List<EventModel> list = new ArrayList<>();

            JSONObject jsonObject = new JSONObject(json);
            JSONArray array = new JSONArray(jsonObject.getJSONObject("datas").getString("events"));
            for (int i = 0; i < array.length(); i++) {
                EventModel model = new EventModel();
                model.setId(array.getJSONObject(i).getString("id"));
                model.setName(array.getJSONObject(i).getString("name"));
                model.setAccount_id(array.getJSONObject(i).getString("account_id"));
                model.setAssignto(array.getJSONObject(i).getString("assign_to"));
                model.setAssignto_name(array.getJSONObject(i).getString("assign_to_name"));
                model.setDate(array.getJSONObject(i).getString("date"));
                model.setDescription(array.getJSONObject(i).getString("description"));
                model.setNama1(array.getJSONObject(i).getString("nama_photo_satu"));
                model.setNama2(array.getJSONObject(i).getString("nama_photo_dua"));
                model.setNama3(array.getJSONObject(i).getString("nama_photo_tiga"));
                model.setPhoto_1(array.getJSONObject(i).getString("photo_satu"));
                model.setPhoto_2(array.getJSONObject(i).getString("photo_dua"));
                model.setPhoto_3(array.getJSONObject(i).getString("photo_tiga"));
                model.setCreated_at(array.getJSONObject(i).getString("created_at"));
                model.setAccount_name(array.getJSONObject(i).getString("account_name"));
                model.setFollow_up(array.getJSONObject(i).getString("follow_up_action"));
                model.setMeet_to(array.getJSONObject(i).getString("meet_to"));
                model.setProduct(array.getJSONObject(i).getString("product"));
                model.setDate_interval(array.getJSONObject(i).getString("date_interval"));
                list.add(model);
            }

            return list;
        }

        public static List<LocationUserModel> getOfflineLocation(Context context) {
            List<LocationUserModel> historyList = new ArrayList<>();
            LocationUserDao locationUserDao = new LocationUserDao(new DBHelper(context), false);

            historyList = locationUserDao.getLocationUserList();

            return historyList;
        }


        public static List<ActivityEventModel> getActivityEventList(String json) throws JSONException {
            List<ActivityEventModel> activityEventModelList = new ArrayList<>();

            JSONObject jsonObject = new JSONObject(json);
            JSONArray array = new JSONArray(jsonObject.getJSONObject("datas").getString("list"));
            for (int i = 0; i < array.length(); i++) {
                ActivityEventModel model = new ActivityEventModel();
                model.setId(array.getJSONObject(i).getString("id"));
                model.setName(array.getJSONObject(i).getString("name"));
                model.setAccountId(array.getJSONObject(i).getString("account_id"));
                model.setAssignTo(array.getJSONObject(i).getString("assign_to"));
                model.setDate(array.getJSONObject(i).getString("date"));
                model.setDescription(array.getJSONObject(i).getString("description"));
                model.setStatus(array.getJSONObject(i).getString("status"));
                model.setNama1(array.getJSONObject(i).getString("nama_photo_satu"));
                model.setNama2(array.getJSONObject(i).getString("nama_photo_dua"));
                model.setNama3(array.getJSONObject(i).getString("nama_photo_tiga"));
                model.setPhoto_1(array.getJSONObject(i).getString("photo_satu"));
                model.setPhoto_2(array.getJSONObject(i).getString("photo_dua"));
                model.setPhoto_3(array.getJSONObject(i).getString("photo_tiga"));
                model.setCreatedAt(array.getJSONObject(i).getString("created_at"));
                model.setCreatedBy(array.getJSONObject(i).getString("created_by"));
                model.setUpdatedAt(array.getJSONObject(i).getString("updated_at"));
                model.setUpdatedBy(array.getJSONObject(i).getString("updated_by"));
                model.setDeletedAt(array.getJSONObject(i).getString("deleted_at"));
                model.setDeletedBy(array.getJSONObject(i).getString("deleted_by"));
                model.setUser_name(array.getJSONObject(i).getString("user_name"));
                model.setAccount_name(array.getJSONObject(i).getString("account_name"));
                model.setFollowUp(array.getJSONObject(i).getString("follow_up_action"));
                model.setMeet_to(array.getJSONObject(i).getString("meet_to"));
                model.setProduct(array.getJSONObject(i).getString("product"));
                activityEventModelList.add(model);
            }
            return activityEventModelList;
        }

        public static List<ActivityEventModel> getActivityEventToday(String json) throws JSONException {
            List<ActivityEventModel> aEventModel = new ArrayList<>();

            JSONObject jsonObject = new JSONObject(json);
            JSONArray array = new JSONArray(jsonObject.getJSONObject("datas").getString("today"));
            for (int i = 0; i < array.length(); i++) {
                ActivityEventModel model = new ActivityEventModel();
                model.setId(array.getJSONObject(i).getString("id"));
                model.setName(array.getJSONObject(i).getString("name"));
                model.setAccountId(array.getJSONObject(i).getString("account_id"));
                model.setAssignTo(array.getJSONObject(i).getString("assign_to"));
                model.setDate(array.getJSONObject(i).getString("date"));
                model.setDescription(array.getJSONObject(i).getString("description"));
                model.setStatus(array.getJSONObject(i).getString("status"));
                model.setNama1(array.getJSONObject(i).getString("nama_photo_satu"));
                model.setNama2(array.getJSONObject(i).getString("nama_photo_dua"));
                model.setNama3(array.getJSONObject(i).getString("nama_photo_tiga"));
                model.setPhoto_1(array.getJSONObject(i).getString("photo_satu"));
                model.setPhoto_2(array.getJSONObject(i).getString("photo_dua"));
                model.setPhoto_3(array.getJSONObject(i).getString("photo_tiga"));
                model.setCreatedAt(array.getJSONObject(i).getString("created_at"));
                model.setCreatedBy(array.getJSONObject(i).getString("created_by"));
                model.setUpdatedAt(array.getJSONObject(i).getString("updated_at"));
                model.setUpdatedBy(array.getJSONObject(i).getString("updated_by"));
                model.setDeletedAt(array.getJSONObject(i).getString("deleted_at"));
                model.setDeletedBy(array.getJSONObject(i).getString("deleted_by"));
                model.setUser_name(array.getJSONObject(i).getString("user_name"));
                model.setFollowUp(array.getJSONObject(i).getString("follow_up_action"));
                model.setMeet_to(array.getJSONObject(i).getString("meet_to"));
                model.setProduct(array.getJSONObject(i).getString("product"));
                model.setAccount_name(array.getJSONObject(i).getString("account_name"));
                aEventModel.add(model);
            }
            return aEventModel;
        }

        public static List<ActivityTodoModel> getActivityTodoList(String json) throws JSONException {
            List<ActivityTodoModel> activityTodoModelList = new ArrayList<>();

            JSONObject jsonObject = new JSONObject(json);
            JSONArray array = new JSONArray(jsonObject.getJSONObject("datas").getString("list"));
            for (int i = 0; i < array.length(); i++) {
                ActivityTodoModel model = new ActivityTodoModel();
                model.setId(array.getJSONObject(i).getString("id"));
                model.setAccountId(array.getJSONObject(i).getString("account_id"));
                model.setAssignTo(array.getJSONObject(i).getString("assign_to"));
                model.setSubject(array.getJSONObject(i).getString("subject"));
                model.setDueDate(array.getJSONObject(i).getString("due_date"));
                model.setDescription(array.getJSONObject(i).getString("description"));
                model.setPriority(array.getJSONObject(i).getString("priority"));
                model.setStatus(array.getJSONObject(i).getString("status"));
                model.setTaskStatus(array.getJSONObject(i).getString("task_status"));
                model.setCreatedAt(array.getJSONObject(i).getString("created_at"));
                model.setCreatedBy(array.getJSONObject(i).getString("created_by"));
                model.setUpdatedAt(array.getJSONObject(i).getString("updated_at"));
                model.setUpdatedBy(array.getJSONObject(i).getString("updated_by"));
                model.setDeletedAt(array.getJSONObject(i).getString("deleted_at"));
                model.setDeletedBy(array.getJSONObject(i).getString("deleted_by"));
                model.setUser_name(array.getJSONObject(i).getString("user_name"));
                model.setAccount_name(array.getJSONObject(i).getString("account_name"));
                model.setPriority_name(array.getJSONObject(i).getString("priority_name"));
                model.setStatus_name(array.getJSONObject(i).getString("status_name"));
                model.setTask_status_name(array.getJSONObject(i).getString("task_status_name"));
                activityTodoModelList.add(model);
            }
            return activityTodoModelList;
        }

        public static List<ActivityTodoModel> getActivityTodoToday(String json) throws JSONException {
            List<ActivityTodoModel> aTodoModel = new ArrayList<>();

            JSONObject jsonObject = new JSONObject(json);
            JSONArray array = new JSONArray(jsonObject.getJSONObject("datas").getString("today"));
            for (int i = 0; i < array.length(); i++) {
                ActivityTodoModel model = new ActivityTodoModel();
                model.setId(array.getJSONObject(i).getString("id"));
                model.setAccountId(array.getJSONObject(i).getString("account_id"));
                model.setAssignTo(array.getJSONObject(i).getString("assign_to"));
                model.setSubject(array.getJSONObject(i).getString("subject"));
                model.setDueDate(array.getJSONObject(i).getString("due_date"));
                model.setDescription(array.getJSONObject(i).getString("description"));
                model.setPriority(array.getJSONObject(i).getString("priority"));
                model.setStatus(array.getJSONObject(i).getString("status"));
                model.setTaskStatus(array.getJSONObject(i).getString("task_status"));
                model.setCreatedAt(array.getJSONObject(i).getString("created_at"));
                model.setCreatedBy(array.getJSONObject(i).getString("created_by"));
                model.setUpdatedAt(array.getJSONObject(i).getString("updated_at"));
                model.setUpdatedBy(array.getJSONObject(i).getString("updated_by"));
                model.setDeletedAt(array.getJSONObject(i).getString("deleted_at"));
                model.setDeletedBy(array.getJSONObject(i).getString("deleted_by"));
                model.setUser_name(array.getJSONObject(i).getString("user_name"));
                model.setAccount_name(array.getJSONObject(i).getString("account_name"));
                model.setPriority_name(array.getJSONObject(i).getString("priority_name"));
                model.setTask_status_name(array.getJSONObject(i).getString("task_status_name"));
                aTodoModel.add(model);
            }
            return aTodoModel;
        }

        public static List<ActivityCallsModel> getActivityCallsList(String json) throws JSONException {
            List<ActivityCallsModel> aCallsModel = new ArrayList<>();

            JSONObject jsonObject = new JSONObject(json);
            JSONArray array = new JSONArray(jsonObject.getJSONObject("datas").getString("list"));
            for (int i = 0; i < array.length(); i++) {
                ActivityCallsModel model = new ActivityCallsModel();
                model.setId(array.getJSONObject(i).getString("id"));
                model.setAccountId(array.getJSONObject(i).getString("account_id"));
                model.setUserId(array.getJSONObject(i).getString("user_id"));
                model.setCallTypeId(array.getJSONObject(i).getString("call_type_id"));
                model.setCallPurposeId(array.getJSONObject(i).getString("call_purpose_id"));
                model.setDate(array.getJSONObject(i).getString("date"));
                model.setDuration(array.getJSONObject(i).getString("duration"));
                model.setDurationLabel(array.getJSONObject(i).getString("duration_label"));
                model.setSubject(array.getJSONObject(i).getString("subject"));
                model.setDetail(array.getJSONObject(i).getString("detail"));
                model.setStatus(array.getJSONObject(i).getString("status"));
                model.setCreatedAt(array.getJSONObject(i).getString("created_at"));
                model.setUpdatedAt(array.getJSONObject(i).getString("updated_at"));
                model.setDeletedAt(array.getJSONObject(i).getString("deleted_at"));
                model.setAccount_name(array.getJSONObject(i).getString("account_name"));
                model.setUser_name(array.getJSONObject(i).getString("user_name"));
                model.setCall_type_name(array.getJSONObject(i).getString("call_type_name"));
                model.setCall_purpose_name(array.getJSONObject(i).getString("call_purpose_name"));
                model.setDuration_interval(array.getJSONObject(i).getString("duration_interval"));
                model.setDate_interval(array.getJSONObject(i).getString("date_interval"));
                aCallsModel.add(model);
            }
            return aCallsModel;
        }

        public static List<ActivityCallsModel> getActivityCallsToday(String json) throws JSONException {
            List<ActivityCallsModel> activityCallsModelList = new ArrayList<>();

            JSONObject jsonObject = new JSONObject(json);
            JSONArray array = new JSONArray(jsonObject.getJSONObject("datas").getString("today"));
            for (int i = 0; i < array.length(); i++) {
                ActivityCallsModel model = new ActivityCallsModel();
                model.setId(array.getJSONObject(i).getString("id"));
                model.setAccountId(array.getJSONObject(i).getString("account_id"));
                model.setUserId(array.getJSONObject(i).getString("user_id"));
                model.setCallTypeId(array.getJSONObject(i).getString("call_type_id"));
                model.setCallPurposeId(array.getJSONObject(i).getString("call_purpose_id"));
                model.setDate(array.getJSONObject(i).getString("date"));
                model.setDuration(array.getJSONObject(i).getString("duration"));
                model.setDurationLabel(array.getJSONObject(i).getString("duration_label"));
                model.setSubject(array.getJSONObject(i).getString("subject"));
                model.setDetail(array.getJSONObject(i).getString("detail"));
                model.setStatus(array.getJSONObject(i).getString("status"));
                model.setCreatedAt(array.getJSONObject(i).getString("created_at"));
                model.setUpdatedAt(array.getJSONObject(i).getString("updated_at"));
                model.setDeletedAt(array.getJSONObject(i).getString("deleted_at"));
                model.setAccount_name(array.getJSONObject(i).getString("account_name"));
                model.setUser_name(array.getJSONObject(i).getString("user_name"));
                model.setCall_type_name(array.getJSONObject(i).getString("call_type_name"));
                model.setCall_purpose_name(array.getJSONObject(i).getString("call_purpose_name"));
                model.setDuration_interval(array.getJSONObject(i).getString("duration_interval"));
                model.setDate_interval(array.getJSONObject(i).getString("date_interval"));
                activityCallsModelList.add(model);
            }
            return activityCallsModelList;
        }

        public static List<TaskModel> getTaskList(String json) throws JSONException {
            List<TaskModel> list = new ArrayList<>();

            JSONObject jsonObject = new JSONObject(json);
            JSONArray array = new JSONArray(jsonObject.getJSONObject("datas").getString("tasks"));
            for (int i = 0; i < array.length(); i++) {
                TaskModel model = new TaskModel();
                model.setId(array.getJSONObject(i).getString("id"));
                model.setSubject(array.getJSONObject(i).getString("subject"));
                model.setAccount_id(array.getJSONObject(i).getString("account_id"));
                model.setAccount_name(array.getJSONObject(i).getString("account_name"));
                model.setAssignto(array.getJSONObject(i).getString("assign_to"));
                model.setAssignto_name(array.getJSONObject(i).getString("assign_to_name"));
                model.setDescription(array.getJSONObject(i).getString("description"));
                model.setDate(array.getJSONObject(i).getString("due_date"));
                model.setDate_interval(array.getJSONObject(i).getString("date_interval"));
                model.setPriority_id(array.getJSONObject(i).getString("priority"));
                model.setPriority_name(array.getJSONObject(i).getString("priority_name"));
                model.setTask_status_id(array.getJSONObject(i).getString("task_status"));
                model.setTask_status_name(array.getJSONObject(i).getString("task_status_name"));
                model.setCreated_at(array.getJSONObject(i).getString("created_at"));
                list.add(model);
            }

            return list;
        }

        public static List<TaskModel> getTaskListbyStatus(String json) throws JSONException {
            List<TaskModel> list = new ArrayList<>();

            JSONObject jsonObject = new JSONObject(json);
            JSONArray array = new JSONArray(jsonObject.getJSONObject("datas").getString("tasks"));
            for (int i = 0; i < array.length(); i++) {
                TaskModel model = new TaskModel();
                model.setId(array.getJSONObject(i).getString("id"));
                model.setSubject(array.getJSONObject(i).getString("subject"));
                model.setAccount_id(array.getJSONObject(i).getString("account_id"));
                model.setAccount_name(array.getJSONObject(i).getString("account_name"));
                model.setAssignto(array.getJSONObject(i).getString("assign_to"));
                model.setAssignto_name(array.getJSONObject(i).getString("assign_to_name"));
                model.setDescription(array.getJSONObject(i).getString("description"));
                model.setDate(array.getJSONObject(i).getString("due_date"));
                model.setDate_interval(array.getJSONObject(i).getString("date_interval"));
                model.setPriority_id(array.getJSONObject(i).getString("priority"));
                model.setPriority_name(array.getJSONObject(i).getString("priority_name"));
                model.setTask_status_id(array.getJSONObject(i).getString("task_status"));
                model.setTask_status_name(array.getJSONObject(i).getString("task_status_name"));
                model.setCreated_at(array.getJSONObject(i).getString("created_at"));
                list.add(model);
            }

            return list;
        }

        public static List<CallsModel> getCallList(String json) throws JSONException {
            List<CallsModel> list = new ArrayList<>();

            JSONObject jsonObject = new JSONObject(json);
            JSONArray array = new JSONArray(jsonObject.getJSONObject("datas").getString("calls"));
            for (int i = 0; i < array.length(); i++) {
                CallsModel model = new CallsModel();
                model.setId(array.getJSONObject(i).getString("id"));
                model.setSubject(array.getJSONObject(i).getString("subject"));
                model.setAccount_id(array.getJSONObject(i).getString("account_id"));
                model.setAccount_name(array.getJSONObject(i).getString("account_name"));
                model.setCall_type_id(array.getJSONObject(i).getString("call_type_id"));
                model.setCall_type_name(array.getJSONObject(i).getString("call_type_name"));
                model.setCall_purpose_id(array.getJSONObject(i).getString("call_purpose_id"));
                model.setCall_purpose_name(array.getJSONObject(i).getString("call_purpose_name"));
                model.setDate(array.getJSONObject(i).getString("date"));
                model.setDate_interval(array.getJSONObject(i).getString("date_interval"));
                model.setDuration(array.getJSONObject(i).getString("duration"));
                model.setDuration_label(array.getJSONObject(i).getString("duration_label"));
                model.setDuration_interval(array.getJSONObject(i).getString("duration_interval"));
                model.setDetail(array.getJSONObject(i).getString("detail"));
                model.setUpdated_at(array.getJSONObject(i).getString("updated_at"));
                model.setCreated_at(array.getJSONObject(i).getString("created_at"));
                list.add(model);
            }

            return list;
        }

        public static List<OpportunityModel> getOpportunityList(String json) throws JSONException {
            List<OpportunityModel> list = new ArrayList<>();

            JSONObject jsonObject = new JSONObject(json);
            JSONArray array = new JSONArray(jsonObject.getJSONObject("datas").getString("opportunity"));
            for (int i = 0; i < array.length(); i++) {
                OpportunityModel model = new OpportunityModel();
                model.setId(array.getJSONObject(i).getString("id"));
                model.setName(array.getJSONObject(i).getString("opportunity_name"));
                model.setAccount_id(array.getJSONObject(i).getString("account_id"));
                model.setAccount_name(array.getJSONObject(i).getString("account_name"));
                model.setFirst_name(array.getJSONObject(i).getString("first_name"));
                model.setLast_name(array.getJSONObject(i).getString("last_name"));
                model.setTitle_id(array.getJSONObject(i).getString("title_id"));
                model.setTitle_name(array.getJSONObject(i).getString("title_name"));
                model.setPosition_id(array.getJSONObject(i).getString("position_id"));
                model.setPosition_name(array.getJSONObject(i).getString("position_name"));
                model.setEmail(array.getJSONObject(i).getString("email"));
                model.setPhone(array.getJSONObject(i).getString("phone"));
                model.setDescription(array.getJSONObject(i).getString("description"));
                model.setSize(array.getJSONObject(i).getString("opportunity_size"));
                model.setType(array.getJSONObject(i).getString("type"));
                model.setStage_id(array.getJSONObject(i).getString("stage_id"));
                model.setStage_name(array.getJSONObject(i).getString("stage_name"));
                model.setSource_id(array.getJSONObject(i).getString("lead_source_id"));
                model.setSource_name(array.getJSONObject(i).getString("lead_source_name"));
                model.setOppor_status_id(array.getJSONObject(i).getString("opportunity_status_id"));
                model.setOppor_status_name(array.getJSONObject(i).getString("opportunity_status_name"));
                model.setOwner_id(array.getJSONObject(i).getString("owner_id"));
                model.setOwner_name(array.getJSONObject(i).getString("owner_name"));
                model.setProgress(array.getJSONObject(i).getString("stage_percentage"));
                model.setNext_step(array.getJSONObject(i).getString("next_step"));
                model.setOpen_date(array.getJSONObject(i).getString("open_date"));
                model.setIndustry(array.getJSONObject(i).getString("industry_id"));
                model.setAddress_billing(array.getJSONObject(i).getString("billing"));
                model.setAddress_shipping(array.getJSONObject(i).getString("shipping"));
                model.setCategory_id(array.getJSONObject(i).getString("category_id"));
                model.setCategory_name(array.getJSONObject(i).getString("category_name"));
                model.setClose_date(array.getJSONObject(i).getString("close_date"));
                model.setClose_date_interval(array.getJSONObject(i).getString("close_date_interval"));
                model.setAssign_to(array.getJSONObject(i).getString("user_id"));
                model.setAssign_to_name(array.getJSONObject(i).getString("user_name"));
                model.setProduct(array.getJSONObject(i).getString("product"));
                model.setAccount_contact(array.getJSONObject(i).getString("account_contact"));
                model.setContact_id(array.getJSONObject(i).getString("contact_id"));
                model.setContact_name(array.getJSONObject(i).getString("contact_name"));
                model.setProbability(array.getJSONObject(i).getString("probability"));
                model.setComplete_ref_number(array.getJSONObject(i).getString("complete_ref_number"));
                model.setReason_not_deal(array.getJSONObject(i).getString("reason_not_deal"));
                model.setOppotunityBilling(new OpportunityBillingModel(
                        array.getJSONObject(i).getJSONObject("billing").getString("id"),
                        array.getJSONObject(i).getJSONObject("billing").getString("alamat"),
                        array.getJSONObject(i).getJSONObject("billing").getString("latitude"),
                        array.getJSONObject(i).getJSONObject("billing").getString("longitude")
                ));
                list.add(model);
            }

            return list;
        }

        public static List<OpportunityModel> getOpportunityListbyNone(String json) throws JSONException {
            List<OpportunityModel> list = new ArrayList<>();

            JSONObject jsonObject = new JSONObject(json);
            JSONArray array = new JSONArray(jsonObject.getJSONObject("datas").getString("opportunity"));
            for (int i = 0; i < array.length(); i++) {
                OpportunityModel model = new OpportunityModel();
                model.setId(array.getJSONObject(i).getString("id"));
                model.setName(array.getJSONObject(i).getString("opportunity_name"));
                model.setAccount_id(array.getJSONObject(i).getString("account_id"));
                model.setAccount_name(array.getJSONObject(i).getString("account_name"));
                model.setFirst_name(array.getJSONObject(i).getString("first_name"));
                model.setLast_name(array.getJSONObject(i).getString("last_name"));
                model.setTitle_id(array.getJSONObject(i).getString("title_id"));
                model.setTitle_name(array.getJSONObject(i).getString("title_name"));
                model.setPosition_id(array.getJSONObject(i).getString("position_id"));
                model.setPosition_name(array.getJSONObject(i).getString("position_name"));
                model.setEmail(array.getJSONObject(i).getString("email"));
                model.setPhone(array.getJSONObject(i).getString("phone"));
                model.setDescription(array.getJSONObject(i).getString("description"));
                model.setSize(array.getJSONObject(i).getString("opportunity_size"));
                model.setType(array.getJSONObject(i).getString("type"));
                model.setStage_id(array.getJSONObject(i).getString("stage_id"));
                model.setStage_name(array.getJSONObject(i).getString("stage_name"));
                model.setSource_id(array.getJSONObject(i).getString("lead_source_id"));
                model.setSource_name(array.getJSONObject(i).getString("lead_source_name"));
                model.setOppor_status_id(array.getJSONObject(i).getString("opportunity_status_id"));
                model.setOppor_status_name(array.getJSONObject(i).getString("opportunity_status_name"));
                model.setOwner_id(array.getJSONObject(i).getString("owner_id"));
                model.setOwner_name(array.getJSONObject(i).getString("owner_name"));
                model.setProgress(array.getJSONObject(i).getString("stage_percentage"));
                model.setNext_step(array.getJSONObject(i).getString("next_step"));
                model.setOpen_date(array.getJSONObject(i).getString("open_date"));
                model.setIndustry(array.getJSONObject(i).getString("industry_id"));
                model.setAddress_billing(array.getJSONObject(i).getString("billing"));
                model.setAddress_shipping(array.getJSONObject(i).getString("shipping"));
                model.setCategory_id(array.getJSONObject(i).getString("category_id"));
                model.setCategory_name(array.getJSONObject(i).getString("category_name"));
                model.setClose_date(array.getJSONObject(i).getString("close_date"));
                model.setClose_date_interval(array.getJSONObject(i).getString("close_date_interval"));
                model.setAssign_to(array.getJSONObject(i).getString("user_id"));
                model.setAssign_to_name(array.getJSONObject(i).getString("user_name"));
                model.setProduct(array.getJSONObject(i).getString("product"));
                model.setAccount_contact(array.getJSONObject(i).getString("account_contact"));
                model.setContact_id(array.getJSONObject(i).getString("contact_id"));
                model.setContact_name(array.getJSONObject(i).getString("contact_name"));
                model.setOppotunityBilling(new OpportunityBillingModel(
                        array.getJSONObject(i).getJSONObject("billing").getString("id"),
                        array.getJSONObject(i).getJSONObject("billing").getString("alamat"),
                        array.getJSONObject(i).getJSONObject("billing").getString("latitude"),
                        array.getJSONObject(i).getJSONObject("billing").getString("longitude")
                ));
                list.add(model);
            }

            return list;
        }

        public static List<VpModel> getViewPipeline(String json) throws JSONException {
            List<VpModel> list = new ArrayList<>();

            JSONObject jsonObject = new JSONObject(json);
            try{
                JSONArray mJsonArray = jsonObject.getJSONArray("datas");
                for (int i = 0; i < mJsonArray.length(); i++){
                    VpModel vpModel = new VpModel();
                    vpModel.setStageId(mJsonArray.getJSONObject(i).getString("stage_id"));
                    vpModel.setStageName(mJsonArray.getJSONObject(i).getString("stage_name"));
                    vpModel.setPercentage(mJsonArray.getJSONObject(i).getString("percentage"));
                    vpModel.setTotalOpportunity(mJsonArray.getJSONObject(i).getString("total_opportunity"));
                    vpModel.setSizeOpportunity(mJsonArray.getJSONObject(i).getString("size_opportunity"));
                    vpModel.setOpportunities(mJsonArray.getJSONObject(i).getString("opportunities"));
                    vpModel.setTotal_opportunity_size(mJsonArray.getJSONObject(i).getString("total_opportunity_size"));
                    list.add(vpModel);
                }
            } catch (JSONException e){
                e.printStackTrace();
            }
            return list;
        }

        public static List<DatabaseMachineModel> getDBmachine (String json) throws JSONException {
            List<DatabaseMachineModel> list = new ArrayList<>();
            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("datas").getString("machines"));
            for(int i = 0; i < jsonArray.length(); i++){
                DatabaseMachineModel model = new DatabaseMachineModel();
                model.setId(jsonArray.getJSONObject(i).getString("id"));
                model.setAccountId(jsonArray.getJSONObject(i).getString("account_id"));
                model.setBrandId(jsonArray.getJSONObject(i).getString("brand_id"));
                model.setApplication(jsonArray.getJSONObject(i).getString("application"));
                model.setUnitNoInCustomerSite(jsonArray.getJSONObject(i).getString("unit_no_in_customer_site"));
                model.setType(jsonArray.getJSONObject(i).getString("type"));
                model.setModel(jsonArray.getJSONObject(i).getString("model"));
                model.setSerialNumber(jsonArray.getJSONObject(i).getString("serial_number"));
                model.setYearOfInstall(jsonArray.getJSONObject(i).getString("year_of_install"));
                model.setPressureVacuum(jsonArray.getJSONObject(i).getString("pressure_vacuum"));
                model.setCapacity(jsonArray.getJSONObject(i).getString("capacity"));
                model.setDiffPressure(jsonArray.getJSONObject(i).getString("diff_pressure"));
                model.setSpeed(jsonArray.getJSONObject(i).getString("speed"));
                model.setMotorSpecs(jsonArray.getJSONObject(i).getString("motor_specs"));
                model.setDateLastOverhaul(jsonArray.getJSONObject(i).getString("date_last_overhaul"));
                model.setTypeOfServiceDone(jsonArray.getJSONObject(i).getString("type_of_service_done"));
                model.setLatestInspectionDate(jsonArray.getJSONObject(i).getString("latest_inspection_date"));
                model.setRunningHours(jsonArray.getJSONObject(i).getString("running_hours"));
                model.setCondition(jsonArray.getJSONObject(i).getString("condition"));
                model.setStatus(jsonArray.getJSONObject(i).getString("status"));
                model.setCreatedAt(jsonArray.getJSONObject(i).getString("created_at"));
                model.setUpdatedAt(jsonArray.getJSONObject(i).getString("updated_at"));
                model.setDeletedAt(jsonArray.getJSONObject(i).getString("deleted_at"));
                model.setAccountName(jsonArray.getJSONObject(i).getString("account_name"));
                model.setBrandName(jsonArray.getJSONObject(i).getString("brand_name"));
                list.add(model);
            }
            return list;
        }

        public static List<VpOppor> getVpDataOppor(String json) throws  JSONException{
            List<VpOppor> list = new ArrayList<>();
            JSONObject jsonObject = new JSONObject(json);
            JSONArray array = new JSONArray(jsonObject.getJSONObject("datas").getString("opportunities"));
            for (int i = 0; i< array.length(); i++){
                VpOppor model = new VpOppor();
                model.setOpportunityName(array.getJSONObject(i).getString("opportunity_name"));
                model.setAccountName(array.getJSONObject(i).getString("account_name"));
                model.setStagePercentage(array.getJSONObject(i).getString("stage_percentage"));
                model.setOpportunitySize(array.getJSONObject(i).getString("opportunity_size"));
                model.setCloseDate(array.getJSONObject(i).getString("close_date"));
                model.setStageName(array.getJSONObject(i).getString("stage_name"));
                model.setStageId(array.getJSONObject(i).getString("stage_id"));
                list.add(model);
            }
            return list;
        }

        public static List<OpportunityModel> getOpportunityListbyMQL(String json) throws JSONException {
            List<OpportunityModel> list = new ArrayList<>();

            JSONObject jsonObject = new JSONObject(json);
            JSONArray array = new JSONArray(jsonObject.getJSONObject("datas").getString("opportunity"));
            for (int i = 0; i < array.length(); i++) {
                OpportunityModel model = new OpportunityModel();
                model.setId(array.getJSONObject(i).getString("id"));
                model.setName(array.getJSONObject(i).getString("opportunity_name"));
                model.setAccount_id(array.getJSONObject(i).getString("account_id"));
                model.setAccount_name(array.getJSONObject(i).getString("account_name"));
                model.setFirst_name(array.getJSONObject(i).getString("first_name"));
                model.setLast_name(array.getJSONObject(i).getString("last_name"));
                model.setTitle_id(array.getJSONObject(i).getString("title_id"));
                model.setTitle_name(array.getJSONObject(i).getString("title_name"));
                model.setPosition_id(array.getJSONObject(i).getString("position_id"));
                model.setPosition_name(array.getJSONObject(i).getString("position_name"));
                model.setEmail(array.getJSONObject(i).getString("email"));
                model.setPhone(array.getJSONObject(i).getString("phone"));
                model.setDescription(array.getJSONObject(i).getString("description"));
                model.setSize(array.getJSONObject(i).getString("opportunity_size"));
                model.setType(array.getJSONObject(i).getString("type"));
                model.setStage_id(array.getJSONObject(i).getString("stage_id"));
                model.setStage_name(array.getJSONObject(i).getString("stage_name"));
                model.setSource_id(array.getJSONObject(i).getString("lead_source_id"));
                model.setSource_name(array.getJSONObject(i).getString("lead_source_name"));
                model.setOppor_status_id(array.getJSONObject(i).getString("opportunity_status_id"));
                model.setOppor_status_name(array.getJSONObject(i).getString("opportunity_status_name"));
                model.setOwner_id(array.getJSONObject(i).getString("owner_id"));
                model.setOwner_name(array.getJSONObject(i).getString("owner_name"));
                model.setProgress(array.getJSONObject(i).getString("stage_percentage"));
                model.setNext_step(array.getJSONObject(i).getString("next_step"));
                model.setOpen_date(array.getJSONObject(i).getString("open_date"));
                model.setIndustry(array.getJSONObject(i).getString("industry_id"));
                model.setAddress_billing(array.getJSONObject(i).getString("billing"));
                model.setAddress_shipping(array.getJSONObject(i).getString("shipping"));
                model.setCategory_id(array.getJSONObject(i).getString("category_id"));
                model.setCategory_name(array.getJSONObject(i).getString("category_name"));
                model.setClose_date(array.getJSONObject(i).getString("close_date"));
                model.setClose_date_interval(array.getJSONObject(i).getString("close_date_interval"));
                model.setAssign_to(array.getJSONObject(i).getString("user_id"));
                model.setAssign_to_name(array.getJSONObject(i).getString("user_name"));
                model.setProduct(array.getJSONObject(i).getString("product"));
                model.setAccount_contact(array.getJSONObject(i).getString("account_contact"));
                model.setContact_id(array.getJSONObject(i).getString("contact_id"));
                model.setContact_name(array.getJSONObject(i).getString("contact_name"));
                model.setOppotunityBilling(new OpportunityBillingModel(
                        array.getJSONObject(i).getJSONObject("billing").getString("id"),
                        array.getJSONObject(i).getJSONObject("billing").getString("alamat"),
                        array.getJSONObject(i).getJSONObject("billing").getString("latitude"),
                        array.getJSONObject(i).getJSONObject("billing").getString("longitude")
                ));
                list.add(model);
            }

            return list;
        }

        public static TotalNPriceOpporModel getTotOpportunityBystage(String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);
            TotalNPriceOpporModel model = new TotalNPriceOpporModel();
            model.setTotalOpportunity(jsonObject.getJSONObject("datas").getString("total_opportunity"));
            model.setTotalPrice(jsonObject.getJSONObject("datas").getString("total_price"));

            return model;

        }

        public static void getPosition(Context context, String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);
            JSONArray array = new JSONArray(jsonObject.getJSONObject("datas").getString("position"));

            for (int i = 0; i < array.length(); i++) {
                PositionModel model = new PositionModel();
                model.setId(array.getJSONObject(i).getString("id"));
                model.setName(array.getJSONObject(i).getString("name"));

                new PositionDao(context).insertTable(model);
            }
        }

        public static void getIndustry(Context context, String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);
            JSONArray array = new JSONArray(jsonObject.getJSONObject("datas").getString("industry"));

            for (int i = 0; i < array.length(); i++) {
                IndustryModel model = new IndustryModel();
                model.setId(array.getJSONObject(i).getString("id"));
                model.setName(array.getJSONObject(i).getString("name"));

                new IndustryDao(context).insertTable(model);
            }
        }

        public static void getEventStatus(Context context, String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);
            JSONArray array = new JSONArray(jsonObject.getJSONObject("datas").getString("event_type"));

            for (int i = 0; i < array.length(); i++) {
                EventStatusModel model = new EventStatusModel();
                model.setId(array.getJSONObject(i).getString("id"));
                model.setName(array.getJSONObject(i).getString("name"));

                new EventStatusDao(context).insertTable(model);
            }
        }

        public static void getEventPurpose(Context context, String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);
            JSONArray array = new JSONArray(jsonObject.getJSONObject("datas").getString("event_purpose"));

            for (int i = 0; i < array.length(); i++) {
                EventPurposeModel model = new EventPurposeModel();
                model.setId(array.getJSONObject(i).getString("id"));
                model.setName(array.getJSONObject(i).getString("name"));

                new EventPurposeDao(context).insertTable(model);
            }
        }


        public static void getAccountType(Context context, String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);
            JSONArray array = new JSONArray(jsonObject.getJSONObject("datas").getString("account_type"));

            for (int i = 0; i < array.length(); i++) {
                AccountTypeModel model = new AccountTypeModel();
                model.setId(array.getJSONObject(i).getString("id"));
                model.setName(array.getJSONObject(i).getString("type"));

                new AccountTypeDao(context).insertTable(model);
            }
        }

        public static void getOpportunityStatus(Context context, String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);
            JSONArray array = new JSONArray(jsonObject.getJSONObject("datas").getString("opportunity_status"));

            for (int i = 0; i < array.length(); i++) {
                OpportunityStatusModel model = new OpportunityStatusModel();
                model.setId(array.getJSONObject(i).getString("id"));
                model.setName(array.getJSONObject(i).getString("status"));

                new OpportunityStatusDao(context).insertTable(model);
            }
        }

        public static void getLeadSource(Context context, String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);
            JSONArray array = new JSONArray(jsonObject.getJSONObject("datas").getString("lead_source"));

            for (int i = 0; i < array.length(); i++) {
                LeadSourceModel model = new LeadSourceModel();
                model.setId(array.getJSONObject(i).getString("id"));
                model.setName(array.getJSONObject(i).getString("source"));

                new LeadSourceDao(context).insertTable(model);
            }
        }

        public static void getLeadStatus(Context context, String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);
            JSONArray array = new JSONArray(jsonObject.getJSONObject("datas").getString("lead_status"));

            for (int i = 0; i < array.length(); i++) {
                LeadStatusModel model = new LeadStatusModel();
                model.setId(array.getJSONObject(i).getString("id"));
                model.setName(array.getJSONObject(i).getString("status"));

                new LeadStatusDao(context).insertTable(model);
            }
        }

        public static void getNoVisit(Context context, String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);
            JSONArray array = new JSONArray(jsonObject.getJSONObject("datas").getString("no_visit_reason"));

            for (int i = 0; i < array.length(); i++) {
                NoVisitModel model = new NoVisitModel();
                model.setId(array.getJSONObject(i).getString("id"));
                model.setName(array.getJSONObject(i).getString("reason"));

                new NoVisitDao(context).insertTable(model);
            }
        }

        public static void getUseFor(Context context, String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);
            JSONArray array = new JSONArray(jsonObject.getJSONObject("datas").getString("use_for"));

            for (int i = 0; i < array.length(); i++) {
                UseForModel model = new UseForModel();
                model.setId(array.getJSONObject(i).getString("id"));
                model.setName(array.getJSONObject(i).getString("name"));

                new UseForDao(context).insertTable(model);
            }
        }

        public static void getNoOrder(Context context, String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);
            JSONArray array = new JSONArray(jsonObject.getJSONObject("datas").getString("no_order_reason"));

            for (int i = 0; i < array.length(); i++) {
                NoOrderModel model = new NoOrderModel();
                model.setId(array.getJSONObject(i).getString("id"));
                model.setName(array.getJSONObject(i).getString("reason"));

                new NoOrderDao(context).insertTable(model);
            }
        }

        public static void getOpportunityStage(Context context, String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);
            JSONArray array = new JSONArray(jsonObject.getJSONObject("datas").getString("opportunity_stage"));

            for (int i = 0; i < array.length(); i++) {
                OpportunityStageModel model = new OpportunityStageModel();
                model.setId(array.getJSONObject(i).getString("id"));
                model.setName(array.getJSONObject(i).getString("name"));
                model.setPercent(array.getJSONObject(i).getString("percentage"));

                new OpportunityStageDao(context).insertTable(model);
            }
        }

        public static List<OpportunityStageModel> getOpportunityStageList(String json) throws JSONException {
            List<OpportunityStageModel> list = new ArrayList<>();

            JSONObject jsonObject = new JSONObject(json);
            JSONArray array = new JSONArray(jsonObject.getJSONObject("datas").getString("opportunity_stage"));

            for (int i = 0; i < array.length(); i++) {
                OpportunityStageModel model = new OpportunityStageModel();
                model.setId(array.getJSONObject(i).getString("id"));
                model.setName(array.getJSONObject(i).getString("name"));
                model.setPercent(array.getJSONObject(i).getString("percentage"));
                list.add(model);
            }
            return list;
        }

        public static void getUserList(Context context, String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);
            JSONArray array = new JSONArray(jsonObject.getJSONObject("datas").getString("list"));

            for (int i = 0; i < array.length(); i++) {
                UserModel model = new UserModel();
                model.setId(array.getJSONObject(i).getString("id"));
                model.setFullname(array.getJSONObject(i).getString("first_name") + " " + array.getJSONObject(i).getString("last_name"));
                model.setImage_master(array.getJSONObject(i).getString("image_master"));

                new UserDao(context).insertTable(model);
            }
        }

        public static List<LocationModel> getLocation(Context context, String json) throws JSONException {
            List<LocationModel> list = new ArrayList<>();

            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("datas").getString("location"));

            for (int i = 0; i < jsonArray.length(); i++) {
                LocationModel model = new LocationModel();
                model.setId(jsonArray.getJSONObject(i).getString("id"));
                model.setName(jsonArray.getJSONObject(i).getString("name"));

                list.add(model);
            }
            return list;
        }

        public static List<LocationModel> getLocationPos(Context context, String json) throws JSONException {
            List<LocationModel> list = new ArrayList<>();

            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("datas").getString("location"));

            for (int i = 0; i < jsonArray.length(); i++) {
                LocationModel model = new LocationModel();
                model.setId(jsonArray.getJSONObject(i).getString("id"));
                model.setName(jsonArray.getJSONObject(i).getString("name"));
                model.setKodepos(jsonArray.getJSONObject(i).getString("kode_pos"));

                list.add(model);
            }
            return list;
        }

        public static TotalOpportunityModel getTotalOportunity(String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);
            TotalOpportunityModel model = new TotalOpportunityModel();
            model.setTotal(jsonObject.getJSONObject("datas").getString("total_opportunity"));

            return model;
        }

        public static PriceModel getPrice(String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);
            PriceModel model = new PriceModel();
            model.setPrice(jsonObject.getJSONObject("datas").getString("price"));
            return model;
        }

        public static OpportunityModel getDetail(String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);
            OpportunityModel model = new OpportunityModel();
            model.setId(jsonObject.getJSONObject("datas").getString("id"));
            model.setName(jsonObject.getJSONObject("datas").getString("opportunity_name"));
            model.setAccount_id(jsonObject.getJSONObject("datas").getString("account_id"));
            model.setAccount_name(jsonObject.getJSONObject("datas").getString("account_name"));
            model.setFirst_name(jsonObject.getJSONObject("datas").getString("first_name"));
            model.setLast_name(jsonObject.getJSONObject("datas").getString("last_name"));
            model.setPosition_name(jsonObject.getJSONObject("datas").getString("position_name"));
            model.setPosition_id(jsonObject.getJSONObject("datas").getString("position_id"));
            model.setTitle_id(jsonObject.getJSONObject("datas").getString("title_id"));
            model.setTitle_name(jsonObject.getJSONObject("datas").getString("title_name"));
            model.setPhone(jsonObject.getJSONObject("datas").getString("phone"));
            model.setEmail(jsonObject.getJSONObject("datas").getString("email"));
            model.setDescription(jsonObject.getJSONObject("datas").getString("description"));
            model.setSize(jsonObject.getJSONObject("datas").getString("opportunity_size"));
            model.setType(jsonObject.getJSONObject("datas").getString("type"));
            model.setStage_id(jsonObject.getJSONObject("datas").getString("stage_id"));
            model.setStage_name(jsonObject.getJSONObject("datas").getString("stage_name"));
            model.setSource_id(jsonObject.getJSONObject("datas").getString("lead_source_id"));
            model.setSource_name(jsonObject.getJSONObject("datas").getString("lead_source_name"));
            model.setOppor_status_id(jsonObject.getJSONObject("datas").getString("opportunity_status_id"));
            model.setOppor_status_name(jsonObject.getJSONObject("datas").getString("opportunity_status_name"));
            model.setOwner_id(jsonObject.getJSONObject("datas").getString("owner_id"));
            model.setOwner_name(jsonObject.getJSONObject("datas").getString("owner_name"));
            model.setProgress(jsonObject.getJSONObject("datas").getString("stage_percentage"));
            model.setNext_step(jsonObject.getJSONObject("datas").getString("next_step"));
            model.setOpen_date(jsonObject.getJSONObject("datas").getString("open_date"));
            model.setIndustry(jsonObject.getJSONObject("datas").getString("industry_id"));
            model.setAddress_billing(jsonObject.getJSONObject("datas").getString("billing"));
            model.setAddress_shipping(jsonObject.getJSONObject("datas").getString("shipping"));
            model.setCategory_id(jsonObject.getJSONObject("datas").getString("category_id"));
            model.setCategory_name(jsonObject.getJSONObject("datas").getString("category_name"));
            model.setClose_date(jsonObject.getJSONObject("datas").getString("close_date"));
            model.setClose_date_interval(jsonObject.getJSONObject("datas").getString("close_date_interval"));
            model.setAssign_to(jsonObject.getJSONObject("datas").getString("user_id"));
            model.setAssign_to_name(jsonObject.getJSONObject("datas").getString("user_name"));
            model.setProduct(jsonObject.getJSONObject("datas").getString("product"));
            model.setAccount_contact(jsonObject.getJSONObject("datas").getString("account_contact"));
            model.setContact_id(jsonObject.getJSONObject("datas").getString("contact_id"));
            model.setContact_name(jsonObject.getJSONObject("datas").getString("contact_name"));
            model.setComplete_ref_number(jsonObject.getJSONObject("datas").getString("complete_ref_number"));
            model.setReason_not_deal(jsonObject.getJSONObject("datas").getString("reason_not_deal"));
            model.setOppotunityBilling(new OpportunityBillingModel(
                    jsonObject.getJSONObject("datas").getJSONObject("billing").getString("id"),
                    jsonObject.getJSONObject("datas").getJSONObject("billing").getString("alamat"),
                    jsonObject.getJSONObject("datas").getJSONObject("billing").getString("latitude"),
                    jsonObject.getJSONObject("datas").getJSONObject("billing").getString("longitude")
            ));


            return model;
        }

        public static LeadConvertModel getConvert(String json) throws JsonIOException, JSONException {
            JSONObject jsonObject = new JSONObject(json);
            LeadConvertModel model = new LeadConvertModel();
            model.setAccount_id(jsonObject.getJSONObject("datas").getString("account_id"));
            model.setAccount_name(jsonObject.getJSONObject("datas").getString("account_name"));
            model.setContact(jsonObject.getJSONObject("datas").getString("contact"));
            model.setLead_id(jsonObject.getJSONObject("datas").getString("lead_id"));
            model.setConvert_status(jsonObject.getJSONObject("datas").getString("convert_status"));
            return  model;
        }

        public static AbsenceModel getAbsence (String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);
            AbsenceModel model = new AbsenceModel();
            model.setButtonCheckIn(jsonObject.getJSONObject("datas").getString("button_check_in"));
            model.setCheckIn(jsonObject.getJSONObject("datas").getString("check_in"));
            model.setCheckInTime(jsonObject.getJSONObject("datas").getString("check_in_time"));
            model.setButtonCheckOut(jsonObject.getJSONObject("datas").getString("button_check_out"));
            model.setCheckOut(jsonObject.getJSONObject("datas").getString("check_out"));
            model.setCheckOutTime(jsonObject.getJSONObject("datas").getString("check_out_time"));
            model.setMessage(jsonObject.getJSONObject("datas").getString("message"));
            model.setUserType(jsonObject.getJSONObject("datas").getString("user_type"));
            model.setWorkingTimeStart(jsonObject.getJSONObject("datas").getString("working_time_start"));
            model.setWorkingTimeEnd(jsonObject.getJSONObject("datas").getString("working_time_end"));
            model.setStatus(jsonObject.getJSONObject("datas").getString("status"));
            return model;
        }


        public static DashboardFirstModel getFirstDashboard(String json) throws JSONException {

            JSONObject jsonObject = new JSONObject(json);
            DashboardFirstModel model = new DashboardFirstModel();
            model.setCount_account(jsonObject.getJSONObject("datas").getString("account"));
            model.setCount_lead(jsonObject.getJSONObject("datas").getString("lead"));
            model.setCount_opportunity(jsonObject.getJSONObject("datas").getString("opportunity"));
            model.setCount_activity(jsonObject.getJSONObject("datas").getString("activity"));
            model.setAccount_list_permission(jsonObject.getJSONObject("datas").getString("account_list_permission"));
            model.setOpportunity_list_permission(jsonObject.getJSONObject("datas").getString("opportunity_list_permission"));
            model.setLead_list_permission(jsonObject.getJSONObject("datas").getString("lead_list_permission"));
            model.setActivity_list_permission(jsonObject.getJSONObject("datas").getString("activity_list_permission"));
            model.setAccount_create_permission(jsonObject.getJSONObject("datas").getString("account_create_permission"));
            model.setLead_create_permission(jsonObject.getJSONObject("datas").getString("lead_create_permission"));
            model.setOpportunity_list_permission(jsonObject.getJSONObject("datas").getString("opportunity_list_permission"));
            model.setCrm_call_create_permission(jsonObject.getJSONObject("datas").getString("crm_call_create_permission"));
            model.setCrm_event_create_permission(jsonObject.getJSONObject("datas").getString("crm_event_create_permission"));
            model.setCrm_task_create_permission(jsonObject.getJSONObject("datas").getString("crm_task_create_permission"));
            model.setDatabase_machine_create_permission(jsonObject.getJSONObject("datas").getString("database_machine_create_permission"));
            model.setAccount_edit_permission(jsonObject.getJSONObject("datas").getString("account_edit_permission"));
            model.setLead_edit_permission(jsonObject.getJSONObject("datas").getString("lead_edit_permission"));
            model.setOpportunity_edit_permission(jsonObject.getJSONObject("datas").getString("opportunity_edit_permission"));
            model.setDatabase_machine_edit_permission(jsonObject.getJSONObject("datas").getString("database_machine_edit_permission"));
            model.setCrm_call_edit_permission(jsonObject.getJSONObject("datas").getString("crm_event_edit_permission"));
            model.setCrm_task_edit_permission(jsonObject.getJSONObject("datas").getString("crm_task_edit_permission"));
            model.setCrm_event_edit_permission(jsonObject.getJSONObject("datas").getString("crm_event_edit_permission"));
            model.setOpportunity_create_permission(jsonObject.getJSONObject("datas").getString("opportunity_create_permission"));
            model.setLead_convert_to_account_permission(jsonObject.getJSONObject("datas").getString("lead_convert_to_account_permission"));
            model.setLead_convert_to_opportunity_permission(jsonObject.getJSONObject("datas").getString("lead_convert_to_opportunity_permission"));
            model.setCrm_call_list_permission(jsonObject.getJSONObject("datas").getString("crm_call_list_permission"));
            model.setCrm_event_list_permission(jsonObject.getJSONObject("datas").getString("crm_event_list_permission"));
            model.setCrm_task_list_permission(jsonObject.getJSONObject("datas").getString("crm_task_list_permission"));
            model.setDatabase_machine_list_permission(jsonObject.getJSONObject("datas").getString("database_machine_list_permission"));

            return model;
        }

        public static DashboardSecondModel getSecondDashboard(String json) throws JSONException {

            JSONObject jsonObject = new JSONObject(json);
            DashboardSecondModel model = new DashboardSecondModel();
            model.setCurrent_month(jsonObject.getJSONObject("datas").getString("current_month"));
            model.setCurrent_call(jsonObject.getJSONObject("datas").getString("totalCallForUserCurrentMonth"));
            model.setCurrent_event(jsonObject.getJSONObject("datas").getString("totalEventForUserCurrentMonth"));
            model.setCurrent_task(jsonObject.getJSONObject("datas").getString("totalTaskForUserCurrentMonth"));
            model.setPrev_month(jsonObject.getJSONObject("datas").getString("previous_month"));
            model.setPrev_call(jsonObject.getJSONObject("datas").getString("totalCallForUserPrevMonth"));
            model.setPrev_event(jsonObject.getJSONObject("datas").getString("totalEventForUserPrevMonth"));
            model.setPrev_task(jsonObject.getJSONObject("datas").getString("totalTaskForUserPrevMonth"));

            return model;
        }

        public static List<DashboardThirdModel> getDashboardLeads(String json) throws JSONException {
            List<DashboardThirdModel> list = new ArrayList<>();

            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("datas").getString("lead"));

            for (int i = 0; i < jsonArray.length(); i++) {
                DashboardThirdModel model = new DashboardThirdModel();
                model.setDate(jsonArray.getJSONObject(i).getString("create_date"));
                model.setTotal(jsonArray.getJSONObject(i).getString("total"));

                list.add(model);
            }
            return list;
        }

        public static List<DashboardThirdModel> getDashboardLeads1(String json) throws JSONException {
            List<DashboardThirdModel> list = new ArrayList<>();

            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("datas").getString("lead2"));

            for (int i = 0; i < jsonArray.length(); i++) {
                DashboardThirdModel model = new DashboardThirdModel();
                model.setDate(jsonArray.getJSONObject(i).getString("create_date"));
                model.setTotal(jsonArray.getJSONObject(i).getString("total"));

                list.add(model);
            }
            return list;
        }

        public static List<DashboardThirdModel> getDashboardOppor(String json) throws JSONException {
            List<DashboardThirdModel> list = new ArrayList<>();

            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("datas").getString("opportunity"));

            for (int i = 0; i < jsonArray.length(); i++) {
                DashboardThirdModel model = new DashboardThirdModel();
                model.setDate(jsonArray.getJSONObject(i).getString("create_date"));
                model.setTotal(jsonArray.getJSONObject(i).getString("total"));

                list.add(model);
            }
            return list;
        }

        public static List<DashboardThirdModel> getDashboardOppor1(String json) throws JSONException {
            List<DashboardThirdModel> list = new ArrayList<>();

            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("datas").getString("opportunity2"));

            for (int i = 0; i < jsonArray.length(); i++) {
                DashboardThirdModel model = new DashboardThirdModel();
                model.setDate(jsonArray.getJSONObject(i).getString("create_date"));
                model.setTotal(jsonArray.getJSONObject(i).getString("total"));

                list.add(model);
            }
            return list;
        }

        public static List<TotalModel> getTotalLeads1(String json) throws JSONException {
            List<TotalModel> list = new ArrayList<>();

            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("datas").getString("periode1_data"));

            for (int i = 0; i < jsonArray.length(); i++) {
                TotalModel model = new TotalModel();
                model.setDate(jsonArray.getJSONObject(i).getString("create_date"));
                model.setTotal(jsonArray.getJSONObject(i).getString("total_lead"));

                list.add(model);
            }
            return list;
        }

        public static List<TotalModel> getTotalLeads2(String json) throws JSONException {
            List<TotalModel> list = new ArrayList<>();

            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("datas").getString("periode2_data"));

            for (int i = 0; i < jsonArray.length(); i++) {
                TotalModel model = new TotalModel();
                model.setDate(jsonArray.getJSONObject(i).getString("create_date"));
                model.setTotal(jsonArray.getJSONObject(i).getString("total_lead"));

                list.add(model);
            }
            return list;
        }

        public static List<TotalModel> getTotalDashLeads1(String json) throws JSONException {
            List<TotalModel> list = new ArrayList<>();

            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("datas").getString("periode1_data"));

            for (int i = 0; i < jsonArray.length(); i++) {
                TotalModel model = new TotalModel();
                model.setDate(jsonArray.getJSONObject(i).getString("create_date"));
                model.setTotal(jsonArray.getJSONObject(i).getString("total"));

                list.add(model);
            }
            return list;
        }

        public static List<TotalModel> getTotalDashLeads2(String json) throws JSONException {
            List<TotalModel> list = new ArrayList<>();

            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("datas").getString("periode2_data"));

            for (int i = 0; i < jsonArray.length(); i++) {
                TotalModel model = new TotalModel();
                model.setDate(jsonArray.getJSONObject(i).getString("create_date"));
                model.setTotal(jsonArray.getJSONObject(i).getString("total"));

                list.add(model);
            }
            return list;
        }

        public static List<TotalModel> getLeadConversion1(String json) throws JSONException {
            List<TotalModel> list = new ArrayList<>();

            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("datas").getString("lead_data"));

            for (int i = 0; i < jsonArray.length(); i++) {
                TotalModel model = new TotalModel();
                model.setDate(jsonArray.getJSONObject(i).getString("create_date"));
                model.setTotal(jsonArray.getJSONObject(i).getString("total"));

                list.add(model);
            }
            return list;
        }

        public static List<TotalModel> getLeadConversion2(String json) throws JSONException {
            List<TotalModel> list = new ArrayList<>();

            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("datas").getString("converted_data"));

            for (int i = 0; i < jsonArray.length(); i++) {
                TotalModel model = new TotalModel();
                model.setDate(jsonArray.getJSONObject(i).getString("create_date"));
                model.setTotal(jsonArray.getJSONObject(i).getString("total"));

                list.add(model);
            }
            return list;
        }

        public static List<TotalModel> getTotalOpportunity1(String json) throws JSONException {
            List<TotalModel> list = new ArrayList<>();

            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("datas").getString("periode1_data"));

            for (int i = 0; i < jsonArray.length(); i++) {
                TotalModel model = new TotalModel();
                model.setDate(jsonArray.getJSONObject(i).getString("create_date"));
                model.setTotal(jsonArray.getJSONObject(i).getString("total"));

                list.add(model);
            }
            return list;
        }

        public static List<TotalModel> getTotalOpportunity2(String json) throws JSONException {
            List<TotalModel> list = new ArrayList<>();

            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("datas").getString("periode2_data"));

            for (int i = 0; i < jsonArray.length(); i++) {
                TotalModel model = new TotalModel();
                model.setDate(jsonArray.getJSONObject(i).getString("create_date"));
                model.setTotal(jsonArray.getJSONObject(i).getString("total"));

                list.add(model);
            }
            return list;
        }

        public static List<TotalModel> getOpporConversion1(String json) throws JSONException {
            List<TotalModel> list = new ArrayList<>();

            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("datas").getString("opportunity_data"));

            for (int i = 0; i < jsonArray.length(); i++) {
                TotalModel model = new TotalModel();
                model.setDate(jsonArray.getJSONObject(i).getString("create_date"));
                model.setTotal(jsonArray.getJSONObject(i).getString("total"));

                list.add(model);
            }
            return list;
        }

        public static List<TotalModel> getOpporConversion2(String json) throws JSONException {
            List<TotalModel> list = new ArrayList<>();

            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("datas").getString("won_data"));

            for (int i = 0; i < jsonArray.length(); i++) {
                TotalModel model = new TotalModel();
                model.setDate(jsonArray.getJSONObject(i).getString("create_date"));
                model.setTotal(jsonArray.getJSONObject(i).getString("total"));

                list.add(model);
            }
            return list;
        }

        public static List<PieChartModel> getOpporIndustry1(String json) throws JSONException {
            List<PieChartModel> list = new ArrayList<>();

            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("datas").getString("periode1"));

            for (int i = 0; i < jsonArray.length(); i++) {
                PieChartModel model = new PieChartModel();
                model.setName(jsonArray.getJSONObject(i).getString("name"));
                model.setTotal(jsonArray.getJSONObject(i).getString("opportunity"));

                list.add(model);
            }
            return list;
        }

        public static List<PieChartModel> getOpporIndustry2(String json) throws JSONException {
            List<PieChartModel> list = new ArrayList<>();

            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("datas").getString("periode2"));

            for (int i = 0; i < jsonArray.length(); i++) {
                PieChartModel model = new PieChartModel();
                model.setName(jsonArray.getJSONObject(i).getString("name"));
                model.setTotal(jsonArray.getJSONObject(i).getString("opportunity"));

                list.add(model);
            }
            return list;
        }

        public static List<TotalModel> getLeadStatus1(String json) throws JSONException {
            List<TotalModel> list = new ArrayList<>();

            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("datas").getString("periode1"));

            for (int i = 0; i < jsonArray.length(); i++) {
                TotalModel model = new TotalModel();
                model.setName(jsonArray.getJSONObject(i).getString("name"));
                model.setTotal(jsonArray.getJSONObject(i).getString("total"));

                list.add(model);
            }
            return list;
        }

        public static List<TotalModel> getLeadStatus2(String json) throws JSONException {
            List<TotalModel> list = new ArrayList<>();

            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("datas").getString("periode2"));

            for (int i = 0; i < jsonArray.length(); i++) {
                TotalModel model = new TotalModel();
                model.setName(jsonArray.getJSONObject(i).getString("name"));
                model.setTotal(jsonArray.getJSONObject(i).getString("total"));

                list.add(model);
            }
            return list;
        }

        public static List<TotalModel> getLeadDashboardStatus1(String json) throws JSONException {
            List<TotalModel> list = new ArrayList<>();

            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("datas").getString("lead_status_periode1"));

            for (int i = 0; i < jsonArray.length(); i++) {
                TotalModel model = new TotalModel();
                model.setName(jsonArray.getJSONObject(i).getString("status_name"));
                model.setTotal(jsonArray.getJSONObject(i).getString("total"));

                list.add(model);
            }
            return list;
        }

        public static List<TotalModel> getLeadDashboardStatus2(String json) throws JSONException {
            List<TotalModel> list = new ArrayList<>();

            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("datas").getString("lead_status_periode2"));

            for (int i = 0; i < jsonArray.length(); i++) {
                TotalModel model = new TotalModel();
                model.setName(jsonArray.getJSONObject(i).getString("status_name"));
                model.setTotal(jsonArray.getJSONObject(i).getString("total"));

                list.add(model);
            }
            return list;
        }

        public static List<TotalModel> getProductSold1(String json) throws JSONException {
            List<TotalModel> list = new ArrayList<>();

            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("datas").getString("periode1"));

            for (int i = 0; i < jsonArray.length(); i++) {
                TotalModel model = new TotalModel();
                model.setName(jsonArray.getJSONObject(i).getString("name"));
                model.setTotal(jsonArray.getJSONObject(i).getString("opportunity"));

                list.add(model);
            }
            return list;
        }

        public static List<TotalModel> getProductSold2(String json) throws JSONException {
            List<TotalModel> list = new ArrayList<>();

            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("datas").getString("periode2"));

            for (int i = 0; i < jsonArray.length(); i++) {
                TotalModel model = new TotalModel();
                model.setName(jsonArray.getJSONObject(i).getString("name"));
                model.setTotal(jsonArray.getJSONObject(i).getString("opportunity"));

                list.add(model);
            }
            return list;
        }

        public static List<TotalModel> getAccountTask1(String json) throws JSONException {
            List<TotalModel> list = new ArrayList<>();

            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("datas").getString("periode1"));

            for (int i = 0; i < jsonArray.length(); i++) {
                TotalModel model = new TotalModel();
                model.setDate(jsonArray.getJSONObject(i).getString("day"));
                model.setTotal(jsonArray.getJSONObject(i).getString("total"));

                list.add(model);
            }
            return list;
        }

        public static List<TotalModel> getAccountTask2(String json) throws JSONException {
            List<TotalModel> list = new ArrayList<>();

            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("datas").getString("periode2"));

            for (int i = 0; i < jsonArray.length(); i++) {
                TotalModel model = new TotalModel();
                model.setDate(jsonArray.getJSONObject(i).getString("day"));
                model.setTotal(jsonArray.getJSONObject(i).getString("total"));

                list.add(model);
            }
            return list;
        }

        public static List<TotalModel> getTotalCalls1(String json) throws JSONException {
            List<TotalModel> list = new ArrayList<>();

            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("datas").getString("periode1"));

            for (int i = 0; i < jsonArray.length(); i++) {
                TotalModel model = new TotalModel();
                model.setDate(jsonArray.getJSONObject(i).getString("create_date"));
                model.setTotal(jsonArray.getJSONObject(i).getString("total"));

                list.add(model);
            }
            return list;
        }

        public static List<TotalModel> getTotalCalls2(String json) throws JSONException {
            List<TotalModel> list = new ArrayList<>();

            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("datas").getString("periode2"));

            for (int i = 0; i < jsonArray.length(); i++) {
                TotalModel model = new TotalModel();
                model.setDate(jsonArray.getJSONObject(i).getString("create_date"));
                model.setTotal(jsonArray.getJSONObject(i).getString("total"));

                list.add(model);
            }
            return list;
        }

        public static SalesCycleModel getGeneralDashboard1(String json) throws JSONException {
            SalesCycleModel model = new SalesCycleModel();

            JSONObject jsonObject = new JSONObject(json);
            model.setTotal_conversion(jsonObject.getJSONObject("datas").getJSONObject("salesCyle").getString("leads_opportunities"));
            model.setTotal_conversionrate(jsonObject.getJSONObject("datas").getJSONObject("salesCyle").getString("leads_opportunities_conversion_rate"));
            model.setTotal_lead(jsonObject.getJSONObject("datas").getJSONObject("salesCyle").getString("leads"));
            model.setTotal_oppor(jsonObject.getJSONObject("datas").getJSONObject("salesCyle").getString("opportunities"));
            model.setTotal_closewon(jsonObject.getJSONObject("datas").getJSONObject("salesCyle").getString("won_opportunities"));
            model.setTotal_closelose(jsonObject.getJSONObject("datas").getJSONObject("salesCyle").getString("lose_opportunities"));
            model.setTotal_sales(jsonObject.getJSONObject("datas").getJSONObject("salesCyle").getString("sales"));
            model.setAvg_sales(jsonObject.getJSONObject("datas").getJSONObject("salesCyle").getString("avg_sales"));
            return model;
        }

        public static List<TotalModel> getGeneralDashboard2(String json) throws JSONException {
            List<TotalModel> list = new ArrayList<>();

            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("datas").getJSONObject("industry").getString("won"));

            for (int i = 0; i < jsonArray.length(); i++) {
                TotalModel model = new TotalModel();
                model.setName(jsonArray.getJSONObject(i).getString("industry"));
                model.setTotal(jsonArray.getJSONObject(i).getString("total_opportunity"));
                model.setAvg(jsonArray.getJSONObject(i).getString("average_duration"));

                list.add(model);
            }
            return list;
        }

        public static List<TotalModel> getGeneralDashboard3(String json) throws JSONException {
            List<TotalModel> list = new ArrayList<>();

            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("datas").getJSONObject("industry").getString("lose"));

            for (int i = 0; i < jsonArray.length(); i++) {
                TotalModel model = new TotalModel();
                model.setName(jsonArray.getJSONObject(i).getString("industry"));
                model.setTotal(jsonArray.getJSONObject(i).getString("total_opportunity"));
                model.setAvg(jsonArray.getJSONObject(i).getString("average_duration"));

                list.add(model);
            }
            return list;
        }

        public static List<TotalModel> getGeneralDashboard4(String json) throws JSONException {
            List<TotalModel> list = new ArrayList<>();

            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("datas").getJSONObject("leadSource").getString("won"));

            for (int i = 0; i < jsonArray.length(); i++) {
                TotalModel model = new TotalModel();
                model.setName(jsonArray.getJSONObject(i).getString("lead_source"));
                model.setTotal(jsonArray.getJSONObject(i).getString("total_opportunity"));
                model.setAvg(jsonArray.getJSONObject(i).getString("average_duration"));

                list.add(model);
            }
            return list;
        }

        public static List<TotalModel> getGeneralDashboard5(String json) throws JSONException {
            List<TotalModel> list = new ArrayList<>();

            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("datas").getJSONObject("leadSource").getString("lose"));

            for (int i = 0; i < jsonArray.length(); i++) {
                TotalModel model = new TotalModel();
                model.setName(jsonArray.getJSONObject(i).getString("lead_source"));
                model.setTotal(jsonArray.getJSONObject(i).getString("total_opportunity"));
                model.setAvg(jsonArray.getJSONObject(i).getString("average_duration"));

                list.add(model);
            }
            return list;
        }

        public static List<Top10CustomerModel> getGeneralDashboard6(String json) throws JSONException {
            List<Top10CustomerModel> list = new ArrayList<>();

            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("datas").getString("top10Customers"));

            for (int i = 0; i < jsonArray.length(); i++) {
                Top10CustomerModel model = new Top10CustomerModel();
                model.setAccount_id(jsonArray.getJSONObject(i).getString("account_id"));
                model.setAccount_name(jsonArray.getJSONObject(i).getString("account_name"));
                model.setAccount_revenue(jsonArray.getJSONObject(i).getString("account_revenue"));

                list.add(model);
            }
            return list;
        }

        public static SalesCycleModel getSalesCycle(String json) throws JSONException {
            SalesCycleModel model = new SalesCycleModel();

            JSONObject jsonObject = new JSONObject(json);
            model.setTotal_lead(jsonObject.getJSONObject("datas").getJSONObject("datas").getString("leads"));
            model.setTotal_oppor(jsonObject.getJSONObject("datas").getJSONObject("datas").getString("opportunities"));
            model.setTotal_closewon(jsonObject.getJSONObject("datas").getJSONObject("datas").getString("won_opportunities"));
            model.setTotal_closelose(jsonObject.getJSONObject("datas").getJSONObject("datas").getString("lose_opportunities"));
            model.setTotal_sales(jsonObject.getJSONObject("datas").getJSONObject("datas").getString("sales"));
            model.setAvg_sales(jsonObject.getJSONObject("datas").getJSONObject("datas").getString("avg_sales"));
            return model;
        }

        public static List<TotalModel> getOpporDuration1(String json) throws JSONException {
            List<TotalModel> list = new ArrayList<>();

            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("datas").getString("industry_won"));

            for (int i = 0; i < jsonArray.length(); i++) {
                TotalModel model = new TotalModel();
                model.setName(jsonArray.getJSONObject(i).getString("industry"));
                model.setTotal(jsonArray.getJSONObject(i).getString("total_opportunity"));
                model.setAvg(jsonArray.getJSONObject(i).getString("average_duration"));

                list.add(model);
            }
            return list;
        }

        public static List<TotalModel> getOpporDuration2(String json) throws JSONException {
            List<TotalModel> list = new ArrayList<>();

            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("datas").getString("industry_lost"));

            for (int i = 0; i < jsonArray.length(); i++) {
                TotalModel model = new TotalModel();
                model.setName(jsonArray.getJSONObject(i).getString("industry"));
                model.setTotal(jsonArray.getJSONObject(i).getString("total_opportunity"));
                model.setAvg(jsonArray.getJSONObject(i).getString("average_duration"));

                list.add(model);
            }
            return list;
        }

        public static List<TotalModel> getOpporDuration3(String json) throws JSONException {
            List<TotalModel> list = new ArrayList<>();

            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("datas").getString("lead_source_won"));

            for (int i = 0; i < jsonArray.length(); i++) {
                TotalModel model = new TotalModel();
                model.setName(jsonArray.getJSONObject(i).getString("lead_source"));
                model.setTotal(jsonArray.getJSONObject(i).getString("total_opportunity"));
                model.setAvg(jsonArray.getJSONObject(i).getString("average_duration"));

                list.add(model);
            }
            return list;
        }

        public static List<TotalModel> getOpporDuration4(String json) throws JSONException {
            List<TotalModel> list = new ArrayList<>();

            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("datas").getString("lead_source_lose"));

            for (int i = 0; i < jsonArray.length(); i++) {
                TotalModel model = new TotalModel();
                model.setName(jsonArray.getJSONObject(i).getString("lead_source"));
                model.setTotal(jsonArray.getJSONObject(i).getString("total_opportunity"));
                model.setAvg(jsonArray.getJSONObject(i).getString("average_duration"));

                list.add(model);
            }
            return list;
        }

        public static void getProfileUser(Context context, String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);
            PreferenceUtility.getInstance().saveData(context, PreferenceUtility.API_KEY,
                    jsonObject.getJSONObject("datas").getJSONObject("user").getString("api_key"));
            PreferenceUtility.getInstance().saveData(context, PreferenceUtility.KEY_SUBDOMAIN,
                    jsonObject.getJSONObject("datas").getJSONObject("user").getString("subdomain"));
            PreferenceUtility.getInstance().saveData(context, PreferenceUtility.KEY_USERID,
                    jsonObject.getJSONObject("datas").getJSONObject("user").getString("user_id"));
            PreferenceUtility.getInstance().saveData(context, PreferenceUtility.KEY_FULLNAME,
                    jsonObject.getJSONObject("datas").getJSONObject("user").getString("name"));
            PreferenceUtility.getInstance().saveData(context, PreferenceUtility.KEY_POSITION,
                    jsonObject.getJSONObject("datas").getJSONObject("user").getString("position"));
            PreferenceUtility.getInstance().saveData(context, PreferenceUtility.KEY_COMPANY,
                    jsonObject.getJSONObject("datas").getJSONObject("user").getString("company"));
            PreferenceUtility.getInstance().saveData(context, PreferenceUtility.KEY_IMAGES_MASTER,
                    jsonObject.getJSONObject("datas").getJSONObject("user").getString("image_master"));

        }

//        public static List<FunctionModel> getFunctionUser(Context context, String json) throws JSONException {
//            List<FunctionModel> list = new ArrayList<>();
//
//            JSONObject jsonObject = new JSONObject(json);
//            JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("datas").getString("function"));
//
//            for (int i = 0; i < jsonArray.length(); i++){
//                FunctionModel model = new FunctionModel();
//                model.setFunctionId(jsonArray.getJSONObject(i).getString("function_id"));
//                model.setFunctionCodeName(jsonArray.getJSONObject(i).getString("function_code_name"));
//                new FunctionDao(context).insertTable(model);
//                list.add(model);
//            }
//
//            return list;
//        }

        public static void getFunctionUser(Context context, String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("datas").getString("function"));

            for (int i = 0; i < jsonArray.length(); i++) {
                FunctionModel model = new FunctionModel();
                model.setFunctionId(jsonArray.getJSONObject(i).getString("function_id"));
                model.setFunctionCodeName(jsonArray.getJSONObject(i).getString("function_code_name"));

                new FunctionDao(context).insertTable(model);
            }
        }


//        public static void getMeetToList(Context context, String json) throws JSONException {
//            JSONObject jsonObject = new JSONObject(json);
//            JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("datas").getString("related_people"));
//
//            for(int i= 0; i < jsonArray.length(); i++){
//                MeetToModel model = new MeetToModel();
//                model.setMeet_to_id(jsonArray.getJSONObject(i).getString("meet_to_id"));
//                model.setName(jsonArray.getJSONObject(i).getString("name"));
//                model.setStatus("1");
//                model.setNotation("1");
//                new MeetToDao(context).insertTable(model);
//            }
//        }

        public static List<ContactModel> getMeetToList(String json) throws JSONException {
            List<ContactModel> listt = new ArrayList<>();
            JSONObject jsonObject = new JSONObject(json);
            JSONArray jsonArray = new JSONArray(jsonObject.getJSONObject("datas").getString("related_people"));

            for(int i= 0; i < jsonArray.length(); i++){
                ContactModel model = new ContactModel();
                model.setId(jsonArray.getJSONObject(i).getString("meet_to_id"));
                model.setFullname(jsonArray.getJSONObject(i).getString("name"));
                listt.add(model);
            }
            return listt;
        }

        public static void getClearDataMaster(Context context) {
            new PositionDao(context).deleteAllRecord();
            new IndustryDao(context).deleteAllRecord();
            new AccountTypeDao(context).deleteAllRecord();
            new OpportunityStatusDao(context).deleteAllRecord();
            new LeadSourceDao(context).deleteAllRecord();
            new LeadStatusDao(context).deleteAllRecord();
            new NoVisitDao(context).deleteAllRecord();
            new UseForDao(context).deleteAllRecord();
            new NoOrderDao(context).deleteAllRecord();
            new OpportunityStageDao(context).deleteAllRecord();
            new UserDao(context).deleteAllRecord();
        }

        public static void getClearDataFunction(Context context) {
            new FunctionDao(context).deleteAllRecord();
        }


    }
}
