package com.msf.project.sales1crm;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;

import com.msf.project.sales1crm.adapter.ProductCartAdapter;
import com.msf.project.sales1crm.callback.CurrencyDialogCallback;
import com.msf.project.sales1crm.callback.OpportunityAddCallback;
import com.msf.project.sales1crm.callback.ProductDialogCallback;
import com.msf.project.sales1crm.customview.CustomDialog;
import com.msf.project.sales1crm.customview.CustomEditText;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.customview.SpinnerCustomAdapter;
import com.msf.project.sales1crm.database.LeadSourceDao;
import com.msf.project.sales1crm.fragment.CurrencyDialogFragment;
import com.msf.project.sales1crm.fragment.ProductDialogFragment;
import com.msf.project.sales1crm.model.OpportunityModel;
import com.msf.project.sales1crm.model.ProductModel;
import com.msf.project.sales1crm.presenter.OpportunityAddPresenter;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.PreferenceUtility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OpportunityAddExtActivity extends BaseActivity implements OpportunityAddCallback {

    @BindView(R.id.ivBack)
    ImageView ivBack;

    @BindView(R.id.ivSave)
    ImageView ivSave;

    @BindView(R.id.tvTitle)
    CustomTextView tvTitle;

    @BindView(R.id.etDesc)
    CustomEditText etDesc;

    @BindView(R.id.etNextStep)
    CustomEditText etNextStep;

    @BindView(R.id.spLeadSource)
    Spinner spLeadSource;

    @BindView(R.id.spPIC)
    Spinner spPIC;

    @BindView(R.id.lvProduct)
    ListView lvProduct;

    @BindView(R.id.tvSearchProduct)
    CustomTextView tvSearchProduct;
    @BindView(R.id.tvTitleBilling)
    CustomTextView tvTitleBilling;
    @BindView(R.id.tvCurency)
    CustomTextView tvCurency;
    @BindView(R.id.ivCurrency)
    ImageView ivCurrency;

    private Context context;
    private String url = "", contact_json = "", lead_source = "0", oppor_pic = "0", oppor_product;
    private int positionPIC = 0;
    private String[] sourceArr;
    private List<ProductModel> productModels = new ArrayList<>();
    private String[] contactArr, contactArrID;
    private OpportunityModel model;
    private ProductCartAdapter simpleAdapter;
    private OpportunityAddPresenter presenter;
    private String Currency_id ="", CurrencyName, currencyIdFrom;
    private boolean from_detail = false;
    private boolean byStatgeActivity = false;
    private boolean convert = false;
    static OpportunityAddExtActivity opportunityAddExtActivity;

    public static OpportunityAddExtActivity getInstance() {
        return opportunityAddExtActivity;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_opportunityaddtambahan);

        ButterKnife.bind(this);
        this.context = this;

        opportunityAddExtActivity = this;
        this.presenter = new OpportunityAddPresenter(context, this);

        this.url = PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.URL);

        try {
            from_detail = getIntent().getExtras().getBoolean("from_detail");
            byStatgeActivity = getIntent().getExtras().getBoolean("byStatgeActivity");
            convert = getIntent().getExtras().getBoolean("convert");
        } catch (Exception e) {
        }


        try {
            model = getIntent().getExtras().getParcelable("opportunityModel");
        } catch (Exception e) {
        }

        try {
            contact_json = getIntent().getExtras().getString("pic");
        } catch (Exception e) {
        }

        getDataMaster();
        initView();
    }

    private void getDataMaster() {
        LeadSourceDao sourceDao = new LeadSourceDao(context);
        sourceArr = sourceDao.getStringArray();
    }

    private void initView() {
        Log.d("TAG", "PIC : " + contact_json);
        Log.d("TAG", "Contact ID : " + model.getContact_id());

        JSONArray array = null;
        try {
            array = new JSONArray(contact_json);

            contactArr = new String[array.length() + 1];
            contactArrID = new String[array.length() + 1];
            for (int i = 0; i <= array.length(); i++) {
                if (i == 0) {
                    contactArr[i] = "PIC";
                    contactArrID[i] = "0";
                } else {
                    if (model.getContact_id().equalsIgnoreCase(array.getJSONObject(i - 1).getString("id"))) {
                        positionPIC = i;
                    }

                    contactArr[i] = array.getJSONObject(i - 1).getString("first_name") + " " +
                            array.getJSONObject(i - 1).getString("last_name");
                    contactArrID[i] = array.getJSONObject(i - 1).getString("id");
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        SpinnerCustomAdapter stageAdapter = new SpinnerCustomAdapter(context,
                R.layout.spinner_default, sourceArr);
        stageAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spLeadSource.setAdapter(stageAdapter);
        spLeadSource.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                if (position != 0) {
                    lead_source = String.valueOf(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        SpinnerCustomAdapter picAdapter = new SpinnerCustomAdapter(context,
                R.layout.spinner_default, contactArr);
        picAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spPIC.setAdapter(picAdapter);
        spPIC.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                if (position != 0) {
                    oppor_pic = contactArrID[position];
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        setupProductCart();

        if (from_detail) {
            tvTitle.setText("Edit Opportunity");

            spLeadSource.setSelection(Integer.parseInt(model.getSource_id()));
            spPIC.setSelection(positionPIC);
            etDesc.setText(model.getDescription());
            etNextStep.setText(model.getNext_step());

            try {
                JSONArray arrProduct = new JSONArray(model.getProduct());
                for (int i = 0; i < arrProduct.length(); i++) {
                    ProductModel model = new ProductModel();
                    model.setDetail_id(arrProduct.getJSONObject(i).getString("product_detail_id"));
                    model.setName(arrProduct.getJSONObject(i).getString("product_name"));
                    model.setQty(arrProduct.getJSONObject(i).getString("quantity"));
                    model.setUnit(arrProduct.getJSONObject(i).getString("unit"));
                    productModels.add(model);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (convert) {
            spPIC.setSelection(positionPIC);
        }

        ivBack.setOnClickListener(click);
        ivSave.setOnClickListener(click);
        ivCurrency.setOnClickListener(click);
        tvSearchProduct.setOnClickListener(click);
    }

    private void setupProductCart() {
        simpleAdapter = new ProductCartAdapter(context, productModels);
        lvProduct.setAdapter(simpleAdapter);
        lvProduct.setSelector(R.drawable.transparent_selector);
        lvProduct.setOverScrollMode(View.OVER_SCROLL_NEVER);
        lvProduct.setOnTouchListener(new View.OnTouchListener() {
            // Setting on Touch Listener for handling the touch inside
            // ScrollView
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // Disallow the touch request for parent scroll on touch of
                // child view
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });
        lvProduct.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {

                final CustomDialog dialog = CustomDialog.setupDialogProductEdit(
                        context, productModels.get(position).getName(), productModels.get(position).getQty(), productModels.get(position).getPrice(),productModels.get(position).getDescription()) ;
                CustomTextView tvYa = (CustomTextView) dialog.findViewById(R.id.tvOK);
                CustomTextView tvTidak = (CustomTextView) dialog.findViewById(R.id.tvBack);
                ImageView ivDelete = (ImageView) dialog.findViewById(R.id.ivDelete);
                final CustomEditText etQty = (CustomEditText) dialog.findViewById(R.id.etQty);
                final CustomEditText etPrice = (CustomEditText) dialog.findViewById(R.id.etPrice);
                final CustomEditText etDesc = (CustomEditText) dialog.findViewById(R.id.etDesc);

                ivDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        productModels.remove(position);

                        simpleAdapter.notifyDataSetChanged();
                        dialog.dismiss();
                    }
                });

                tvYa.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        productModels.get(position).setQty(etQty.getText().toString());
                        productModels.get(position).setCategory_id(Currency_id);
                        productModels.get(position).setPrice(etPrice.getText().toString());
                        productModels.get(position).setDescription(etDesc.getText().toString());

                        simpleAdapter.notifyDataSetChanged();
                        dialog.dismiss();
                    }
                });
                tvTidak.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
    }

    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.ivBack:
                    final CustomDialog dialogback = CustomDialog.setupDialogConfirmation(context, "Anda ingin kembali? Data yang anda masukkan akan hilang?");
                    CustomTextView tvCancel = (CustomTextView) dialogback.findViewById(R.id.tvCancel);
                    CustomTextView tvYa = (CustomTextView) dialogback.findViewById(R.id.tvOK);

                    tvCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialogback.dismiss();
                        }
                    });

                    tvYa.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            finish();
                            dialogback.dismiss();
                        }
                    });
                    dialogback.show();
                    break;
                case R.id.ivSave:
                    if (from_detail) {
                        makeProductJSON();

                        model.setDescription(etDesc.getText().toString());
                        model.setSource_id(lead_source);
                        model.setNext_step(etNextStep.getText().toString());
                        model.setPic(oppor_pic);
                        model.setProduct(oppor_product);
                        model.setUpdate_date(getCurrentDate());

                        showLoadingDialog();
                        presenter.setupEditOpportunity(ApiParam.API_042, model);
//                      finish();
//                        OpportunityAddActivity.getInstance().finish(); //inisiatif
//                        OpportunityDetailActivity.getInstance().finish(); //inisiatif


                    } else {

                        makeProductJSON();


                        model.setDescription(etDesc.getText().toString());
                        Log.d("BBB","desc = " + etDesc.getText().toString());
                        model.setSource_id(lead_source);
                        Log.d("BBB","source = " + lead_source);
                        model.setNext_step(etNextStep.getText().toString());
                        Log.d("BBB","next = " + etNextStep.getText().toString());
                        model.setPic(oppor_pic);
                        Log.d("BBB","PIC = " + oppor_pic);
                        model.setProduct(oppor_product);
                        Log.d("BBB","product = " + oppor_product);
                        model.setOpen_date(getCurrentDate());
                        Log.d("BBB","dete = " + getCurrentDate());

                        showLoadingDialog();
                        presenter.setupAdd(ApiParam.API_041, model);
                    }
                    break;
                case R.id.tvSearchProduct:
                    if(Currency_id.equals("0") || Currency_id.equalsIgnoreCase("")){
                        tvCurency.setError("Currency wajib isi!!!");
                    } else {
                        ProductDialogFragment DialogFragment = new ProductDialogFragment(OpportunityAddExtActivity.this, new ProductDialogCallback() {

                            @Override
                            public void onProductDialogCallback(Bundle data) {
                                ProductModel model = new ProductModel();
                                model.setDetail_id(data.getString("id"));
                                model.setName(data.getString("name"));
                                model.setQty(data.getString("qty"));
                                model.setUnit(data.getString("unit"));
                                model.setPrice(data.getString("price"));
                                model.setDescription(data.getString("desc"));


                                productModels.add(model);
                                simpleAdapter.notifyDataSetChanged();
                            }
                        });

                        Bundle bundle = new Bundle();
                        bundle.putString("currencyNameid", Currency_id);
                        DialogFragment.setArguments(bundle);
                        DialogFragment.show(getSupportFragmentManager(), "productDialog");
                    }
                    break;
                case R.id.ivCurrency:
                    CurrencyDialogFragment currencyDialogFragment = new CurrencyDialogFragment(OpportunityAddExtActivity.this, new CurrencyDialogCallback() {
                        @Override
                        public void onCurrencyDialogCallback(Bundle data) {
                            tvCurency.setText(data.getString("name"));
                            CurrencyName = data.getString("name");
                            Currency_id = data.getString("id");
                            Log.d("TAG","cu_id " + Currency_id);
                        }
                    });
                    currencyDialogFragment.show(getSupportFragmentManager(),"currencyDialog");
                    break;
            }
        }
    };

    private void makeProductJSON() {
        JSONArray productCart = new JSONArray();
        try {
            for (int i = 0; i < productModels.size(); i++) {
                JSONObject product = new JSONObject();
                product.put("quantity", productModels.get(i).getQty());
                product.put("product_detail_id", productModels.get(i).getDetail_id());
                product.put("currency_id",Currency_id);
                product.put("price", productModels.get(i).getPrice());
                product.put("description", productModels.get(i).getDescription());
                productCart.put(product);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        oppor_product = productCart.toString();
    }


    private String getCurrentDate() {
        Calendar c = Calendar.getInstance();

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        return df.format(c.getTime());
    }

    @Override
    public void finishAdd(String result, String response) {
        dismissLoadingDialog();
        if (result.equalsIgnoreCase("OK")) {
            if (convert) {
                finish();
                OpportunityAddActivity.getInstance().finish();
//                LeadDetailActivity.getInstance().finish();
                finish();
            }
            finish();
            OpportunityAddActivity.getInstance().finish();

        }
    }

    @Override
    public void finishEdit(String result, String response) {
        dismissLoadingDialog();
//        if (result.equalsIgnoreCase("OK")){
        if (byStatgeActivity) {
            finish();
            OpportunityAddActivity.getInstance().finish();
            OpportunityDetailByStageActivity.getInstance().finish();
        } else {
            finish();
            OpportunityAddActivity.getInstance().finish();
            OpportunityDetailActivity.getInstance().finish();
        }

//        }
    }

}
