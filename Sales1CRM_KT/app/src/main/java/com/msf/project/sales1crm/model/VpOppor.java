package com.msf.project.sales1crm.model;

import android.os.Parcel;
import android.os.Parcelable;

public class VpOppor implements Comparable, Parcelable {
	private String closeDate;
	private String other;
	private String industryId;
	private String categoryName;
	private String ownerId;
	private String description;
	private String type;
	private String contactId;
	private String branch;
	private String stagePercentage;
	private String opportunitySize;
	private String lastUpdatedBy;
	private String deletedDate;
	private String categoryId;
	private String nextStep;
	private String opportunityName;
	private String accountName;
	private String opportunityStatusName;
	private String stageName;
	private String id;
	private String titleName;
	private String createDate;
	private String firstName;
	private String email;
	private String openDate;
	private String contactName;
	private String ownerName;
	private String reminder;
	private String stageId;
	private String probability;
	private String positionName;
	private String titleId;
	private String lastName;
	private String updateDate;
	private String leadSourceId;
	private String opportunityStatusId;
	private String accountId;
	private String lastUpdatedName;
	private String userId;
	private String phone;
	private String actualCloseDate;
	private String leadSourceName;
	private String status;
	private String positionId;

	public VpOppor(){
		setCloseDate("");
		setOther("");
		setIndustryId("");
		setCategoryName("");
		setOwnerId("");
		setDescription("");
		setType("");
		setContactId("");
		setBranch("");
		setStagePercentage("");
		setOpportunitySize("");
		setLastUpdatedBy("");
		setDeletedDate("");
		setCategoryId("");
		setNextStep("");
		setOpportunityName("");
		setAccountName("");
		setOpportunityStatusName("");
		setStageName("");
		setId("");
		setTitleName("");
		setCreateDate("");
		setFirstName("");
		setEmail("");
		setOpenDate("");
		setContactName("");
		setOwnerName("");
		setReminder("");
		setProbability("");
		setPositionName("");
		setTitleId("");
		setLastName("");
		setUpdateDate("");
		setLeadSourceId("");
		setOpportunityStatusId("");
		setAccountId("");
		setLastUpdatedName("");
		setUserId("");
		setPhone("");
		setActualCloseDate("");
		setLeadSourceName("");
		setStatus("");
		setPositionId("");





	}

	public VpOppor(Parcel in){
		readFromParcel(in);
	}

	public static final Creator<VpOppor> CREATOR = new Creator<VpOppor>() {
		@Override
		public VpOppor createFromParcel(Parcel in) {
			return new VpOppor(in);
		}

		@Override
		public VpOppor[] newArray(int size) {
			return new VpOppor[size];
		}
	};

	public void readFromParcel (Parcel in){
		closeDate = in.readString();
		other = in.readString();
		industryId = in.readString();
		categoryName = in.readString();
		ownerId = in.readString();
		description = in.readString();
		type = in.readString();
		categoryId = in.readString();
		branch = in.readString();
		stagePercentage = in.readString();
		opportunitySize = in.readString();
		lastUpdatedBy = in.readString();
		deletedDate = in.readString();
		categoryId = in.readString();
		nextStep = in.readString();
		opportunityName = in.readString();
		accountName = in.readString();
		opportunityStatusName = in.readString();
	stageName = in.readString();
	id = in.readString();
	titleName = in.readString();
	email = in.readString();
	openDate = in.readString();
	categoryName = in.readString();
	ownerName = in.readString();
	reminder = in.readString();
	stageId = in.readString();
	probability = in.readString();
	positionName = in.readString();
	titleId = in.readString();
	lastName = in.readString();
	updateDate = in.readString();
	leadSourceId = in.readString();
	opportunityStatusId = in.readString();
	accountId = in.readString();
	lastUpdatedName = in.readString();
	userId = in.readString();
	phone = in.readString();
	actualCloseDate = in.readString();
	leadSourceName = in.readString();
	status = in.readString();
	positionId = in.readString();
	}

	public void setCloseDate(String closeDate){
		this.closeDate = closeDate;
	}

	public String getCloseDate(){
		return closeDate;
	}

	public void setOther(String other){
		this.other = other;
	}

	public String getOther(){
		return other;
	}

	public void setIndustryId(String industryId){
		this.industryId = industryId;
	}

	public String getIndustryId(){
		return industryId;
	}

	public void setCategoryName(String categoryName){
		this.categoryName = categoryName;
	}

	public String getCategoryName(){
		return categoryName;
	}

	public void setOwnerId(String ownerId){
		this.ownerId = ownerId;
	}

	public String getOwnerId(){
		return ownerId;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setType(String type){
		this.type = type;
	}

	public String getType(){
		return type;
	}

	public void setContactId(String contactId){
		this.contactId = contactId;
	}

	public String getContactId(){
		return contactId;
	}

	public void setBranch(String branch){
		this.branch = branch;
	}

	public String getBranch(){
		return branch;
	}

	public void setStagePercentage(String stagePercentage){
		this.stagePercentage = stagePercentage;
	}

	public String getStagePercentage(){
		return stagePercentage;
	}

	public void setOpportunitySize(String opportunitySize){
		this.opportunitySize = opportunitySize;
	}

	public String getOpportunitySize(){
		return opportunitySize;
	}

	public void setLastUpdatedBy(String lastUpdatedBy){
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public String getLastUpdatedBy(){
		return lastUpdatedBy;
	}

	public void setDeletedDate(String deletedDate){
		this.deletedDate = deletedDate;
	}

	public String getDeletedDate(){
		return deletedDate;
	}

	public void setCategoryId(String categoryId){
		this.categoryId = categoryId;
	}

	public String getCategoryId(){
		return categoryId;
	}

	public void setNextStep(String nextStep){
		this.nextStep = nextStep;
	}

	public String getNextStep(){
		return nextStep;
	}

	public void setOpportunityName(String opportunityName){
		this.opportunityName = opportunityName;
	}

	public String getOpportunityName(){
		return opportunityName;
	}

	public void setAccountName(String accountName){
		this.accountName = accountName;
	}

	public String getAccountName(){
		return accountName;
	}

	public void setOpportunityStatusName(String opportunityStatusName){
		this.opportunityStatusName = opportunityStatusName;
	}

	public String getOpportunityStatusName(){
		return opportunityStatusName;
	}

	public void setStageName(String stageName){
		this.stageName = stageName;
	}

	public String getStageName(){
		return stageName;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setTitleName(String titleName){
		this.titleName = titleName;
	}

	public String getTitleName(){
		return titleName;
	}

	public void setCreateDate(String createDate){
		this.createDate = createDate;
	}

	public String getCreateDate(){
		return createDate;
	}

	public void setFirstName(String firstName){
		this.firstName = firstName;
	}

	public String getFirstName(){
		return firstName;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setOpenDate(String openDate){
		this.openDate = openDate;
	}

	public String getOpenDate(){
		return openDate;
	}

	public void setContactName(String contactName){
		this.contactName = contactName;
	}

	public String getContactName(){
		return contactName;
	}

	public void setOwnerName(String ownerName){
		this.ownerName = ownerName;
	}

	public String getOwnerName(){
		return ownerName;
	}

	public void setReminder(String reminder){
		this.reminder = reminder;
	}

	public String getReminder(){
		return reminder;
	}

	public void setStageId(String stageId){
		this.stageId = stageId;
	}

	public String getStageId(){
		return stageId;
	}

	public void setProbability(String probability){
		this.probability = probability;
	}

	public String getProbability(){
		return probability;
	}

	public void setPositionName(String positionName){
		this.positionName = positionName;
	}

	public String getPositionName(){
		return positionName;
	}

	public void setTitleId(String titleId){
		this.titleId = titleId;
	}

	public String getTitleId(){
		return titleId;
	}

	public void setLastName(String lastName){
		this.lastName = lastName;
	}

	public String getLastName(){
		return lastName;
	}

	public void setUpdateDate(String updateDate){
		this.updateDate = updateDate;
	}

	public String getUpdateDate(){
		return updateDate;
	}

	public void setLeadSourceId(String leadSourceId){
		this.leadSourceId = leadSourceId;
	}

	public String getLeadSourceId(){
		return leadSourceId;
	}

	public void setOpportunityStatusId(String opportunityStatusId){
		this.opportunityStatusId = opportunityStatusId;
	}

	public String getOpportunityStatusId(){
		return opportunityStatusId;
	}

	public void setAccountId(String accountId){
		this.accountId = accountId;
	}

	public String getAccountId(){
		return accountId;
	}

	public void setLastUpdatedName(String lastUpdatedName){
		this.lastUpdatedName = lastUpdatedName;
	}

	public String getLastUpdatedName(){
		return lastUpdatedName;
	}

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public void setPhone(String phone){
		this.phone = phone;
	}

	public String getPhone(){
		return phone;
	}

	public void setActualCloseDate(String actualCloseDate){
		this.actualCloseDate = actualCloseDate;
	}

	public String getActualCloseDate(){
		return actualCloseDate;
	}

	public void setLeadSourceName(String leadSourceName){
		this.leadSourceName = leadSourceName;
	}

	public String getLeadSourceName(){
		return leadSourceName;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	public void setPositionId(String positionId){
		this.positionId = positionId;
	}

	public String getPositionId(){
		return positionId;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(closeDate);
		dest.writeString(other);
		dest.writeString(industryId);
		dest.writeString(categoryName);
		dest.writeString(ownerId);
		dest.writeString(description);
		dest.writeString(type);
		dest.writeString(contactId);
		dest.writeString(branch);
		dest.writeString(stagePercentage);
		dest.writeString(opportunitySize);
		dest.writeString(lastUpdatedBy);
		dest.writeString(deletedDate);
		dest.writeString(categoryId);
		dest.writeString(nextStep);
		dest.writeString(opportunityName);
		dest.writeString(accountName);
		dest.writeString(opportunityStatusName);
		dest.writeString(stageName);
		dest.writeString(id);
		dest.writeString(titleName);
		dest.writeString(createDate);
		dest.writeString(firstName);
		dest.writeString(email);
		dest.writeString(openDate);
		dest.writeString(contactName);
		dest.writeString(ownerName);
		dest.writeString(reminder);
		dest.writeString(stageId);
		dest.writeString(probability);
		dest.writeString(positionName);
		dest.writeString(titleId);
		dest.writeString(lastName);
		dest.writeString(updateDate);
		dest.writeString(leadSourceId);
		dest.writeString(opportunityStatusId);
		dest.writeString(accountId);
		dest.writeString(lastUpdatedName);
		dest.writeString(userId);
		dest.writeString(phone);
		dest.writeString(actualCloseDate);
		dest.writeString(leadSourceName);
		dest.writeString(status);
		dest.writeString(positionId);
	}

	@Override
	public int compareTo(Object o) {
		return 0;
	}
}
