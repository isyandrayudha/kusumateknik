package com.msf.project.sales1crm.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.adapter.SortAdapter;
import com.msf.project.sales1crm.callback.SortDialogCallback;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.model.SortModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

@SuppressLint("ValidFragment")
public class SortDialogFragment extends BaseDialogFragment {

    @BindView(R.id.rlNoConnection)
    RelativeLayout rlNoConnection;

    @BindView(R.id.tvRefresh)
    CustomTextView tvRefresh;

    @BindView(R.id.lvSort)
    ListView lvSort;

    @BindView(R.id.lvSortSwipe)
    SwipeRefreshLayout lvSortSwipe;

    @BindView(R.id.llNotFound)
    LinearLayout llNotFound;

    private View view;
    private Context context;
    private SortAdapter adapter;
    private SortDialogCallback callback;
    private List<SortModel> list = new ArrayList<>();
    private String[] sort;

    @SuppressLint("ValidFragment")
    public SortDialogFragment(Context context, SortDialogCallback listener, String[] sort) {
        this.context = context;
        this.callback = listener;
        this.sort = sort;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, 0);
    }

    @Override
    public void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
        Window window = getDialog().getWindow();
        WindowManager.LayoutParams windowParams = window.getAttributes();
        windowParams.width = (int) (this.context.getResources().getDisplayMetrics().widthPixels * 0.8);
        windowParams.height = (int) (this.context.getResources().getDisplayMetrics().heightPixels * 0.5);
        window.setAttributes(windowParams);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreateView(inflater, container, savedInstanceState);

        this.view = inflater.inflate(R.layout.dialog_sortlist, container, false);

        return this.view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);

        initView();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
    }

    private void initView() {
        for (int x = 0; x < sort.length; x++){
            SortModel model = new SortModel();
            model.setId("" + Integer.valueOf(x+1));
            model.setName(sort[x]);
            list.add(model);
        }

        setList();
    }

    private void setList() {
        adapter = new SortAdapter(context, this.list);

        List<SortAdapter.Row> rows = new ArrayList<SortAdapter.Row>();
        for (SortModel cartModel : list){
            //Read List by Row to insert into List Adapter
            rows.add(new SortAdapter.Item(cartModel));
        }
        //Set Row Adapter
        adapter.setRows(rows);
        lvSort.setAdapter(adapter);
        lvSort.setOnItemClickListener(new SortDialogFragment.AdapterOnItemClick());
        lvSort.deferNotifyDataSetChanged();
    }

    private class AdapterOnItemClick implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            // TODO Auto-generated method stub
            final SortAdapter.Item item = (SortAdapter.Item) lvSort.getAdapter().getItem(position);
            Bundle bundle = new Bundle();
            bundle.putString("id", item.text.getId());
            callback.onDialogCallback(bundle);
            dismiss();
        }
    }
}
