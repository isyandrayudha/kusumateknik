package com.msf.project.sales1crm.model;

public class AccountContactItem{
	private String accountId;
	private String phone;
	private String lastName;
	private String id;
	private String title;
	private String firstName;
	private String email;
	private String positionId;

	public void setAccountId(String accountId){
		this.accountId = accountId;
	}

	public String getAccountId(){
		return accountId;
	}

	public void setPhone(String phone){
		this.phone = phone;
	}

	public String getPhone(){
		return phone;
	}

	public void setLastName(String lastName){
		this.lastName = lastName;
	}

	public String getLastName(){
		return lastName;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}

	public void setFirstName(String firstName){
		this.firstName = firstName;
	}

	public String getFirstName(){
		return firstName;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setPositionId(String positionId){
		this.positionId = positionId;
	}

	public String getPositionId(){
		return positionId;
	}

	@Override
 	public String toString(){
		return 
			"AccountContactItem{" + 
			"account_id = '" + accountId + '\'' + 
			",phone = '" + phone + '\'' + 
			",last_name = '" + lastName + '\'' + 
			",id = '" + id + '\'' + 
			",title = '" + title + '\'' + 
			",first_name = '" + firstName + '\'' + 
			",email = '" + email + '\'' + 
			",position_id = '" + positionId + '\'' + 
			"}";
		}
}
