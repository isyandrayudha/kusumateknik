package com.msf.project.sales1crm.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.model.AccountModel;

import java.util.List;

public class cardAdapter extends PagerAdapter{

    private Context context;
    private String url;
    private List<AccountModel> accountModelList;
    private LayoutInflater layoutInflater;

    public cardAdapter (Context context, List<AccountModel> objects) {
        this.context = context;
        this.accountModelList = objects;
    }

    public static abstract class Card {

    }

    public static final class Section extends cardAdapter.Card {
        public final String text ;

        public Section(String text) {
            this.text = text;
        }

    }

    public static final class  Item extends Card {
        public final AccountModel text;

        public Item(AccountModel text) {
            this.text = text;
        }

        public AccountModel getText() {
            return text;
        }
    }

    private List<cardAdapter.Card> cards;

    public void setCards(List<cardAdapter.Card> cards) {
        this.cards = cards;
    }


    @Override
    public int getCount() {
        return cards.size();
    }

    public Object getItem (int i) {
        return  cards.get(i);
    }

    public long getItemId (int i) {
        return i;
    }



    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view.equals(object);
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {

         layoutInflater = LayoutInflater.from(context);
         View view = layoutInflater.inflate(R.layout.item,container,false);

        CustomTextView tvAccountName, tvIndustry, tvIndustryy, tvType, tvOwnerName;

        tvAccountName = view.findViewById(R.id.tvAccountName);
        tvIndustry = view.findViewById(R.id.tvIndustry);
        tvIndustryy = view.findViewById(R.id.tvindustryy);
        tvType = view.findViewById(R.id.tvType);
        tvOwnerName = view.findViewById(R.id.tvOwnerName);


        tvAccountName.setText(accountModelList.get(position).getAccount_name());
        tvIndustry.setText(accountModelList.get(position).getIndustry_name());
        tvIndustryy.setText(accountModelList.get(position).getIndustry_name());
        tvType.setText(accountModelList.get(position).getType_name());
        tvOwnerName.setText(accountModelList.get(position).getOwner_name());

        container.addView(view,0);
        return view;
    }

    private static class ViewHolder {
        CustomTextView tvAccountName, tvIndustry, tvIndustryy, tvType, tvOwnerName;

    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View)object);
    }
}
