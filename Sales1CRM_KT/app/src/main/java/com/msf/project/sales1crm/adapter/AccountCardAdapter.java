package com.msf.project.sales1crm.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;

import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.model.AccountModel;

import java.util.List;

public class AccountCardAdapter  extends BaseAdapter{

    private Context context;
    private String url;
    private List<AccountModel> accountModelList;

    public AccountCardAdapter(Context context, List<AccountModel> objects){
        this.context = context;
        this.accountModelList = objects;
    }

    public static abstract class Card {

    }

    public static final class Section extends Card {
        public final String text ;

        public Section(String text) {
            this.text = text;
        }

    }

        public static final class  Item extends Card {
            public final AccountModel text;

            public Item(AccountModel text) {
                this.text = text;
            }

            public AccountModel getText() {
                return text;
            }
        }

        private List<Card> cards;

        public void setCards(List<Card> cards) {
            this.cards = cards;
        }



    @Override
    public int getCount() {
        return cards.size() ;
    }

    @Override
    public Object getItem(int i) {
        return cards.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View converview, ViewGroup viewGroup) {
       View view = converview;
       final ViewHolder holder;


       final Item item = (Item) getItem(i);
       if (view == null) {
           LayoutInflater inflater = (LayoutInflater) viewGroup.getContext()
                   .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = (LinearLayout) inflater.inflate(R.layout.card_account,viewGroup,false);
            holder = new ViewHolder();
            holder.tvAccountName = (CustomTextView) view.findViewById(R.id.tvAccountName);
            holder.tvIndustry = (CustomTextView) view.findViewById(R.id.tvIndustry);
            holder.tvIndustryy = (CustomTextView) view.findViewById(R.id.tvindustryy);
            holder.tvType = (CustomTextView) view.findViewById(R.id.tvType);
            holder.tvOwnerName = (CustomTextView) view.findViewById(R.id.tvAccountName);
            view.setTag(holder);
       }
       else {
           holder = (ViewHolder) view.getTag();
       }
       holder.tvAccountName.setText(item.text.getAccount_name());
       holder.tvIndustry.setText(item.text.getIndustry_name());
       holder.tvIndustryy.setText(item.text.getIndustry_name());
       holder.tvType.setText(item.text.getType_name());
       holder.tvOwnerName.setText(item.text.getOwner_name());
        return view;
    }

    private static class ViewHolder {
        CustomTextView tvAccountName, tvIndustry, tvIndustryy, tvType, tvOwnerName;

    }
}

