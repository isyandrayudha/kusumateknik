package com.msf.project.sales1crm.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.msf.project.sales1crm.MenuActivity;
import com.msf.project.sales1crm.OpportunityAddActivity;
import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.adapter.OpportunityListTabByStageAdapter;
import com.msf.project.sales1crm.callback.SortDialogCallback;
import com.msf.project.sales1crm.customview.CustomEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ViewpipelineTabFragment extends Fragment {

    @BindView(R.id.etSearch)
    CustomEditText etSearch;
    @BindView(R.id.ivClear)
    ImageView ivClear;
    @BindView(R.id.viewpager)
    ViewPager viewpager;
    @BindView(R.id.tabDots)
    TabLayout tabDots;
    Unbinder unbinder;
    @BindView(R.id.faOpportunityAdd)
    FloatingActionButton faOpportunityAdd;
    @BindView(R.id.menu)
    FloatingActionMenu menu;
    @BindView(R.id.prev)
    ImageView prev;
    @BindView(R.id.next)
    ImageView next;
    private View view;
    private Context context;
    private OpportunityListTabByStageAdapter adapter;
    private MenuActivity menuActivity;
    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    private int sort_id = 0;

    private String[] sort = {"Name A to Z", "Name Z to A", "Opportunity Size A to Z", "Opportunity Size Z to A" ,
            "Create Date Ascending", "Create Date Descending", "Due Date Ascending", "Due Date Descending"};

    interface ClickActionFilterListener {
        void onClickItemFilter(Integer id);
    }

    private ClickActionFilterListener mFilterListener;

    public void setClickFilterListener (ClickActionFilterListener listener) {
        this.mFilterListener = listener;
    }


    public static Fragment newInstance() {
        ViewpipelineTabFragment fragment = new ViewpipelineTabFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onAttach(Activity activity) {
        menuActivity = (MenuActivity) activity;
        super.onAttach(activity);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_tabviewpipeline, container, false);
        setHasOptionsMenu(true);
        ButterKnife.bind(this, view);
        this.context = getActivity();
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.context = getActivity();
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Opportunity");
        initData();

    }



    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.faOpportunityAdd:
                        startActivity(new Intent(context, OpportunityAddActivity.class));
                    break;
                case R.id.prev:
                   viewpager.setCurrentItem(getItem(-1),true);
                    break;
                case R.id.next:
                    viewpager.setCurrentItem(getItem(+1),true);
                    break;
                case R.id.ivClear:
                    etSearch.setText("");
                    etSearch.clearFocus();
                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(etSearch.getWindowToken(),0);
                    break;
            }
        }
    };

    private int getItem(int i) {
        return viewpager.getCurrentItem() + i;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_oppor, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_list:
                    MenuActivity.getInstance().setFragment(OpportunityTabFragment.newInstance());
                    item.setIcon(R.drawable.ic_list_white_24dp);
                break;
            case R.id.action_cards:
                    MenuActivity.getInstance().setFragment(OpportunityTabCardFragment.newInstance());
                    item.setIcon(R.drawable.ic_cards_black_24dp);
                break;
            case R.id.action_maps:
                    MenuActivity.getInstance().setFragment(OpportunityTabMapFragment.newInstance());
                    item.setIcon(R.drawable.ic_map_white_24dp);
                break;
            case R.id.action_viewpipeline:
                    MenuActivity.getInstance().setFragment(ViewpipelineTabFragment.newInstance());
                break;
            case R.id.action_filter:
                SortDialogFragment dialogFragment = new SortDialogFragment(context, new SortDialogCallback() {
                    @Override
                    public void onDialogCallback(Bundle data) {
                        sort_id = Integer.parseInt(data.getString("id"));
                        mFilterListener.onClickItemFilter(sort_id);
                    }
                }, sort);
                dialogFragment.show(getActivity().getSupportFragmentManager(), "sortDialog");
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void initData() {
        faOpportunityAdd.setOnClickListener(click);
        prev.setOnClickListener(click);
        next.setOnClickListener(click);
       ivClear.setOnClickListener(click);
//        tabDots.addTab(tabDots.newTab());
        tabDots.addTab(tabDots.newTab());
        tabDots.addTab(tabDots.newTab());
        tabDots.addTab(tabDots.newTab());
        tabDots.addTab(tabDots.newTab());
        tabDots.addTab(tabDots.newTab());
        tabDots.addTab(tabDots.newTab());

        adapter = new OpportunityListTabByStageAdapter(menuActivity.getSupportFragmentManager(), tabDots.getTabCount(),this);
        viewpager.setAdapter(adapter);
        viewpager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabDots));
        tabDots.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }




    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
