package com.msf.project.sales1crm.service;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.os.StrictMode;
import android.util.Log;

import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import com.msf.project.sales1crm.utility.Sales1CRMParams;

import org.json.JSONException;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * Created by christianmacbook on 11/05/18.
 */

public class NetworkConnection extends IntentService {
    private final String TAG = NetworkConnection.class.getSimpleName();
    private String[] responseString = {""};
    private StringBuilder builder;
    private Messenger messenger;
    private Message message;
    private String url;
    private String params;
    private String from = "";
    private RequestBody requestBody;

    public NetworkConnection() {
        super("NetworkConnection");
    }

    public void doObtainDataFromServer(String url) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);

        //Limit Time Out HTTP Client 10 Second
        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setConnectTimeout(5, TimeUnit.SECONDS);
        okHttpClient.setReadTimeout(5, TimeUnit.SECONDS);
        okHttpClient.setWriteTimeout(5, TimeUnit.SECONDS);


        try {
            if (from.equals("keyword"))
                this.requestBody = Sales1CRMParams.PostParamUtility.getDomainCheckParam(params);
            else if (from.equals("LoginToServer"))
                this.requestBody = Sales1CRMParams.PostParamUtility.getLoginPostParam(params);
            else if (from.equals("ApiKey"))
                this.requestBody = Sales1CRMParams.PostParamUtility.getAPIKeyPostParam(params);
            else if (from.equals("LocationID"))
                this.requestBody = Sales1CRMParams.PostParamUtility.getLocationID(params);
            else if (from.equals("AccountList"))
                this.requestBody = Sales1CRMParams.PostParamUtility.getAccountListParam(params);
            else if (from.equals("AccountAdd"))
                this.requestBody = Sales1CRMParams.PostParamUtility.getAccountAddParam(params);
            else if (from.equals("AccountQuick"))
                this.requestBody = Sales1CRMParams.PostParamUtility.getAccountQuickParam(params);
            else if (from.equals("AccountEdit"))
                this.requestBody = Sales1CRMParams.PostParamUtility.getAccountEditParam(params);
            else if (from.equals("LeadAdd"))
                this.requestBody = Sales1CRMParams.PostParamUtility.getLeadAddParams(params);
            else if (from.equals("LeadEdit"))
                this.requestBody = Sales1CRMParams.PostParamUtility.getLeadEditParams(params);
            else if (from.equals("OpportunityList"))
                this.requestBody = Sales1CRMParams.PostParamUtility.getOpportunityListParam(params);
            else if (from.equals("OpportunityAdd"))
                this.requestBody = Sales1CRMParams.PostParamUtility.getOpportunityAddParam(params);
            else if (from.equals("OpportunityEdit"))
                this.requestBody = Sales1CRMParams.PostParamUtility.getOpportunityEditParam(params);
            else if (from.equals("OpportunityListByNone"))
                this.requestBody = Sales1CRMParams.PostParamUtility.getOpportunityListByNone(params);
            else if (from.equals("OpportunityListByMQL"))
                this.requestBody = Sales1CRMParams.PostParamUtility.getOpportunityListByMQL(params);
            else if (from.equals("OpportunityCLose"))
                this.requestBody = Sales1CRMParams.PostParamUtility.getOpportunityClose(params);
            else if (from.equals("ProductList"))
                this.requestBody = Sales1CRMParams.PostParamUtility.getProductList(params);
            else if (from.equals("EventList"))
                this.requestBody = Sales1CRMParams.PostParamUtility.getEventList(params);
            else if (from.equals("EventAdd"))
                this.requestBody = Sales1CRMParams.PostParamUtility.getEventAdd(params);
            else if (from.equals("EventEdit"))
                this.requestBody = Sales1CRMParams.PostParamUtility.getEventEdit(params);
            else if (from.equals("TaskList"))
                this.requestBody = Sales1CRMParams.PostParamUtility.getTaskList(params);
            else if (from.equals("TaskByStatus"))
                this.requestBody = Sales1CRMParams.PostParamUtility.getTaskListbystatus(params);
            else if (from.equals("TaskAdd"))
                this.requestBody = Sales1CRMParams.PostParamUtility.getTaskAdd(params);
            else if (from.equals("TaskEdit"))
                this.requestBody = Sales1CRMParams.PostParamUtility.getTaskEdit(params);
            else if (from.equals("CallList"))
                this.requestBody = Sales1CRMParams.PostParamUtility.getCallList(params);
            else if (from.equals("CallAdd"))
                this.requestBody = Sales1CRMParams.PostParamUtility.getCallAdd(params);
            else if (from.equals("CallEdit"))
                this.requestBody = Sales1CRMParams.PostParamUtility.getCallEdit(params);
            else if (from.equals("TotalLeads"))
                this.requestBody = Sales1CRMParams.PostParamUtility.getTotalLeads(params);
            else if (from.equals("LeadConversion"))
                this.requestBody = Sales1CRMParams.PostParamUtility.getLeadConversion(params);
            else if (from.equals("OpportunityIndustry"))
                this.requestBody = Sales1CRMParams.PostParamUtility.getOpportuntiyIndustry(params);
            else if (from.equals("OpportunityDuration"))
                this.requestBody = Sales1CRMParams.PostParamUtility.getOpportunityDuration(params);
            else if (from.equals("SalesCycle"))
                this.requestBody = Sales1CRMParams.PostParamUtility.getSalesCycle(params);
            else if (from.equals("ProductSold"))
                this.requestBody = Sales1CRMParams.PostParamUtility.getProductSold(params);
            else if (from.equals("ActivityEventList"))
                this.requestBody = Sales1CRMParams.PostParamUtility.getActivityEventList(params);
            else if (from.equals("ActivityTodoList"))
                this.requestBody = Sales1CRMParams.PostParamUtility.getActivityTodoList(params);
            else if (from.equals("ActivityCallsList"))
                this.requestBody = Sales1CRMParams.PostParamUtility.getActivityCallsList(params);
            else if (from.equals("ActivityEventToday"))
                this.requestBody = Sales1CRMParams.PostParamUtility.getActivityEventToday(params);
            else if (from.equals("ActivityTodoEdit"))
                this.requestBody = Sales1CRMParams.PostParamUtility.getActivityTodoEdit(params);
            else if (from.equals("UploadImage"))
                this.requestBody = Sales1CRMParams.PostParamUtility.getUploadImage(params);
            else if (from.equals("LocationUser"))
                this.requestBody = Sales1CRMParams.PostParamUtility.getSaveLocationUser(params);
            else if(from.equals("ContactList"))
                this.requestBody = Sales1CRMParams.PostParamUtility.getRelatedPeople(params);
            else if(from.equals("contactAdd"))
                this.requestBody = Sales1CRMParams.PostParamUtility.getContactAdd(params);
            else if (from.equals("Vp"))
                this.requestBody = Sales1CRMParams.PostParamUtility.getVp(params);
            else if (from.equals("DBmachineList"))
                this.requestBody = Sales1CRMParams.PostParamUtility.getDBmachineList(params);
            else if (from.equals("DBmachineAdd"))
                this.requestBody = Sales1CRMParams.PostParamUtility.getDBmachineAdd(params);
            else if(from.equals("DBmachineEdit"))
                this.requestBody = Sales1CRMParams.PostParamUtility.getDBmachineEdit(params);
            else if(from.equals("opporDetail"))
                this.requestBody = Sales1CRMParams.PostParamUtility.getOppDetail(params);
            else if(from.equals("convert"))
                this.requestBody = Sales1CRMParams.PostParamUtility.getConvert(params);
            else if(from.equalsIgnoreCase("KursCalculator"))
                this.requestBody = Sales1CRMParams.PostParamUtility.getPrice(params);
            else if(from.equals("QuotationList"))
                this.requestBody = Sales1CRMParams.PostParamUtility.getQoutation(params);
            else if(from.equals("contactEdit"))
                this.requestBody = Sales1CRMParams.PostParamUtility.getContactEdit(params);
            else if(from.equalsIgnoreCase("meetToList")){
                this.requestBody = Sales1CRMParams.PostParamUtility.getMeetTolist(params);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Request request = new Request.Builder()
                .url(url)
                .addHeader("Content-Type", "application/x-www-form-urlencoded")
                .addHeader("Content-Encoding", "application/gzip")
                .addHeader("Connection", "Keep-Alive")
                .addHeader("User-Agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.45 Safari/535.19")
                .post(this.requestBody)
                .build();
        Log.d(TAG, url);

        Call call = okHttpClient.newCall(request);
        call.enqueue(new NetworkConnectionCallback());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        this.messenger = (Messenger) intent.getParcelableExtra("messenger");
        this.url = intent.getStringExtra("url");
        this.params = intent.getStringExtra("params");
        this.from = intent.getStringExtra("from");
        this.message = Message.obtain();
        Log.d(TAG, "params : " + params);
        Log.d(TAG, "from : " + from);
        doObtainDataFromServer(this.url);
    }

    private class   NetworkConnectionCallback implements Callback {

        //Network Failure Connection
        @Override
        public void onFailure(Request request, IOException e) {
            if (e.getMessage() != null) {
                Log.d(TAG, "failure " + e.getMessage());
            }
            onFailureInMainThread();
        }

        //Network Success Connection
        @Override
        public void onResponse(final Response response) throws IOException {
            try {
                if (response.isSuccessful()) {
                    onResponseInMainThread(response);
                }else {
                    onFailureInMainThread();
                }
            } catch (IOException e) {
                Log.d(TAG, "Exception " + e.getMessage());
                if (e.getMessage().equalsIgnoreCase("timeout")){
                    onFailureInMainThread();
                }
            }
        }
    }

    //Response Failure Connection
    public void onFailureInMainThread() {
        Bundle bundle = new Bundle();
        bundle.putString("network_response", this.responseString[0]);
        bundle.putString("network_failure", "yes");
        this.message.setData(bundle);
        try {
            this.messenger.send(this.message);
        } catch (RemoteException e) {
            Log.d(TAG, "Exception" + e.getMessage());
        }
    }

    //Response Success Connection
    public void onResponseInMainThread(Response response) throws IOException {
        this.responseString[0] = response.body().string();
        Log.d(TAG, "responseString[0] " + this.responseString[0]);
        Log.d(TAG, "message.what" + this.message.what);
        Bundle bundle = new Bundle();
        bundle.putString("network_response", this.responseString[0]);
        bundle.putString("network_failure", "no");
        this.message.setData(bundle);
        try {
            this.messenger.send(this.message);
        } catch (RemoteException e) {
            Log.d(TAG, "Exception11" + e.getMessage());
        }
    }
}
