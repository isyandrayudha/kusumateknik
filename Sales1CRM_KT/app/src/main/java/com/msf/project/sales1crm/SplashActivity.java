package com.msf.project.sales1crm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessaging;
import com.msf.project.sales1crm.firebase.FirebaseNotification;
import com.msf.project.sales1crm.utility.PreferenceUtility;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

/**
 * Created by christianmacbook on 11/05/18.
 */

public class SplashActivity extends BaseActivity {
    private Context context;
    private BroadcastReceiver mRegistrationBroadcastReceiver;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        this.context = this;

        PreferenceUtility.getInstance().saveData(context, PreferenceUtility.URL, "https://kusumateknik.sales1crm.com");

        runUIThread();

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(Sales1CRMUtils.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(Sales1CRMUtils.TOPIC_GLOBAL);

                    displayFirebaseRegId();

                } else if (intent.getAction().equals(Sales1CRMUtils.PUSH_NOTIFICATION)) {
                    // new push notification is received
                    String message = intent.getStringExtra("message");
                    Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();

                }
            }
        };
    }

    private void runUIThread() {
        new Handler().postDelayed(new Runnable() {

			/*
             * Showing splash screen with a timer. This will be useful when you
			 * want to show case your app logo / company
			 */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                // Check Domain
//                if (PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.KEY_DOMAIN).equalsIgnoreCase("")) {
//                    Intent i = new Intent(SplashActivity.this,
//                            DomainCheckActivity.class);
//                    startActivity(i);
//                    finish();
//                } else {
                    if (PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.API_KEY).equalsIgnoreCase("")) {
                        startActivity(new Intent(context, LoginActivity.class));
                        finish();
                    } else {

                        if (PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.KEY_SYNCMASTER).equalsIgnoreCase("1")) {
                            startActivity(new Intent(context, MenuActivity.class));
                        } else {
                            startActivity(new Intent(context, SyncMasterActivity.class));
                        }
                        finish();
                    }
//                }
            }
        }, 1500);
    }
    private void displayFirebaseRegId() {
        Log.d("TAG", "Firebase reg id: "
                + PreferenceUtility.getInstance().loadDataString(
                SplashActivity.this, PreferenceUtility.getInstance().PREFERENCES_PROPERTY_REG_ID));
    }

    @Override
    protected void onResume() {
        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Sales1CRMUtils.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Sales1CRMUtils.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        FirebaseNotification.clearNotifications(getApplicationContext());

        super.onResume();
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }
}
