package com.msf.project.sales1crm.presenter;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;

import com.msf.project.sales1crm.callback.UploadImageCallback;
import com.msf.project.sales1crm.model.UploadImageModel;
import com.msf.project.sales1crm.model.UserModel;
import com.msf.project.sales1crm.service.NetworkConnection;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.PreferenceUtility;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;
import com.msf.project.sales1crm.utility.Sales1CRMUtilsJSON;

import org.json.JSONException;
import org.json.JSONObject;

public class UploadImageProfilePresenter {
    private final String TAG = UploadImageProfilePresenter.class.getSimpleName();
    private Context context;
    private Messenger messenger;
    private Message message;
    private Bundle bundle;
    private Handler handler;
    private String failureResponse = "";
    private String[] stringResponse = {""};
    private UploadImageCallback callback;
    private UserModel userModel;
    private String resultUpload = "NG";
    private String response = "Tolong Periksa Koneksi Anda";

    public UploadImageProfilePresenter(Context context, UploadImageCallback listener) {
        this.context = context;
        this.callback = listener;
    }

    private void doNetworkService(String url, String params, String from) {
        Log.d(TAG, "url Upload " + url);
        Log.d(TAG, "params : " + params);
        Intent networkIntent = new Intent(this.context, NetworkConnection.class);
        this.messenger = new Messenger(this.handler);
        networkIntent.putExtra("messenger", this.messenger);
        networkIntent.putExtra("url", url);
        networkIntent.putExtra("params", params);
        networkIntent.putExtra("from", from);
        this.context.startService(networkIntent);
    }

    public void uploadImage(int apiIndex, UploadImageModel uploadImageModel) {
        if(Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)){
            obtainUploadImage(apiIndex, uploadImageModel);
        } else {
            callback.finishUplaod(this.resultUpload,this.response);
        }
    }

    public void obtainUploadImage(int apiIndex, UploadImageModel uploadImageModel) {
        this.handler = new Handler(this.context.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                parseUploadImage(msg);
            }
        };

            JSONObject jsonObject = new JSONObject();
            try{
                jsonObject.put("api_key", PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.API_KEY));
                jsonObject.put("photo",uploadImageModel.getPhoto());
                jsonObject.put("name",uploadImageModel.getName());
            }catch (JSONException e) {
                e.printStackTrace();
            }

            doNetworkService(ApiParam.urlBuilder(this.context,apiIndex), jsonObject.toString(),"UploadImage");
        }

        private void parseUploadImage (Message message) {
            this.message = message;
            this.bundle = this.message.getData();
            this.stringResponse[0] = this.bundle.getString("network_response");
            this.failureResponse = this.bundle.getString("network_failure");
            Log.d(TAG, "responseString[0] info " + this.stringResponse[0]);
            if(this.failureResponse.equalsIgnoreCase("yes")){
                this.callback.finishUplaod(this.resultUpload, this.response);
            } else {
                try {
                    this.resultUpload = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                    this.callback.finishUplaod(this.resultUpload, this.response);
                } catch (JSONException e) {
                    Log.d(TAG, "Exception detail Response " + e.getMessage());
                }
            }
        }

        public void setupDisplayImage(int apiIndex){
        if(Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)){
            obtainDisplayImage(apiIndex);
        } else {
            callback.displayImage(this.resultUpload,this.userModel);
        }
        }

        public void obtainDisplayImage(int apiIndex){
        this.handler = new Handler(this.context.getMainLooper()){
            @Override
            public void handleMessage(Message msg) {
                parseDisplayImage(msg);
            }
        };

            JSONObject jsonObject = new JSONObject();
            try{
                jsonObject.put("api_key",PreferenceUtility.getInstance().loadDataString(context,PreferenceUtility.API_KEY));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            doNetworkService(ApiParam.urlBuilder(this.context,apiIndex),jsonObject.toString(),"ApiKey");
        }

        private void parseDisplayImage(Message message){
            this.message = message;
            this.bundle = this.message.getData();
            this.stringResponse[0] = this.bundle.getString("network_response");
            this.failureResponse = this.bundle.getString("network_failure");
            Log.d(TAG, "responseString[0] info images " +  this.stringResponse[0]);
            if (this.failureResponse.equalsIgnoreCase("yes")){
                this.callback.displayImage(this.resultUpload,this.userModel);
            } else {
                try{
                    this.resultUpload =Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                    if(this.resultUpload.equalsIgnoreCase("OK")){
                        this.userModel = Sales1CRMUtilsJSON.JSONUtility.getDisplayImage(this.stringResponse[0]);
                    }
                    this.callback.displayImage(this.resultUpload,this.userModel);
                } catch (JSONException e) {
                    Log.d(TAG, "Exception detail Response " + e.getMessage());
                }
            }
        }
    }

