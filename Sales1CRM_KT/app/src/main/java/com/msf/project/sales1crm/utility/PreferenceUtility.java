package com.msf.project.sales1crm.utility;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by christianmacbook on 11/05/18.
 */

public class PreferenceUtility {

    private final String SP_COMMON = "TOMBOL_PREF";
    public static final int DB_NEWVERSION = 4;
    public static final String API_KEY = "API_KEY";
    public static final String URL = "URL";
    public static final String URL_GLOBAL = "URL_GLOBAL";
    public static final String DEVICE_TOKEN = "DEVICE_TOKEN";
    public static final String PREFERENCES_PROPERTY_REG_ID = "PREFERENCES_PROPERTY_REG_ID";
    public static final String KEY_DOMAIN= "DOMAIN";
    public static final String KEY_SYNCMASTER = "KEY_SYNCMASTER";
    public static final String KEY_EMAIL = "EMAIL";
    public static final String KEY_PASSWORD = "PASSWORD";
    public static final String KEY_SUBDOMAIN = "SUBDOMAIN";
    public static final String KEY_USERID = "USERID";
    public static final String KEY_FULLNAME = "FULLNAME";
    public static final String KEY_COMPANY = "COMPANY";
    public static final String KEY_POSITION = "POSITION";
    public static final String KEY_IMAGES_MASTER = "IMAGES_MASTER";
    public static final String FOTO = "FOTO";
    public static final String FUNCTION_ID = "FUNCTION_ID";
    public static final int PENDINGINTENT_LOCATION = 99999;
    public static boolean service_running = false;
    public static String today_date;


    /**
     * Singleton instance
     */
    private static PreferenceUtility instance = null;

    /**
     * Private constructor to avoid instantiation outside class
     */
    protected PreferenceUtility() {
    }

    /**
     * Get the singleton instance
     *
     * @return the PreferenceUtility instance
     */
    public static PreferenceUtility getInstance() {
        if (instance == null) {
            return new PreferenceUtility();
        }
        return instance;
    }

    /**
     * Set the singleton instance
     *
     * @param instance the PreferenceUtility instance
     */
    public static synchronized void setInstance(PreferenceUtility instance) {
        PreferenceUtility.instance = instance;
    }

    /**
     * private constructor, not to be instantiated with new keyword
     *
     * @param context application context
     * @return SharedPreference object of current application
     */
    public SharedPreferences sharedPreferences(Context context) {
        return context.getSharedPreferences(SP_COMMON, Context.MODE_PRIVATE);
    }

    /**
     * Save data base on key given, and the value is on integer format
     *
     * @param context
     * @param key
     * @param value
     */
    public void saveData(Context context, String key, int value) {
        saveData(context, key, String.valueOf(value));
    }

    /**
     * Save data base on key given, and the value is on string format
     *
     * @param context
     * @param key
     * @param value
     */
    public void saveData(Context context, String key, String value) {
        sharedPreferences(context).edit().putString(key, value).commit();
    }

    public void saveData(Context context, String key, Set<String> value) {
        sharedPreferences(context).edit().putStringSet(key, value).commit();
    }

    /**
     * Save data base on key given, and the value is on string format
     *
     * @param context
     * @param key
     * @param value
     */
    public void saveData(Context context, String key, Boolean value) {
        sharedPreferences(context).edit().putBoolean(key, value).commit();
    }

    /**
     * Save data base on key given, and the value is on long format
     *
     * @param context
     * @param key
     * @param value
     */
    public void saveData(Context context, String key, long value) {
        sharedPreferences(context).edit().putLong(key, value).commit();
    }

    /**
     * get data base on key given, and the value is on integer format
     *
     * @param context
     * @param key
     * @return
     */
    public int loadDataInt(Context context, String key) {
        return Integer.parseInt(sharedPreferences(context).getString(key, "0"));
    }

    /**
     * get data base on key given, and the value is on String format
     *
     * @param context
     * @param key
     * @return
     */
    public String loadDataString(Context context, String key) {
        return sharedPreferences(context).getString(key, "");
    }

    public Set<String> loadDataStringSet(Context context, String key) {
        Set<String> stringSet = new HashSet<>();
        return sharedPreferences(context).getStringSet(key, stringSet);
    }

    /**
     * get data base on key given, and the value is on String format
     *
     * @param context
     * @param key
     * @return
     */
    public Boolean loadDataBoolean(Context context, String key) {
        return loadDataBoolean(context, key, true);
    }

    /**
     * get data base on key given, and the value is on String format
     *
     * @param context
     * @param key
     * @return
     */
    public Boolean loadDataBoolean(Context context, String key, boolean defaultValue) {
        return sharedPreferences(context).getBoolean(key, defaultValue);
    }

    /**
     * get data base on key given, and the value is on long format
     *
     * @param context
     * @param key
     * @return
     */
    public long loadDataOfLong(Context context, String key) {
        return sharedPreferences(context).getLong(key, 0);
    }

    /**
     * Save data base on key given, and the value is on string format
     *
     * @param context
     * @param key
     */
    public void delete(Context context, String key) {
        sharedPreferences(context).edit().remove(key).commit();
    }

    /**
     * Delete data base for all existing key, and based on context
     *
     * @param context
     */
    public void deleteAll(Context context) {
        sharedPreferences(context).edit().clear().commit();
    }




}
