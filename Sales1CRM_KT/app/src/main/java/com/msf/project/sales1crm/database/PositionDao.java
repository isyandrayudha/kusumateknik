package com.msf.project.sales1crm.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.msf.project.sales1crm.model.PositionModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by christianmacbook on 24/05/18.
 */

public class PositionDao extends BaseDao<PositionModel> implements DBSchema.Position {

    public PositionDao(Context c) {
        super(c, TABLE_NAME);
    }

    public PositionDao(Context c, boolean willWrite) {
        super(c, TABLE_NAME, willWrite);
    }

    public PositionDao(DBHelper db) {
        super(db, TABLE_NAME);
    }

    public PositionDao(DBHelper db, boolean willWrite) {
        super(db, TABLE_NAME, willWrite);
    }

    public PositionModel getById(int id) {
        String qry = "SELECT * FROM " + getTable() + " WHERE " + COL_ID + " = " + id;
        Cursor c = getSqliteDb().rawQuery(qry, null);
        PositionModel model = new PositionModel();
        try {
            if(c != null && c.moveToFirst()) {
                model = getByCursor(c);
            }
        } finally {
            c.close();
        }
        return model;
    }

    public List<PositionModel> getPositionList() {
        String query = "SELECT * FROM " + getTable();

        Cursor c = getSqliteDb().rawQuery(query, null);
        List<PositionModel> list = new ArrayList<>();
        try {
            if(c != null && c.moveToFirst()) {
                list.add(getByCursor(c));
                while (c.moveToNext()) {
                    list.add(getByCursor(c));

                }
            }
        } finally {
            c.close();
        }
        return list;
    }

    @Override
    public PositionModel getByCursor(Cursor c) {
        PositionModel model = new PositionModel();
        model.setId(c.getString(0));
        model.setName(c.getString(1));
        return model;
    }

    @Override
    protected ContentValues upDataValues(PositionModel model, boolean update) {
        ContentValues cv = new ContentValues();
        if (update == true)
            cv.put(COL_ID, model.getId());
        cv.put(KEY_NAME, model.getName());
        return cv;
    }
}
