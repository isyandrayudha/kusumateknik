package com.msf.project.sales1crm;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.msf.project.sales1crm.callback.UserDialogCallback;
import com.msf.project.sales1crm.customview.CustomDialog;
import com.msf.project.sales1crm.customview.CustomEditText;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.customview.SpinnerCustomAdapter;
import com.msf.project.sales1crm.database.AccountTypeDao;
import com.msf.project.sales1crm.database.IndustryDao;
import com.msf.project.sales1crm.fragment.UserDialogFragment;
import com.msf.project.sales1crm.imageloader.ImageCompression;
import com.msf.project.sales1crm.model.AccountModel;
import com.msf.project.sales1crm.model.AccountTypeModel;
import com.msf.project.sales1crm.model.IndustryModel;
import com.msf.project.sales1crm.utility.PreferenceUtility;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by christianmacbook on 23/05/18.
 */

public class AccountAddActivity extends BaseActivity {

    @BindView(R.id.ivNext)
    ImageView ivNext;

    @BindView(R.id.ivBack)
    ImageView ivBack;

    @BindView(R.id.tvTitle)
    CustomTextView tvTitle;

    @BindView(R.id.ivAccountPicture)
    ImageView ivAccountPicture;

    @BindView(R.id.tvAssignTo)
    CustomEditText tvAssignTo;

    @BindView(R.id.spIndustry)
    Spinner spIndustry;

    @BindView(R.id.spAccountType)
    Spinner spAccountType;

//    @BindView(R.id.tilAccountName)
//    TextInputLayout tilAccountName;

    @BindView(R.id.etAccountName)
    CustomEditText etAccountName;

    @BindView(R.id.etEmployee)
    CustomEditText etEmployee;

    @BindView(R.id.etDesc)
    CustomEditText etDesc;

    @BindView(R.id.etPhone)
    CustomEditText etPhone;

    @BindView(R.id.etEmail)
    CustomEditText etEmail;

    @BindView(R.id.etWebsite)
    CustomEditText etWebsite;
    @BindView(R.id.tabGeneral)
    LinearLayout tabGeneral;
    @BindView(R.id.tabAddress)
    CustomTextView tabAddress;

    private Context context;
    private IndustryModel industryModel;
    private AccountTypeModel accountTypeModel;
    private AccountModel model;
    private List<IndustryModel> industryList = new ArrayList<>();
    private List<AccountTypeModel> accountTypeList = new ArrayList<>();

    private SpinnerCustomAdapter spIndustryAdapter, spAccountTypeAdapter;
    private ArrayList<String> arrIndustry = new ArrayList<>();
    private ArrayList<String> arrAccountType = new ArrayList<>();
    private int indexIndustry = 0, indexAccountType = 0;

    private boolean from_detail = false;

    //Image Utils
    private String photoPath = "", filePath = "", myBase64Image = "", url = "", assign_id = "0";
    private Bitmap savedBitmap;
    private String kecamatan, kabupaten, provinsi, kelurahan, alamat,jalan,no,latitude,longitude, postalCode;

    static AccountAddActivity accountAddActivity;

    public static AccountAddActivity getInstance() {
        return accountAddActivity;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Sales1CRMUtils.GALLERY:
                if (resultCode == RESULT_OK) {
                    Uri selectedImageUri = data.getData();
                    BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
                    bmpFactoryOptions.inJustDecodeBounds = false;
                    bmpFactoryOptions.inSampleSize = 4;

                    // imageFilePath image path which you pass with intent
                    Bitmap bmp = null;
                    try {
                        bmp = Sales1CRMUtils.Utils.scaleImage(context, selectedImageUri);
                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    savedBitmap = bmp.copy(bmp.getConfig(), bmp.isMutable() ? true
                            : false);
                    ivAccountPicture.setImageBitmap(Sales1CRMUtils.Utils.getroundBitmap(bmp));

                    myBase64Image = Sales1CRMUtils.ImageUtility.encodeToBase64(bmp, Bitmap.CompressFormat.JPEG, 50);

                    bmp.recycle();
                    bmp = null;
                }
                break;
            case Sales1CRMUtils.CAMERA_REQUEST:
                if (resultCode == RESULT_OK) {
                    BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
                    bmpFactoryOptions.inJustDecodeBounds = false;
                    bmpFactoryOptions.inSampleSize = 4;

                    Log.d("TAG", "Photo Path : " + photoPath);

                    // imageFilePath image path which you pass withintent
                    Bitmap bmp = BitmapFactory.decodeFile(photoPath,
                            bmpFactoryOptions);
                    // BitmapFactory.Options options = new BitmapFactory.Options();
                    // options.inSampleSize = 4;
                    Log.i("HUAI", "data : " + bmp);
                    // Bitmap photo = (Bitmap) data.getExtras().get("data");
                    if (bmp.getWidth() >= bmp.getHeight()) {

                        bmp = Bitmap.createBitmap(bmp,
                                bmp.getWidth() / 2 - bmp.getHeight() / 2, 0,
                                bmp.getHeight(), bmp.getHeight(), null, true);

                    } else {

                        bmp = Bitmap.createBitmap(bmp, 0,
                                bmp.getHeight() / 2 - bmp.getWidth() / 2,
                                bmp.getWidth(), bmp.getWidth(), null, true);
                    }
                    bmp = Sales1CRMUtils.ImageUtility.scaleDownBitmap(bmp);
                    ExifInterface ei;
                    int orientation = 0;
                    try {
                        ei = new ExifInterface(photoPath);
                        orientation = ei.getAttributeInt(
                                ExifInterface.TAG_ORIENTATION,
                                ExifInterface.ORIENTATION_NORMAL);

                    } catch (IOException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                    switch (orientation) {
                        case ExifInterface.ORIENTATION_ROTATE_90:
                            bmp = Sales1CRMUtils.ImageUtility.RotateBitmap(bmp, 90);
                            break;
                        case ExifInterface.ORIENTATION_ROTATE_180:
                            bmp = Sales1CRMUtils.ImageUtility.RotateBitmap(bmp, 180);
                            break;
                        // etc.
                    }
                    savedBitmap = bmp.copy(bmp.getConfig(), bmp.isMutable() ? true
                            : false);
                    ivAccountPicture.setImageBitmap(Sales1CRMUtils.Utils.getrectBitmap(savedBitmap));
                    bmp.recycle();
                    bmp = null;

                    onCaptureImageResult(data);
                }
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case Sales1CRMUtils.KEY_CAMERA_PERMISION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    cameraIntent();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(context, "Doesn't have permission... ", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accountadd);

        ButterKnife.bind(this);
        this.context = this;

        accountAddActivity = this;

        this.url = PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.URL);

        initView();
    }

    private void initView() {
        try {
            from_detail = getIntent().getExtras().getBoolean("from_detail");
        } catch (Exception e) {
        }

        try {
            model = getIntent().getExtras().getParcelable("accountModel");
        } catch (Exception e) {
        }

        tvAssignTo.setText(PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.KEY_FULLNAME));
        assign_id = PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.KEY_USERID);

        getDataSpinner();

        getDataFromDetail();

        getDataIntent();

        ivAccountPicture.setOnClickListener(click);
        tvAssignTo.setOnClickListener(click);
        ivNext.setOnClickListener(click);
        ivBack.setOnClickListener(click);
//        tabAddress.setOnClickListener(click);
//        tabGeneral.setOnClickListener(click);
    }

    private void getDataSpinner() {
        // industry
        industryList = new IndustryDao(context).getIndustryList();

        if (from_detail && !model.getIndustry_id().equalsIgnoreCase("0")) {
            for (int i = 0; i < industryList.size(); i++) {
                if (i == 0) {
                    arrIndustry.add("Choose Industry");
                }

                if (model.getIndustry_id().equalsIgnoreCase(industryList.get(i).getId()))
                    indexIndustry = i + 1;
                arrIndustry.add(industryList.get(i).getName());
            }
        } else {
            for (int i = 0; i < industryList.size(); i++) {
                if (i == 0) {
                    arrIndustry.add("Choose Industry");
                }
                arrIndustry.add(industryList.get(i).getName());
            }
        }
        String[] industry = new String[arrIndustry.size()];
        industry = arrIndustry.toArray(industry);


        spIndustryAdapter = new SpinnerCustomAdapter(context,
                R.layout.spinner_default, industry);
        spIndustryAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spIndustry.setAdapter(spIndustryAdapter);
        spIndustry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                if (position != 0) {
                    indexIndustry = position;
                    industryModel = industryList.get(position - 1);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        accountTypeList = new AccountTypeDao(context).getAccountType();

        if (from_detail && !model.getType_id().equalsIgnoreCase("0")) {
            for (int i = 0; i < accountTypeList.size(); i++) {
                if (i == 0) {
                    arrAccountType.add("Choose account Type");
                }

                if (model.getType_id().equalsIgnoreCase(accountTypeList.get(i).getId()))
                    indexAccountType = i + 1;
                arrAccountType.add(accountTypeList.get(i).getName());
            }
        } else {
            for (int i = 0; i < accountTypeList.size(); i++) {
                if (i == 0) {
                    arrAccountType.add("Choose account Type");
                }
                arrAccountType.add(accountTypeList.get(i).getName());
            }
        }
        String[] accountType = new String[arrAccountType.size()];
        accountType = arrAccountType.toArray(accountType);


        spAccountTypeAdapter = new SpinnerCustomAdapter(context,
                R.layout.spinner_default, accountType);
        spAccountTypeAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spAccountType.setAdapter(spAccountTypeAdapter);
        spAccountType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                if (position != 0) {
                    indexAccountType = position;
                    accountTypeModel = accountTypeList.get(position - 1);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void getDataIntent() {
        Intent data = getIntent();
        if (data != null) {
            alamat = data.getStringExtra("alamat");
            jalan = data.getStringExtra("jalan");
            no = data.getStringExtra("no");
            provinsi = data.getStringExtra("provinsi");
            kabupaten = data.getStringExtra("kabupaten");
            kecamatan = data.getStringExtra("kecamatan");
            kelurahan = data.getStringExtra("kelurahan");
            postalCode = data.getStringExtra("postalCode");
            latitude = data.getStringExtra("latitude");
            longitude = data.getStringExtra("longitude");

//            Toast.makeText(context, latitude, Toast.LENGTH_SHORT).show();
        }
    }

    private void getDataFromDetail() {
        if (from_detail) {
            tvTitle.setText("Edit Account");

            Sales1CRMUtils.Utils.imageLoader.DisplayImage(url + model.getImageUrl(),
                    ivAccountPicture, Bitmap.Config.ARGB_8888, 150, Sales1CRMUtils.TYPE_ROUND);

            Log.d("TAG", "Index Industry : " + indexIndustry);
            Log.d("TAG", "Index Type : " + indexAccountType);

            assign_id = model.getAssign_to();

            spIndustry.setSelection(indexIndustry);
            spAccountType.setSelection(indexAccountType);

            tvAssignTo.setText(model.getAssign_to_name());
            etAccountName.setText(model.getAccount_name());
            etDesc.setText(model.getDescription());
            etEmployee.setText(model.getTotal_employee());
            etEmail.setText(model.getEmail());
            etPhone.setText(model.getPhone());
            etWebsite.setText(model.getWebsite());
        }
    }

    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.ivAccountPicture:
                    if(from_detail){
                            selectImage();
                    } else {
                            selectImage();
                    }

                    break;
                case R.id.tvAssignTo:
                    UserDialogFragment DialogFragment = new UserDialogFragment(AccountAddActivity.this, new UserDialogCallback() {

                        @Override
                        public void onUserDialogCallback(Bundle data) {
                            tvAssignTo.setText(data.getString("name"));
                            assign_id = data.getString("id");
                        }
                    });
                    DialogFragment.show(getSupportFragmentManager(), "accountDialog");
                    break;
                case R.id.ivNext:
                    if (checkField()) {
                        if (!from_detail) {
                            //Input Into Model
                            model = new AccountModel();
                            model.setAssign_to(assign_id);
                            model.setAccount_name(etAccountName.getText().toString());
                            model.setBase64Image(myBase64Image);
                            model.setTotal_employee(etEmployee.getText().toString());
                            model.setDescription(etDesc.getText().toString());
                            model.setPhone(etPhone.getText().toString());
                            model.setEmail(etEmail.getText().toString());
                            model.setWebsite(etWebsite.getText().toString());
                            model.setCreated_date(getCurrentDate());
                            model.setBase64Image(myBase64Image);

                            if (indexIndustry != 0) {
                                model.setIndustry_id(industryModel.getId());
                                model.setIndustry_name(industryModel.getName());
                            }

                            if (indexAccountType != 0) {
                                model.setType_id(accountTypeModel.getId());
                                model.setType_name(accountTypeModel.getName());
                            }

                            //Enable Error
//                            tilAccountName.setError(null);
                            etAccountName.setError(null);


                            //Start Activity
                            Intent intent = new Intent(context, AccountAddAddressActivity.class);
                            intent.putExtra("accountModel", model);
                            intent.putExtra("alamat", alamat);
                            intent.putExtra("no",no);
                            intent.putExtra("jalan",jalan);
                            intent.putExtra("provinsi", provinsi);
                            intent.putExtra("kabupaten", kabupaten);
                            intent.putExtra("kecamatan", kecamatan);
                            intent.putExtra("kelurahan", kelurahan);
                            intent.putExtra("postalCode", postalCode);
                            intent.putExtra("latitude",latitude);
                            intent.putExtra("longitude",longitude);
                            startActivity(intent);
                        } else {
                            //Input Into Model
                            model.setAccount_name(etAccountName.getText().toString());
                            model.setAssign_to(assign_id);
                            model.setBase64Image(myBase64Image);
                            model.setTotal_employee(etEmployee.getText().toString());
                            model.setDescription(etDesc.getText().toString());
                            model.setPhone(etPhone.getText().toString());
                            model.setEmail(etEmail.getText().toString());
                            model.setWebsite(etWebsite.getText().toString());
                            model.setCreated_date(getCurrentDate());
                            model.setBase64Image(myBase64Image);

                            if (indexIndustry != 0) {
                                model.setIndustry_id(industryModel.getId());
                                model.setIndustry_name(industryModel.getName());
                            }

                            if (indexAccountType != 0) {
                                model.setType_id(accountTypeModel.getId());
                                model.setType_name(accountTypeModel.getName());
                            }

                            //Enable Error
//                            tilAccountName.setError(null);
                            etAccountName.setError(null);

                            //Start Activity
                            Intent intent = new Intent(context, AccountAddAddressActivity.class);
                            intent.putExtra("accountModel", model);
                            intent.putExtra("from_detail", from_detail);
                            intent.putExtra("alamat", alamat);
                            intent.putExtra("no",no);
                            intent.putExtra("jalan",jalan);
                            intent.putExtra("provinsi", provinsi);
                            intent.putExtra("kabupaten", kabupaten);
                            intent.putExtra("kecamatan", kecamatan);
                            intent.putExtra("kelurahan", kelurahan);
                            intent.putExtra("postalCode", postalCode);
                            intent.putExtra("latitude",latitude);
                            intent.putExtra("longitude",longitude);
                            startActivity(intent);
                        }
                    }
                    break;
                case R.id.ivBack:
                    final CustomDialog dialogback = CustomDialog.setupDialogConfirmation(context, "Anda ingin kembali? Data yang anda masukkan akan hilang?");
                    CustomTextView tvCancel = (CustomTextView) dialogback.findViewById(R.id.tvCancel);
                    CustomTextView tvYa = (CustomTextView) dialogback.findViewById(R.id.tvOK);

                    tvCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialogback.dismiss();
                        }
                    });

                    tvYa.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
//                            finish();
//                            dialogback.dismiss();
                            onBackPressed();
                        }
                    });
                    dialogback.show();
                    break;

//                case R.id.tabGeneral:
//                    Intent intent = new Intent(AccountAddActivity.this,AccountAddActivity.class);
//                    startActivity(intent);
//                    break;
//                case R.id.tabAddress:
//                    Intent intentt = new Intent(AccountAddActivity.this,AccountAddAddressActivity.class);
//                    startActivity(intentt);
//                    break;
            }
        }
    };


    private void selectImage() {
        final CharSequence[] items = {"Take Picture", "Galery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Add Picture!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals("Take Picture")) {

                    if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.CAMERA}, Sales1CRMUtils.KEY_CAMERA_PERMISION);
                    } else {
                        cameraIntent();
                    }
                } else if (items[item].equals("Galery")) {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(
                            Intent.createChooser(intent, "Select Picture"),
                            Sales1CRMUtils.GALLERY);
                    dialog.dismiss();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void cameraIntent() {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        Intent intentCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File photoFile = null;
        try {
            photoFile = createImageFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (photoFile != null) {
            Uri photoURI = Uri.fromFile(photoFile);
            intentCamera.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
            startActivityForResult(intentCamera, Sales1CRMUtils.CAMERA_REQUEST);
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            String Name = new SimpleDateFormat("yyyyMMdd_HHmm").format(new Date());
            String imageFileName = Name + ".jpg";
            File storageDir = new File(Environment.getExternalStorageDirectory().getPath(), imageFileName);
            photoPath = storageDir.getPath();
            return storageDir;
        } else {
            String imageFileName = new SimpleDateFormat("yyyyMMdd_HHmm").format(new Date());
            File storageDir = context.getExternalFilesDir("Pictures");
            File image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );
            Log.d("TAG", image.getPath());
            photoPath = image.getPath();
            return image;
        }
    }

    private void onCaptureImageResult(Intent data) {
        //Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        Log.d("TAG", "Masuk OnCaptureImage");
        try {
            ImageCompression imageCompression = new ImageCompression(context);
            filePath = imageCompression.compressImage(photoPath);
            Log.d("PPP", "Path : " + filePath);
            Bitmap thumbnail = Sales1CRMUtils.ImageUtility.getThumbnail(filePath);

            myBase64Image = Sales1CRMUtils.ImageUtility.encodeToBase64(thumbnail, Bitmap.CompressFormat.JPEG, 50);

            File imgFile = new File(photoPath);
            imgFile.delete();
            photoPath = "";
        } catch (IOException e) {
            e.printStackTrace();
            Log.d("TAG", e.getMessage());
        }
    }

    private boolean checkField() {
        if (etAccountName.getText().toString().equals("")) {
//            tilAccountName.setError("account name is required");
            etAccountName.setError("Account name is required !!!");
            return false;
        }
        return true;
    }

    private String getCurrentDate() {
        Calendar c = Calendar.getInstance();

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        return df.format(c.getTime());
    }
}
