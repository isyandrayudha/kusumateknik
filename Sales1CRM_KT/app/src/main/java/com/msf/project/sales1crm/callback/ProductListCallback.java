package com.msf.project.sales1crm.callback;

import com.msf.project.sales1crm.model.ProductModel;

import java.util.List;

public interface ProductListCallback {
    void finishProductList(String result, List<ProductModel> list);
    void finishProductMoreList(String result, List<ProductModel> list);
}
