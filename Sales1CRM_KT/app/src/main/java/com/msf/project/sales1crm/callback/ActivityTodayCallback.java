package com.msf.project.sales1crm.callback;

import com.msf.project.sales1crm.model.ActivityCallsModel;
import com.msf.project.sales1crm.model.ActivityEventModel;
import com.msf.project.sales1crm.model.ActivityTodoModel;

import java.util.List;

public interface ActivityTodayCallback {
    void finishAEventToday(String result, List<ActivityEventModel> aEventModels);
    void finishAEventMoreToday(String result, List<ActivityEventModel> aEventModels);
    void finishATodoToday(String result, List<ActivityTodoModel> aTodoModels);
    void finishATodoMoreToday(String result, List<ActivityTodoModel> aTodoModels);
    void finishACallsToday(String result, List<ActivityCallsModel> aCallsModels);
    void finishACallsMoreToday(String result, List<ActivityCallsModel> aCallsModels);
}
