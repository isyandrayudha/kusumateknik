package com.msf.project.sales1crm.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.database.DBSchema;
import com.msf.project.sales1crm.database.MeeToSelectDao;
import com.msf.project.sales1crm.model.ContactModel;

import java.util.ArrayList;
import java.util.List;


public class ContactListAdapter extends BaseAdapter {
    private Context context;
    private LayoutInflater layoutInflater;
    private List<ContactModel> listData = new ArrayList<>();


    public ContactListAdapter(Context context, List<ContactModel> objects) {
        this.context = context;
        this.listData = objects;
    }

    public static abstract class Row {
    }

    public static final class Section extends Row {
        public final String text;

        public Section(String text) {
            this.text = text;
        }
    }

    public static final class Item extends Row {
        public final ContactModel text;

        public Item(ContactModel text) {
            this.text = text;
        }

        public ContactModel getItem() {
            return text;
        }
    }

    private List<Row> rows;

    public void setRows(List<Row> rows) {
        this.rows = rows;
        notifyDataSetChanged();
    }


    @Override
    public int getCount() {
        return rows.size();
    }

    @Override
    public Object getItem(int position) {
        return rows.get(position);
    }

    @Override
    public long getItemId(int position) {
        return rows.indexOf(getItem(position));
    }


    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        if (getItem(position) instanceof Section) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        final ViewHolder holder;
        final Item item = (Item) getItem(position);
        if (convertView == null) {

            layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(
                    R.layout.dialog_meet_to, parent, false);
            holder = new ViewHolder();
            holder.tvContactName = (CustomTextView) view.findViewById(R.id.tvContactName);
            holder.deleteMeeToSelect = (ImageView) view.findViewById(R.id.deleteMeeToSelect);
            holder.llcontact = (LinearLayout) view.findViewById(R.id.llcontact);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();

        }

            holder.tvContactName.setText(item.text.getFullname());
            holder.deleteMeeToSelect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(context);
                    alert.setTitle("Alert !!!");
                    alert.setMessage("Are you sure delete this Meet to?");
                    alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            MeeToSelectDao mse = new MeeToSelectDao(context);
                            mse.delete(Integer.valueOf(item.text.getId()), DBSchema.MeetTo.KEY_MEET_TO_ID);
                            ((Activity) context).setResult(Activity.RESULT_OK);
                            notifyDataSetChanged();
                            holder.llcontact.setVisibility(View.GONE);
                            dialog.dismiss();

                        }
                    });

                    alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    alert.show();
                }
            });

        return view;
    }


    private static class ViewHolder {
        CustomTextView tvContactName;
        ImageView deleteMeeToSelect;
        LinearLayout llcontact;
    }

}
