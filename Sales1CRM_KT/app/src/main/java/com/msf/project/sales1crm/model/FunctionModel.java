package com.msf.project.sales1crm.model;

public class FunctionModel{
	private String functionCodeName;
	private String functionId;

	public void setFunctionCodeName(String functionCodeName){
		this.functionCodeName = functionCodeName;
	}

	public String getFunctionCodeName(){
		return functionCodeName;
	}

	public void setFunctionId(String functionId){
		this.functionId = functionId;
	}

	public String getFunctionId() {
		return functionId;
	}

	@Override
 	public String toString(){
		return 
			"FunctionModel{" + 
			"function_code_name = '" + functionCodeName + '\'' + 
			",function_id = '" + functionId + '\'' + 
			"}";
		}
}
