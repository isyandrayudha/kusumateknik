package com.msf.project.sales1crm.callback;

import com.msf.project.sales1crm.model.OpportunityModel;
import com.msf.project.sales1crm.model.TotalNPriceOpporModel;

import java.util.List;

public interface NoneCallback {
    void finishNone(String result, List<OpportunityModel> list);
    void finishMoreNone(String result, List<OpportunityModel> list);
    void totNone(String result, TotalNPriceOpporModel totalNPriceOpporModel);
}
