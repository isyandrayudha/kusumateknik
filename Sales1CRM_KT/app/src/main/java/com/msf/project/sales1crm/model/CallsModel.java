package com.msf.project.sales1crm.model;

import android.annotation.SuppressLint;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

@SuppressLint("ParcelCreator")
public class CallsModel implements Comparable, Parcelable {
    private String id;
    private String subject;
    private String detail;
    private String account_id;
    private String account_name;
    private String call_type_id;
    private String call_type_name;
    private String call_purpose_id;
    private String call_purpose_name;
    private String duration;
    private String duration_label;
    private String duration_interval;
    private String date;
    private String date_interval;
    private String created_at;
    private String updated_at;

    public CallsModel() {
        setId("");
        setSubject("");
        setDetail("");
        setAccount_id("");
        setAccount_name("");
        setCall_type_id("");
        setCall_type_name("");
        setCall_purpose_id("");
        setCall_purpose_name("");
        setDuration("");
        setDuration_label("");
        setDuration_interval("");
        setDate("");
        setDate_interval("");
        setCreated_at("");
        setUpdated_at("");
    }

    public CallsModel(Parcel in) {
        readFromParcel(in);
    }

    public void readFromParcel(Parcel in) {
        id = in.readString();
        subject = in.readString();
        detail = in.readString();
        account_id = in.readString();
        account_name = in.readString();
        call_type_id = in.readString();
        call_type_name = in.readString();
        call_purpose_id = in.readString();
        call_purpose_name = in.readString();
        duration = in.readString();
        duration_label = in.readString();
        duration_interval = in.readString();
        date = in.readString();
        date_interval = in.readString();
        created_at = in.readString();
        updated_at = in.readString();
    }

    public static final Creator<CallsModel> CREATOR = new Creator<CallsModel>() {
        @Override
        public CallsModel createFromParcel(Parcel in) {
            return new CallsModel(in);
        }

        @Override
        public CallsModel[] newArray(int size) {
            return new CallsModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getAccount_id() {
        return account_id;
    }

    public void setAccount_id(String account_id) {
        this.account_id = account_id;
    }

    public String getAccount_name() {
        return account_name;
    }

    public void setAccount_name(String account_name) {
        this.account_name = account_name;
    }

    public String getCall_type_id() {
        return call_type_id;
    }

    public void setCall_type_id(String call_type_id) {
        this.call_type_id = call_type_id;
    }

    public String getCall_type_name() {
        return call_type_name;
    }

    public void setCall_type_name(String call_type_name) {
        this.call_type_name = call_type_name;
    }

    public String getCall_purpose_id() {
        return call_purpose_id;
    }

    public void setCall_purpose_id(String call_purpose_id) {
        this.call_purpose_id = call_purpose_id;
    }

    public String getCall_purpose_name() {
        return call_purpose_name;
    }

    public void setCall_purpose_name(String call_purpose_name) {
        this.call_purpose_name = call_purpose_name;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getDuration_label() {
        return duration_label;
    }

    public void setDuration_label(String duration_label) {
        this.duration_label = duration_label;
    }

    public String getDuration_interval() {
        return duration_interval;
    }

    public void setDuration_interval(String duration_interval) {
        this.duration_interval = duration_interval;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDate_interval() {
        return date_interval;
    }

    public void setDate_interval(String date_interval) {
        this.date_interval = date_interval;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(subject);
        dest.writeString(detail);
        dest.writeString(account_id);
        dest.writeString(account_name);
        dest.writeString(call_type_id);
        dest.writeString(call_type_name);
        dest.writeString(call_purpose_id);
        dest.writeString(call_purpose_name);
        dest.writeString(duration);
        dest.writeString(duration_label);
        dest.writeString(duration_interval);
        dest.writeString(date);
        dest.writeString(date_interval);
        dest.writeString(created_at);
        dest.writeString(updated_at);
    }

    @Override
    public int compareTo(@NonNull Object o) {
        return 0;
    }
}
