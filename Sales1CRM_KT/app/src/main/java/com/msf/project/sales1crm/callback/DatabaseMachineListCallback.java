package com.msf.project.sales1crm.callback;

import com.msf.project.sales1crm.model.DatabaseMachineModel;

import java.util.List;

public interface DatabaseMachineListCallback {
    void finishDBmachine(String result, List<DatabaseMachineModel> list);
    void finishDBmachineMore(String result, List<DatabaseMachineModel> list);
}

