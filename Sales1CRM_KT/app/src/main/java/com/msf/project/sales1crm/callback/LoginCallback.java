package com.msf.project.sales1crm.callback;

/**
 * Created by christianmacbook on 11/05/18.
 */

public interface LoginCallback {
    void finishedLogin(String result, String response);
}
