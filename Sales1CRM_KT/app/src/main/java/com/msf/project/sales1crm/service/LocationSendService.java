package com.msf.project.sales1crm.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.IBinder;
import android.util.Log;

import com.msf.project.sales1crm.utility.PreferenceUtility;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class LocationSendService extends Service {

    public static String TAG = LocationSendService.class.getSimpleName();

    private  String stringLatitude, stringLongitude, date;
    private int year, month, day, hour, minute, second;
    private String apiKey;
    private Context context;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(intent != null) {
            if(intent.getExtras().containsKey("send_Location")){
                if(intent.getExtras().getBoolean("send_location")){
                    Log.i("AAA","SERVICE JALAN");
                    GPSTracker gpsTracker = new GPSTracker(this);
                    AlarmReceive alarmReceive = new AlarmReceive();
                    alarmReceive.stopSendTaskData(context);
                    alarmReceive.SetAlarmForGeoService(context);
                    if(gpsTracker.canGetLocation()) {
                        stringLatitude = String.valueOf(gpsTracker.location.getLongitude());

                        stringLongitude = String.valueOf(gpsTracker.location.getLatitude());

                        Geocoder geocoder = new Geocoder(this);
                        List<Address> addresses = new ArrayList<>();
                        try{
                            addresses = geocoder.getFromLocation(Double.parseDouble(stringLatitude), Double.parseDouble(stringLongitude),1);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        StringBuilder stringBuilder = new StringBuilder();
                        if(geocoder.isPresent()) {
                            if(addresses.size() > 0) {
                                Address returnAddress = addresses.get(0);
                                stringBuilder.append(returnAddress.getAddressLine(0) + ", " + returnAddress.getLocality() + " " + returnAddress.getAdminArea() +
                                        ", " + returnAddress.getCountryName() + " " + returnAddress.getPostalCode());
                            } else {
                                stringBuilder.append("-");
                            }
                        }
                        Log.d("Address Line : ", stringBuilder.toString());
                        Log.i(TAG, "longitude : " + stringLongitude
                                + ", latituded : " + stringLatitude
                                + ", date : " + date
                                + ", address : " + stringBuilder.toString());
                    }

                }
            }
        }




        Log.i("AAA" , "Service SENDLOCATION");
        Log.i("Date", date);

        return START_STICKY;
    }


    private String getCurrentDate() {
//        final Calendar c = Calendar.getInstance();
//        year = c.get(Calendar.YEAR);
//        month = c.get(Calendar.MONTH) + 1;
//        day = c.get(Calendar.DAY_OF_MONTH);
//        hour = c.get(Calendar.HOUR_OF_DAY);
//        minute = c.get(Calendar.MINUTE);
//        second = c.get(Calendar.SECOND);
//        date = year + "-" + month + "-" + day + " " + hour
//                + ":" + minute + ":" + second;

        Calendar c = Calendar.getInstance();

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        date = df.format(c.getTime());
        return date;
    }

    private String getGpsTracking(){
        GPSTracker gpsTracker = new GPSTracker(this);
        if(gpsTracker.canGetLocation()) {
            stringLatitude = String.valueOf(gpsTracker.latitude);

            stringLongitude = String.valueOf(gpsTracker.longitude);

            Geocoder geocoder = new Geocoder(this);
            List<Address> addresses = new ArrayList<>();
            try{
                addresses = geocoder.getFromLocation(Double.parseDouble(stringLatitude), Double.parseDouble(stringLongitude),1);
            } catch (IOException e) {
                e.printStackTrace();
            }
            StringBuilder stringBuilder = new StringBuilder();
            if(geocoder.isPresent()) {
                if(addresses.size() > 0) {
                    Address returnAddress = addresses.get(0);
                    stringBuilder.append(returnAddress.getAddressLine(0) + ", " + returnAddress.getLocality() + " " + returnAddress.getAdminArea() +
                            ", " + returnAddress.getCountryName() + " " + returnAddress.getPostalCode());
                } else {
                    stringBuilder.append("-");
                }
            }
            Log.d("Address Line : ", stringBuilder.toString());
            Log.i("AAA", "longitude : " + stringLongitude
                    + ", latituded : " + stringLatitude
                    + ", date : " + date
                    + ", address : " + stringBuilder.toString());
        }
        return null;
    }


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        Log.i("AAA", "oncreate service SENDLOCATION");
        getCurrentDate();
        getGpsTracking();

    }



    @Override
    public void onDestroy() {
        Log.i("AAA", "location service destroy");
        PreferenceUtility.service_running = false;
        super.onDestroy();

    }




}
