package com.msf.project.sales1crm.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.msf.project.sales1crm.model.Product;

import java.util.ArrayList;
import java.util.List;


public class ProductDao extends BaseDao<Product> implements DBSchema.Product {
    public ProductDao(Context c) {
        super(c, TABLE_NAME);
    }

    public ProductDao(Context c, String table, boolean willWrite) {
        super(c, table, willWrite);
    }

    public ProductDao(DBHelper dbHelper) {
        super(dbHelper);
    }

    public ProductDao(DBHelper dbHelper, String table) {
        super(dbHelper, table);
    }

    public ProductDao(DBHelper dbHelper, String table, boolean willWrite) {
        super(dbHelper, table, willWrite);
    }

    public ProductDao(DBHelper db, boolean willWrite) {
        super(db, TABLE_NAME, willWrite);
    }

    @Override
    public Product getById(int id) {
        return null;
    }

    public List<Product> getProductList() {
        String query = "SELECT * FROM " + getTable() + " WHERE " + DBSchema.Product.KEY_STATUC + " = '2'";
        Cursor c = getSqliteDb().rawQuery(query, null);
        List<Product> list = new ArrayList<>();
        try {
            if (c != null & c.moveToFirst()) {
                list.add(getByCursor(c));
                while (c.moveToNext()) {
                    list.add(getByCursor(c));
                }
            }
        } finally {
            c.close();
        }
        return list;
    }

    public Product getProduc(String status) {
        String qry = " SELECT * FROM " + getTable() + " WHERE " +  DBSchema.Product.KEY_STATUC + " = " + status;
        Cursor c = getSqliteDb().rawQuery(qry, null);
        Product product = new Product();
        try {
            if (c != null && c.moveToFirst()) {
                product = getByCursor(c);
            }
        } finally {
            c.close();
        }

        return product;
    }

    public List<Product> getProduct(){
        String qty = " SELECT * FROM " + getTable();
        Cursor c = getSqliteDb().rawQuery(qty,null);
        List<Product> list = new ArrayList<>();
        try{
            if(c!=null && c.moveToFirst()){
                list.add(getByCursor(c));
            } while (c.moveToNext()){
                list.add(getByCursor(c));
            }
        } finally {
            c.close();
        }
        return list;
    }

    public Product getProductById(String id){
        String query = " SELECT * FROM " + getTable() + " WHERE " + DBSchema.Product.KEY_ID_TEMP + " = " + id;
        Cursor c = getSqliteDb().rawQuery(query,null);
        Product product = new Product();
        try {
            if (c != null && c.moveToFirst()) {
                product = getByCursor(c);
            }
        } finally {
            c.close();
        }
        return product;
    }

    @Override
    public Product getByCursor(Cursor c) {
        Product model = new Product();
        model.setId_temp(c.getString(0));
        model.setId(c.getString(1));
        model.setBrand(c.getString(2));
        model.setType(c.getString(3));
        model.setSerialNumber(c.getString(4));
        model.setCondition(c.getString(5));
        model.setStatus(c.getString(6));
        return model;
    }


    @Override
    protected ContentValues upDataValues(Product product, boolean update) {
        ContentValues cv = new ContentValues();
        cv.put(KEY_ID_TEMP,product.getId_temp());
        cv.put(KEY_ID, product.getId());
        cv.put(KEY_BRAND, product.getBrand());
        cv.put(KEY_TYPE, product.getType());
        cv.put(KEY_SERIAL_NUMBER, product.getSerialNumber());
        cv.put(KEY_CONDITION, product.getCondition());
        cv.put(KEY_STATUC, product.getStatus());
        return cv;
    }
}
