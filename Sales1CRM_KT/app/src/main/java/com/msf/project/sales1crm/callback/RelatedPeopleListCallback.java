package com.msf.project.sales1crm.callback;

import com.msf.project.sales1crm.model.ContactModel;

import java.util.List;

public interface RelatedPeopleListCallback {
    void finishRelatedPeople(String result, List<ContactModel>list);
}
