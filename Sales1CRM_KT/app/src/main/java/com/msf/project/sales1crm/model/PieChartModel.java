package com.msf.project.sales1crm.model;

public class PieChartModel {
    private String total;
    private String name;

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
