package com.msf.project.sales1crm;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CallLog;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Spinner;

import com.msf.project.sales1crm.callback.AccountDialogCallback;
import com.msf.project.sales1crm.callback.CallsAddCallback;
import com.msf.project.sales1crm.customview.CustomDialog;
import com.msf.project.sales1crm.customview.CustomEditText;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.customview.DatePickerFragment;
import com.msf.project.sales1crm.customview.SpinnerCustomAdapter;
import com.msf.project.sales1crm.fragment.AccountDialogFragment;
import com.msf.project.sales1crm.model.AccountModel;
import com.msf.project.sales1crm.model.CallsModel;
import com.msf.project.sales1crm.presenter.CallsAddPresenter;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.ButterKnife;
import butterknife.BindView;

public class CallsAddActivity extends BaseActivity implements CallsAddCallback {

    @BindView(R.id.ivSave)
    ImageView ivSave;

    @BindView(R.id.ivBack)
    ImageView ivBack;

    @BindView(R.id.tvTitle)
    CustomTextView tvTitle;

    @BindView(R.id.ivAccountName)
    ImageView ivAccountName;

    @BindView(R.id.tvAccountName)
    CustomTextView tvAccountName;

    @BindView(R.id.etSubject)
    CustomEditText etSubject;

//    @BindView(R.id.tilSubject)
//    TextInputLayout tilSubject;

    @BindView(R.id.tvDate)
    CustomTextView tvDate;

    @BindView(R.id.etDuration)
    CustomEditText etDuration;

    @BindView(R.id.etDetail)
    CustomEditText etDetail;

//    @BindView(R.id.tilDetail)
//    TextInputLayout tilDetail;

    @BindView(R.id.spCallType)
    Spinner spCallType;

    @BindView(R.id.spCallPurpose)
    Spinner spCallPurpose;

    private Context context;
    private DatePickerFragment datepicker;
    private CallsAddPresenter presenter;
    private String url = "", contact_json, account_id = "0";
    private String open_date = "", string_year, string_month, string_day, index_type = "0", index_purpose = "0";
    private int year, month, day;
    private boolean from_detail = false, from_account = false;
    private String[] call_type = {"Call Type", "Current Call", "Completed Call", "Schedule Call"}, call_purpose = {"Purpose", "Prospecting", "Administrative", "Negotiation", "Demo", "Project", "Support"};
    private CallsModel model;
    private AccountModel accountModel;

    static CallsAddActivity callsAddActivity;

    public static CallsAddActivity getInstance() {
        return callsAddActivity;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_callsadd);

        ButterKnife.bind(this);
        this.context = this;

        callsAddActivity = this;

        this.presenter = new CallsAddPresenter(context, this);

        try {
            from_detail = getIntent().getExtras().getBoolean("from_detail");
        } catch (Exception e) {
        }

        try {
            model = getIntent().getExtras().getParcelable("callModel");
        } catch (Exception e) {
        }

        try {
            from_account = getIntent().getExtras().getBoolean("from_account");
            Log.d("TAG", "FROM ACCOUNT : " + from_account);
            accountModel = getIntent().getExtras().getParcelable("accountModel");
            Log.d("TAG", "ACCOUNT NAME : " + accountModel.getAccount_name());
        } catch (Exception e) {
        }

        datepicker = new DatePickerFragment();
        datepicker.setListener(dateListener);

        initDate();
        initView();
    }

    private void initDate() {
        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH) + 1;
        day = c.get(Calendar.DAY_OF_MONTH);
    }

    private void initView() {
        try {
            tvDate.setText(getIntent().getExtras().getString("date"));
        } catch (Exception e) {
            tvDate.setText("Date");
        }

        getSpinnerAdapter();

        if (from_detail) {
            tvTitle.setText("Edit calls");

            account_id = model.getAccount_id();

            spCallType.setSelection(Integer.parseInt(model.getCall_type_id()));
            spCallPurpose.setSelection(Integer.parseInt(model.getCall_purpose_id()));

            tvAccountName.setText(model.getAccount_name());
            tvDate.setText(model.getDate());
            etSubject.setText(model.getSubject());
            etDetail.setText(model.getDetail());
            etDuration.setText(model.getDuration());

            ivAccountName.setEnabled(false);
            tvAccountName.setEnabled(false);
            tvDate.setEnabled(false);
            etDuration.setEnabled(false);
            spCallPurpose.setEnabled(false);
            spCallType.setEnabled(false);
        }else if (from_account){
            account_id = accountModel.getId();

            tvAccountName.setText(accountModel.getAccount_name());
            etDuration.setText(LastCall());
            etDuration.setEnabled(false);
        }

        ivBack.setOnClickListener(click);
        ivSave.setOnClickListener(click);
        tvAccountName.setOnClickListener(click);
        ivAccountName.setOnClickListener(click);
        tvDate.setOnClickListener(click);
    }

    private void getSpinnerAdapter() {
        SpinnerCustomAdapter spinnerCustomAdapter = new SpinnerCustomAdapter(context,
                R.layout.spinner_default, call_type);
        spinnerCustomAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCallType.setAdapter(spinnerCustomAdapter);
        spCallType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                if (position != 0) {
                    index_type = String.valueOf(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        SpinnerCustomAdapter spinnerAdapter = new SpinnerCustomAdapter(context,
                R.layout.spinner_default, call_purpose);
        spinnerAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCallPurpose.setAdapter(spinnerAdapter);
        spCallPurpose.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                if (position != 0) {
                    index_purpose = String.valueOf(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.ivBack:
                    final CustomDialog dialogback = CustomDialog.setupDialogConfirmation(context, "Anda ingin kembali? Data yang anda masukkan akan hilang?");
                    CustomTextView tvCancel = (CustomTextView) dialogback.findViewById(R.id.tvCancel);
                    CustomTextView tvYa = (CustomTextView) dialogback.findViewById(R.id.tvOK);

                    tvCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialogback.dismiss();
                        }
                    });

                    tvYa.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            finish();
                            dialogback.dismiss();
                        }
                    });
                    dialogback.show();
                    break;
                case R.id.ivSave:
                    if (checkField()) {
                        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(context)) {
                            if (from_detail) {
                                model.setAccount_id(account_id);
                                model.setSubject(etSubject.getText().toString());
                                model.setDate(tvDate.getText().toString());
                                model.setDetail(etDetail.getText().toString());
                                model.setCreated_at(getCurrentDate());
                                model.setDuration(etDuration.getText().toString());
                                model.setCall_type_id(index_type);
                                model.setCall_purpose_id(index_purpose);

                                showLoadingDialog();

                                presenter.setupEdit(ApiParam.API_082, model);
                            } else {
                                CallsModel callsModel = new CallsModel();
                                callsModel.setAccount_id(account_id);
                                callsModel.setSubject(etSubject.getText().toString());
                                callsModel.setDate(tvDate.getText().toString());
                                callsModel.setDetail(etDetail.getText().toString());
                                callsModel.setDuration(etDuration.getText().toString());
                                callsModel.setCreated_at(getCurrentDate());
                                callsModel.setCall_type_id(index_type);
                                callsModel.setCall_purpose_id(index_purpose);

                                showLoadingDialog();

                                presenter.setupAdd(ApiParam.API_081, callsModel);
                            }
                        } else {

                        }
                    }
                    break;
                case R.id.tvAccountName:
                    AccountDialogFragment accountDialogFragment = new AccountDialogFragment(CallsAddActivity.this, new AccountDialogCallback() {

                        @Override
                        public void onAccountDialogCallback(Bundle data) {
                            tvAccountName.setText(data.getString("name"));
                            contact_json = data.getString("pic");
                            account_id = data.getString("id");
                        }
                    });
                    accountDialogFragment.show(getSupportFragmentManager(), "accountDialog");
                    break;
                case R.id.ivAccountName:
                    AccountDialogFragment DialogFragment = new AccountDialogFragment(CallsAddActivity.this, new AccountDialogCallback() {

                        @Override
                        public void onAccountDialogCallback(Bundle data) {
                            tvAccountName.setText(data.getString("name"));
                            contact_json = data.getString("pic");
                            account_id = data.getString("id");

                            Log.d("TAG", "Contact JSON : " + contact_json);
                        }
                    });
                    DialogFragment.show(getSupportFragmentManager(), "accountDialog");
                    break;
                case R.id.tvDate:
                    datepicker.setDateFragment(year, month, day);
                    datepicker.show(getFragmentManager(), "datePicker");
                    break;
            }
        }
    };

    private boolean checkField() {
        if (account_id.equalsIgnoreCase("")) {
            tvAccountName.setError("Nama Akun wajib diisi");
            return false;
        } else if (etSubject.getText().toString().equalsIgnoreCase("")) {
            etSubject.setError("Subject Harus Diisi !!!");
            return false;
        } else if (tvDate.getText().toString().equalsIgnoreCase("")) {
            tvDate.setError("the Closing Date cannot be Empty !!!");
            return false;
        }
        return true;
    }

    DatePickerDialog.OnDateSetListener dateListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int r_year, int monthOfYear,
                              int dayOfMonth) {
            year = r_year;
            month = monthOfYear + 1;
            day = dayOfMonth;

            string_year = r_year + "";

            if (month < 10) {

                string_month = "0" + month;
            } else {
                string_month = month + "";
            }
            if (day < 10) {

                string_day = "0" + day;
            } else {
                string_day = day + "";
            }
            open_date = year + "-" + string_month + "-" + string_day;

            tvDate.setText(year + "-" + string_month + "-" + string_day);
        }
    };

    @Override
    public void finishAdd(String result, String response) {
        dismissLoadingDialog();
        if (result.equalsIgnoreCase("OK")) {
            finish();
        }
    }

    @Override
    public void finishEdit(String result, String response) {
        dismissLoadingDialog();
        if (result.equalsIgnoreCase("OK")) {
            finish();
            CallsDetailActivity.getInstance().finish();
        }
    }

    public String LastCall() {
        String callDura = "0";
        StringBuffer sb = new StringBuffer();
        Uri contacts = CallLog.Calls.CONTENT_URI;
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
        }

        Cursor managedCursor = getApplicationContext().getContentResolver().query(
                contacts, null, null, null, android.provider.CallLog.Calls.DATE + " DESC limit 1;");
        int number = managedCursor.getColumnIndex(CallLog.Calls.NUMBER);
        int duration1 = managedCursor.getColumnIndex(CallLog.Calls.DURATION);
        int date = managedCursor.getColumnIndex(CallLog.Calls.DATE);
        if (managedCursor.moveToFirst() == true) {
            String phNumber = managedCursor.getString(number);
            callDura = managedCursor.getString(duration1);
            String callDate = managedCursor.getString(date);
            String callDayTime = new Date(Long.valueOf(callDate)).toString();
            String dir = null;
            Log.e("DUR", "\nPhone Number:--- " + phNumber + " \nCall duration in sec :--- " + callDura + " \nCall Date in sec :--- " + callDayTime);
        }
        managedCursor.close();

        return callDura;
    }

    private String getCurrentDate() {
        Calendar c = Calendar.getInstance();

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        return df.format(c.getTime());
    }
}
