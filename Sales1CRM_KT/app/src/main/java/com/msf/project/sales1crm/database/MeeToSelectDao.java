package com.msf.project.sales1crm.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.msf.project.sales1crm.model.ContactModel;

import java.util.ArrayList;
import java.util.List;

public class MeeToSelectDao extends BaseDao<ContactModel> implements DBSchema.MeetTo {
    public MeeToSelectDao(Context c) {
        super(c, TABLE_NAME);
    }

    public MeeToSelectDao(Context c, String table, boolean willWrite) {
        super(c, table, willWrite);
    }

    public MeeToSelectDao(DBHelper dbHelper) {
        super(dbHelper);
    }

    public MeeToSelectDao(DBHelper dbHelper, String table) {
        super(dbHelper, table);
    }

    public MeeToSelectDao(DBHelper dbHelper, String table, boolean willWrite) {
        super(dbHelper, TABLE_NAME, willWrite);
    }

    public MeeToSelectDao(DBHelper db, boolean willWrite) {
        super(db, TABLE_NAME, willWrite);
    }

    @Override
    public ContactModel getById(int id) {
        return null;
    }

    public ContactModel delete(String id){
        String qry = "DELETE FROM " + getTable() + " WHERE " + DBSchema.MeetTo.KEY_MEET_TO_ID + "='"+id+"'";
        Cursor c = getSqliteDb().rawQuery(qry,null);
        ContactModel model = new ContactModel();
        try {
            if(c != null && c.moveToFirst()){
                model = getByCursor(c);
            }
        } finally {
            c.close();
        }
        return model;
    }

    public List<ContactModel> getContactList(){
        String querty = "SELECT * FROM " + getTable() + " WHERE " + DBSchema.MeetTo.KEY_IS_CHECK + " = '1'";
        Cursor c = getSqliteDb().rawQuery(querty,null);
        List<ContactModel> list = new ArrayList<>();
        try{
            if(c !=null & c.moveToFirst()){
                list.add(getByCursor(c));
                while (c.moveToNext()){
                    list.add(getByCursor(c));
                }
            }
        } finally {
            c.close();
        }
        return list;
    }

    public List<ContactModel> getByCheck(){
        String qry = " SELECT * FROM " + getTable() + " WHERE " + DBSchema.MeetTo.KEY_IS_CHECK + " = '1'";
        Cursor c = getSqliteDb().rawQuery(qry, null);
        List<ContactModel> list = new ArrayList<>();
        try {
            if(c != null && c.moveToFirst()){
                list.add(getByCursor(c));
                while (c.moveToFirst()){
                    list.add(getByCursor(c));
                }
            }
        } finally {
            c.close();
        }
        return list;
    }


    public List<ContactModel> getFullName(){
        String query = "SELECT * FROM " + getTable();
        Cursor c = getSqliteDb().rawQuery(query,null);
        List<ContactModel> list = new ArrayList<>();
        try{
            if(c != null && c.moveToFirst()) {
                list.add(getByCursor(c));
                while (c.moveToNext()) {
                    list.add(getByCursor(c));
                }
            }
        } finally {
            c.close();
        }

        return list;
    }



    @Override
    public ContactModel getByCursor(Cursor c) {
        ContactModel model = new ContactModel();
        model.setAccount_id(c.getString(1));
        model.setId(c.getString(2));
        model.setFullname(c.getString(3));
        model.setIsCheck(c.getString(4));
        return model;
    }

    @Override
    protected ContentValues upDataValues(ContactModel contactModel, boolean update) {
        ContentValues cv = new ContentValues();
        cv.put(KEY_ACCOUNT_ID, contactModel.getAccount_id());
        cv.put(KEY_MEET_TO_ID, contactModel.getId());
        cv.put(KEY_FULLNAME, contactModel.getFullname());
        cv.put(KEY_IS_CHECK, contactModel.getIsCheck());
        return cv;
    }
}
