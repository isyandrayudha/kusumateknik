package com.msf.project.sales1crm.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.msf.project.sales1crm.ErrorAPIKeyActivity;
import com.msf.project.sales1crm.MenuActivity;
import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.adapter.ATodoListAdapter;
import com.msf.project.sales1crm.callback.Tcallback;
import com.msf.project.sales1crm.customview.CustomEditText;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.imageloader.ImageLoader;
import com.msf.project.sales1crm.model.ActivityTodoModel;
import com.msf.project.sales1crm.presenter.ActivityTodoListPresenter;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ATodoFragment extends Fragment implements Tcallback {

    @BindView(R.id.tvTodoName)
    CustomTextView tvTodoName;
    @BindView(R.id.rcTodo)
    RecyclerView rcTodo;
    Unbinder unbinder;
    @BindView(R.id.shimmerLayout)
    ShimmerFrameLayout shimmerLayout;
    @BindView(R.id.llshim)
    LinearLayout llshim;
    @BindView(R.id.etSearch)
    CustomEditText etSearch;
    @BindView(R.id.ivClear)
    ImageView ivClear;
    @BindView(R.id.llNotFound)
    LinearLayout llNotFound;
    @BindView(R.id.tvRefresh)
    CustomTextView tvRefresh;
    @BindView(R.id.rlNoConnection)
    RelativeLayout rlNoConnection;
    @BindView(R.id.swipeRefresh)
    SwipeRefreshLayout swipeRefresh;
    private View view;
    private Context context;
    private int prevSize = 0, sort_id = 0;
    private boolean isLoadMore = true;
    private boolean isMaxSize = true;
    private boolean pause = false;
    private boolean isScrolling = false;
    private int currentItems, totalItems, scrollOutItems;

    private ATodoListAdapter todoAdapter;
    private ActivityTodoListPresenter todoPresneter;
    private List<ActivityTodoModel> aTodoModelList = new ArrayList<>();


    public static ATodoFragment newInstance() {
        ATodoFragment fragment = new ATodoFragment();
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.context = getActivity();
        this.todoPresneter = new ActivityTodoListPresenter(context, this);
        rcTodo.setNestedScrollingEnabled(false);
        initData();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_a_todo, container, false);
        ButterKnife.bind(this, this.view);
        return this.view;
    }

    private void initData() {
        Sales1CRMUtils.Utils.imageLoader = new ImageLoader(context);
        isMaxSize = true;
        isLoadMore = true;
        llNotFound.setVisibility(View.GONE);
        tvRefresh.setOnClickListener(click);

        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                llshim.setVisibility(View.VISIBLE);
                shimmerLayout.setVisibility(View.VISIBLE);
                shimmerLayout.startShimmer();
                if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(context)) {

//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
                    todoPresneter.setupList(ApiParam.API_108, etSearch.getText().toString());
//                }
//            }, 2000);

                } else {
                    swipeRefresh.setVisibility(View.GONE);
                    rlNoConnection.setVisibility(View.VISIBLE);
                }
            }
        });


        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(context)) {

//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
            todoPresneter.setupList(ApiParam.API_108, etSearch.getText().toString());
//                }
//            }, 2000);

        } else {
            rlNoConnection.setVisibility(View.VISIBLE);
        }

    }

    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.tvRefresh:
                    llshim.setVisibility(View.VISIBLE);
                    rlNoConnection.setVisibility(View.GONE);
                    initData();
                    break;

            }
        }
    };

    private void setTodoList() {
        llshim.setVisibility(View.VISIBLE);
        shimmerLayout.startShimmer();
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rcTodo.setLayoutManager(layoutManager);
        rcTodo.setHasFixedSize(true);
        rcTodo.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {

                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                currentItems = layoutManager.getChildCount();
                totalItems = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstVisibleItemPosition();

                if (isScrolling && (currentItems + scrollOutItems == totalItems)) {
                    isScrolling = false;
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            todoPresneter.setupMoreList(ApiParam.API_108, etSearch.getText().toString());
                            todoAdapter.notifyDataSetChanged();
                        }
                    }, 5000);

                }
            }
        });
//        todoAdapter = new TodoAdapter(context, this.aTodoModelList);

        List<ATodoListAdapter.Row> rows = new ArrayList<ATodoListAdapter.Row>();

        for (ActivityTodoModel tm : aTodoModelList) {
            //Read List by Row to insert into List Adapter
            rows.add(new ATodoListAdapter.Item(tm));
        }
        todoAdapter.setRows(rows);
        rcTodo.setAdapter(todoAdapter);
        llshim.setVisibility(View.INVISIBLE);
        shimmerLayout.stopShimmer();
        shimmerLayout.setVisibility(View.INVISIBLE);
    }

    private void setTodoMoreList() {
        List<ATodoListAdapter.Row> rows = new ArrayList<ATodoListAdapter.Row>();
        for (ActivityTodoModel country : aTodoModelList) {
            // Add the country to the list
            rows.add(new ATodoListAdapter.Item(country));
        }
        todoAdapter.setRows(rows);
        todoAdapter.notifyDataSetChanged();
    }

    @Override
    public void finishATodoList(String result, List<ActivityTodoModel> list) {
       swipeRefresh.setRefreshing(false);
        if (result.equalsIgnoreCase("OK")) {
            prevSize = list.size();
            this.aTodoModelList = list;
            Log.e("TODO LIST", list.toString());

            isMaxSize = false;
            isLoadMore = false;

            todoAdapter = new ATodoListAdapter(context, this.aTodoModelList);
            setTodoList();
            if (list.size() > 0) {
                llNotFound.setVisibility(View.GONE);
            } else {
                llNotFound.setVisibility(View.VISIBLE);
            }
        } else if (result.equalsIgnoreCase("FAILED_KEY")) {
            startActivity(new Intent(context, ErrorAPIKeyActivity.class));
            MenuActivity.getInstance().finish();
        } else {

            rlNoConnection.setVisibility(View.VISIBLE);
            Log.e("TODO LIST", "KOSONG");
        }
    }

    @Override
    public void finishATodoMoreList(String result, List<ActivityTodoModel> list) {
        if (result.equals("OK")) {
            for (int i = 0; i < list.size(); i++) {
                this.aTodoModelList.add(list.get(i));
            }

            if (this.prevSize == this.aTodoModelList.size())
                isMaxSize = true;
            this.prevSize = this.aTodoModelList.size();
            setTodoMoreList();
        }
        this.isLoadMore = false;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
