package com.msf.project.sales1crm.presenter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;

import com.msf.project.sales1crm.callback.PermissionCallback;
import com.msf.project.sales1crm.model.UserPermissionModel;
import com.msf.project.sales1crm.service.NetworkConnection;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.PreferenceUtility;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;
import com.msf.project.sales1crm.utility.Sales1CRMUtilsJSON;

import org.json.JSONException;
import org.json.JSONObject;

public class PermissionPresenter {
    private final String TAG = PermissionPresenter.class.getSimpleName();
    private Context context;
    private Messenger messenger;
    private Message message;
    private Handler handler;
    private Bundle bundle;
    private String failurResponse = "";
    private String[] stringsResponse = {""};
    private PermissionCallback permissionCallback;
    private String resultPermission = "NG";
    private String response = "Check Your Connection";

    public PermissionPresenter(Context context, PermissionCallback listner) {
        this.context = context;
        this.permissionCallback = listner;
    }

    private void doNetworkService(String url, String params, String from) {
        Log.d(TAG,"URL Permission" + url);
        Log.d(TAG,"params : + params");
        Intent intent = new Intent(this.context, NetworkConnection.class);
        this.messenger = new Messenger(this.handler);
        intent.putExtra("messenger",this.messenger);
        intent.putExtra("url",url);
        intent.putExtra("params",params);
        intent.putExtra("from",from);
        this.context.startService(intent);
    }

    public void setupPermissionItem(int apiIndex, UserPermissionModel userPermissionModel) {
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)) {
            obtainPermissionItems(apiIndex, userPermissionModel);
        } else  {
            permissionCallback.finishedPermission(this.resultPermission,this.response);
        }
    }

    public void obtainPermissionItems(int apiIndex, UserPermissionModel userPermissionModel) {
        this.handler = new Handler(this.context.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                parsePermissionItem(msg);
            }
        };

        JSONObject jsonObject = new JSONObject();
        try{
            jsonObject.put("api_key", PreferenceUtility.getInstance().loadDataString(context,PreferenceUtility.API_KEY));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        doNetworkService(ApiParam.urlBuilderGlobal(this.context,apiIndex),jsonObject.toString(),"api_key");
    }

    private void parsePermissionItem(Message message) {
        this.message = message;
        this.bundle = this.message.getData();
        this.stringsResponse[0] = this.bundle.getString("network_response");
        this.failurResponse = this.bundle.getString("network_failure");
        Log.d(TAG,"responseString[0] permission info" + "network_failure");
        if (this.failurResponse.equalsIgnoreCase("ok")) {
            this.permissionCallback.finishedPermission(this.resultPermission,this.response);
        } else {
            try {
                this.resultPermission = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringsResponse[0]);
                Log.d("Result" , this.resultPermission);
                if (this.resultPermission.equalsIgnoreCase("OK")) {
                    this.response = Sales1CRMUtilsJSON.JSONUtility.getPermissionService(this.stringsResponse[0]);
            } else {
                    this.response = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringsResponse[0]);
                }
                this.permissionCallback.finishedPermission(this.resultPermission,this.response);
            } catch (JSONException e) {
                Log.d(TAG,"EXCEPTION DETAIL RESPONSE" + e.getMessage());
            }
        }
    }
}


