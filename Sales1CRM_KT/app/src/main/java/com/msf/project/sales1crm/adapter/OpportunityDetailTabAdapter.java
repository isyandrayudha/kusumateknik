package com.msf.project.sales1crm.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.msf.project.sales1crm.fragment.OpportunityTabPersonalFragment;
import com.msf.project.sales1crm.fragment.OpportunityTabProductFragment;

public class OpportunityDetailTabAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;
    Bundle bundle;

    public OpportunityDetailTabAdapter(FragmentManager fm, int NumOfTabs, Bundle bundle) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
        this.bundle = bundle;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                OpportunityTabPersonalFragment tab1 = new OpportunityTabPersonalFragment();
                tab1.setArguments(bundle);
                return tab1;
            case 1:
                OpportunityTabProductFragment tab2 = new OpportunityTabProductFragment();
                tab2.setArguments(bundle);
                return tab2;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
