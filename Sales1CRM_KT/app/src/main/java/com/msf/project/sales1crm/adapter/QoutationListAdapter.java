package com.msf.project.sales1crm.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;

import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.model.QuotationModel;

import java.util.ArrayList;
import java.util.List;

public class QoutationListAdapter extends BaseAdapter {
    private List<QuotationModel> list = new ArrayList<>();
    private Context context;

    public QoutationListAdapter(Context context, List<QuotationModel> obj) {
        this.context = context;
        this.list = obj;
    }

    public static abstract class Row {

    }

    public static final class Section extends Row {
        public final String text;

        public Section(String text) {
            this.text = text;
        }
    }

    public static final class Item extends Row {
        public final QuotationModel text;

        public Item(QuotationModel text) {
            this.text = text;
        }

        public QuotationModel getItem() {
            return text;
        }
    }

    private List<Row> rows;

    public void setRows(List<Row> rows) {
        this.rows = rows;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return rows.size();
    }

    @Override
    public Object getItem(int position) {
        return rows.get(position);
    }

    @Override
    public long getItemId(int position) {
        return rows.indexOf(getItem(position));
    }

    @Override
    public int getItemViewType(int position) {
        if (getItem(position) instanceof KursListAdapter.Section) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        final ViewHolder holder;
        final Item item = (Item) getItem(position);
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = (LinearLayout) inflater.inflate(R.layout.row_quotation, parent, false);
            holder = new ViewHolder();
            holder.tvReferencesNumber = (CustomTextView) view.findViewById(R.id.tvReferencesNumber);
            holder.tvAmount = (CustomTextView) view.findViewById(R.id.tvAmount);
            holder.tvProductCount = (CustomTextView) view.findViewById(R.id.tvProductCount);
        } else {
            holder =(ViewHolder) view.getTag();
        }
        holder.tvAmount.setText(item.text.getAmount());
        holder.tvReferencesNumber.setText(item.text.getReferencesNumber());
        holder.tvProductCount.setText(item.text.getProductCount());
        return view;
    }


    public static class ViewHolder {
        CustomTextView tvReferencesNumber;
        CustomTextView tvAmount;
        CustomTextView tvProductCount;
    }

}
