package com.msf.project.sales1crm.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.msf.project.sales1crm.OpportunityDetailActivity;
import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.model.OpportunityModel;

import java.util.List;

public class OpportunityListCardAdapter extends RecyclerView.Adapter<OpportunityListCardAdapter.OpportunityListCardHolder>{

    private Context context;
    private String url;
    private String iterated;
    private int size;
    private String[] type = {"opportunity Type", "None", "New Business", "Ext Business"};
    private List<OpportunityModel> opportunityModelList;
    private LayoutInflater layoutInflater;

    public OpportunityListCardAdapter (Context context, List<OpportunityModel> objects) {
        this.context = context;
        this. opportunityModelList = objects;
    }

    public static abstract class Row {

    }

    public static final  class Section extends Row {
        public final String text;

        public Section(String text) {
            this.text = text;
        }
    }

    public static final  class  Item extends Row {
        public final OpportunityModel text;

        public Item(OpportunityModel text) {
            this.text = text;
        }

        public OpportunityModel getItem() {
            return text;
        }
    }

    private  List<Row> rows ;

    public void setRows(List<Row>rows) {
        this.rows = rows;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return rows.size();
    }

    @Override
    public OpportunityListCardAdapter.OpportunityListCardHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.card_opportunityactive,null);
        return new OpportunityListCardHolder(v);
    }

    @Override
    public void onBindViewHolder(OpportunityListCardHolder holder, int position) {
        final Item item = (Item) rows.get(position);
        Log.i("TAG",
                "number: "
                        + coolFormat(Double.parseDouble(item.getItem()
                        .getSize()), 0));
        holder.tvOpportunityName.setText(item.text.getName());
        holder.tvAccountName.setText(item.text.getAccount_name());
        holder.tvOpportunityType.setText(type[Integer.parseInt(item.text.getType())]);
        holder.tvOpportunityStatus.setText(item.text.getOppor_status_name());
        holder.tvStage.setText(item.text.getStage_name());
        holder.tvCloseDate.setText(item.text.getClose_date());
        holder.tvProgress.setText(item.text.getProgress() + "%");
        holder.tvSize.setText(size + iterated);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    Intent intent = new Intent(context, OpportunityDetailActivity.class);
                    intent.putExtra("opportunityModel", item.getItem());
                    context.startActivity(intent);
            }
        });
    }

    private char[] c = new char[] { 'K', 'M', 'B', 'T' };

    private String coolFormat(double n, int iteration) {
        double d = ((long) n / 100) / 10.0;
        boolean isRound = (d * 10) % 10 == 0;// true if the decimal part is
        // equal to 0 (then it's trimmed
        // anyway)
        Log.i("TAG", "num : " + d);
        size = (int) d;
        iterated = String.valueOf(c[iteration]);
        Log.i("TAG", "iteration : " + c[iteration]);
        return (d < 1000 ? // this determines the class, i.e. 'k', 'm' etc
                ((d > 99.9 || isRound || (!isRound && d > 9.99) ? // this decides
                        // whether to trim
                        // the decimals
                        (int) d * 10 / 10
                        : d + "" // (int) d * 10 / 10 drops the decimal
                ) + "" + c[iteration])
                : coolFormat(d, iteration + 1));
    }



   public class OpportunityListCardHolder extends RecyclerView.ViewHolder {
       CustomTextView tvAccountName, tvOpportunityName, tvOpportunityType,
       tvOpportunityStatus, tvType, tvStage, tvCloseDate, tvProgress, tvSize;
        public OpportunityListCardHolder(View itemView) {
            super(itemView);
//            Log.i("TAG",
//                    "number: "
//                            + coolFormat(Double.parseDouble(item.getItem()
//                            .getSize()), 0));
            tvOpportunityName = (CustomTextView) itemView.findViewById(R.id.tvOpportunityName);
            tvAccountName = (CustomTextView) itemView.findViewById(R.id.tvAccountName);
            tvOpportunityType = (CustomTextView) itemView.findViewById(R.id.tvOpportunityType);
            tvOpportunityStatus = (CustomTextView) itemView.findViewById(R.id.tvOpportunityStatus);
            tvStage = (CustomTextView) itemView.findViewById(R.id.tvStage);
            tvCloseDate = (CustomTextView) itemView.findViewById(R.id.tvCloseDate);
            tvProgress = (CustomTextView) itemView.findViewById(R.id.tvProgress);
            tvSize = (CustomTextView) itemView.findViewById(R.id.tvSize);
        }


    }

}
