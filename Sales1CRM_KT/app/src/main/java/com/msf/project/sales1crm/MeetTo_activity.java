package com.msf.project.sales1crm;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.msf.project.sales1crm.adapter.ContactListAdapter;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.database.DBHelper;
import com.msf.project.sales1crm.database.MeeToSelectDao;
import com.msf.project.sales1crm.database.MeetToDao;
import com.msf.project.sales1crm.model.ContactModel;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MeetTo_activity extends BaseActivity {

    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.tvTitle)
    CustomTextView tvTitle;
    @BindView(R.id.ivAdd)
    ImageView ivAdd;
    @BindView(R.id.rl)
    RelativeLayout rl;
    @BindView(R.id.lvContact)
    ListView lvContact;
    @BindView(R.id.llNotFound)
    LinearLayout llNotFound;
    private View view;
    private Context context;
    private ContactListAdapter adapter;
    private List<ContactModel> list = new ArrayList<>();
    private String account_id;
    private int prevSize = 0, pos = 0;
    private boolean isLoadMore = true;
    private boolean isMaxSize = true;
    private boolean pause = false;
    private boolean selected = false;
    static MeetTo_activity meetTo_activity;


    public static MeetTo_activity getInstance() {
        return meetTo_activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meet_to_activity);
        ButterKnife.bind(this);
        context = this;

        try {
            account_id = getIntent().getExtras().getString("account_id");
        } catch (Exception e) {

        }

        ivAdd.setOnClickListener(click);
        ivBack.setOnClickListener(click);


            setList();


        if (list.size() <= 0){
            llNotFound.setVisibility(View.VISIBLE);
        } else {
            llNotFound.setVisibility(View.GONE);
        }

        MeeToSelectDao meetToSelect = new MeeToSelectDao(context);
        Log.d("TO", "L " + meetToSelect.getContactList());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Sales1CRMUtils.LIST_MEET_TO:
                if (resultCode == RESULT_OK) {
                    setList();
                    if (list.size() <= 0){
                        llNotFound.setVisibility(View.VISIBLE);
                    } else {
                        llNotFound.setVisibility(View.GONE);
                    }
                    adapter.notifyDataSetChanged();
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.ivAdd:
                    Intent i = new Intent(context, MeetToOnline.class);
                    i.putExtra("account_id", account_id);
                    startActivityForResult(i, Sales1CRMUtils.LIST_MEET_TO);
                    break;
                case R.id.ivBack:
                    MeetToDao dao = new MeetToDao(context);
                    dao.deleteAllRecord();
                    finish();
                    break;
            }
        }
    };

    private void setList() {
        MeeToSelectDao meetToSelect = new MeeToSelectDao(new DBHelper(context), true);
        list = meetToSelect.getContactList();
        adapter = new ContactListAdapter(context, this.list);
        List<ContactListAdapter.Row> rows = new ArrayList<>();
        for (ContactModel model : list) {
            rows.add(new ContactListAdapter.Item(model));
        }

        Log.d("TAG","LISI SIZE : " + list.size());



        adapter.setRows(rows);
        lvContact.setAdapter(adapter);
        lvContact.setSelector(R.drawable.transparent_selector);
        lvContact.setOverScrollMode(View.OVER_SCROLL_NEVER);
        lvContact.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

//        lvContact.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                ContactListAdapter.Item item = (ContactListAdapter.Item) lvContact.getAdapter().getItem(position);
//                AlertDialog.Builder alert = new AlertDialog.Builder(context);
//                alert.setTitle("Alert !!!");
//                alert.setMessage("Delete Meet to?");
//                alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        MeeToSelectDao mse = new MeeToSelectDao(context);
//                        mse.delete(Integer.valueOf(list.get(position).getId()),DBSchema.MeetTo.KEY_MEET_TO_ID);
//                        adapter.notifyDataSetChanged();
//                        setList();
//                        setResult(Activity.RESULT_OK);
//                        dialog.dismiss();
//
//                    }
//                });
//
//                alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        dialog.dismiss();
//                    }
//                });
//                alert.show();
//            }
//        });


    }



    @Override
    public void onResume() {
        super.onResume();
    }
}
