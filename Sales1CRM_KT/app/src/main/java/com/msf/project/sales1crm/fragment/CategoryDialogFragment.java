package com.msf.project.sales1crm.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.adapter.CategoryAdapter;
import com.msf.project.sales1crm.callback.CategoryDialogCallback;
import com.msf.project.sales1crm.callback.CategoryListCallback;
import com.msf.project.sales1crm.customview.CustomEditText;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.model.CategoryModel;
import com.msf.project.sales1crm.presenter.CategoryPresenter;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

@SuppressLint("ValidFragment")
public class CategoryDialogFragment extends BaseDialogFragment implements CategoryListCallback {


    @BindView(R.id.etSearch)
    CustomEditText etSearch;
    @BindView(R.id.ivClear)
    ImageView ivClear;
    @BindView(R.id.tvRefresh)
    CustomTextView tvRefresh;
    @BindView(R.id.rlNoConnection)
    RelativeLayout rlNoConnection;
    @BindView(R.id.llNotFound)
    LinearLayout llNotFound;
    @BindView(R.id.rlProgress)
    RelativeLayout rlProgress;
    @BindView(R.id.lvcategory)
    ListView lvcategory;
    @BindView(R.id.lvSwipe)
    SwipeRefreshLayout lvSwipe;
    @BindView(R.id.rlcategoryList)
    RelativeLayout rlcategoryList;
    private View view;
    private Context context;
    private CategoryAdapter adapter;
    private CategoryDialogCallback callback;
    private CategoryPresenter presenter;
    private List<CategoryModel> list = new ArrayList<>();


    @SuppressLint("ValidFragment")
    public CategoryDialogFragment(Context context, CategoryDialogCallback callback) {
        this.context = context;
        this.callback = callback;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, 0);
    }

    @Override
    public void onStart() {
        super.onStart();
        Window window = getDialog().getWindow();
        WindowManager.LayoutParams windowParams = window.getAttributes();
        windowParams.width = (int) (this.context.getResources().getDisplayMetrics().widthPixels * 0.99);
        windowParams.height = (int) (this.context.getResources().getDisplayMetrics().heightPixels * 0.97);
        window.setAttributes(windowParams);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreateView(inflater, container, savedInstanceState);

        this.view = inflater.inflate(R.layout.dialog_category_list, container, false);

        ButterKnife.bind(this, this.view);
        return this.view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        this.presenter = new CategoryPresenter(context, this);
        getDataAPI();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
    }

    private void getDataAPI() {
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(context)) {
            presenter.setupList(ApiParam.API_118);
        } else {
            lvSwipe.setVisibility(View.GONE);
            rlNoConnection.setVisibility(View.VISIBLE);
        }

    }

    private void setList() {
        adapter = new CategoryAdapter(context, this.list);
        List<CategoryAdapter.Row> rows = new ArrayList<>();
        for (CategoryModel cm : list) {
            rows.add(new CategoryAdapter.Item(cm));
        }

        adapter.setRows(rows);
        lvcategory.setAdapter(adapter);
        lvcategory.setOnItemClickListener(new CategoryDialogFragment.AdapterOnItemClick());
        lvcategory.deferNotifyDataSetChanged();
    }


    @Override
    public void finishCategoryList(String result, List<CategoryModel> list) {
        lvSwipe.setRefreshing(false);
        rlProgress.setVisibility(View.GONE);
        if (result.equalsIgnoreCase("OK")) {
            this.list = list;
            setList();
            if (list.size() > 0) {
                llNotFound.setVisibility(View.GONE);
            } else {
                llNotFound.setVisibility(View.VISIBLE);
            }
        } else {
            rlNoConnection.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    private class AdapterOnItemClick implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final CategoryAdapter.Item item = (CategoryAdapter.Item) lvcategory.getAdapter().getItem(position);
            Bundle bundle = new Bundle();
            Log.d("TAG","name : " + item.text.getName());
            bundle.putString("id", item.text.getId());
            bundle.putString("name", item.text.getName());
            callback.onAccountDialogCallback(bundle);
            dismiss();
        }
    }
}
