package com.msf.project.sales1crm.fragment;

import android.support.v4.app.DialogFragment;

import com.msf.project.sales1crm.utility.CustomLoadingCRM;

/**
 * Created by christianmacbook on 25/05/18.
 */

public class BaseDialogFragment extends DialogFragment {

    private CustomLoadingCRM dialog;

    public void showLoadingDialog() {
        dialog = new CustomLoadingCRM(getActivity());
        dialog.show(getActivity().getSupportFragmentManager(), "customLoadingDialog");
    }

    public void dismissLoadingDialog() {
        if (null == dialog) return;
        dialog.dismiss();
    }
}
