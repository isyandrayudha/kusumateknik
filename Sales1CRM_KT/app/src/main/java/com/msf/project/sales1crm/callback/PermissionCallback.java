package com.msf.project.sales1crm.callback;

public interface PermissionCallback {
    void finishedPermission (String result, String response);
}
