package com.msf.project.sales1crm;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.msf.project.sales1crm.fragment.OpportunityActiveFragment;
import com.msf.project.sales1crm.fragment.OpportunityCloseFragment;
import com.msf.project.sales1crm.fragment.OpportunityTabFragment;

public class OpportunityTabListAdapter extends FragmentStatePagerAdapter

    {
        int mNumOfTabs;
        Bundle bundle;
        private OpportunityTabFragment mFragment;

    public OpportunityTabListAdapter(FragmentManager fm, int NumOfTabs, OpportunityTabFragment fragment) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
        this.mFragment = fragment;
    }

        @Override
        public Fragment getItem(int position) {

        switch (position) {
            case 0:
                OpportunityActiveFragment tab1 = new OpportunityActiveFragment();
                mFragment.setClickActionFilterListener(tab1);
                return tab1;
            case 1:
                OpportunityCloseFragment tab2 = new OpportunityCloseFragment();
                mFragment.setClickActionFilterListener(tab2);
                return tab2;
            default:
                return null;
        }
    }

        @Override
        public int getCount() {
        return mNumOfTabs;
    }
}
