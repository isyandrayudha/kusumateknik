package com.msf.project.sales1crm.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.msf.project.sales1crm.model.MeetToModel;

import java.util.ArrayList;
import java.util.List;

public class MeetToDao extends BaseDao<MeetToModel> implements DBSchema.Meet_to_list {
    public MeetToDao(Context c) {
        super(c, TABLE_NAME);
    }

    public MeetToDao(Context c, String table, boolean willWrite) {
        super(c, table, willWrite);
    }

    public MeetToDao(DBHelper dbHelper) {
        super(dbHelper);
    }

    public MeetToDao(DBHelper db, boolean willWrite) {
        super(db,TABLE_NAME, willWrite);
    }

    public MeetToDao(DBHelper dbHelper, String table, boolean willWrite) {
        super(dbHelper, table, willWrite);
    }

    public MeetToDao(Context c, DBHelper db, String table, boolean willWrite) {
        super(c, db, table, willWrite);
    }

    @Override
    public MeetToModel getById(int id) {
        return null;
    }

    public List<MeetToModel> getMeetToList(){
        String querty = "SELECT * FROM " + getTable() + " WHERE " + DBSchema.Meet_to_list.KEY_NOTATION + " = '1'";
        Cursor c = getSqliteDb().rawQuery(querty,null);
        List<MeetToModel> list = new ArrayList<>();
        try{
            if(c !=null & c.moveToFirst()){
                list.add(getByCursor(c));
                while (c.moveToNext()){
                    list.add(getByCursor(c));
                }
            }
        } finally {
            c.close();
        }
        return list;
    }

    public MeetToModel getMeetToId(String id){
        String qery = " SELECT * FROM " + getTable() + " WHERE " + DBSchema.Meet_to_list.KEY_MEET_TO_ID + " = " + id;
        Cursor c = getSqliteDb().rawQuery(qery,null);
        MeetToModel model = new MeetToModel();
        try {
            if(c != null && c.moveToFirst()){
                model = getByCursor(c);
            }
        } finally {
            c.close();
        }
        return model;
    }

    public List<MeetToModel> getMeetToByStatus(){
        String querty = "SELECT * FROM " + getTable() + " WHERE " + DBSchema.Meet_to_list.KEY_STATUS + " = '1'";
        Cursor c = getSqliteDb().rawQuery(querty,null);
        List<MeetToModel> list = new ArrayList<>();
        try{
            if(c !=null & c.moveToFirst()){
                list.add(getByCursor(c));
                while (c.moveToNext()){
                    list.add(getByCursor(c));
                }
            }
        } finally {
            c.close();
        }
        return list;
    }

    @Override
    public MeetToModel getByCursor(Cursor c) {
        MeetToModel model = new MeetToModel();
        model.setMeet_to_id(c.getString(1));
        model.setName(c.getString(2));
        model.setStatus(c.getString(3));
        model.setNotation(c.getString(4));
        return model;
    }

    @Override
    protected ContentValues upDataValues(MeetToModel meetToModel, boolean update) {
        ContentValues cv = new ContentValues();
        cv.put(KEY_MEET_TO_ID, meetToModel.getMeet_to_id());
        cv.put(KEY_FULLNAME, meetToModel.getName());
        cv.put(KEY_STATUS,meetToModel.getStatus());
        cv.put(KEY_NOTATION,meetToModel.getNotation());
        return cv;
    }
}
