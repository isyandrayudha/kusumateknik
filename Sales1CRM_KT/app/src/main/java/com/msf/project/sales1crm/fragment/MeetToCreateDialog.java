package com.msf.project.sales1crm.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.callback.MeetToAddCallback;
import com.msf.project.sales1crm.customview.CustomEditText;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.customview.SpinnerCustomAdapter;
import com.msf.project.sales1crm.database.PositionDao;
import com.msf.project.sales1crm.model.ContactModel;
import com.msf.project.sales1crm.model.PositionModel;
import com.msf.project.sales1crm.presenter.MeetToAddPresenter;
import com.msf.project.sales1crm.utility.ApiParam;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


@SuppressLint("ValidFragment")
public class MeetToCreateDialog extends BaseDialogFragment implements MeetToAddCallback {
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.tvTitle)
    CustomTextView tvTitle;
    @BindView(R.id.ivSave)
    ImageView ivSave;
    @BindView(R.id.rl)
    RelativeLayout rl;
    @BindView(R.id.spTitle)
    Spinner spTitle;
    @BindView(R.id.ivSpin)
    ImageView ivSpin;
    @BindView(R.id.etFirstName)
    CustomEditText etFirstName;
    @BindView(R.id.etLastName)
    CustomEditText etLastName;
    @BindView(R.id.spPosition)
    Spinner spPosition;
    @BindView(R.id.ivspinn)
    ImageView ivspinn;
    @BindView(R.id.etPhone)
    CustomEditText etPhone;
    @BindView(R.id.etEmail)
    CustomEditText etEmail;
    private View view;
    private Context context;
    private SpinnerCustomAdapter spTitleAdapter,spPositionAdapter;
    private int indexTitle = 0, indexPosition = 0;
    private String[] title = {"Select Title", "Mr.", "Ms.", "Mrs.", "Dr.",
            "Prof."};
    private PositionModel positionModel;
    private MeetToAddPresenter presenter;
    private ContactModel model;
    private String account_id;
    private List<PositionModel> positionList = new ArrayList<>();
    private ArrayList<String> arrPosition = new ArrayList<>();
    private boolean from_detail = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, 0);
        account_id = getArguments().getString("account_id");
        Log.d("TAG", "ACCOUNT_ID" + account_id);
    }

    @Override
    public void onStart() {
        super.onStart();
        Window window = getDialog().getWindow();
        WindowManager.LayoutParams windowParams = window.getAttributes();
        windowParams.width = (int) (this.context.getResources().getDisplayMetrics().widthPixels * 0.99);
        windowParams.height = (int) (this.context.getResources().getDisplayMetrics().heightPixels * 0.97);
        window.setAttributes(windowParams);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        this.view = inflater.inflate(R.layout.fragment_meet_to, container, false);
        this.context = getActivity();
        this.presenter = new MeetToAddPresenter(context, this);
        ButterKnife.bind(this, this.view);
        getDataSpinner();
        ivBack.setOnClickListener(click);
        ivSave.setOnClickListener(click);
        account_id = getArguments().getString("account_id");
        Log.d("TAG", "ACCOUNT_ID" + account_id);
        return this.view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        account_id = getArguments().getString("account_id");
        Log.d("TAG", "ACCOUNT_ID" + account_id);
    }

    private void getDataSpinner() {
        //Title
        spTitleAdapter = new SpinnerCustomAdapter(context,
                R.layout.spinner_default, title);
        spTitleAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spTitle.setAdapter(spTitleAdapter);
        spTitle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                if (position != 0) {
                    indexTitle = position;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        positionList = new PositionDao(context).getPositionList();

//        if (from_detail && !model.getPosition_id().equalsIgnoreCase("0")) {
//            for (int i = 0; i < positionList.size(); i++) {
//                if (i == 0) {
//                    arrPosition.add("Select Position");
//                }
//
//                if (model.getPosition_id().equalsIgnoreCase(positionList.get(i).getId()))
//                    indexPosition = i + 1;
//                arrPosition.add(positionList.get(i).getName());
//            }
//        } else {
            for (int i = 0; i < positionList.size(); i++) {
                if (i == 0) {
                    arrPosition.add("Select Position");
                }
                arrPosition.add(positionList.get(i).getName());
            }
//        }
        String[] position = new String[arrPosition.size()];
        position = arrPosition.toArray(position);
        spPositionAdapter = new SpinnerCustomAdapter(context,
                R.layout.spinner_default, position);
        spPositionAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spPosition.setAdapter(spPositionAdapter);
        spPosition.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                if (position != 0) {
                    indexPosition = position;
                    positionModel = positionList.get(position - 1);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.ivBack:
                    dismiss();
                    break;
                case R.id.ivSave:
                    model = new ContactModel();
                    model.setTitle(indexTitle + "");
                    model.setAccount_id(account_id);
                    model.setFirst_name(etFirstName.getText().toString());
                    model.setLast_name(etLastName.getText().toString());
                    if(indexPosition !=0){
                        model.setPosition(positionModel.getId());
                    }
                    model.setPhone(etPhone.getText().toString());
                    model.setEmail(etEmail.getText().toString());
                    etFirstName.setError(null);
                    etPhone.setError(null);
                    showLoadingDialog();

                    presenter.setupMeetTo(ApiParam.API_116,model);
                    break;
            }
        }
    };
    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void finishAdd(String result, String response) {
        dismissLoadingDialog();
        if(result.equalsIgnoreCase("OK")){
            dismiss();
            
        }else {

        }
    }

    @Override
    public void finishEdit(String result, String response) {

    }
}
