package com.msf.project.sales1crm.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.msf.project.sales1crm.ErrorAPIKeyActivity;
import com.msf.project.sales1crm.MenuActivity;
import com.msf.project.sales1crm.OpportunityDetailActivity;
import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.adapter.OpportunityListAdapter;
import com.msf.project.sales1crm.callback.OpportunityListCloseCallback;
import com.msf.project.sales1crm.callback.SortDialogCallback;
import com.msf.project.sales1crm.customview.CustomEditText;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.imageloader.ImageLoader;
import com.msf.project.sales1crm.model.OpportunityModel;
import com.msf.project.sales1crm.presenter.OpportunityListClosePresenter;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class OpportunityCloseFragment extends Fragment implements OpportunityListCloseCallback, OpportunityTabFragment.ClickActionFilterListener {

    @BindView(R.id.rlNoConnection)
    RelativeLayout rlNoConnection;

    @BindView(R.id.tvRefresh)
    CustomTextView tvRefresh;

    @BindView(R.id.etSearch)
    CustomEditText etSearch;

    @BindView(R.id.ivClear)
    ImageView ivClear;

    @BindView(R.id.lvOpportunity)
    ListView lvOpportunity;

    @BindView(R.id.lvOpportunitySwipe)
    SwipeRefreshLayout lvOpportunitySwipe;

    @BindView(R.id.llNotFound)
    LinearLayout llNotFound;

    @BindView(R.id.shimmerLayout)
    ShimmerFrameLayout shimmerLayout;
    Unbinder unbinder;
    @BindView(R.id.llshim)
    LinearLayout llshim;
    @BindView(R.id.ivSort)
    ImageView ivSort;
    @BindView(R.id.llsort)
    LinearLayout llsort;
    @BindView(R.id.rlLeadList)
    RelativeLayout rlLeadList;

    private View view;
    private Context context;
    private OpportunityListClosePresenter presenter;

    private OpportunityListAdapter adapter;
    private List<OpportunityModel> list = new ArrayList<>();

    private int prevSize = 0, sort_id = 0;
    private boolean isLoadMore = true;
    private boolean isMaxSize = true;
    private boolean pause = false;
    private String[] sort = {"Name A-Z", "Name Z-A", "Opportunity Size Small - Big", "Opportunity Size Big - Small"};



    public static OpportunityCloseFragment newInstance() {
        OpportunityCloseFragment fragment = new OpportunityCloseFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_opportunityclose, container, false);

        unbinder = ButterKnife.bind(this, this.view);
        return this.view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);
        this.context = getActivity();

        this.presenter = new OpportunityListClosePresenter(context, this);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Opportunity");

        initData();
    }

    private void initData() {
        Sales1CRMUtils.Utils.imageLoader = new ImageLoader(context);
        etSearch.clearFocus();
        lvOpportunity.setOnScrollListener(new ListLazyLoad());
        lvOpportunitySwipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                llshim.setVisibility(View.VISIBLE);
                shimmerLayout.setVisibility(View.VISIBLE);
                shimmerLayout.startShimmer();
                getDataFromAPI(0, 0);
            }
        });

        //Set Seach Function
        setTextWatcherForSearch();
        getDataFromAPI(0, sort_id);

        ivClear.setOnClickListener(click);
        tvRefresh.setOnClickListener(click);
        llsort.setOnClickListener(click);
    }

    private void setTextWatcherForSearch() {
        etSearch.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    if (etSearch.getText().length() == 0) {
                        ivClear.setVisibility(View.VISIBLE);
                    }
                } else {
                    if (etSearch.getText().length() == 0) {
                        ivClear.setVisibility(View.GONE);
                    } else {
                        ivClear.setVisibility(View.VISIBLE);
                    }

                }
            }
        });

        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    llshim.setVisibility(View.VISIBLE);
                    shimmerLayout.setVisibility(View.VISIBLE);
                    shimmerLayout.startShimmer();
                    getDataFromAPI(0, 0);
                }

                return true;
            }
        });
    }

    public void getDataFromAPI(int prev, int sort_id) {
        isMaxSize = true;
        isLoadMore = true;

        llNotFound.setVisibility(View.GONE);

        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(context)) {
            presenter.setupList(ApiParam.API_043, prev, sort_id, etSearch.getText().toString());
        } else {
            lvOpportunitySwipe.setVisibility(View.GONE);
            rlNoConnection.setVisibility(View.VISIBLE);
        }
    }

    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.ivClear:
                    etSearch.setText("");
                    etSearch.clearFocus();
                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);
                    getDataFromAPI(0, sort_id);
                    break;
                case R.id.tvRefresh:
                    llshim.setVisibility(View.VISIBLE);
                    rlNoConnection.setVisibility(View.GONE);
                    getDataFromAPI(0, sort_id);
                    break;
                case R.id.llsort:
                    SortDialogFragment DialogFragment = new SortDialogFragment(context, new SortDialogCallback() {
                        @Override
                        public void onDialogCallback(Bundle data) {
                            llshim.setVisibility(View.VISIBLE);
                            shimmerLayout.setVisibility(View.VISIBLE);
                            shimmerLayout.startShimmer();
                            sort_id = Integer.parseInt(data.getString("id"));
                            getDataFromAPI(0, sort_id);
                        }
                    }, sort);
                    DialogFragment.show(getActivity().getSupportFragmentManager(), "sortDialog");
                    break;
                default:
                    break;
            }
        }
    };

    private void setList() {
        llshim.setVisibility(View.VISIBLE);
        shimmerLayout.setVisibility(View.GONE);
        adapter = new OpportunityListAdapter(context, this.list);
        List<OpportunityListAdapter.Row> rows = new ArrayList<OpportunityListAdapter.Row>();
        for (OpportunityModel leadModel : list) {
            //Read List by Row to insert into List Adapter
            rows.add(new OpportunityListAdapter.Item(leadModel));
        }
        //Set Row Adapter
        adapter.setRows(rows);
        lvOpportunity.setAdapter(adapter);
        lvOpportunity.setOnItemClickListener(new AdapterOnItemClick());
        lvOpportunity.deferNotifyDataSetChanged();
        llshim.setVisibility(View.INVISIBLE);
        shimmerLayout.stopShimmer();
        shimmerLayout.setVisibility(View.INVISIBLE);
    }

    private void setMoreList() {
        List<OpportunityListAdapter.Row> rows = new ArrayList<OpportunityListAdapter.Row>();
        for (OpportunityModel country : list) {
            // Add the country to the list
            rows.add(new OpportunityListAdapter.Item(country));
        }
        adapter.setRows(rows);
        adapter.notifyDataSetChanged();

    }

    @Override
    public void finishOpporList(String result, List<OpportunityModel> list) {
        lvOpportunitySwipe.setRefreshing(false);
        if (result.equalsIgnoreCase("OK")) {
            prevSize = list.size();
            this.list = list;

            isMaxSize = false;
            isLoadMore = false;
            setList();
            if (list.size() > 0) {
                llNotFound.setVisibility(View.GONE);
            } else {
                llNotFound.setVisibility(View.VISIBLE);
            }
        } else if (result.equalsIgnoreCase("FAILED_KEY")) {
            startActivity(new Intent(context, ErrorAPIKeyActivity.class));
            MenuActivity.getInstance().finish();
        } else {
//            lvOpportunitySwipe.setVisibility(View.GONE);
            rlNoConnection.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void finishOpporMoreList(String result, List<OpportunityModel> list) {
        if (result.equals("OK")) {
            for (int i = 0; i < list.size(); i++) {
                this.list.add(list.get(i));
            }

            if (this.prevSize == this.list.size())
                isMaxSize = true;
            this.prevSize = this.list.size();
            setMoreList();
        }
        this.isLoadMore = false;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onClickItemFilter(int id) {
        new Handler().postDelayed(() -> getDataFromAPI(0, id), 3000);
    }

    private class ListLazyLoad implements AbsListView.OnScrollListener {

        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {

        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            if (firstVisibleItem + visibleItemCount == totalItemCount && totalItemCount != 0) {
                Log.d("TAG", "isLoadMore : " + isLoadMore);
                if (isLoadMore == false && isMaxSize == false) {
                    isLoadMore = true;
                    prevSize = list.size();

                    presenter.setupMoreList(ApiParam.API_043, prevSize, sort_id, etSearch.getText().toString());
                }
            }
        }
    }

    private class AdapterOnItemClick implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            // TODO Auto-generated method stub
            OpportunityListAdapter.Item item = (OpportunityListAdapter.Item) lvOpportunity.getAdapter().getItem(position);
            switch (view.getId()) {
                default:
                    Intent intent = new Intent(context, OpportunityDetailActivity.class);
                    intent.putExtra("opportunityModel", item.getItem());
                    intent.putExtra("close_oppor", true);
                    startActivity(intent);
                    break;
            }
        }
    }

    @Override
    public void onPause() {
        pause = true;
        super.onPause();
    }

    @Override
    public void onResume() {
        if (pause) {
            getDataFromAPI(0, sort_id);
        }
        super.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
