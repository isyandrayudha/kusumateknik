package com.msf.project.sales1crm.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.msf.project.sales1crm.DatabaseMachineDetail;
import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.model.DatabaseMachineModel;

import java.util.List;

import butterknife.BindView;

public class DatabaseMachineAdapter extends RecyclerView.Adapter<DatabaseMachineAdapter.Holder> {

    private Context context;
    private List<DatabaseMachineModel> list;

    public DatabaseMachineAdapter(Context context, List<DatabaseMachineModel> list) {
        this.context = context;
        this.list = list;
    }

    public static abstract class Row {

    }

    public static final class Section extends Row {
        public final String text;

        public Section(String text) {
            this.text = text;
        }
    }

    public static final class Item extends Row {
        public final DatabaseMachineModel text;

        public Item(DatabaseMachineModel text) {
            this.text = text;
        }

        public DatabaseMachineModel getItem() {
            return text;
        }
    }

    private List<Row> rows;

    public void setRows(List<Row> rows) {
        this.rows = rows;
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_database_machine, null);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        final Item item = (Item) rows.get(position);
        holder.tvAccountName.setText(item.text.getAccountName());
        holder.tvType.setText(item.text.getType());
        holder.tvBrand.setText(item.text.getBrandName());
        holder.tvModel.setText(item.text.getModel());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, DatabaseMachineDetail.class);
                i.putExtra("model", item.getItem());
                context.startActivity(i);
            }
        });
    }


    @Override
    public int getItemCount() {
        return rows.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvAccountName)
        CustomTextView tvAccountName;
        @BindView(R.id.tvType)
        CustomTextView tvType;
        @BindView(R.id.tvBrand)
        CustomTextView tvBrand;
        @BindView(R.id.tvModel)
        CustomTextView tvModel;
        public Holder(View itemView) {
            super(itemView);
            tvAccountName = (CustomTextView) itemView.findViewById(R.id.tvAccountName);
            tvType = (CustomTextView) itemView.findViewById(R.id.tvType);
            tvBrand = (CustomTextView) itemView.findViewById(R.id.tvBrand);
            tvModel = (CustomTextView) itemView.findViewById(R.id.tvModel);
        }
    }
}
