package com.msf.project.sales1crm.fragment;

import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.callback.TotalLeadsCallback;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.customview.DatePickerFragment;
import com.msf.project.sales1crm.model.TotalModel;
import com.msf.project.sales1crm.presenter.ViewReportLeadConversionPresenter;
import com.msf.project.sales1crm.service.GPSTracker;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ViewReportLeadConversionFragment extends Fragment implements TotalLeadsCallback {

    @BindView(R.id.lineChart)
    LineChart lineChart;

    @BindView(R.id.tvStartDate)
    CustomTextView tvStartDate;

    @BindView(R.id.tvEndDate)
    CustomTextView tvEndDate;

    @BindView(R.id.tvSubmit)
    CustomTextView tvSubmit;

    private View view;
    private Context context;
    private ViewReportLeadConversionPresenter presenter;
    private List<TotalModel> list = new ArrayList<>();
    private List<TotalModel> list1 = new ArrayList<>();
    private String startDate = "", endDate = "";
    private DatePickerFragment datepicker1, datepicker2;
    private String open_date = "", string_year, string_month, string_day;
    private int year, month, day;

    public static ViewReportLeadConversionFragment newInstance() {
        ViewReportLeadConversionFragment fragment = new ViewReportLeadConversionFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_leadconversion, container, false);

        return this.view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);
        this.context = getActivity();

        this.presenter = new ViewReportLeadConversionPresenter(context, this);

        datepicker1 = new DatePickerFragment();
        datepicker1.setListener(dateListener1);

        datepicker2 = new DatePickerFragment();
        datepicker2.setListener(dateListener2);

        initDate();
        initData();
    }

    private void initDate() {
        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH) + 1;
        day = c.get(Calendar.DAY_OF_MONTH);
    }

    private void initData(){
//        getDataFromAPI();

        tvStartDate.setOnClickListener(click);
        tvEndDate.setOnClickListener(click);
        tvSubmit.setOnClickListener(click);
    }

    public void getDataFromAPI(){
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(context)){
            GPSTracker gpsTracker = new GPSTracker(context);
            if (gpsTracker.canGetLocation()) {
                presenter.setupTotal(ApiParam.API_091, startDate, endDate);
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gpsTracker.showSettingsAlert();
            }
        }
    }

    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.tvSubmit:
                    getDataFromAPI();
                    break;
                case R.id.tvStartDate:
                    datepicker1.setDateFragment(year, month, day);
                    datepicker1.show(getActivity().getFragmentManager(), "datePicker");
                    break;
                case R.id.tvEndDate:
                    datepicker2.setDateFragment(year, month, day);
                    datepicker2.show(getActivity().getFragmentManager(), "datePicker");
                    break;
            }
        }
    };

    @Override
    public void finished(String result, List<TotalModel> listLead, List<TotalModel> listLead1) {
        if (result.equalsIgnoreCase("OK")){

            ArrayList<Entry> leadValue = new ArrayList<Entry>();
            ArrayList<Entry> leadValue1 = new ArrayList<Entry>();
            ArrayList<String> leadString = new ArrayList<String>();
            for (int x = 0; x < listLead.size(); x++){
                leadValue.add(new Entry(x+1, Integer.parseInt(listLead.get(x).getTotal())));
                leadValue1.add(new Entry(x+1, Integer.parseInt(listLead1.get(x).getTotal())));
                leadString.add(String.valueOf(Integer.valueOf(x+1)));
            }

            setDataLead(leadValue, leadValue1);
        }
    }

    private void setDataLead(ArrayList<Entry> yVals, ArrayList<Entry> yVals1){
        // create a dataset and give it a type
        LineDataSet set1 = new LineDataSet(yVals, "Total leads");
        set1.setFillAlpha(110);
        set1.setFillColor(Color.BLUE);

        // set the line to be drawn like this "- - - - - -"
        // set1.enableDashedLine(10f, 5f, 0f);
        // set1.enableDashedHighlightLine(10f, 5f, 0f);
        set1.setColor(Color.BLUE);
        set1.setCircleColor(Color.BLUE);
        set1.setLineWidth(1f);
        set1.setCircleRadius(2f);
        set1.setDrawCircleHole(false);
        set1.setValueTextSize(8f);
        set1.setDrawFilled(true);

        LineDataSet set2 = new LineDataSet(yVals1, "Lead Conversion");
        set2.setFillAlpha(110);
        set2.setFillColor(Color.GREEN);

        // set the line to be drawn like this "- - - - - -"
        // set2.enableDashedLine(10f, 5f, 0f);
        // set2.enableDashedHighlightLine(10f, 5f, 0f);
        set2.setColor(Color.GREEN);
        set2.setCircleColor(Color.GREEN);
        set2.setLineWidth(1f);
        set2.setCircleRadius(2f);
        set2.setDrawCircleHole(false);
        set2.setValueTextSize(8f);
        set2.setDrawFilled(true);

        XAxis xAxis = lineChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawAxisLine(true);

        YAxis leftAxis = lineChart.getAxisLeft();
        leftAxis.setDrawGridLines(false);

        YAxis rightAxis = lineChart.getAxisRight();
        rightAxis.setTextColor(Color.WHITE);

        ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        dataSets.add(set1); // add the datasets
        dataSets.add(set2); // add the datasets

        // create a data object with the datasets
        LineData data = new LineData(dataSets);
        data.setDrawValues(false);

        // set data
        lineChart.setData(data);
        lineChart.animateY(1000);
        lineChart.setDragEnabled(true);
        lineChart.setScaleEnabled(true);
        lineChart.highlightValue(null);
        lineChart.invalidate();
        lineChart.getDescription().setEnabled(false);
    }

    DatePickerDialog.OnDateSetListener dateListener1 = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int r_year, int monthOfYear,
                              int dayOfMonth) {
            year = r_year;
            month = monthOfYear + 1;
            day = dayOfMonth;

            string_year = r_year + "";

            if (month < 10) {

                string_month = "0" + month;
            } else {
                string_month = month + "";
            }
            if (day < 10) {

                string_day = "0" + day;
            } else {
                string_day = day + "";
            }
            startDate = year + "-" + string_month + "-" + string_day;

            tvStartDate.setText(year + "-" + string_month + "-" + string_day);
        }
    };

    DatePickerDialog.OnDateSetListener dateListener2 = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int r_year, int monthOfYear,
                              int dayOfMonth) {
            year = r_year;
            month = monthOfYear + 1;
            day = dayOfMonth;

            string_year = r_year + "";

            if (month < 10) {

                string_month = "0" + month;
            } else {
                string_month = month + "";
            }
            if (day < 10) {

                string_day = "0" + day;
            } else {
                string_day = day + "";
            }
            endDate = year + "-" + string_month + "-" + string_day;

            tvEndDate.setText(year + "-" + string_month + "-" + string_day);
        }
    };
}
