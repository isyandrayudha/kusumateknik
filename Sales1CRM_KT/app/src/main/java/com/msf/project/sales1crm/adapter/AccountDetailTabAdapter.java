package com.msf.project.sales1crm.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.msf.project.sales1crm.fragment.AccountTabAddressFragment;
import com.msf.project.sales1crm.fragment.AccountTabContactFragment;
import com.msf.project.sales1crm.fragment.AccountTabPersonalFragment;

/**
 * Created by christianmacbook on 14/05/18.
 */

public class AccountDetailTabAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;
    Bundle bundle;

    public AccountDetailTabAdapter(FragmentManager fm, int NumOfTabs, Bundle bundle) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
        this.bundle = bundle;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                AccountTabPersonalFragment tab1 = new AccountTabPersonalFragment();
                tab1.setArguments(bundle);
                return tab1;
            case 1:
                AccountTabAddressFragment tab2 = new AccountTabAddressFragment();
                tab2.setArguments(bundle);
                return tab2;
            case 2:
                AccountTabContactFragment tab3 = new AccountTabContactFragment();
                tab3.setArguments(bundle);
                return tab3;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
