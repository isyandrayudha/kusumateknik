package com.msf.project.sales1crm.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.msf.project.sales1crm.AccountAddActivity;
import com.msf.project.sales1crm.AccountDetailActivity;
import com.msf.project.sales1crm.ErrorAPIKeyActivity;
import com.msf.project.sales1crm.MenuActivity;
import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.adapter.AccountListAdapter;
import com.msf.project.sales1crm.callback.AccountListCallback;
import com.msf.project.sales1crm.callback.DashboardCallback;
import com.msf.project.sales1crm.callback.SortDialogCallback;
import com.msf.project.sales1crm.customview.CustomDialog;
import com.msf.project.sales1crm.customview.CustomEditText;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.imageloader.ImageLoader;
import com.msf.project.sales1crm.model.AbsenceModel;
import com.msf.project.sales1crm.model.AccountModel;
import com.msf.project.sales1crm.model.DashboardFirstModel;
import com.msf.project.sales1crm.model.DashboardSecondModel;
import com.msf.project.sales1crm.model.DashboardThirdModel;
import com.msf.project.sales1crm.model.TotalOpportunityModel;
import com.msf.project.sales1crm.presenter.AccountListPresenter;
import com.msf.project.sales1crm.presenter.DashboardPresenter;
import com.msf.project.sales1crm.service.GPSTracker;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by christianmacbook on 09/05/18.
 */

public class AccountFrangment extends Fragment implements AccountListCallback, DashboardCallback {

    @BindView(R.id.rlNoConnection)
    RelativeLayout rlNoConnection;

    @BindView(R.id.tvRefresh)
    CustomTextView tvRefresh;

    @BindView(R.id.etSearch)
    CustomEditText etSearch;

    @BindView(R.id.ivClear)
    ImageView ivClear;


    @BindView(R.id.lvAccountSwipe)
    SwipeRefreshLayout lvAccountSwipe;

    @BindView(R.id.faAccountAdd)
    FloatingActionButton faAccountAdd;

    @BindView(R.id.faQuickAdd)
    FloatingActionButton faQuickAdd;

    @BindView(R.id.llNotFound)
    LinearLayout llNotFound;

    //    @BindView(R.id.rlProgress)
//    RelativeLayout rlProgress;
    @BindView(R.id.menu)
    FloatingActionMenu menu;
    @BindView(R.id.rlAccountList)
    RelativeLayout rlAccountList;
    Unbinder unbinder;
    @BindView(R.id.shimmerLayout)
    ShimmerFrameLayout shimmerLayout;
    @BindView(R.id.llshim)
    LinearLayout llshim;
    @BindView(R.id.lvAccount)
    ListView lvAccount;

//    @BindView(R.id.rlSort)
//    RelativeLayout rlSort;

//    @BindView(R.id.ivSelectSort)
//    ImageView ivSelectSort;

//    @BindView(R.id.rlFilter)
//    RelativeLayout rlFilter;

    private View view;
    private Context context;
    private AccountListAdapter adapter;
    private AccountListPresenter presenter;
    private DashboardPresenter dashboardPresenter;
    private List<AccountModel> list = new ArrayList<>();
    private String stringLatitude, stringLongitude;
    private CustomDialog dialogQuick;
    private String[] sort = {"Name A to Z", "Name Z to A", "Industry A to Z", "Industry Z to A"};
    private Toolbar mtoolbar;
    private BottomSheetBehavior mBottomSheetBehavior;
    RelativeLayout persistantState;
    private int prevSize = 0, sort_id = 0;
    private boolean isLoadMore = true;
    private boolean isMaxSize = true;
    private boolean pause = false;

    BottomSheetDialog dialog;

    public static AccountFrangment newInstance() {
        AccountFrangment fragment = new AccountFrangment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_account, container, false);
        this.context = getActivity();
        setHasOptionsMenu(true);
        return this.view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);
        this.context = getActivity();
        this.presenter = new AccountListPresenter(context, this);
        this.dashboardPresenter = new DashboardPresenter(context, this);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Account");


        initData();

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_list:
                MenuActivity.getInstance().setFragment(AccountFrangment.newInstance());
                item.setIcon(R.drawable.ic_list_white_24dp);
                break;
            case R.id.action_cards:
                MenuActivity.getInstance().setFragment(AccountCardFragmentViewPager.newInstance());
                item.setIcon(R.drawable.ic_cards_black_24dp);
                break;
//            case R.id.action_maps:
//                checkFunction = new CheckFunction(getActivity().getApplicationContext());
//                if(!checkFunction.userAccessControl(1)){
//                    Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
//                } else {
//                    MenuActivity.getInstance().setFragment(AccountMapStreesFragment.newInstance());
////                BottomDialogMaps bottomDialogMaps = new BottomDialogMaps();
////                bottomDialogMaps.show(getFragmentManager(),"bottomDialogMaps");
////                Intent intent = new Intent(getActivity().getApplication(),map.class);
////                startActivity(intent);
//                    item.setIcon(R.drawable.ic_map_white_24dp);
//                }
//                break;
            case R.id.action_filter:
                SortDialogFragment DialogFragment = new SortDialogFragment(context, new SortDialogCallback() {
                    @Override
                    public void onDialogCallback(Bundle data) {
                        sort_id = Integer.parseInt(data.getString("id"));

                        getDataFromAPI(0, sort_id);
//                            ivSelectSort.setVisibility(View.VISIBLE);
                    }
                }, sort);
                DialogFragment.show(getActivity().getSupportFragmentManager(), "sortDialog");
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    private void initData() {
        Sales1CRMUtils.Utils.imageLoader = new ImageLoader(context);
        etSearch.clearFocus();
        lvAccount.setOnScrollListener(new ListLazyLoad());

        lvAccountSwipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                llshim.setVisibility(View.VISIBLE);
                shimmerLayout.setVisibility(View.VISIBLE);
                shimmerLayout.startShimmer();
                getDataFromAPI(0, sort_id);
            }
        });

        //Set Seach Function
        setTextWatcherForSearch();
        getDataFromAPI(0, sort_id);

        ivClear.setOnClickListener(click);
        tvRefresh.setOnClickListener(click);
//        rlSort.setOnClickListener(click);
    }

    private void setTextWatcherForSearch() {
        etSearch.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    if (etSearch.getText().length() == 0) {
                        ivClear.setVisibility(View.VISIBLE);
                    }
//                } else {
//                    if (etSearch.getText().length()== 0)
//                    {
//                        ivClear.setVisibility(View.GONE);
//                    }else{
//                        ivClear.setVisibility(View.VISIBLE);
//                    }

                }
            }
        });

        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    llshim.setVisibility(View.VISIBLE);
                    shimmerLayout.setVisibility(View.VISIBLE);
                    shimmerLayout.startShimmer();
                    getDataFromAPI(0, sort_id);
                }

                return true;
            }
        });
    }

    private void getDataFromAPI(int prev, int sort_id) {
        isMaxSize = true;
        isLoadMore = true;
        llNotFound.setVisibility(View.GONE);

        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(context)) {
            GPSTracker gpsTracker = new GPSTracker(context);
            if (gpsTracker.canGetLocation()) {
                stringLatitude = String.valueOf(gpsTracker.latitude);
                stringLongitude = String.valueOf(gpsTracker.longitude);

                pause = false;

                dashboardPresenter.setupFirst(ApiParam.API_005);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        presenter.setupList(ApiParam.API_010, prev, sort_id, etSearch.getText().toString(), stringLatitude, stringLongitude);
                    }
                }, 1000);
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gpsTracker.showSettingsAlert();
                lvAccountSwipe.setRefreshing(false);


            }
        } else {

            lvAccountSwipe.setVisibility(View.GONE);
            rlNoConnection.setVisibility(View.VISIBLE);
        }
    }


    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.ivClear:
                    etSearch.setText("");
                    etSearch.clearFocus();
                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);
                    getDataFromAPI(0, sort_id);
                    break;
                case R.id.tvRefresh:
                    llshim.setVisibility(View.VISIBLE);
                    rlNoConnection.setVisibility(View.GONE);
                    getDataFromAPI(0, sort_id);
                    break;
//                case R.id.rlSort:

//                    break;
                default:
                    break;
            }
        }
    };


    private void setList() {
        llshim.setVisibility(View.VISIBLE);
        shimmerLayout.setVisibility(View.GONE);
        adapter = new AccountListAdapter(context, this.list);
        List<AccountListAdapter.Row> rows = new ArrayList<AccountListAdapter.Row>();
        for (AccountModel cartModel : list) {
            //Read List by Row to insert into List Adapter
            rows.add(new AccountListAdapter.Item(cartModel));
        }

        Log.d("TAG", "row " + list.size());

        //Set Row Adapter
        adapter.setRows(rows);
        lvAccount.setAdapter(adapter);
        lvAccount.setOnItemClickListener(new AdapterOnItemClick());
        lvAccount.deferNotifyDataSetChanged();
        llshim.setVisibility(View.INVISIBLE);
        shimmerLayout.stopShimmer();
        shimmerLayout.setVisibility(View.INVISIBLE);

    }

    private void setMoreList() {
        List<AccountListAdapter.Row> rows = new ArrayList<AccountListAdapter.Row>();
        for (AccountModel country : list) {
            // Add the country to the list
            rows.add(new AccountListAdapter.Item(country));
        }
        Log.d("TAG", "row more " + list.size());
        adapter.setRows(rows);
        adapter.notifyDataSetChanged();


    }

    @Override
    public void finishAccountList(String result, List<AccountModel> list) {

        lvAccountSwipe.setRefreshing(false);

//        rlProgress.setVisibility(View.GONE);
        if (result.equalsIgnoreCase("OK")) {
            prevSize = list.size();
            this.list = list;

            isMaxSize = false;
            isLoadMore = false;

            setList();
            if (list.size() > 0) {
                llNotFound.setVisibility(View.GONE);

            } else {
                llNotFound.setVisibility(View.VISIBLE);
            }
        } else if (result.equalsIgnoreCase("FAILED_KEY")) {
            startActivity(new Intent(context, ErrorAPIKeyActivity.class));
            MenuActivity.getInstance().finish();
        } else {
            rlNoConnection.setVisibility(View.VISIBLE);

        }
    }

    @Override
    public void finishAccountMoreList(String result, List<AccountModel> list) {

        if (result.equals("OK")) {
            for (int i = 0; i < list.size(); i++) {
                this.list.add(list.get(i));
            }

            if (this.prevSize == this.list.size())
                isMaxSize = true;
            this.prevSize = this.list.size();
            setMoreList();
        }
        this.isLoadMore = false;
    }

    @Override
    public void finishQuickCreate(String result) {
        if (result.equalsIgnoreCase("OK")) {
            dialogQuick.dismiss();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    @Override
    public void finishFirst(String result, DashboardFirstModel model) {
        if (result.equalsIgnoreCase("OK")) {
            if (model.getAccount_create_permission().equalsIgnoreCase("1")) {
                faAccountAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(context, AccountAddActivity.class));
                    }
                });
                faAccountAdd.setColorNormal(R.color.BlueCRM);
                faAccountAdd.setColorPressed(R.color.colorPrimaryDark);
                faAccountAdd.setColorPressedResId(R.color.colorPrimaryDark);
                faAccountAdd.setColorNormalResId(R.color.BlueCRM);

                faQuickAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogQuick = CustomDialog.setupDialogAccountAdd(context);
                        final CustomEditText etName = (CustomEditText) dialogQuick.findViewById(R.id.etAccountName);
                        final CustomEditText etPhone = (CustomEditText) dialogQuick.findViewById(R.id.etPhone);
                        CustomTextView tvYa = (CustomTextView) dialogQuick.findViewById(R.id.tvOK);
                        CustomTextView tvTidak = (CustomTextView) dialogQuick.findViewById(R.id.tvBack);

                        tvYa.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                AccountModel model = new AccountModel();
                                model.setAccount_name(etName.getText().toString());
                                model.setPhone(etPhone.getText().toString());

                                presenter.setupCreate(ApiParam.API_013, model);
                            }
                        });
                        tvTidak.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialogQuick.dismiss();
                            }
                        });
                        dialogQuick.show();
                    }
                });

                faQuickAdd.setColorNormal(R.color.BlueCRM);
                faQuickAdd.setColorPressed(R.color.colorPrimaryDark);
                faQuickAdd.setColorPressedResId(R.color.colorPrimaryDark);
                faQuickAdd.setColorNormalResId(R.color.BlueCRM);

            } else {
                faAccountAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
                    }
                });
                faAccountAdd.setColorNormal(R.color.BlueCRM_light);
                faAccountAdd.setColorPressed(R.color.colorPrimaryDark_light);
                faAccountAdd.setColorPressedResId(R.color.colorPrimaryDark_light);
                faAccountAdd.setColorNormalResId(R.color.BlueCRM_light);

                faQuickAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
                    }
                });
                faQuickAdd.setColorNormal(R.color.BlueCRM_light);
                faQuickAdd.setColorPressed(R.color.colorPrimaryDark_light);
                faQuickAdd.setColorPressedResId(R.color.colorPrimaryDark_light);
                faQuickAdd.setColorNormalResId(R.color.BlueCRM_light);
            }
        }
    }

    @Override
    public void finishSecond(String result, DashboardSecondModel model) {

    }

    @Override
    public void finishThird(String result, List<DashboardThirdModel> listLead, List<DashboardThirdModel> listLead1, List<DashboardThirdModel> listOppor, List<DashboardThirdModel> listOppor1) {

    }

    @Override
    public void TotOpportunity(String result, TotalOpportunityModel totOporModel) {

    }

    @Override
    public void absence(String result, AbsenceModel model) {

    }


    private class ListLazyLoad implements AbsListView.OnScrollListener {

        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {
        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            if (firstVisibleItem + visibleItemCount == totalItemCount && totalItemCount != 0) {

//                Log.d("TAG", "isLoadMore : " + isLoadMore);
                if (isLoadMore == false && isMaxSize == false) {
                    isLoadMore = true;
//                    prevSize = list.size();

                    presenter.setupMoreList(ApiParam.API_010, prevSize, sort_id, etSearch.getText().toString(), stringLatitude, stringLongitude);

                }
            }
        }
    }

    private class AdapterOnItemClick implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            AccountListAdapter.Item item = (AccountListAdapter.Item) lvAccount.getAdapter().getItem(position);
            switch (view.getId()) {
                default:
                    Intent intent = new Intent(context, AccountDetailActivity.class);
                    intent.putExtra("accountModel", item.getItem());
                    startActivity(intent);
                    break;
            }
        }
    }

    @Override
    public void onPause() {
        pause = true;
        super.onPause();
    }

    @Override
    public void onResume() {
        if (pause) {
            getDataFromAPI(0, sort_id);
        }
        super.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
        shimmerLayout.stopShimmer();
    }




}