package com.msf.project.sales1crm.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.model.ProductModel;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by christianmacbook on 22/05/18.
 */

public class ProductSimpleAdapter extends RecyclerView.Adapter<ProductSimpleAdapter.ViewHolder> {
    
    //Vars
    private List<ProductModel> list = new ArrayList<>();
    private Context context;

    private Locale localeID = new Locale("in", "ID");
    private NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);

    public ProductSimpleAdapter(Context context, List<ProductModel> object) {
        this.context = context;
        this.list = object;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_simpleproduct, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tvName.setText(list.get(position).getName());
        holder.tvNote.setText(list.get(position).getNote());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CustomTextView tvName, tvNote;

        public ViewHolder(View itemView) {
            super(itemView);

            tvName = itemView.findViewById(R.id.tvProductName);
            tvNote = itemView.findViewById(R.id.tvNote);
        }
    }
}
