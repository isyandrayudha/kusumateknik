package com.msf.project.sales1crm.presenter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;

import com.msf.project.sales1crm.callback.LoginCallback;
import com.msf.project.sales1crm.model.UserModel;
import com.msf.project.sales1crm.service.NetworkConnection;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.PreferenceUtility;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;
import com.msf.project.sales1crm.utility.Sales1CRMUtilsJSON;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by christianmacbook on 23/05/18.
 */

public class SplashPresenter {
    private final String TAG = SplashPresenter.class.getSimpleName();
    private Context context;
    private Messenger messenger;
    private Message message;
    private Bundle bundle;
    private Handler handler;
    private String failureResponse = "";
    private String[] stringResponse = {""};
    private LoginCallback loginCallback;
    private String resultLogin = "NG";
    private String response = "Tolong Periksa Koneksi Anda";

    public SplashPresenter(Context context, LoginCallback listener) {
        this.context = context;
        this.loginCallback = listener;
    }

    private void doNetworkService(String url, String params, String from) {
        Log.d(TAG, "url login " + url);
        Log.d(TAG, "params : " + params);
        Intent networkIntent = new Intent(this.context, NetworkConnection.class);
        this.messenger = new Messenger(this.handler);
        networkIntent.putExtra("messenger", this.messenger);
        networkIntent.putExtra("url", url);
        networkIntent.putExtra("params", params);
        networkIntent.putExtra("from", from);
        this.context.startService(networkIntent);
    }

    public void setupSplash(int apiIndex, UserModel userModel) {
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)) {
            obtainSplash(apiIndex, userModel);
        }else{
            loginCallback.finishedLogin(this.resultLogin, this.response);
        }
    }

    public void obtainSplash(int apiIndex, UserModel userModel) {
        this.handler = new Handler(this.context.getMainLooper()) {

            @Override
            public void handleMessage(Message msg) {
                parseSplashItem(msg);
            }

        };

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("email", userModel.getEmail());
            jsonObject.put("password", userModel.getPassword());
            jsonObject.put("device_token", PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.PREFERENCES_PROPERTY_REG_ID));
            jsonObject.put("device_type", "2");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        doNetworkService(ApiParam.urlBuilder(this.context, apiIndex), jsonObject.toString(), "LoginToServer");
    }

    private void parseSplashItem(Message message) {
        this.message = message;
        this.bundle = this.message.getData();
        this.stringResponse[0] = this.bundle.getString("network_response");
        this.failureResponse = this.bundle.getString("network_failure");
        Log.d(TAG, "responseString[0] login info " + this.stringResponse[0]);
        if (this.failureResponse.equalsIgnoreCase("yes")){
            this.loginCallback.finishedLogin(this.resultLogin, this.response);
        }else{
            try {
                this.resultLogin = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                if (this.resultLogin.equalsIgnoreCase("OK")){
                    //Get API Key FROM Server
                    Sales1CRMUtilsJSON.JSONUtility.getProfileUser(context, this.stringResponse[0]);
                }else{
                    this.response = Sales1CRMUtilsJSON.JSONUtility.getResponseFromServer(this.stringResponse[0]);
                }
                this.loginCallback.finishedLogin(this.resultLogin, this.response);
            } catch (JSONException e) {
                Log.d(TAG, "Exception detail Response " + e.getMessage());
            }
        }
    }
}
