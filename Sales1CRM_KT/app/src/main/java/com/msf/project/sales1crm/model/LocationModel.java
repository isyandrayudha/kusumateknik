package com.msf.project.sales1crm.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

/**
 * Created by christianmacbook on 25/05/18.
 */

public class LocationModel implements Comparable, Parcelable {
    private String id;
    private String name;
    private String kodepos;

    public LocationModel() {
        setId("");
        setName("");
        setKodepos("");
    }

    public LocationModel(Parcel in) {
        readFromParcel(in);
    }

    public void readFromParcel(Parcel in) {
        id = in.readString();
        name = in.readString();
        kodepos = in.readString();
    }

    public static final Creator<LocationModel> CREATOR = new Creator<LocationModel>() {
        @Override
        public LocationModel createFromParcel(Parcel in) {
            return new LocationModel(in);
        }

        @Override
        public LocationModel[] newArray(int size) {
            return new LocationModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public String getKodepos() { return kodepos; }

    public void setKodepos(String kodepos) { this.kodepos = kodepos; }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(kodepos);
    }

    @Override
    public int compareTo(@NonNull Object o) {
        return this.getName().compareToIgnoreCase(
                ((LocationModel) o).getName());
    }
}
