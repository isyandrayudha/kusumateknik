package com.msf.project.sales1crm.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.msf.project.sales1crm.ErrorAPIKeyActivity;
import com.msf.project.sales1crm.MenuActivity;
import com.msf.project.sales1crm.OpportunityAddActivity;
import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.adapter.OpportunityListCardAdapter;
import com.msf.project.sales1crm.callback.DashboardCallback;
import com.msf.project.sales1crm.callback.OpportunityListActiveCallback;
import com.msf.project.sales1crm.customview.CustomEditText;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.imageloader.ImageLoader;
import com.msf.project.sales1crm.model.AbsenceModel;
import com.msf.project.sales1crm.model.DashboardFirstModel;
import com.msf.project.sales1crm.model.DashboardSecondModel;
import com.msf.project.sales1crm.model.DashboardThirdModel;
import com.msf.project.sales1crm.model.OpportunityModel;
import com.msf.project.sales1crm.model.TotalOpportunityModel;
import com.msf.project.sales1crm.presenter.DashboardPresenter;
import com.msf.project.sales1crm.presenter.OpportunityListActivePresenter;
import com.msf.project.sales1crm.service.GPSTracker;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class OpportunityActiveCardFragment extends Fragment implements OpportunityListActiveCallback, OpportunityTabCardFragment.ClickActionFilterListener, DashboardCallback {

    @BindView(R.id.etSearch)
    CustomEditText etSearch;
    @BindView(R.id.ivClear)
    ImageView ivClear;
    @BindView(R.id.tvRefresh)
    CustomTextView tvRefresh;
    @BindView(R.id.rlNoConnection)
    RelativeLayout rlNoConnection;
    @BindView(R.id.llNotFound)
    LinearLayout llNotFound;
//    @BindView(R.id.rlProgress)
//    RelativeLayout rlProgress;
    @BindView(R.id.lvOpportunitySwipe)
    SwipeRefreshLayout lvOpportunitySwipe;
    @BindView(R.id.faOpportunityAdd)
    FloatingActionButton faOpportunityAdd;
    @BindView(R.id.menu)
    FloatingActionMenu menu;
    @BindView(R.id.rlLeadList)
    RelativeLayout rlLeadList;
    Unbinder unbinder;
    @BindView(R.id.lvOpportunity)
    RecyclerView lvOpportunity;
    @BindView(R.id.shimmerLayout)
    ShimmerFrameLayout shimmerLayout;
    @BindView(R.id.llshim)
    LinearLayout llshim;

    private View view;
    private Context context;
    private OpportunityListActivePresenter presenter;
    private OpportunityListCardAdapter adapter;
    private DashboardPresenter dashboardPresenter;
    private List<OpportunityModel> list = new ArrayList<>();
    private String[] sort = {"Name A to Z", "Name Z to A", "Opportunity Size A to Z", "Opportunity Size Z to A" ,
            "Create Date Ascending", "Create Date Descending", "Due Date Ascending", "Due Date Descending"};

    private int prevSize = 0, sort_id = 0;
    private boolean isLoadMore = true;
    private boolean isMaxSize = true;
    private boolean pause = false;


    public static OpportunityActiveCardFragment newInstance() {
        OpportunityActiveCardFragment fragment = new OpportunityActiveCardFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_opportunityactivecard, container, false);
        ButterKnife.bind(this,view);
        this.context = getActivity();
        this.dashboardPresenter = new DashboardPresenter(context, this);
        this.presenter = new OpportunityListActivePresenter(context, this);
        return this.view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        this.context = getActivity();
        this.dashboardPresenter = new DashboardPresenter(context, this);
        this.presenter = new OpportunityListActivePresenter(context, this);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Opportunity");

        initData();
    }

    private void initData() {
        Sales1CRMUtils.Utils.imageLoader = new ImageLoader(context);
        etSearch.clearFocus();
//        lvOpportunity.setOnScrollChangeListener(new OpportunityCloseCardFragment);
        lvOpportunitySwipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                llshim.setVisibility(View.VISIBLE);
                shimmerLayout.setVisibility(View.VISIBLE);
                shimmerLayout.startShimmer();
                getDataFromAPI(0, 0);
            }
        });


        //Set Seach Function
        setTextWatcherForSearch();
        getDataFromAPI(0, 0);

        ivClear.setOnClickListener(click);
        tvRefresh.setOnClickListener(click);
    }

    private void setTextWatcherForSearch() {
        etSearch.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    if (etSearch.getText().length() == 0) {
                        ivClear.setVisibility(View.VISIBLE);
                    }
                } else {
                    if (etSearch.getText().length() == 0) {
                        ivClear.setVisibility(View.GONE);
                    } else {
                        ivClear.setVisibility(View.VISIBLE);
                    }

                }
            }
        });

        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    llshim.setVisibility(View.VISIBLE);
                    shimmerLayout.setVisibility(View.VISIBLE);
                    shimmerLayout.startShimmer();
                    getDataFromAPI(0, 0);
                }

                return true;
            }
        });
    }

    public void getDataFromAPI(int prev, int sort_id) {
        isMaxSize = true;
        isLoadMore = true;

        llNotFound.setVisibility(View.GONE);

        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(context)) {
            GPSTracker gpsTracker = new GPSTracker(context);
            if (gpsTracker.canGetLocation()) {
                dashboardPresenter.setupFirst(ApiParam.API_005);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        presenter.setupList(ApiParam.API_040, prev, sort_id, etSearch.getText().toString());
                    }
                },1000);
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gpsTracker.showSettingsAlert();
                lvOpportunitySwipe.setRefreshing(false);
            }
        } else {
            lvOpportunitySwipe.setVisibility(View.GONE);
            rlNoConnection.setVisibility(View.VISIBLE);
        }
    }


    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.ivClear:
                    etSearch.setText("");
                    etSearch.clearFocus();
                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);
                    getDataFromAPI(0, 0);
                    break;
                case R.id.tvRefresh:
                    llshim.setVisibility(View.VISIBLE);
                    rlNoConnection.setVisibility(View.GONE);
                    getDataFromAPI(0, 0);
                    break;
                default:
                    break;
            }
        }
    };

    private void setList() {
        llshim.setVisibility(View.VISIBLE);
        shimmerLayout.setVisibility(View.GONE);
        adapter = new OpportunityListCardAdapter(context, this.list);
        List<OpportunityListCardAdapter.Row> rows = new ArrayList<OpportunityListCardAdapter.Row>();
        for (OpportunityModel cartModel : list) {
            rows.add(new OpportunityListCardAdapter.Item(cartModel));
        }

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        lvOpportunity.setLayoutManager(layoutManager);
        adapter.setRows(rows);
        lvOpportunity.setAdapter(adapter);
        llshim.setVisibility(View.INVISIBLE);
        shimmerLayout.stopShimmer();
        shimmerLayout.setVisibility(View.INVISIBLE);
    }

    private void setMoreList() {
        List<OpportunityListCardAdapter.Row> rows = new ArrayList<OpportunityListCardAdapter.Row>();
        for (OpportunityModel country : list) {
            rows.add(new OpportunityListCardAdapter.Item(country));
        }
        adapter.setRows(rows);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void finishOpporList(String result, List<OpportunityModel> list) {
        lvOpportunitySwipe.setRefreshing(false);
//        rlProgress.setVisibility(View.GONE);
        if (result.equalsIgnoreCase("OK")) {
            prevSize = list.size();
            this.list = list;

            isMaxSize = false;
            isLoadMore = false;

            setList();
            if (list.size() > 0) {
                llNotFound.setVisibility(View.GONE);
            } else {
                llNotFound.setVisibility(View.VISIBLE);
            }
        } else if (result.equalsIgnoreCase("FAILED_KEY")) {
            startActivity(new Intent(context, ErrorAPIKeyActivity.class));
            MenuActivity.getInstance().finish();
        } else {
//            lvOpportunitySwipe.setVisibility(View.GONE);
            rlNoConnection.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void finishOpporMoreList(String result, List<OpportunityModel> list) {
        if (result.equalsIgnoreCase("OK")) {
            for (int i = 0; i < list.size(); i++) {
                this.list.add(list.get(i));
            }

            if (this.prevSize == this.list.size())
                isMaxSize = true;
            this.prevSize = this.list.size();
            setMoreList();
        }
        this.isLoadMore = false;

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onPause() {
        pause = true;
        super.onPause();
    }

    @Override
    public void onResume() {
        if (pause) {
            getDataFromAPI(0, 0);
        }
        super.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
//        shimmerLayout.stopShimmer();
    }

    @Override
    public void onClickItemFilter(Integer id) {
       getDataFromAPI(0,id);
    }

    @Override
    public void finishFirst(String result, DashboardFirstModel model) {
        if(model.getOpportunity_create_permission().equalsIgnoreCase("1")){
            faOpportunityAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(context, OpportunityAddActivity.class));
                }
            });
            faOpportunityAdd.setColorNormal(R.color.BlueCRM);
            faOpportunityAdd.setColorPressed(R.color.colorPrimaryDark);
            faOpportunityAdd.setColorPressedResId(R.color.colorPrimaryDark);
            faOpportunityAdd.setColorNormalResId(R.color.BlueCRM);
        } else {
            faOpportunityAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
                }
            });
            faOpportunityAdd.setColorNormal(R.color.BlueCRM_light);
            faOpportunityAdd.setColorPressed(R.color.colorPrimaryDark_light);
            faOpportunityAdd.setColorPressedResId(R.color.colorPrimaryDark_light);
            faOpportunityAdd.setColorNormalResId(R.color.BlueCRM_light);
        }
    }

    @Override
    public void finishSecond(String result, DashboardSecondModel model) {

    }

    @Override
    public void finishThird(String result, List<DashboardThirdModel> listLead, List<DashboardThirdModel> listLead1, List<DashboardThirdModel> listOppor, List<DashboardThirdModel> listOppor1) {

    }

    @Override
    public void TotOpportunity(String result, TotalOpportunityModel totOporModel) {

    }

    @Override
    public void absence(String result, AbsenceModel model) {

    }
}
