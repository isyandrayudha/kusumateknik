package com.msf.project.sales1crm.utility;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.RequestBody;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by christianmacbook on 11/05/18.
 */

public class Sales1CRMParams {
    public static class PostParamUtility {
        public static RequestBody getAPIKeyPostParam(String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);

            RequestBody formBody = new FormEncodingBuilder()
                    .add("api_key", jsonObject.getString("api_key"))
                    .build();

            return formBody;
        }

        public static RequestBody getDomainCheckParam(String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);

            RequestBody formBody = new FormEncodingBuilder()
                    .add("keyword", jsonObject.getString("keyword"))
                    .build();

            return formBody;
        }

        public static RequestBody getLoginPostParam(String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);

            RequestBody formBody = new FormEncodingBuilder()
                    .add("email", jsonObject.getString("email"))
                    .add("password", jsonObject.getString("password"))
                    .add("device_token", jsonObject.getString("device_token"))
                    .add("device_type", jsonObject.getString("device_type"))
                    .build();

            return formBody;
        }

        public static RequestBody getAccountListParam(String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);

            RequestBody formBody = new FormEncodingBuilder()
                    .add("api_key", jsonObject.getString("api_key"))
                    .add("offset", jsonObject.getString("offset"))
                    .add("sort", jsonObject.getString("sort"))
                    .add("search", jsonObject.getString("search"))
                    .add("latitude", jsonObject.getString("latitude"))
                    .add("longitude", jsonObject.getString("longitude"))
                    .build();

            return formBody;
        }

        public static RequestBody getRelatedPeople(String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);
            RequestBody formBody = new FormEncodingBuilder()
                    .add("api_key", jsonObject.getString("api_key"))
                    .add("account_id", jsonObject.getString("account_id"))
                    .build();
            return formBody;
        }

        public static RequestBody getAccountAddParam(String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);

            RequestBody formBody = new FormEncodingBuilder()
                    .add("api_key", jsonObject.getString("api_key"))
                    .add("assign_to", jsonObject.getString("assign_to"))
                    .add("type", jsonObject.getString("type"))
                    .add("industry", jsonObject.getString("industry"))
                    .add("create_date", jsonObject.getString("create_date"))
                    .add("name", jsonObject.getString("name"))
                    .add("description", jsonObject.getString("description"))
                    .add("email", jsonObject.getString("email"))
                    .add("website", jsonObject.getString("website"))
                    .add("total_employee", jsonObject.getString("total_employee"))
                    .add("phone", jsonObject.getString("phone"))
                    .add("image_master", jsonObject.getString("image_master"))
                    .add("billing_alamat", jsonObject.getString("billing_alamat"))
                    .add("billing_provinsi", jsonObject.getString("billing_provinsi"))
                    .add("billing_kabupaten", jsonObject.getString("billing_kabupaten"))
                    .add("billing_kecamatan", jsonObject.getString("billing_kecamatan"))
                    .add("billing_kelurahan", jsonObject.getString("billing_kelurahan"))
                    .add("billing_latitude", jsonObject.getString("billing_latitude"))
                    .add("billing_longitude", jsonObject.getString("billing_longitude"))
                    .add("shipping_alamat", jsonObject.getString("shipping_alamat"))
                    .add("shipping_provinsi", jsonObject.getString("shipping_provinsi"))
                    .add("shipping_kabupaten", jsonObject.getString("shipping_kabupaten"))
                    .add("shipping_kecamatan", jsonObject.getString("shipping_kecamatan"))
                    .add("shipping_kelurahan", jsonObject.getString("shipping_kelurahan"))
                    .add("shipping_latitude", jsonObject.getString("shipping_latitude"))
                    .add("shipping_longitude", jsonObject.getString("shipping_longitude"))
                    .build();

            return formBody;
        }

        public static RequestBody getAccountQuickParam(String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);

            RequestBody formBody = new FormEncodingBuilder()
                    .add("api_key", jsonObject.getString("api_key"))
                    .add("name", jsonObject.getString("name"))
                    .add("phone", jsonObject.getString("phone"))
                    .build();

            return formBody;
        }

        public static RequestBody getAccountEditParam(String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);

            RequestBody formBody = new FormEncodingBuilder()
                    .add("api_key", jsonObject.getString("api_key"))
                    .add("account_id", jsonObject.getString("account_id"))
                    .add("assign_to", jsonObject.getString("assign_to"))
                    .add("type", jsonObject.getString("type"))
                    .add("industry", jsonObject.getString("industry"))
                    .add("create_date", jsonObject.getString("create_date"))
                    .add("name", jsonObject.getString("name"))
                    .add("description", jsonObject.getString("description"))
                    .add("email", jsonObject.getString("email"))
                    .add("website", jsonObject.getString("website"))
                    .add("total_employee", jsonObject.getString("total_employee"))
                    .add("phone", jsonObject.getString("phone"))
                    .add("image_master", jsonObject.getString("image_master"))
                    .add("billing_alamat", jsonObject.getString("billing_alamat"))
                    .add("billing_provinsi", jsonObject.getString("billing_provinsi"))
                    .add("billing_kabupaten", jsonObject.getString("billing_kabupaten"))
                    .add("billing_kecamatan", jsonObject.getString("billing_kecamatan"))
                    .add("billing_kelurahan", jsonObject.getString("billing_kelurahan"))
                    .add("billing_latitude", jsonObject.getString("billing_latitude"))
                    .add("billing_longitude", jsonObject.getString("billing_longitude"))
                    .add("shipping_alamat", jsonObject.getString("shipping_alamat"))
                    .add("shipping_provinsi", jsonObject.getString("shipping_provinsi"))
                    .add("shipping_kabupaten", jsonObject.getString("shipping_kabupaten"))
                    .add("shipping_kecamatan", jsonObject.getString("shipping_kecamatan"))
                    .add("shipping_kelurahan", jsonObject.getString("shipping_kelurahan"))
                    .add("shipping_latitude", jsonObject.getString("shipping_latitude"))
                    .add("shipping_longitude", jsonObject.getString("shipping_longitude"))
                    .build();

            return formBody;
        }

        public static RequestBody getLeadAddParams(String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);

            RequestBody formBody = new FormEncodingBuilder()
                    .add("api_key", jsonObject.getString("api_key"))
                    .add("assign_to", jsonObject.getString("assign_to"))
                    .add("title", jsonObject.getString("title"))
                    .add("first_name", jsonObject.getString("first_name"))
                    .add("last_name", jsonObject.getString("last_name"))
                    .add("company", jsonObject.getString("company"))
                    .add("position", jsonObject.getString("position"))
                    .add("description", jsonObject.getString("description"))
                    .add("email", jsonObject.getString("email"))
                    .add("total_employee", jsonObject.getString("total_employee"))
                    .add("phone", jsonObject.getString("phone"))
                    .add("image_master", jsonObject.getString("image_master"))
                    .add("industry", jsonObject.getString("industry"))
                    .add("lead_stage", jsonObject.getString("lead_stage"))
                    .add("lead_source", jsonObject.getString("lead_source"))
                    .add("street", jsonObject.getString("street"))
                    .add("provinsi", jsonObject.getString("provinsi"))
                    .add("kabupaten", jsonObject.getString("kabupaten"))
                    .add("kecamatan", jsonObject.getString("kecamatan"))
                    .add("kelurahan", jsonObject.getString("kelurahan"))
                    .add("latitude", jsonObject.getString("latitude"))
                    .add("longitude", jsonObject.getString("longitude"))
                    .build();

            return formBody;
        }

        public static RequestBody getLeadEditParams(String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);

            RequestBody formBody = new FormEncodingBuilder()
                    .add("api_key", jsonObject.getString("api_key"))
                    .add("lead_id", jsonObject.getString("lead_id"))
                    .add("assign_to", jsonObject.getString("assign_to"))
                    .add("title", jsonObject.getString("title"))
                    .add("first_name", jsonObject.getString("first_name"))
                    .add("last_name", jsonObject.getString("last_name"))
                    .add("company", jsonObject.getString("company"))
                    .add("position", jsonObject.getString("position"))
                    .add("description", jsonObject.getString("description"))
                    .add("email", jsonObject.getString("email"))
                    .add("total_employee", jsonObject.getString("total_employee"))
                    .add("phone", jsonObject.getString("phone"))
                    .add("image_master", jsonObject.getString("image_master"))
                    .add("industry", jsonObject.getString("industry"))
                    .add("lead_stage", jsonObject.getString("lead_stage"))
                    .add("lead_source", jsonObject.getString("lead_source"))
                    .add("street", jsonObject.getString("street"))
                    .add("provinsi", jsonObject.getString("provinsi"))
                    .add("kabupaten", jsonObject.getString("kabupaten"))
                    .add("kecamatan", jsonObject.getString("kecamatan"))
                    .add("kelurahan", jsonObject.getString("kelurahan"))
                    .add("latitude", jsonObject.getString("latitude"))
                    .add("longitude", jsonObject.getString("longitude"))
                    .build();

            return formBody;
        }

        public static RequestBody getOpportunityListParam(String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);

            RequestBody formBody = new FormEncodingBuilder()
                    .add("api_key", jsonObject.getString("api_key"))
                    .add("offset", jsonObject.getString("offset"))
                    .add("sort", jsonObject.getString("sort"))
                    .add("search", jsonObject.getString("search"))
                    .build();

            return formBody;
        }

        public static RequestBody getOpportunityListByNone(String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);

            RequestBody formBody = new FormEncodingBuilder()
                    .add("api_key", jsonObject.getString("api_key"))
                    .add("offset", jsonObject.getString("offset"))
                    .add("stage_id", jsonObject.getString("stage_id"))
                    .build();

            return formBody;
        }

        public static RequestBody getOpportunityListByMQL(String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);

            RequestBody formBody = new FormEncodingBuilder()
                    .add("api_key", jsonObject.getString("api_key"))
                    .add("offset", jsonObject.getString("offset"))
                    .add("stage_id", jsonObject.getString("stage_id"))
                    .build();

            return formBody;
        }

        public static RequestBody getVp(String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);

            RequestBody formBody = new FormEncodingBuilder()
                    .add("api_key", jsonObject.getString("api_key"))
                    .add("offset", jsonObject.getString("offset"))
                    .add("sort", jsonObject.getString("sort"))
                    .add("search",jsonObject.getString("search"))
                    .build();

            return formBody;
        }

        public static RequestBody getDBmachineList(String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);
            RequestBody formBody = new FormEncodingBuilder()
                    .add("api_key", jsonObject.getString("api_key"))
                    .add("offset", jsonObject.getString("offset"))
                    .add("search", jsonObject.getString("search"))
                    .build();
            return formBody;
        }

        public static RequestBody getOpportunityClose(String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);

            RequestBody formBody = new FormEncodingBuilder()
                    .add("api_key", jsonObject.getString("api_key"))
                    .add("offset", jsonObject.getString("offset"))
                    .build();

            return formBody;
        }

        public static RequestBody getOpportunityAddParam(String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);

            RequestBody formBody = new FormEncodingBuilder()
                    .add("api_key", jsonObject.getString("api_key"))
                    .add("account_id", jsonObject.getString("account_id"))
                    .add("name", jsonObject.getString("name"))
                    .add("type", jsonObject.getString("type"))
                    .add("stage", jsonObject.getString("stage"))
                    .add("probability", jsonObject.getString("probability"))
                    .add("opportunity_size", jsonObject.getString("opportunity_size"))
                    .add("next_step", jsonObject.getString("next_step"))
                    .add("lead_source", jsonObject.getString("lead_source"))
                    .add("contact_id", jsonObject.getString("contact_id"))
                    .add("description", jsonObject.getString("description"))
                    .add("opportunity_status", jsonObject.getString("opportunity_status"))
                    .add("close_date", jsonObject.getString("close_date"))
                    .add("create_date", jsonObject.getString("create_date"))
                    .add("product", jsonObject.getString("product"))
                    .add("assign_to", jsonObject.getString("assign_to"))
                    .add("category", jsonObject.getString("category"))
                    .add("industry_id", jsonObject.getString("industry_id"))
                    .add("reason_not_deal", jsonObject.getString("reason_not_deal"))
                    .build();
            return formBody;
        }

        public static RequestBody getOpportunityEditParam(String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);

            RequestBody formBody = new FormEncodingBuilder()
                    .add("api_key", jsonObject.getString("api_key"))
                    .add("opportunity_id", jsonObject.getString("opportunity_id"))
                    .add("account_id", jsonObject.getString("account_id"))
                    .add("name", jsonObject.getString("name"))
                    .add("type", jsonObject.getString("type"))
                    .add("stage", jsonObject.getString("stage"))
                    .add("probability", jsonObject.getString("probability"))
                    .add("opportunity_size", jsonObject.getString("opportunity_size"))
                    .add("next_step", jsonObject.getString("next_step"))
                    .add("lead_source", jsonObject.getString("lead_source"))
                    .add("contact_id", jsonObject.getString("contact_id"))
                    .add("description", jsonObject.getString("description"))
                    .add("opportunity_status", jsonObject.getString("opportunity_status"))
                    .add("close_date", jsonObject.getString("close_date"))
                    .add("update_date", jsonObject.getString("update_date"))
                    .add("product", jsonObject.getString("product"))
                    .add("assign_to", jsonObject.getString("assign_to"))
                    .add("category", jsonObject.getString("category"))
                    .add("industry_id", jsonObject.getString("industry_id"))
                    .add("actual_opportunity_size", jsonObject.getString("actual_opportunity_size"))
                    .add("final_quotation_id", jsonObject.getString("final_quotation_id"))
                    .add("reason_not_deal", jsonObject.getString("reason_not_deal"))
                    .build();
            return formBody;
        }

        public static RequestBody getProductList(String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);

            RequestBody formBody = new FormEncodingBuilder()
                    .add("api_key", jsonObject.getString("api_key"))
                    .add("offset", jsonObject.getString("offset"))
                    .add("search", jsonObject.getString("search"))
                    .add("currency_id", jsonObject.getString("currency_id"))
                    .build();

            return formBody;
        }

        public static RequestBody getOppDetail(String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);

            RequestBody formBody = new FormEncodingBuilder()
                    .add("api_key", jsonObject.getString("api_key"))
                    .add("opportunity_id", jsonObject.getString("opportunity_id"))
                    .build();

            return formBody;
        }

        public static RequestBody getConvert(String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);
            RequestBody formBody = new FormEncodingBuilder()
                    .add("api_key", jsonObject.getString("api_key"))
                    .add("id", jsonObject.getString("id"))
                    .add("convert_status", jsonObject.getString("convert_status"))
                    .build();
            return formBody;
        }

        public static RequestBody getPrice(String json) throws JSONException{
            JSONObject jsonObject = new JSONObject(json);
            RequestBody formBody = new FormEncodingBuilder()
                    .add("api_key", jsonObject.getString("api_key"))
                    .add("product_detail_id",jsonObject.getString("product_detail_id"))
                    .add("currency_id", jsonObject.getString("currency_id"))
                    .build();
            return formBody;
        }

        public static RequestBody getQoutation(String json) throws JSONException{
            JSONObject jsonObject = new JSONObject(json);
            RequestBody formBody = new FormEncodingBuilder()
                    .add("api_key", jsonObject.getString("api_key"))
                    .add("opportunity_id", jsonObject.getString("opportunity_id"))
                    .build();
            return formBody;
        }

        public static RequestBody getEventList(String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);

            RequestBody formBody = new FormEncodingBuilder()
                    .add("api_key", jsonObject.getString("api_key"))
                    .add("offset", jsonObject.getString("offset"))
                    .add("search", jsonObject.getString("search"))
                    .add("date", jsonObject.getString("date"))
                    .add("account_id", jsonObject.getString("account_id"))
                    .build();

            return formBody;
        }

        public static RequestBody getUploadImage(String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);

            RequestBody formBody = new FormEncodingBuilder()
                    .add("api_key", jsonObject.getString("api_key"))
                    .add("photo", jsonObject.getString("photo"))
                    .add("name", jsonObject.getString("name"))
                    .build();

            return formBody;
        }

        public static RequestBody getEventAdd(String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);

            FormEncodingBuilder formBuilder = new FormEncodingBuilder();
            formBuilder.add("api_key", jsonObject.getString("api_key"));
            formBuilder.add("event_id", jsonObject.getString("event_id"));
            formBuilder.add("id_product_event", jsonObject.getString("id_product_event"));
            formBuilder.add("product_status", jsonObject.getString("product_status"));
            formBuilder.add("account_id", jsonObject.getString("account_id"));
            formBuilder.add("assign_to", jsonObject.getString("assign_to"));
            formBuilder.add("date", jsonObject.getString("date"));
            formBuilder.add("name", jsonObject.getString("name"));
            formBuilder.add("description", jsonObject.getString("description"));
            formBuilder.add("follow_up_action", jsonObject.getString("follow_up_action"));

            if (jsonObject.has("nama_photo_satu")) {
                formBuilder.add("nama_photo_satu", jsonObject.getString("nama_photo_satu"));
                formBuilder.add("photo_1", jsonObject.getString("photo_1"));
            }
            if (jsonObject.has("nama_photo_dua")) {
                formBuilder.add("nama_photo_dua", jsonObject.getString("nama_photo_dua"));
                formBuilder.add("photo_2", jsonObject.getString("photo_2"));
            }
            if (jsonObject.has("nama_photo_tiga")) {
                formBuilder.add("nama_photo_tiga", jsonObject.getString("nama_photo_tiga"));
                formBuilder.add("photo_3", jsonObject.getString("photo_3"));
            }
            formBuilder.add("meet_to", jsonObject.getString("meet_to"));
            formBuilder.add("created_at", jsonObject.getString("created_at"));
            formBuilder.add("brand", jsonObject.getString("brand"));
            formBuilder.add("type", jsonObject.getString("type"));
            formBuilder.add("serial_number", jsonObject.getString("serial_number"));
            formBuilder.add("condition", jsonObject.getString("condition"));
            RequestBody formBody = formBuilder.build();

            return formBody;
        }

        public static RequestBody getEventEdit(String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);

            FormEncodingBuilder formBuilder = new FormEncodingBuilder();
            formBuilder.add("api_key", jsonObject.getString("api_key"));
            formBuilder.add("event_id", jsonObject.getString("event_id"));
            formBuilder.add("id_product_event", jsonObject.getString("id_product_event"));
            formBuilder.add("product_status", jsonObject.getString("product_status"));
            formBuilder.add("account_id", jsonObject.getString("account_id"));
            formBuilder.add("assign_to", jsonObject.getString("assign_to"));
            formBuilder.add("date", jsonObject.getString("date"));
            formBuilder.add("name", jsonObject.getString("name"));
            formBuilder.add("description", jsonObject.getString("description"));
            formBuilder.add("follow_up_action", jsonObject.getString("follow_up_action"));

            if (jsonObject.has("nama_photo_satu")) {
                formBuilder.add("nama_photo_satu", jsonObject.getString("nama_photo_satu"));
                formBuilder.add("photo_1", jsonObject.getString("photo_1"));
            }
            if (jsonObject.has("nama_photo_dua")) {
                formBuilder.add("nama_photo_dua", jsonObject.getString("nama_photo_dua"));
                formBuilder.add("photo_2", jsonObject.getString("photo_2"));
            }
            if (jsonObject.has("nama_photo_tiga")) {
                formBuilder.add("nama_photo_tiga", jsonObject.getString("nama_photo_tiga"));
                formBuilder.add("photo_3", jsonObject.getString("photo_3"));
            }
            formBuilder.add("meet_to", jsonObject.getString("meet_to"));
            formBuilder.add("created_at", jsonObject.getString("created_at"));
            formBuilder.add("brand", jsonObject.getString("brand"));
            formBuilder.add("type", jsonObject.getString("type"));
            formBuilder.add("serial_number", jsonObject.getString("serial_number"));
            formBuilder.add("condition", jsonObject.getString("condition"));
            RequestBody formBody = formBuilder.build();

            return formBody;
        }

        public static RequestBody getTaskList(String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);

            RequestBody formBody = new FormEncodingBuilder()
                    .add("api_key", jsonObject.getString("api_key"))
                    .add("offset", jsonObject.getString("offset"))
                    .add("sort", jsonObject.getString("sort"))
                    .add("search", jsonObject.getString("search"))
                    .add("due_date", jsonObject.getString("due_date"))
                    .add("account_id", jsonObject.getString("account_id"))
                    .build();

            return formBody;
        }

        public static RequestBody getTaskListbystatus(String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);

            RequestBody formBody = new FormEncodingBuilder()
                    .add("api_key", jsonObject.getString("api_key"))
                    .add("offset", jsonObject.getString("offset"))
                    .add("sort", jsonObject.getString("sort"))
//                    .add("search", jsonObject.getString("search"))
                    .add("due_date", jsonObject.getString("due_date"))
                    .add("account_id", jsonObject.getString("account_id"))
                    .add("task_status", jsonObject.getString("task_status"))
                    .build();

            return formBody;
        }

        public static RequestBody getTaskAdd(String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);

            RequestBody formBody = new FormEncodingBuilder()
                    .add("api_key", jsonObject.getString("api_key"))
                    .add("account_id", jsonObject.getString("account_id"))
                    .add("assign_to", jsonObject.getString("assign_to"))
                    .add("due_date", jsonObject.getString("due_date"))
                    .add("subject", jsonObject.getString("subject"))
                    .add("description", jsonObject.getString("description"))
                    .add("priority", jsonObject.getString("priority"))
                    .add("task_status", jsonObject.getString("task_status"))
                    .add("created_at", jsonObject.getString("created_at"))
                    .build();

            return formBody;
        }


        public static RequestBody getTaskEdit(String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);

            RequestBody formBody = new FormEncodingBuilder()
                    .add("api_key", jsonObject.getString("api_key"))
                    .add("task_id", jsonObject.getString("task_id"))
                    .add("account_id", jsonObject.getString("account_id"))
                    .add("assign_to", jsonObject.getString("assign_to"))
                    .add("due_date", jsonObject.getString("due_date"))
                    .add("subject", jsonObject.getString("subject"))
                    .add("description", jsonObject.getString("description"))
                    .add("priority", jsonObject.getString("priority"))
                    .add("task_status", jsonObject.getString("task_status"))
                    .add("updated_at", jsonObject.getString("updated_at"))
                    .build();

            return formBody;
        }


        public static RequestBody getActivityTodoEdit(String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);

            RequestBody formBody = new FormEncodingBuilder()
                    .add("api_key", jsonObject.getString("api_key"))
                    .add("task_id", jsonObject.getString("task_id"))
                    .add("account_id", jsonObject.getString("account_id"))
                    .add("assign_to", jsonObject.getString("assign_to"))
                    .add("due_date", jsonObject.getString("due_date"))
                    .add("subject", jsonObject.getString("subject"))
                    .add("description", jsonObject.getString("description"))
                    .add("priority", jsonObject.getString("priority"))
                    .add("task_status", jsonObject.getString("task_status"))
                    .add("updated_at", jsonObject.getString("updated_at"))
                    .build();

            return formBody;
        }

        public static RequestBody getActivityEventList(String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);

            RequestBody formBody = new FormEncodingBuilder()
                    .add("api_key", jsonObject.getString("api_key"))
                    .build();

            return formBody;
        }

        public static RequestBody getActivityEventToday(String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);

            RequestBody formBody = new FormEncodingBuilder()
                    .add("api_key", jsonObject.getString("api_key"))
                    .build();

            return formBody;
        }

        public static RequestBody getActivityTodoList(String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);

            RequestBody formBody = new FormEncodingBuilder()
                    .add("api_key", jsonObject.getString("api_key"))
                    .build();

            return formBody;
        }

        public static RequestBody getActivityCallsList(String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);

            RequestBody formBody = new FormEncodingBuilder()
                    .add("api_key", jsonObject.getString("api_key"))
                    .build();
            return formBody;
        }

        public static RequestBody getCallList(String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);

            RequestBody formBody = new FormEncodingBuilder()
                    .add("api_key", jsonObject.getString("api_key"))
                    .add("offset", jsonObject.getString("offset"))
                    .add("search", jsonObject.getString("search"))
                    .build();

            return formBody;
        }

        public static RequestBody getCallAdd(String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);

            RequestBody formBody = new FormEncodingBuilder()
                    .add("api_key", jsonObject.getString("api_key"))
                    .add("account_id", jsonObject.getString("account_id"))
                    .add("call_type_id", jsonObject.getString("call_type_id"))
                    .add("call_purpose_id", jsonObject.getString("call_purpose_id"))
                    .add("date", jsonObject.getString("date"))
                    .add("duration", jsonObject.getString("duration"))
                    .add("subject", jsonObject.getString("subject"))
                    .add("detail", jsonObject.getString("detail"))
                    .add("created_at", jsonObject.getString("created_at"))
                    .build();

            return formBody;
        }

        public static RequestBody getContactAdd(String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);

            RequestBody formBody = new FormEncodingBuilder()
                    .add("api_key", jsonObject.getString("api_key"))
                    .add("account_id", jsonObject.getString("account_id"))
                    .add("title", jsonObject.getString("title"))
                    .add("first_name", jsonObject.getString("first_name"))
                    .add("last_name", jsonObject.getString("last_name"))
                    .add("position_id", jsonObject.getString("position_id"))
                    .add("email", jsonObject.getString("email"))
                    .add("phone", jsonObject.getString("phone"))
                    .build();

            return formBody;
        }

        public static RequestBody getContactEdit(String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);

            RequestBody formBody = new FormEncodingBuilder()
                    .add("api_key", jsonObject.getString("api_key"))
                    .add("contact_id", jsonObject.getString("contact_id"))
                    .add("account_id", jsonObject.getString("account_id"))
                    .add("title", jsonObject.getString("title"))
                    .add("first_name", jsonObject.getString("first_name"))
                    .add("last_name", jsonObject.getString("last_name"))
                    .add("position_id", jsonObject.getString("position_id"))
                    .add("email", jsonObject.getString("email"))
                    .add("phone", jsonObject.getString("phone"))
                    .build();

            return formBody;
        }

        public static RequestBody getMeetTolist(String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);

            RequestBody formBody = new FormEncodingBuilder()
                    .add("api_key", jsonObject.getString("api_key"))
                    .add("meet_to_id", jsonObject.getString("meet_to_id"))
                    .add("account_id", jsonObject.getString("account_id"))
                    .build();

            return formBody;
        }

        public static RequestBody getCallEdit(String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);

            RequestBody formBody = new FormEncodingBuilder()
                    .add("api_key", jsonObject.getString("api_key"))
                    .add("call_id", jsonObject.getString("call_id"))
                    .add("account_id", jsonObject.getString("account_id"))
                    .add("call_type_id", jsonObject.getString("call_type_id"))
                    .add("call_purpose_id", jsonObject.getString("call_purpose_id"))
                    .add("date", jsonObject.getString("date"))
                    .add("duration", jsonObject.getString("duration"))
                    .add("subject", jsonObject.getString("subject"))
                    .add("detail", jsonObject.getString("detail"))
                    .add("created_at", jsonObject.getString("created_at"))
                    .build();

            return formBody;
        }

        public static RequestBody getTotalLeads(String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);

            RequestBody formBody = new FormEncodingBuilder()
                    .add("api_key", jsonObject.getString("api_key"))
                    .add("start_date_periode_1", jsonObject.getString("start_date_periode_1"))
                    .add("end_date_periode_1", jsonObject.getString("end_date_periode_1"))
                    .add("start_date_periode_2", jsonObject.getString("start_date_periode_2"))
                    .add("end_date_periode_2", jsonObject.getString("end_date_periode_2"))
                    .build();

            return formBody;
        }

        public static RequestBody getLeadConversion(String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);

            RequestBody formBody = new FormEncodingBuilder()
                    .add("api_key", jsonObject.getString("api_key"))
                    .add("start_date", jsonObject.getString("start_date"))
                    .add("end_date", jsonObject.getString("end_date"))
                    .build();

            return formBody;
        }

        public static RequestBody getOpportuntiyIndustry(String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);

            RequestBody formBody = new FormEncodingBuilder()
                    .add("api_key", jsonObject.getString("api_key"))
                    .add("month_1", jsonObject.getString("month_1"))
                    .add("year_1", jsonObject.getString("year_1"))
                    .add("month_2", jsonObject.getString("month_2"))
                    .add("year_2", jsonObject.getString("year_2"))
                    .build();

            return formBody;
        }

        public static RequestBody getOpportunityDuration(String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);

            RequestBody formBody = new FormEncodingBuilder()
                    .add("api_key", jsonObject.getString("api_key"))
                    .add("month", jsonObject.getString("month"))
                    .add("year", jsonObject.getString("year"))
                    .build();

            return formBody;
        }

        public static RequestBody getSalesCycle(String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);

            RequestBody formBody = new FormEncodingBuilder()
                    .add("api_key", jsonObject.getString("api_key"))
                    .add("month_1", jsonObject.getString("month_1"))
                    .add("year_1", jsonObject.getString("year_1"))
                    .build();

            return formBody;
        }

        public static RequestBody getProductSold(String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);

            RequestBody formBody = new FormEncodingBuilder()
                    .add("api_key", jsonObject.getString("api_key"))
                    .add("start_date_periode_1", jsonObject.getString("start_date_periode_1"))
                    .add("end_date_periode_1", jsonObject.getString("end_date_periode_1"))
                    .add("start_date_periode_2", jsonObject.getString("start_date_periode_2"))
                    .add("end_date_periode_2", jsonObject.getString("end_date_periode_2"))
                    .add("account_id", jsonObject.getString("account_id"))
                    .build();

            return formBody;
        }


        public static RequestBody getLocationID(String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);

            RequestBody formBody = new FormEncodingBuilder()
                    .add("api_key", jsonObject.getString("api_key"))
                    .add("id", jsonObject.getString("id"))
                    .build();

            return formBody;

        }

        public static RequestBody getSaveLocationUser(String json) throws JSONException {
            JSONObject jsonObject = new JSONObject(json);

            RequestBody formBody = new FormEncodingBuilder()
                    .add("api_key", jsonObject.getString("api_key"))
                    .add("latitude", jsonObject.getString("latitude"))
                    .add("longitude", jsonObject.getString("longitude"))
                    .add("address", jsonObject.getString("address"))
                    .add("date", jsonObject.getString("date"))
                    .build();
            return formBody;
        }

        public static RequestBody getDBmachineAdd (String json) throws  JSONException {
            JSONObject jsonObject = new JSONObject(json);

            RequestBody formBody = new FormEncodingBuilder()
                    .add("api_key", jsonObject.getString("api_key"))
                    .add("account_id", jsonObject.getString("account_id"))
                    .add("brand_id", jsonObject.getString("brand_id"))
                    .add("application", jsonObject.getString("application"))
                    .add("unit_no_in_customer_site", jsonObject.getString("unit_no_in_customer_site"))
                    .add("type", jsonObject.getString("type"))
                    .add("model", jsonObject.getString("model"))
                    .add("serial_number", jsonObject.getString("serial_number"))
                    .add("year_of_install", jsonObject.getString("year_of_install"))
                    .add("pressure_vacuum", jsonObject.getString("pressure_vacuum"))
                    .add("capacity", jsonObject.getString("capacity"))
                    .add("diff_pressure", jsonObject.getString("diff_pressure"))
                    .add("speed", jsonObject.getString("speed"))
                    .add("motor_specs", jsonObject.getString("motor_specs"))
                    .add("date_last_overhaul", jsonObject.getString("date_last_overhaul"))
                    .add("type_of_service_done", jsonObject.getString("type_of_service_done"))
                    .add("latest_inspection_date", jsonObject.getString("latest_inspection_date"))
                    .add("running_hours", jsonObject.getString("running_hours"))
                    .add("condition", jsonObject.getString("condition"))
                    .build();
            return formBody;
        }

        public static RequestBody getDBmachineEdit (String json) throws  JSONException {
            JSONObject jsonObject = new JSONObject(json);

            RequestBody formBody = new FormEncodingBuilder()
                    .add("api_key", jsonObject.getString("api_key"))
                    .add("account_id", jsonObject.getString("account_id"))
                    .add("database_machine_id", jsonObject.getString("database_machine_id"))
                    .add("brand_id", jsonObject.getString("brand_id"))
                    .add("application", jsonObject.getString("application"))
                    .add("unit_no_in_customer_site", jsonObject.getString("unit_no_in_customer_site"))
                    .add("type", jsonObject.getString("type"))
                    .add("model", jsonObject.getString("model"))
                    .add("serial_number", jsonObject.getString("serial_number"))
                    .add("year_of_install", jsonObject.getString("year_of_install"))
                    .add("pressure_vacuum", jsonObject.getString("pressure_vacuum"))
                    .add("capacity", jsonObject.getString("capacity"))
                    .add("diff_pressure", jsonObject.getString("diff_pressure"))
                    .add("speed", jsonObject.getString("speed"))
                    .add("motor_specs", jsonObject.getString("motor_specs"))
                    .add("date_last_overhaul", jsonObject.getString("date_last_overhaul"))
                    .add("type_of_service_done", jsonObject.getString("type_of_service_done"))
                    .add("latest_inspection_date", jsonObject.getString("latest_inspection_date"))
                    .add("running_hours", jsonObject.getString("running_hours"))
                    .add("condition", jsonObject.getString("condition"))
                    .build();
            return formBody;
        }

    }
}
