package com.msf.project.sales1crm.database;

/**
 * Created by christianmacbook on 23/05/18.
 */

public class DBSchema {
    public interface Base {
        public static final String COL_ID = "id_";
    }

    public interface User {
        public static final String TABLE_NAME = "t_user";
        public static final String KEY_USER_ID = "user_id"; //1
        public static final String KEY_NAME = "name"; //1
    }

    public interface Position {
        public static final String TABLE_NAME = "t_position";
        public static final String KEY_NAME = "name"; //1
    }

    public interface Industry {
        public static final String TABLE_NAME = "t_industry";
        public static final String KEY_NAME = "name"; //1
    }

    public interface EventStatus {
        public static final String TABLE_NAME = "t_event_status";
        public static final String KEY_NAME = "name";//1
    }

    public interface EventPurpose {
        public static final String TABLE_NAME = "t_event_purpose";
        public static final String KEY_NAME = "name";//1
    }

    public interface AccountType {
        public static final String TABLE_NAME = "t_account_type";
        public static final String KEY_NAME = "name"; //1
    }

    public interface OpportunityStatus {
        public static final String TABLE_NAME = "t_opportunity_status";
        public static final String KEY_NAME = "name"; //1
    }

    public interface OpportunityStage {
        public static final String TABLE_NAME = "t_opportunity_stage";
        public static final String KEY_NAME = "name"; //1
        public static final String KEY_PERCENT = "percent"; //1
    }

    public interface LeadSource {
        public static final String TABLE_NAME = "t_lead_source";
        public static final String KEY_NAME = "name"; //1
    }

    public interface LeadStatus {
        public static final String TABLE_NAME = "t_lead_status";
        public static final String KEY_NAME = "name"; //1
    }

    public interface NoVisitReason {
        public static final String TABLE_NAME = "t_no_visit_reason";
        public static final String KEY_NAME = "name"; //1
    }

    public interface UseFor {
        public static final String TABLE_NAME = "t_use_for";
        public static final String KEY_NAME = "name"; //1
    }

    public interface NoOrderReason {
        public static final String TABLE_NAME = "t_no_order_reason";
        public static final String KEY_NAME = "name"; //1
    }

    public interface Function {
        public static final String TABLE_NAME = "t_function";
        public static final String KEY_FUNCTION_ID ="function_id"; //1
        public static final String KEY_FUNCTION_CODE_NAME ="function_code_name"; //2
    }

    public interface FunctionUAC {
        public static final String TABLE_NAME = "t_function_UAC";
        public static final String KEY_FUNCTION_ID ="function_id"; //1
        public static final String KEY_FUNCTION_CODE_NAME ="function_code_name"; //2
    }

    public interface LocationUser {
        public static final String TABLE_NAME = "t_location_user";
        public static final String KEY_USER_ID = "user_id";
        public static final String KEY_LATITUDE = "latitude"; //1
        public static final String KEY_LONGITUDE = "longitude"; //2
        public static final String KEY_ADDRESS = "address"; //3
        public static final String KEY_DATE = "date"; // 4
    }

    public interface MeetTo {
        public static final String TABLE_NAME = "t_meet_to";
        public static final String KEY_ACCOUNT_ID = "account_id";
        public static final String KEY_MEET_TO_ID = "meet_to_id";
        public static final String KEY_FULLNAME = "fullname";
        public static final String KEY_IS_CHECK = "is_check";
    }

    public interface Product {
        public static final String TABLE_NAME = "t_product";
        public static final String KEY_ID_TEMP = "id_temp";
        public static final String KEY_ID   = "id_product";
        public static final String KEY_BRAND = "brand";
        public static final String KEY_TYPE = "type";
        public static final String KEY_SERIAL_NUMBER = "serial_number";
        public static final String KEY_CONDITION = "condition";
        public static final String KEY_STATUC = "status";
    }


    public interface Meet_to_list {
        public static final String TABLE_NAME = "t_meet_to_list";
        public static final String KEY_MEET_TO_ID = "meet_to_id";
        public static final String KEY_FULLNAME = "name";
        public static final String KEY_STATUS = "status";
        public static final String KEY_NOTATION = "notation";
    }

    public interface Reason {
        public static final String TABLE_NAME = "t_reason";
        public static final String KEY_ID = "reason_id";
        public static final String KEY_REASON = "reason";
        public static final String KEY_STAT = "stat";
    }
}
