package com.msf.project.sales1crm;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.msf.project.sales1crm.fragment.AccountCardFrangment;
import com.msf.project.sales1crm.fragment.AccountFrangment;
import com.msf.project.sales1crm.model.AccountModel;
import com.msf.project.sales1crm.presenter.AccountListPresenter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class AccountMapFragment_googlemaps extends Fragment implements OnMapReadyCallback {


    @BindView(R.id.faAccountAdd)
    FloatingActionButton faAccountAdd;
    @BindView(R.id.faQuickAdd)
    FloatingActionButton faQuickAdd;
    @BindView(R.id.menu)
    FloatingActionMenu menu;
    Unbinder unbinder;
    @BindView(R.id.myLocationNow)
    FloatingActionButton famyLocationNow;
    private GoogleMap mMap;
    private View view;
    private Double[] latitude, longitude;
    private AccountListPresenter presenter;
    private Context context;
    private AccountModel accountModel;
    private String url;
    MapView mMapView;
    FragmentManager fragmentManager;
    SupportMapFragment mFragment;

    public static AccountMapFragment_googlemaps newInstance() {
        AccountMapFragment_googlemaps fragment = new AccountMapFragment_googlemaps();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.


//        ActionBar actionBar = getActionBar();
//        getActionBar().setTitle("account");
//        String title = actionBar.getTitle().toString();

//        this.url = PreferenceUtility.getInstance().loadDataString(context,PreferenceUtility.URL);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("account");


    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_maps_googlemaps, container, false);
        setHasOptionsMenu(true);
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
//        mapFragment.getMapAsync(new OnMapReadyCallback() {
//            @Override
//            public void onMapReady(GoogleMap googleMap) {
//                googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
//                googleMap.clear();
//
//                CameraPosition goolePlex = CameraPosition.builder()
//                        .target();
//
//            }
//        });
//        SupportMapFragment mapFragment = (SupportMapFragment) getFragmentManager()
//                .findFragmentById(R.id.map);
//        mapFragment.getMapAsync(this);

        unbinder = ButterKnife.bind(this, this.view);

//       mMapView = (MapView) view.findViewById(R.id.map);
//       mMapView.onCreate(savedInstanceState);
//       mMapView.onResume();
        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
//        mMap = mMapView.getMap();
        double latitude = 17.385044;
        double longitude = 78.486671;

        MarkerOptions markerOptions = new MarkerOptions().position(new LatLng(latitude, longitude)).title("Hello Maps");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE));

//        mMap.addMarker(markerOptions);
//        CameraPosition cameraPosition = new CameraPosition.Builder()
//                .target(new LatLng(17.385044, 78.486671)).zoom(12).build();
//        mMap.animateCamera(CameraUpdateFactory
//                .newCameraPosition(cameraPosition));

        return this.view;
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng map = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(map).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(map));
        mMap.setMapType(GoogleMap.MAP_TYPE_NONE);
//        mMap.setMyLocationEnabled(true);

        SupportMapFragment mapFragment = (SupportMapFragment) getFragmentManager().findFragmentById(R.id.map);
    }

    private void getAllLocation(String result, List<AccountModel> accountModels) {
        if (result.equals("OK")) {


        }
    }

    private void userLocationFAB() {

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_list:
                MenuActivity.getInstance().setFragment(AccountFrangment.newInstance());
                item.setIcon(R.drawable.ic_list_white_24dp);
                break;
            case R.id.action_cards:
                MenuActivity.getInstance().setFragment(AccountCardFrangment.newInstance());
                item.setIcon(R.drawable.ic_cards_black_24dp);
                break;
            case R.id.action_maps:
                MenuActivity.getInstance().setFragment(AccountMapFragment_googlemaps.newInstance());
                item.setIcon(R.drawable.ic_map_white_24dp);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        mMapView.onDestroy();
        unbinder.unbind();
    }

    @Override
    public void onResume() {
        super.onResume();
//        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
//        mMapView.onPause();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
//        mMapView.onLowMemory();
    }
}
