package com.msf.project.sales1crm.callback;

import com.msf.project.sales1crm.model.LeadModel;

import java.util.List;

/**
 * Created by christianmacbook on 30/05/18.
 */

public interface LeadListCallback {
    void finishLeadList(String result, List<LeadModel> list);
    void finishLeadMoreList(String result, List<LeadModel> list);
}
