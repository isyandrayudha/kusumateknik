package com.msf.project.sales1crm.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.msf.project.sales1crm.model.IndustryModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by christianmacbook on 24/05/18.
 */

public class IndustryDao extends BaseDao<IndustryModel> implements DBSchema.Industry {

    public IndustryDao(Context c) {
        super(c, TABLE_NAME);
    }

    public IndustryDao(Context c, boolean willWrite) {
        super(c, TABLE_NAME, willWrite);
    }

    public IndustryDao(DBHelper db) {
        super(db, TABLE_NAME);
    }

    public IndustryDao(DBHelper db, boolean willWrite) {
        super(db, TABLE_NAME, willWrite);
    }

    public IndustryModel getById(int id) {
        String qry = "SELECT * FROM " + getTable() + " WHERE " + COL_ID + " = " + id;
        Cursor c = getSqliteDb().rawQuery(qry, null);
        IndustryModel model = new IndustryModel();
        try {
            if(c != null && c.moveToFirst()) {
                model = getByCursor(c);
            }
        } finally {
            c.close();
        }
        return model;
    }

    public List<IndustryModel> getIndustryList() {
        String query = "SELECT * FROM " + getTable();

        Cursor c = getSqliteDb().rawQuery(query, null);
        List<IndustryModel> list = new ArrayList<>();
        try {
            if(c != null && c.moveToFirst()) {
                list.add(getByCursor(c));
                while (c.moveToNext()) {
                    list.add(getByCursor(c));

                }
            }
        } finally {
            c.close();
        }
        return list;
    }

    public String[] getStringArray(){
        String query = "SELECT * FROM " + getTable();

        Cursor c = getSqliteDb().rawQuery(query, null);
        String[] array = new String[c.getCount() + 1];
        try {
            if(c != null && c.moveToFirst()) {
                array[0] = "Industri";
                array[1] = getByCursor(c).getName();
                int i = 2;
                while (c.moveToNext()) {
                    array[i] = getByCursor(c).getName();
                    i++;

                }
            }
        } finally {
            c.close();
        }

        return array;
    }

    public String[] getStringArrayID(){
        String query = "SELECT * FROM " + getTable();

        Cursor c = getSqliteDb().rawQuery(query, null);
        String[] array = new String[c.getCount() + 1];
        try {
            if(c != null && c.moveToFirst()) {
                array[0] = "0";
                array[1] = getByCursor(c).getId();
                int i = 2;
                while (c.moveToNext()) {
                    array[i] = getByCursor(c).getId();
                    i++;

                }
            }
        } finally {
            c.close();
        }

        return array;
    }

    @Override
    public IndustryModel getByCursor(Cursor c) {
        IndustryModel model = new IndustryModel();
        model.setId(c.getString(0));
        model.setName(c.getString(1));
        return model;
    }

    @Override
    protected ContentValues upDataValues(IndustryModel model, boolean update) {
        ContentValues cv = new ContentValues();
        if (update == true)
            cv.put(COL_ID, model.getId());
        cv.put(KEY_NAME, model.getName());
        return cv;
    }
}
