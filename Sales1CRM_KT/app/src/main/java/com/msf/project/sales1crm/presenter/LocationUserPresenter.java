package com.msf.project.sales1crm.presenter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;

import com.msf.project.sales1crm.callback.LocationUserCallback;
import com.msf.project.sales1crm.model.LocationUserModel;
import com.msf.project.sales1crm.service.NetworkConnection;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.PreferenceUtility;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;
import com.msf.project.sales1crm.utility.Sales1CRMUtilsJSON;

import org.json.JSONException;
import org.json.JSONObject;

public class LocationUserPresenter {
    private final String TAG = LocationUserPresenter.class.getSimpleName();
    private Context context;
    private Messenger messenger;
    private Message message;
    private Handler handler;
    private Bundle bundle;
    private String failureResponse= "";
    private String[] stringResponse= {""};
    private LocationUserCallback callback;
    private String resultUserLocation = "NG";
    private String response = "Tolong Periksa Koneksi Anda";

    public LocationUserPresenter (Context context, LocationUserCallback callback){
        this.context = context;
        this.callback = callback;
    }

    public void doNetworkService(String url, String params, String form) {
        Log.d(TAG,"URL USER LOCATION " + url);
        Log.d(TAG, "params : " + params);
        Intent networkIntent = new Intent(this.context, NetworkConnection.class);
        this.messenger = new Messenger(this.handler);
        networkIntent.putExtra("messenger", this.messenger);
        networkIntent.putExtra("url", url);
        networkIntent.putExtra("params", params);
        networkIntent.putExtra("from", form);
        this.context.startService(networkIntent);
    }

    public void saveData(int apiIndex, LocationUserModel locationUserModel) {
        if(Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)){
            obtainSaveData(apiIndex, locationUserModel);
        } else {
            callback.finishSave(this.resultUserLocation, this.response, locationUserModel.getLatitude());
        }
    }

    public void obtainSaveData(int apiIndex, LocationUserModel locationUserModel) {
        this.handler = new Handler(this.context.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                parseSaveData(msg, locationUserModel.getLatitude());
            }
        };

        JSONObject jsonObject = new JSONObject();
        try{
            jsonObject.put("api_key", PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.API_KEY));
            jsonObject.put("latitude",locationUserModel.getLatitude());
            jsonObject.put("longitude",locationUserModel.getLongitude());
            jsonObject.put("address",locationUserModel.getAddress());
            jsonObject.put("date",locationUserModel.getDate());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        doNetworkService(ApiParam.urlBuilder(this.context, apiIndex), jsonObject.toString(), "LocationUser");
    }

    private void parseSaveData (Message message, String id) {
        this.message = message;
        this.bundle = this.message.getData();
        this.stringResponse[0] = this.bundle.getString("network_response");
        this.failureResponse = this.bundle.getString("network_failure");
        Log.d(TAG, "responseString[0] info " + this.stringResponse[0]);
        if(this.failureResponse.equalsIgnoreCase("yes")){
            this.callback.finishSave(this.resultUserLocation, this.response, id);
        } else {
            try{
                this.resultUserLocation = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                this.callback.finishSave(this.resultUserLocation, this.response, id);
            } catch (JSONException e) {
                Log.d(TAG,"Exception detail response " + e.getMessage());
            }
        }
    }
}
