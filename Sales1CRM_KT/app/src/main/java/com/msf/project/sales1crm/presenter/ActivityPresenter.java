package com.msf.project.sales1crm.presenter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;

import com.msf.project.sales1crm.callback.ActivityListCallback;
import com.msf.project.sales1crm.model.ActivityCallsModel;
import com.msf.project.sales1crm.model.ActivityEventModel;
import com.msf.project.sales1crm.model.ActivityTodoModel;
import com.msf.project.sales1crm.service.NetworkConnection;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.PreferenceUtility;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;
import com.msf.project.sales1crm.utility.Sales1CRMUtilsJSON;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ActivityPresenter {
    private final String TAG = ActivityPresenter.class.getSimpleName();
    private Context context;
    private Messenger messenger;
    private Message message;
    private Bundle bundle;
    private Handler handler;
    private String failurResponse ="";
    private String[] stringResponse = {""};
    private ActivityListCallback callback;
    private String result = "NG";
    private List<ActivityEventModel> activityEventModelList = new ArrayList<>();
    private List<ActivityTodoModel> activityTodoModelList = new ArrayList<>();
    private List<ActivityCallsModel> activityCallsModelList = new ArrayList<>();

    public ActivityPresenter(Context context, ActivityListCallback listCallback){
        this.context = context;
        this.callback = listCallback;
    }

    private void doNetworkService(String url, String params, String from) {
        Log.d (TAG,"url activity list " + url);
        Log.d(TAG,"params : " +params);
        Intent intent = new Intent(this.context, NetworkConnection.class);
        this.messenger = new Messenger(this.handler);
        intent.putExtra("messenger",this.messenger);
        intent.putExtra("url",url);
        intent.putExtra("params",params);
        intent.putExtra("from",from);
        this.context.startService(intent);
    }

    public void setupListEvent(int apiIndex,String search){
       if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)){
            ObtainListItemEvent(apiIndex,search);
       }else {
           callback.finishActivityEventList(this.result,this.activityEventModelList);
       }
    }
    public void ObtainListItemEvent(int apiIndex, String search){
        this.handler = new Handler(this.context.getMainLooper()){
            @Override
            public void handleMessage(Message msg) {
                parseListEvent(msg);
            }
        };

        JSONObject jsonObject = new JSONObject();
        try{
            jsonObject.put("api_key", PreferenceUtility.getInstance().loadDataString(context,PreferenceUtility.API_KEY));
            jsonObject.put("search", search);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        doNetworkService(ApiParam.urlBuilder(this.context,apiIndex),jsonObject.toString(),"ActivityEventList");
    }

    private void parseListEvent(Message message) {
        this.message = message;
        this.bundle = this.message.getData();
        this.stringResponse[0] = this.bundle.getString("network_response");
        this.failurResponse = this.bundle.getString("network_failure");
        Log.d(TAG,"responseString[0] event info " + this.stringResponse[0]);
        if (this.failurResponse.equalsIgnoreCase("yes")){
            this.callback.finishActivityEventList(this.result,this.activityEventModelList);
        } else {
            try{
                this.result = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                if (this.result.equalsIgnoreCase("OK")) {
                    this.activityEventModelList = Sales1CRMUtilsJSON.JSONUtility.getActivityEventList(this.stringResponse[0]);
                }
                this.callback.finishActivityEventList(this.result,this.activityEventModelList);
            } catch (JSONException e) {
                Log.d(TAG,"EXCEPTION DETAIL RESPONSE" + e.getMessage());
            }
        }
    }

    public void setupMoreListEvent(int apiIndex, String search){
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)){
                ObtainMoreListItemEvent(apiIndex, search);
        }else {
            callback.finishActivityEventMoreList(this.result,this.activityEventModelList);
        }
    }
    public void ObtainMoreListItemEvent(int apiIndex, String search){
        this.handler = new Handler(this.context.getMainLooper()){
            @Override
            public void handleMessage(Message msg) {
                parseMoreListEvent(msg);
            }
        };

        JSONObject jsonObject = new JSONObject();
        try{
            jsonObject.put("api_key", PreferenceUtility.getInstance().loadDataString(context,PreferenceUtility.API_KEY));
            jsonObject.put("search", search);
         } catch (JSONException e) {
            e.printStackTrace();
        }
        doNetworkService(ApiParam.urlBuilder(this.context,apiIndex),jsonObject.toString(),"ActivityEventList");
    }

    private void parseMoreListEvent(Message message) {
        this.message = message;
        this.bundle = this.message.getData();
        this.stringResponse[0] = this.bundle.getString("network_response");
        this.failurResponse = this.bundle.getString("network_failure");
        Log.d(TAG,"responseString[0] event more info " + this.stringResponse[0]);
        if (this.failurResponse.equalsIgnoreCase("yes")){
            this.callback.finishActivityEventMoreList(this.result,this.activityEventModelList);
        } else {
            try{
                this.result = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                if (this.result.equalsIgnoreCase("OK")) {
                    this.activityEventModelList = Sales1CRMUtilsJSON.JSONUtility.getActivityEventList(this.stringResponse[0]);
                }
                this.callback.finishActivityEventMoreList(this.result,this.activityEventModelList);
            } catch (JSONException e) {
                Log.d(TAG,"EXCEPTION DETAIL RESPONSE" + e.getMessage());
            }
        }
    }

    public void setupListTodo(int apiIndex){
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)) {
            obtainListItemTodo(apiIndex);
        }else{
            callback.finishActivityTodoMoreList(this.result, this.activityTodoModelList);
        }
    }

    public void obtainListItemTodo(int apiIndex) {
        this.handler = new Handler(this.context.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                parseListItemTodo(msg);
            }
        };
        JSONObject  jsonObject = new JSONObject();
        try{
            jsonObject.put("api_key", PreferenceUtility.getInstance().loadDataString(context,PreferenceUtility.API_KEY));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        doNetworkService(ApiParam.urlBuilder(this.context,apiIndex),jsonObject.toString(),"ApiKey");
    }

    public void parseListItemTodo(Message message) {
        this.message = message;
        this.bundle = this.message.getData();
        this.stringResponse[0] = this.bundle.getString("network_response");
        this.failurResponse = this.bundle.getString("network_failure");
        Log.d(TAG, "responseString[0] TODO LIST INFO " + this.stringResponse[0]);
        if (this.failurResponse.equalsIgnoreCase("yes")){
            this.callback.finishActivityTodoList(this.result, this.activityTodoModelList);
        }else{
            try {
                this.result = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                if (this.result.equalsIgnoreCase("OK")){
                    this.activityTodoModelList = Sales1CRMUtilsJSON.JSONUtility.getActivityTodoList(this.stringResponse[0]);
                }
                this.callback.finishActivityTodoList(this.result, this.activityTodoModelList);
            } catch (JSONException e) {
                Log.d(TAG, "Exception detail Response " + e.getMessage());
            }
        }
    }

    public void setupMoreTodoList(int apiIndex){
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)) {
            obtainMoreListTodo(apiIndex);
        }else{
            callback.finishActivityTodoMoreList(this.result, this.activityTodoModelList);
        }
    }
    public void obtainMoreListTodo(int apiIndex) {
        this.handler = new Handler(this.context.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                parseMoreActivityTodo(msg);
            }
        };
        JSONObject jsonObject = new JSONObject();
        try{
            jsonObject.put("api_key",PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.API_KEY));
        } catch (JSONException e){
            e.printStackTrace();
        }
        doNetworkService(ApiParam.urlBuilder(this.context,apiIndex),jsonObject.toString(),"ApiKey");
    }

    public void parseMoreActivityTodo(Message message) {
        this.message = message;
        this.bundle = this.message.getData();
        this.stringResponse[0] = this.bundle.getString("network_response");
        this.failurResponse = this.bundle.getString("network_failure");
        Log.d(TAG, "responseString[0] TODO MORE LIST INFO " + this.stringResponse[0]);
        if (this.failurResponse.equalsIgnoreCase("yes")){
            this.callback.finishActivityTodoMoreList(this.result, this.activityTodoModelList);
        }else{
            try {
                this.result = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                if (this.result.equalsIgnoreCase("OK")){
                    this.activityTodoModelList = Sales1CRMUtilsJSON.JSONUtility.getActivityTodoList(this.stringResponse[0]);
                }
                this.callback.finishActivityTodoMoreList(this.result, this.activityTodoModelList);
            } catch (JSONException e) {
                Log.d(TAG, "Exception detail Response " + e.getMessage());
            }
        }
    }

    public void setupCallList(int apiIndex, String search) {
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)) {
            obtainListCallItem(apiIndex,search);
        } else {
            callback.finishActivityCallList(this.result, this.activityCallsModelList);
        }
    }

    public void obtainListCallItem(int apiIndex, String search) {
        this.handler = new Handler(this.context.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                parseListCallitem(msg);
            }
        };

        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("api_key", PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.API_KEY));
            jsonObject.put("search", search);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        doNetworkService(ApiParam.urlBuilder(this.context,apiIndex),jsonObject.toString(),"ActivityCallsList");

    }

    private void parseListCallitem(Message message) {
        this.message = message;
        this.bundle = this.message.getData();
        this.stringResponse[0] = this.bundle.getString("network_response");
        this.failurResponse = this.bundle.getString("network_failure");
        Log.d(TAG, "responseString[0] Activity calls info " + this.stringResponse[0]);
        if (this.failurResponse.equalsIgnoreCase("yes")) {
            this.callback.finishActivityCallList(this.result, this.activityCallsModelList);
        } else {
            try {
                this.result = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                if (this.result.equalsIgnoreCase("OK")) {
                    this.activityCallsModelList = Sales1CRMUtilsJSON.JSONUtility.getActivityCallsList(this.stringResponse[0]);
                }
                this.callback.finishActivityCallList(this.result,this.activityCallsModelList);
            } catch (JSONException e) {
                Log.d(TAG, "Exception detail Response " + e.getMessage());

            }
        }
    }

    public void setupMoreListCalls(int apiIndex, String search) {
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)) {
            obtainMoreListCallItem(apiIndex, search);
        } else {
            callback.finishActivityCallMoreList(this.result, this.activityCallsModelList);
        }
    }

    public void obtainMoreListCallItem(int apiIndex, String search) {
        this.handler = new Handler(this.context.getMainLooper()){
            @Override
            public void handleMessage(Message msg) {
                parseMoreCallListItem(msg);
            }
        };
        JSONObject jsonObject =  new JSONObject();
        try{
            jsonObject.put("api_key",PreferenceUtility.getInstance().loadDataString(context,PreferenceUtility.API_KEY));
            jsonObject.put("search", search);
        }catch (JSONException e){
            e.printStackTrace();
        }
        doNetworkService(ApiParam.urlBuilder(this.context,apiIndex),jsonObject.toString(),"ActivityCallsList");
    }


    private void parseMoreCallListItem(Message message) {
        this.message = message;
        this.bundle = this.message.getData();
        this.stringResponse[0] = this.bundle.getString("network_response");
        this.failurResponse = this.bundle.getString("network_failure");
        Log.d(TAG, "responseString[0] Call More Info " + this.stringResponse[0]);
        if (this.failurResponse.equalsIgnoreCase("yes")) {
            this.callback.finishActivityCallMoreList(this.result, this.activityCallsModelList);
        } else {
            try {
                this.result = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                if (this.result.equalsIgnoreCase("OK")) {
                    this.activityCallsModelList = Sales1CRMUtilsJSON.JSONUtility.getActivityCallsList(this.stringResponse[0]);
                }
                this.callback.finishActivityCallMoreList(this.result, this.activityCallsModelList);
            } catch (JSONException e) {
                Log.d(TAG, "Exception detail Response " + e.getMessage());
            }
        }
    }

}
