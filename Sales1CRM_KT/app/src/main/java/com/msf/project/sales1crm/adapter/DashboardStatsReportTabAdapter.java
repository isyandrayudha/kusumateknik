package com.msf.project.sales1crm.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.msf.project.sales1crm.fragment.ViewDashboardGeneralDashboardFragment;
import com.msf.project.sales1crm.fragment.ViewDashboardLeadDashboardFragment;

public class DashboardStatsReportTabAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;
    Bundle bundle;

    public DashboardStatsReportTabAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                ViewDashboardGeneralDashboardFragment tab1 = new ViewDashboardGeneralDashboardFragment();
                return tab1;
            case 1:
                ViewDashboardLeadDashboardFragment tab2 = new ViewDashboardLeadDashboardFragment();
                return tab2;
            case 2:
                ViewDashboardGeneralDashboardFragment tab3 = new ViewDashboardGeneralDashboardFragment();
                return tab3;
            case 3:
                ViewDashboardGeneralDashboardFragment tab4 = new ViewDashboardGeneralDashboardFragment();
                return tab4;
            case 4:
                ViewDashboardGeneralDashboardFragment tab5 = new ViewDashboardGeneralDashboardFragment();
                return tab5;
            case 5:
                ViewDashboardGeneralDashboardFragment tab6 = new ViewDashboardGeneralDashboardFragment();
                return tab6;
            case 6:
                ViewDashboardGeneralDashboardFragment tab7 = new ViewDashboardGeneralDashboardFragment();
                return tab7;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
