package com.msf.project.sales1crm.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;

import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.model.ProductModel;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;

public class OpportunityProductAdapter extends BaseAdapter {
    private Context context;
    private Bitmap icon;
    private List<ProductModel> listData;

    public OpportunityProductAdapter(Context context, List<ProductModel> objects) {
        this.context = context;
        this.listData = objects;
    }

    public static abstract class Row {
    }

    // This is for making Section, ex: A B C D etc
    public static final class Section extends Row {
        public final String text;

        public Section(String text) {
            this.text = text;
        }
    }

    // This is for making Row, ex: A B C D etc
    public static final class Item extends Row {
        public final ProductModel text;

        public Item(ProductModel text) {
            this.text = text;
        }

        public ProductModel getItem() {
            return text;
        }
    }

    private List<Row> rows;

    public void setRows(List<Row> rows) {
        this.rows = rows;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return rows.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return rows.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        if (getItem(position) instanceof Section) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public View getView(final int position, View converView, ViewGroup parent) {
        View view = converView;
        final ViewHolder holder;

        final Item item = (Item) getItem(position);
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) parent.getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = (LinearLayout) inflater.inflate(
                    R.layout.row_opportunityproduct, parent, false);
            holder = new ViewHolder();
            holder.tvProductName = (CustomTextView) view
                    .findViewById(R.id.tvProductName);
            holder.tvProductCode= (CustomTextView) view
                    .findViewById(R.id.tvProductCode);
            holder.tvBranch= (CustomTextView) view
                    .findViewById(R.id.tvBranch);
            holder.tvCategoryName = (CustomTextView) view
                    .findViewById(R.id.tvCategoryName);
            holder.tvPrice = (CustomTextView) view
                    .findViewById(R.id.tvPrice);
            holder.tvStock = (CustomTextView) view
                    .findViewById(R.id.tvStock);
            holder.tvQty = (CustomTextView) view
                    .findViewById(R.id.tvQty);
//            holder.llDelete = (LinearLayout) view
//                    .findViewById(R.id.llDelete);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }


        DecimalFormat df = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols dfs = new DecimalFormatSymbols();
        dfs.setCurrencySymbol("");
        dfs.setDecimalSeparator(',');
        dfs.setGroupingSeparator('.');
        df.setDecimalFormatSymbols(dfs);
        String hsl = df.format(Double.parseDouble(item.text.getPrice()));

        holder.tvProductName.setText(item.text.getName());
        holder.tvProductCode.setText(item.text.getCode());
        holder.tvBranch.setText(item.text.getBranch_name());
        holder.tvCategoryName.setText(item.text.getCategory_name());
        holder.tvQty.setText(item.text.getQty() + " Pcs" );

        if (item.text.getShow_price().equalsIgnoreCase("1")){
            holder.tvPrice.setText(item.text.getCurrency_name() +" " + hsl);
        }else{
            holder.tvPrice.setText("-");
        }

        if (item.text.getShow_stock().equalsIgnoreCase("1")){
            holder.tvStock.setText(item.text.getStock() + " " + item.text.getUnit());
        }else{
            holder.tvStock.setText("-");
        }

        return view;
    }

    private static class ViewHolder {
        CustomTextView tvProductName, tvProductCode, tvBranch, tvCategoryName, tvPrice, tvStock, tvQty;
    }
}
