package com.msf.project.sales1crm.presenter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;

import com.msf.project.sales1crm.callback.MeetToListCallback;
import com.msf.project.sales1crm.model.ContactModel;
import com.msf.project.sales1crm.service.NetworkConnection;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.PreferenceUtility;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;
import com.msf.project.sales1crm.utility.Sales1CRMUtilsJSON;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MeetToListPresenter {
    private final String TAG = MeetToListPresenter.class.getSimpleName();
    private Context context;
    private Messenger messenger;
    private Message message;
    private Handler handler;
    private Bundle bundle;
    private String failureResponse = "";
    private String[] stringResponse = {""};
    private MeetToListCallback callback;
    private String result = "NG";
    private List<ContactModel> list = new ArrayList<>();
    private String response = "Tolong Perkisa Koneksi Anda";

    public MeetToListPresenter (Context context, MeetToListCallback callback){
        this.context = context;
        this.callback = callback;
    }

    private void doNetworkService(String url, String params, String from) {
        Log.d(TAG, "url " + url);
        Log.d(TAG, "params :" + params);
        Intent intent = new Intent(this.context, NetworkConnection.class);
        this.messenger = new Messenger(this.handler);
        intent.putExtra("messenger", this.messenger);
        intent.putExtra("url", url);
        intent.putExtra("params", params);
        intent.putExtra("from", from);
        this.context.startService(intent);
    }

    public void setupList(int apiIndex,String account_id, String meet_to_id){
        if(Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)){
            obtainListItem(apiIndex,account_id, meet_to_id);
        } else {
            callback.finishListMeetTo(this.result,this.list);
        }
    }

    public void obtainListItem(int apiIndex, String account_id, String meet_to_id) {
        this.handler = new Handler(this.context.getMainLooper()) {

            @Override
            public void handleMessage(Message msg) {
                parseListItem(msg);
            }

        };

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("api_key", PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.API_KEY));
            jsonObject.put("account_id", account_id);
            jsonObject.put("meet_to_id", meet_to_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        doNetworkService(ApiParam.urlBuilder(this.context, apiIndex), jsonObject.toString(), "meetToList");
    }

    private void parseListItem(Message message) {
        this.message = message;
        this.bundle = this.message.getData();
        this.stringResponse[0] = this.bundle.getString("network_response");
        this.failureResponse = this.bundle.getString("network_failure");
        Log.d(TAG, "responseString[0] login info " + this.stringResponse[0]);
        if (this.failureResponse.equalsIgnoreCase("yes")){
            this.callback.finishListMeetTo(this.result, this.list);
        }else{
            try {
                this.result = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                if (this.result.equalsIgnoreCase("OK")){
                this.list = Sales1CRMUtilsJSON.JSONUtility.getMeetToList(this.stringResponse[0]);
                }
                this.callback.finishListMeetTo(this.result, this.list);
            } catch (JSONException e) {
                Log.d(TAG, "Exception detail Response " + e.getMessage());
            }
        }
    }


}
