package com.msf.project.sales1crm.customview;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.msf.project.sales1crm.OpportunityAddActivity;
import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.fragment.OpportunityTabFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class SheetBottomDialogOpportunity extends BottomSheetDialogFragment {

    @BindView(R.id.OpportunityAdd)
    LinearLayout OpportunityAdd;
    @BindView(R.id.OpportunityList)
    LinearLayout OpportunityList;
    @BindView(R.id.bottomSheet)
    LinearLayout bottomSheet;
    Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_bottom_sheet_dialog_opportunity, container, false);
        unbinder = ButterKnife.bind(this, v);

        OpportunityAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity().getApplication(),OpportunityAddActivity.class);
                startActivity(intent);
            }
        });

        OpportunityList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_layout, OpportunityTabFragment.newInstance());
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                transaction.commit();
                dismiss();
            }
        });
        return v;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
