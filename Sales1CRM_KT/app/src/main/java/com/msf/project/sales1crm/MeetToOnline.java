package com.msf.project.sales1crm;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.msf.project.sales1crm.adapter.MeetoAdapter;
import com.msf.project.sales1crm.callback.MeetToListCallback;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.database.MeeToSelectDao;
import com.msf.project.sales1crm.model.ContactModel;
import com.msf.project.sales1crm.presenter.MeetToListPresenter;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MeetToOnline extends BaseActivity implements MeetToListCallback {


    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.tvTitle)
    CustomTextView tvTitle;
    @BindView(R.id.rl)
    RelativeLayout rl;
    @BindView(R.id.llNotFound)
    LinearLayout llNotFound;
    @BindView(R.id.faAccountAdd)
    FloatingActionButton faAccountAdd;
    @BindView(R.id.menu)
    FloatingActionMenu menu;
    @BindView(R.id.lvMeetoo)
    RecyclerView lvMeetoo;
    @BindView(R.id.shimmerLayout)
    ShimmerFrameLayout shimmerLayout;
    @BindView(R.id.llshim)
    LinearLayout llshim;
    private Context context;
    private View view;
    private MeetoAdapter adapter;
    private List<ContactModel> list = new ArrayList<>();
    private String account_id;
    static MeetToOnline meetToOnline;
    private MeetToListPresenter presenter;

    public static MeetToOnline getInstance() {
        return meetToOnline;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meet_to_online);
        ButterKnife.bind(this);
        context = this;
        this.presenter = new MeetToListPresenter(context, this);

        try {
            account_id = getIntent().getExtras().getString("account_id");
        } catch (Exception e) {

        }
        initData();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Sales1CRMUtils.LIST_MEET_TO:
                if (resultCode == RESULT_OK) {
                    initData();
                    adapter.notifyDataSetChanged();
                }
                break;
        }
    }

    private void initData() {
        MeeToSelectDao dao = new MeeToSelectDao(context);
        List<ContactModel> check = dao.getFullName();
        String item = "0";
        for (int i = 0; i < check.size(); i++) {
            item = item + check.get(i).getId();
            if (i != check.size() - 1) {
                item = item + ", ";
            }
        }
        Log.d("TAG", "item meet to : " + item);
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(context)) {
            presenter.setupList(ApiParam.API_131, account_id, item);
        }

        ivBack.setOnClickListener(click);
        faAccountAdd.setOnClickListener(click);
    }

    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.ivBack:
                    finish();
                    break;
                case R.id.faAccountAdd:
                    Intent intent = new Intent(context, MeetToCreateActivity.class);
                    intent.putExtra("account_id", account_id);
                    startActivityForResult(intent, Sales1CRMUtils.LIST_MEET_TO);
                    break;
            }
        }
    };

    private void setList() {
        llshim.setVisibility(View.VISIBLE);
        shimmerLayout.setVisibility(View.GONE);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        lvMeetoo.setLayoutManager(layoutManager);
        lvMeetoo.setHasFixedSize(true);
        List<MeetoAdapter.Row> rows = new ArrayList<MeetoAdapter.Row>();
        for (ContactModel mt : list) {
            rows.add(new MeetoAdapter.Item(mt));
        }
        adapter.setRows(rows);
        lvMeetoo.setAdapter(adapter);
        llshim.setVisibility(View.INVISIBLE);
        shimmerLayout.stopShimmer();
        shimmerLayout.setVisibility(View.INVISIBLE);
    }

    @Override
    public void finishListMeetTo(String result, List<ContactModel> list) {
        if (result.equalsIgnoreCase("OK")) {
            this.list = list;
            adapter = new MeetoAdapter(context, this.list);
            setList();
            if (list.size() > 0) {
                llNotFound.setVisibility(View.GONE);
            } else {
                llNotFound.setVisibility(View.VISIBLE);
            }
        }
    }
}
