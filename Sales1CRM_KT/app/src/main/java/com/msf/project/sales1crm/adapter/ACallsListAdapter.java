package com.msf.project.sales1crm.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.msf.project.sales1crm.ACallsDetailActivity;
import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.model.ActivityCallsModel;

import java.util.List;

import butterknife.BindView;

public class ACallsListAdapter extends RecyclerView.Adapter<ACallsListAdapter.ACallsListHolder> {

    private Context context;
    private Bitmap icon;
    private String url;
    private List<ActivityCallsModel> activityCallsModelList;

    public ACallsListAdapter(Context context, List<ActivityCallsModel> object) {
        this.context = context;
        this.activityCallsModelList = object;
    }

    public static abstract class Row {

    }

    public static final class Section extends Row {
        public final String text;

        public Section(String text) {
            this.text = text;
        }
    }

    public static final class Item extends Row {
        public final ActivityCallsModel text;


        public Item(ActivityCallsModel text) {
            this.text = text;
        }

        public ActivityCallsModel getItem() {
            return text;
        }
    }

    private List<Row> rows;

    public void setRows(List<Row> rows) {
        this.rows = rows;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ACallsListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_a_calllist,null);
        return new ACallsListHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ACallsListHolder holder, int position) {
        final Item item = (Item) rows.get(position);
        holder.tvAccountName.setText(item.text.getAccount_name());
        holder.tvSubject.setText(item.text.getSubject());
        holder.tvDuration.setText(item.text.getDuration_interval());
        holder.tvDate.setText(item.text.getDate_interval());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    Intent intent = new Intent(context, ACallsDetailActivity.class);
                    intent.putExtra("aCallsModel", item.getItem());
                    context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return rows.size();
    }

    public class ACallsListHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.tvAccountName)
        CustomTextView tvAccountName;
        @BindView(R.id.tvSubject)
        CustomTextView tvSubject;
        @BindView(R.id.tvDuration)
        CustomTextView tvDuration;
        @BindView(R.id.tvDetail)
        CustomTextView tvDetail;
        @BindView(R.id.tvDate)
        CustomTextView tvDate;
        public ACallsListHolder(View view) {
            super(view);
            tvAccountName = (CustomTextView) view.findViewById(R.id.tvAccountName);
            tvSubject = (CustomTextView) view.findViewById(R.id.tvSubject);
            tvDuration = (CustomTextView) view.findViewById(R.id.tvDuration);
            tvDetail = (CustomTextView) view.findViewById(R.id.tvDetail);
            tvDate = (CustomTextView) view.findViewById(R.id.tvDate);
        }
    }
}
