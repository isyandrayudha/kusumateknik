package com.msf.project.sales1crm.model;

public class PriceModel {
    private String price;

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
