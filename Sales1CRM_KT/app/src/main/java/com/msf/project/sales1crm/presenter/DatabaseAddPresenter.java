package com.msf.project.sales1crm.presenter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;

import com.msf.project.sales1crm.callback.DatabaseMachineAddCallback;
import com.msf.project.sales1crm.model.DatabaseMachineModel;
import com.msf.project.sales1crm.service.NetworkConnection;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.PreferenceUtility;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;
import com.msf.project.sales1crm.utility.Sales1CRMUtilsJSON;

import org.json.JSONException;
import org.json.JSONObject;

public class DatabaseAddPresenter {
    private final String TAG = DatabaseMachineListPresenter.class.getSimpleName();
    private Context context;
    private Messenger messenger;
    private Message message;
    private Bundle bundle;
    private Handler handler;
    private String failureResponse = "";
    private String[] stringResponse = {""};
    private DatabaseMachineAddCallback callback;
    private String result = "NG";
    private String response = "Tolong Periksa Koneksi Anda";

    public DatabaseAddPresenter(Context context, DatabaseMachineAddCallback callback) {
        this.context = context;
        this.callback = callback;
    }

    private void doNetworkService(String url, String params, String from) {
        Log.d(TAG, "url login " + url);
        Log.d(TAG, "params : " + params);
        Intent networkIntent = new Intent(this.context, NetworkConnection.class);
        this.messenger = new Messenger(this.handler);
        networkIntent.putExtra("messenger", this.messenger);
        networkIntent.putExtra("url", url);
        networkIntent.putExtra("params", params);
        networkIntent.putExtra("from", from);
        this.context.startService(networkIntent);
    }

    public void setupAdd(int apiIndex, DatabaseMachineModel databaseMachineModel){
        if(Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)){
            obtainAdd(apiIndex, databaseMachineModel);
        } else {
            callback.finishAdd(this.result, this.response);
        }
    }

    public void obtainAdd(int apiIndex, DatabaseMachineModel databaseMachineModel){
        this.handler = new Handler(this.context.getMainLooper()){
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                parseAdd(msg);
            }
        };
        JSONObject jsonObject = new JSONObject();
        try{
            jsonObject.put("api_key", PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.API_KEY));
            jsonObject.put("account_id", databaseMachineModel.getAccountId());
            jsonObject.put("brand_id", databaseMachineModel.getBrandId());
            jsonObject.put("application", databaseMachineModel.getApplication());
            jsonObject.put("unit_no_in_customer_site", databaseMachineModel.getUnitNoInCustomerSite());
            jsonObject.put("type", databaseMachineModel.getType());
            jsonObject.put("model", databaseMachineModel.getModel());
            jsonObject.put("serial_number", databaseMachineModel.getSerialNumber());
            jsonObject.put("year_of_install", databaseMachineModel.getYearOfInstall());
            jsonObject.put("pressure_vacuum", databaseMachineModel.getPressureVacuum());
            jsonObject.put("capacity", databaseMachineModel.getCapacity());
            jsonObject.put("diff_pressure", databaseMachineModel.getDiffPressure());
            jsonObject.put("speed", databaseMachineModel.getSpeed());
            jsonObject.put("motor_specs", databaseMachineModel.getMotorSpecs());
            jsonObject.put("date_last_overhaul", databaseMachineModel.getDateLastOverhaul());
            jsonObject.put("type_of_service_done", databaseMachineModel.getTypeOfServiceDone());
            jsonObject.put("latest_inspection_date", databaseMachineModel.getLatestInspectionDate());
            jsonObject.put("running_hours", databaseMachineModel.getRunningHours());
            jsonObject.put("condition", databaseMachineModel.getCondition());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        doNetworkService(ApiParam.urlBuilder(this.context, apiIndex), jsonObject.toString(),"DBmachineAdd");
    }

    private void parseAdd(Message message){
        this.message = message;
        this.bundle = this.message.getData();
        this.stringResponse[0] = this.bundle.getString("network_response");
        this.failureResponse = this.bundle.getString("network_failure");
        Log.d(TAG, "responseString[0] info " + this.stringResponse[0]);
        if (this.failureResponse.equalsIgnoreCase("yes")){
            this.callback.finishAdd(this.result, this.response);
        }else{
            try {
                this.result = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                this.callback.finishAdd(this.result, this.response);
            } catch (JSONException e) {
                Log.d(TAG, "Exception detail Response " + e.getMessage());
            }
        }
    }

    public void setupEdit(int apiIndex, DatabaseMachineModel databaseMachineModel){
        if(Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)){
            obtainEdit(apiIndex, databaseMachineModel);
        }else {
            callback.finishEdit(this.result, this.response);
        }
    }

    public void obtainEdit(int apiIndex, DatabaseMachineModel databaseMachineModel){
        this.handler = new Handler(this.context.getMainLooper()){
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                parseEdit(msg);
            }
        };

        JSONObject jsonObject = new JSONObject();
        try{
            jsonObject.put("api_key", PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.API_KEY));
            jsonObject.put("database_machine_id",databaseMachineModel.getId());
            jsonObject.put("account_id", databaseMachineModel.getAccountId());
            jsonObject.put("brand_id", databaseMachineModel.getBrandId());
            jsonObject.put("application", databaseMachineModel.getApplication());
            jsonObject.put("unit_no_in_customer_site", databaseMachineModel.getUnitNoInCustomerSite());
            jsonObject.put("type", databaseMachineModel.getType());
            jsonObject.put("model", databaseMachineModel.getModel());
            jsonObject.put("serial_number", databaseMachineModel.getSerialNumber());
            jsonObject.put("year_of_install", databaseMachineModel.getYearOfInstall());
            jsonObject.put("pressure_vacuum", databaseMachineModel.getPressureVacuum());
            jsonObject.put("capacity", databaseMachineModel.getCapacity());
            jsonObject.put("diff_pressure", databaseMachineModel.getDiffPressure());
            jsonObject.put("speed", databaseMachineModel.getSpeed());
            jsonObject.put("motor_specs", databaseMachineModel.getMotorSpecs());
            jsonObject.put("date_last_overhaul", databaseMachineModel.getDateLastOverhaul());
            jsonObject.put("type_of_service_done", databaseMachineModel.getTypeOfServiceDone());
            jsonObject.put("latest_inspection_date", databaseMachineModel.getLatestInspectionDate());
            jsonObject.put("running_hours", databaseMachineModel.getRunningHours());
            jsonObject.put("condition", databaseMachineModel.getCondition());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        doNetworkService(ApiParam.urlBuilder(this.context, apiIndex), jsonObject.toString(),"DBmachineEdit");
    }

    private void parseEdit(Message message) {
        this.message = message;
        this.bundle = this.message.getData();
        this.stringResponse[0] = this.bundle.getString("network_response");
        this.failureResponse = this.bundle.getString("network_failure");
        Log.d(TAG, "responseString[0] info " + this.stringResponse[0]);
        if (this.failureResponse.equalsIgnoreCase("yes")){
            this.callback.finishEdit(this.result, this.response);
        }else{
            try {
                this.result = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                this.callback.finishEdit(this.result, this.response);
            } catch (JSONException e) {
                Log.d(TAG, "Exception detail Response " + e.getMessage());
            }
        }
    }



}
