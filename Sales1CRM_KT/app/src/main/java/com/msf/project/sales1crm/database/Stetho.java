package com.msf.project.sales1crm.database;

import android.app.Application;
import android.content.Context;



public class Stetho extends Application {

    private static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
        com.facebook.stetho.Stetho.initializeWithDefaults(this);
        com.facebook.stetho.Stetho.initialize(
                com.facebook.stetho.Stetho.newInitializerBuilder(this)
                        .enableDumpapp(
                                com.facebook.stetho.Stetho.defaultDumperPluginsProvider(this))
                .enableWebKitInspector(
                        com.facebook.stetho.Stetho.defaultInspectorModulesProvider(this))
                        .build());
    }

    private static Context getContext(){
        return mContext;
    }
}
