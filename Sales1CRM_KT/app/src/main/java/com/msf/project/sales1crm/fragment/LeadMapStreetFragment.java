package com.msf.project.sales1crm.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.github.clans.fab.FloatingActionButton;
import com.mapbox.android.core.location.LocationEngine;
import com.mapbox.android.core.location.LocationEngineListener;
import com.mapbox.android.core.location.LocationEnginePriority;
import com.mapbox.android.core.location.LocationEngineProvider;
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.annotations.Icon;
import com.mapbox.mapboxsdk.annotations.IconFactory;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.plugins.locationlayer.LocationLayerPlugin;
import com.mapbox.mapboxsdk.plugins.locationlayer.modes.CameraMode;
import com.mapbox.mapboxsdk.plugins.locationlayer.modes.RenderMode;
import com.msf.project.sales1crm.LeadAddActivity;
import com.msf.project.sales1crm.LeadDetailActivity;
import com.msf.project.sales1crm.MenuActivity;
import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.callback.LeadListCallback;
import com.msf.project.sales1crm.customview.CustomEditText;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.model.LeadModel;
import com.msf.project.sales1crm.presenter.LeadListPresenter;
import com.msf.project.sales1crm.service.GPSTracker;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class LeadMapStreetFragment extends Fragment implements OnMapReadyCallback, LocationEngineListener,
        PermissionsListener, MapboxMap.OnMapClickListener, LeadListCallback {


    Unbinder unbinder;

    @BindView(R.id.faMyLocation)
    FloatingActionButton faMyLocation;
    @BindView(R.id.tvset)
    CustomTextView tvset;
    @BindView(R.id.etSearch)
    CustomEditText etSearch;
    @BindView(R.id.ivClear)
    ImageView ivClear;
    @BindView(R.id.rlSearch)
    RelativeLayout rlSearch;
    @BindView(R.id.btn_map)
    ImageView btnMap;
    @BindView(R.id.LocationAccount)
    CustomTextView LocationAccount;
    @BindView(R.id.LeadAdd)
    CustomTextView LeadAdd;
    @BindView(R.id.rl_big)
    RelativeLayout rlBig;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.llNotFound)
    LinearLayout llNotFound;
    private MapView mapView;
    private MapboxMap mMapboxMap;
    private Context context;
    private LeadListPresenter leadListPresenter;
    private PermissionsManager permissionsManager;
    private LocationEngine locationEngine;
    private LocationLayerPlugin locationLayerPlugin;
    private Location originLocation;
    private List<LeadModel> list = new ArrayList<>();
    private Point originPosisition;
    private Marker destinationMarker;
    private Point destinationPosisition;
    private String stringLatitude, stringLongitude;
    private Menu menu;
    private String kecamatan, kabupaten, provinsi, kelurahan, alamat, jalan, no, latitude, longitude, postalCode;


    private int prevSize = 0, sort_id = 0;
    private boolean isLoadMore = true;
    private boolean isMaxSize = true;
    private boolean pause = false;

    public static LeadMapStreetFragment newInstance() {
        LeadMapStreetFragment fragment = new LeadMapStreetFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_lead_mapstreet, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeThisFragment(savedInstanceState);

        this.context = getActivity();
        leadListPresenter = new LeadListPresenter(context, this);

        getDataFromAPI(0, sort_id);

        LeadAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, LeadAddActivity.class);
                intent.putExtra("alamat", alamat);
                intent.putExtra("jalan", jalan);
                intent.putExtra("no", no);
                intent.putExtra("provinsi", provinsi);
                intent.putExtra("kabupaten", kabupaten);
                intent.putExtra("kecamatan", kecamatan);
                intent.putExtra("kelurahan", kelurahan);
                intent.putExtra("postalCode", postalCode);
                intent.putExtra("latitude", latitude);
                intent.putExtra("longitude", longitude);
                startActivity(intent);
            }
        });


    }

    private void initializeThisFragment(Bundle savedInstanceState) {
        Mapbox.getInstance(getActivity(), getString(R.string.key_access_token_development));
        mapView = getView().findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.setStyleUrl(getResources().getString(R.string.layer_streets) + "?auth=" + getResources().getString(R.string.key_access_token_development));
        mapView.getMapAsync(this);

    }



    private void getDataFromAPI(int prev, int sort_id) {
        isMaxSize = true;
        isLoadMore = true;
        llNotFound.setVisibility(View.GONE);

        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(context)) {
            GPSTracker gpsTracker = new GPSTracker(context);
            if (gpsTracker.canGetLocation()) {
                stringLatitude = String.valueOf(gpsTracker.latitude);
                stringLongitude = String.valueOf(gpsTracker.longitude);

                pause = false;
                leadListPresenter.setupList(ApiParam.API_030, prev, sort_id, etSearch.getText().toString(), stringLatitude, stringLongitude);
            } else {
                gpsTracker.showSettingsAlert();
            }
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menus, menu);
        this.menu = menu;
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_list:
                MenuActivity.getInstance().setFragment(LeadFragment.newInstance());
                menu.getItem(0).setIcon(ContextCompat.getDrawable(getActivity(), R.drawable.ic_list_white_24dp));
//                item.setIcon(R.drawable.ic_list_white_24dp);
                break;
            case R.id.action_cards:
                MenuActivity.getInstance().setFragment(LeadCardFragment.newInstance());
                menu.getItem(0).setIcon(ContextCompat.getDrawable(getActivity(), R.drawable.ic_cards_black_24dp));
//                item.setIcon(R.drawable.ic_cards_black_24dp);
                break;
            case R.id.action_maps:
                MenuActivity.getInstance().setFragment(LeadMapStreetFragment.newInstance());
                menu.getItem(0).setIcon(ContextCompat.getDrawable(getActivity(), R.drawable.ic_map_white_24dp));
//                item.setIcon(R.drawable.ic_map_white_24dp);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public void onMapReady(MapboxMap mapboxMap) {
        mMapboxMap = mapboxMap;
        mMapboxMap.addOnMapClickListener(this);


        faMyLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMapboxMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(originLocation.getLatitude()
                        , originLocation.getLongitude()), 12.0));
            }
        });

        enabledLocation();

    }

    private void enabledLocation() {
        if (PermissionsManager.areLocationPermissionsGranted(getActivity())) {
            initializedLocationEnginering();
            intializedLocationLayer();
        } else {
            permissionsManager = new PermissionsManager(this);
            permissionsManager.requestLocationPermissions(getActivity());
        }
    }

    @SuppressWarnings("MissingPermission")
    private void initializedLocationEnginering() {
        locationEngine = new LocationEngineProvider(getActivity()).obtainBestLocationEngineAvailable();
        locationEngine.setPriority(LocationEnginePriority.HIGH_ACCURACY);
        locationEngine.activate();

        Location lastLocation = locationEngine.getLastLocation();
        if (lastLocation != null) {
            originLocation = lastLocation;
            setCameraPosition(lastLocation);
        } else {
            locationEngine.addLocationEngineListener(this);
        }
    }

    @SuppressWarnings("MissingPermission")
    private void intializedLocationLayer() {
        locationLayerPlugin = new LocationLayerPlugin(mapView, mMapboxMap, locationEngine);
        locationLayerPlugin.setLocationLayerEnabled(true);
        locationLayerPlugin.setCameraMode(CameraMode.TRACKING);
        locationLayerPlugin.setRenderMode(RenderMode.NORMAL);
    }

    private void setCameraPosition(Location location) {
        mMapboxMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude()
                , location.getLongitude()), 8.0));

        Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(originLocation.getLatitude(), originLocation.getLongitude(), 1);
            if (addresses != null && addresses.size() > 0) {
                alamat = addresses.get(0).getAddressLine(0);
                jalan = addresses.get(0).getThoroughfare();
                no = addresses.get(0).getSubThoroughfare();
                kecamatan = addresses.get(0).getLocality();
                String country = addresses.get(0).getCountryName();
                postalCode = addresses.get(0).getPostalCode();
                String knowName = addresses.get(0).getFeatureName();
                kelurahan = addresses.get(0).getSubLocality();
                provinsi = addresses.get(0).getAdminArea();
                kabupaten = addresses.get(0).getSubAdminArea();
                latitude = String.valueOf(addresses.get(0).getLatitude());
                longitude = String.valueOf(addresses.get(0).getLongitude());

                progressBar.setVisibility(View.VISIBLE);
                LocationAccount.setText(alamat);
                LocationAccount.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
            }
            if  (list.size() > 0) {
                llNotFound.setVisibility(View.GONE);
            } else {
                llNotFound.setVisibility(View.VISIBLE);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        ivClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mMapboxMap.clear();
                List<Address> addresses;
                try {
                    addresses = geocoder.getFromLocationName(etSearch.getText().toString(), 1);
                    if (addresses.size() > 0) {
                        Double lat = (double) (addresses.get(0).getLatitude());
                        Double lon = (double) (addresses.get(0).getLongitude());

                        final LatLng user = new LatLng(lat, lon);
                        IconFactory iconFactory = IconFactory.getInstance(context);
                        Icon icon = iconFactory.fromResource(R.drawable.mapicon);
                        mMapboxMap.addMarker(new MarkerOptions()
                                .position(user)
                                .title(String.valueOf(etSearch.getText().toString()))
                                .icon(icon));

                        mMapboxMap.moveCamera(CameraUpdateFactory.newLatLngZoom(user, 15));
                        mMapboxMap.animateCamera(CameraUpdateFactory.zoomBy(10), 2000, null);
                    }

                    Geocoder geocoder1 = new Geocoder(getActivity(), Locale.getDefault());
                    try {
                        List<Address> addresses1 = geocoder1.getFromLocation(addresses.get(0).getLatitude(), addresses.get(0).getLongitude(), 1);
                        if (addresses1 != null && addresses1.size() > 0) {
                            alamat = addresses.get(0).getAddressLine(0);
                            jalan = addresses.get(0).getThoroughfare();
                            no = addresses.get(0).getSubThoroughfare();
                            kecamatan = addresses.get(0).getLocality();
                            String country = addresses.get(0).getCountryName();
                            postalCode = addresses.get(0).getPostalCode();
                            String knowName = addresses.get(0).getFeatureName();
                            kelurahan = addresses.get(0).getSubLocality();
                            provinsi = addresses.get(0).getAdminArea();
                            kabupaten = addresses.get(0).getSubAdminArea();
                            latitude = String.valueOf(addresses.get(0).getLatitude());
                            longitude = String.valueOf(addresses.get(0).getLongitude());

                            progressBar.setVisibility(View.VISIBLE);
                            LocationAccount.setText(alamat);
                            LocationAccount.setVisibility(View.VISIBLE);
                            progressBar.setVisibility(View.GONE);

                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    @Override
    @SuppressWarnings("MissingPermission")
    public void onConnected() {
        locationEngine.requestLocationUpdates();
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            originLocation = location;
            setCameraPosition(location);
        }
    }

    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {

    }

    @Override
    public void onPermissionResult(boolean granted) {
        if (granted) {
            enabledLocation();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }


    @Override
    public void onMapClick(@NonNull LatLng point) {
        if (destinationMarker != null) {
            mMapboxMap.removeMarker(destinationMarker);
        }
        IconFactory iconFactory = IconFactory.getInstance(context);
        Icon icon = iconFactory.fromResource(R.drawable.mapicon);
        destinationMarker = mMapboxMap.addMarker(new MarkerOptions()
                .position(point).icon(icon));
        Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(point.getLatitude(), point.getLongitude(), 1);
            if (addresses != null && addresses.size() > 0) {
                alamat = addresses.get(0).getAddressLine(0);
                jalan = addresses.get(0).getThoroughfare();
                no = addresses.get(0).getSubThoroughfare();
                kecamatan = addresses.get(0).getLocality();
                String country = addresses.get(0).getCountryName();
                postalCode = addresses.get(0).getPostalCode();
                String knowName = addresses.get(0).getFeatureName();
                kelurahan = addresses.get(0).getSubLocality();
                provinsi = addresses.get(0).getAdminArea();
                kabupaten = addresses.get(0).getSubAdminArea();
                latitude = String.valueOf(addresses.get(0).getLatitude());
                longitude = String.valueOf(addresses.get(0).getLongitude());


                progressBar.setVisibility(View.VISIBLE);
                LocationAccount.setText(alamat);
                LocationAccount.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);


            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    @Override
    public void finishLeadList(String result, List<LeadModel> list) {
//        Log.i("List map", list.get(0).getAddress());
        mapView.getMapAsync((MapboxMap mapboxMap) -> {
            mapboxMap.setMaxZoomPreference(18);
            LatLng[] latLngs = new LatLng[list.size()];

            LeadModel[] leadModels = new LeadModel[list.size()];
            for (int i = 0; i < list.size(); i++) {
                try {
                    JSONObject jsonObject = new JSONObject(list.get(i).getAddress());
                    Log.e("Size Map", String.valueOf(jsonObject.length()));
                    latLngs[i] = new LatLng(Double.parseDouble(list.get(i).getLatitude()),
                            Double.parseDouble(list.get(i).getLongitude()));
                    leadModels[i] = list.get(i);
                    mapboxMap.addMarker(new MarkerOptions()
                            .position(latLngs[i])
                            .title(list.get(i).getLast_name())
                            .snippet(jsonObject.getString("street")));


                } catch (Exception e) {
                    e.printStackTrace();;
                }
                mapboxMap.setOnInfoWindowClickListener(new MapboxMap.OnInfoWindowClickListener() {
                    @Override
                    public boolean onInfoWindowClick(@NonNull Marker marker) {
                        for (int i = 0; i < list.size(); i++) {
                            if (marker.getPosition().equals(latLngs[i])) {
                                Intent intent = new Intent(context, LeadDetailActivity.class);
                                intent.putExtra("leadModel", leadModels[i]);
                                startActivity(intent);
                            }
                        }
                        return false;
                    }
                });



            }

        });
        if  (list.size() > 0) {
            llNotFound.setVisibility(View.GONE);
        } else {
            llNotFound.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void finishLeadMoreList(String result, List<LeadModel> list) {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mapView.onDestroy();
        unbinder.unbind();
    }
}
