package com.msf.project.sales1crm.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.msf.project.sales1crm.model.AccountTypeModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by christianmacbook on 24/05/18.
 */

public class AccountTypeDao extends BaseDao<AccountTypeModel> implements DBSchema.AccountType {

    public AccountTypeDao(Context c) {
        super(c, TABLE_NAME);
    }

    public AccountTypeDao(Context c, boolean willWrite) {
        super(c, TABLE_NAME, willWrite);
    }

    public AccountTypeDao(DBHelper db) {
        super(db, TABLE_NAME);
    }

    public AccountTypeDao(DBHelper db, boolean willWrite) {
        super(db, TABLE_NAME, willWrite);
    }

    public AccountTypeModel getById(int id) {
        String qry = "SELECT * FROM " + getTable() + " WHERE " + COL_ID + " = " + id;
        Cursor c = getSqliteDb().rawQuery(qry, null);
        AccountTypeModel model = new AccountTypeModel();
        try {
            if(c != null && c.moveToFirst()) {
                model = getByCursor(c);
            }
        } finally {
            c.close();
        }
        return model;
    }

    public List<AccountTypeModel> getAccountType() {
        String query = "SELECT * FROM " + getTable();
        Cursor c = getSqliteDb().rawQuery(query, null);
        List<AccountTypeModel> list = new ArrayList<>();
        try {
            if(c != null && c.moveToFirst()) {
                list.add(getByCursor(c));
                while (c.moveToNext()) {
                    list.add(getByCursor(c));

                }
            }
        } finally {
            c.close();
        }
        return list;
    }

    @Override
    public AccountTypeModel getByCursor(Cursor c) {
        AccountTypeModel model = new AccountTypeModel();
        model.setId(c.getString(0));
        model.setName(c.getString(1));
        return model;
    }

    @Override
    protected ContentValues upDataValues(AccountTypeModel model, boolean update) {
        ContentValues cv = new ContentValues();
        if (update == true)
            cv.put(COL_ID, model.getId());
        cv.put(KEY_NAME, model.getName());
        return cv;
    }
}
