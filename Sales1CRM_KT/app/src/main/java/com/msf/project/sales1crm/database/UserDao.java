package com.msf.project.sales1crm.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.msf.project.sales1crm.model.UserModel;

import java.util.ArrayList;
import java.util.List;

public class UserDao extends BaseDao<UserModel> implements DBSchema.User {

    public UserDao(Context c) {
        super(c, TABLE_NAME);
    }

    public UserDao(Context c, boolean willWrite) {
        super(c, TABLE_NAME, willWrite);
    }

    public UserDao(DBHelper db) {
        super(db, TABLE_NAME);
    }

    public UserDao(DBHelper db, boolean willWrite) {
        super(db, TABLE_NAME, willWrite);
    }

    public UserModel getById(int id) {
        String qry = "SELECT * FROM " + getTable() + " WHERE " + COL_ID + " = " + id;
        Cursor c = getSqliteDb().rawQuery(qry, null);
        UserModel model = new UserModel();
        try {
            if(c != null && c.moveToFirst()) {
                model = getByCursor(c);
            }
        } finally {
            c.close();
        }
        return model;
    }

    public List<UserModel> getUserList() {
        String query = "SELECT * FROM " + getTable();

        Cursor c = getSqliteDb().rawQuery(query, null);
        List<UserModel> list = new ArrayList<>();
        try {
            if(c != null && c.moveToFirst()) {
                list.add(getByCursor(c));
                while (c.moveToNext()) {
                    list.add(getByCursor(c));

                }
            }
        } finally {
            c.close();
        }
        return list;
    }

    public List<UserModel> getUserSearchList(String search) {
        String query = "SELECT * FROM " + getTable() + " WHERE " + DBSchema.User.KEY_NAME + " LIKE '%"+search+"%'";

        Cursor c = getSqliteDb().rawQuery(query, null);
        List<UserModel> list = new ArrayList<>();
        try {
            if(c != null && c.moveToFirst()) {
                list.add(getByCursor(c));
                while (c.moveToNext()) {
                    list.add(getByCursor(c));

                }
            }
        } finally {
            c.close();
        }
        return list;
    }

    @Override
    public UserModel getByCursor(Cursor c) {
        UserModel model = new UserModel();
        model.setId(c.getString(1));
        model.setFullname(c.getString(2));
        return model;
    }

    @Override
    protected ContentValues upDataValues(UserModel model, boolean update) {
        ContentValues cv = new ContentValues();
        cv.put(KEY_USER_ID, model.getId());
        cv.put(KEY_NAME, model.getFullname());
        return cv;
    }
}
