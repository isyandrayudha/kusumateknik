package com.msf.project.sales1crm.callback;

import com.msf.project.sales1crm.model.TotalModel;

import java.util.List;

public interface TotalLeadsCallback {
    void finished(String result, List<TotalModel> listLead, List<TotalModel> listLead1);
}
