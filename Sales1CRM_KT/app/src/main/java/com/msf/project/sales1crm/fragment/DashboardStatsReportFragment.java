package com.msf.project.sales1crm.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.msf.project.sales1crm.MenuActivity;
import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.adapter.DashboardStatsReportTabAdapter;
import com.msf.project.sales1crm.customview.CustomTextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DashboardStatsReportFragment extends Fragment {


    @BindView(R.id.tabStatsReport)
    TabLayout tabStatsReport;

    @BindView(R.id.vpStatsReport)
    ViewPager vpStatsReport;

    @BindView(R.id.rlViewReport)
    RelativeLayout rlViewReport;

    private View view;
    private Context context;
    private DashboardStatsReportTabAdapter adapter;
    private MenuActivity myContext;

    public static DashboardStatsReportFragment newInstance() {
        DashboardStatsReportFragment fragment = new DashboardStatsReportFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Activity activity) {
        myContext = (MenuActivity) activity;
        super.onAttach(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_dashboardstatsreport, container, false);

        return this.view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);
        this.context = getActivity();

        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Stats Report");

        initData();
    }

    private void initData(){
        rlViewReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_layout, StatsReportFragment.newInstance());
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                transaction.commit();
            }
        });

        CustomTextView tabOne = (CustomTextView) LayoutInflater.from(context).inflate(R.layout.custom_tab, null);
        tabOne.setText("General Dashboard");
        tabStatsReport.addTab(tabStatsReport.newTab().setCustomView(tabOne));

        CustomTextView tabTwo = (CustomTextView) LayoutInflater.from(context).inflate(R.layout.custom_tab, null);
        tabTwo.setText("Lead Dashboard");
        tabStatsReport.addTab(tabStatsReport.newTab().setCustomView(tabTwo));

        CustomTextView tabThree = (CustomTextView) LayoutInflater.from(context).inflate(R.layout.custom_tab, null);
        tabThree.setText("opportunity Dashboard");
        tabStatsReport.addTab(tabStatsReport.newTab().setCustomView(tabThree));

        CustomTextView tabFour = (CustomTextView) LayoutInflater.from(context).inflate(R.layout.custom_tab, null);
        tabFour.setText("Activities Dashboard");
        tabStatsReport.addTab(tabStatsReport.newTab().setCustomView(tabFour));

        CustomTextView tabFive = (CustomTextView) LayoutInflater.from(context).inflate(R.layout.custom_tab, null);
        tabFive.setText("Product Dashboard");
        tabStatsReport.addTab(tabStatsReport.newTab().setCustomView(tabFive));

        CustomTextView tabSix = (CustomTextView) LayoutInflater.from(context).inflate(R.layout.custom_tab, null);
        tabSix.setText("KPI Dashboard");
        tabStatsReport.addTab(tabStatsReport.newTab().setCustomView(tabSix));

        CustomTextView tabSeven = (CustomTextView) LayoutInflater.from(context).inflate(R.layout.custom_tab, null);
        tabSeven.setText("After Sales Dashboard");
        tabStatsReport.addTab(tabStatsReport.newTab().setCustomView(tabSeven));

        tabStatsReport.setTabMode(TabLayout.MODE_SCROLLABLE);

        adapter = new DashboardStatsReportTabAdapter(myContext.getSupportFragmentManager(), tabStatsReport.getTabCount());
        vpStatsReport.setAdapter(adapter);
        vpStatsReport.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabStatsReport));
        tabStatsReport.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                vpStatsReport.setCurrentItem(tab.getPosition(), true);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }
}
