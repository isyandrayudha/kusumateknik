package com.msf.project.sales1crm.service;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

public class PhonecallReceiver extends BroadcastReceiver {

    private static final String TAG = "TAGG";
    Context mContext;
    String incoming_nr;
    private int prev_state;

    @SuppressLint("LongLogTag")
    @Override
    public void onReceive(Context context, Intent intent) {
        TelephonyManager telephony = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE); //TelephonyManager object
        CustomPhoneStateListener customPhoneListener = new CustomPhoneStateListener();
        telephony.listen(customPhoneListener, PhoneStateListener.LISTEN_CALL_STATE); //Register our listener with TelephonyManager

        Bundle bundle = intent.getExtras();
        String phoneNr= bundle.getString("incoming_number");
        Log.v(TAG, "phoneNr: "+phoneNr);
        mContext=context;
    }

    /* Custom PhoneStateListener */
    public class CustomPhoneStateListener  extends PhoneStateListener {

        private static final String TAG = "TAGG";

        @SuppressLint("LongLogTag")
        @Override
        public void onCallStateChanged(int state, String incomingNumber){

            if(incomingNumber!=null&&incomingNumber.length()>0) incoming_nr=incomingNumber;

            Log.d("TAGG", "STATE : " + state);
            Log.d("TAGG", "PREV STATE : " + prev_state);

            switch(state){
                case TelephonyManager.CALL_STATE_RINGING:
                    Log.d(TAG, "CALL_STATE_RINGING");
                    prev_state=state;
                    break;
                case TelephonyManager.CALL_STATE_OFFHOOK:
                    Log.d(TAG, "CALL_STATE_OFFHOOK");
                    prev_state=state;
                    break;
                case TelephonyManager.CALL_STATE_IDLE:
                    if (prev_state==TelephonyManager.CALL_STATE_IDLE){
//                        prev_state=state;

                        Intent x = new Intent("android.intent.action.PHONE");
                        mContext.sendBroadcast(x);

                    }else if(prev_state==TelephonyManager.CALL_STATE_OFFHOOK){
//                        prev_state=state;

                        Intent x = new Intent("android.intent.action.PHONE");
                        mContext.sendBroadcast(x);
                        //Answered Call which is ended
                    }else if(prev_state==TelephonyManager.CALL_STATE_RINGING){
//                        prev_state=state;
                        //Rejected or Missed call
                    }
                    break;

            }
        }
    }
}
