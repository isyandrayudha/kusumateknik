package com.msf.project.sales1crm.model;

/**
 * Created by christianmacbook on 24/05/18.
 */

public class PositionModel {
    private String id;
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
