package com.msf.project.sales1crm.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.msf.project.sales1crm.model.UseForModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by christianmacbook on 25/05/18.
 */

public class UseForDao extends BaseDao<UseForModel> implements DBSchema.UseFor {

    public UseForDao(Context c) {
        super(c, TABLE_NAME);
    }

    public UseForDao(Context c, boolean willWrite) {
        super(c, TABLE_NAME, willWrite);
    }

    public UseForDao(DBHelper db) {
        super(db, TABLE_NAME);
    }

    public UseForDao(DBHelper db, boolean willWrite) {
        super(db, TABLE_NAME, willWrite);
    }

    public UseForModel getById(int id) {
        String qry = "SELECT * FROM " + getTable() + " WHERE " + COL_ID + " = " + id;
        Cursor c = getSqliteDb().rawQuery(qry, null);
        UseForModel model = new UseForModel();
        try {
            if(c != null && c.moveToFirst()) {
                model = getByCursor(c);
            }
        } finally {
            c.close();
        }
        return model;
    }

    public List<UseForModel> getFromList() {
        String query = "SELECT * FROM " + getTable();

        Cursor c = getSqliteDb().rawQuery(query, null);
        List<UseForModel> list = new ArrayList<>();
        try {
            if(c != null && c.moveToFirst()) {
                list.add(getByCursor(c));
                while (c.moveToNext()) {
                    list.add(getByCursor(c));

                }
            }
        } finally {
            c.close();
        }
        return list;
    }

    public String[] getStringArray(){
        String query = "SELECT * FROM " + getTable();

        Cursor c = getSqliteDb().rawQuery(query, null);
        String[] array = new String[c.getCount() + 1];
        try {
            if(c != null && c.moveToFirst()) {
                array[0] = "Kategori";
                array[1] = getByCursor(c).getName();
                int i = 2;
                while (c.moveToNext()) {
                    array[i] = getByCursor(c).getName();
                    i++;

                }
            }
        } finally {
            c.close();
        }

        return array;
    }

    public String[] getStringArrayID(){
        String query = "SELECT * FROM " + getTable();

        Cursor c = getSqliteDb().rawQuery(query, null);
        String[] array = new String[c.getCount() + 1];
        try {
            if(c != null && c.moveToFirst()) {
                array[0] = "0";
                array[1] = getByCursor(c).getId();
                int i = 2;
                while (c.moveToNext()) {
                    array[i] = getByCursor(c).getId();
                    i++;

                }
            }
        } finally {
            c.close();
        }

        return array;
    }

    @Override
    public UseForModel getByCursor(Cursor c) {
        UseForModel model = new UseForModel();
        model.setId(c.getString(0));
        model.setName(c.getString(1));
        return model;
    }

    @Override
    protected ContentValues upDataValues(UseForModel model, boolean update) {
        ContentValues cv = new ContentValues();
        if (update == true)
            cv.put(COL_ID, model.getId());
        cv.put(KEY_NAME, model.getName());
        return cv;
    }
}
