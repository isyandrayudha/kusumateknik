package com.msf.project.sales1crm.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.msf.project.sales1crm.CallsAddActivity;
import com.msf.project.sales1crm.ErrorAPIKeyActivity;
import com.msf.project.sales1crm.EventAddActivity;
import com.msf.project.sales1crm.MenuActivity;
import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.TaskAddActivity;
import com.msf.project.sales1crm.adapter.ActivityCallsAdapter;
import com.msf.project.sales1crm.adapter.ActivityEventListAdapter;
import com.msf.project.sales1crm.adapter.ActivityTodoAdapter;
import com.msf.project.sales1crm.adapter.TodoAdapter;
import com.msf.project.sales1crm.callback.ActivityListCallback;
import com.msf.project.sales1crm.callback.DashboardCallback;
import com.msf.project.sales1crm.callback.Tcallback;
import com.msf.project.sales1crm.customview.CustomEditText;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.database.MeeToSelectDao;
import com.msf.project.sales1crm.imageloader.ImageLoader;
import com.msf.project.sales1crm.model.AbsenceModel;
import com.msf.project.sales1crm.model.ActivityCallsModel;
import com.msf.project.sales1crm.model.ActivityEventModel;
import com.msf.project.sales1crm.model.ActivityTodoModel;
import com.msf.project.sales1crm.model.DashboardFirstModel;
import com.msf.project.sales1crm.model.DashboardSecondModel;
import com.msf.project.sales1crm.model.DashboardThirdModel;
import com.msf.project.sales1crm.model.TotalOpportunityModel;
import com.msf.project.sales1crm.presenter.ActivityPresenter;
import com.msf.project.sales1crm.presenter.ActivityTodoListPresenter;
import com.msf.project.sales1crm.presenter.DashboardPresenter;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ActivityListFragment extends Fragment implements ActivityListCallback, Tcallback, DashboardCallback {


    @BindView(R.id.etSearch)
    CustomEditText etSearch;
    @BindView(R.id.ivClear)
    ImageView ivClear;
    @BindView(R.id.tvRefresh)
    CustomTextView tvRefresh;
    @BindView(R.id.rlNoConnection)
    RelativeLayout rlNoConnection;
    @BindView(R.id.lcEvent)
    RecyclerView lcEvent;
    @BindView(R.id.lcTodo)
    RecyclerView lcTodo;
    @BindView(R.id.lcCalls)
    RecyclerView lcCalls;
    Unbinder unbinder;
    @BindView(R.id.event)
    CustomTextView event;
    @BindView(R.id.faEventAdd)
    FloatingActionButton faEventAdd;
    @BindView(R.id.faTodoAdd)
    FloatingActionButton faTodoAdd;
    @BindView(R.id.faCallAdd)
    FloatingActionButton faCallAdd;
    @BindView(R.id.menu)
    FloatingActionMenu menu;
    @BindView(R.id.shimmerLayout)
    ShimmerFrameLayout shimmerLayout;
    @BindView(R.id.llshim)
    LinearLayout llshim;
    @BindView(R.id.llNotFound)
    LinearLayout llNotFound;
    @BindView(R.id.swipeRefresh)
    SwipeRefreshLayout swipeRefresh;
    @BindView(R.id.tvNoDataEvent)
    CustomTextView tvNoDataEvent;
    @BindView(R.id.tvNoDataTodo)
    CustomTextView tvNoDataTodo;
    @BindView(R.id.tvNoDataCalls)
    CustomTextView tvNoDataCalls;
    private View view;
    private Context context;

    private ActivityEventListAdapter activityEventListAdapter;
    private ActivityTodoAdapter activityTodoAdapter;
    private ActivityCallsAdapter activityCallsAdapter;
    private DashboardPresenter dashboardPresenter;


    private TodoAdapter todoAdapter;
    private ActivityPresenter presenter;
    private ActivityTodoListPresenter todoPresneter;

    private List<ActivityEventModel> activityEventModelList = new ArrayList<>();
    private List<ActivityTodoModel> aTodoModelList = new ArrayList<>();
    private List<ActivityCallsModel> activityCallsModelList = new ArrayList<>();


    private String date_selected = "";
    private Calendar calendar;

    private int prevSize = 0, sort_id = 0;
    private boolean isLoadMore = true;
    private boolean isMaxSize = true;
    private boolean pause = false;

    private boolean isScrolling = false;
    private int currentItems, totalItems, scrollOutItems;

    public static ActivityListFragment newInstance() {
        ActivityListFragment fragment = new ActivityListFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_activitylist, container, false);
        ButterKnife.bind(this, this.view);
        this.context= getActivity();
        setHasOptionsMenu(true);
        return this.view;
    }



    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.context = getActivity();
        this.presenter = new ActivityPresenter(context, this);
        this.todoPresneter = new ActivityTodoListPresenter(context, this);
        this.dashboardPresenter = new DashboardPresenter(context, this);
        lcTodo.setNestedScrollingEnabled(false);
        lcEvent.setNestedScrollingEnabled(false);
        lcCalls.setNestedScrollingEnabled(false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Activity");
        initData();

    }

//    @Override
//    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        inflater.inflate(R.menu.menu_item, menu);
//        super.onCreateOptionsMenu(menu, inflater);
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        int id = item.getItemId();
//        switch (id) {
//            case R.id.action_list:
//                MenuActivity.getInstance().setFragment(ActivityListFragment.newInstance());
//                break;
//            case R.id.action_cards:
//                MenuActivity.getInstance().setFragment(ActivityListTabFragment.newInstance());
//                break;
//            default:
//                return super.onOptionsItemSelected(item);
//        }
//        return true;
//    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    private void initData() {
        Sales1CRMUtils.Utils.imageLoader = new ImageLoader(context);
        etSearch.clearFocus();
        getDataActivity();
        setTextWatcherForSearch();
        ivClear.setOnClickListener(click);
        tvRefresh.setOnClickListener(click);


        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                llshim.setVisibility(View.VISIBLE);
                shimmerLayout.setVisibility(View.VISIBLE);
                shimmerLayout.startShimmer();
                getDataActivity();
            }
        });
    }

    private void setTextWatcherForSearch() {
        etSearch.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    if (etSearch.getText().length() == 0) {
                        ivClear.setVisibility(View.VISIBLE);
                    }
                }
            }
        });

        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_GO) {
                    getDataActivity();
                }

                return true;
            }
        });

    }


    private void getDataActivity() {
        isMaxSize = true;
        isLoadMore = true;
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(context)) {
            dashboardPresenter.setupFirst(ApiParam.API_005);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    presenter.setupListEvent(ApiParam.API_107, etSearch.getText().toString());
                }
            }, 1000);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    todoPresneter.setupList(ApiParam.API_108, etSearch.getText().toString());
                }
            }, 2000);

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    presenter.setupCallList(ApiParam.API_109, etSearch.getText().toString());
                }
            }, 3000);

        } else {
            swipeRefresh.setVisibility(View.GONE);
        }
    }


//    private void checkActivity() {
//        if (activityEventModelList.size() == 0 && aTodoModelList.size() == 0 && activityCallsModelList.size() == 0) {
//            llNotFound.setVisibility(View.VISIBLE);
//        } else {
//            llNotFound.setVisibility(View.GONE);
//        }
//    }


    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.ivClear:
                    etSearch.setText("");
                    etSearch.clearFocus();
                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);
                    getDataActivity();
                    break;
                case R.id.tvRefresh:
                    llshim.setVisibility(View.VISIBLE);
                    rlNoConnection.setVisibility(View.GONE);
                    getDataActivity();
                    break;
            }
        }
    };

    private void setEvenList() {
        llshim.setVisibility(View.VISIBLE);
        shimmerLayout.startShimmer();
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        lcEvent.setLayoutManager(layoutManager);
        lcEvent.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {

                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                currentItems = layoutManager.getChildCount();
                totalItems = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstVisibleItemPosition();

                if (isScrolling && (currentItems + scrollOutItems == totalItems)) {
                    isScrolling = false;
                    fetchData();

                }
            }
        });

//        activityEventListAdapter = new ActivityEventListAdapter(context, this.activityEventModelList);

        List<ActivityEventListAdapter.Row> rows = new ArrayList<>();
        for (ActivityEventModel activityEventModel : activityEventModelList) {
            rows.add(new ActivityEventListAdapter.Item(activityEventModel));
        }
        activityEventListAdapter.setRows(rows);
        lcEvent.setAdapter(activityEventListAdapter);
        llshim.setVisibility(View.INVISIBLE);
        shimmerLayout.stopShimmer();
        shimmerLayout.setVisibility(View.INVISIBLE);
    }

    private void fetchData() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                presenter.setupMoreListEvent(ApiParam.API_107, etSearch.getText().toString());
                activityEventListAdapter.notifyDataSetChanged();
            }
        }, 5000);
    }

    private void setEvenMoreList() {
        List<ActivityEventListAdapter.Row> rows = new ArrayList<>();
        for (ActivityEventModel activityEventModel : activityEventModelList) {
            rows.add(new ActivityEventListAdapter.Item(activityEventModel));
        }
        activityEventListAdapter.setRows(rows);
        activityEventListAdapter.notifyDataSetChanged();

    }

    @Override
    public void finishActivityEventList(String result, List<ActivityEventModel> activityEventModelList) {
        swipeRefresh.setRefreshing(false);
        if (result.equalsIgnoreCase("OK")) {
            prevSize = activityEventModelList.size();
            this.activityEventModelList = activityEventModelList;

            Log.e("EVENT LIST", activityEventModelList.toString());

            isMaxSize = false;
            isLoadMore = false;

            activityEventListAdapter = new ActivityEventListAdapter(context, this.activityEventModelList);
            setEvenList();
            if (activityEventModelList.size() > 0) {
                tvNoDataEvent.setVisibility(View.GONE);
            } else {
                tvNoDataEvent.setVisibility(View.VISIBLE);
            }
        }else if (result.equalsIgnoreCase("FAILED_KEY")) {
                startActivity(new Intent(context, ErrorAPIKeyActivity.class));
                MenuActivity.getInstance().finish();
            } else {
//                checkActivity();
//                rlNoConnection.setVisibility(View.VISIBLE);

            }
        }
    @Override
    public void finishActivityEventMoreList(String result, List<ActivityEventModel> activityEventModelList) {
        if (result.equals("OK")) {
            for (int i = 0; i < activityEventModelList.size(); i++) {
                this.activityEventModelList.add(activityEventModelList.get(i));
            }

            if (this.prevSize == this.activityEventModelList.size())
                isMaxSize = true;
            this.prevSize = this.activityEventModelList.size();
            setEvenMoreList();

        }
        this.isLoadMore = false;
    }


    @Override
    public void finishActivityTodoList(String result, List<ActivityTodoModel> activityTodoModelList) {

    }

    @Override
    public void finishActivityTodoMoreList(String result, List<ActivityTodoModel> activityTodoModelList) {
    }

    private void setCallsList() {
        final LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        lcCalls.setLayoutManager(layoutManager);
        lcCalls.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {

                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                currentItems = layoutManager.getChildCount();
                totalItems = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstVisibleItemPosition();

                if (isScrolling && (currentItems + scrollOutItems == totalItems)) {
                    isScrolling = false;
                    fetchDataCalls();
                }
            }


        });

//        activityCallsAdapter = new ActivityCallsAdapter(context, this.activityCallsModelList);
        List<ActivityCallsAdapter.Row> rows = new ArrayList<>();
        for (ActivityCallsModel activityCallsModel : activityCallsModelList) {
            rows.add(new ActivityCallsAdapter.Item(activityCallsModel));
        }
        activityCallsAdapter.setRows(rows);
        lcCalls.setAdapter(activityCallsAdapter);
    }

    private void fetchDataCalls() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                presenter.setupMoreListCalls(ApiParam.API_109, etSearch.getText().toString());

            }
        }, 5000);
    }

    private void setCallsMoreList() {
        List<ActivityCallsAdapter.Row> rows = new ArrayList<>();
        for (ActivityCallsModel acm : activityCallsModelList) {
            rows.add(new ActivityCallsAdapter.Item(acm));
        }
        activityCallsAdapter.setRows(rows);
        activityCallsAdapter.notifyDataSetChanged();
    }

    @Override
    public void finishActivityCallList(String result, List<ActivityCallsModel> activityCallsModelList) {
        swipeRefresh.setRefreshing(false);
        if (result.equalsIgnoreCase("OK")) {
            prevSize = activityCallsModelList.size();
            this.activityCallsModelList = activityCallsModelList;
            Log.e("CALL LIST", activityCallsModelList.toString());

            isMaxSize = false;
            isLoadMore = false;

            activityCallsAdapter = new ActivityCallsAdapter(context, this.activityCallsModelList);
            setCallsList();
            if (activityCallsModelList.size() > 0) {
                tvNoDataCalls.setVisibility(View.GONE);
            } else {
                tvNoDataCalls.setVisibility(View.VISIBLE);
            }
        }
            else if (result.equalsIgnoreCase("FAILED_KEY")) {
                startActivity(new Intent(context, ErrorAPIKeyActivity.class));
                MenuActivity.getInstance().finish();
            } else {
//                checkActivity();
//                rlNoConnection.setVisibility(View.VISIBLE);
            }

    }

    @Override
    public void finishActivityCallMoreList(String result, List<ActivityCallsModel> activityCallsModelList) {
        if (result.equals("OK")) {
            for (int i = 0; i < activityCallsModelList.size(); i++) {
                this.activityCallsModelList.add(activityCallsModelList.get(i));
            }

            if (this.prevSize == this.activityCallsModelList.size())
                isMaxSize = true;
            this.prevSize = this.activityCallsModelList.size();
            setCallsMoreList();
        }
        this.isLoadMore = false;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onPause() {
        pause = true;
        super.onPause();
    }

    @Override
    public void onResume() {
        if (pause) {
            getDataActivity();
        }
        super.onResume();
    }

    private void setList() {
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        lcTodo.setLayoutManager(layoutManager);
        lcTodo.setHasFixedSize(true);
        lcTodo.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {

                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                currentItems = layoutManager.getChildCount();
                totalItems = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstVisibleItemPosition();

                if (isScrolling && (currentItems + scrollOutItems == totalItems)) {
                    isScrolling = false;
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            todoPresneter.setupMoreList(ApiParam.API_108, etSearch.getText().toString());
                            todoAdapter.notifyDataSetChanged();
                        }
                    }, 5000);

                }
            }
        });
//        todoAdapter = new TodoAdapter(context, this.aTodoModelList);

        List<TodoAdapter.Row> rows = new ArrayList<TodoAdapter.Row>();

        for (ActivityTodoModel tm : aTodoModelList) {
            //Read List by Row to insert into List Adapter
            rows.add(new TodoAdapter.Item(tm));
        }
        todoAdapter.setGaris(rows);
        lcTodo.setAdapter(todoAdapter);
    }

    private void setMoreList() {
        List<TodoAdapter.Row> rows = new ArrayList<TodoAdapter.Row>();
        for (ActivityTodoModel country : aTodoModelList) {
            // Add the country to the list
            rows.add(new TodoAdapter.Item(country));
        }
        todoAdapter.setGaris(rows);
        todoAdapter.notifyDataSetChanged();

    }

    @Override
    public void finishATodoList(String result, List<ActivityTodoModel> list) {
        swipeRefresh.setRefreshing(false);
        if (result.equalsIgnoreCase("OK")) {
            prevSize = list.size();
            this.aTodoModelList = list;
            Log.e("TODO LIST", list.toString());

            isMaxSize = false;
            isLoadMore = false;

            todoAdapter = new TodoAdapter(context, this.aTodoModelList);
            setList();
            if (list.size() > 0) {
                    tvNoDataTodo.setVisibility(View.GONE);
            } else {
                tvNoDataTodo.setVisibility(View.VISIBLE);
            }
        } else if (result.equalsIgnoreCase("FAILED_KEY")) {
            startActivity(new Intent(context, ErrorAPIKeyActivity.class));
            MenuActivity.getInstance().finish();
        } else {
//            checkActivity();

//            rlNoConnection.setVisibility(View.VISIBLE);
            Log.e("TODO LIST", "KOSONG");
        }
    }

    @Override
    public void finishATodoMoreList(String result, List<ActivityTodoModel> list) {
        if (result.equals("OK")) {
            for (int i = 0; i < list.size(); i++) {
                this.aTodoModelList.add(list.get(i));
            }

            if (this.prevSize == this.aTodoModelList.size())
                isMaxSize = true;
            this.prevSize = this.aTodoModelList.size();
            setMoreList();
        }
        this.isLoadMore = false;
    }


    @Override
    public void finishFirst(String result, DashboardFirstModel model) {
        if(result.equalsIgnoreCase("OK")) {
            if (model.getCrm_event_create_permission().equalsIgnoreCase("1")) {
                faEventAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(context, EventAddActivity.class));
                        MeeToSelectDao dao = new MeeToSelectDao(context);
                        dao.deleteAllRecord();
                    }
                });
                faEventAdd.setColorNormal(R.color.BlueCRM);
                faEventAdd.setColorPressed(R.color.colorPrimaryDark);
                faEventAdd.setColorPressedResId(R.color.colorPrimaryDark);
                faEventAdd.setColorNormalResId(R.color.BlueCRM);

            } else {
                faEventAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
                    }
                });
                faEventAdd.setColorNormal(R.color.BlueCRM_light);
                faEventAdd.setColorPressed(R.color.colorPrimaryDark_light);
                faEventAdd.setColorPressedResId(R.color.colorPrimaryDark_light);
                faEventAdd.setColorNormalResId(R.color.BlueCRM_light);

            }

            if (model.getCrm_task_create_permission().equalsIgnoreCase("1")) {
                faTodoAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(context, TaskAddActivity.class));
                    }
                });
                faTodoAdd.setColorNormal(R.color.BlueCRM);
                faTodoAdd.setColorPressed(R.color.colorPrimaryDark);
                faTodoAdd.setColorPressedResId(R.color.colorPrimaryDark);
                faTodoAdd.setColorNormalResId(R.color.BlueCRM);
            } else {
                faTodoAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
                    }
                });
                faTodoAdd.setColorNormal(R.color.BlueCRM_light);
                faTodoAdd.setColorPressed(R.color.colorPrimaryDark_light);
                faTodoAdd.setColorPressedResId(R.color.colorPrimaryDark_light);
                faTodoAdd.setColorNormalResId(R.color.BlueCRM_light);
            }

            if (model.getCrm_call_create_permission().equalsIgnoreCase("1")) {
                faCallAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(context, CallsAddActivity.class));
                    }
                });
                faCallAdd.setColorNormal(R.color.BlueCRM);
                faCallAdd.setColorPressed(R.color.colorPrimaryDark);
                faCallAdd.setColorPressedResId(R.color.colorPrimaryDark);
                faCallAdd.setColorNormalResId(R.color.BlueCRM);
            } else {
                faCallAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
                    }
                });
                faCallAdd.setColorNormal(R.color.BlueCRM_light);
                faCallAdd.setColorPressed(R.color.colorPrimaryDark_light);
                faCallAdd.setColorPressedResId(R.color.colorPrimaryDark_light);
                faCallAdd.setColorNormalResId(R.color.BlueCRM_light);
            }
        }
    }

    @Override
    public void finishSecond(String result, DashboardSecondModel model) {

    }

    @Override
    public void finishThird(String result, List<DashboardThirdModel> listLead, List<DashboardThirdModel> listLead1, List<DashboardThirdModel> listOppor, List<DashboardThirdModel> listOppor1) {

    }

    @Override
    public void TotOpportunity(String result, TotalOpportunityModel totOporModel) {

    }

    @Override
    public void absence(String result, AbsenceModel model) {

    }
}
