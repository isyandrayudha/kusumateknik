package com.msf.project.sales1crm.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.adapter.OpportunityDetailProductAdapter;
import com.msf.project.sales1crm.customview.CustomEditText;
import com.msf.project.sales1crm.model.OpportunityModel;
import com.msf.project.sales1crm.model.ProductModel;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.BindView;

public class OpportunityTabProductFragment extends Fragment {
    private final static String TAG = OpportunityTabProductFragment.class.getSimpleName();

    @BindView(R.id.lvProduct)
    ListView lvProduct;

    @BindView(R.id.etSearch)
    CustomEditText etSearch;

    @BindView(R.id.ivClear)
    ImageView ivClear;

    @BindView(R.id.llNotFound)
    LinearLayout llNotFound;

    private View view;
    private Context context;
    private OpportunityDetailProductAdapter adapter;
    private List<ProductModel> list = new ArrayList<>();
    private List<ProductModel> searchList = new ArrayList<>();
    private OpportunityModel model;

    public static OpportunityTabProductFragment newInstance() {
        OpportunityTabProductFragment fragment = new OpportunityTabProductFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_opporproduct, container, false);

        return this.view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);
        this.context = getActivity();
        
        initView();
    }

    private void initView(){
        try {
            model = getArguments().getParcelable("opportunityModel");
        }catch (Exception e){}

        ivClear.setOnClickListener(click);

        setList();
        setTextWatcherForSearch();
    }

    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.ivClear:
                    etSearch.setText("");
                    etSearch.clearFocus();
                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);

                    llNotFound.setVisibility(View.GONE);
                    setList();
                    break;
            }
        }
    };

    private void setList(){
        list.clear();
        try {
            JSONArray array = new JSONArray(model.getProduct());

            for (int i = 0; i < array.length(); i++){
                ProductModel productModel = new ProductModel();
                productModel.setId(array.getJSONObject(i).getString("id"));
                productModel.setOppor_id(array.getJSONObject(i).getString("opportunity_id"));
                productModel.setName(array.getJSONObject(i).getString("product_name"));
                productModel.setCode(array.getJSONObject(i).getString("product_code"));
                productModel.setGroup_id(array.getJSONObject(i).getString("product_group_id"));
                productModel.setDetail_id(array.getJSONObject(i).getString("product_detail_id"));
                productModel.setCategory_id(array.getJSONObject(i).getString("category_id"));
                productModel.setCategory_name(array.getJSONObject(i).getString("category_name"));
                productModel.setBranch_id(array.getJSONObject(i).getString("branch_id"));
                productModel.setBranch_name(array.getJSONObject(i).getString("branch_name"));
                productModel.setPrice(array.getJSONObject(i).getString("price"));
                productModel.setStock(array.getJSONObject(i).getString("stock"));
                productModel.setQty(array.getJSONObject(i).getString("quantity"));
                productModel.setUnit(array.getJSONObject(i).getString("unit"));
                productModel.setNote(array.getJSONObject(i).getString("description_product"));
                productModel.setShow_price(array.getJSONObject(i).getString("show_price"));
                productModel.setShow_stock(array.getJSONObject(i).getString("show_stock"));
                list.add(productModel);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        adapter = new OpportunityDetailProductAdapter(context, list);

        List<OpportunityDetailProductAdapter.Row> rows = new ArrayList<OpportunityDetailProductAdapter.Row>();
        for (ProductModel productModel : list){
            //Read List by Row to insert into List Adapter
            rows.add(new OpportunityDetailProductAdapter.Item(productModel));
        }

        //Set Row Adapter
        adapter.setRows(rows);
        lvProduct.setAdapter(adapter);
        lvProduct.setOnItemClickListener(new OpportunityTabProductFragment.AdapterOnItemClick());
    }

    private void setTextWatcherForSearch() {
        etSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                Log.i(TAG, "onTextChanged");

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                Log.i(TAG, "beforeTextChanged");
            }

            @Override
            public void afterTextChanged(Editable s) {
                Log.i(TAG, "afterTextChanged");
                String search = etSearch.getText().toString();
                searchList.clear();

                if (!search.equals("")) {
                    if (list.size() > 0) {
                        for (int i = 0; i < list.size(); i++) {
                            if (list.get(i).getName().toLowerCase().contains(search.toLowerCase())) {
                                searchList.add(list.get(i));
                            }
                        }

                        setListSearch();
                        if (searchList.size() > 0){
                            llNotFound.setVisibility(View.GONE);
                        }else{
                            llNotFound.setVisibility(View.VISIBLE);
                        }
                    }

                    for (int i = 0; i < searchList.size(); i++) {
                        Log.i(TAG, "search data : "
                                + searchList.get(i).getName());
                    }
                } else {
                    llNotFound.setVisibility(View.GONE);
                    adapter.notifyDataSetChanged();
                    setList();
                }
            }
        });
    }

    private void setListSearch(){
        adapter = new OpportunityDetailProductAdapter(context, searchList);

        List<OpportunityDetailProductAdapter.Row> rows = new ArrayList<OpportunityDetailProductAdapter.Row>();
        for (ProductModel productModel : searchList){
            //Read List by Row to insert into List Adapter
            rows.add(new OpportunityDetailProductAdapter.Item(productModel));
        }

        //Set Row Adapter
        adapter.setRows(rows);
        lvProduct.setAdapter(adapter);
        lvProduct.setOnItemClickListener(new OpportunityTabProductFragment.AdapterOnItemClick());
    }

    private class AdapterOnItemClick implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            // TODO Auto-generated method stub
            final OpportunityDetailProductAdapter.Item item = (OpportunityDetailProductAdapter.Item) lvProduct.getAdapter().getItem(position);
            switch (view.getId()){
                default:
                    break;
            }
        }
    }
}
