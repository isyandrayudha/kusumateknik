package com.msf.project.sales1crm.fragment;

import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.callback.LeadDashboardCallback;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.customview.DatePickerFragment;
import com.msf.project.sales1crm.model.TotalModel;
import com.msf.project.sales1crm.presenter.ViewDashboardLeadDashboardPresenter;
import com.msf.project.sales1crm.service.GPSTracker;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ViewDashboardLeadDashboardFragment extends Fragment implements LeadDashboardCallback {

    @BindView(R.id.lineChart)
    LineChart lineChart;

    @BindView(R.id.lineChart1)
    LineChart lineChart1;

    @BindView(R.id.pieChart)
    PieChart pieChart;

    @BindView(R.id.pieChart1)
    PieChart pieChart1;

    @BindView(R.id.tvStartPeriod1)
    CustomTextView tvStartPeriod1;

    @BindView(R.id.tvEndPeriod1)
    CustomTextView tvEndPeriod1;

    @BindView(R.id.tvStartPeriod2)
    CustomTextView tvStartPeriod2;

    @BindView(R.id.tvEndPeriod2)
    CustomTextView tvEndPeriod2;

    @BindView(R.id.tvSubmit)
    CustomTextView tvSubmit;

    private View view;
    private Context context;
    private ViewDashboardLeadDashboardPresenter presenter;
    private List<TotalModel> list = new ArrayList<>();
    private List<TotalModel> list1 = new ArrayList<>();
    private String startDate1 = "", endDate1 = "", startDate2 = "", endDate2 = "";
    private DatePickerFragment datepicker1, datepicker2, datepicker3, datepicker4;
    private String open_date = "", string_year, string_month, string_day;
    private int year, month, day;

    public static ViewDashboardLeadDashboardFragment newInstance() {
        ViewDashboardLeadDashboardFragment fragment = new ViewDashboardLeadDashboardFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_leaddashboard, container, false);

        return this.view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);
        this.context = getActivity();

        this.presenter = new ViewDashboardLeadDashboardPresenter(context, this);

        datepicker1 = new DatePickerFragment();
        datepicker1.setListener(dateListener1);

        datepicker2 = new DatePickerFragment();
        datepicker2.setListener(dateListener2);

        datepicker3 = new DatePickerFragment();
        datepicker3.setListener(dateListener3);

        datepicker4 = new DatePickerFragment();
        datepicker4.setListener(dateListener4);

        initDate();
        initData();
    }

    private void initDate() {
        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH) + 1;
        day = c.get(Calendar.DAY_OF_MONTH);
    }

    private void initData(){
//        getDataFromAPI();

        tvStartPeriod1.setOnClickListener(click);
        tvEndPeriod1.setOnClickListener(click);
        tvStartPeriod2.setOnClickListener(click);
        tvEndPeriod2.setOnClickListener(click);
        tvSubmit.setOnClickListener(click);
    }

    public void getDataFromAPI(){
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(context)){
            GPSTracker gpsTracker = new GPSTracker(context);
            if (gpsTracker.canGetLocation()) {
                presenter.setupTotal(ApiParam.API_105, startDate1, endDate1, startDate2, endDate2);
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gpsTracker.showSettingsAlert();
            }
        }
    }

    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.tvSubmit:
                    getDataFromAPI();
                    break;
                case R.id.tvStartPeriod1:
                    datepicker1.setDateFragment(year, month, day);
                    datepicker1.show(getActivity().getFragmentManager(), "datePicker");
                    break;
                case R.id.tvEndPeriod1:
                    datepicker2.setDateFragment(year, month, day);
                    datepicker2.show(getActivity().getFragmentManager(), "datePicker");
                    break;
                case R.id.tvStartPeriod2:
                    datepicker3.setDateFragment(year, month, day);
                    datepicker3.show(getActivity().getFragmentManager(), "datePicker");
                    break;
                case R.id.tvEndPeriod2:
                    datepicker4.setDateFragment(year, month, day);
                    datepicker4.show(getActivity().getFragmentManager(), "datePicker");
                    break;
            }
        }
    };

    @Override
    public void finished(String result, List<TotalModel> listLead, List<TotalModel> listLead1, List<TotalModel> pieLead, List<TotalModel> pieLead1) {
        if (result.equalsIgnoreCase("OK")){

            ArrayList<Entry> leadValue = new ArrayList<Entry>();
            ArrayList<Entry> leadValue1 = new ArrayList<Entry>();
            ArrayList<String> leadString = new ArrayList<String>();
            for (int x = 0; x < listLead.size(); x++){
                leadValue.add(new Entry(Integer.parseInt(listLead.get(x).getDate()), Integer.parseInt(listLead.get(x).getTotal())));
            }
            for (int x = 0; x < listLead1.size(); x++){
                leadValue1.add(new Entry(Integer.parseInt(listLead1.get(x).getDate()), Integer.parseInt(listLead1.get(x).getTotal())));
            }


            ArrayList<PieEntry> data1 = new ArrayList<PieEntry>();
            for (int x = 0; x < list.size(); x++){
                Log.d("TAG", list.get(x).getTotal() + " - " + list.get(x).getName());

                data1.add(new PieEntry(Integer.parseInt(list.get(x).getTotal()), list.get(x).getName()));
            }

            ArrayList<PieEntry> data2 = new ArrayList<PieEntry>();
            for (int x = 0; x < list1.size(); x++){
                data2.add(new PieEntry(Integer.parseInt(list1.get(x).getTotal()), list1.get(x).getName()));
            }

            setPieCurrent(data1);
            setPieCurrent1(data2);

            setDataLead(leadValue);
            setDataLead1(leadValue1);
        }
    }

    private void setDataLead(ArrayList<Entry> yVals){
        // create a dataset and give it a type
        LineDataSet set1 = new LineDataSet(yVals, "Periode 1");
        set1.setFillAlpha(110);
        set1.setFillColor(Color.BLUE);

        // set the line to be drawn like this "- - - - - -"
        // set1.enableDashedLine(10f, 5f, 0f);
        // set1.enableDashedHighlightLine(10f, 5f, 0f);
        set1.setColor(Color.BLUE);
        set1.setCircleColor(Color.BLUE);
        set1.setLineWidth(1f);
        set1.setCircleRadius(2f);
        set1.setDrawCircleHole(false);
        set1.setValueTextSize(8f);
        set1.setDrawFilled(true);

        XAxis xAxis = lineChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawAxisLine(true);

        YAxis leftAxis = lineChart.getAxisLeft();
        leftAxis.setDrawGridLines(false);

        YAxis rightAxis = lineChart.getAxisRight();
        rightAxis.setTextColor(Color.WHITE);

        ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        dataSets.add(set1); // add the datasets

        // create a data object with the datasets
        LineData data = new LineData(dataSets);
        data.setDrawValues(false);

        // set data
        lineChart.setData(data);
        lineChart.animateY(1000);
        lineChart.setDragEnabled(true);
        lineChart.setScaleEnabled(true);
        lineChart.highlightValue(null);
        lineChart.invalidate();
        lineChart.getDescription().setEnabled(false);
    }

    private void setDataLead1(ArrayList<Entry> yVals1){
        LineDataSet set2 = new LineDataSet(yVals1, "Periode 2");
        set2.setFillAlpha(110);
        set2.setFillColor(Color.GREEN);

        // set the line to be drawn like this "- - - - - -"
        // set2.enableDashedLine(10f, 5f, 0f);
        // set2.enableDashedHighlightLine(10f, 5f, 0f);
        set2.setColor(Color.GREEN);
        set2.setCircleColor(Color.GREEN);
        set2.setLineWidth(1f);
        set2.setCircleRadius(2f);
        set2.setDrawCircleHole(false);
        set2.setValueTextSize(8f);
        set2.setDrawFilled(true);

        XAxis xAxis = lineChart1.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawAxisLine(true);

        YAxis leftAxis = lineChart1.getAxisLeft();
        leftAxis.setDrawGridLines(false);

        YAxis rightAxis = lineChart1.getAxisRight();
        rightAxis.setTextColor(Color.WHITE);

        ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        dataSets.add(set2); // add the datasets

        // create a data object with the datasets
        LineData data = new LineData(dataSets);
        data.setDrawValues(false);

        // set data
        lineChart1.setData(data);
        lineChart1.animateY(1000);
        lineChart1.setDragEnabled(true);
        lineChart1.setScaleEnabled(true);
        lineChart1.highlightValue(null);
        lineChart1.invalidate();
        lineChart1.getDescription().setEnabled(false);
    }

    private void setPieCurrent(ArrayList<PieEntry> current){
        PieDataSet dataSet = new PieDataSet(current, "");
        dataSet.setColors(ColorTemplate.COLORFUL_COLORS);

        PieData data = new PieData(dataSet);
        // In Percentage
        data.setValueTextSize(13f);
        data.setValueTextColor(Color.DKGRAY);

        // Default value
        //data.setValueFormatter(new DefaultValueFormatter(0));
        pieChart.setData(data);
        pieChart.setEntryLabelColor(Color.DKGRAY);
        pieChart.getDescription().setEnabled(false);
        pieChart.setDrawHoleEnabled(true);
        pieChart.setTransparentCircleRadius(58f);
        pieChart.setHoleRadius(58f);
        pieChart.spin( 500,0,-360f, Easing.EasingOption.EaseInOutQuad);
        pieChart.invalidate(); // refresh
    }

    private void setPieCurrent1(ArrayList<PieEntry> current){
        PieDataSet dataSet = new PieDataSet(current, "");
        dataSet.setColors(ColorTemplate.COLORFUL_COLORS);

        PieData data = new PieData(dataSet);
        // In Percentage
        data.setValueTextSize(13f);
        data.setValueTextColor(Color.DKGRAY);

        // Default value
        //data.setValueFormatter(new DefaultValueFormatter(0));
        pieChart1.setData(data);
        pieChart1.setEntryLabelColor(Color.DKGRAY);
        pieChart1.getDescription().setEnabled(false);
        pieChart1.setDrawHoleEnabled(true);
        pieChart1.setTransparentCircleRadius(58f);
        pieChart1.setHoleRadius(58f);
        pieChart1.spin( 500,0,-360f, Easing.EasingOption.EaseInOutQuad);
        pieChart1.invalidate(); // refresh
    }

    DatePickerDialog.OnDateSetListener dateListener1 = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int r_year, int monthOfYear,
                              int dayOfMonth) {
            year = r_year;
            month = monthOfYear + 1;
            day = dayOfMonth;

            string_year = r_year + "";

            if (month < 10) {

                string_month = "0" + month;
            } else {
                string_month = month + "";
            }
            if (day < 10) {

                string_day = "0" + day;
            } else {
                string_day = day + "";
            }
            startDate1 = year + "-" + string_month + "-" + string_day;

            tvStartPeriod1.setText(year + "-" + string_month + "-" + string_day);
        }
    };

    DatePickerDialog.OnDateSetListener dateListener2 = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int r_year, int monthOfYear,
                              int dayOfMonth) {
            year = r_year;
            month = monthOfYear + 1;
            day = dayOfMonth;

            string_year = r_year + "";

            if (month < 10) {

                string_month = "0" + month;
            } else {
                string_month = month + "";
            }
            if (day < 10) {

                string_day = "0" + day;
            } else {
                string_day = day + "";
            }
            endDate1 = year + "-" + string_month + "-" + string_day;

            tvEndPeriod1.setText(year + "-" + string_month + "-" + string_day);
        }
    };

    DatePickerDialog.OnDateSetListener dateListener3 = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int r_year, int monthOfYear,
                              int dayOfMonth) {
            year = r_year;
            month = monthOfYear + 1;
            day = dayOfMonth;

            string_year = r_year + "";

            if (month < 10) {

                string_month = "0" + month;
            } else {
                string_month = month + "";
            }
            if (day < 10) {

                string_day = "0" + day;
            } else {
                string_day = day + "";
            }
            startDate2 = year + "-" + string_month + "-" + string_day;

            tvStartPeriod2.setText(year + "-" + string_month + "-" + string_day);
        }
    };

    DatePickerDialog.OnDateSetListener dateListener4 = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int r_year, int monthOfYear,
                              int dayOfMonth) {
            year = r_year;
            month = monthOfYear + 1;
            day = dayOfMonth;

            string_year = r_year + "";

            if (month < 10) {

                string_month = "0" + month;
            } else {
                string_month = month + "";
            }
            if (day < 10) {

                string_day = "0" + day;
            } else {
                string_day = day + "";
            }
            endDate2 = year + "-" + string_month + "-" + string_day;

            tvEndPeriod2.setText(year + "-" + string_month + "-" + string_day);
        }
    };
}
