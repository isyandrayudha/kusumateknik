package com.msf.project.sales1crm.presenter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;

import com.msf.project.sales1crm.callback.ActivityListCallback;
import com.msf.project.sales1crm.model.ActivityEventModel;
import com.msf.project.sales1crm.service.NetworkConnection;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.PreferenceUtility;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;
import com.msf.project.sales1crm.utility.Sales1CRMUtilsJSON;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ActivityEventListPresenter {
    private final String TAG = ActivityEventListPresenter.class.getSimpleName();
    private Context context;
    private Messenger messenger;
    private Message message;
    private Bundle bundle;
    private Handler handler;
    private String failureResponse = "";
    private String[] stringResponse = {""};
    private ActivityListCallback callback;
    private String result = "NG";
    private List<ActivityEventModel> activityEventModelsList = new ArrayList<>();

    public ActivityEventListPresenter(Context context, ActivityListCallback listener) {
        this.context = context;
        this.callback = listener;
    }

    private void doNetworkingService(String url, String params, String from) {
        Log.d(TAG,"url activity event " + url);
        Log.d(TAG,"params : " + params);
        Intent networkIntent = new Intent(this.context, NetworkConnection.class);
        this.messenger = new Messenger(this.handler);
        networkIntent.putExtra("messenger", this.messenger);
        networkIntent.putExtra("url", url);
        networkIntent.putExtra("params", params);
        networkIntent.putExtra("from", from);
        this.context.startService(networkIntent);
    }

    public void setupList(int apiIndex, int offset, String search, String account_id, String date) {
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)) {
            obtainListItem(apiIndex, offset, search, account_id, date);
        }else{
            callback.finishActivityEventList(this.result, this.activityEventModelsList);
        }
    }

    public void obtainListItem(int apiIndex, int offset, String search, String account_id, String date ) {
        this.handler = new Handler(this.context.getMainLooper()){
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                parseListItem(msg);
            }
        };

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("api_key", PreferenceUtility.getInstance().loadDataString(context,PreferenceUtility.API_KEY));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        doNetworkingService(ApiParam.urlBuilder(this.context,apiIndex),jsonObject.toString(),"ActivityEventList");
    }


    private void parseListItem(Message message) {
        this.message = message;
        this.bundle = this.message.getData();
        this.stringResponse[0] = this.bundle.getString("network_response");
        this.failureResponse = this.bundle.getString("network_failure");
        Log.d(TAG, "responseString[0] login info " + this.stringResponse[0]);
        if (this.failureResponse.equalsIgnoreCase("yes")) {
            this.callback.finishActivityEventList(this.result,this.activityEventModelsList);
        } else {
            try{
                this.result = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                if (this.result.equalsIgnoreCase("OK")){
                    this.activityEventModelsList = Sales1CRMUtilsJSON.JSONUtility.getActivityEventList(this.stringResponse[0]);
                }
                this.callback.finishActivityEventList(this.result,this.activityEventModelsList);
            } catch (JSONException e) {
                Log.d(TAG,"Exception detail Response " + e.getMessage());
            }
        }
    }

    public void setupMoreList(int apiIndex, int offset, String search, String account_id, String date) {
        this.handler = new Handler(this.context.getMainLooper()) {
            @Override
            public void handleMessage(Message msg) {
                parseMoreListItem(msg);

                }
            };

            JSONObject jsonObject = new JSONObject();
            try{
                jsonObject.put("api_key",PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.API_KEY));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            doNetworkingService(ApiParam.urlBuilder(this.context, apiIndex), jsonObject.toString(),"ActivityEventList");
       }

     private void parseMoreListItem(Message message) {
        this.message = message;
        this.bundle = this.message.getData();
        this.stringResponse[0] = this.bundle.getString("network_response");
        this.failureResponse = this.bundle.getString("network_failure");
        Log.d(TAG,"responseString[0] Activity Event Info " + this.stringResponse[0]);
        if(this.failureResponse.equalsIgnoreCase("yes")){
            this.callback.finishActivityEventMoreList(this.result,this.activityEventModelsList);
         }else {
            try {
                this.result = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                if (this.result.equalsIgnoreCase("OK")) {
                    this.activityEventModelsList = Sales1CRMUtilsJSON.JSONUtility.getActivityEventList(this.stringResponse[0]);
                }
            } catch (JSONException e) {
                Log.d(TAG,"EXCEPTION DETAIL RESPONSE" + e.getMessage());
            }

        }
    }
}
