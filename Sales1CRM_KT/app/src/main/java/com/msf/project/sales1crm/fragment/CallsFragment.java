package com.msf.project.sales1crm.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.msf.project.sales1crm.CallsAddActivity;
import com.msf.project.sales1crm.CallsDetailActivity;
import com.msf.project.sales1crm.ErrorAPIKeyActivity;
import com.msf.project.sales1crm.MenuActivity;
import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.adapter.CallsListAdapter;
import com.msf.project.sales1crm.callback.CallListCallback;
import com.msf.project.sales1crm.callback.DashboardCallback;
import com.msf.project.sales1crm.customview.CustomEditText;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.imageloader.ImageLoader;
import com.msf.project.sales1crm.model.AbsenceModel;
import com.msf.project.sales1crm.model.CallsModel;
import com.msf.project.sales1crm.model.DashboardFirstModel;
import com.msf.project.sales1crm.model.DashboardSecondModel;
import com.msf.project.sales1crm.model.DashboardThirdModel;
import com.msf.project.sales1crm.model.TotalOpportunityModel;
import com.msf.project.sales1crm.presenter.CallListPresenter;
import com.msf.project.sales1crm.presenter.DashboardPresenter;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class CallsFragment extends Fragment implements CallListCallback, DashboardCallback {


    @BindView(R.id.etSearch)
    CustomEditText etSearch;
    @BindView(R.id.ivClear)
    ImageView ivClear;
    @BindView(R.id.tvRefresh)
    CustomTextView tvRefresh;
    @BindView(R.id.rlNoConnection)
    RelativeLayout rlNoConnection;
    @BindView(R.id.llNotFound)
    LinearLayout llNotFound;
    @BindView(R.id.lvTask)
    ListView lvTask;
    @BindView(R.id.lvTaskSwipe)
    SwipeRefreshLayout lvTaskSwipe;
    @BindView(R.id.faCallAdd)
    FloatingActionButton faCallAdd;
    @BindView(R.id.menu)
    FloatingActionMenu menu;
    @BindView(R.id.shimmerLayout)
    ShimmerFrameLayout shimmerLayout;
    @BindView(R.id.llshim)
    LinearLayout llshim;
    @BindView(R.id.rlTaskList)
    RelativeLayout rlTaskList;
    Unbinder unbinder;
    private View view;
    private Context context;
    private CallsListAdapter adapter;
    private CallListPresenter presenter;
    private List<CallsModel> list = new ArrayList<>();
    private String date_selected = "";
    private Calendar calendar;
    private DashboardPresenter dashboardPresenter;


    private int prevSize = 0;
    private boolean isLoadMore = true;
    private boolean isMaxSize = true;
    private boolean pause = false;

    public static CallsFragment newInstance() {
        CallsFragment fragment = new CallsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_calls, container, false);
        this.context = getActivity();
        unbinder = ButterKnife.bind(this, this.view);
        return this.view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);
        this.context = getActivity();
        this.presenter = new CallListPresenter(context, this);
        this.dashboardPresenter = new DashboardPresenter(context, this);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Calls");

        initData();

    }

    private void initData() {
        Sales1CRMUtils.Utils.imageLoader = new ImageLoader(context);

        etSearch.clearFocus();
        lvTask.setOnScrollListener(new ListLazyLoad());
        lvTaskSwipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                llshim.setVisibility(View.VISIBLE);
                shimmerLayout.setVisibility(View.VISIBLE);
                shimmerLayout.startShimmer();
                getDataFromAPI(0, date_selected);
            }
        });

        //Set Seach Function
        setTextWatcherForSearch();
        getDataFromAPI(0, date_selected);

        ivClear.setOnClickListener(click);
        tvRefresh.setOnClickListener(click);

    }

    private void setTextWatcherForSearch() {
        etSearch.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    if (etSearch.getText().length() == 0) {
                        ivClear.setVisibility(View.VISIBLE);
                    }
                } else {
                    if (etSearch.getText().length() == 0) {
                        ivClear.setVisibility(View.GONE);
                    } else {
                        ivClear.setVisibility(View.VISIBLE);
                    }

                }
            }
        });

        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_GO) {
                    getDataFromAPI(0, date_selected);
                }

                return true;
            }
        });
    }

    private void getDataFromAPI(int prev, String date) {
        isMaxSize = true;
        isLoadMore = true;

        llNotFound.setVisibility(View.GONE);

        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(context)) {
            dashboardPresenter.setupFirst(ApiParam.API_005);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    presenter.setupList(ApiParam.API_080, prev, etSearch.getText().toString());
                }
            },1000);
        } else {
            rlNoConnection.setVisibility(View.VISIBLE);
            lvTaskSwipe.setVisibility(View.GONE);
        }
    }


    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.ivClear:
                    etSearch.setText("");
                    etSearch.clearFocus();
                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);
                    getDataFromAPI(0, date_selected);
                    break;
                case R.id.tvRefresh:
                    lvTaskSwipe.setVisibility(View.VISIBLE);
                    rlNoConnection.setVisibility(View.GONE);

                    getDataFromAPI(0, date_selected);
                    break;
                default:
                    break;
            }
        }
    };

    private void setList() {
        llshim.setVisibility(View.VISIBLE);
        shimmerLayout.setVisibility(View.GONE);
        adapter = new CallsListAdapter(context, this.list);

        List<CallsListAdapter.Row> rows = new ArrayList<CallsListAdapter.Row>();
        for (CallsModel model : list) {
            //Read List by Row to insert into List Adapter
            rows.add(new CallsListAdapter.Item(model));
        }
        //Set Row Adapter
        adapter.setRows(rows);
        lvTask.setAdapter(adapter);
        lvTask.setSelector(R.drawable.transparent_selector);
        lvTask.setOnItemClickListener(new AdapterOnItemClick());
        lvTask.deferNotifyDataSetChanged();
        llshim.setVisibility(View.INVISIBLE);
        shimmerLayout.stopShimmer();
        shimmerLayout.setVisibility(View.INVISIBLE);
    }

    private void setMoreList() {
        List<CallsListAdapter.Row> rows = new ArrayList<CallsListAdapter.Row>();

        for (CallsModel country : list) {
            // Add the country to the list
            rows.add(new CallsListAdapter.Item(country));
        }
        adapter.setRows(rows);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void finishCallList(String result, List<CallsModel> list) {
        lvTaskSwipe.setRefreshing(false);
//        rlProgress.setVisibility(View.GONE);
        if (result.equalsIgnoreCase("OK")) {
            prevSize = list.size();
            this.list = list;

            isMaxSize = false;
            isLoadMore = false;

            setList();
            if (list.size() > 0) {
                llNotFound.setVisibility(View.GONE);
            } else {
                llNotFound.setVisibility(View.VISIBLE);
            }
        } else if (result.equalsIgnoreCase("FAILED_KEY")) {
            startActivity(new Intent(context, ErrorAPIKeyActivity.class));
            MenuActivity.getInstance().finish();
        } else {
            lvTaskSwipe.setVisibility(View.GONE);
            rlNoConnection.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void finishCallMoreList(String result, List<CallsModel> list) {
        if (result.equals("OK")) {
            for (int i = 0; i < list.size(); i++) {
                this.list.add(list.get(i));
            }

            if (this.prevSize == this.list.size())
                isMaxSize = true;
            this.prevSize = this.list.size();
            setMoreList();
        }
        this.isLoadMore = false;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void finishFirst(String result, DashboardFirstModel model) {
if(model.getCrm_call_create_permission().equalsIgnoreCase("1")){
    faCallAdd.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(context, CallsAddActivity.class);
            intent.putExtra("date", date_selected);
            startActivity(intent);
        }
    });
    faCallAdd.setColorNormal(R.color.BlueCRM);
    faCallAdd.setColorPressed(R.color.colorPrimaryDark);
    faCallAdd.setColorPressedResId(R.color.colorPrimaryDark);
    faCallAdd.setColorNormalResId(R.color.BlueCRM);

} else {
    faCallAdd.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
        }
    });
    faCallAdd.setColorNormal(R.color.BlueCRM_light);
    faCallAdd.setColorPressed(R.color.colorPrimaryDark_light);
    faCallAdd.setColorPressedResId(R.color.colorPrimaryDark_light);
    faCallAdd.setColorNormalResId(R.color.BlueCRM_light);
}
    }

    @Override
    public void finishSecond(String result, DashboardSecondModel model) {

    }

    @Override
    public void finishThird(String result, List<DashboardThirdModel> listLead, List<DashboardThirdModel> listLead1, List<DashboardThirdModel> listOppor, List<DashboardThirdModel> listOppor1) {

    }

    @Override
    public void TotOpportunity(String result, TotalOpportunityModel totOporModel) {

    }

    @Override
    public void absence(String result, AbsenceModel model) {

    }

    private class ListLazyLoad implements AbsListView.OnScrollListener {

        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {

        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            if (firstVisibleItem + visibleItemCount == totalItemCount && totalItemCount != 0) {
                Log.d("TAG", "isLoadMore : " + isLoadMore);
                if (isLoadMore == false && isMaxSize == false) {
                    isLoadMore = true;
                    prevSize = list.size();

                    presenter.setupMoreList(ApiParam.API_080, prevSize, etSearch.getText().toString());
                }
            }
        }
    }

    private class AdapterOnItemClick implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            // TODO Auto-generated method stub
            CallsListAdapter.Item item = (CallsListAdapter.Item) lvTask.getAdapter().getItem(position);
            switch (view.getId()) {
                default:
                        Intent intent = new Intent(context, CallsDetailActivity.class);
                        intent.putExtra("callsModel", item.getItem());
                        startActivity(intent);
                    break;
            }
        }
    }

    @Override
    public void onPause() {
        pause = true;
        super.onPause();
    }

    @Override
    public void onResume() {
        if (pause) {
            getDataFromAPI(0, date_selected);
        }
        super.onResume();
    }
}
