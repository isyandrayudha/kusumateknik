package com.msf.project.sales1crm.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

/**
 * Created by christianmacbook on 15/05/18.
 */

public class AccountAddressModel implements Comparable, Parcelable {
    private String id;
    private String address;
    private String provinsi_id;
    private String provinsi_name;
    private String kabupaten_id;
    private String kabupaten_name;
    private String kecamatan_id;
    private String kecamatan_name;
    private String kelurahan_id;
    private String kelurahan_name;
    private String kodepos;
    private String longitude;
    private String latitude;
    private String distance;

    public AccountAddressModel() {
        setId("");
        setAddress("");
        setProvinsi_id("");
        setProvinsi_name("");
        setKabupaten_id("");
        setKabupaten_name("");
        setKecamatan_id("");
        setKecamatan_name("");
        setKelurahan_id("");
        setKelurahan_name("");
        setKodepos("");
        setLongitude("");
        setLatitude("");
    }

    public AccountAddressModel(String id, String address, String longitude, String latitude) {
        this.id = id;
        this.address = address;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public AccountAddressModel(Parcel in) {
        readFromParcel(in);
    }

    public void readFromParcel(Parcel in) {
        id = in.readString();
        address = in.readString();
        provinsi_id = in.readString();
        provinsi_name = in.readString();
        kabupaten_id = in.readString();
        kabupaten_name = in.readString();
        kecamatan_id = in.readString();
        kecamatan_name = in.readString();
        kelurahan_id = in.readString();
        kelurahan_name = in.readString();
        kodepos = in.readString();
        longitude = in.readString();
        latitude = in.readString();
    }

    public static final Creator<AccountAddressModel> CREATOR = new Creator<AccountAddressModel>() {
        @Override
        public AccountAddressModel createFromParcel(Parcel in) {
            return new AccountAddressModel(in);
        }

        @Override
        public AccountAddressModel[] newArray(int size) {
            return new AccountAddressModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getProvinsi_id() {
        return provinsi_id;
    }

    public void setProvinsi_id(String provinsi_id) {
        this.provinsi_id = provinsi_id;
    }

    public String getProvinsi_name() {
        return provinsi_name;
    }

    public void setProvinsi_name(String provinsi_name) {
        this.provinsi_name = provinsi_name;
    }

    public String getKabupaten_id() {
        return kabupaten_id;
    }

    public void setKabupaten_id(String kabupaten_id) {
        this.kabupaten_id = kabupaten_id;
    }

    public String getKabupaten_name() {
        return kabupaten_name;
    }

    public void setKabupaten_name(String kabupaten_name) {
        this.kabupaten_name = kabupaten_name;
    }

    public String getKecamatan_id() {
        return kecamatan_id;
    }

    public void setKecamatan_id(String kecamatan_id) {
        this.kecamatan_id = kecamatan_id;
    }

    public String getKecamatan_name() {
        return kecamatan_name;
    }

    public void setKecamatan_name(String kecamatan_name) {
        this.kecamatan_name = kecamatan_name;
    }

    public String getKelurahan_id() {
        return kelurahan_id;
    }

    public void setKelurahan_id(String kelurahan_id) {
        this.kelurahan_id = kelurahan_id;
    }

    public String getKelurahan_name() {
        return kelurahan_name;
    }

    public void setKelurahan_name(String kelurahan_name) {
        this.kelurahan_name = kelurahan_name;
    }

    public String getKodepos() {
        return kodepos;
    }

    public void setKodepos(String kodepos) {
        this.kodepos = kodepos;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(address);
        dest.writeString(provinsi_id);
        dest.writeString(provinsi_name);
        dest.writeString(kabupaten_id);
        dest.writeString(kabupaten_name);
        dest.writeString(kecamatan_id);
        dest.writeString(kecamatan_name);
        dest.writeString(kelurahan_id);
        dest.writeString(kelurahan_name);
        dest.writeString(kodepos);
        dest.writeString(longitude);
        dest.writeString(latitude);
    }

    @Override
    public int compareTo(@NonNull Object o) {
        return this.getAddress().compareToIgnoreCase(
                ((AccountAddressModel) o).getAddress());
    }


}
