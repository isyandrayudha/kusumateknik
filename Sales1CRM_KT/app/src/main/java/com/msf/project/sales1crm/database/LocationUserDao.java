package com.msf.project.sales1crm.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.msf.project.sales1crm.model.LocationUserModel;

import java.util.ArrayList;
import java.util.List;

public class LocationUserDao extends BaseDao<LocationUserModel> implements DBSchema.LocationUser {
    public LocationUserDao(Context c) {
        super(c, TABLE_NAME);
    }

    public LocationUserDao(Context c, String table, boolean willWrite) {
        super(c, table, willWrite);
    }

    public LocationUserDao(DBHelper dbHelper) {
        super(dbHelper);
    }

    public LocationUserDao(DBHelper dbHelper, boolean willWrite) {
        super(dbHelper, TABLE_NAME, willWrite);
    }

    public LocationUserDao(DBHelper dbHelper, String table, boolean willWrite) {
        super(dbHelper, table, willWrite);
    }

    public LocationUserDao(Context c, DBHelper db, String table, boolean willWrite) {
        super(c, db, table, willWrite);
    }

    @Override
    public LocationUserModel getById(int id) {
        String qry = "SELECT * FROM " + getTable() + " WHERE " + COL_ID + " = " + id + " ORDER BY " + COL_ID + " DESC";
        Cursor c = getSqliteDb().rawQuery(qry,null);
        LocationUserModel model = new LocationUserModel();
        try{
            if(c != null && c.moveToFirst()) {
                model = getByCursor(c);
            }
        } finally {
            c.close();
        }
        return model;
    }

    public LocationUserModel getLastLocationUser() {
        String qry = "SELECT * FROM " + getTable() + " ORDER BY " + COL_ID + " DESC LIMIT 1";
        Cursor c = getSqliteDb().rawQuery(qry,null);
        LocationUserModel model = new LocationUserModel();
        try{
            if(c != null && c.moveToFirst()) {
                model = getByCursor(c);
            }
        } finally {
            c.close();

        }
        return model;
    }

    public LocationUserModel delete(String lat) {
        String qry = "DELETE FROM "+ getTable() +" WHERE "+ DBSchema.LocationUser.KEY_LATITUDE +"='"+lat+"'";
        Cursor c = getSqliteDb().rawQuery(qry,null);
        LocationUserModel model = new LocationUserModel();
        try {
            if(c != null && c.moveToFirst()) {
                model = getByCursor(c);
            }
        } finally {
            c.close();
        }
        return model;
    }

    public List<LocationUserModel> getLocationUserList(){
        String query = "SELECT * FROM " + getTable() + " ORDER BY " + COL_ID + " ASC";
        Cursor c = getSqliteDb().rawQuery(query,null);
        List<LocationUserModel> list = new ArrayList<>();
        try{
            if(c != null && c.moveToFirst()) {
                list.add(getByCursor(c));
                while (c.moveToNext()) {
                    list.add(getByCursor(c));
                }
            }
        } finally {
            c.close();
        }

        return list;
    }


    public String[] getStringArray(){
        String query = "SELECT * FROM " + getTable();

        Cursor c = getSqliteDb().rawQuery(query, null);
        String[] array = new String[c.getCount() + 1];
        try {
            if(c != null && c.moveToFirst()) {
                array[0] = "locationUser";
                array[1] = getByCursor(c).getLatitude();
                int i = 2;
                while (c.moveToNext()) {
                    array[i] = getByCursor(c).getLatitude();
                    i++;

                }
            }
        } finally {
            c.close();
        }

        return array;
    }



    @Override
    public LocationUserModel getByCursor(Cursor c) {
        LocationUserModel model = new LocationUserModel();
        model.setId(c.getString(1));
        model.setLatitude(c.getString(2));
        model.setLongitude(c.getString(3));
        model.setAddress(c.getString(4));
        model.setDate(c.getString(5));
        return model;
    }

    @Override
    protected ContentValues upDataValues(LocationUserModel model, boolean update) {
        ContentValues cv = new ContentValues();
        if(update == true)
            cv.put(KEY_USER_ID,model.getId());
        cv.put(KEY_LATITUDE, model.getLatitude());
        cv.put(KEY_LONGITUDE, model.getLongitude());
        cv.put(KEY_ADDRESS, model.getAddress());
        cv.put(KEY_DATE,model.getDate());
        return cv;
    }
}
