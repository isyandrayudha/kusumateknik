package com.msf.project.sales1crm.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;

import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.model.LeadModel;
import com.msf.project.sales1crm.utility.PreferenceUtility;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by christianmacbook on 30/05/18.
 */

public class LeadListAdapter extends BaseAdapter {
    private Context context;
    private Bitmap icon;
    private String url;
    private List<LeadModel> listData;

    public LeadListAdapter(Context context, List<LeadModel> objects) {
        this.context = context;
        this.listData = objects;

        this.url = PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.URL);
    }

    public static abstract class Row {
    }

    // This is for making Section, ex: A B C D etc
    public static final class Section extends Row {
        public final String text;

        public Section(String text) {
            this.text = text;
        }
    }

    // This is for making Row, ex: A B C D etc
    public static final class Item extends Row {
        public final LeadModel text;

        public Item(LeadModel text) {
            this.text = text;
        }

        public LeadModel getItem() {
            return text;
        }
    }

    private List<Row> rows;

    public void setRows(List<Row> rows) {
        this.rows = rows;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return rows.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return rows.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        if (getItem(position) instanceof Section) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public View getView(final int position, View converView, ViewGroup parent) {
        View view = converView;
        final ViewHolder holder;

        final Item item = (Item) getItem(position);
        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) parent.getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = (LinearLayout) inflater.inflate(
                    R.layout.row_leadlist, parent, false);
            holder = new ViewHolder();
            holder.tvLeadName = (CustomTextView) view
                    .findViewById(R.id.tvLeadName);
            holder.tvPosition = (CustomTextView) view
                    .findViewById(R.id.tvPosition);
            holder.tvCompany = (CustomTextView) view
                    .findViewById(R.id.tvCompany);
            holder.tvPhone = (CustomTextView) view
                    .findViewById(R.id.tvPhone);
            holder.ivImage = (CircleImageView) view
                    .findViewById(R.id.ivImage);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        Sales1CRMUtils.Utils.imageLoader.DisplayImage(url + item.text.getImage_url(),
                holder.ivImage, Bitmap.Config.ARGB_8888, 500, Sales1CRMUtils.TYPE_RECT);

        holder.tvLeadName.setText(item.text.getFirst_name() + " " + item.text.getLast_name());
        holder.tvPosition.setText(item.text.getPosition_name());
        holder.tvCompany.setText(item.text.getCompany());
        holder.tvPhone.setText(item.text.getPhone());

        return view;
    }

    private static class ViewHolder {
        CustomTextView tvLeadName, tvPosition, tvCompany, tvPhone;
        CircleImageView ivImage;
    }
}
