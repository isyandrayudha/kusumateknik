package com.msf.project.sales1crm;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.mapbox.android.core.location.LocationEngine;
import com.mapbox.android.core.location.LocationEngineListener;
import com.mapbox.android.core.location.LocationEnginePriority;
import com.mapbox.android.core.location.LocationEngineProvider;
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.annotations.Icon;
import com.mapbox.mapboxsdk.annotations.IconFactory;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.plugins.locationlayer.LocationLayerPlugin;
import com.mapbox.mapboxsdk.plugins.locationlayer.modes.CameraMode;
import com.mapbox.mapboxsdk.plugins.locationlayer.modes.RenderMode;
import com.msf.project.sales1crm.callback.AccountAddCallback;
import com.msf.project.sales1crm.callback.LocationDialogCallback;
import com.msf.project.sales1crm.customview.CustomEditText;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.fragment.LocationFragment;
import com.msf.project.sales1crm.model.AccountAddressModel;
import com.msf.project.sales1crm.model.AccountModel;
import com.msf.project.sales1crm.presenter.AccountAddPresenter;
import com.msf.project.sales1crm.utility.ApiParam;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by christianmacbook on 24/05/18.
 */

public class AccountAddAddressActivity extends BaseActivity implements OnMapReadyCallback, AccountAddCallback,
        LocationEngineListener, PermissionsListener, MapboxMap.OnMapClickListener {

    @BindView(R.id.ivBack)
    ImageView ivBack;

    @BindView(R.id.ivSave)
    ImageView ivSave;

    @BindView(R.id.tvTitle)
    CustomTextView tvTitle;

    @BindView(R.id.cbShipping)
    CheckBox cbShipping;

    @BindView(R.id.llShipping)
    LinearLayout llShipping;

    @BindView(R.id.tvTitleBilling)
    CustomTextView tvTitleBilling;

    @BindView(R.id.tilAddress)
    TextInputLayout tilAddress;

    @BindView(R.id.etAddress)
    CustomEditText etAddress;

    @BindView(R.id.tvProvinsi)
    CustomTextView tvProvinsi;

    @BindView(R.id.tvKabupaten)
    CustomTextView tvKabupaten;

    @BindView(R.id.tvKecamatan)
    CustomTextView tvKecamatan;

    @BindView(R.id.tvKelurahan)
    CustomTextView tvKelurahan;

    @BindView(R.id.tvKodepos)
    CustomTextView tvKodepos;

    @BindView(R.id.tilAddressShipping)
    TextInputLayout tilAddressShipping;

    @BindView(R.id.etAddressShipping)
    CustomEditText etAddressShipping;

    @BindView(R.id.tvProvinsiShipping)
    CustomTextView tvProvinsiShipping;

    @BindView(R.id.tvKabupatenShipping)
    CustomTextView tvKabupatenShipping;

    @BindView(R.id.tvKecamatanShipping)
    CustomTextView tvKecamatanShipping;

    @BindView(R.id.tvKelurahanShipping)
    CustomTextView tvKelurahanShipping;

    @BindView(R.id.tvKodeposShipping)
    CustomTextView tvKodeposShipping;
    @BindView(R.id.tvBillingLatitude)
    CustomTextView tvBillingLatitude;
    @BindView(R.id.tvBillingLongitude)
    CustomTextView tvBillingLongitude;
    @BindView(R.id.tvShippingLatitude)
    CustomTextView tvShippingLatitude;
    @BindView(R.id.tvShippingLongitude)
    CustomTextView tvShippingLongitude;


    private Context context;
    private AccountModel model;
    private AccountAddressModel billingModel, shippingModel;
    private AccountAddPresenter presenter;
    private MapView mapViewShipping, mapViewBilling;
    private MapboxMap mMapboxMap;
    private Marker markerViewBilling;
    private Marker markerViewShipping;
    private LocationEngine locationEngine;
    private LocationLayerPlugin locationLayerPlugin;
    private PermissionsManager permissionsManager;
    private Location originLocation;
    private Point originPosition;
    private Point destinationPosition;
    private String kecamatan, kabupaten, provinsi, kelurahan, alamat, jalan, no, latitude, longitude, postalCode;


    private LocationFragment locationFragment;
    private String provinsiID = "0", kabupatenID = "0", kecamatanID = "0", kelurahanID = "0",
            provinsiShippingID = "0", kabupatenShippingID = "0", kecamatanShippingID = "0", kelurahanShippingID = "0", latutudeID = "0", longitudeID = "0";
    private boolean from_detail = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accountaddaddress);
        Mapbox.getInstance(this, getString(R.string.key_access_token_development));
        mapViewShipping = findViewById(R.id.mapViewShipping);
        mapViewShipping.setStyleUrl(getResources().getString(R.string.layer_streets) + "?auth=" + getResources().getString(R.string.key_access_token_development));
//        mapView.setStyleUrl(Style.MAPBOX_STREETS);

        mapViewShipping.getMapAsync(this);

        mapViewBilling = findViewById(R.id.mapViewBilling);
        mapViewBilling.setStyleUrl(getResources().getString(R.string.layer_streets) + "?auth=" + getResources().getString(R.string.key_access_token_development));
//        mapView.setStyleUrl(Style.MAPBOX_STREETS);
        mapViewBilling.getMapAsync(this);



        ButterKnife.bind(this);
        this.context = this;

        this.presenter = new AccountAddPresenter(context, this);

        initView();
    }

    private void initView() {
        model = getIntent().getExtras().getParcelable("accountModel");

        try {
            from_detail = getIntent().getExtras().getBoolean("from_detail");
        } catch (Exception e) {
        }

        getDataIntent();

        getDataFromDetail();

        tvProvinsi.setOnClickListener(click);
        tvKabupaten.setOnClickListener(click);
        tvKecamatan.setOnClickListener(click);
        tvKelurahan.setOnClickListener(click);

        tvProvinsiShipping.setOnClickListener(click);
        tvKabupatenShipping.setOnClickListener(click);
        tvKecamatanShipping.setOnClickListener(click);
        tvKelurahanShipping.setOnClickListener(click);

        ivBack.setOnClickListener(click);
        ivSave.setOnClickListener(click);
        cbShipping.setOnClickListener(click);
        mapViewBilling.setVisibility(View.VISIBLE);


    }

    private void getDataFromDetail() {
        if (from_detail) {
            tvTitle.setText("Edit Account");

            try {
                JSONObject billingObject = new JSONObject(model.getAddress_billing());
                billingModel = new AccountAddressModel();
                billingModel.setId(billingObject.getString("id"));
                billingModel.setAddress(billingObject.getString("alamat"));
                billingModel.setProvinsi_id(billingObject.getString("provinsi"));
                billingModel.setProvinsi_name(billingObject.getString("provinsi_name"));
                billingModel.setKabupaten_id(billingObject.getString("kabupaten"));
                billingModel.setKabupaten_name(billingObject.getString("kabupaten_name"));
                billingModel.setKecamatan_id(billingObject.getString("kecamatan"));
                billingModel.setKecamatan_name(billingObject.getString("kecamatan_name"));
                billingModel.setKelurahan_id(billingObject.getString("kelurahan"));
                billingModel.setKelurahan_name(billingObject.getString("kelurahan_name"));
                billingModel.setKodepos(billingObject.getString("kodepos_name"));
                billingModel.setLatitude(billingObject.getString("latitude"));
                billingModel.setLongitude(billingObject.getString("longitude"));


                JSONObject shippingObject = new JSONObject(model.getAddress_shipping());
                shippingModel = new AccountAddressModel();
                shippingModel.setId(shippingObject.getString("id"));
                shippingModel.setAddress(shippingObject.getString("alamat"));
                shippingModel.setProvinsi_id(shippingObject.getString("provinsi"));
                shippingModel.setProvinsi_name(shippingObject.getString("provinsi_name"));
                shippingModel.setKabupaten_id(shippingObject.getString("kabupaten"));
                shippingModel.setKabupaten_name(shippingObject.getString("kabupaten_name"));
                shippingModel.setKecamatan_id(shippingObject.getString("kecamatan"));
                shippingModel.setKecamatan_name(shippingObject.getString("kecamatan_name"));
                shippingModel.setKelurahan_id(shippingObject.getString("kelurahan"));
                shippingModel.setKelurahan_name(shippingObject.getString("kelurahan_name"));
                shippingModel.setKodepos(shippingObject.getString("kodepos_name"));
                shippingModel.setLatitude(shippingObject.getString("latitude"));
                shippingModel.setLongitude(shippingObject.getString("longitude"));

            } catch (JSONException e) {
                e.printStackTrace();
            }

            etAddress.setText(billingModel.getAddress());
            tvProvinsi.setText(billingModel.getProvinsi_name());
            tvKabupaten.setText(billingModel.getKabupaten_name());
            tvKecamatan.setText(billingModel.getKecamatan_name());
            tvKelurahan.setText(billingModel.getKelurahan_name());
            tvKodepos.setText(billingModel.getKodepos());
            tvBillingLatitude.setText(billingModel.getLatitude());
            tvBillingLongitude.setText(billingModel.getLongitude());
//            tvlat.setText(billingModel.getLatitude());
            mapViewBilling.setVisibility(View.VISIBLE);

            etAddressShipping.setText(shippingModel.getAddress());
            tvProvinsiShipping.setText(shippingModel.getProvinsi_name());
            tvKabupatenShipping.setText(shippingModel.getKabupaten_name());
            tvKecamatanShipping.setText(shippingModel.getKecamatan_name());
            tvKelurahanShipping.setText(shippingModel.getKelurahan_name());
            tvKodeposShipping.setText(shippingModel.getKodepos());
            tvShippingLatitude.setText(shippingModel.getLatitude());
            tvShippingLongitude.setText(shippingModel.getLongitude());
            mapViewShipping.setVisibility(View.VISIBLE);


            provinsiID = billingModel.getProvinsi_id();
            kabupatenID = billingModel.getKabupaten_id();
            kecamatanID = billingModel.getKecamatan_id();
            kelurahanID = billingModel.getKelurahan_id();
            latutudeID = billingModel.getLatitude();


            provinsiShippingID = shippingModel.getProvinsi_id();
            kabupatenShippingID = shippingModel.getKabupaten_id();
            kecamatanShippingID = shippingModel.getKecamatan_id();
            kelurahanShippingID = shippingModel.getKelurahan_id();

            if (!billingModel.getAddress().equalsIgnoreCase(shippingModel.getAddress())) {
                cbShipping.setChecked(false);
                llShipping.setVisibility(View.VISIBLE);
            }


            mapViewShipping = findViewById(R.id.mapViewShipping);
//            mapViewShipping.setStyleUrl(getResources().getString(R.string.layer_streets) + "?auth=" + getResources().getString(R.string.key_access_token_development));
//        mapView.setStyleUrl(Style.MAPBOX_STREETS);
            mapViewShipping.getMapAsync((MapboxMap mapboxMap) -> {
                mapboxMap.setMaxZoomPreference(18);
                mapboxMap.addMarker(new MarkerOptions()
                        .position(new LatLng(Double.parseDouble(shippingModel.getLatitude()),
                                Double.parseDouble(shippingModel.getLongitude()))));

                LatLng latLng = new LatLng(Double.parseDouble(shippingModel.getLatitude()),
                        Double.parseDouble(shippingModel.getLongitude()));

                mapboxMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 20));
            });

            mapViewBilling = findViewById(R.id.mapViewBilling);
            mapViewBilling.setStyleUrl(getResources().getString(R.string.layer_streets) + "?auth=" + getResources().getString(R.string.key_access_token_development));
//        mapView.setStyleUrl(Style.MAPBOX_STREETS);
            mapViewBilling.getMapAsync((MapboxMap mapboxMap) -> {
                mapboxMap.setMaxZoomPreference(18);
                mapboxMap.addMarker(new MarkerOptions()
                        .position(new LatLng(Double.parseDouble(billingModel.getLatitude()),
                                Double.parseDouble(billingModel.getLongitude()))));

                LatLng latLng = new LatLng(Double.parseDouble(billingModel.getLatitude()),
                        Double.parseDouble(billingModel.getLongitude()));

                mapboxMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 20));
            });


        }
    }

    private void getDataIntent() {
        Intent data = getIntent();
        if (data != null) {
            alamat = data.getStringExtra("alamat");
            jalan = data.getStringExtra("jalan");
            no = data.getStringExtra("no");

            provinsi = data.getStringExtra("provinsi");
            kabupaten = data.getStringExtra("kabupaten");
            kecamatan = data.getStringExtra("kecamatan");
            kelurahan = data.getStringExtra("kelurahan");
            postalCode = data.getStringExtra("postalCode");
            latitude = data.getStringExtra("latitude");
            longitude = data.getStringExtra("longitude");


            etAddress.setText(alamat);
            tvProvinsi.setText(provinsi);
            tvKabupaten.setText(kabupaten);
            tvKecamatan.setText(kecamatan);
            tvKelurahan.setText(kelurahan);
            tvKodepos.setText(postalCode);
            tvBillingLatitude.setText(latitude);
            tvBillingLongitude.setText(longitude);

            etAddressShipping.setText(alamat);
            tvProvinsiShipping.setText(provinsi);
            tvKabupatenShipping.setText(kabupaten);
            tvKecamatanShipping.setText(kecamatan);
            tvKelurahanShipping.setText(kelurahan);
            tvKodeposShipping.setText(postalCode);
            tvShippingLatitude.setText(latitude);
            tvShippingLongitude.setText(longitude);


//                        Toast.makeText(context, latitude, Toast.LENGTH_SHORT).show();
        }
    }



    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.ivBack:
//                    final CustomDialog dialogback = CustomDialog.setupDialogConfirmation(context, "Anda ingin kembali? Data alamat anda akan hilang?");
//                    CustomTextView tvCancel = (CustomTextView) dialogback.findViewById(R.id.tvCancel);
//                    CustomTextView tvYa = (CustomTextView) dialogback.findViewById(R.id.tvOK);
//
//                    tvCancel.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            dialogback.dismiss();
//                        }
//                    });
//
//                    tvYa.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            finish();
//                            dialogback.dismiss();
//                        }
//                    });
//                    dialogback.show();
//                    Intent intent = new Intent(AccountAddAddressActivity.this,AccountAddActivity.class);
//                    startActivity(intent);
                    onBackPressed();
                    break;
                case R.id.ivSave:
                    if (checkField()) {
                        billingModel = new AccountAddressModel();
                        billingModel.setAddress(etAddress.getText().toString());
                        billingModel.setProvinsi_id(provinsiID);
                        billingModel.setKabupaten_id(kabupatenID);
                        billingModel.setKecamatan_id(kecamatanID);
                        billingModel.setKelurahan_id(kelurahanID);
                        billingModel.setProvinsi_name(tvProvinsi.getText().toString());
                        billingModel.setKabupaten_name(tvKabupaten.getText().toString());
                        billingModel.setKecamatan_name(tvKecamatan.getText().toString());
                        billingModel.setKelurahan_name(tvKelurahan.getText().toString());
                        billingModel.setKodepos(tvKodepos.getText().toString());
                        billingModel.setLongitude(tvBillingLongitude.getText().toString());
                        billingModel.setLatitude(tvBillingLatitude.getText().toString());


                        shippingModel = new AccountAddressModel();
                        if (cbShipping.isChecked()) {
                            shippingModel.setAddress(etAddress.getText().toString());
                            shippingModel.setProvinsi_id(provinsiID);
                            shippingModel.setKabupaten_id(kabupatenID);
                            shippingModel.setKecamatan_id(kecamatanID);
                            shippingModel.setKelurahan_id(kelurahanID);
                            shippingModel.setProvinsi_name(tvProvinsi.getText().toString());
                            shippingModel.setKabupaten_name(tvKabupaten.getText().toString());
                            shippingModel.setKecamatan_name(tvKecamatan.getText().toString());
                            shippingModel.setKelurahan_name(tvKelurahan.getText().toString());
                            shippingModel.setKodepos(tvKodepos.getText().toString());
                            shippingModel.setLongitude(tvShippingLongitude.getText().toString());
                            shippingModel.setLatitude(tvShippingLatitude.getText().toString());
                        } else {
                            shippingModel.setAddress(etAddressShipping.getText().toString());
                            shippingModel.setProvinsi_id(provinsiShippingID);
                            shippingModel.setKabupaten_id(kabupatenShippingID);
                            shippingModel.setKecamatan_id(kecamatanShippingID);
                            shippingModel.setKelurahan_id(kelurahanShippingID);
                            shippingModel.setProvinsi_name(tvProvinsiShipping.getText().toString());
                            shippingModel.setKabupaten_name(tvKabupatenShipping.getText().toString());
                            shippingModel.setKecamatan_name(tvKecamatanShipping.getText().toString());
                            shippingModel.setKelurahan_name(tvKelurahanShipping.getText().toString());
                            shippingModel.setKodepos(tvKodeposShipping.getText().toString());
                            shippingModel.setLongitude(tvShippingLongitude.getText().toString());
                            shippingModel.setLatitude(tvShippingLatitude.getText().toString());
                        }

                        tilAddress.setError(null);
                        tilAddressShipping.setError(null);

                        showLoadingDialog();
                        if (from_detail) {
                            presenter.setupEdit(ApiParam.API_012, model, billingModel, shippingModel);
                            Log.e("data address", billingModel.getKabupaten_name());
                        } else {
                            presenter.setupAdd(ApiParam.API_011, model, billingModel, shippingModel);
                            Log.e("data address", billingModel.getKabupaten_name());
                        }
                    }



                    break;
                case R.id.cbShipping:
                    if (cbShipping.isChecked()) {
                        llShipping.setVisibility(View.GONE);
                        tvTitleBilling.setText("Informasi Alamat");
                    } else {
                        llShipping.setVisibility(View.VISIBLE);
                        tvTitleBilling.setText("Informasi Alamat Penagihan");
                    }
                    break;
                case R.id.tvProvinsi:
                    locationFragment = new LocationFragment(AccountAddAddressActivity.this, "provinsi", "0", new LocationDialogCallback() {

                        @Override
                        public void onLocationDialogCallback(Bundle data) {
                            tvProvinsi.setText(data.getString("name"));
                            provinsiID = data.getString("id");

                            tvKabupaten.setText("Kabupaten");
                            tvKecamatan.setText("Kecamatan");
                            tvKelurahan.setText("Kelurahan");
                            tvKodepos.setText("Kodepos");

                            kabupatenID = "0";
                            kecamatanID = "0";
                            kelurahanID = "0";
                        }
                    });
                    locationFragment.show(getSupportFragmentManager(), "locationDialog");
                    break;
                case R.id.tvKabupaten:
                    if (!provinsiID.equalsIgnoreCase("0")) {
                        locationFragment = new LocationFragment(AccountAddAddressActivity.this, "kabupaten", provinsiID, new LocationDialogCallback() {

                            @Override
                            public void onLocationDialogCallback(Bundle data) {
                                tvKabupaten.setText(data.getString("name"));
                                kabupatenID = data.getString("id");

                                tvKecamatan.setText("Kecamatan");
                                tvKelurahan.setText("Kelurahan");
                                tvKodepos.setText("Kodepos");


                                kecamatanID = "0";
                                kelurahanID = "0";
                            }
                        });
                        locationFragment.show(getSupportFragmentManager(), "locationDialog");
                    }
                    break;
                case R.id.tvKecamatan:
                    if (!kabupatenID.equalsIgnoreCase("0")) {
                        locationFragment = new LocationFragment(AccountAddAddressActivity.this, "kecamatan", kabupatenID, new LocationDialogCallback() {

                            @Override
                            public void onLocationDialogCallback(Bundle data) {
                                tvKecamatan.setText(data.getString("name"));
                                kecamatanID = data.getString("id");

                                tvKelurahan.setText("Kelurahan");
                                tvKodepos.setText("Kodepos");

                                kelurahanID = "0";
                            }
                        });
                        locationFragment.show(getSupportFragmentManager(), "locationDialog");
                    }
                    break;
                case R.id.tvKelurahan:
                    if (!kecamatanID.equalsIgnoreCase("0")) {
                        locationFragment = new LocationFragment(AccountAddAddressActivity.this, "kelurahan", kecamatanID, new LocationDialogCallback() {

                            @Override
                            public void onLocationDialogCallback(Bundle data) {
                                tvKelurahan.setText(data.getString("name"));
                                tvKodepos.setText(data.getString("kodepos"));
                                kelurahanID = data.getString("id");
                            }
                        });
                        locationFragment.show(getSupportFragmentManager(), "locationDialog");
                    }
                    break;


                case R.id.tvProvinsiShipping:
                    locationFragment = new LocationFragment(AccountAddAddressActivity.this, "provinsi", "0", new LocationDialogCallback() {

                        @Override
                        public void onLocationDialogCallback(Bundle data) {
                            tvProvinsiShipping.setText(data.getString("name"));
                            provinsiShippingID = data.getString("id");

                            tvKabupatenShipping.setText("Kabupaten");
                            tvKecamatanShipping.setText("Kecamatan");
                            tvKelurahanShipping.setText("Kelurahan");
                            tvKodeposShipping.setText("Kodepos");

                            kabupatenShippingID = "0";
                            kecamatanShippingID = "0";
                            kelurahanShippingID = "0";
                        }
                    });
                    locationFragment.show(getSupportFragmentManager(), "locationDialog");
                    break;
                case R.id.tvKabupatenShipping:
                    if (!provinsiShippingID.equalsIgnoreCase("0")) {
                        locationFragment = new LocationFragment(AccountAddAddressActivity.this, "kabupaten", provinsiShippingID, new LocationDialogCallback() {

                            @Override
                            public void onLocationDialogCallback(Bundle data) {
                                tvKabupatenShipping.setText(data.getString("name"));
                                kabupatenShippingID = data.getString("id");

                                tvKecamatanShipping.setText("Kecamatan");
                                tvKelurahanShipping.setText("Kelurahan");
                                tvKodeposShipping.setText("Kodepos");

                                kecamatanShippingID = "0";
                                kelurahanShippingID = "0";
                            }
                        });
                        locationFragment.show(getSupportFragmentManager(), "locationDialog");
                    }
                    break;
                case R.id.tvKecamatanShipping:
                    if (!kabupatenShippingID.equalsIgnoreCase("0")) {
                        locationFragment = new LocationFragment(AccountAddAddressActivity.this, "kecamatan", kabupatenShippingID, new LocationDialogCallback() {

                            @Override
                            public void onLocationDialogCallback(Bundle data) {
                                tvKecamatanShipping.setText(data.getString("name"));
                                kecamatanShippingID = data.getString("id");

                                tvKelurahanShipping.setText("Kelurahan");
                                tvKodeposShipping.setText("Kodepos");

                                kelurahanShippingID = "0";
                            }
                        });
                        locationFragment.show(getSupportFragmentManager(), "locationDialog");
                    }
                    break;
                case R.id.tvKelurahanShipping:
                    if (!kecamatanShippingID.equalsIgnoreCase("0")) {
                        locationFragment = new LocationFragment(AccountAddAddressActivity.this, "kelurahan", kecamatanShippingID, new LocationDialogCallback() {

                            @Override
                            public void onLocationDialogCallback(Bundle data) {
                                tvKelurahanShipping.setText(data.getString("name"));
                                tvKodeposShipping.setText(data.getString("kodepos"));
                                kelurahanShippingID = data.getString("id");
                            }
                        });
                        locationFragment.show(getSupportFragmentManager(), "locationDialog");
                    }
                    break;
                default:

                    break;
            }
        }
    };

    private boolean checkField() {
        if (etAddress.getText().toString().equalsIgnoreCase("")) {
            tilAddress.setError("Alamat wajib diisi");
            return false;
        } else if (!cbShipping.isChecked() && etAddressShipping.getText().toString().equalsIgnoreCase("")) {
//            tilAddressShipping.setError("Alamat wajib diisi");
            return false;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
//        final CustomDialog dialogback = CustomDialog.setupDialogConfirmation(context, "Anda ingin kembali? Data alamat anda akan hilang?");
//        CustomTextView tvCancel = (CustomTextView) dialogback.findViewById(R.id.tvCancel);
//        CustomTextView tvYa = (CustomTextView) dialogback.findViewById(R.id.tvOK);
//
//        tvCancel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                dialogback.dismiss();
//            }
//        });
//
//        tvYa.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                finish();
////                dialogback.dismiss();
//            }
//        });
////        dialogback.show();
//        Intent intent = new Intent(AccountAddAddressActivity.this,AccountAddActivity.class);
//        startActivity(intent);
        finish();

    }

    @Override
    public void finishAdd(String result, String response) {
        dismissLoadingDialog();
        if (result.equalsIgnoreCase("OK")) {
            finish();
            AccountAddActivity.getInstance().finish();
        }
    }

    @Override
    public void finishEdit(String result, String response) {
        dismissLoadingDialog();
        if (result.equalsIgnoreCase("OK")) {
            finish();
            AccountAddActivity.getInstance().finish();
            AccountDetailActivity.getInstance().finish();
        }
    }

    @Override
    public void onMapReady(MapboxMap mapboxMap) {
        mMapboxMap = mapboxMap;
        mMapboxMap.addOnMapClickListener(this);





//        markerViewBilling = mMapboxMap.addMarker(new MarkerOptions().position(new LatLng(Double.parseDouble(billingModel.getLatitude()),Double.parseDouble(billingModel.getLongitude()))));
//        markerViewShipping = mMapboxMap.addMarker(new MarkerOptions().position(new LatLng(Double.parseDouble(shippingModel.getLatitude()),Double.parseDouble(shippingModel.getLongitude()))));
//        enableLocation();


//        mapViewBilling.getMapAsync((MapboxMap mapboxMap) -> {
//            mapboxMap.setMaxZoomPreference(18);
//            mapboxMap.addMarker(new MarkerOptions()
//                    .position(new LatLng(Double.parseDouble(billingModel.getLatitude()),
//                            Double.parseDouble(billingModel.getLongitude()))));
//
//            LatLng latLng = new LatLng(Double.parseDouble(billingModel.getLatitude()),
//                    Double.parseDouble(billingModel.getLongitude()));
//
//            mapboxMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 20));
//        });



    }

    @Override
    @SuppressWarnings("MissingPermission")
    public void onConnected() {
        locationEngine.requestLocationUpdates();
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            originLocation = location;
//            setCameraPosition(location);

        }

    }

    @SuppressWarnings("MissingPermission")
    private void initializedLocationEngine() {
        locationEngine = new LocationEngineProvider(this).obtainBestLocationEngineAvailable();
        locationEngine.setPriority(LocationEnginePriority.HIGH_ACCURACY);
        locationEngine.activate();

        Location lastLocation = locationEngine.getLastLocation();
        if (lastLocation != null) {
            originLocation = lastLocation;
//            setCameraPosition(lastLocation);
        } else {
            locationEngine.addLocationEngineListener(this);
        }
    }


    @SuppressWarnings("MissingPermission")
    private void intializedLocationLayer() {
        locationLayerPlugin = new LocationLayerPlugin(mapViewBilling, mMapboxMap, locationEngine);
        locationLayerPlugin.setLocationLayerEnabled(true);
        locationLayerPlugin.setCameraMode(CameraMode.TRACKING);
        locationLayerPlugin.setRenderMode(RenderMode.NORMAL);
    }

//    private void setCameraPosition(Location location) {
//      mMapboxMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude()
//      ,location.getLongitude()),8.0));
//    }


    private void enableLocation() {
        if (PermissionsManager.areLocationPermissionsGranted(this)) {
            initializedLocationEngine();
            intializedLocationLayer();
        } else {
            permissionsManager = new PermissionsManager(this);
            permissionsManager.requestLocationPermissions(this);
        }
    }

    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {

    }

    @Override
    public void onPermissionResult(boolean granted) {
        if (granted) {
            enableLocation();
        }

    }




    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onMapClick(@NonNull LatLng point) {

        if (markerViewBilling != null) {
            mMapboxMap.removeMarker(markerViewBilling);
        }


        IconFactory iconFactory = IconFactory.getInstance(context);
        Icon icon = iconFactory.fromResource(R.drawable.mapicon);
        markerViewBilling = mMapboxMap.addMarker(new MarkerOptions()
                .position(point)
                .icon(icon));

        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(point.getLatitude(), point.getLongitude(), 1);
            if (addresses != null && addresses.size() > 0) {
                alamat = addresses.get(0).getAddressLine(0);
                jalan = addresses.get(0).getThoroughfare();
                no = addresses.get(0).getSubThoroughfare();
                kecamatan = addresses.get(0).getLocality();
                String country = addresses.get(0).getCountryName();
                postalCode = addresses.get(0).getPostalCode();
                String knowName = addresses.get(0).getFeatureName();
                kelurahan = addresses.get(0).getSubLocality();
                provinsi = addresses.get(0).getAdminArea();
                kabupaten = addresses.get(0).getSubAdminArea();
                latitude = String.valueOf(addresses.get(0).getLatitude());
                longitude = String.valueOf(addresses.get(0).getLongitude());


                etAddress.setText(alamat);
                tvBillingLatitude.setText(latitude);
                tvBillingLongitude.setText(longitude);

                etAddressShipping.setText(alamat);
                tvShippingLatitude.setText(latitude);
                tvShippingLongitude.setText(longitude);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }




    }


    @Override
    public void onStart() {
        super.onStart();
        mapViewBilling.onStart();
        mapViewShipping.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapViewShipping.onResume();
        mapViewBilling.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapViewShipping.onPause();
        mapViewBilling.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapViewShipping.onStop();
        mapViewBilling.onStop();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapViewShipping.onLowMemory();
        mapViewBilling.onLowMemory();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapViewShipping.onDestroy();
        mapViewBilling.onDestroy();
    }

}