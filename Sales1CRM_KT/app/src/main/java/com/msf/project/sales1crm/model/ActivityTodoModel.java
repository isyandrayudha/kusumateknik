package com.msf.project.sales1crm.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

public class ActivityTodoModel implements Comparable, Parcelable {
	private String taskStatus;
	private String assignTo;
	private String subject;
	private String dueDate;
	private String description;
	private String createdAt;
	private String deletedBy;
	private String priority;
	private String createdBy;
	private String deletedAt;
	private String accountId;
	private String updatedAt;
	private String updatedBy;
	private String id;
	private String status;
	private String user_name;
	private String account_name;
	private String priority_name;
	private String status_name;
	private String task_status_name;


	public ActivityTodoModel(){
		setTaskStatus("");
		setAssignTo("");
		setSubject("");
		setDueDate("");
		setDescription("");
		setCreatedAt("");
		setDeletedBy("");
		setPriority("");
		setCreatedBy("");
		setDeletedAt("");
		setAccountId("");
		setUpdatedAt("");
		setUpdatedBy("");
		setId("");
		setStatus("");
		setUser_name("");
		setAccount_name("");
		setPriority_name("");
		setStatus_name("");
		setTask_status_name("");

	}

	public ActivityTodoModel(Parcel in) {
		readFromParcel(in);
	}

	public void readFromParcel(Parcel in) {
		taskStatus = in.readString();
		assignTo = in.readString();
		subject = in.readString();
		dueDate = in.readString();
		description = in.readString();
		createdAt = in.readString();
		deletedBy = in.readString();
		priority = in.readString();
		createdBy = in.readString();
		deletedAt = in.readString();
		accountId = in.readString();
		updatedAt = in.readString();
		updatedBy = in.readString();
		id = in.readString();
		status = in.readString();
		user_name = in.readString();
		account_name = in.readString();
		priority_name = in.readString();
		status_name = in.readString();
		task_status_name = in.readString();
	}

	public static final Creator<ActivityTodoModel> CREATOR = new Creator<ActivityTodoModel>() {
		@Override
		public ActivityTodoModel createFromParcel(Parcel in) {
			return new ActivityTodoModel(in);
		}

		@Override
		public ActivityTodoModel[] newArray(int size) {
			return new ActivityTodoModel[size];
		}
	};

	public String getPriority_name() {
		return priority_name;
	}

	public void setPriority_name(String priority_name) {
		this.priority_name = priority_name;
	}

	public String getStatus_name() {
		return status_name;
	}

	public void setStatus_name(String status_name) {
		this.status_name = status_name;
	}

	public String getTask_status_name() {
		return task_status_name;
	}

	public void setTask_status_name(String task_status_name) {
		this.task_status_name = task_status_name;
	}

	public void setTaskStatus(String taskStatus){
		this.taskStatus = taskStatus;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public String getAccount_name() {
		return account_name;
	}

	public void setAccount_name(String account_name) {
		this.account_name = account_name;
	}

	public String getTaskStatus(){
		return taskStatus;
	}

	public void setAssignTo(String assignTo){
		this.assignTo = assignTo;
	}

	public String getAssignTo(){
		return assignTo;
	}

	public void setSubject(String subject){
		this.subject = subject;
	}

	public String getSubject(){
		return subject;
	}

	public void setDueDate(String dueDate){
		this.dueDate = dueDate;
	}

	public String getDueDate(){
		return dueDate;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setDeletedBy(String deletedBy){
		this.deletedBy = deletedBy;
	}

	public String getDeletedBy(){
		return deletedBy;
	}

	public void setPriority(String priority){
		this.priority = priority;
	}

	public String getPriority(){
		return priority;
	}

	public void setCreatedBy(String createdBy){
		this.createdBy = createdBy;
	}

	public String getCreatedBy(){
		return createdBy;
	}

	public void setDeletedAt(String deletedAt){
		this.deletedAt = deletedAt;
	}

	public String getDeletedAt(){
		return deletedAt;
	}

	public void setAccountId(String accountId){
		this.accountId = accountId;
	}

	public String getAccountId(){
		return accountId;
	}

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public void setUpdatedBy(String updatedBy){
		this.updatedBy = updatedBy;
	}

	public String getUpdatedBy(){
		return updatedBy;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel parcel, int i) {
		parcel.writeString(taskStatus);
		parcel.writeString(assignTo);
		parcel.writeString(subject);
		parcel.writeString(dueDate);
		parcel.writeString(description);
		parcel.writeString(createdAt);
		parcel.writeString(deletedBy);
		parcel.writeString(priority);
		parcel.writeString(createdBy);
		parcel.writeString(deletedAt);
		parcel.writeString(accountId);
		parcel.writeString(updatedAt);
		parcel.writeString(updatedBy);
		parcel.writeString(id);
		parcel.writeString(status);
		parcel.writeString(user_name);
		parcel.writeString(account_name);
		parcel.writeString(priority_name);
		parcel.writeString(status_name);
		parcel.writeString(task_status_name);
	}

	@Override
	public int compareTo(@NonNull Object o) {
		return 0;
	}
}
