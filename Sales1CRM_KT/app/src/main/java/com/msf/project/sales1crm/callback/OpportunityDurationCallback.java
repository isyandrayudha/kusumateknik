package com.msf.project.sales1crm.callback;

import com.msf.project.sales1crm.model.TotalModel;

import java.util.List;

public interface OpportunityDurationCallback {
    void finished(String result, List<TotalModel> wonIndustry, List<TotalModel> loseIndustry, List<TotalModel> wonSource, List<TotalModel> loseSource);
}
