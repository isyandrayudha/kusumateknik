package com.msf.project.sales1crm.model;

import java.util.List;

public class OpportunityItem{
	private String closeDate;
	private String categoryName;
	private String ownerId;
	private String type;
	private String branch;
	private String opportunitySize;
	private Billing billing;
	private String lastUpdatedBy;
	private String deletedDate;
	private Shipping shipping;
	private String nextStep;
	private String opportunityName;
	private String stageName;
	private String id;
	private String createDate;
	private String openDate;
	private String ownerName;
	private String stageId;
	private String probability;
	private String updateDate;
	private String userId;
	private String phone;
	private String showEditButton;
	private String status;
	private String positionId;
	private String other;
	private String industryId;
	private String assignTo;
	private String userName;
	private String description;
	private String contactId;
	private String stagePercentage;
	private String categoryId;
	private String closeDateInterval;
	private String accountName;
	private String opportunityStatusName;
	private String titleName;
	private String firstName;
	private String email;
	private String contactName;
	private List<ProductItem> product;
	private String reminder;
	private String positionName;
	private String titleId;
	private String lastName;
	private String leadSourceId;
	private String opportunityStatusId;
	private String accountId;
	private String lastUpdatedName;
	private List<AccountContactItem> accountContact;
	private String actualCloseDate;
	private String leadSourceName;
	private String showDeleteButton;

	public void setCloseDate(String closeDate){
		this.closeDate = closeDate;
	}

	public String getCloseDate(){
		return closeDate;
	}

	public void setCategoryName(String categoryName){
		this.categoryName = categoryName;
	}

	public String getCategoryName(){
		return categoryName;
	}

	public void setOwnerId(String ownerId){
		this.ownerId = ownerId;
	}

	public String getOwnerId(){
		return ownerId;
	}

	public void setType(String type){
		this.type = type;
	}

	public String getType(){
		return type;
	}

	public void setBranch(String branch){
		this.branch = branch;
	}

	public String getBranch(){
		return branch;
	}

	public void setOpportunitySize(String opportunitySize){
		this.opportunitySize = opportunitySize;
	}

	public String getOpportunitySize(){
		return opportunitySize;
	}

	public void setBilling(Billing billing){
		this.billing = billing;
	}

	public Billing getBilling(){
		return billing;
	}

	public void setLastUpdatedBy(String lastUpdatedBy){
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public String getLastUpdatedBy(){
		return lastUpdatedBy;
	}

	public void setDeletedDate(String deletedDate){
		this.deletedDate = deletedDate;
	}

	public String getDeletedDate(){
		return deletedDate;
	}

	public void setShipping(Shipping shipping){
		this.shipping = shipping;
	}

	public Shipping getShipping(){
		return shipping;
	}

	public void setNextStep(String nextStep){
		this.nextStep = nextStep;
	}

	public String getNextStep(){
		return nextStep;
	}

	public void setOpportunityName(String opportunityName){
		this.opportunityName = opportunityName;
	}

	public String getOpportunityName(){
		return opportunityName;
	}

	public void setStageName(String stageName){
		this.stageName = stageName;
	}

	public String getStageName(){
		return stageName;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setCreateDate(String createDate){
		this.createDate = createDate;
	}

	public String getCreateDate(){
		return createDate;
	}

	public void setOpenDate(String openDate){
		this.openDate = openDate;
	}

	public String getOpenDate(){
		return openDate;
	}

	public void setOwnerName(String ownerName){
		this.ownerName = ownerName;
	}

	public String getOwnerName(){
		return ownerName;
	}

	public void setStageId(String stageId){
		this.stageId = stageId;
	}

	public String getStageId(){
		return stageId;
	}

	public void setProbability(String probability){
		this.probability = probability;
	}

	public String getProbability(){
		return probability;
	}

	public void setUpdateDate(String updateDate){
		this.updateDate = updateDate;
	}

	public String getUpdateDate(){
		return updateDate;
	}

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public void setPhone(String phone){
		this.phone = phone;
	}

	public String getPhone(){
		return phone;
	}

	public void setShowEditButton(String showEditButton){
		this.showEditButton = showEditButton;
	}

	public String getShowEditButton(){
		return showEditButton;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	public void setPositionId(String positionId){
		this.positionId = positionId;
	}

	public String getPositionId(){
		return positionId;
	}

	public void setOther(String other){
		this.other = other;
	}

	public String getOther(){
		return other;
	}

	public void setIndustryId(String industryId){
		this.industryId = industryId;
	}

	public String getIndustryId(){
		return industryId;
	}

	public void setAssignTo(String assignTo){
		this.assignTo = assignTo;
	}

	public String getAssignTo(){
		return assignTo;
	}

	public void setUserName(String userName){
		this.userName = userName;
	}

	public String getUserName(){
		return userName;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setContactId(String contactId){
		this.contactId = contactId;
	}

	public String getContactId(){
		return contactId;
	}

	public void setStagePercentage(String stagePercentage){
		this.stagePercentage = stagePercentage;
	}

	public String getStagePercentage(){
		return stagePercentage;
	}

	public void setCategoryId(String categoryId){
		this.categoryId = categoryId;
	}

	public String getCategoryId(){
		return categoryId;
	}

	public void setCloseDateInterval(String closeDateInterval){
		this.closeDateInterval = closeDateInterval;
	}

	public String getCloseDateInterval(){
		return closeDateInterval;
	}

	public void setAccountName(String accountName){
		this.accountName = accountName;
	}

	public String getAccountName(){
		return accountName;
	}

	public void setOpportunityStatusName(String opportunityStatusName){
		this.opportunityStatusName = opportunityStatusName;
	}

	public String getOpportunityStatusName(){
		return opportunityStatusName;
	}

	public void setTitleName(String titleName){
		this.titleName = titleName;
	}

	public String getTitleName(){
		return titleName;
	}

	public void setFirstName(String firstName){
		this.firstName = firstName;
	}

	public String getFirstName(){
		return firstName;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setContactName(String contactName){
		this.contactName = contactName;
	}

	public String getContactName(){
		return contactName;
	}

	public void setProduct(List<ProductItem> product){
		this.product = product;
	}

	public List<ProductItem> getProduct(){
		return product;
	}

	public void setReminder(String reminder){
		this.reminder = reminder;
	}

	public String getReminder(){
		return reminder;
	}

	public void setPositionName(String positionName){
		this.positionName = positionName;
	}

	public String getPositionName(){
		return positionName;
	}

	public void setTitleId(String titleId){
		this.titleId = titleId;
	}

	public String getTitleId(){
		return titleId;
	}

	public void setLastName(String lastName){
		this.lastName = lastName;
	}

	public String getLastName(){
		return lastName;
	}

	public void setLeadSourceId(String leadSourceId){
		this.leadSourceId = leadSourceId;
	}

	public String getLeadSourceId(){
		return leadSourceId;
	}

	public void setOpportunityStatusId(String opportunityStatusId){
		this.opportunityStatusId = opportunityStatusId;
	}

	public String getOpportunityStatusId(){
		return opportunityStatusId;
	}

	public void setAccountId(String accountId){
		this.accountId = accountId;
	}

	public String getAccountId(){
		return accountId;
	}

	public void setLastUpdatedName(String lastUpdatedName){
		this.lastUpdatedName = lastUpdatedName;
	}

	public String getLastUpdatedName(){
		return lastUpdatedName;
	}

	public void setAccountContact(List<AccountContactItem> accountContact){
		this.accountContact = accountContact;
	}

	public List<AccountContactItem> getAccountContact(){
		return accountContact;
	}

	public void setActualCloseDate(String actualCloseDate){
		this.actualCloseDate = actualCloseDate;
	}

	public String getActualCloseDate(){
		return actualCloseDate;
	}

	public void setLeadSourceName(String leadSourceName){
		this.leadSourceName = leadSourceName;
	}

	public String getLeadSourceName(){
		return leadSourceName;
	}

	public void setShowDeleteButton(String showDeleteButton){
		this.showDeleteButton = showDeleteButton;
	}

	public String getShowDeleteButton(){
		return showDeleteButton;
	}

	@Override
 	public String toString(){
		return 
			"OpportunityItem{" + 
			"close_date = '" + closeDate + '\'' + 
			",category_name = '" + categoryName + '\'' + 
			",owner_id = '" + ownerId + '\'' + 
			",type = '" + type + '\'' + 
			",branch = '" + branch + '\'' + 
			",opportunity_size = '" + opportunitySize + '\'' + 
			",billing = '" + billing + '\'' + 
			",last_updated_by = '" + lastUpdatedBy + '\'' + 
			",deleted_date = '" + deletedDate + '\'' + 
			",shipping = '" + shipping + '\'' + 
			",next_step = '" + nextStep + '\'' + 
			",opportunity_name = '" + opportunityName + '\'' + 
			",stage_name = '" + stageName + '\'' + 
			",id = '" + id + '\'' + 
			",create_date = '" + createDate + '\'' + 
			",open_date = '" + openDate + '\'' + 
			",owner_name = '" + ownerName + '\'' + 
			",stage_id = '" + stageId + '\'' + 
			",probability = '" + probability + '\'' + 
			",update_date = '" + updateDate + '\'' + 
			",user_id = '" + userId + '\'' + 
			",phone = '" + phone + '\'' + 
			",show_edit_button = '" + showEditButton + '\'' + 
			",status = '" + status + '\'' + 
			",position_id = '" + positionId + '\'' + 
			",other = '" + other + '\'' + 
			",industry_id = '" + industryId + '\'' + 
			",assign_to = '" + assignTo + '\'' + 
			",user_name = '" + userName + '\'' + 
			",description = '" + description + '\'' + 
			",contact_id = '" + contactId + '\'' + 
			",stage_percentage = '" + stagePercentage + '\'' + 
			",category_id = '" + categoryId + '\'' + 
			",close_date_interval = '" + closeDateInterval + '\'' + 
			",account_name = '" + accountName + '\'' + 
			",opportunity_status_name = '" + opportunityStatusName + '\'' + 
			",title_name = '" + titleName + '\'' + 
			",first_name = '" + firstName + '\'' + 
			",email = '" + email + '\'' + 
			",contact_name = '" + contactName + '\'' + 
			",product = '" + product + '\'' + 
			",reminder = '" + reminder + '\'' + 
			",position_name = '" + positionName + '\'' + 
			",title_id = '" + titleId + '\'' + 
			",last_name = '" + lastName + '\'' + 
			",lead_source_id = '" + leadSourceId + '\'' + 
			",opportunity_status_id = '" + opportunityStatusId + '\'' + 
			",account_id = '" + accountId + '\'' + 
			",last_updated_name = '" + lastUpdatedName + '\'' + 
			",account_contact = '" + accountContact + '\'' + 
			",actual_close_date = '" + actualCloseDate + '\'' + 
			",lead_source_name = '" + leadSourceName + '\'' + 
			",show_delete_button = '" + showDeleteButton + '\'' + 
			"}";
		}
}