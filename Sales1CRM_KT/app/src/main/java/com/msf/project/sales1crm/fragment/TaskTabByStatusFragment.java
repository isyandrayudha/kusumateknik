package com.msf.project.sales1crm.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.msf.project.sales1crm.MenuActivity;
import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.TaskAddActivity;
import com.msf.project.sales1crm.adapter.TaskTabByStatusAdapter;
import com.msf.project.sales1crm.callback.SortDialogCallback;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class TaskTabByStatusFragment extends Fragment {

    //    @BindView(R.id.etSearch)
//    CustomEditText etSearch;
//    @BindView(R.id.ivClear)
//    ImageView ivClear;
    @BindView(R.id.viewpager)
    ViewPager viewpager;
    @BindView(R.id.tabDots)
    TabLayout tabDots;
    Unbinder unbinder;
    @BindView(R.id.faTaskAdd)
    FloatingActionButton faTaskAdd;
    @BindView(R.id.menu)
    FloatingActionMenu menu;
    @BindView(R.id.prev)
    ImageView prev;
    @BindView(R.id.next)
    ImageView next;
    private View view;
    private Context context;
    private TaskTabByStatusAdapter adapter;
    private MenuActivity menuActivity;
    private int sort_id = 0;
    private String[] sort = {"Subject A to Z", "Sub Z to A", "account Name A to Z", "account Name Z to A", "Due Date A to Z", "Due Date Z to A"};


    public static TaskTabByStatusFragment newInstance() {
        TaskTabByStatusFragment fragment = new TaskTabByStatusFragment();
        return fragment;
    }

    interface ClickActionFilterListner{
        void onClickItemFilter(Integer id);
    }

    private ClickActionFilterListner mFilterLisner;

    public void setClickFilterListener (ClickActionFilterListner listener) {
        this.mFilterLisner = listener;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onAttach(Activity activity) {
        menuActivity = (MenuActivity) activity;
        super.onAttach(activity);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_task_tab, container, false);
        setHasOptionsMenu(true);
        ButterKnife.bind(this, this.view);
        this.context = getActivity();
        return this.view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.context = getActivity();
        this.context = getActivity();
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Task");
        initData();

    }



    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.faTaskAdd:
                        startActivity(new Intent(context, TaskAddActivity.class));
                    break;
                case R.id.prev:
                    viewpager.setCurrentItem(getItem(-1),true);
                    break;
                case R.id.next:
                    viewpager.setCurrentItem(getItem(+1),true);
            }
        }
    };

    private int getItem(int i) {
        return viewpager.getCurrentItem() + i;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_item, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_cards:
                    MenuActivity.getInstance().setFragment(TaskTabByStatusFragment.newInstance());
                break;
            case R.id.action_list:
                    MenuActivity.getInstance().setFragment(TaskFragment.newInstance());
                break;
            case R.id.action_filter:
                SortDialogFragment dialogFragment = new SortDialogFragment(context, new SortDialogCallback() {
                    @Override
                    public void onDialogCallback(Bundle data) {
                        sort_id = Integer.parseInt(data.getString("id"));
                        mFilterLisner.onClickItemFilter(sort_id);
                    }
                }, sort);
                dialogFragment.show(getActivity().getSupportFragmentManager(),"sortDialog");
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    private void initData() {
        faTaskAdd.setOnClickListener(click);
        prev.setOnClickListener(click);
        next.setOnClickListener(click);
        tabDots.addTab(tabDots.newTab());
        tabDots.addTab(tabDots.newTab());
        tabDots.addTab(tabDots.newTab());
        tabDots.addTab(tabDots.newTab());
        tabDots.addTab(tabDots.newTab());
        adapter = new TaskTabByStatusAdapter(menuActivity.getSupportFragmentManager(), tabDots.getTabCount(), this);
        viewpager.setAdapter(adapter);
        viewpager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabDots));
        tabDots.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
