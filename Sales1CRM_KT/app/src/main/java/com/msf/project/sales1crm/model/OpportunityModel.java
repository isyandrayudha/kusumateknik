package com.msf.project.sales1crm.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

/**
 * Created by christianmacbook on 21/05/18.
 */

public class OpportunityModel implements Comparable, Parcelable {
    private String id;
    private String account_id;
    private String account_name;
    private String title_id;
    private String title_name;
    private String name;
    private String first_name;
    private String last_name;
    private String phone;
    private String email;
    private String size;
    private String type;
    private String position_id;
    private String position_name;
    private String stage_id;
    private String stage_name;
    private String source_id;
    private String source_name;
    private String oppor_status_id;
    private String oppor_status_name;
    private String progress;
    private String next_step;
    private String description;
    private String owner_id;
    private String address_billing;
    private String address_shipping;
    private String owner_name;
    private String category_id;
    private String category_name;
    private String open_date;
    private String update_date;
    private String close_date;
    private String close_date_interval;
    private String pic;
    private String account_contact;
    private String industry;
    private String product;
    private String assign_to;
    private String assign_to_name;
    private String contact_id;
    private String contact_name;
    private String probability;
    private String actual_opportunity_size;
    private String final_quotation_id;
    private String complete_ref_number;
    private String reason_not_deal;

    private OpportunityBillingModel oppotunityBilling;

    public OpportunityModel() {
        setId("");
        setAccount_id("");
        setAccount_name("");
        setTitle_id("");
        setTitle_name("");
        setName("");
        setFirst_name("");
        setLast_name("");
        setPhone("");
        setEmail("");
        setSize("");
        setType("");
        setPosition_id("");
        setPosition_name("");
        setStage_id("");
        setStage_name("");
        setSource_id("");
        setSource_name("");
        setOppor_status_id("");
        setOppor_status_name("");
        setProgress("");
        setNext_step("");
        setDescription("");
        setOwner_id("");
        setOwner_name("");
        setCategory_id("");
        setCategory_name("");
        setOpen_date("");
        setClose_date("");
        setClose_date_interval("");
        setPic("");
        setAccount_contact("");
        setIndustry("");
        setProduct("");
        setAssign_to("");
        setAssign_to_name("");
        setContact_id("");
        setContact_name("");
        setUpdate_date("");
        setProbability("");
        setActual_opportunity_size("");
        setFinal_quotation_id("");
        setComplete_ref_number("");
        setReason_not_deal("");
    }

    public OpportunityModel(Parcel in) {
        readFromParcel(in);
    }

    public void readFromParcel(Parcel in) {
        id = in.readString();
        account_id = in.readString();
        account_name = in.readString();
        title_id = in.readString();
        title_name = in.readString();
        name = in.readString();
        first_name = in.readString();
        last_name = in.readString();
        phone = in.readString();
        email = in.readString();
        size = in.readString();
        type = in.readString();
        position_id = in.readString();
        position_name = in.readString();
        stage_id = in.readString();
        stage_name = in.readString();
        source_id = in.readString();
        source_name = in.readString();
        oppor_status_id = in.readString();
        oppor_status_name = in.readString();
        progress = in.readString();
        next_step = in.readString();
        description = in.readString();
        owner_id = in.readString();
        owner_name = in.readString();
        category_id = in.readString();
        category_name = in.readString();
        open_date = in.readString();
        close_date = in.readString();
        close_date_interval = in.readString();
        pic = in.readString();
        account_contact = in.readString();
        industry = in.readString();
        product = in.readString();
        assign_to = in.readString();
        assign_to_name = in.readString();
        contact_id = in.readString();
        contact_name = in.readString();
        address_billing = in.readString();
        address_shipping = in.readString();
        update_date = in.readString();
        probability = in.readString();
        actual_opportunity_size = in.readString();
        final_quotation_id = in.readString();
        complete_ref_number = in.readString();
        reason_not_deal = in.readString();
    }

    public String getAddress_billing() {
        return address_billing;
    }

    public void setAddress_billing(String address_billing) {
        this.address_billing = address_billing;
    }

    public String getAddress_shipping() {
        return address_shipping;
    }

    public void setAddress_shipping(String address_shipping) {
        this.address_shipping = address_shipping;
    }

    public static final Creator<OpportunityModel> CREATOR = new Creator<OpportunityModel>() {
        @Override
        public OpportunityModel createFromParcel(Parcel in) {
            return new OpportunityModel(in);
        }

        @Override
        public OpportunityModel[] newArray(int size) {
            return new OpportunityModel[size];
        }
    };

    public OpportunityBillingModel getOppotunityBilling() {
        return oppotunityBilling;
    }

    public void setOppotunityBilling(OpportunityBillingModel oppotunityBilling) {
        this.oppotunityBilling = oppotunityBilling;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAccount_id() {
        return account_id;
    }

    public void setAccount_id(String account_id) {
        this.account_id = account_id;
    }

    public String getAccount_name() {
        return account_name;
    }

    public void setAccount_name(String account_name) {
        this.account_name = account_name;
    }

    public String getTitle_id() {
        return title_id;
    }

    public void setTitle_id(String title_id) {
        this.title_id = title_id;
    }

    public String getTitle_name() {
        return title_name;
    }

    public void setTitle_name(String title_name) {
        this.title_name = title_name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPosition_id() {
        return position_id;
    }

    public void setPosition_id(String position_id) {
        this.position_id = position_id;
    }

    public String getPosition_name() {
        return position_name;
    }

    public void setPosition_name(String position_name) {
        this.position_name = position_name;
    }

    public String getStage_id() {
        return stage_id;
    }

    public void setStage_id(String stage_id) {
        this.stage_id = stage_id;
    }

    public String getStage_name() {
        return stage_name;
    }

    public void setStage_name(String stage_name) {
        this.stage_name = stage_name;
    }

    public String getSource_id() {
        return source_id;
    }

    public void setSource_id(String source_id) {
        this.source_id = source_id;
    }

    public String getSource_name() {
        return source_name;
    }

    public void setSource_name(String source_name) {
        this.source_name = source_name;
    }

    public String getOppor_status_id() {
        return oppor_status_id;
    }

    public void setOppor_status_id(String oppor_status_id) {
        this.oppor_status_id = oppor_status_id;
    }

    public String getOppor_status_name() {
        return oppor_status_name;
    }

    public void setOppor_status_name(String oppor_status_name) {
        this.oppor_status_name = oppor_status_name;
    }

    public String getProgress() {
        return progress;
    }

    public void setProgress(String progress) {
        this.progress = progress;
    }

    public String getNext_step() {
        return next_step;
    }

    public void setNext_step(String next_step) {
        this.next_step = next_step;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOwner_id() {
        return owner_id;
    }

    public void setOwner_id(String owner_id) {
        this.owner_id = owner_id;
    }

    public String getOwner_name() {
        return owner_name;
    }

    public void setOwner_name(String owner_name) {
        this.owner_name = owner_name;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }

    public String getOpen_date() {
        return open_date;
    }

    public void setOpen_date(String open_date) {
        this.open_date = open_date;
    }

    public String getClose_date() {
        return close_date;
    }

    public void setClose_date(String close_date) {
        this.close_date = close_date;
    }

    public String getClose_date_interval() {
        return close_date_interval;
    }

    public void setClose_date_interval(String close_date_interval) {
        this.close_date_interval = close_date_interval;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getAccount_contact() {
        return account_contact;
    }

    public void setAccount_contact(String account_contact) {
        this.account_contact = account_contact;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getAssign_to() {
        return assign_to;
    }

    public void setAssign_to(String assign_to) {
        this.assign_to = assign_to;
    }

    public String getAssign_to_name() {
        return assign_to_name;
    }

    public void setAssign_to_name(String assign_to_name) {
        this.assign_to_name = assign_to_name;
    }

    public String getContact_id() {
        return contact_id;
    }

    public void setContact_id(String contact_id) {
        this.contact_id = contact_id;
    }

    public String getContact_name() {
        return contact_name;
    }

    public void setContact_name(String contact_name) {
        this.contact_name = contact_name;
    }

    public String getUpdate_date() {
        return update_date;
    }

    public void setUpdate_date(String update_date) {
        this.update_date = update_date;
    }

    public String getProbability() {
        return probability;
    }

    public void setProbability(String probability) {
        this.probability = probability;
    }

    public String getActual_opportunity_size() {
        return actual_opportunity_size;
    }

    public void setActual_opportunity_size(String actual_opportunity_size) {
        this.actual_opportunity_size = actual_opportunity_size;
    }

    public String getFinal_quotation_id() {
        return final_quotation_id;
    }

    public void setFinal_quotation_id(String final_quotation_id) {
        this.final_quotation_id = final_quotation_id;
    }

    public String getComplete_ref_number() {
        return complete_ref_number;
    }

    public void setComplete_ref_number(String complete_ref_number) {
        this.complete_ref_number = complete_ref_number;
    }

    public String getReason_not_deal() {
        return reason_not_deal;
    }

    public void setReason_not_deal(String reason_not_deal) {
        this.reason_not_deal = reason_not_deal;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(account_id);
        dest.writeString(account_name);
        dest.writeString(title_id);
        dest.writeString(title_name);
        dest.writeString(name);
        dest.writeString(first_name);
        dest.writeString(last_name);
        dest.writeString(phone);
        dest.writeString(email);
        dest.writeString(size);
        dest.writeString(type);
        dest.writeString(position_id);
        dest.writeString(position_name);
        dest.writeString(stage_id);
        dest.writeString(stage_name);
        dest.writeString(source_id);
        dest.writeString(source_name);
        dest.writeString(oppor_status_id);
        dest.writeString(oppor_status_name);
        dest.writeString(progress);
        dest.writeString(next_step);
        dest.writeString(description);
        dest.writeString(owner_id);
        dest.writeString(owner_name);
        dest.writeString(category_id);
        dest.writeString(category_name);
        dest.writeString(open_date);
        dest.writeString(close_date);
        dest.writeString(close_date_interval);
        dest.writeString(pic);
        dest.writeString(account_contact);
        dest.writeString(industry);
        dest.writeString(product);
        dest.writeString(assign_to);
        dest.writeString(assign_to_name);
        dest.writeString(contact_id);
        dest.writeString(contact_name);
        dest.writeString(address_billing);
        dest.writeString(address_shipping);
        dest.writeString(update_date);
        dest.writeString(probability);
        dest.writeString(actual_opportunity_size);
        dest.writeString(final_quotation_id);
        dest.writeString(complete_ref_number);
        dest.writeString(reason_not_deal);
    }

    @Override
    public int compareTo(@NonNull Object o) {
        return 0;
    }
}
