package com.msf.project.sales1crm.callback;

public interface OpportunityAddCallback {
    void finishAdd(String result, String response);
    void finishEdit(String result, String response);
}
