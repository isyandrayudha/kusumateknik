package com.msf.project.sales1crm;

import android.animation.LayoutTransition;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Cobacoba extends AppCompatActivity {

    @BindView(R.id.add)
    Button add;
    @BindView(R.id.textin)
    EditText textin;
    @BindView(R.id.container)
    LinearLayout container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cobacoba);
        ButterKnife.bind(this);


        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("TAG","coba" + textin.getText().toString());
                LayoutInflater layoutInflater =
                        (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View addView = layoutInflater.inflate(R.layout.coba, null);
                final EditText textOut = (EditText) addView.findViewById(R.id.textout);
                textOut.setText(textin.getText().toString());
                Button buttonRemove = (Button) addView.findViewById(R.id.remove);
                buttonRemove.setOnClickListener(new View.OnClickListener(){

                    @Override
                    public void onClick(View v) {
                        ((LinearLayout)addView.getParent()).removeView(addView);
                    }});

                Button buttonInsert = (Button)addView.findViewById(R.id.insert);
                buttonInsert.setOnClickListener(new View.OnClickListener(){

                    @Override
                    public void onClick(View v) {
                        String text = textOut.getText().toString();
                        String newText = textin.getText().toString() + text;
                        textin.setText(newText);
                    }});

                container.addView(addView, 0);
            }

        });

        LayoutTransition transition = new LayoutTransition();
        container.setLayoutTransition(transition);
    }}
