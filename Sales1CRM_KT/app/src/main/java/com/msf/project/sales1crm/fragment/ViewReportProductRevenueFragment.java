package com.msf.project.sales1crm.fragment;

import android.app.DatePickerDialog;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.callback.AccountDialogCallback;
import com.msf.project.sales1crm.callback.TotalStatsCallback;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.customview.DatePickerFragment;
import com.msf.project.sales1crm.customview.SpinnerCustomAdapter;
import com.msf.project.sales1crm.model.TotalModel;
import com.msf.project.sales1crm.presenter.ViewReportProductSoldPresenter;
import com.msf.project.sales1crm.service.GPSTracker;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ViewReportProductRevenueFragment extends Fragment implements TotalStatsCallback {

    @BindView(R.id.pieChart)
    PieChart pieChart;

    @BindView(R.id.pieChart1)
    PieChart pieChart1;

    @BindView(R.id.tvStartPeriod1)
    CustomTextView tvStartPeriod1;

    @BindView(R.id.tvEndPeriod1)
    CustomTextView tvEndPeriod1;

    @BindView(R.id.tvStartPeriod2)
    CustomTextView tvStartPeriod2;

    @BindView(R.id.tvEndPeriod2)
    CustomTextView tvEndPeriod2;

    @BindView(R.id.tvAssignTo)
    CustomTextView tvAssignTo;

    @BindView(R.id.tvSubmit)
    CustomTextView tvSubmit;

    private View view;
    private Context context;
    private ViewReportProductSoldPresenter presenter;
    private SpinnerCustomAdapter spinnerCustomAdapter;
    private List<TotalModel> list = new ArrayList<>();
    private List<TotalModel> list1 = new ArrayList<>();
    private String startDate1 = "", endDate1 = "", startDate2 = "", endDate2 = "";
    private DatePickerFragment datepicker1, datepicker2, datepicker3, datepicker4;
    private String open_date = "", string_year, string_month, string_day;
    private int year, month, day;
    private String assign_id = "";

    public static ViewReportProductRevenueFragment newInstance() {
        ViewReportProductRevenueFragment fragment = new ViewReportProductRevenueFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_productrevenue, container, false);

        return this.view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);
        this.context = getActivity();

        this.presenter = new ViewReportProductSoldPresenter(context, this);

        datepicker1 = new DatePickerFragment();
        datepicker1.setListener(dateListener1);

        datepicker2 = new DatePickerFragment();
        datepicker2.setListener(dateListener2);

        datepicker3 = new DatePickerFragment();
        datepicker3.setListener(dateListener3);

        datepicker4 = new DatePickerFragment();
        datepicker4.setListener(dateListener4);

        initDate();
        initData();
    }

    private void initDate() {
        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH) + 1;
        day = c.get(Calendar.DAY_OF_MONTH);
    }

    private void initData(){

        tvStartPeriod1.setOnClickListener(click);
        tvEndPeriod1.setOnClickListener(click);
        tvStartPeriod2.setOnClickListener(click);
        tvEndPeriod2.setOnClickListener(click);
        tvAssignTo.setOnClickListener(click);
        tvSubmit.setOnClickListener(click);
    }

    public void getDataFromAPI(){
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(context)){
            GPSTracker gpsTracker = new GPSTracker(context);
            if (gpsTracker.canGetLocation()) {
                presenter.setupTotal(ApiParam.API_102, startDate1, endDate1, startDate2, endDate2, assign_id);
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gpsTracker.showSettingsAlert();
            }
        }
    }

    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.tvSubmit:
                    getDataFromAPI();
                    break;
                case R.id.tvAssignTo:
                    AccountDialogFragment accountDialogFragment = new AccountDialogFragment(context, new AccountDialogCallback() {

                        @Override
                        public void onAccountDialogCallback(Bundle data) {
                            tvAssignTo.setText(data.getString("name"));
                            assign_id = data.getString("id");
                        }
                    });
                    accountDialogFragment.show(getActivity().getSupportFragmentManager(), "accountDialog");
                    break;
                case R.id.tvStartPeriod1:
                    datepicker1.setDateFragment(year, month, day);
                    datepicker1.show(getActivity().getFragmentManager(), "datePicker");
                    break;
                case R.id.tvEndPeriod1:
                    datepicker2.setDateFragment(year, month, day);
                    datepicker2.show(getActivity().getFragmentManager(), "datePicker");
                    break;
                case R.id.tvStartPeriod2:
                    datepicker3.setDateFragment(year, month, day);
                    datepicker3.show(getActivity().getFragmentManager(), "datePicker");
                    break;
                case R.id.tvEndPeriod2:
                    datepicker4.setDateFragment(year, month, day);
                    datepicker4.show(getActivity().getFragmentManager(), "datePicker");
                    break;
            }
        }
    };

    private void setPieCurrent(ArrayList<PieEntry> current){
        PieDataSet dataSet = new PieDataSet(current, "");
        dataSet.setColors(ColorTemplate.COLORFUL_COLORS);

        PieData data = new PieData(dataSet);
        // In Percentage
        data.setValueTextSize(13f);
        data.setValueTextColor(Color.DKGRAY);

        // Default value
        //data.setValueFormatter(new DefaultValueFormatter(0));
        pieChart.setData(data);
        pieChart.setEntryLabelColor(Color.DKGRAY);
        pieChart.getDescription().setEnabled(false);
        pieChart.setDrawHoleEnabled(true);
        pieChart.setTransparentCircleRadius(58f);
        pieChart.setHoleRadius(58f);
        pieChart.spin( 500,0,-360f, Easing.EasingOption.EaseInOutQuad);
        pieChart.invalidate(); // refresh
    }

    private void setPieCurrent1(ArrayList<PieEntry> current){
        PieDataSet dataSet = new PieDataSet(current, "");
        dataSet.setColors(ColorTemplate.COLORFUL_COLORS);

        PieData data = new PieData(dataSet);
        // In Percentage
        data.setValueTextSize(13f);
        data.setValueTextColor(Color.DKGRAY);

        // Default value
        //data.setValueFormatter(new DefaultValueFormatter(0));
        pieChart1.setData(data);
        pieChart1.setEntryLabelColor(Color.DKGRAY);
        pieChart1.getDescription().setEnabled(false);
        pieChart1.setDrawHoleEnabled(true);
        pieChart1.setTransparentCircleRadius(58f);
        pieChart1.setHoleRadius(58f);
        pieChart1.spin( 500,0,-360f, Easing.EasingOption.EaseInOutQuad);
        pieChart1.invalidate(); // refresh
    }

    @Override
    public void finished(String result, List<TotalModel> list, List<TotalModel> list1) {
        if (result.equalsIgnoreCase("OK")){
            ArrayList<PieEntry> data1 = new ArrayList<PieEntry>();
            for (int x = 0; x < list.size(); x++){
                Log.d("TAG", list.get(x).getTotal() + " - " + list.get(x).getName());

                data1.add(new PieEntry(Integer.parseInt(list.get(x).getTotal()), list.get(x).getName()));
            }

            ArrayList<PieEntry> data2 = new ArrayList<PieEntry>();
            for (int x = 0; x < list1.size(); x++){
                data2.add(new PieEntry(Integer.parseInt(list1.get(x).getTotal()), list1.get(x).getName()));
            }

            setPieCurrent(data1);
            setPieCurrent1(data2);
        }
    }

    DatePickerDialog.OnDateSetListener dateListener1 = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int r_year, int monthOfYear,
                              int dayOfMonth) {
            year = r_year;
            month = monthOfYear + 1;
            day = dayOfMonth;

            string_year = r_year + "";

            if (month < 10) {

                string_month = "0" + month;
            } else {
                string_month = month + "";
            }
            if (day < 10) {

                string_day = "0" + day;
            } else {
                string_day = day + "";
            }
            startDate1 = year + "-" + string_month + "-" + string_day;

            tvStartPeriod1.setText(year + "-" + string_month + "-" + string_day);
        }
    };

    DatePickerDialog.OnDateSetListener dateListener2 = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int r_year, int monthOfYear,
                              int dayOfMonth) {
            year = r_year;
            month = monthOfYear + 1;
            day = dayOfMonth;

            string_year = r_year + "";

            if (month < 10) {

                string_month = "0" + month;
            } else {
                string_month = month + "";
            }
            if (day < 10) {

                string_day = "0" + day;
            } else {
                string_day = day + "";
            }
            endDate1 = year + "-" + string_month + "-" + string_day;

            tvEndPeriod1.setText(year + "-" + string_month + "-" + string_day);
        }
    };

    DatePickerDialog.OnDateSetListener dateListener3 = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int r_year, int monthOfYear,
                              int dayOfMonth) {
            year = r_year;
            month = monthOfYear + 1;
            day = dayOfMonth;

            string_year = r_year + "";

            if (month < 10) {

                string_month = "0" + month;
            } else {
                string_month = month + "";
            }
            if (day < 10) {

                string_day = "0" + day;
            } else {
                string_day = day + "";
            }
            startDate2 = year + "-" + string_month + "-" + string_day;

            tvStartPeriod2.setText(year + "-" + string_month + "-" + string_day);
        }
    };

    DatePickerDialog.OnDateSetListener dateListener4 = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int r_year, int monthOfYear,
                              int dayOfMonth) {
            year = r_year;
            month = monthOfYear + 1;
            day = dayOfMonth;

            string_year = r_year + "";

            if (month < 10) {

                string_month = "0" + month;
            } else {
                string_month = month + "";
            }
            if (day < 10) {

                string_day = "0" + day;
            } else {
                string_day = day + "";
            }
            endDate2 = year + "-" + string_month + "-" + string_day;

            tvEndPeriod2.setText(year + "-" + string_month + "-" + string_day);
        }
    };
}
