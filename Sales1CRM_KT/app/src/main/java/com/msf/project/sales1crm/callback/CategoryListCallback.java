package com.msf.project.sales1crm.callback;

import com.msf.project.sales1crm.model.CategoryModel;

import java.util.List;

public interface CategoryListCallback {
    void finishCategoryList(String result, List<CategoryModel> list);
}
