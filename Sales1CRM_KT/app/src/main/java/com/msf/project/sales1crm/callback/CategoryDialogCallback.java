package com.msf.project.sales1crm.callback;

import android.os.Bundle;

public interface CategoryDialogCallback {
    void onAccountDialogCallback(Bundle data);
}
