package com.msf.project.sales1crm;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Spinner;

import com.msf.project.sales1crm.callback.AccountDialogCallback;
import com.msf.project.sales1crm.callback.TaskAddCallback;
import com.msf.project.sales1crm.callback.UserDialogCallback;
import com.msf.project.sales1crm.customview.CustomDialog;
import com.msf.project.sales1crm.customview.CustomEditText;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.customview.DatePickerFragment;
import com.msf.project.sales1crm.customview.SpinnerCustomAdapter;
import com.msf.project.sales1crm.fragment.AccountDialogFragment;
import com.msf.project.sales1crm.fragment.UserDialogFragment;
import com.msf.project.sales1crm.model.ActivityTodoModel;
import com.msf.project.sales1crm.presenter.ATodoAddPresenter;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.PreferenceUtility;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ATodoAddActivity extends BaseActivity implements TaskAddCallback {


    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.tvTitle)
    CustomTextView tvTitle;
    @BindView(R.id.ivSave)
    ImageView ivSave;
    @BindView(R.id.tvAssignTo)
    CustomTextView tvAssignTo;
    @BindView(R.id.tvAccountName)
    CustomTextView tvAccountName;
    @BindView(R.id.ivAccountName)
    ImageView ivAccountName;
    @BindView(R.id.etSubject)
    CustomEditText etSubject;
//    @BindView(R.id.tilSubject)
//    TextInputLayout tilSubject;
    @BindView(R.id.tvCloseDate)
    CustomTextView tvCloseDate;
    @BindView(R.id.etDesc)
    CustomEditText etDesc;
//    @BindView(R.id.tilDesc)
//    TextInputLayout tilDesc;
    @BindView(R.id.spPriority)
    Spinner spPriority;
    @BindView(R.id.spTaskStatus)
    Spinner spTaskStatus;
    private Context context;
    private DatePickerFragment datepicker;
    private ATodoAddPresenter presenter;
    private String url = "", contact_json, account_id = "0", assign_id = "0";
    private String open_date = "", string_year, string_month, string_day, index_priority = "0", index_status = "0";
    private int year, month, day;
    private boolean from_detail_todo = false;
    private String[] priority = {"Priority", "Urgen", "High", "Medium", "Low"}, task_status = {"Task Status", "Not Started", "Pending", "In Progress", "Completed", "Waiting for Someone else"};
    private ActivityTodoModel model;

    static ATodoAddActivity aTodoAddActivity;

    public static ATodoAddActivity getInstance() {
        return aTodoAddActivity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_atodo_add);

        ButterKnife.bind(this);
        this.context = this;

        aTodoAddActivity = this;

        this.presenter = new ATodoAddPresenter(context, this);

        try {
            from_detail_todo = getIntent().getExtras().getBoolean("from_detail_todo");
        } catch (Exception e) {
        }

        try {
            model = getIntent().getExtras().getParcelable("aTodoModels");
        } catch (Exception e) {
        }


        datepicker = new DatePickerFragment();
        datepicker.setListener(dateListener);

        initDate();
        initView();
    }

    private void initDate() {
        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH) + 1;
        day = c.get(Calendar.DAY_OF_MONTH);
    }

    private void initView() {
        try {
            tvCloseDate.setText(getIntent().getExtras().getString("date"));
        } catch (Exception e) {
            tvCloseDate.setText("Date");
        }

        tvAssignTo.setText(PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.KEY_FULLNAME));
        assign_id = PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.KEY_USERID);

        getSpinnerAdapter();

        if (from_detail_todo) {
            tvTitle.setText("Edit Ext Todo");
            assign_id = model.getAssignTo();
            account_id = model.getAccountId();
            spPriority.setSelection(Integer.parseInt(model.getPriority()));
            spTaskStatus.setSelection(Integer.parseInt(model.getTaskStatus()));
            tvAssignTo.setText(model.getUser_name());
            tvAccountName.setText(model.getAccount_name());
            tvCloseDate.setText(model.getDueDate());
            etSubject.setText(model.getSubject());
            etDesc.setText(model.getDescription());
        }

        ivBack.setOnClickListener(click);
        ivSave.setOnClickListener(click);
        tvAccountName.setOnClickListener(click);
        ivAccountName.setOnClickListener(click);
        tvCloseDate.setOnClickListener(click);
        tvAssignTo.setOnClickListener(click);
    }

    private void getSpinnerAdapter() {
        SpinnerCustomAdapter spinnerCustomAdapter = new SpinnerCustomAdapter(context,
                R.layout.spinner_default, priority);
        spinnerCustomAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spPriority.setAdapter(spinnerCustomAdapter);
        spPriority.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                if (position != 0) {
                    index_priority = String.valueOf(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        SpinnerCustomAdapter spinnerAdapter = new SpinnerCustomAdapter(context,
                R.layout.spinner_default, task_status);
        spinnerAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spTaskStatus.setAdapter(spinnerAdapter);
        spTaskStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                if (position != 0) {
                    index_status = String.valueOf(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.ivBack:
                    final CustomDialog dialogback = CustomDialog.setupDialogConfirmation(context, "Anda ingin kembali? Data yang anda masukkan akan hilang?");
                    CustomTextView tvCancel = (CustomTextView) dialogback.findViewById(R.id.tvCancel);
                    CustomTextView tvYa = (CustomTextView) dialogback.findViewById(R.id.tvOK);

                    tvCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialogback.dismiss();
                        }
                    });

                    tvYa.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            finish();
                            dialogback.dismiss();
                        }
                    });
                    dialogback.show();
                    break;
                case R.id.ivSave:
                    if (checkField()) {
                        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(context)) {
                            if (from_detail_todo) {
                                model.setAccountId(account_id);
                                model.setAssignTo(assign_id);
                                model.setSubject(etSubject.getText().toString());
                                model.setDueDate(tvCloseDate.getText().toString());
                                model.setDescription(etDesc.getText().toString());
                                model.setCreatedAt(getCurrentDate());
                                model.setPriority(index_priority);
                                model.setTaskStatus(index_status);

                                showLoadingDialog();

                                presenter.setupEdit(ApiParam.API_072, model);
                            } else {
                                ActivityTodoModel atodoModel = new ActivityTodoModel();
                                atodoModel.setAccountId(account_id);
                                atodoModel.setAssignTo(assign_id);
                                atodoModel.setSubject(etSubject.getText().toString());
                                atodoModel.setDueDate(tvCloseDate.getText().toString());
                                atodoModel.setDescription(etDesc.getText().toString());
                                atodoModel.setCreatedAt(getCurrentDate());
                                atodoModel.setPriority(index_priority);
                                atodoModel.setTaskStatus(index_status);

                                showLoadingDialog();

                                presenter.setupAdd(ApiParam.API_071, atodoModel);
                            }

                        } else {

                        }
                    }
                    break;
                case R.id.tvAccountName:
                    AccountDialogFragment accountDialogFragment = new AccountDialogFragment(ATodoAddActivity.this, new AccountDialogCallback() {

                        @Override
                        public void onAccountDialogCallback(Bundle data) {
                            tvAccountName.setText(data.getString("name"));
                            contact_json = data.getString("pic");
                            account_id = data.getString("id");
                        }
                    });
                    accountDialogFragment.show(getSupportFragmentManager(), "accountDialog");
                    break;
                case R.id.ivAccountName:
                    AccountDialogFragment DialogFragment = new AccountDialogFragment(ATodoAddActivity.this, new AccountDialogCallback() {

                        @Override
                        public void onAccountDialogCallback(Bundle data) {
                            tvAccountName.setText(data.getString("name"));
                            contact_json = data.getString("pic");
                            account_id = data.getString("id");

                            Log.d("TAG", "Contact JSON : " + contact_json);
                        }
                    });
                    DialogFragment.show(getSupportFragmentManager(), "accountDialog");
                    break;
                case R.id.tvCloseDate:
                    datepicker.setDateFragment(year, month, day);
                    datepicker.show(getFragmentManager(), "datePicker");
                    break;
                case R.id.tvAssignTo:
                    UserDialogFragment userFragment = new UserDialogFragment(ATodoAddActivity.this, new UserDialogCallback() {

                        @Override
                        public void onUserDialogCallback(Bundle data) {
                            tvAssignTo.setText(data.getString("name"));
                            assign_id = data.getString("id");
                        }
                    });
                    userFragment.show(getSupportFragmentManager(), "UserFragment");
                    break;
            }
        }
    };

    private boolean checkField() {
        if (account_id.equalsIgnoreCase("")) {
            tvAccountName.setError("Nama Akun wajib diisi");
            return false;
        } else if (etSubject.getText().toString().equalsIgnoreCase("")) {
            etSubject.setError("Subject Harus Diisi !!!");
            return false;
        } else if (tvCloseDate.getText().toString().equalsIgnoreCase("")) {
            tvCloseDate.setError("the Closing Date cannot be Empty !!!");
            return false;
        }
        return true;
    }

    DatePickerDialog.OnDateSetListener dateListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int r_year, int monthOfYear,
                              int dayOfMonth) {
            year = r_year;
            month = monthOfYear + 1;
            day = dayOfMonth;

            string_year = r_year + "";

            if (month < 10) {

                string_month = "0" + month;
            } else {
                string_month = month + "";
            }
            if (day < 10) {

                string_day = "0" + day;
            } else {
                string_day = day + "";
            }
            open_date = year + "-" + string_month + "-" + string_day;

            tvCloseDate.setText(year + "-" + string_month + "-" + string_day);
        }
    };

    @Override
    public void finishAdd(String result, String response) {
        dismissLoadingDialog();
        if (result.equalsIgnoreCase("OK")) {
            finish();
        }
    }

    @Override
    public void finishEdit(String result, String response) {
        dismissLoadingDialog();
        if (result.equalsIgnoreCase("OK")) {
            finish();
            ATodoAddActivity.getInstance().finish();
        }
    }

    private String getCurrentDate() {
        Calendar c = Calendar.getInstance();

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        return df.format(c.getTime());
    }
}
