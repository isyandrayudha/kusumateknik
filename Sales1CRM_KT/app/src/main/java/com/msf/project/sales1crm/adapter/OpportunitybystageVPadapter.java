package com.msf.project.sales1crm.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.msf.project.sales1crm.OpportunityDetailActivity;
import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.model.OpportunityModel;

import java.util.List;

import butterknife.BindView;

public class OpportunitybystageVPadapter extends RecyclerView.Adapter<OpportunitybystageVPadapter.OpportunityStageVPHolder> {

    private Context context;
    private Bitmap icon;
    private List<OpportunityModel> listData;
    private String url, iterated;
    private int size;

    public OpportunitybystageVPadapter(Context context, List<OpportunityModel> object) {
        this.context = context;
        this.listData = object;
    }

    public static abstract class Row {
    }

    // This is for making Section, ex: A B C D etc
    public static final class Section extends Row {
        public final String text;

        public Section(String text) {
            this.text = text;
        }
    }

    // This is for making Row, ex: A B C D etc
    public static final class Item extends Row {
        public final OpportunityModel text;

        public Item(OpportunityModel text) {
            this.text = text;
        }

        public OpportunityModel getItem() {
            return text;
        }
    }

    private List<Row> rows;

    public void setRows(List<Row> rows) {
        this.rows = rows;
    }

    @Override
    public long getItemId(int position) {
        return (position);
    }

    @Override
    public int getItemCount() {
        return rows.size();
    }


    @NonNull
    @Override
    public OpportunityStageVPHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_stage_vp, null);
        return new OpportunityStageVPHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OpportunityStageVPHolder holder, int position) {
        final Item item = (Item) rows.get(position);
        Log.i("TAG",
                "number: "
                        + coolFormat(Double.parseDouble(item.getItem()
                        .getSize()), 0));
        holder.tvOpportunityName.setText(item.text.getName());
        holder.tvAccountName.setText(item.text.getAccount_name());
        holder.tvStage.setText(item.text.getStage_name());
        holder.tvCloseDate.setText(item.text.getClose_date());
        holder.tvProgress.setText(item.text.getProgress() + "%");
        holder.tvSize.setText(size + iterated);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    Intent intent = new Intent(context, OpportunityDetailActivity.class);
                    intent.putExtra("opportunityModel", item.getItem());
                    context.startActivity(intent);

            }
        });
    }

    private char[] c = new char[]{'K', 'M', 'B', 'T'};

    private String coolFormat(double n, int iteration) {
        double d = ((long) n / 100) / 10.0;
        boolean isRound = (d * 10) % 10 == 0;// true if the decimal part is
        // equal to 0 (then it's trimmed
        // anyway)
        Log.i("TAG", "num : " + d);
        size = (int) d;
        iterated = String.valueOf(c[iteration]);
        Log.i("TAG", "iteration : " + c[iteration]);
        return (d < 1000 ? // this determines the class, i.e. 'k', 'm' etc
                ((d > 99.9 || isRound || (!isRound && d > 9.99) ? // this decides
                        // whether to trim
                        // the decimals
                        (int) d * 10 / 10
                        : d + "" // (int) d * 10 / 10 drops the decimal
                ) + "" + c[iteration])
                : coolFormat(d, iteration + 1));
    }


    public class OpportunityStageVPHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvOpportunityName)
        CustomTextView tvOpportunityName;
        @BindView(R.id.tvAccountName)
        CustomTextView tvAccountName;
        @BindView(R.id.tvProgress)
        CustomTextView tvProgress;
        @BindView(R.id.tvSize)
        CustomTextView tvSize;
        @BindView(R.id.tvStage)
        CustomTextView tvStage;
        @BindView(R.id.tvCloseDate)
        CustomTextView tvCloseDate;
        public OpportunityStageVPHolder(View view) {
            super(view);
            tvOpportunityName = (CustomTextView) view.findViewById(R.id.tvOpportunityName);
            tvAccountName = (CustomTextView) view.findViewById(R.id.tvAccountName);
            tvProgress = (CustomTextView) view.findViewById(R.id.tvProgress);
            tvSize = (CustomTextView) view.findViewById(R.id.tvSize);
            tvStage = (CustomTextView) view.findViewById(R.id.tvStage);
            tvCloseDate = (CustomTextView) view.findViewById(R.id.tvCloseDate);
        }
    }
}
