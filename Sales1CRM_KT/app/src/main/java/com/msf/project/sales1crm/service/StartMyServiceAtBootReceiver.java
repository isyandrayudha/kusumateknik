package com.msf.project.sales1crm.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.msf.project.sales1crm.utility.Sales1CRMUtils;

public class StartMyServiceAtBootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            if (!Sales1CRMUtils.ConnectionUtility.isMyServiceRunning(context, TrackingService.class)){
                context.startService(new Intent(context, TrackingService.class));

                //Notify the user that tracking has been enabled//
//                Toast.makeText(context, "GPS tracking enabled", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
