package com.msf.project.sales1crm.callback;

import com.msf.project.sales1crm.model.ActivityCallsModel;
import com.msf.project.sales1crm.model.ActivityEventModel;
import com.msf.project.sales1crm.model.ActivityTodoModel;

import java.util.List;

public interface ActivityListCallback {
    void finishActivityEventList(String result, List<ActivityEventModel> activityEventModel);
    void finishActivityEventMoreList(String result, List<ActivityEventModel> activityEventModel);
    void finishActivityTodoList(String result, List<ActivityTodoModel> activityTodoModel);
    void finishActivityTodoMoreList(String result, List<ActivityTodoModel> activityTodoModel);
    void finishActivityCallList(String result, List<ActivityCallsModel> activityCallsModel);
    void finishActivityCallMoreList(String result, List<ActivityCallsModel> activityCallsModel);
}
