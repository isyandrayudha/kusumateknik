package com.msf.project.sales1crm;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.msf.project.sales1crm.callback.MeetToAddCallback;
import com.msf.project.sales1crm.callback.MeetToListCallback;
import com.msf.project.sales1crm.customview.CustomEditText;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.customview.SpinnerCustomAdapter;
import com.msf.project.sales1crm.database.MeeToSelectDao;
import com.msf.project.sales1crm.database.MeetToDao;
import com.msf.project.sales1crm.database.PositionDao;
import com.msf.project.sales1crm.model.ContactModel;
import com.msf.project.sales1crm.model.PositionModel;
import com.msf.project.sales1crm.presenter.MeetToAddPresenter;
import com.msf.project.sales1crm.presenter.MeetToListPresenter;
import com.msf.project.sales1crm.utility.ApiParam;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MeetToCreateActivity extends Activity implements MeetToAddCallback, MeetToListCallback {

    @BindView(R.id.tvTitle)
    CustomTextView tvTitle;
    @BindView(R.id.rl)
    RelativeLayout rl;
    @BindView(R.id.spTitle)
    Spinner spTitle;
    @BindView(R.id.ivSpin)
    ImageView ivSpin;
    @BindView(R.id.etFirstName)
    CustomEditText etFirstName;
    @BindView(R.id.etLastName)
    CustomEditText etLastName;
    @BindView(R.id.spPosition)
    Spinner spPosition;
    @BindView(R.id.ivspinn)
    ImageView ivspinn;
    @BindView(R.id.etPhone)
    CustomEditText etPhone;
    @BindView(R.id.etEmail)
    CustomEditText etEmail;
    @BindView(R.id.tvCancel)
    CustomTextView tvCancel;
    @BindView(R.id.tvSave)
    CustomTextView tvSave;
    @BindView(R.id.tvErrorTitle)
    CustomTextView tvErrorTitle;
    @BindView(R.id.tvErrorPosition)
    CustomTextView tvErrorPosition;

    private View view;
    private Context context;
    private SpinnerCustomAdapter spTitleAdapter, spPositionAdapter;
    private int indexTitle = 0, indexPosition = 0;
    private String[] title = {"Select Title", "Mr.", "Ms.", "Mrs.", "Dr.",
            "Prof."};
    private PositionModel positionModel;
    private MeetToAddPresenter presenter;
    private ContactModel model;
    private String account_id;
    private List<PositionModel> positionList = new ArrayList<>();
    private ArrayList<String> arrPosition = new ArrayList<>();
    private boolean from_detail = false;
    private MeetToListPresenter meetToListPresenter;
    static MeetToCreateActivity meetToCreateActivity;

    public static MeetToCreateActivity getInstance() {
        return meetToCreateActivity;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        this.context = this;
        this.presenter = new MeetToAddPresenter(context, this);
        this.meetToListPresenter = new MeetToListPresenter(context, (MeetToListCallback) this);
        setContentView(R.layout.activity_meet_to_create);
        ButterKnife.bind(this);
        getDataSpinner();
        tvCancel.setOnClickListener(click);
        tvSave.setOnClickListener(click);
        account_id = getIntent().getExtras().getString("account_id");
        Log.d("TAG", "ACCOUNT_ID" + account_id);

    }



    private void getDataSpinner() {
        //Title
        spTitleAdapter = new SpinnerCustomAdapter(context,
                R.layout.spinner_default, title);
        spTitleAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spTitle.setAdapter(spTitleAdapter);
        spTitle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                if (position != 0) {
                    indexTitle = position;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        positionList = new PositionDao(context).getPositionList();

//        if (from_detail && !model.getPosition_id().equalsIgnoreCase("0")) {
//            for (int i = 0; i < positionList.size(); i++) {
//                if (i == 0) {
//                    arrPosition.add("Select Position");
//                }
//
//                if (model.getPosition_id().equalsIgnoreCase(positionList.get(i).getId()))
//                    indexPosition = i + 1;
//                arrPosition.add(positionList.get(i).getName());
//            }
//        } else {
        for (int i = 0; i < positionList.size(); i++) {
            if (i == 0) {
                arrPosition.add("Select Position");
            }
            arrPosition.add(positionList.get(i).getName());
        }
//        }
        String[] position = new String[arrPosition.size()];
        position = arrPosition.toArray(position);
        spPositionAdapter = new SpinnerCustomAdapter(context,
                R.layout.spinner_default, position);
        spPositionAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spPosition.setAdapter(spPositionAdapter);
        spPosition.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                if (position != 0) {
                    indexPosition = position;
                    positionModel = positionList.get(position - 1);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.tvCancel:
                    finish();
                    break;
                case R.id.tvSave:
                    if (validation()) {
                        model = new ContactModel();
                        model.setTitle(indexTitle + "");
                        model.setAccount_id(account_id);
                        if (etFirstName.getText().toString().equalsIgnoreCase("")) {
                            etFirstName.setError("First Name is not Empty!!!");
                            model.setFirst_name("");
                        } else {
                            model.setFirst_name(etFirstName.getText().toString());
                        }
                        model.setLast_name(etLastName.getText().toString());
                        if (indexPosition != 0) {
                            model.setPosition(positionModel.getId());
                            tvErrorPosition.setVisibility(View.GONE);
                        } else if (indexPosition == 0) {
                            tvErrorPosition.setText("Position is not Empty");
                            tvErrorPosition.setVisibility(View.VISIBLE);
                            model.setPosition("0");
                        }
                        if (etPhone.getText().toString().equalsIgnoreCase("")) {
                            etPhone.setError("Phone is not Empty!!!");
                            model.setPhone("");
                        } else {
                            model.setPhone(etPhone.getText().toString());
                        }
                        model.setEmail(etEmail.getText().toString());
                        presenter.setupMeetTo(ApiParam.API_116, model);
                        setResult(Activity.RESULT_OK);
                    }
                    break;
            }
        }
    };

    private boolean validation() {
        if (indexTitle == 0) {
            tvErrorTitle.setText("Title must select");
            tvErrorTitle.setVisibility(View.VISIBLE);
            return false;
        } else if (indexTitle != 0) {
            tvErrorTitle.setVisibility(View.GONE);
            return true;
        } else if (etFirstName.getText().toString().equalsIgnoreCase("")) {
            etFirstName.setError("First Name is not Empty!!!");
            return false;
        } else if (etPhone.getText().toString().equalsIgnoreCase("")) {
            etPhone.setError("Phone is not Empty!!!");
            return false;
        }
        return true;
    }

    @Override
    public void finishAdd(String result, String response) {
        if (result.equalsIgnoreCase("OK")) {
            Toast.makeText(context, "Success Create New Meet to!!!", Toast.LENGTH_SHORT).show();

            MeetToDao dao = new MeetToDao(context);
            dao.deleteAllRecord();


            MeeToSelectDao dau = new MeeToSelectDao(context);
            List<ContactModel> check = dau.getFullName();
            String item = "";
            for (int i = 0; i < check.size(); i++) {
                item = item + check.get(i).getId();
                if (i != check.size() - 1) {
                    item = item + ", ";
                }
            }
            Log.d("TAG", "item meet to : " + item);
            meetToListPresenter.setupList(ApiParam.API_131, account_id, item);


        } else {
            etEmail.setError("Email is not valid");
        }
    }

    @Override
    public void finishEdit(String result, String response) {

    }

    @Override
    public void finishListMeetTo(String result, List<ContactModel> list) {
        if (result.equalsIgnoreCase("OK")) {
            finish();
        }
    }
}
