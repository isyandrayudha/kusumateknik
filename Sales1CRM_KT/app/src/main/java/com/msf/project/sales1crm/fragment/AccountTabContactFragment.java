package com.msf.project.sales1crm.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CallLog;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.telephony.PhoneNumberUtils;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.msf.project.sales1crm.ContactCreateActivity;
import com.msf.project.sales1crm.ContactEditActivity;
import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.adapter.AccountContactListAdapter;
import com.msf.project.sales1crm.customview.CustomEditText;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.model.AccountModel;
import com.msf.project.sales1crm.model.ContactModel;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by christianmacbook on 16/05/18.
 */

public class AccountTabContactFragment extends Fragment {

    private static final int RESULT_OK = -1 ;
    @BindView(R.id.gvContact)
    GridView gvContact;

    @BindView(R.id.etSearch)
    CustomEditText etSearch;

    @BindView(R.id.ivClear)
    ImageView ivClear;

    @BindView(R.id.llNotFound)
    LinearLayout llNotFound;
    @BindView(R.id.tvRefresh)
    CustomTextView tvRefresh;
    @BindView(R.id.rlNoConnection)
    RelativeLayout rlNoConnection;
    @BindView(R.id.faContact)
    FloatingActionButton faContact;
    @BindView(R.id.menu)
    FloatingActionMenu menu;
    @BindView(R.id.rlContactList)
    RelativeLayout rlContactList;
    Unbinder unbinder;

    private View view;
    private Context context;
    private AccountContactListAdapter adapter;
    private List<ContactModel> list = new ArrayList<>();
    private List<ContactModel> searchList = new ArrayList<>();
    private AccountModel model;

    private final static String TAG = AccountTabContactFragment.class.getSimpleName();

    private LinearLayout llCall, llSMS, llWhatsapp, llEmail;
    private CustomTextView Edit;

    public static AccountTabContactFragment newInstance() {
        AccountTabContactFragment fragment = new AccountTabContactFragment();
        return fragment;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case Sales1CRMUtils.REQUEST_PHONE_CALL: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    callNumber(model.getPhone());
                }
                return;
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Sales1CRMUtils.LIST_MEET_TO:
                if (resultCode == RESULT_OK ) {
                    setList();
                    adapter.notifyDataSetChanged();
                    Log.d("TAG", "===OK===");
                }else {
                    Log.d("TAG", "===DAMN===");
                }
                break;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_accountcontact, container, false);

        unbinder = ButterKnife.bind(this, this.view);
        return this.view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);
        this.context = getActivity();

        initView();
    }


    private void initView() {
        try {
            model = getArguments().getParcelable("accountUmum");
        } catch (Exception e) {
        }

        ivClear.setOnClickListener(click);
        faContact.setOnClickListener(click);

        setList();
        setTextWatcherForSearch();
    }

    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.ivClear:
                    etSearch.setText("");
                    etSearch.clearFocus();
                    InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);

                    llNotFound.setVisibility(View.GONE);
                    setList();
                    break;
                case R.id.faContact:
                    Intent intent = new Intent(context, ContactCreateActivity.class);
                    intent.putExtra("account_id",model.getId());
                    intent.putExtra("tabContact", false);
                    Log.d("TAG","account_id " + model.getId());
                    startActivityForResult(intent, Sales1CRMUtils.LIST_MEET_TO);
                    break;
            }
        }
    };

    private void setList() {
        list.clear();
        try {
            JSONArray array = new JSONArray(model.getRelated_people());

            for (int i = 0; i < array.length(); i++) {
                ContactModel contactModel = new ContactModel();
                contactModel.setId(array.getJSONObject(i).getString("id"));
                contactModel.setFullname(array.getJSONObject(i).getString("first_name") +
                        " " + array.getJSONObject(i).getString("last_name"));
                contactModel.setPosition(array.getJSONObject(i).getString("position"));
                contactModel.setPhone(array.getJSONObject(i).getString("phone"));
                contactModel.setEmail(array.getJSONObject(i).getString("email"));
                contactModel.setTitle(array.getJSONObject(i).getString("title"));
                contactModel.setFirst_name(array.getJSONObject(i).getString("first_name"));
                contactModel.setLast_name(array.getJSONObject(i).getString("last_name"));
                contactModel.setPositionId(array.getJSONObject(i).getString("position_id"));
                list.add(contactModel);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        adapter = new AccountContactListAdapter(context, list);

        List<AccountContactListAdapter.Row> rows = new ArrayList<AccountContactListAdapter.Row>();
        for (ContactModel contactModel : list) {
            //Read List by Row to insert into List Adapter
            rows.add(new AccountContactListAdapter.Item(contactModel));
        }

        //Set Row Adapter
        adapter.setRows(rows);
        gvContact.setAdapter(adapter);
        gvContact.setOnItemClickListener(new AdapterOnItemClick());
    }

    private void setTextWatcherForSearch() {
        etSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                Log.i(TAG, "onTextChanged");

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                Log.i(TAG, "beforeTextChanged");
            }

            @Override
            public void afterTextChanged(Editable s) {
                Log.i(TAG, "afterTextChanged");
                String search = etSearch.getText().toString();
                searchList.clear();

                if (!search.equals("")) {
                    if (list.size() > 0) {
                        for (int i = 0; i < list.size(); i++) {
                            if (list.get(i).getFullname().toLowerCase().contains(search.toLowerCase())) {
                                searchList.add(list.get(i));
                            }
                        }

                        setListSearch();
                        if (searchList.size() > 0) {
                            llNotFound.setVisibility(View.GONE);
                        } else {
                            llNotFound.setVisibility(View.VISIBLE);
                        }
                    }

                    for (int i = 0; i < searchList.size(); i++) {
                        Log.i(TAG, "search data : "
                                + searchList.get(i).getFullname());
                    }
                } else {
                    llNotFound.setVisibility(View.GONE);
                    adapter.notifyDataSetChanged();
                    setList();
                }
            }
        });
    }

    private void setListSearch() {
        adapter = new AccountContactListAdapter(context, searchList);

        List<AccountContactListAdapter.Row> rows = new ArrayList<AccountContactListAdapter.Row>();
        for (ContactModel contactModel : searchList) {
            //Read List by Row to insert into List Adapter
            rows.add(new AccountContactListAdapter.Item(contactModel));
        }

        //Set Row Adapter
        adapter.setRows(rows);
        gvContact.setAdapter(adapter);
        gvContact.setOnItemClickListener(new AdapterOnItemClick());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    private class AdapterOnItemClick implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            // TODO Auto-generated method stub
            final AccountContactListAdapter.Item item = (AccountContactListAdapter.Item) gvContact.getAdapter().getItem(position);
            switch (view.getId()) {
                default:
                    final Dialog dialog = new Dialog(context);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.dialog_contact);

                    final View vEmail = (View) dialog
                            .findViewById(R.id.vEmail);
                    llEmail = (LinearLayout) dialog
                            .findViewById(R.id.llEmail);
                    llCall = (LinearLayout) dialog
                            .findViewById(R.id.llCall);
                    llSMS = (LinearLayout) dialog
                            .findViewById(R.id.llSMS);
                    llWhatsapp = (LinearLayout) dialog
                            .findViewById(R.id.llWhatsapp);

                    Edit = (CustomTextView) dialog
                            .findViewById(R.id.Edit);

                    llEmail.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (!item.text.getEmail().equalsIgnoreCase("")) {
                                String mailto = "mailto:" + item.text.getEmail();

                                Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
                                emailIntent.setData(Uri.parse(mailto));

                                try {
                                    startActivity(Intent.createChooser(emailIntent, "Pilih Email"));
                                } catch (ActivityNotFoundException e) {
                                    //TODO: Handle case where no email app is available
                                }
                            } else {
                                llEmail.setVisibility(View.GONE);
                                vEmail.setVisibility(View.GONE);
                            }
                            dialog.dismiss();
                        }
                    });

                    llCall.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (ContextCompat.checkSelfPermission(context,
                                    Manifest.permission.CALL_PHONE)
                                    != PackageManager.PERMISSION_GRANTED) {

                                ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.CALL_PHONE}, Sales1CRMUtils.REQUEST_PHONE_CALL);
                            } else {
                                callNumber(item.text.getPhone());
                            }
                            dialog.dismiss();
                        }
                    });

                    llSMS.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            sendSMS(item.text.getPhone());
                            dialog.dismiss();
                        }
                    });

                    llWhatsapp.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            openWhatsApp(item.text.getPhone());
                        }
                    });
                    dialog.show();

                    Edit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(context, ContactEditActivity.class);
                            intent.putExtra("id", model.getId());
                            intent.putExtra("account_id",model.getId());
                            intent.putExtra("title", item.text.getTitle());
                            intent.putExtra("firtsName", item.text.getFirst_name());
                            intent.putExtra("LastName", item.text.getLast_name());
                            intent.putExtra("position",item.text.getPositionId());
                            intent.putExtra("telp", item.text.getPhone());
                            intent.putExtra("email", item.text.getEmail());
                            Log.d("TAG","name " + item.text.getPhone());
                            Log.d("TAG","title " + item.text.getTitle());
                            Log.d("TAG", "position " + item.text.getPositionId());
                            Log.d("TAG", "lastName " + item.text.getLast_name());
                            startActivity(intent);
                        }
                    });
                    dialog.show();
                    break;
            }
        }
    }

    private void openWhatsApp(String number) {
        final String appPackageName = "com.whatsapp";

        if (Sales1CRMUtils.Utils.appInstalledOrNot(context, appPackageName)) {
            number = PhoneNumberUtils.formatNumber(number).replace("-", "");
            if (number.substring(0, 2).equalsIgnoreCase("08")) {
                number = "62" + number.substring(1);
            } else {
                number = number.replace(" ", "").replace("+", "");
            }

            Log.d("TAG", "Number : " + number);
            try {
                Intent sendIntent = new Intent("android.intent.action.MAIN");
                sendIntent.setComponent(new ComponentName("com.whatsapp", "com.whatsapp.Conversation"));
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.setType("text/plain");
                sendIntent.putExtra(Intent.EXTRA_TEXT, "");
                sendIntent.putExtra("jid", number + "@s.whatsapp.net");
                sendIntent.setPackage("com.whatsapp");
                startActivity(sendIntent);
            } catch (Exception e) {
                Toast.makeText(context, "Error/n" + e.toString(), Toast.LENGTH_SHORT).show();
            }
        } else {
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
            } catch (ActivityNotFoundException anfe) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
            }
        }
    }

    private void callNumber(String number) {
        PackageManager packageManager = context.getPackageManager();
        if (packageManager.hasSystemFeature(PackageManager.FEATURE_TELEPHONY)) {
            // call phone
            Intent intent = new Intent(Intent.ACTION_CALL);

            intent.setData(Uri.parse("tel:" + number));

            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            startActivity(intent);
        }
    }

    private void sendSMS(String number) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("smsto:" + Uri.encode(number)));
        startActivity(intent);
    }

    public String lastCall() {
        String callDura = "0";
        StringBuffer sb = new StringBuffer();
        Uri contacts = CallLog.Calls.CONTENT_URI;
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
        }

        Cursor managedCursor = context.getContentResolver().query(
                contacts, null, null, null, CallLog.Calls.DATE + " DESC limit 1;");
        int number = managedCursor.getColumnIndex(CallLog.Calls.NUMBER);
        int duration1 = managedCursor.getColumnIndex(CallLog.Calls.DURATION);
        int date = managedCursor.getColumnIndex(CallLog.Calls.DATE);
        if (managedCursor.moveToFirst() == true) {
            String phNumber = managedCursor.getString(number);
            callDura = managedCursor.getString(duration1);
            String callDate = managedCursor.getString(date);
            String callDayTime = new Date(Long.valueOf(callDate)).toString();
            String dir = null;
            Log.e("DUR", "\nPhone Number:--- " + phNumber + " \nCall duration in sec :--- " + callDura + " \nCall Date in sec :--- " + callDayTime);
        }
        managedCursor.close();

        return callDura;
    }
}
