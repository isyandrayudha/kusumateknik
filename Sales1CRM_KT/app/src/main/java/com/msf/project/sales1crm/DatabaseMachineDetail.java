package com.msf.project.sales1crm;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.msf.project.sales1crm.callback.DashboardCallback;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.model.AbsenceModel;
import com.msf.project.sales1crm.model.DashboardFirstModel;
import com.msf.project.sales1crm.model.DashboardSecondModel;
import com.msf.project.sales1crm.model.DashboardThirdModel;
import com.msf.project.sales1crm.model.DatabaseMachineModel;
import com.msf.project.sales1crm.model.TotalOpportunityModel;
import com.msf.project.sales1crm.presenter.DashboardPresenter;
import com.msf.project.sales1crm.utility.ApiParam;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DatabaseMachineDetail extends BaseActivity implements DashboardCallback {

    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.tvAccountNameTitle)
    CustomTextView tvAccountNameTitle;
    @BindView(R.id.ivEdit)
    ImageView ivEdit;
    @BindView(R.id.tvCreateDate)
    CustomTextView tvCreateDate;
    @BindView(R.id.tvAccountName)
    CustomTextView tvAccountName;
    @BindView(R.id.tvBrand)
    CustomTextView tvBrand;
    @BindView(R.id.tvApplication)
    CustomTextView tvApplication;
    @BindView(R.id.tvUnitNoInCustomer)
    CustomTextView tvUnitNoInCustomer;
    @BindView(R.id.tvType)
    CustomTextView tvType;
    @BindView(R.id.tvModel)
    CustomTextView tvModel;
    @BindView(R.id.tvSerialNumber)
    CustomTextView tvSerialNumber;
    @BindView(R.id.tvYearOf)
    CustomTextView tvYearOf;
    @BindView(R.id.tvPressureVacum)
    CustomTextView tvPressureVacum;
    @BindView(R.id.tvCapacity)
    CustomTextView tvCapacity;
    @BindView(R.id.tvdiff)
    CustomTextView tvdiff;
    @BindView(R.id.tvSpeed)
    CustomTextView tvSpeed;
    @BindView(R.id.tvMotorSpecs)
    CustomTextView tvMotorSpecs;
    @BindView(R.id.tvDateLast)
    CustomTextView tvDateLast;
    @BindView(R.id.tvTypeOf)
    CustomTextView tvTypeOf;
    @BindView(R.id.tvlatestInspection)
    CustomTextView tvlatestInspection;
    @BindView(R.id.tvRunning)
    CustomTextView tvRunning;
    @BindView(R.id.tvCondition)
    CustomTextView tvCondition;

    private Context context;
    private DatabaseMachineModel databaseMachineModel;
    static DatabaseMachineDetail databaseMachineDetail;
    private DashboardPresenter dashboardPresenter;

    public static DatabaseMachineDetail getInstance(){
        return databaseMachineDetail;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_database_machine_detail);
        ButterKnife.bind(this);
        this.context = this;
        this.dashboardPresenter = new DashboardPresenter(context, this);

        databaseMachineDetail = this;

        try{
            databaseMachineModel = getIntent().getExtras().getParcelable("model");
        } catch (Exception e){

        }
        initView();

    }

    private void initView(){
        dashboardPresenter.setupFirst(ApiParam.API_005);
        tvAccountNameTitle.setText(databaseMachineModel.getAccountName());
        tvCreateDate.setText(databaseMachineModel.getCreatedAt());
        tvAccountName.setText(databaseMachineModel.getAccountName());
        tvBrand.setText(databaseMachineModel.getBrandName());
        tvApplication.setText(databaseMachineModel.getApplication());
        tvUnitNoInCustomer.setText(databaseMachineModel.getUnitNoInCustomerSite());
        tvType.setText(databaseMachineModel.getType());
        tvModel.setText(databaseMachineModel.getModel());
        tvSerialNumber.setText(databaseMachineModel.getSerialNumber());
        tvYearOf.setText(databaseMachineModel.getYearOfInstall());
        tvPressureVacum.setText(databaseMachineModel.getPressureVacuum());
        tvCapacity.setText(databaseMachineModel.getCapacity());
        tvdiff.setText(databaseMachineModel.getDiffPressure());
        tvSpeed.setText(databaseMachineModel.getSpeed());
        tvMotorSpecs.setText(databaseMachineModel.getMotorSpecs());
        tvDateLast.setText(databaseMachineModel.getDateLastOverhaul());
        tvTypeOf.setText(databaseMachineModel.getTypeOfServiceDone());
        tvlatestInspection.setText(databaseMachineModel.getLatestInspectionDate());
        tvCondition.setText(databaseMachineModel.getCondition());
        tvRunning.setText(databaseMachineModel.getRunningHours());

        ivBack.setOnClickListener(click);
    }

    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.ivBack:
                    finish();
                    break;
            }
        }
    };

    @Override
    public void finishFirst(String result, DashboardFirstModel model) {
        if (result.equalsIgnoreCase("OK")) {
            if (model.getDatabase_machine_edit_permission().equalsIgnoreCase("1")) {
                ivEdit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(context, DatabaseMachineCreate.class);
                        i.putExtra("from_detail", true);
                        i.putExtra("model", databaseMachineModel);
                        startActivity(i);
                    }
                });
                ivEdit.setVisibility(View.VISIBLE);
            } else {
                ivEdit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
                    }
                });
                ivEdit.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void finishSecond(String result, DashboardSecondModel model) {

    }

    @Override
    public void finishThird(String result, List<DashboardThirdModel> listLead, List<DashboardThirdModel> listLead1, List<DashboardThirdModel> listOppor, List<DashboardThirdModel> listOppor1) {

    }

    @Override
    public void TotOpportunity(String result, TotalOpportunityModel totOporModel) {

    }

    @Override
    public void absence(String result, AbsenceModel model) {

    }
}
