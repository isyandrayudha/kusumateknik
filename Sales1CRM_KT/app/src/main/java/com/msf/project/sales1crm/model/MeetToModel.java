package com.msf.project.sales1crm.model;

public class MeetToModel{
	private String meet_to_id;
	private String name;
	private String status;
	private String notation;

	public String getMeet_to_id() {
		return meet_to_id;
	}

	public void setMeet_to_id(String meet_to_id) {
		this.meet_to_id = meet_to_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getNotation() {
		return notation;
	}

	public void setNotation(String notation) {
		this.notation = notation;
	}
}
