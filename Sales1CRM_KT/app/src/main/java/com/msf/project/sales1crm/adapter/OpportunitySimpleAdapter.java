package com.msf.project.sales1crm.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.model.OpportunityModel;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by christianmacbook on 21/05/18.
 */

public class OpportunitySimpleAdapter extends RecyclerView.Adapter<OpportunitySimpleAdapter.ViewHolder> {

    private static final String TAG = "OpportunitySimpleAdapter";

    //Vars
    private List<OpportunityModel> list = new ArrayList<>();
    private Context context;

    private Locale localeID = new Locale("in", "ID");
    private NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);

    public OpportunitySimpleAdapter(Context context, List<OpportunityModel> object) {
        this.context = context;
        this.list = object;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_simpleopportunity, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tvName.setText(list.get(position).getName());
        holder.tvSize.setText(formatRupiah.format((Double) Double.parseDouble(list.get(position).getSize())));
        holder.tvDate.setText(list.get(position).getClose_date());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CustomTextView tvName, tvSize, tvDate;

        public ViewHolder(View itemView) {
            super(itemView);

            tvName = itemView.findViewById(R.id.tvOpportunityName);
            tvSize = itemView.findViewById(R.id.tvSize);
            tvDate = itemView.findViewById(R.id.tvCloseDate);
        }
    }
}
