package com.msf.project.sales1crm.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.msf.project.sales1crm.fragment.ACallsFragment;
import com.msf.project.sales1crm.fragment.ATodoFragment;
import com.msf.project.sales1crm.fragment.AeventFragment;

public class ActivityTabAdapter extends FragmentStatePagerAdapter{
    int mNumOfTabs;
    Bundle bundle;


    public ActivityTabAdapter(FragmentManager supportFragmentManager, int NumOfTabs) {
        super(supportFragmentManager);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
       switch (position) {
           case 0 :
               AeventFragment aeventFragment = new AeventFragment();
               return aeventFragment;
           case 1 :
               ATodoFragment aTodoFragment = new ATodoFragment();
               return aTodoFragment;
           case 2 :
               ACallsFragment aCallsFragment = new ACallsFragment();
               return aCallsFragment;
           default:
               return null;
       }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
