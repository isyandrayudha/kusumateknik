package com.msf.project.sales1crm.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.msf.project.sales1crm.ErrorAPIKeyActivity;
import com.msf.project.sales1crm.MenuActivity;
import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.adapter.ACallsListAdapter;
import com.msf.project.sales1crm.callback.ActivityListCallback;
import com.msf.project.sales1crm.customview.CustomEditText;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.imageloader.ImageLoader;
import com.msf.project.sales1crm.model.ActivityCallsModel;
import com.msf.project.sales1crm.model.ActivityEventModel;
import com.msf.project.sales1crm.model.ActivityTodoModel;
import com.msf.project.sales1crm.presenter.ActivityPresenter;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ACallsFragment extends Fragment implements ActivityListCallback {
    @BindView(R.id.tvCallName)
    CustomTextView tvCallName;
    @BindView(R.id.rcCall)
    RecyclerView rcCall;
    Unbinder unbinder;
    @BindView(R.id.shimmerLayout)
    ShimmerFrameLayout shimmerLayout;
    @BindView(R.id.llshim)
    LinearLayout llshim;
    @BindView(R.id.etSearch)
    CustomEditText etSearch;
    @BindView(R.id.ivClear)
    ImageView ivClear;
    @BindView(R.id.llNotFound)
    LinearLayout llNotFound;
    @BindView(R.id.tvRefresh)
    CustomTextView tvRefresh;
    @BindView(R.id.rlNoConnection)
    RelativeLayout rlNoConnection;
    @BindView(R.id.swipeRefresh)
    SwipeRefreshLayout swipeRefresh;
    private View view;
    private Context context;
    private boolean isScrolling = false;
    private int currentItems, totalItems, scrollOutItems;
    private ACallsListAdapter activityCallsAdapter;
    private int prevSize = 0, sort_id = 0;
    private boolean isLoadMore = true;
    private boolean isMaxSize = true;
    private boolean pause = false;
    private ActivityPresenter presenter;
    private List<ActivityCallsModel> activityCallsModelList = new ArrayList<>();

    public static ACallsFragment newInstance() {
        ACallsFragment fragment = new ACallsFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_a_call, container, false);
        ButterKnife.bind(this, this.view);
        return this.view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.context = getActivity();
        this.presenter = new ActivityPresenter(context, this);
        rcCall.setNestedScrollingEnabled(false);
        initData();
    }

    private void initData() {
        Sales1CRMUtils.Utils.imageLoader = new ImageLoader(context);
        isMaxSize = true;
        isLoadMore = true;
        llNotFound.setVisibility(View.GONE);
        tvRefresh.setOnClickListener(click);

        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                llshim.setVisibility(view.VISIBLE);
                shimmerLayout.setVisibility(View.VISIBLE);
                shimmerLayout.startShimmer();
                if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(context)) {
                    presenter.setupCallList(ApiParam.API_109, etSearch.getText().toString());
                } else {
                    swipeRefresh.setVisibility(View.GONE);
                    rlNoConnection.setVisibility(View.VISIBLE);
                }
            }
        });


        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(context)) {

//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
            presenter.setupCallList(ApiParam.API_109, etSearch.getText().toString());
//                }
//            }, 3000);

        } else {
            rlNoConnection.setVisibility(View.VISIBLE);
        }

    }

    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.tvRefresh:
                    llshim.setVisibility(View.VISIBLE);
                    rlNoConnection.setVisibility(View.GONE);
                    initData();
                    break;
            }
        }
    };

    private void setCallsList() {
        llshim.setVisibility(View.VISIBLE);
        shimmerLayout.setVisibility(View.GONE);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rcCall.setLayoutManager(layoutManager);
        rcCall.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {

                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                currentItems = layoutManager.getChildCount();
                totalItems = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstVisibleItemPosition();

                if (isScrolling && (currentItems + scrollOutItems == totalItems)) {
                    isScrolling = false;
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            presenter.setupMoreListCalls(ApiParam.API_109, etSearch.getText().toString());

                        }
                    }, 3000);
                }
            }


        });

//        activityCallsAdapter = new ActivityCallsAdapter(context, this.activityCallsModelList);
        List<ACallsListAdapter.Row> rows = new ArrayList<>();
        for (ActivityCallsModel activityCallsModel : activityCallsModelList) {
            rows.add(new ACallsListAdapter.Item(activityCallsModel));
        }
        activityCallsAdapter.setRows(rows);
        rcCall.setAdapter(activityCallsAdapter);
        llshim.setVisibility(View.INVISIBLE);
        shimmerLayout.stopShimmer();
        shimmerLayout.setVisibility(View.INVISIBLE);
    }

    private void setMoreCallsList() {
        List<ACallsListAdapter.Row> rows = new ArrayList<>();
        for (ActivityCallsModel acm : activityCallsModelList) {
            rows.add(new ACallsListAdapter.Item(acm));
        }
        activityCallsAdapter.setRows(rows);
        activityCallsAdapter.notifyDataSetChanged();
    }

    @Override
    public void finishActivityEventList(String result, List<ActivityEventModel> activityEventModel) {

    }

    @Override
    public void finishActivityEventMoreList(String result, List<ActivityEventModel> activityEventModel) {

    }

    @Override
    public void finishActivityTodoList(String result, List<ActivityTodoModel> activityTodoModel) {

    }

    @Override
    public void finishActivityTodoMoreList(String result, List<ActivityTodoModel> activityTodoModel) {

    }

    @Override
    public void finishActivityCallList(String result, List<ActivityCallsModel> activityCallsModel) {
        swipeRefresh.setRefreshing(false);
        if (result.equalsIgnoreCase("OK")) {
            prevSize = activityCallsModel.size();
            this.activityCallsModelList = activityCallsModel;
            Log.e("CALL LIST", activityCallsModel.toString());

            isMaxSize = false;
            isLoadMore = false;

            activityCallsAdapter = new ACallsListAdapter(context, this.activityCallsModelList);
            setCallsList();
            if (activityCallsModel.size() > 0) {
                llNotFound.setVisibility(View.GONE);
            } else {
                llNotFound.setVisibility(View.VISIBLE);
            }
        } else if (result.equalsIgnoreCase("FAILED_KEY")) {
            startActivity(new Intent(context, ErrorAPIKeyActivity.class));
            MenuActivity.getInstance().finish();
        } else {
            rlNoConnection.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void finishActivityCallMoreList(String result, List<ActivityCallsModel> activityCallsModel) {
        if (result.equals("OK")) {
            for (int i = 0; i < activityCallsModel.size(); i++) {
                this.activityCallsModelList.add(activityCallsModel.get(i));
            }

            if (this.prevSize == this.activityCallsModelList.size())
                isMaxSize = true;
            this.prevSize = this.activityCallsModelList.size();
            setMoreCallsList();
        }
        this.isLoadMore = false;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
