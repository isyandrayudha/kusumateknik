package com.msf.project.sales1crm.callback;

import com.msf.project.sales1crm.model.LocationModel;

import java.util.List;

/**
 * Created by christianmacbook on 25/05/18.
 */

public interface LocationListCallback {
    void finishList(String result, List<LocationModel> list);
}
