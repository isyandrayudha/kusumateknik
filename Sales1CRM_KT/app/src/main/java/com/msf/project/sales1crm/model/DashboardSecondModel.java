package com.msf.project.sales1crm.model;

public class DashboardSecondModel {
    private String current_month;
    private String current_call;
    private String current_event;
    private String current_task;
    private String prev_month;
    private String prev_call;
    private String prev_event;
    private String prev_task;

    public String getCurrent_month() {
        return current_month;
    }

    public void setCurrent_month(String current_month) {
        this.current_month = current_month;
    }

    public String getCurrent_call() {
        return current_call;
    }

    public void setCurrent_call(String current_call) {
        this.current_call = current_call;
    }

    public String getCurrent_event() {
        return current_event;
    }

    public void setCurrent_event(String current_event) {
        this.current_event = current_event;
    }

    public String getCurrent_task() {
        return current_task;
    }

    public void setCurrent_task(String current_task) {
        this.current_task = current_task;
    }

    public String getPrev_month() {
        return prev_month;
    }

    public void setPrev_month(String prev_month) {
        this.prev_month = prev_month;
    }

    public String getPrev_call() {
        return prev_call;
    }

    public void setPrev_call(String prev_call) {
        this.prev_call = prev_call;
    }

    public String getPrev_event() {
        return prev_event;
    }

    public void setPrev_event(String prev_event) {
        this.prev_event = prev_event;
    }

    public String getPrev_task() {
        return prev_task;
    }

    public void setPrev_task(String prev_task) {
        this.prev_task = prev_task;
    }
}
