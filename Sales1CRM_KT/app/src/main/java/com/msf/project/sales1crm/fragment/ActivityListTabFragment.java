package com.msf.project.sales1crm.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.msf.project.sales1crm.CallsAddActivity;
import com.msf.project.sales1crm.EventAddActivity;
import com.msf.project.sales1crm.MenuActivity;
import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.TaskAddActivity;
import com.msf.project.sales1crm.adapter.ActivityTabAdapter;
import com.msf.project.sales1crm.callback.DashboardCallback;
import com.msf.project.sales1crm.model.AbsenceModel;
import com.msf.project.sales1crm.model.DashboardFirstModel;
import com.msf.project.sales1crm.model.DashboardSecondModel;
import com.msf.project.sales1crm.model.DashboardThirdModel;
import com.msf.project.sales1crm.model.TotalOpportunityModel;
import com.msf.project.sales1crm.presenter.DashboardPresenter;
import com.msf.project.sales1crm.utility.ApiParam;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ActivityListTabFragment extends Fragment implements DashboardCallback {


    @BindView(R.id.viewpager)
    ViewPager viewpager;
    Unbinder unbinder;
    @BindView(R.id.tabDots)
    TabLayout tabDots;
//    @BindView(R.id.etSearch)
//    CustomEditText etSearch;
//    @BindView(R.id.ivClear)
//    ImageView ivClear;
    @BindView(R.id.faEventAdd)
    FloatingActionButton faEventAdd;
    @BindView(R.id.faTodoAdd)
    FloatingActionButton faTodoAdd;
    @BindView(R.id.faCallAdd)
    FloatingActionButton faCallAdd;
    @BindView(R.id.menu)
    FloatingActionMenu menu;
    private View view;
    private Context context;
    private ActivityTabAdapter adapter;
    private MenuActivity menuActivity;
    private TabLayout mTablayout;
    private ViewPager mViewPager;
    private DashboardPresenter dashboardPresenter;


    public static ActivityListTabFragment newInstance() {
        ActivityListTabFragment fragment = new ActivityListTabFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onAttach(Activity activity) {
        menuActivity = (MenuActivity) activity;
        super.onAttach(activity);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_activitytab, container, false);
        ButterKnife.bind(this, view);
        setHasOptionsMenu(true);
        this.dashboardPresenter = new DashboardPresenter(context, this);
        this.context= getActivity();
        return view;
    }



    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);
        this.context = getActivity();
        this.dashboardPresenter = new DashboardPresenter(context, this);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Activity");

        initData();

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_item, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_list:
                MenuActivity.getInstance().setFragment(ActivityListFragment.newInstance());
                break;
            case R.id.action_cards:
                MenuActivity.getInstance().setFragment(ActivityListTabFragment.newInstance());
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    private void initData() {
        dashboardPresenter.setupFirst(ApiParam.API_005);
        tabDots.addTab(tabDots.newTab());
        tabDots.addTab(tabDots.newTab());
        tabDots.addTab(tabDots.newTab());
        tabDots.setTabGravity(tabDots.GRAVITY_FILL);
        adapter = new ActivityTabAdapter(menuActivity.getSupportFragmentManager(), tabDots.getTabCount());
        viewpager.setAdapter(adapter);
        viewpager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabDots));
        tabDots.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewpager.setCurrentItem(tab.getPosition(), true);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    @Override
    public void finishFirst(String result, DashboardFirstModel model) {
        if(model.getCrm_event_create_permission().equalsIgnoreCase("1")){
            faEventAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(context, EventAddActivity.class));
                }
            });
            faEventAdd.setColorNormal(R.color.BlueCRM);
            faEventAdd.setColorPressed(R.color.colorPrimaryDark);
            faEventAdd.setColorPressedResId(R.color.colorPrimaryDark);
            faEventAdd.setColorNormalResId(R.color.BlueCRM);

        } else {
            faEventAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
                }
            });
            faEventAdd.setColorNormal(R.color.BlueCRM_light);
            faEventAdd.setColorPressed(R.color.colorPrimaryDark_light);
            faEventAdd.setColorPressedResId(R.color.colorPrimaryDark_light);
            faEventAdd.setColorNormalResId(R.color.BlueCRM_light);

        }

        if(model.getCrm_task_create_permission().equalsIgnoreCase("1")){
            faTodoAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(context, TaskAddActivity.class));
                }
            });
            faTodoAdd.setColorNormal(R.color.BlueCRM);
            faTodoAdd.setColorPressed(R.color.colorPrimaryDark);
            faTodoAdd.setColorPressedResId(R.color.colorPrimaryDark);
            faTodoAdd.setColorNormalResId(R.color.BlueCRM);
        } else {
            faTodoAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
                }
            });
            faTodoAdd.setColorNormal(R.color.BlueCRM_light);
            faTodoAdd.setColorPressed(R.color.colorPrimaryDark_light);
            faTodoAdd.setColorPressedResId(R.color.colorPrimaryDark_light);
            faTodoAdd.setColorNormalResId(R.color.BlueCRM_light);
        }

        if(model.getCrm_call_create_permission().equalsIgnoreCase("1")){
            faCallAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(context, CallsAddActivity.class));
                }
            });
            faCallAdd.setColorNormal(R.color.BlueCRM);
            faCallAdd.setColorPressed(R.color.colorPrimaryDark);
            faCallAdd.setColorPressedResId(R.color.colorPrimaryDark);
            faCallAdd.setColorNormalResId(R.color.BlueCRM);
        } else {
            faCallAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
                }
            });
            faCallAdd.setColorNormal(R.color.BlueCRM_light);
            faCallAdd.setColorPressed(R.color.colorPrimaryDark_light);
            faCallAdd.setColorPressedResId(R.color.colorPrimaryDark_light);
            faCallAdd.setColorNormalResId(R.color.BlueCRM_light);
        }
    }

    @Override
    public void finishSecond(String result, DashboardSecondModel model) {

    }

    @Override
    public void finishThird(String result, List<DashboardThirdModel> listLead, List<DashboardThirdModel> listLead1, List<DashboardThirdModel> listOppor, List<DashboardThirdModel> listOppor1) {

    }

    @Override
    public void TotOpportunity(String result, TotalOpportunityModel totOporModel) {

    }

    @Override
    public void absence(String result, AbsenceModel model) {

    }
}
