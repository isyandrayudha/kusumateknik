package com.msf.project.sales1crm.customview;

import android.app.Dialog;
import android.content.Context;
import android.support.v4.view.ViewPager;

import com.msf.project.sales1crm.R;

/**
 * Created by christianmacbook on 14/05/18.
 */

public class CustomDialog extends Dialog {
    private Context context;

    public CustomDialog(Context context, int theme) {
        super(context, theme);
        this.context = context;
    }

    public static CustomDialog setupDialogConfirmation(Context context, String keterangan) {
        CustomDialog dialog = new CustomDialog(context,
                R.style.CustomProgressDialog);
        dialog.setContentView(R.layout.dialog_confirm);
        dialog.getWindow().setLayout(ViewPager.LayoutParams.MATCH_PARENT,
                ViewPager.LayoutParams.MATCH_PARENT);

        CustomTextView tvKeterangan = (CustomTextView) dialog.findViewById(R.id.tvKeterangan);
        tvKeterangan.setText(keterangan);

        return dialog;
    }

    public static CustomDialog setupDialogAbsence(Context context){
        CustomDialog dialog = new CustomDialog(context,
                R.style.CustomProgressDialog);
        dialog.setContentView(R.layout.dialog_absen);
        dialog.getWindow().setLayout(ViewPager.LayoutParams.MATCH_PARENT,
                ViewPager.LayoutParams.MATCH_PARENT);
        return dialog;
    }

    public static CustomDialog setupDialogConvert(Context context){
        CustomDialog dialog = new CustomDialog(context,
                R.style.CustomProgressDialog);
        dialog.setContentView(R.layout.dialog_convert);
        dialog.getWindow().setLayout(ViewPager.LayoutParams.MATCH_PARENT,
                ViewPager.LayoutParams.MATCH_PARENT);
        return dialog;
    }

    public static CustomDialog setupDialogAccountAdd(Context context) {
        CustomDialog dialog = new CustomDialog(context,
                R.style.CustomProgressDialog);
        dialog.setContentView(R.layout.dialog_accountadd);
        dialog.getWindow().setLayout(ViewPager.LayoutParams.MATCH_PARENT,
                ViewPager.LayoutParams.MATCH_PARENT);
        dialog.setCancelable(false);

        return dialog;
    }

    public static CustomDialog setupDialogProduct(Context context, String productName) {
        CustomDialog dialog = new CustomDialog(context,
                R.style.CustomProgressDialog);
        dialog.setContentView(R.layout.dialog_buyproduct);
        dialog.getWindow().setLayout(ViewPager.LayoutParams.MATCH_PARENT,
                ViewPager.LayoutParams.MATCH_PARENT);

        CustomTextView tvProductName = (CustomTextView) dialog.findViewById(R.id.tvProductName);
        CustomEditText etPrice = (CustomEditText) dialog.findViewById(R.id.etPrice);
        CustomEditText etDesc = (CustomEditText) dialog.findViewById(R.id.etDesc);
        tvProductName.setText(productName);


        return dialog;
    }

    public static CustomDialog setupDialogProductEdit(Context context, String productName, String qty,String price, String desc) {
        CustomDialog dialog = new CustomDialog(context,
                R.style.CustomProgressDialog);
        dialog.setContentView(R.layout.dialog_editproduct);
        dialog.getWindow().setLayout(ViewPager.LayoutParams.MATCH_PARENT,
                ViewPager.LayoutParams.MATCH_PARENT);

        CustomTextView tvProductName = (CustomTextView) dialog.findViewById(R.id.tvProductName);
        CustomEditText etQty = (CustomEditText) dialog.findViewById(R.id.etQty);
        CustomEditText etPrice = (CustomEditText) dialog.findViewById(R.id.etPrice);
        CustomEditText etDesc = (CustomEditText) dialog.findViewById(R.id.etDesc);
        tvProductName.setText(productName);
        etQty.setText(qty);
       etPrice.setText(price);
       etDesc.setText(desc);

        return dialog;
    }
}
