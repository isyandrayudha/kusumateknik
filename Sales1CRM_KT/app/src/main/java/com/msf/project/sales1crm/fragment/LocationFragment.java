package com.msf.project.sales1crm.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.adapter.LocationAdapter;
import com.msf.project.sales1crm.callback.LocationDialogCallback;
import com.msf.project.sales1crm.callback.LocationListCallback;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.model.LocationModel;
import com.msf.project.sales1crm.presenter.LocationPresenter;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.BindView;

/**
 * Created by christianmacbook on 25/05/18.
 */

@SuppressLint("ValidFragment")
public class LocationFragment extends BaseDialogFragment implements LocationListCallback {

    @BindView(R.id.rlNoConnection)
    RelativeLayout rlNoConnection;

    @BindView(R.id.tvRefresh)
    CustomTextView tvRefresh;

    @BindView(R.id.lvLocation)
    ListView lvLocation;

    @BindView(R.id.lvLocationSwipe)
    SwipeRefreshLayout lvLocationSwipe;

    private Context context;
    private View view, vFooter;
    private LocationDialogCallback callback;
    private LocationPresenter presenter;
    private List<LocationModel> list = new ArrayList<>();
    private LocationAdapter adapter;

    private String type="", id="0";

    @SuppressLint("ValidFragment")
    public LocationFragment(Context context, String type, String id, LocationDialogCallback listener) {
        this.context = context;
        this.callback = listener;
        this.type = type;
        this.id = id;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, 0);
    }

    @Override
    public void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
        Window window = getDialog().getWindow();
        WindowManager.LayoutParams windowParams = window.getAttributes();
        windowParams.width = (int) (this.context.getResources().getDisplayMetrics().widthPixels * 0.99);
        windowParams.height = (int) (this.context.getResources().getDisplayMetrics().heightPixels * 0.97);
        window.setAttributes(windowParams);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreateView(inflater, container, savedInstanceState);

        this.view = inflater.inflate(R.layout.dialog_location, container, false);

        return this.view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);

        this.presenter = new LocationPresenter(this.context, this);

        initData();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
    }

    private void initData(){
        lvLocationSwipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getDataFromAPI();
            }
        });

        tvRefresh.setOnClickListener(click);
        getDataFromAPI();
    }

    private void getDataFromAPI(){
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(context)){
            if (type.equalsIgnoreCase("provinsi")){
                presenter.setupLocation(ApiParam.API_020, id, type);
            }else if (type.equalsIgnoreCase("kabupaten")){
                presenter.setupLocation(ApiParam.API_021, id, type);
            }else if (type.equalsIgnoreCase("kecamatan")){
                presenter.setupLocation(ApiParam.API_022, id, type);
            }else if (type.equalsIgnoreCase("kelurahan")){
                presenter.setupLocation(ApiParam.API_023, id, type);
            }
        }else{
            lvLocationSwipe.setVisibility(View.GONE);
            rlNoConnection.setVisibility(View.VISIBLE);
        }
    }

    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.tvRefresh:
                    lvLocationSwipe.setVisibility(View.VISIBLE);
                    rlNoConnection.setVisibility(View.GONE);

                    getDataFromAPI();
                    break;
                default:
                    break;
            }
        }
    };

    private void setList() {
        adapter = new LocationAdapter(context, this.list);

        List<LocationAdapter.Row> rows = new ArrayList<LocationAdapter.Row>();
        for (LocationModel model : list){
            //Read List by Row to insert into List Adapter
            rows.add(new LocationAdapter.Item(model));
        }
        //Set Row Adapter
        adapter.setRows(rows);
        lvLocation.setAdapter(adapter);
        lvLocation.setOnItemClickListener(new LocationFragment.AdapterOnItemClick());
    }

    @Override
    public void finishList(String result, List<LocationModel> list) {
        if (result.equalsIgnoreCase("OK")){
            this.list = list;
            setList();
        }
    }

    private class AdapterOnItemClick implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            // TODO Auto-generated method stub
            final LocationAdapter.Item item = (LocationAdapter.Item) lvLocation.getAdapter().getItem(position);
            Bundle bundle = new Bundle();
            bundle.putString("id", item.text.getId());
            bundle.putString("name", item.text.getName());
            bundle.putString("kodepos", item.text.getKodepos());
            callback.onLocationDialogCallback(bundle);
            dismiss();
        }
    }

    private String getDate() {
        Calendar c = Calendar.getInstance();

        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        return df.format(c.getTime());
    }
}
