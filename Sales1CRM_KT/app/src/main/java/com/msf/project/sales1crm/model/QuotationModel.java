package com.msf.project.sales1crm.model;

public class QuotationModel {
	private String amount;
	private String quotationId;
	private String productCount;
	private String referencesNumber;
	private String createDate;

	public void setAmount(String amount){
		this.amount = amount;
	}

	public String getAmount(){
		return amount;
	}

	public void setQuotationId(String quotationId){
		this.quotationId = quotationId;
	}

	public String getQuotationId(){
		return quotationId;
	}

	public void setProductCount(String productCount){
		this.productCount = productCount;
	}

	public String getProductCount(){
		return productCount;
	}

	public void setReferencesNumber(String referencesNumber){
		this.referencesNumber = referencesNumber;
	}

	public String getReferencesNumber(){
		return referencesNumber;
	}

	public void setCreateDate(String createDate){
		this.createDate = createDate;
	}

	public String getCreateDate(){
		return createDate;
	}
}
