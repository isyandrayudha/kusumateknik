package com.msf.project.sales1crm.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

/**
 * Created by christianmacbook on 30/05/18.
 */

public class LeadModel implements Comparable, Parcelable {
    private String id;
    private String title_id;
    private String first_name;
    private String last_name;
    private String company;
    private String image_url;
    private String base64Image;
    private String position_id;
    private String position_name;
    private String industry_id;
    private String industry_name;
    private String lead_source_id;
    private String lead_source_name;
    private String lead_status_id;
    private String lead_status_name;
    private String email;
    private String phone;
    private String total_employee;
    private String create_date;
    private String description;
    private String owner_name;
    private String count_date;
    private String assign_to;
    private String assign_to_name;

    //Address
    private String address;
    private String provinsi_id;
    private String provinsi_name;
    private String kabupaten_id;
    private String kabupaten_name;
    private String kecamatan_id;
    private String kecamatan_name;
    private String kelurahan_id;
    private String kelurahan_name;
    private String kodepos;
    private String longitude;
    private String latitude;
    private String distance;

    public LeadModel() {
        setId("");
        setTitle_id("");
        setFirst_name("");
        setLast_name("");
        setCompany("");
        setImage_url("");
        setBase64Image("");
        setPosition_id("");
        setPosition_name("");
        setIndustry_id("");
        setIndustry_name("");
        setLead_source_id("");
        setLead_source_name("");
        setLead_status_id("");
        setLead_status_name("");
        setEmail("");
        setPhone("");
        setTotal_employee("");
        setCreate_date("");
        setDescription("");
        setOwner_name("");
        setCount_date("");
        setAssign_to("");
        setAssign_to_name("");
        setAddress("");
        setProvinsi_id("");
        setProvinsi_name("");
        setKabupaten_id("");
        setKabupaten_name("");
        setKecamatan_id("");
        setKecamatan_name("");
        setKelurahan_id("");
        setKelurahan_name("");
        setKodepos("");
        setLongitude("");
        setLatitude("");
        setDistance("");
    }

    public LeadModel(Parcel in) {
        readFromParcel(in);
    }

    public void readFromParcel(Parcel in) {
        id = in.readString();
        title_id = in.readString();
        first_name = in.readString();
        last_name = in.readString();
        company = in.readString();
        image_url = in.readString();
        base64Image = in.readString();
        position_id = in.readString();
        position_name = in.readString();
        industry_id = in.readString();
        industry_name = in.readString();
        lead_source_id = in.readString();
        lead_source_name = in.readString();
        lead_status_id = in.readString();
        lead_status_name = in.readString();
        email = in.readString();
        phone = in.readString();
        total_employee = in.readString();
        create_date = in.readString();
        description = in.readString();
        owner_name = in.readString();
        count_date = in.readString();
        assign_to = in.readString();
        assign_to_name = in.readString();
        address = in.readString();
        provinsi_id = in.readString();
        provinsi_name = in.readString();
        kabupaten_id = in.readString();
        kabupaten_name = in.readString();
        kecamatan_id = in.readString();
        kecamatan_name = in.readString();
        kelurahan_id = in.readString();
        kelurahan_name = in.readString();
        kodepos = in.readString();
        longitude = in.readString();
        latitude = in.readString();
        distance = in.readString();
    }

    public static final Creator<LeadModel> CREATOR = new Creator<LeadModel>() {
        @Override
        public LeadModel createFromParcel(Parcel in) {
            return new LeadModel(in);
        }

        @Override
        public LeadModel[] newArray(int size) {
            return new LeadModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle_id() {
        return title_id;
    }

    public void setTitle_id(String title_id) {
        this.title_id = title_id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getBase64Image() {
        return base64Image;
    }

    public void setBase64Image(String base64Image) {
        this.base64Image = base64Image;
    }

    public String getPosition_id() {
        return position_id;
    }

    public void setPosition_id(String position_id) {
        this.position_id = position_id;
    }

    public String getPosition_name() {
        return position_name;
    }

    public void setPosition_name(String position_name) {
        this.position_name = position_name;
    }

    public String getIndustry_id() {
        return industry_id;
    }

    public void setIndustry_id(String industry_id) {
        this.industry_id = industry_id;
    }

    public String getIndustry_name() {
        return industry_name;
    }

    public void setIndustry_name(String industry_name) {
        this.industry_name = industry_name;
    }

    public String getLead_source_id() {
        return lead_source_id;
    }

    public void setLead_source_id(String lead_source_id) {
        this.lead_source_id = lead_source_id;
    }

    public String getLead_source_name() {
        return lead_source_name;
    }

    public void setLead_source_name(String lead_source_name) {
        this.lead_source_name = lead_source_name;
    }

    public String getLead_status_id() {
        return lead_status_id;
    }

    public void setLead_status_id(String lead_status_id) {
        this.lead_status_id = lead_status_id;
    }

    public String getLead_status_name() {
        return lead_status_name;
    }

    public void setLead_status_name(String lead_status_name) {
        this.lead_status_name = lead_status_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getTotal_employee() {
        return total_employee;
    }

    public void setTotal_employee(String total_employee) {
        this.total_employee = total_employee;
    }

    public String getCreate_date() {
        return create_date;
    }

    public void setCreate_date(String create_date) {
        this.create_date = create_date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOwner_name() {
        return owner_name;
    }

    public void setOwner_name(String owner_name) {
        this.owner_name = owner_name;
    }

    public String getCount_date() {
        return count_date;
    }

    public void setCount_date(String count_date) {
        this.count_date = count_date;
    }

    public String getAssign_to() {
        return assign_to;
    }

    public void setAssign_to(String assign_to) {
        this.assign_to = assign_to;
    }

    public String getAssign_to_name() {
        return assign_to_name;
    }

    public void setAssign_to_name(String assign_to_name) {
        this.assign_to_name = assign_to_name;
    }

    //Address
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getProvinsi_id() {
        return provinsi_id;
    }

    public void setProvinsi_id(String provinsi_id) {
        this.provinsi_id = provinsi_id;
    }

    public String getProvinsi_name() {
        return provinsi_name;
    }

    public void setProvinsi_name(String provinsi_name) {
        this.provinsi_name = provinsi_name;
    }

    public String getKabupaten_id() {
        return kabupaten_id;
    }

    public void setKabupaten_id(String kabupaten_id) {
        this.kabupaten_id = kabupaten_id;
    }

    public String getKabupaten_name() {
        return kabupaten_name;
    }

    public void setKabupaten_name(String kabupaten_name) {
        this.kabupaten_name = kabupaten_name;
    }

    public String getKecamatan_id() {
        return kecamatan_id;
    }

    public void setKecamatan_id(String kecamatan_id) {
        this.kecamatan_id = kecamatan_id;
    }

    public String getKecamatan_name() {
        return kecamatan_name;
    }

    public void setKecamatan_name(String kecamatan_name) {
        this.kecamatan_name = kecamatan_name;
    }

    public String getKelurahan_id() {
        return kelurahan_id;
    }

    public void setKelurahan_id(String kelurahan_id) {
        this.kelurahan_id = kelurahan_id;
    }

    public String getKelurahan_name() {
        return kelurahan_name;
    }

    public void setKelurahan_name(String kelurahan_name) {
        this.kelurahan_name = kelurahan_name;
    }

    public String getKodepos() {
        return kodepos;
    }

    public void setKodepos(String kodepos) {
        this.kodepos = kodepos;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(title_id);
        dest.writeString(first_name);
        dest.writeString(last_name);
        dest.writeString(company);
        dest.writeString(image_url);
        dest.writeString(base64Image);
        dest.writeString(position_id);
        dest.writeString(position_name);
        dest.writeString(industry_id);
        dest.writeString(industry_name);
        dest.writeString(lead_source_id);
        dest.writeString(lead_source_name);
        dest.writeString(lead_status_id);
        dest.writeString(lead_status_name);
        dest.writeString(email);
        dest.writeString(phone);
        dest.writeString(total_employee);
        dest.writeString(create_date);
        dest.writeString(description);
        dest.writeString(owner_name);
        dest.writeString(count_date);
        dest.writeString(assign_to);
        dest.writeString(assign_to_name);
        dest.writeString(address);
        dest.writeString(provinsi_id);
        dest.writeString(provinsi_name);
        dest.writeString(kabupaten_id);
        dest.writeString(kabupaten_name);
        dest.writeString(kecamatan_id);
        dest.writeString(kecamatan_name);
        dest.writeString(kelurahan_id);
        dest.writeString(kelurahan_name);
        dest.writeString(kodepos);
        dest.writeString(longitude);
        dest.writeString(latitude);
        dest.writeString(distance);
    }

    @Override
    public int compareTo(@NonNull Object o) {
        return this.getFirst_name().compareToIgnoreCase(
                ((LeadModel) o).getFirst_name());
    }
}
