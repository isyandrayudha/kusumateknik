package com.msf.project.sales1crm.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.msf.project.sales1crm.fragment.ViewpipelineTabFragment;

public class OpportunityListTabByStageAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;
    Bundle bundle;
    private ViewpipelineTabFragment mFragment;

    public OpportunityListTabByStageAdapter(FragmentManager supportFragmentManager , int NumOfTabs, ViewpipelineTabFragment fragment) {
        super(supportFragmentManager);
        this.mNumOfTabs = NumOfTabs;
        this.mFragment = fragment;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
//            case 0 :
////                VNoneFragment vNoneFragment = new VNoneFragment();
////                return vNoneFragment;

//            case 6:
//                VNoneFragment vNoneFragment = new VNoneFragment();
//                return vNoneFragment;

        }
        return null;
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
