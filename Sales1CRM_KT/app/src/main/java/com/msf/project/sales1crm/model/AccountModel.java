package com.msf.project.sales1crm.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

/**
 * Created by christianmacbook on 11/05/18.
 */

public class AccountModel implements Comparable, Parcelable {
    private String id;
    private String account_code;
    private String account_name;
    private String base64Image;
    private String imageUrl;
    private String email;
    private String phone;
    private String website;
    private String description;
    private String type_id;
    private String type_name;
    private String industry_id;
    private String industry_name;
    private String owner_name;
    private String total_employee;
    private String last_update_name;
    private String address_billing;
    private String address_shipping;
    private String related_people;
    private String top_opportunity;
    private String top_5_product;
    private String new_event;
    private String new_task;
    private String count_opportunity;
    private String count_task;
    private String count_event;
    private String created_date;
    private String assign_to;
    private String assign_to_name;
    private AccountAddressModel billing;

    public AccountModel() {
        setId("");
        setAccount_code("");
        setAccount_name("");
        setBase64Image("");
        setImageUrl("");
        setEmail("");
        setPhone("");
        setWebsite("");
        setDescription("");
        setType_id("");
        setType_name("");
        setIndustry_id("");
        setIndustry_name("");
        setOwner_name("");
        setTotal_employee("");
        setLast_update_name("");
        setAddress_shipping("");
        setAddress_billing("");
        setRelated_people("");
        setTop_opportunity("");
        setTop_5_product("");
        setNew_event("");
        setNew_task("");
        setCount_opportunity("");
        setCount_task("");
        setCount_event("");
        setCreated_date("");
        setAssign_to("");
        setAssign_to_name("");
    }

    public AccountModel(Parcel in) {
        readFromParcel(in);
    }

    public void readFromParcel(Parcel in) {
        id = in.readString();
        account_code = in.readString();
        account_name = in.readString();
        base64Image = in.readString();
        imageUrl = in.readString();
        email = in.readString();
        phone = in.readString();
        website = in.readString();
        description = in.readString();
        type_id = in.readString();
        type_name = in.readString();
        industry_id = in.readString();
        industry_name = in.readString();
        owner_name = in.readString();
        total_employee = in.readString();
        last_update_name = in.readString();
        address_billing = in.readString();
        address_shipping = in.readString();
        related_people = in.readString();
        top_opportunity = in.readString();
        top_5_product = in.readString();
        new_event = in.readString();
        new_task = in.readString();
        count_opportunity = in.readString();
        count_task = in.readString();
        count_event = in.readString();
        created_date = in.readString();
        assign_to = in.readString();
        assign_to_name = in.readString();
    }

    public static final Creator<AccountModel> CREATOR = new Creator<AccountModel>() {
        @Override
        public AccountModel createFromParcel(Parcel in) {
            return new AccountModel(in);
        }

        @Override
        public AccountModel[] newArray(int size) {
            return new AccountModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAccount_code() {
        return account_code;
    }

    public void setAccount_code(String account_code) {
        this.account_code = account_code;
    }

    public String getAccount_name() {
        return account_name;
    }

    public void setAccount_name(String account_name) {
        this.account_name = account_name;
    }

    public String getBase64Image() {
        return base64Image;
    }

    public void setBase64Image(String base64Image) {
        this.base64Image = base64Image;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType_id() {
        return type_id;
    }

    public void setType_id(String type_id) {
        this.type_id = type_id;
    }

    public String getType_name() {
        return type_name;
    }

    public void setType_name(String type_name) {
        this.type_name = type_name;
    }

    public String getIndustry_id() {
        return industry_id;
    }

    public void setIndustry_id(String industry_id) {
        this.industry_id = industry_id;
    }

    public String getIndustry_name() {
        return industry_name;
    }

    public void setIndustry_name(String industry_name) {
        this.industry_name = industry_name;
    }

    public String getOwner_name() {
        return owner_name;
    }

    public void setOwner_name(String owner_name) {
        this.owner_name = owner_name;
    }

    public String getTotal_employee() {
        return total_employee;
    }

    public void setTotal_employee(String total_employee) {
        this.total_employee = total_employee;
    }

    public String getLast_update_name() {
        return last_update_name;
    }

    public void setLast_update_name(String last_update_name) {
        this.last_update_name = last_update_name;
    }

    public String getAddress_billing() {
        return address_billing;
    }

    public void setAddress_billing(String address_billing) {
        this.address_billing = address_billing;
    }

    public String getAddress_shipping() {
        return address_shipping;
    }

    public void setAddress_shipping(String address_shipping) {
        this.address_shipping = address_shipping;
    }

    public String getRelated_people() {
        return related_people;
    }

    public void setRelated_people(String related_people) {
        this.related_people = related_people;
    }

    public String getTop_opportunity() {
        return top_opportunity;
    }

    public void setTop_opportunity(String top_opportunity) {
        this.top_opportunity = top_opportunity;
    }

    public String getTop_5_product() {
        return top_5_product;
    }

    public void setTop_5_product(String top_5_product) {
        this.top_5_product = top_5_product;
    }

    public String getNew_event() {
        return new_event;
    }

    public void setNew_event(String new_event) {
        this.new_event = new_event;
    }

    public String getNew_task() {
        return new_task;
    }

    public void setNew_task(String new_task) {
        this.new_task = new_task;
    }

    public String getCount_opportunity() {
        return count_opportunity;
    }

    public void setCount_opportunity(String count_opportunity) {
        this.count_opportunity = count_opportunity;
    }

    public String getCount_task() {
        return count_task;
    }

    public void setCount_task(String count_task) {
        this.count_task = count_task;
    }

    public String getCount_event() {
        return count_event;
    }

    public void setCount_event(String count_event) {
        this.count_event = count_event;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public String getAssign_to() {
        return assign_to;
    }

    public void setAssign_to(String assign_to) {
        this.assign_to = assign_to;
    }

    public String getAssign_to_name() {
        return assign_to_name;
    }

    public void setAssign_to_name(String assign_to_name) {
        this.assign_to_name = assign_to_name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(account_code);
        dest.writeString(account_name);
        dest.writeString(base64Image);
        dest.writeString(imageUrl);
        dest.writeString(email);
        dest.writeString(phone);
        dest.writeString(website);
        dest.writeString(description);
        dest.writeString(type_id);
        dest.writeString(type_name);
        dest.writeString(industry_id);
        dest.writeString(industry_name);
        dest.writeString(owner_name);
        dest.writeString(total_employee);
        dest.writeString(last_update_name);
        dest.writeString(address_billing);
        dest.writeString(address_shipping);
        dest.writeString(related_people);
        dest.writeString(top_opportunity);
        dest.writeString(top_5_product);
        dest.writeString(new_event);
        dest.writeString(new_task);
        dest.writeString(count_opportunity);
        dest.writeString(count_task);
        dest.writeString(count_event);
        dest.writeString(created_date);
        dest.writeString(assign_to);
        dest.writeString(assign_to_name);
    }

    @Override
    public int compareTo(@NonNull Object o) {
        return this.getAccount_name().compareToIgnoreCase(
                ((AccountModel) o).getAccount_name());
    }

    public AccountAddressModel getBilling() {
        return billing;
    }

    public void setBilling(AccountAddressModel billing) {
        this.billing = billing;
    }
}
