package com.msf.project.sales1crm.fragment;


import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.github.clans.fab.FloatingActionButton;
import com.mapbox.android.core.location.LocationEngine;
import com.mapbox.android.core.location.LocationEngineListener;
import com.mapbox.android.core.location.LocationEnginePriority;
import com.mapbox.android.core.location.LocationEngineProvider;
import com.mapbox.android.core.permissions.PermissionsListener;
import com.mapbox.android.core.permissions.PermissionsManager;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.annotations.Icon;
import com.mapbox.mapboxsdk.annotations.IconFactory;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.plugins.locationlayer.LocationLayerPlugin;
import com.mapbox.mapboxsdk.plugins.locationlayer.modes.CameraMode;
import com.mapbox.mapboxsdk.plugins.locationlayer.modes.RenderMode;
import com.msf.project.sales1crm.AccountAddActivity;
import com.msf.project.sales1crm.AccountDetailActivity;
import com.msf.project.sales1crm.MenuActivity;
import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.callback.AccountListCallback;
import com.msf.project.sales1crm.customview.CustomDialog;
import com.msf.project.sales1crm.customview.CustomEditText;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.model.AccountModel;
import com.msf.project.sales1crm.presenter.AccountListPresenter;
import com.msf.project.sales1crm.service.GPSTracker;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

;


public class AccountMapStreesFragment extends Fragment implements OnMapReadyCallback, LocationEngineListener,
        PermissionsListener, MapboxMap.OnMapClickListener, AccountListCallback {

    //    @BindView(R.id.mapView)
//    MapView map;


    @BindView(R.id.faMyLocation)
    FloatingActionButton faMyLocation;
    Unbinder unbinder;
    @BindView(R.id.etSearch)
    CustomEditText etSearch;
    @BindView(R.id.ivClear)
    ImageView ivClear;
    @BindView(R.id.rlSearch)
    RelativeLayout rlSearch;
    //    @BindView(R.id.faAccountAdd)
//    FloatingActionButton faAccountAdd;
    @BindView(R.id.LocationAccount)
    CustomTextView LocationAccount;
    @BindView(R.id.AccountAdd)
    CustomTextView AccountAdd;
    @BindView(R.id.btn_map)
    ImageView btnMap;
    @BindView(R.id.rl_big)
    RelativeLayout rlBig;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.llNotFound)
    LinearLayout llNotFound;


    private MapView mapView;
    private MapboxMap mMapboxMap;
    private Context context;
    private CustomDialog dialogQuick;
    private AccountListPresenter presenter;
    private PermissionsManager permissionsManager;
    private LocationEngine locationEngine;
    private LocationLayerPlugin locationLayerPlugin;
    private Location originLocation;
    private List<AccountModel> list = new ArrayList<>();
    private Point originPosisition;
    private Point destinationPosisition;
    private Marker destinationMarker;
    private String stringLatitude, stringLongitude;
    private Geocoder geocoder;
    private List<Address> addresses;
    private BottomSheetBehavior mBottomSheetBehavior;
    private Menu menu;


    private int prevSize = 0, sort_id = 0;
    private boolean isLoadMore = true;
    private boolean isMaxSize = true;
    private boolean pause = false;
    private String kecamatan;
    private String kabupaten;
    private String provinsi;
    private String kelurahan;
    private String alamat;
    private String jalan;
    private String no;
    private String latitude;
    private String longitude;
    private String postalCode;


    public static AccountMapStreesFragment newInstance() {
        AccountMapStreesFragment fragment = new AccountMapStreesFragment();
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);


    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_account_map_strees, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;


    }

//  public void onMapSearch(View view) {
//        etSearch.getText().toString();
//        List<Address> addressList = null;
//        if (location !)
//  }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeThisFragment(savedInstanceState);


        this.context = getActivity();
        presenter = new AccountListPresenter(context, this);


        getDataFromAPI(0, sort_id);

        AccountAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, AccountAddActivity.class);
                intent.putExtra("alamat", alamat);
                intent.putExtra("jalan", jalan);
                intent.putExtra("no", no);
                intent.putExtra("provinsi", provinsi);
                intent.putExtra("kabupaten", kabupaten);
                intent.putExtra("kecamatan", kecamatan);
                intent.putExtra("kelurahan", kelurahan);
                intent.putExtra("postalCode", postalCode);
                intent.putExtra("latitude", latitude);
                intent.putExtra("longitude", longitude);
                startActivity(intent);


            }
        });

    }


    private void initializeThisFragment(Bundle savedInstanceState) {
        //Instantiate mapview
        Mapbox.getInstance(getActivity(), getString(R.string.key_access_token_production));
        mapView = getView().findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.setStyleUrl(getResources().getString(R.string.layer_streets) + "?auth=" + getResources().getString(R.string.key_access_token_development));
//        mapView.setStyleUrl(Style.MAPBOX_STREETS);
        mapView.getMapAsync(this);


//        mapView.getMapAsync((MapboxMap mapboxMap) -> {
//            AccountMapStreesFragment.this.mMapboxMap = mapboxMap;
//
//            mapboxMap.setMaxZoomPreference(18);
//            mapboxMap.addMarker(new MarkerOptions()
//                    .position(new LatLng(-6.173460, 106.816917))
//                    .title("Petojo").snippet("Tanah Abang"));
//
//        });


    }


    private void getDataFromAPI(int prev, int sord_id) {
        isMaxSize = true;
        isLoadMore = true;
        llNotFound.setVisibility(View.GONE);

        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(context)) {
            GPSTracker gpsTracker = new GPSTracker(context);
            if (gpsTracker.canGetLocation()) {
                stringLatitude = String.valueOf(gpsTracker.latitude);
                stringLongitude = String.valueOf(gpsTracker.longitude);

                pause = false;
                presenter.setupList(ApiParam.API_010, prev, sort_id, etSearch.getText().toString(), stringLatitude, stringLongitude);
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gpsTracker.showSettingsAlert();


            }
        }
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menus, menu);
        this.menu = menu;
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_list:
                    MenuActivity.getInstance().setFragment(AccountFrangment.newInstance());
                    item.setIcon(R.drawable.ic_list_white_24dp);
                break;
            case R.id.action_cards:
                    MenuActivity.getInstance().setFragment(AccountCardFrangment.newInstance());
                    item.setIcon(R.drawable.ic_cards_black_24dp);
                break;
            case R.id.action_maps:
                    MenuActivity.getInstance().setFragment(AccountMapStreesFragment.newInstance());
                    item.setIcon(R.drawable.ic_map_white_24dp);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public void onMapReady(MapboxMap mapboxMap) {
        mMapboxMap = mapboxMap;
        mMapboxMap.addOnMapClickListener(this);
        mapboxMap.getUiSettings().setRotateGesturesEnabled(false);


        faMyLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.i("sekarang", String.valueOf(originLocation.getLatitude()));
                mMapboxMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(originLocation.getLatitude()
                        , originLocation.getLongitude()), 12.0));

//                LocationAccount.setText(originLocation.getLatitude()+","+originLocation.getLongitude());

            }
        });


        enableLocation();
    }

    private void enableLocation() {
        if (PermissionsManager.areLocationPermissionsGranted(getActivity())) {
            initializedLocationEngine();
            intializedLocationLayer();

        } else {
            permissionsManager = new PermissionsManager(this);
            permissionsManager.requestLocationPermissions(getActivity());
        }

    }

    @SuppressWarnings("MissingPermission")
    private void initializedLocationEngine() {
        locationEngine = new LocationEngineProvider(getActivity()).obtainBestLocationEngineAvailable();
        locationEngine.setPriority(LocationEnginePriority.HIGH_ACCURACY);
        locationEngine.activate();

        Location lastLocation = locationEngine.getLastLocation();
        if (lastLocation != null) {
            originLocation = lastLocation;
            setCameraPosition(lastLocation);
        } else {
            locationEngine.addLocationEngineListener(this);
        }
    }

    @SuppressWarnings("MissingPermission")
    private void intializedLocationLayer() {
        locationLayerPlugin = new LocationLayerPlugin(mapView, mMapboxMap, locationEngine);
        locationLayerPlugin.setLocationLayerEnabled(true);
        locationLayerPlugin.setCameraMode(CameraMode.TRACKING);
        locationLayerPlugin.setRenderMode(RenderMode.NORMAL);
    }

    private void setCameraPosition(Location location) {
        mMapboxMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude()
                , location.getLongitude()), 8.0));
        Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(originLocation.getLatitude(), originLocation.getLongitude(), 1);
            if (addresses != null && addresses.size() > 0) {
                alamat = addresses.get(0).getAddressLine(0);
                jalan = addresses.get(0).getThoroughfare();
                no = addresses.get(0).getSubThoroughfare();
                kecamatan = addresses.get(0).getLocality();
                String country = addresses.get(0).getCountryName();
                postalCode = addresses.get(0).getPostalCode();
                String knowName = addresses.get(0).getFeatureName();
                kelurahan = addresses.get(0).getSubLocality();
                provinsi = addresses.get(0).getAdminArea();
                kabupaten = addresses.get(0).getSubAdminArea();
                latitude = String.valueOf(addresses.get(0).getLatitude());
                longitude = String.valueOf(addresses.get(0).getLongitude());


                progressBar.setVisibility(View.VISIBLE);
                LocationAccount.setText(alamat);
                LocationAccount.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        ivClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mMapboxMap.clear();
                List<Address> addresses;
                try {
                    addresses = geocoder.getFromLocationName(etSearch.getText().toString(), 1);
                    if (addresses.size() > 0) {
                        Double lat = (double) (addresses.get(0).getLatitude());
                        Double lon = (double) (addresses.get(0).getLongitude());

                        final LatLng user = new LatLng(lat, lon);
                        IconFactory iconFactory = IconFactory.getInstance(context);
                        Icon icon = iconFactory.fromResource(R.drawable.mapicon);
                        mMapboxMap.addMarker(new MarkerOptions()
                                .position(user)
                                .title(String.valueOf(etSearch.getText().toString()))
                                .icon(icon));

                        mMapboxMap.moveCamera(CameraUpdateFactory.newLatLngZoom(user, 15));
                        mMapboxMap.animateCamera(CameraUpdateFactory.zoomBy(10), 2000, null);
                    }

                    Geocoder geocoder1 = new Geocoder(getActivity(), Locale.getDefault());
                    try {
                        List<Address> addresses1 = geocoder1.getFromLocation(addresses.get(0).getLatitude(), addresses.get(0).getLongitude(), 1);
                        if (addresses1 != null && addresses1.size() > 0) {
                            alamat = addresses.get(0).getAddressLine(0);
                            jalan = addresses.get(0).getThoroughfare();
                            no = addresses.get(0).getSubThoroughfare();
                            kecamatan = addresses.get(0).getLocality();
                            String country = addresses.get(0).getCountryName();
                            postalCode = addresses.get(0).getPostalCode();
                            String knowName = addresses.get(0).getFeatureName();
                            kelurahan = addresses.get(0).getSubLocality();
                            provinsi = addresses.get(0).getAdminArea();
                            kabupaten = addresses.get(0).getSubAdminArea();
                            latitude = String.valueOf(addresses.get(0).getLatitude());
                            longitude = String.valueOf(addresses.get(0).getLongitude());


                            progressBar.setVisibility(View.VISIBLE);
                            LocationAccount.setText(alamat);
                            LocationAccount.setVisibility(View.VISIBLE);
                            progressBar.setVisibility(View.GONE);

                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

//        etSearch.getText().toString();
//        List<Address> addressList = null;
//        if (location !=null || !location.equals("")) {
//            Geocoder geocoder1 = new Geocoder(getActivity());
//            try {
//                addressList = geocoder1.getFromLocationName(String.valueOf(location),1);
//
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            Address address = addressList.get(0);
//            LatLng latLng = new LatLng (address.getLatitude(),address.getLongitude());
//            mMapboxMap.addMarker(new MarkerOptions().position(latLng).title("Marker"));
//            mMapboxMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
//        }


//        btnMap.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                rlBig.setVisibility(View.INVISIBLE);
//                faMyLocation.setVisibility(View.INVISIBLE);
//
//
//            }
//        });


//        LocationAccount.setText(originLocation.getLatitude()+","+originLocation.getLongitude());
    }


    @Override
    public void onMapClick(@NonNull LatLng point) {


        if (destinationMarker != null) {
            mMapboxMap.removeMarker(destinationMarker);
        }
        IconFactory iconFactory = IconFactory.getInstance(context);
        Icon icon = iconFactory.fromResource(R.drawable.mapicon);
        destinationMarker = mMapboxMap.addMarker(new MarkerOptions()
                .position(point).icon(icon));
        Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(point.getLatitude(), point.getLongitude(), 1);
            if (addresses != null && addresses.size() > 0) {
                alamat = addresses.get(0).getAddressLine(0);
                jalan = addresses.get(0).getThoroughfare();
                no = addresses.get(0).getSubThoroughfare();
                kecamatan = addresses.get(0).getLocality();
                String country = addresses.get(0).getCountryName();
                postalCode = addresses.get(0).getPostalCode();
                String knowName = addresses.get(0).getFeatureName();
                kelurahan = addresses.get(0).getSubLocality();
                provinsi = addresses.get(0).getAdminArea();
                kabupaten = addresses.get(0).getSubAdminArea();
                latitude = String.valueOf(addresses.get(0).getLatitude());
                longitude = String.valueOf(addresses.get(0).getLongitude());


                progressBar.setVisibility(View.VISIBLE);
                LocationAccount.setText(alamat);
                LocationAccount.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);


            }

        } catch (IOException e) {
            e.printStackTrace();
        }


    }


    @Override
    @SuppressWarnings("MissingPermission")
    public void onConnected() {
        locationEngine.requestLocationUpdates();

    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            originLocation = location;
            setCameraPosition(location);
        }
    }


    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {


    }

    @Override
    public void onPermissionResult(boolean granted) {
        if (granted) {
            enableLocation();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mapView.onDestroy();
        unbinder.unbind();
    }


    @Override
    public void finishAccountList(String result, List<AccountModel> list) {
        mapView.getMapAsync((MapboxMap mapboxMap) -> {
            AccountMapStreesFragment.this.mMapboxMap = mapboxMap;
            mapboxMap.setMaxZoomPreference(18);
            LatLng[] latLng = new LatLng[list.size()];
            AccountModel[] accountModels = new AccountModel[list.size()];
            for (int i = 0; i < list.size(); i++) {
                try {
                    JSONObject billingObject = new JSONObject(list.get(i).getAddress_billing());
                    Log.i("SIZE MAP", String.valueOf(billingObject.length()));
                    latLng[i] = new LatLng(Double.parseDouble(billingObject.getString("latitude")),
                            Double.parseDouble(billingObject.getString("longitude")));
                    accountModels[i] = list.get(i);
                    mapboxMap.addMarker(new MarkerOptions()
                            .position(latLng[i])
                            .title(list.get(i).getAccount_name())
                            .snippet(billingObject.getString("alamat")
                                    .concat("\n" + list.get(i).getPhone())));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                mapboxMap.setOnInfoWindowClickListener(new MapboxMap.OnInfoWindowClickListener() {
                    @Override
                    public boolean onInfoWindowClick(@NonNull Marker marker) {
                        for (int i = 0; i < list.size(); i++) {
                            if (marker.getPosition().equals(latLng[i])) {
                                Intent intent = new Intent(context, AccountDetailActivity.class);
                                intent.putExtra("accountModel", accountModels[i]);
                                startActivity(intent);
                            }
                        }
                        return false;
                    }
                });

//                mapboxMap.setOnMarkerClickListener(new MapboxMap.OnMarkerClickListener() {
//                    @Override
//                    public boolean onMarkerClick(@NonNull Marker marker) {
//                        for (int i = 0; i < list.size(); i++) {
//                                if (marker.getPosition().equals(latLng[i])) {
//                                    Intent intent = new Intent(context, AccountDetailActivity.class);
//                                    intent.putExtra("accountModel", accountModels[i]);
//                                    startActivity(intent);
//                                }
//                        }
//                        return false;
//                    }
//                });

                if (list.size() > 0){
                    llNotFound.setVisibility(View.GONE);
                } else {
                    llNotFound.setVisibility(View.VISIBLE);
                }
            }


        });


    }


    @Override
    public void finishAccountMoreList(String result, List<AccountModel> list) {

    }

    @Override
    public void finishQuickCreate(String result) {

    }


}
