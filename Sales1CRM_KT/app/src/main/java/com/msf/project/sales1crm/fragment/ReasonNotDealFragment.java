package com.msf.project.sales1crm.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.adapter.ReasonListAdapter;
import com.msf.project.sales1crm.callback.ReasonCallback;
import com.msf.project.sales1crm.callback.ReasonDialogCallback;
import com.msf.project.sales1crm.customview.CustomEditText;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.model.ReasonModel;
import com.msf.project.sales1crm.presenter.ReasonPresenter;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

@SuppressLint("ValidFragment")
public class ReasonNotDealFragment extends BaseDialogFragment implements ReasonCallback {
    @BindView(R.id.etSearch)
    CustomEditText etSearch;
    @BindView(R.id.ivClear)
    ImageView ivClear;
    @BindView(R.id.tvRefresh)
    CustomTextView tvRefresh;
    @BindView(R.id.rlNoConnection)
    RelativeLayout rlNoConnection;
    @BindView(R.id.llNotFound)
    LinearLayout llNotFound;
    @BindView(R.id.rlProgress)
    RelativeLayout rlProgress;
    @BindView(R.id.lvReason)
    ListView lvReason;
    @BindView(R.id.lvSwipe)
    SwipeRefreshLayout lvSwipe;
    @BindView(R.id.rlReason)
    RelativeLayout rlReason;
    Unbinder unbinder;
    private View view;
    private Context context;
    private ReasonDialogCallback callback;
    private ReasonPresenter presenter;
    private List<ReasonModel> list = new ArrayList<>();
    private ReasonListAdapter adapter;

    @SuppressLint("ValidFragment")
    public ReasonNotDealFragment(Context context, ReasonDialogCallback callback) {
        this.context = context;
        this.callback = callback;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, 0);
    }

    @Override
    public void onStart() {
        super.onStart();
        Window window = getDialog().getWindow();
        WindowManager.LayoutParams windowParams = window.getAttributes();
        windowParams.width = (int) (this.context.getResources().getDisplayMetrics().widthPixels * 0.99);
        windowParams.height = (int) (this.context.getResources().getDisplayMetrics().heightPixels * 0.97);
        window.setAttributes(windowParams);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        this.view = inflater.inflate(R.layout.fragment_dialog_reason, container);
        unbinder = ButterKnife.bind(this, this.view);
        return this.view;
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.presenter = new ReasonPresenter(context, this);
        getDataApi();
    }

    private void getDataApi() {
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(context)) {
            presenter.setupReason(ApiParam.API_132);
        } else {
            lvSwipe.setVisibility(View.GONE);
            rlNoConnection.setVisibility(View.VISIBLE);
        }
    }

    private void setList() {
        adapter = new ReasonListAdapter(context, this.list);
        List<ReasonListAdapter.Row> rows = new ArrayList<>();
        for (ReasonModel qm : list) {
            rows.add(new ReasonListAdapter.Item(qm));
        }
        adapter.setRows(rows);
        lvReason.setAdapter(adapter);
        lvReason.setOnItemClickListener(new ReasonNotDealFragment.AdapterOnItemClick());
        lvReason.deferNotifyDataSetChanged();
    }

    @Override
    public void finishReason(String result, List<ReasonModel> list) {
        lvSwipe.setRefreshing(false);
        rlProgress.setVisibility(View.GONE);
        if (result.equalsIgnoreCase("OK")) {
            this.list = list;
            setList();
            if (list.size() > 0) {
                llNotFound.setVisibility(View.GONE);
                llNotFound.setVisibility(View.GONE);
            } else {
                llNotFound.setVisibility(View.VISIBLE);
            }
        } else {
            rlNoConnection.setVisibility(View.VISIBLE);
        }
    }

    private class AdapterOnItemClick implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final ReasonListAdapter.Item item = (ReasonListAdapter.Item) lvReason.getAdapter().getItem(position);
            Bundle bundle = new Bundle();
            bundle.putString("id", item.text.getId());
            bundle.putString("reason", item.text.getReason());
            callback.onReason(bundle);
            dismiss();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }
}
