package com.msf.project.sales1crm.model;

public class Datas{
	private Calls calls;
	private ToDoList toDoList;
	private Event event;

	public void setCalls(Calls calls){
		this.calls = calls;
	}

	public Calls getCalls(){
		return calls;
	}

	public void setToDoList(ToDoList toDoList){
		this.toDoList = toDoList;
	}

	public ToDoList getToDoList(){
		return toDoList;
	}

	public void setEvent(Event event){
		this.event = event;
	}

	public Event getEvent(){
		return event;
	}

	@Override
 	public String toString(){
		return 
			"Datas{" + 
			"calls = '" + calls + '\'' + 
			",to_do_list = '" + toDoList + '\'' + 
			",event = '" + event + '\'' + 
			"}";
		}
}
