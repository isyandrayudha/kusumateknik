package com.msf.project.sales1crm.callback;

/**
 * Created by christianmacbook on 24/05/18.
 */

public interface SyncMasterCallback {
    void finishSync(String result);
    void finishUser(String result);
}
