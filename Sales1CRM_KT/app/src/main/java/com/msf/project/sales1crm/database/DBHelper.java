package com.msf.project.sales1crm.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import static com.msf.project.sales1crm.BuildConfig.DB_NAME;
import static com.msf.project.sales1crm.BuildConfig.DB_VERSION;

/**
 * Created by christianmacbook on 23/05/18.
 */

public class DBHelper extends SQLiteOpenHelper {
    public static DBHelper dbHelperInstance;
    private Context mContext;

    public static DBHelper getDbHelperInstance(Context c) {
        if (dbHelperInstance == null) {
            dbHelperInstance = new DBHelper(c);
        }
        return dbHelperInstance;
    }

    public DBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        this.mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        setupTables(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d("TAG", "onUpgrade");
    }

    private void setupTables(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS `" + DBSchema.User.TABLE_NAME + "` (\n" +
                "`" + DBSchema.Base.COL_ID + "` INTEGER PRIMARY KEY,\n" +
                "`" + DBSchema.User.KEY_USER_ID + "` TEXT,\n" +
                "`" + DBSchema.User.KEY_NAME + "` TEXT" +
                ");");
        db.execSQL("CREATE TABLE IF NOT EXISTS `" + DBSchema.Position.TABLE_NAME + "` (\n" +
                "`" + DBSchema.Base.COL_ID + "` INTEGER PRIMARY KEY,\n" +
                "`" + DBSchema.Position.KEY_NAME + "` TEXT" +
                ");");

        db.execSQL("CREATE TABLE IF NOT EXISTS `" + DBSchema.Industry.TABLE_NAME + "` (\n" +
                "`" + DBSchema.Base.COL_ID + "` INTEGER PRIMARY KEY,\n" +
                "`" + DBSchema.Industry.KEY_NAME + "` TEXT" +
                ");");

        db.execSQL("CREATE TABLE IF NOT EXISTS `" + DBSchema.EventStatus.TABLE_NAME + "` (\n" +
                "`" + DBSchema.Base.COL_ID + "` INTEGER PRIMARY KEY,\n" +
                "`" + DBSchema.EventStatus.KEY_NAME + "` TEXT" +
                ");");

        db.execSQL("CREATE TABLE IF NOT EXISTS `" + DBSchema.EventPurpose.TABLE_NAME + "` (\n" +
                "`" + DBSchema.Base.COL_ID + "` INTEGER PRIMARY KEY,\n" +
                "`" + DBSchema.EventPurpose.KEY_NAME + "` TEXT" +
                ");");

        db.execSQL("CREATE TABLE IF NOT EXISTS `" + DBSchema.AccountType.TABLE_NAME + "` (\n" +
                "`" + DBSchema.Base.COL_ID + "` INTEGER PRIMARY KEY,\n" +
                "`" + DBSchema.AccountType.KEY_NAME + "` TEXT" +
                ");");

        db.execSQL("CREATE TABLE IF NOT EXISTS `" + DBSchema.OpportunityStatus.TABLE_NAME + "` (\n" +
                "`" + DBSchema.Base.COL_ID + "` INTEGER PRIMARY KEY,\n" +
                "`" + DBSchema.OpportunityStatus.KEY_NAME + "` TEXT" +
                ");");

        db.execSQL("CREATE TABLE IF NOT EXISTS `" + DBSchema.OpportunityStage.TABLE_NAME + "` (\n" +
                "`" + DBSchema.Base.COL_ID + "` INTEGER PRIMARY KEY,\n" +
                "`" + DBSchema.OpportunityStage.KEY_NAME + "` TEXT,\n" +
                "`" + DBSchema.OpportunityStage.KEY_PERCENT + "` TEXT" +
                ");");

        db.execSQL("CREATE TABLE IF NOT EXISTS `" + DBSchema.LeadSource.TABLE_NAME + "` (\n" +
                "`" + DBSchema.Base.COL_ID + "` INTEGER PRIMARY KEY,\n" +
                "`" + DBSchema.LeadSource.KEY_NAME + "` TEXT" +
                ");");

        db.execSQL("CREATE TABLE IF NOT EXISTS `" + DBSchema.LeadStatus.TABLE_NAME + "` (\n" +
                "`" + DBSchema.Base.COL_ID + "` INTEGER PRIMARY KEY,\n" +
                "`" + DBSchema.LeadStatus.KEY_NAME + "` TEXT" +
                ");");

        db.execSQL("CREATE TABLE IF NOT EXISTS `" + DBSchema.NoVisitReason.TABLE_NAME + "` (\n" +
                "`" + DBSchema.Base.COL_ID + "` INTEGER PRIMARY KEY,\n" +
                "`" + DBSchema.NoVisitReason.KEY_NAME + "` TEXT" +
                ");");

        db.execSQL("CREATE TABLE IF NOT EXISTS `" + DBSchema.UseFor.TABLE_NAME + "` (\n" +
                "`" + DBSchema.Base.COL_ID + "` INTEGER PRIMARY KEY,\n" +
                "`" + DBSchema.UseFor.KEY_NAME + "` TEXT" +
                ");");

        db.execSQL("CREATE TABLE IF NOT EXISTS `" + DBSchema.NoOrderReason.TABLE_NAME + "` (\n" +
                "`" + DBSchema.Base.COL_ID + "` INTEGER PRIMARY KEY,\n" +
                "`" + DBSchema.NoOrderReason.KEY_NAME + "` TEXT" +
                ");");

        db.execSQL("CREATE TABLE IF NOT EXISTS `" + DBSchema.Function.TABLE_NAME + "` (\n" +
                "`" + DBSchema.Function.KEY_FUNCTION_ID + "` TEXT,\n" +
                "`" + DBSchema.Function.KEY_FUNCTION_CODE_NAME + "` TEXT" +
                ");");

        db.execSQL("CREATE TABLE IF NOT EXISTS `" + DBSchema.FunctionUAC.TABLE_NAME + "` (\n" +
                "`" + DBSchema.Function.KEY_FUNCTION_ID + "` TEXT,\n" +
                "`" + DBSchema.Function.KEY_FUNCTION_CODE_NAME + "` TEXT" +
                ");");

        db.execSQL("CREATE TABLE IF NOT EXISTS `" + DBSchema.LocationUser.TABLE_NAME + "` (\n" +
                "`" + DBSchema.Base.COL_ID + "` INTEGER PRIMARY KEY, \n" +
                "`" + DBSchema.User.KEY_USER_ID + "` TEXT,\n" +
                "`" + DBSchema.LocationUser.KEY_LATITUDE + "` TEXT, \n" +
                "`" + DBSchema.LocationUser.KEY_LONGITUDE + "` TEXT, \n" +
                "`" + DBSchema.LocationUser.KEY_ADDRESS + "` TEXT NOT NULL DEFAULT '-' , \n" +
                "`" + DBSchema.LocationUser.KEY_DATE + "` TEXT" +
                ");");

        db.execSQL("CREATE TABLE IF NOT EXISTS `" + DBSchema.MeetTo.TABLE_NAME + "` (\n" +
                "`" + DBSchema.Base.COL_ID + "` INTEGER PRIMARY KEY, \n" +
                "`" + DBSchema.MeetTo.KEY_ACCOUNT_ID + "` TEXT, \n" +
                "`" + DBSchema.MeetTo.KEY_MEET_TO_ID + "` TEXT, \n" +
                "`" + DBSchema.MeetTo.KEY_FULLNAME + "` TEXT, \n" +
                "`" + DBSchema.MeetTo.KEY_IS_CHECK + "` TEXT" +
                ");");

        db.execSQL("CREATE TABLE IF NOT EXISTS `" + DBSchema.Product.TABLE_NAME + "` (\n" +
                "`" + DBSchema.Product.KEY_ID_TEMP + "` INTEGER PRIMARY KEY AUTOINCREMENT, \n" +
                "`" + DBSchema.Product.KEY_ID + "` TEXT NOT NULL DEFAULT '0', \n" +
                "`" + DBSchema.Product.KEY_BRAND + "` TEXT, \n" +
                "`" + DBSchema.Product.KEY_TYPE + "` TEXT, \n" +
                "`" + DBSchema.Product.KEY_SERIAL_NUMBER + "` TEXT, \n" +
                "`" + DBSchema.Product.KEY_CONDITION + "` TEXT, \n" +
                "`" + DBSchema.Product.KEY_STATUC + "` TEXT" +
                ");");

        db.execSQL("CREATE TABLE IF NOT EXISTS `" + DBSchema.Meet_to_list.TABLE_NAME + "` (\n" +
                "`" + DBSchema.Base.COL_ID + "` INTEGER PRIMARY KEY AUTOINCREMENT, \n" +
                "`" + DBSchema.Meet_to_list.KEY_MEET_TO_ID + "` TEXT, \n" +
                "`" + DBSchema.Meet_to_list.KEY_FULLNAME + "` TEXT, \n" +
                "`" + DBSchema.Meet_to_list.KEY_STATUS + "` TEXT, \n" +
                "`" + DBSchema.Meet_to_list.KEY_NOTATION + "` TEXT" +
                ");");

        db.execSQL("CREATE TABLE IF NOT EXISTS `" + DBSchema.Reason.TABLE_NAME + "` (\n" +
                "`" + DBSchema.Base.COL_ID + "` INTEGER PRIMARY KEY AUTOINCREMENT, \n" +
                "`" + DBSchema.Reason.KEY_ID + "` TEXT, \n" +
                "`" + DBSchema.Reason.KEY_REASON + "` TEXT, \n" +
                "`" + DBSchema.Reason.KEY_STAT + "` TEXT" +
                ");");
    }
}
