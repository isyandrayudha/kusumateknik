package com.msf.project.sales1crm.presenter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;

import com.msf.project.sales1crm.callback.TotalStatsCallback;
import com.msf.project.sales1crm.model.TotalModel;
import com.msf.project.sales1crm.service.NetworkConnection;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.PreferenceUtility;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;
import com.msf.project.sales1crm.utility.Sales1CRMUtilsJSON;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ViewReportProductSoldPresenter {
    private final String TAG = ViewReportLeadStatusPresenter.class.getSimpleName();
    private Context context;
    private Messenger messenger;
    private Message message;
    private Bundle bundle;
    private Handler handler;
    private String failureResponse = "";
    private String[] stringResponse = {""};
    private TotalStatsCallback callback;
    private String result = "NG";
    private List<TotalModel> list = new ArrayList<>();
    private List<TotalModel> list1 = new ArrayList<>();

    public ViewReportProductSoldPresenter(Context context, TotalStatsCallback listener) {
        this.context = context;
        this.callback = listener;
    }

    private void doNetworkService(String url, String params, String from) {
        Log.d(TAG, "url login " + url);
        Log.d(TAG, "params : " + params);
        Intent networkIntent = new Intent(this.context, NetworkConnection.class);
        this.messenger = new Messenger(this.handler);
        networkIntent.putExtra("messenger", this.messenger);
        networkIntent.putExtra("url", url);
        networkIntent.putExtra("params", params);
        networkIntent.putExtra("from", from);
        this.context.startService(networkIntent);
    }

    public void setupTotal(int apiIndex, String startDate1, String endDate1, String startDate2, String endDate2, String account_id) {
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(this.context)) {
            obtainItem(apiIndex, startDate1, endDate1, startDate2, endDate2, account_id);
        }else{
            callback.finished(this.result, this.list, this.list1);
        }
    }

    public void obtainItem(int apiIndex, String startDate1, String endDate1, String startDate2, String endDate2, String account_id) {
        this.handler = new Handler(this.context.getMainLooper()) {

            @Override
            public void handleMessage(Message msg) {
                parseItem(msg);
            }

        };

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("api_key", PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.API_KEY));
            jsonObject.put("start_date_periode_1", startDate1);
            jsonObject.put("end_date_periode_1", endDate1);
            jsonObject.put("start_date_periode_2", startDate2);
            jsonObject.put("end_date_periode_2", endDate2);
            jsonObject.put("account_id", account_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        doNetworkService(ApiParam.urlBuilder(this.context, apiIndex), jsonObject.toString(), "ProductSold");
    }

    private void parseItem(Message message) {
        this.message = message;
        this.bundle = this.message.getData();
        this.stringResponse[0] = this.bundle.getString("network_response");
        this.failureResponse = this.bundle.getString("network_failure");
        Log.d(TAG, "responseString[0] login info " + this.stringResponse[0]);
        if (this.failureResponse.equalsIgnoreCase("yes")){
            this.callback.finished(this.result, this.list, this.list1);
        }else{
            try {
                this.result = Sales1CRMUtilsJSON.JSONUtility.getResultFromServer(this.stringResponse[0]);
                if (this.result.equalsIgnoreCase("OK")){
                    this.list = Sales1CRMUtilsJSON.JSONUtility.getProductSold1(this.stringResponse[0]);
                    this.list1 = Sales1CRMUtilsJSON.JSONUtility.getProductSold2(this.stringResponse[0]);
                }
                this.callback.finished(this.result, this.list, this.list1);
            } catch (JSONException e) {
                Log.d(TAG, "Exception detail Response " + e.getMessage());
            }
        }
    }
}
