package com.msf.project.sales1crm.utility;

import android.content.Context;

/**
 * Created by christianmacbook on 11/05/18.
 */

public class ApiParam {

    private final static String TAG = ApiParam.class.getSimpleName();

    /**
     * Domain
     */
    public final static int API_000 = 0;

    /**
     * Login
     */
    public final static int API_001 = 1;

    /**
     * Sync Master
     */
    public final static int API_002 = 2;

    /**
     * User List Master
     */

    public final static int API_003 = 3;

    /**
     * Permisssion
     */

    public final static int API_004 = 4;

    /**
     * Dashboard First
     */
    public final static int API_005 = 5;

    /**
     * Dashboard Second
     */
    public final static int API_006 = 6;

    /**
     * Dashboard Third
     */
    public final static int API_007 = 7;

    /**
     * Dashboard Fourth
     */
    public final static int API_008 = 8;

    /**
     * Dashboard Fifth
     */
    public final static int API_009 = 9;

    /**
     * account List
     */
    public final static int API_010 = 10;

    /**
     * account Add
     */
    public final static int API_011 = 11;

    /**
     * account Edit
     */
    public final static int API_012 = 12;

    /**
     * account Fast Create
     */
    public final static int API_013 = 13;

    /**
     * Privinsi List
     */
    public final static int API_020 = 20;

    /**
     * Kabupaten List
     */
    public final static int API_021 = 21;

    /**
     * Kecamatan List
     */
    public final static int API_022 = 22;

    /**
     * Kelurahan List
     */
    public final static int API_023 = 23;

    /**
     * Lead List
     */
    public final static int API_030 = 30;

    /**
     * Lead Create
     */
    public final static int API_031 = 31;

    /**
     * Lead Edit
     */
    public final static int API_032 = 32;

    /**
     * opportunity List Active
     */
    public final static int API_040 = 40;

    /**
     * opportunity Add
     */
    public final static int API_041 = 41;

    /**
     * opportunity Edit
     */
    public final static int API_042 = 42;

    /**
     * opportunity List Close
     */
    public final static int API_043 = 43;

    /**
     * Product List
     */
    public final static int API_050 = 50;

    /**
     * Event List
     */
    public final static int API_060 = 60;

    /**
     * Event Create
     */
    public final static int API_061 = 61;

    /**
     * Event Edit
     */
    public final static int API_062 = 62;

    /**
     * Task List
     */
    public final static int API_070 = 70;

    /**
     * Task Create
     */
    public final static int API_071 = 71;

    /**
     * Task Edit
     */
    public final static int API_072 = 72;

    /**
     * calls List
     */
    public final static int API_080 = 80;

    /**
     * calls Create
     */
    public final static int API_081 = 81;

    /**
     * calls Edit
     */
    public final static int API_082 = 82;

    /**
     * Stats Report View Report Total leads
     */
    public final static int API_090 = 90;

    /**
     * Stats Report View Report leads Conversion
     */
    public final static int API_091 = 91;

    /**
     * Stats Report View Report Total Oppor
     */
    public final static int API_092 = 92;

    /**
     * Stats Report View Report Oppor Conversion
     */
    public final static int API_093 = 93;

    /**
     * Stats Report View Report Oppor by Industry
     */
    public final static int API_094 = 94;

    /**
     * Stats Report View Report Call report
     */
    public final static int API_095 = 95;

    /**
     * Stats Report View Report Sales Cycle
     */
    public final static int API_096 = 96;

    /**
     * Stats Report View Report Lead Status
     */
    public final static int API_097 = 97;

    /**
     * Stats Report View Report account Task
     */
    public final static int API_098 = 98;

    /**
     * Stats Report View Report account Event
     */
    public final static int API_099 = 99;

    /**
     * Stats Report View Report account Event
     */
    public final static int API_100 = 100;

    /**
     * Stats Report View Report opportunity Source
     */
    public final static int API_101 = 101;

    /**
     * Stats Report View Report Total Revenue
     */
    public final static int API_102 = 102;

    /**
     * Stats Report View Report opportunity Duration
     */
    public final static int API_103 = 103;

    /**
     * Stats Report View Dashboard General
     */
    public final static int API_104 = 104;

    /**
     * Stats Report View Dashboard Lead Dashboard
     */
    public final static int API_105 = 105;

    /**
     *
     *  Activity List
     */
    public final static int API_106 = 106;

    /**
     *
     *  Activity List Event
     */
    public final static int API_107 = 107;

    /**
     *
     *  Activity List To-do
     */
    public final static int API_108 = 108;

    /**
     *
     *  Activity List calls
     */
    public final static int API_109 = 109;

    /**
     * Total Value opportunity
     * @param context
     * @param apiIndex
     * @return
     */
    public final static int API_110 = 110;

    /**
     * opportunity By Stage_Id
     * @param context
     * @param apiIndex
     * @return
     */

    public final static int API_111 = 111;

    /**
     * opportunity list close lost
     * @param context
     * @param apiIndex
     * @return
     */

    public final static int API_112 = 112;

    /**
     * opportunity list close won
     * @param context
     * @param apiIndex
     * @return
     */

    public final static int API_113 = 113;

    /**
     * Upload-Image User
     */

    public final static int API_114 = 114;

    /**
     * User Location
     * @param context
     * @param apiIndex
     * @return
     */

    public final static int API_115 = 115;


    /**
     * Meet to Create
     * @param context
     * @param apiIndex
     * @return
     */

    public final static int API_116 = 116;

    /**
     * Related People
     * @param context
     * @param apiIndex
     * @return
     */

    public final static int API_117 = 117;

    /**
     * Category list
     * @param context
     * @param apiIndex
     * @return
     */

    public final static int API_118 = 118;

    /**
     * Database Machine List
     * @param context
     * @param apiIndex
     * @return
     */

    public final static int API_119 = 119;

    /**
     * Database Machine create
     * @param context
     * @param apiIndex
     * @return
     */

    public final static int API_120 = 120;

    /**
     * Database Machine Edit
     * @param context
     * @param apiIndex
     * @return
     */

    public final static int API_121 = 121;

    /**
     * Absence
     * @param context
     * @param apiIndex
     * @return
     */
    public final static int API_122 = 122;

    /**
     * checkin
     * @param context
     * @param apiIndex
     * @return
     */
    public final static int API_123 = 123;

    /**
     * checkout
     * @param context
     * @param apiIndex
     * @return
     */

    public final static int API_124 = 124;

    /**
     * activity task list by status
     * @param context
     * @param apiIndex
     * @return
     */

    public final static int API_125 = 125;

    /**
     * opportunity detail
     * @param context
     * @param apiIndex
     * @return
     */

    public final static int API_126 = 126;

    /**
     * lead convert
     * @param context
     * @param apiIndex
     * @return
     */

    public final static int API_127 = 127;

    /**
     * kurs list
     * @param context
     * @param apiIndex
     * @return
     */

    public final static int API_128 = 128;

    /**
     * kurs calculate
     * @param context
     * @param apiIndex
     * @return
     */
    public final static int API_129 = 129;


    /**
     * Quotation List
     * @param context
     * @param apiIndex
     * @return
     */

    public final static int API_130 = 130;

    /**
     * meet to list
     * @param context
     * @param apiIndex
     * @return
     */

    public final static int API_131 = 131;

    /**
     * reason not order
     * @param context
     * @param apiIndex
     * @return
     */

    public final static int API_132 = 132;

    /**
     * Logout
     * @param context
     * @param apiIndex
     * @return
     */

    public final static int API_133 = 133;





    public static String urlBuilder(Context context, int apiIndex) {
        String url = PreferenceUtility.getInstance().loadDataString(context,
                PreferenceUtility.URL);
        url += getApiUrl(apiIndex);

        return url;
    }

    public static String urlBuilderGlobal(Context context, int apiIndex) {
        String url = PreferenceUtility.getInstance().loadDataString(context,
                PreferenceUtility.URL_GLOBAL);
        url += getApiUrl(apiIndex);

        return url;
    }

    public static String getApiUrl(int apiIndex) {
        String apiUrl = "";
        switch (apiIndex) {
            case API_000:
                apiUrl = "/api/domain/active";
                break;
            case API_001:
                apiUrl = "/api/user/login";
                break;
            case API_002:
                apiUrl = "/api/setting/configuration";
                break;
            case API_003:
                apiUrl = "/api/user/list";
                break;
            case API_004:
                apiUrl = "/api/user/permission";
                break;
            case API_005:
                apiUrl = "/api/dashboard/first";
                break;
            case API_006:
                apiUrl = "/api/dashboard/second";
                break;
            case API_007:
                apiUrl = "/api/dashboard/third";
                break;
            case API_008:
                apiUrl = "/api/dashboard/fourth";
                break;
            case API_009:
                apiUrl = "/api/dashboard/fifth";
                break;
            case API_010:
                apiUrl = "/api/account/list";
                break;
            case API_011:
                apiUrl = "/api/account/create";
                break;
            case API_012:
                apiUrl = "/api/account/edit";
                break;
            case API_013:
                apiUrl = "/api/account/fast-create";
                break;
            case API_020:
                apiUrl = "/api/area/provinsi-list";
                break;
            case API_021:
                apiUrl = "/api/area/kabupaten-list";
                break;
            case API_022:
                apiUrl = "/api/area/kecamatan-list";
                break;
            case API_023:
                apiUrl = "/api/area/kelurahan-list";
                break;
            case API_030:
                apiUrl = "/api/lead/list";
                break;
            case API_031:
                apiUrl = "/api/lead/create";
                break;
            case API_032:
                apiUrl = "/api/lead/edit";
                break;
            case API_040:
                apiUrl = "/api/opportunity/list";
                break;
            case API_041:
                apiUrl = "/api/opportunity/create";
                break;
            case API_042:
                apiUrl = "/api/opportunity/edit";
                break;
            case API_043:
                apiUrl = "/api/opportunity/list-close-won-lost";
                break;
            case API_050:
                apiUrl = "/api/product/detail";
                break;
            case API_060:
                apiUrl = "/api/crm-event/list";
                break;
            case API_061:
                apiUrl = "/api/crm-event/create";
                break;
            case API_062:
                apiUrl = "/api/crm-event/edit";
                break;
            case API_070:
                apiUrl = "/api/crm-task/list";
                break;
            case API_071:
                apiUrl = "/api/crm-task/create";
                break;
            case API_072:
                apiUrl = "/api/crm-task/edit";
                break;
            case API_080:
                apiUrl = "/api/call/list";
                break;
            case API_081:
                apiUrl = "/api/call/create";
                break;
            case API_082:
                apiUrl = "/api/call/edit";
                break;
            case API_090:
                apiUrl = "/api/stats-report/total-leads";
                break;
            case API_091:
                apiUrl = "/api/stats-report/leads-conversion";
                break;
            case API_092:
                apiUrl = "/api/stats-report/total-opportunities";
                break;
            case API_093:
                apiUrl = "/api/stats-report/opportunity-conversion";
                break;
            case API_094:
                apiUrl = "/api/stats-report/opportunity-by-industry";
                break;
            case API_095:
                apiUrl = "/api/stats-report/call-report";
                break;
            case API_096:
                apiUrl = "/api/stats-report/sales-cycle";
                break;
            case API_097:
                apiUrl = "/api/stats-report/lead-by-status";
                break;
            case API_098:
                apiUrl = "/api/stats-report/account-task";
                break;
            case API_099:
                apiUrl = "/api/stats-report/account-events";
                break;
            case API_100:
                apiUrl = "/api/stats-report/total-product-sold";
                break;
            case API_101:
                apiUrl = "/api/stats-report/opportunity-by-source";
                break;
            case API_102:
                apiUrl = "/api/stats-report/product-revenue";
                break;
            case API_103:
                apiUrl = "/api/stats-report/opportunity-duration";
                break;
            case API_104:
                apiUrl = "/api/stats-report/general-dashboard";
                break;
            case API_105:
                apiUrl = "/api/stats-report/lead-dashboard";
                break;
            case API_106:
                apiUrl = "/api/activity/list";
                break;
            case API_107:
                apiUrl = "/api/activity/list-event";
                break;
            case API_108:
                apiUrl = "/api/activity/list-to-do-list";
                break;
            case API_109:
                apiUrl = "/api/activity/list-calls";
                break;
            case API_110:
                apiUrl = "/api/activity/total-opportunity";
                break;
            case API_111:
                apiUrl = "/api/opportunity/list-by-stage-id";
                break;
            case API_112:
                apiUrl = "/api/opportunity/list-close-lost/";
                break;
            case API_113:
                apiUrl = "/api/opportunity/list-close-won/";
                break;
            case API_114:
                apiUrl = "/api/user/upload-image/";
                break;
            case API_115 :
                apiUrl = "/api/user/location/";
                break;
            case API_116 :
                apiUrl = "/api/crm-event/meet-to-create";
                break;
            case API_117 :
                apiUrl = "/api/account/list-related-people";
                break;
            case API_118 :
                apiUrl = "/api/product/category-list";
                break;
            case API_119:
                apiUrl = "/api/database-machine/list";
                break;
            case API_120:
                apiUrl = "/api/database-machine/create";
                break;
            case API_121:
                apiUrl = "/api/database-machine/edit";
                break;
            case API_122:
                apiUrl = "/api/user/absence";
                break;
            case API_123:
                apiUrl = "/api/user/checkin";
                break;
            case API_124:
                apiUrl = "/api/user/checkout";
                break;
            case API_125:
                apiUrl = "/api/activity/list-by-status";
                break;
            case API_126:
                apiUrl = "/api/opportunity/detail";
                break;
            case API_127:
                apiUrl = "/api/lead/convert";
                break;
            case API_128:
                apiUrl = "/api/kurs/list";
                break;
            case API_129:
                apiUrl = "/api/kurs/calculate";
                break;
            case API_130:
                apiUrl = "/api/opportunity/quotation-list";
                break;
            case API_131:
                apiUrl = "/api/account/list-meet-to";
                break;
            case API_132:
                apiUrl = "/api/opportunity/reason-not-deal";
                break;
            case API_133:
                apiUrl = "/api/user/logout";
                break;
        }
        return apiUrl;
    }
}
