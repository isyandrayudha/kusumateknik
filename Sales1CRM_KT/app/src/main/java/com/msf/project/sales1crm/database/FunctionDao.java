package com.msf.project.sales1crm.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.msf.project.sales1crm.model.FunctionModel;

import java.util.ArrayList;
import java.util.List;

public class FunctionDao extends BaseDao<FunctionModel> implements DBSchema.Function {
    public FunctionDao(Context c) {
        super(c, TABLE_NAME);
    }

    public FunctionDao(Context c, String table, boolean willWrite) {
        super(c, table, willWrite);
    }

    public FunctionDao(DBHelper dbHelper) {
        super(dbHelper);
    }

    public FunctionDao(DBHelper dbHelper, String table) {
        super(dbHelper, table);
    }

    public FunctionDao(DBHelper dbHelper, String table, boolean willWrite) {
        super(dbHelper, table, willWrite);
    }

    public FunctionDao(Context c, DBHelper db, String table, boolean willWrite) {
        super(c, db, table, willWrite);
    }

    @Override
    public FunctionModel getById(int id) {
        String qry = "SELECT * FROM t_function WHERE function_id = " + id;
        Cursor c = getSqliteDb().rawQuery(qry, null);
        FunctionModel model = new FunctionModel();
        try {
            if(c != null && !c.moveToFirst()) {
                 model = getByCursor(c);
            }
        } finally {
             c.close();
        }
        return model;
    }

    public Integer getUACByid(int id) {
        String qry = " SELECT * FROM " + getTable() + " WHERE " + DBSchema.Function.KEY_FUNCTION_ID + " = " + "'" + id + "'";
        Cursor c = getSqliteDb().rawQuery(qry, null);
        FunctionModel model = new FunctionModel();
        try {
            if(c != null && !c.moveToFirst() && !c.isClosed()) {
                model = getByCursor(c);
            }
        } finally  {
                c.close();
        }

        int result;
        if (model == null) {
            result = 0;
        } else {
            result = 1;
        }
        return result;
    }

    public Integer getFunctionByid(String id) {
        String qry = " SELECT * FROM " + getTable() + " WHERE " + DBSchema.Function.KEY_FUNCTION_ID + " = " + "'" + id + "'";
        Cursor c = getSqliteDb().rawQuery(qry, null);
        FunctionModel model = new FunctionModel();
        try {
            if(c != null && !c.moveToFirst()) {
                model = getByCursor(c);
            }
        } finally {
            c.close();
        }

        int result;
        if (model == null) {
            result = 0;
        } else {
            result = 1;
        }
        return result;
    }



    public Integer getCountById(String name) {
        String qry = "SELECT * FROM " + getTable() + " WHERE " + DBSchema.Function.KEY_FUNCTION_CODE_NAME + " = " + "'" + name + "'";
        Cursor c = getSqliteDb().rawQuery(qry, null);

        FunctionModel model = new FunctionModel();
        try {
            if(c != null && c.moveToFirst()) {
                model = getByCursor(c);
            }
        } finally {
            c.close();
        }
        int result;
        if (model == null) {
            result = 0;
        } else {
            result = 1;
        }
        return result;
    }


    public List<FunctionModel> getFunctionList(){
        String query = "SELECT * FROM " + getTable();
        Cursor c = getSqliteDb().rawQuery(query,null);
        List<FunctionModel> list = new ArrayList<>();
        try{
            if (c !=null && c.moveToFirst()){
                list.add(getByCursor(c));
                while (c.moveToNext()) {
                    list.add(getByCursor(c));
                }
            }
        } finally {
            c.close();
        }
        return list;
    }

    public String[] getStringArray(){
        String query = "SELECT * FROM " + getTable();

        Cursor c = getSqliteDb().rawQuery(query,null);
        String[] array = new String[c.getCount() + 1];
        try{
            if(c !=null && c.moveToFirst()) {
                array[0] = "FunctionUser";
                array[0] = getByCursor(c).getFunctionCodeName();
                int i = 2;
                while (c.moveToNext()) {
                    array[i] = getByCursor(c).getFunctionCodeName();
                    i++;
                }
            }
        } finally {
            c.close();
        }
        return  array;
    }

    public String[] getStringArrayID() {
        String query = "SELECT * FROM " + getTable();

        Cursor c = getSqliteDb().rawQuery(query, null);
        String[] array = new String[c.getCount() + 1];
        try{
            if(c != null && c.moveToFirst()) {
                array[0] = "0";
                array[1] = getByCursor(c).getFunctionId();
                int i = 2;
                while (c.moveToNext()) {
                    array[i] = getByCursor(c).getFunctionId();
                    i++;
                }
            }
        } finally {
            {
                c.close();
            }
        }
        return array;
    }



    @Override
    public FunctionModel getByCursor(Cursor c) {
        FunctionModel model = new FunctionModel();
        model.setFunctionId(c.getString(0));
        model.setFunctionCodeName(c.getString(1));
        return model;
    }

    @Override
    protected ContentValues upDataValues(FunctionModel functionModel, boolean update) {
        ContentValues cv = new ContentValues();
//        if (update == true)
            cv.put(KEY_FUNCTION_ID,functionModel.getFunctionId());
            cv.put(KEY_FUNCTION_CODE_NAME,functionModel.getFunctionCodeName());
            return  cv;
    }

}
