package com.msf.project.sales1crm.model;

/**
 * Created by christianmacbook on 25/05/18.
 */

public class OpportunityStageModel {
    private String id;
    private String name;
    private String percent;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPercent() {
        return percent;
    }

    public void setPercent(String percent) {
        this.percent = percent;
    }
}
