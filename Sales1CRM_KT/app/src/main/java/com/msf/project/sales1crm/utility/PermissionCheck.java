package com.msf.project.sales1crm.utility;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by christianmacbook on 15/05/18.
 */

public class PermissionCheck {

    public static final int  REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;
    public static final int REQUEST_CODE_ASK_PERMISSION = 123;

    private int currentApiVersion = Build.VERSION.SDK_INT;

    public boolean checkPermission(final Context context) {
        if (currentApiVersion >= Build.VERSION_CODES.M) {

            List<String> permissionsNeeded = new ArrayList<String>();

            final List<String> permissionsList = new ArrayList<String>();
            if (!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE, context))
                permissionsNeeded.add("WriteStorage");
            if (!addPermission(permissionsList, Manifest.permission.READ_EXTERNAL_STORAGE, context))
                permissionsNeeded.add("ReadStorage");
            if (!addPermission(permissionsList, Manifest.permission.CAMERA, context))
                permissionsNeeded.add("Camera");
            if (!addPermission(permissionsList, Manifest.permission.ACCESS_COARSE_LOCATION, context))
                permissionsNeeded.add("NetworkLocation");
            if (!addPermission(permissionsList, Manifest.permission.ACCESS_FINE_LOCATION, context))
                permissionsNeeded.add("GPSLocation");
            if (!addPermission(permissionsList, Manifest.permission.CALL_PHONE, context))
                permissionsNeeded.add("Phone");
            if (!addPermission(permissionsList, Manifest.permission.READ_CALL_LOG, context))
                permissionsNeeded.add("Call Log");
            if (!addPermission(permissionsList, Manifest.permission.READ_PHONE_STATE, context))
                permissionsNeeded.add("Read Phone");


            if (permissionsList.size() > 0) {
                if (permissionsNeeded.size() > 0) {

                    ActivityCompat.requestPermissions((Activity)context
                            ,permissionsList.toArray(new String[permissionsList.size()]),
                            REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);

                }
            }
            return true;
        } else {
            return true;
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private boolean addPermission(List<String> permissionList, String permission, Context context) {

        if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
            permissionList.add(permission);

            if (!ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, permission))
                ;
            return false;
        }

        return true;

    }
}
