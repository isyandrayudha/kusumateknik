package com.msf.project.sales1crm.callback;

import com.msf.project.sales1crm.model.KursModel;

import java.util.List;

public interface KursCallback {
    void finishKurs(String result, List<KursModel> list);
}
