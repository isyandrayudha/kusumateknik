package com.msf.project.sales1crm.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.msf.project.sales1crm.MenuActivity;
import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.adapter.StatsReportTabAdapter;
import com.msf.project.sales1crm.customview.CustomTextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StatsReportFragment extends Fragment {


    @BindView(R.id.tabStatsReport)
    TabLayout tabStatsReport;

    @BindView(R.id.vpStatsReport)
    ViewPager vpStatsReport;

    @BindView(R.id.rlViewDashboard)
    RelativeLayout rlViewDashboard;

    private View view;
    private Context context;
    private StatsReportTabAdapter adapter;
    private MenuActivity myContext;

    public static StatsReportFragment newInstance() {
        StatsReportFragment fragment = new StatsReportFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Activity activity) {
        myContext = (MenuActivity) activity;
        super.onAttach(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_statsreport, container, false);

        return this.view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);
        this.context = getActivity();

        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("Stats Report");

        initData();
    }

    private void initData(){
        rlViewDashboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Create new fragment and transaction
                Fragment newFragment = new DashboardStatsReportFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();

                // Replace whatever is in the fragment_container view with this fragment,
                // and add the transaction to the back stack if needed
                transaction.replace(R.id.frame_layout, newFragment);
                transaction.addToBackStack(null);

                // Commit the transaction
                transaction.commit();
            }
        });

        CustomTextView tabOne = (CustomTextView) LayoutInflater.from(context).inflate(R.layout.custom_tab, null);
        tabOne.setText("Total leads");
        tabStatsReport.addTab(tabStatsReport.newTab().setCustomView(tabOne));

        CustomTextView tabTwo = (CustomTextView) LayoutInflater.from(context).inflate(R.layout.custom_tab, null);
        tabTwo.setText("Lead Conversion");
        tabStatsReport.addTab(tabStatsReport.newTab().setCustomView(tabTwo));

        CustomTextView tabThree = (CustomTextView) LayoutInflater.from(context).inflate(R.layout.custom_tab, null);
        tabThree.setText("Total opportunity");
        tabStatsReport.addTab(tabStatsReport.newTab().setCustomView(tabThree));

        CustomTextView tabFour = (CustomTextView) LayoutInflater.from(context).inflate(R.layout.custom_tab, null);
        tabFour.setText("opportunity Conversion");
        tabStatsReport.addTab(tabStatsReport.newTab().setCustomView(tabFour));

        CustomTextView tabFive = (CustomTextView) LayoutInflater.from(context).inflate(R.layout.custom_tab, null);
        tabFive.setText("Lead By Status");
        tabStatsReport.addTab(tabStatsReport.newTab().setCustomView(tabFive));

        CustomTextView tabSix = (CustomTextView) LayoutInflater.from(context).inflate(R.layout.custom_tab, null);
        tabSix.setText("opportunity By Industry");
        tabStatsReport.addTab(tabStatsReport.newTab().setCustomView(tabSix));

        CustomTextView tabSeven = (CustomTextView) LayoutInflater.from(context).inflate(R.layout.custom_tab, null);
        tabSeven.setText("opportunity By Source");
        tabStatsReport.addTab(tabStatsReport.newTab().setCustomView(tabSeven));

        CustomTextView tabEight = (CustomTextView) LayoutInflater.from(context).inflate(R.layout.custom_tab, null);
        tabEight.setText("account Task");
        tabStatsReport.addTab(tabStatsReport.newTab().setCustomView(tabEight));

        CustomTextView tabNine = (CustomTextView) LayoutInflater.from(context).inflate(R.layout.custom_tab, null);
        tabNine.setText("account events");
        tabStatsReport.addTab(tabStatsReport.newTab().setCustomView(tabNine));

        CustomTextView tabTen = (CustomTextView) LayoutInflater.from(context).inflate(R.layout.custom_tab, null);
        tabTen.setText("Total Product Sold");
        tabStatsReport.addTab(tabStatsReport.newTab().setCustomView(tabTen));

        CustomTextView tabElevent = (CustomTextView) LayoutInflater.from(context).inflate(R.layout.custom_tab, null);
        tabElevent.setText("Total Product Revenue");
        tabStatsReport.addTab(tabStatsReport.newTab().setCustomView(tabElevent));

        CustomTextView tabTwelve = (CustomTextView) LayoutInflater.from(context).inflate(R.layout.custom_tab, null);
        tabTwelve.setText("opportunity Duration");
        tabStatsReport.addTab(tabStatsReport.newTab().setCustomView(tabTwelve));

        CustomTextView tabThirteen = (CustomTextView) LayoutInflater.from(context).inflate(R.layout.custom_tab, null);
        tabThirteen.setText("Sales Cycle");
        tabStatsReport.addTab(tabStatsReport.newTab().setCustomView(tabThirteen));

        CustomTextView tabFourteen = (CustomTextView) LayoutInflater.from(context).inflate(R.layout.custom_tab, null);
        tabFourteen.setText("Total calls");
        tabStatsReport.addTab(tabStatsReport.newTab().setCustomView(tabFourteen));

        tabStatsReport.setTabMode(TabLayout.MODE_SCROLLABLE);
//        tabStatsReport.setTabGravity(tabStatsReport.GRAVITY_FILL);

        adapter = new StatsReportTabAdapter(myContext.getSupportFragmentManager(), tabStatsReport.getTabCount());
        vpStatsReport.setAdapter(adapter);
        vpStatsReport.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabStatsReport));
        tabStatsReport.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                vpStatsReport.setCurrentItem(tab.getPosition(), true);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }
}
