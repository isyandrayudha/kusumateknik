package com.msf.project.sales1crm.utility;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.msf.project.sales1crm.R;

/**
 * Created by christianmacbook on 25/05/18.
 */

public class CustomLoadingCRM extends DialogFragment {
    private Context context;
    private int loadPosition = 0;
    private View view;
    private Handler mHandler;

    private ImageView image_1, image_2, image_3, image_4;

    public CustomLoadingCRM() {

    }

    @SuppressLint("ValidFragment")
    public CustomLoadingCRM(Context context) {
        this.context = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, 0);
    }

    @Override
    public void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
//        Window window = getDialog().getWindow();
//        WindowManager.LayoutParams windowParams = window.getAttributes();
//        windowParams.width = (int) (this.context.getResources().getDisplayMetrics().widthPixels);
//        windowParams.height = (int) (this.context.getResources().getDisplayMetrics().heightPixels);
//        windowParams.dimAmount = 0.75f;
//        window.setAttributes(windowParams);

        mHandler = new Handler();
        mStatusChecker.run();
    }

    Runnable mStatusChecker = new Runnable() {
        @Override
        public void run() {

            displayLoadingPosition(loadPosition);

            loadPosition++;

            mHandler.postDelayed(mStatusChecker, 500);
        }
    };

    private void displayLoadingPosition(int loadPosition) {
        int emphasizedViewPos = loadPosition % 4;

        switch (emphasizedViewPos) {
            case 0:
                image_1.setImageResource(R.drawable.loading_step_02);

                image_1.setVisibility(View.VISIBLE);
                image_2.setVisibility(View.GONE);
                image_3.setVisibility(View.GONE);
                image_4.setVisibility(View.GONE);
                break;

            case 1:
                image_2.setImageResource(R.drawable.loading_step_03);

                image_1.setVisibility(View.GONE);
                image_2.setVisibility(View.VISIBLE);
                image_3.setVisibility(View.GONE);
                image_4.setVisibility(View.GONE);
                break;

            case 2:
                image_3.setImageResource(R.drawable.loading_step_04);

                image_1.setVisibility(View.GONE);
                image_2.setVisibility(View.GONE);
                image_3.setVisibility(View.VISIBLE);
                image_4.setVisibility(View.GONE);
                break;

            case 3:
                image_4.setImageResource(R.drawable.loading_step_01);

                image_1.setVisibility(View.GONE);
                image_2.setVisibility(View.GONE);
                image_3.setVisibility(View.GONE);
                image_4.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreateView(inflater, container, savedInstanceState);

        this.view = inflater.inflate(R.layout.dialog_loading_crm, container, false);

        image_1 = (ImageView) view.findViewById(R.id.image_1);
        image_2 = (ImageView) view.findViewById(R.id.image_2);
        image_3 = (ImageView) view.findViewById(R.id.image_3);
        image_4 = (ImageView) view.findViewById(R.id.image_4);

        return this.view;
    }
}
