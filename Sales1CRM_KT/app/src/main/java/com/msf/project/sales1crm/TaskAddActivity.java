package com.msf.project.sales1crm;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Spinner;
import com.msf.project.sales1crm.callback.AccountDialogCallback;
import com.msf.project.sales1crm.callback.TaskAddCallback;
import com.msf.project.sales1crm.callback.UserDialogCallback;
import com.msf.project.sales1crm.customview.CustomDialog;
import com.msf.project.sales1crm.customview.CustomEditText;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.customview.DatePickerFragment;
import com.msf.project.sales1crm.customview.SpinnerCustomAdapter;
import com.msf.project.sales1crm.fragment.AccountDialogFragment;
import com.msf.project.sales1crm.fragment.UserDialogFragment;
import com.msf.project.sales1crm.model.TaskModel;
import com.msf.project.sales1crm.presenter.TaskAddPresenter;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.PreferenceUtility;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import butterknife.ButterKnife;
import butterknife.BindView;

public class TaskAddActivity extends BaseActivity implements TaskAddCallback {

    @BindView(R.id.ivSave)
    ImageView ivSave;

    @BindView(R.id.ivBack)
    ImageView ivBack;

    @BindView(R.id.tvTitle)
    CustomTextView tvTitle;

    @BindView(R.id.ivAccountName)
    ImageView ivAccountName;

    @BindView(R.id.tvAssignTo)
    CustomEditText tvAssignTo;

    @BindView(R.id.tvAccountName)
    CustomEditText tvAccountName;

    @BindView(R.id.etSubject)
    CustomEditText etSubject;

//    @BindView(R.id.tilSubject)
//    TextInputLayout tilSubject;

    @BindView(R.id.tvCloseDate)
    CustomTextView tvCloseDate;

    @BindView(R.id.etDesc)
    CustomEditText etDesc;

//    @BindView(R.id.tilDesc)
//    TextInputLayout tilDesc;
    
    @BindView(R.id.spPriority)
    Spinner spPriority;
    
    @BindView(R.id.spTaskStatus)
    Spinner spTaskStatus;

    private Context context;
    private DatePickerFragment datepicker;
    private TaskAddPresenter presenter;
    private String url="", contact_json, account_id = "0", assign_id = "0";
    private String open_date = "", string_year, string_month, string_day, index_priority="0", index_status="0";
    private int year, month, day;
    private boolean from_detail = false;
    private String[] priority = {"Priority", "Urgen", "High", "Medium", "Low"}, task_status = {"Task Status", "Not Started", "Pending", "In Progress", "Completed", "Waiting for Someone else"};
    private TaskModel model;

    static TaskAddActivity taskAddActivity;

    public static TaskAddActivity getInstance(){
        return taskAddActivity;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_taskadd);

        ButterKnife.bind(this);
        this.context = this;

        taskAddActivity = this;

        this.presenter = new TaskAddPresenter(context, this);

        try {
            from_detail = getIntent().getExtras().getBoolean("from_detail");
        }catch (Exception e){}

        try {
            model = getIntent().getExtras().getParcelable("taskModel");
        }catch (Exception e){}


        datepicker = new DatePickerFragment();
        datepicker.setListener(dateListener);

        initDate();
        initView();
    }

    private void initDate() {
        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH) + 1;
        day = c.get(Calendar.DAY_OF_MONTH);
    }

    private void initView(){
        try {
            tvCloseDate.setText(getIntent().getExtras().getString("date"));
        }catch (Exception e){
            tvCloseDate.setText("Date");
        }

        tvAssignTo.setText(PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.KEY_FULLNAME));
        assign_id = PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.KEY_USERID);

        getSpinnerAdapter();

        if (from_detail){
            tvTitle.setText("Edit Task");
            assign_id = model.getAssignto();
            account_id = model.getAccount_id();
            spPriority.setSelection(Integer.parseInt(model.getPriority_id()));
            spTaskStatus.setSelection(Integer.parseInt(model.getTask_status_id()));
            tvAssignTo.setText(model.getAssignto_name());
            tvAccountName.setText(model.getAccount_name());
            tvCloseDate.setText(model.getDate());
            etSubject.setText(model.getSubject());
            etDesc.setText(model.getDescription());
        }

        ivBack.setOnClickListener(click);
        ivSave.setOnClickListener(click);
        tvAccountName.setOnClickListener(click);
        ivAccountName.setOnClickListener(click);
        tvCloseDate.setOnClickListener(click);
        tvAssignTo.setOnClickListener(click);
    }

    private void getSpinnerAdapter(){
        SpinnerCustomAdapter spinnerCustomAdapter = new SpinnerCustomAdapter(context,
                R.layout.spinner_default, priority);
        spinnerCustomAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spPriority.setAdapter(spinnerCustomAdapter);
        spPriority.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                if (position != 0){
                    index_priority = String.valueOf(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        SpinnerCustomAdapter spinnerAdapter = new SpinnerCustomAdapter(context,
                R.layout.spinner_default, task_status);
        spinnerAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spTaskStatus.setAdapter(spinnerAdapter);
        spTaskStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                if (position != 0){
                    index_status = String.valueOf(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.ivBack:
                    final CustomDialog dialogback = CustomDialog.setupDialogConfirmation(context, "Anda ingin kembali? Data yang anda masukkan akan hilang?");
                    CustomTextView tvCancel = (CustomTextView) dialogback.findViewById(R.id.tvCancel);
                    CustomTextView tvYa = (CustomTextView) dialogback.findViewById(R.id.tvOK);

                    tvCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialogback.dismiss();
                        }
                    });

                    tvYa.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            finish();
                            dialogback.dismiss();
                        }
                    });
                    dialogback.show();
                    break;
                case R.id.ivSave:
                    if (checkField()){
                        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(context)){
                            if (from_detail){
                                model.setAccount_id(account_id);
                                model.setAssignto(assign_id);
                                model.setSubject(etSubject.getText().toString());
                                model.setDate(tvCloseDate.getText().toString());
                                model.setDescription(etDesc.getText().toString());
                                model.setCreated_at(getCurrentDate());
                                model.setPriority_id(index_priority);
                                model.setTask_status_id(index_status);

                                showLoadingDialog();

                                presenter.setupEdit(ApiParam.API_072, model);
                            }else{
                                TaskModel taskModel =  new TaskModel();
                                taskModel.setAccount_id(account_id);
                                taskModel.setAssignto(assign_id);
                                taskModel.setSubject(etSubject.getText().toString());
                                taskModel.setDate(tvCloseDate.getText().toString());
                                taskModel.setDescription(etDesc.getText().toString());
                                taskModel.setCreated_at(getCurrentDate());
                                taskModel.setPriority_id(index_priority);
                                taskModel.setTask_status_id(index_status);

                                showLoadingDialog();

                                presenter.setupAdd(ApiParam.API_071, taskModel);
                            }

                        }else{

                        }
                    }
                    break;
                case R.id.tvAccountName:
                    AccountDialogFragment accountDialogFragment = new AccountDialogFragment(TaskAddActivity.this, new AccountDialogCallback() {

                        @Override
                        public void onAccountDialogCallback(Bundle data) {
                            tvAccountName.setText(data.getString("name"));
                            contact_json = data.getString("pic");
                            account_id = data.getString("id");
                        }
                    });
                    accountDialogFragment.show(getSupportFragmentManager(), "accountDialog");
                    break;
                case R.id.ivAccountName:
                    AccountDialogFragment DialogFragment = new AccountDialogFragment(TaskAddActivity.this, new AccountDialogCallback() {

                        @Override
                        public void onAccountDialogCallback(Bundle data) {
                            tvAccountName.setText(data.getString("name"));
                            contact_json = data.getString("pic");
                            account_id = data.getString("id");

                            Log.d("TAG", "Contact JSON : " + contact_json);
                        }
                    });
                    DialogFragment.show(getSupportFragmentManager(), "accountDialog");
                    break;
                case R.id.tvCloseDate:
                    datepicker.setDateFragment(year, month, day);
                    datepicker.show(getFragmentManager(), "datePicker");
                    break;
                case R.id.tvAssignTo:
                    UserDialogFragment userFragment = new UserDialogFragment(TaskAddActivity.this, new UserDialogCallback() {

                        @Override
                        public void onUserDialogCallback(Bundle data) {
                            tvAssignTo.setText(data.getString("name"));
                            assign_id = data.getString("id");
                        }
                    });
                    userFragment.show(getSupportFragmentManager(), "UserFragment");
                    break;
            }
        }
    };

    private boolean checkField(){
        if (account_id.equalsIgnoreCase("")) {
            tvAccountName.setError("Nama Akun wajib diisi");
            return false;
        }else if (etSubject.getText().toString().equalsIgnoreCase("")){
            etSubject.setError("Subject Harus Diisi !!!");
            return false;
        }else if (tvCloseDate.getText().toString().equalsIgnoreCase("")){
            tvCloseDate.setError("the Closing Date cannot be Empty !!!");
            return false;
        }
        return true;
    }

    DatePickerDialog.OnDateSetListener dateListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int r_year, int monthOfYear,
                              int dayOfMonth) {
            year = r_year;
            month = monthOfYear + 1;
            day = dayOfMonth;

            string_year = r_year + "";

            if (month < 10) {

                string_month = "0" + month;
            } else {
                string_month = month + "";
            }
            if (day < 10) {

                string_day = "0" + day;
            } else {
                string_day = day + "";
            }
            open_date = year + "-" + string_month + "-" + string_day;

            tvCloseDate.setText(year + "-" + string_month + "-" + string_day);
        }
    };

    @Override
    public void finishAdd(String result, String response) {
        dismissLoadingDialog();
        if (result.equalsIgnoreCase("OK")){
            finish();
        }
    }

    @Override
    public void finishEdit(String result, String response) {
        dismissLoadingDialog();
        if (result.equalsIgnoreCase("OK")){
            finish();
            TaskDetailActivity.getInstance().finish();
        }
    }

    private String getCurrentDate() {
        Calendar c = Calendar.getInstance();

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        return df.format(c.getTime());
    }
}
