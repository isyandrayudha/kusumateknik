package com.msf.project.sales1crm.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.msf.project.sales1crm.model.ReasonModel;

import java.util.ArrayList;
import java.util.List;

public class ReasonDao extends BaseDao<ReasonModel> implements DBSchema.Reason {
    public ReasonDao(Context c) {
        super(c, TABLE_NAME);
    }

    public ReasonDao(Context c, String table, boolean willWrite) {
        super(c, table, willWrite);
    }

    public ReasonDao(DBHelper dbHelper) {
        super(dbHelper);
    }

    public ReasonDao(DBHelper dbHelper, String table) {
        super(dbHelper, table);
    }

    public ReasonDao(DBHelper dbHelper, String table, boolean willWrite) {
        super(dbHelper, table, willWrite);
    }

    public ReasonDao(Context c, DBHelper db, String table, boolean willWrite) {
        super(c, db, table, willWrite);
    }

    @Override
    public ReasonModel getById(int id) {
        String qry = "SELECT * FROM " + getTable() + " WHERE " + COL_ID + " = " + id;
        Cursor c = getSqliteDb().rawQuery(qry,null);
        ReasonModel model = new ReasonModel();
        try {
            if(c != null && c.moveToFirst()) {
                model = getByCursor(c);
            }
        } finally {
            c.close();
        }
        return model;
    }

    public List<ReasonModel>getReasonList(){
        String query = "SELECT * FROM " + getTable() + " WHERE " + DBSchema.Reason.KEY_STAT + " = '1'" ;
        Cursor c = getSqliteDb().rawQuery(query, null);
        List<ReasonModel> list = new ArrayList<>();
        try{
            if(c !=null && c.moveToFirst()){
                list.add(getByCursor(c));
                while (c.moveToNext()){
                    list.add(getByCursor(c));
                }
            }
        } finally {
            c.close();
        }
        return list;
    }


    @Override
    public ReasonModel getByCursor(Cursor c) {
        ReasonModel model = new ReasonModel();
        model.setId(c.getString(0));
        model.setReason(c.getString(1));
        model.setStat(c.getString(2));
        return model;
    }

    @Override
    protected ContentValues upDataValues(ReasonModel reasonModel, boolean update) {
        ContentValues cv = new ContentValues();
        cv.put(KEY_ID, reasonModel.getId());
        cv.put(KEY_REASON, reasonModel.getReason());
        cv.put(KEY_STAT, reasonModel.getStat());
        return cv;
    }
}
