package com.msf.project.sales1crm.fragment;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.ListView;

import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.adapter.OpportunityDurationAdapter;
import com.msf.project.sales1crm.adapter.Top10CustomerAdapter;
import com.msf.project.sales1crm.callback.GeneralDashboardCallback;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.customview.DatePickerFragment;
import com.msf.project.sales1crm.model.SalesCycleModel;
import com.msf.project.sales1crm.model.Top10CustomerModel;
import com.msf.project.sales1crm.model.TotalModel;
import com.msf.project.sales1crm.presenter.ViewDashboardGeneralDashboardPresenter;
import com.msf.project.sales1crm.service.GPSTracker;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ViewDashboardGeneralDashboardFragment extends Fragment implements GeneralDashboardCallback {

    @BindView(R.id.tvStartDate)
    CustomTextView tvStartDate;

    @BindView(R.id.tvEndDate)
    CustomTextView tvEndDate;

    @BindView(R.id.lvCloseWonIndustry)
    ListView lvCloseWonIndustry;

    @BindView(R.id.lvCloseLoseIndustry)
    ListView lvCloseLoseIndustry;

    @BindView(R.id.lvCloseWonSource)
    ListView lvCloseWonSource;

    @BindView(R.id.lvCloseLoseSource)
    ListView lvCloseLoseSource;

    @BindView(R.id.tvConversion)
    CustomTextView tvConversion;

    @BindView(R.id.tvConversionRate)
    CustomTextView tvConversionRate;

    @BindView(R.id.tvTotalLeads)
    CustomTextView tvTotalLeads;

    @BindView(R.id.tvTotalOppor)
    CustomTextView tvTotalOppor;

    @BindView(R.id.tvCloseWon)
    CustomTextView tvCloseWon;

    @BindView(R.id.tvCloseLose)
    CustomTextView tvCloseLose;

    @BindView(R.id.tvTotalSales)
    CustomTextView tvTotalSales;

    @BindView(R.id.tvTotalAvgSales)
    CustomTextView tvTotalAvgSales;

    @BindView(R.id.lvTop10)
    ListView lvTop10;

    @BindView(R.id.tvSubmit)
    CustomTextView tvSubmit;

    private View view;
    private Context context;
    private ViewDashboardGeneralDashboardPresenter presenter;
    private OpportunityDurationAdapter adapter, adapter1, adapter2, adapter3;
    private Top10CustomerAdapter customerAdapter;
    private List<TotalModel> list = new ArrayList<>();
    private List<TotalModel> list1 = new ArrayList<>();
    private String startDate = "", endDate = "";
    private DatePickerFragment datepicker1, datepicker2;
    private String open_date = "", string_year, string_month, string_day;
    private int year, month, day;

    private DecimalFormat df = (DecimalFormat) DecimalFormat.getCurrencyInstance();
    private DecimalFormatSymbols dfs = new DecimalFormatSymbols();

    public static ViewDashboardGeneralDashboardFragment newInstance() {
        ViewDashboardGeneralDashboardFragment fragment = new ViewDashboardGeneralDashboardFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_generaldashboard, container, false);

        return this.view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);
        this.context = getActivity();

        this.presenter = new ViewDashboardGeneralDashboardPresenter(context, this);

        datepicker1 = new DatePickerFragment();
        datepicker1.setListener(dateListener1);

        datepicker2 = new DatePickerFragment();
        datepicker2.setListener(dateListener2);

        initDate();
        initData();
    }

    private void initDate() {
        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH) + 1;
        day = c.get(Calendar.DAY_OF_MONTH);
    }

    private void initData(){
        tvStartDate.setOnClickListener(click);
        tvEndDate.setOnClickListener(click);
        tvSubmit.setOnClickListener(click);
    }

    public void getDataFromAPI(){
        if (Sales1CRMUtils.ConnectionUtility.isNetworkConnected(context)){
            GPSTracker gpsTracker = new GPSTracker(context);
            if (gpsTracker.canGetLocation()) {
                presenter.setupTotal(ApiParam.API_104, startDate, endDate);
            } else {
                // can't get location
                // GPS or Network is not enabled
                // Ask user to enable GPS/network in settings
                gpsTracker.showSettingsAlert();
            }
        }
    }

    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.tvSubmit:
                    getDataFromAPI();
                    break;
                case R.id.tvStartDate:
                    datepicker1.setDateFragment(year, month, day);
                    datepicker1.show(getActivity().getFragmentManager(), "datePicker");
                    break;
                case R.id.tvEndDate:
                    datepicker2.setDateFragment(year, month, day);
                    datepicker2.show(getActivity().getFragmentManager(), "datePicker");
                    break;
            }
        }
    };

    @Override
    public void finished(String result, SalesCycleModel model, List<TotalModel> wonIndustry, List<TotalModel> loseIndustry, List<TotalModel> wonSource, List<TotalModel> loseSource, List<Top10CustomerModel> listTop10) {
        if (result.equalsIgnoreCase("OK")){
            df.setDecimalFormatSymbols(dfs);

            tvConversion.setText(model.getTotal_conversion());
            tvConversionRate.setText(model.getTotal_conversionrate());
            tvTotalOppor.setText(model.getTotal_oppor());
            tvTotalLeads.setText(model.getTotal_lead());
            tvCloseWon.setText(model.getTotal_closewon());
            tvCloseLose.setText(model.getTotal_closelose());
            tvTotalSales.setText(df.format(Double.parseDouble(model.getTotal_sales())));
            tvTotalAvgSales.setText(df.format(Double.parseDouble(model.getAvg_sales())));

            if (wonIndustry.size() > 0){
                adapter = new OpportunityDurationAdapter(context, wonIndustry);
                lvCloseWonIndustry.setAdapter(adapter);
                lvCloseWonIndustry.setSelector(R.drawable.transparent_selector);
                lvCloseWonIndustry.setOverScrollMode(View.OVER_SCROLL_NEVER);
                lvCloseWonIndustry.setOnTouchListener(new View.OnTouchListener() {
                    // Setting on Touch Listener for handling the touch inside
                    // ScrollView
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        // Disallow the touch request for parent scroll on touch of
                        // child view
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        return false;
                    }
                });
                lvCloseWonIndustry.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                    }
                });
            }

            if (loseIndustry.size() > 0){
                adapter1 = new OpportunityDurationAdapter(context, loseIndustry);
                lvCloseLoseIndustry.setAdapter(adapter1);
                lvCloseLoseIndustry.setSelector(R.drawable.transparent_selector);
                lvCloseLoseIndustry.setOverScrollMode(View.OVER_SCROLL_NEVER);
                lvCloseLoseIndustry.setOnTouchListener(new View.OnTouchListener() {
                    // Setting on Touch Listener for handling the touch inside
                    // ScrollView
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        // Disallow the touch request for parent scroll on touch of
                        // child view
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        return false;
                    }
                });
                lvCloseLoseIndustry.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                    }
                });
            }

            if (wonSource.size() > 0){
                adapter2 = new OpportunityDurationAdapter(context, wonSource);
                lvCloseWonSource.setAdapter(adapter2);
                lvCloseWonSource.setSelector(R.drawable.transparent_selector);
                lvCloseWonSource.setOverScrollMode(View.OVER_SCROLL_NEVER);
                lvCloseWonSource.setOnTouchListener(new View.OnTouchListener() {
                    // Setting on Touch Listener for handling the touch inside
                    // ScrollView
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        // Disallow the touch request for parent scroll on touch of
                        // child view
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        return false;
                    }
                });
                lvCloseWonSource.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                    }
                });
            }

            if (loseSource.size() > 0){
                adapter3 = new OpportunityDurationAdapter(context, loseSource);
                lvCloseLoseSource.setAdapter(adapter3);
                lvCloseLoseSource.setSelector(R.drawable.transparent_selector);
                lvCloseLoseSource.setOverScrollMode(View.OVER_SCROLL_NEVER);
                lvCloseLoseSource.setOnTouchListener(new View.OnTouchListener() {
                    // Setting on Touch Listener for handling the touch inside
                    // ScrollView
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        // Disallow the touch request for parent scroll on touch of
                        // child view
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        return false;
                    }
                });
                lvCloseLoseSource.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                    }
                });
            }

            if (listTop10.size() > 0){
                customerAdapter = new Top10CustomerAdapter(context, listTop10);
                lvTop10.setAdapter(customerAdapter);
                lvTop10.setSelector(R.drawable.transparent_selector);
                lvTop10.setOverScrollMode(View.OVER_SCROLL_NEVER);
                lvTop10.setOnTouchListener(new View.OnTouchListener() {
                    // Setting on Touch Listener for handling the touch inside
                    // ScrollView
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        // Disallow the touch request for parent scroll on touch of
                        // child view
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        return false;
                    }
                });
                lvTop10.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                    }
                });
            }
        }
    }

    DatePickerDialog.OnDateSetListener dateListener1 = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int r_year, int monthOfYear,
                              int dayOfMonth) {
            year = r_year;
            month = monthOfYear + 1;
            day = dayOfMonth;

            string_year = r_year + "";

            if (month < 10) {

                string_month = "0" + month;
            } else {
                string_month = month + "";
            }
            if (day < 10) {

                string_day = "0" + day;
            } else {
                string_day = day + "";
            }
            startDate = year + "-" + string_month + "-" + string_day;

            tvStartDate.setText(year + "-" + string_month + "-" + string_day);
        }
    };

    DatePickerDialog.OnDateSetListener dateListener2 = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int r_year, int monthOfYear,
                              int dayOfMonth) {
            year = r_year;
            month = monthOfYear + 1;
            day = dayOfMonth;

            string_year = r_year + "";

            if (month < 10) {

                string_month = "0" + month;
            } else {
                string_month = month + "";
            }
            if (day < 10) {

                string_day = "0" + day;
            } else {
                string_day = day + "";
            }
            endDate = year + "-" + string_month + "-" + string_day;

            tvEndDate.setText(year + "-" + string_month + "-" + string_day);
        }
    };
}
