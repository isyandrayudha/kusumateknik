package com.msf.project.sales1crm.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.msf.project.sales1crm.AEventDetailActivity;
import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.database.MeeToSelectDao;
import com.msf.project.sales1crm.model.ActivityEventModel;

import java.util.List;

public class ActivityEventTodayAdapter extends RecyclerView.Adapter<ActivityEventTodayAdapter.ActivityEventTodayHolder> {

    private Context context;
    private Bitmap icon;
    private String url;
    private List<ActivityEventModel> activityEventTodayModelList;

    public ActivityEventTodayAdapter(Context context, List<ActivityEventModel> object){
        this.context = context;
        this.activityEventTodayModelList = object;

    }

    public static abstract class Row {
    }

    public static final class Section extends Row {
        public final String text;

        public Section(String text) {
            this.text = text;
        }
    }

    public static final class Item extends Row {
        public final ActivityEventModel text;

        public Item(ActivityEventModel text) {
            this.text = text;
        }

        public ActivityEventModel getItem() {
            return text;
        }
    }

    private List<Row> rows;

    public void setRows(List<Row> rows) {
        this.rows = rows;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return rows.size();
    }

    @NonNull
    @Override
    public ActivityEventTodayHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_activity_eventtoday,null);
        return new ActivityEventTodayHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ActivityEventTodayHolder holder, int position) {
        final Item item = (Item) rows.get(position);
        holder.tvEventName.setText(item.text.getName());
        holder.tvAccountName.setText(item.text.getAccount_name());
        holder.tvAssignTo.setText(item.text.getUser_name());
        holder.tvDescription.setText(item.text.getDescription());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    Intent intent = new Intent(context, AEventDetailActivity.class);
                    intent.putExtra("aEventModels", item.getItem());
                MeeToSelectDao dao = new MeeToSelectDao(context);
                dao.deleteAllRecord();
                    context.startActivity(intent);
            }
        });
    }



    public class ActivityEventTodayHolder extends RecyclerView.ViewHolder {
        CustomTextView tvEventName;
        CustomTextView tvAccountName;
        CustomTextView tvAssignTo;
        CustomTextView tvDescription;
        public ActivityEventTodayHolder(View view) {
            super(view);
            tvEventName = (CustomTextView) view.findViewById(R.id.tvEventName);
            tvAccountName = (CustomTextView) view.findViewById(R.id.tvAccountName);
            tvAssignTo = (CustomTextView) view.findViewById(R.id.tvAssignTo);
            tvDescription = (CustomTextView) view.findViewById(R.id.tvDescription);
        }
    }
}
