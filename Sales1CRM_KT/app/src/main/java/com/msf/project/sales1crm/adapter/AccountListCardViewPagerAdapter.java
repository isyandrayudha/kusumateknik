package com.msf.project.sales1crm.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.msf.project.sales1crm.AccountDetailActivity;
import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.model.AccountModel;
import com.msf.project.sales1crm.utility.PreferenceUtility;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


public class AccountListCardViewPagerAdapter extends PagerAdapter {

    private Context context;
    private String url;
    private List<AccountModel> listData;

    public AccountListCardViewPagerAdapter(Context context, List<AccountModel> objects) {
        this.context = context;
        this.listData = objects;

        this.url = PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.URL);
    }

    public static abstract class Row {
    }


    public static final class Section extends Row {
        public final String text;

        public Section(String text) {
            this.text = text;
        }
    }

    // This is for making Row, ex: A B C D etc
    public static final class Item extends Row {
        public final AccountModel text;

        public Item(AccountModel text) {
            this.text = text;
        }

        public AccountModel getItem() {
            return text;
        }
    }

    private List<Row> rows;

    public void setRows(List<Row> rows) {
        this.rows = rows;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return rows.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        final int pos = position ;
        final ViewHolder holder;
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.card_account, container, false);
        holder = new ViewHolder();
        holder.ivImage = (CircleImageView) view.findViewById(R.id.ivImage);
        holder.tvAccountName = (CustomTextView) view.findViewById(R.id.tvAccountName);
        holder.tvindustry = (CustomTextView) view.findViewById(R.id.tvindustry);
        holder.tvindustryy = (CustomTextView) view.findViewById(R.id.tvindustryy);
        holder.tvType = (CustomTextView) view.findViewById(R.id.tvType);
        holder.tvOwnerName = (CustomTextView) view.findViewById(R.id.tvOwnerName);
        holder.tvtelephone = (CustomTextView) view.findViewById(R.id.tvtelephone);
        holder.tvCountEvent = (CustomTextView) view.findViewById(R.id.tvCountEvent);
        holder.tvCountOpportunity = (CustomTextView) view.findViewById(R.id.tvCountOpportunity);
        holder.tvCountTask = (CustomTextView) view.findViewById(R.id.tvCountTask);
        holder.cv = (CardView) view.findViewById(R.id.cv);

        Sales1CRMUtils.Utils.imageLoader.DisplayImage(url + listData.get(pos).getImageUrl(),
                holder.ivImage, Bitmap.Config.ARGB_8888, 100, Sales1CRMUtils.TYPE_RECT);
        holder.tvAccountName.setText(listData.get(pos).getAccount_name());
        holder.tvindustry.setText(listData.get(pos).getIndustry_name());
        holder.tvindustryy.setText(listData.get(pos).getIndustry_name());
        holder.tvType.setText(listData.get(pos).getType_name());
        holder.tvOwnerName.setText(listData.get(pos).getOwner_name());
        holder.tvtelephone.setText(listData.get(pos).getPhone());
        holder.tvCountOpportunity.setText(listData.get(pos).getCount_opportunity());
        holder.tvCountTask.setText(listData.get(pos).getCount_task());
        holder.tvCountEvent.setText(listData.get(pos).getCount_event());
        holder.cv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    Intent intent = new Intent(context, AccountDetailActivity.class);
                    intent.putExtra("accountModel", listData.get(pos));

                    context.startActivity(intent);
            }
        });
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    private static class ViewHolder {
        CircleImageView ivImage;
        CustomTextView tvAccountName;
        CustomTextView tvindustry;
        CustomTextView tvindustryy;
        CustomTextView tvType;
        CustomTextView tvOwnerName;
        CustomTextView tvtelephone;
        LinearLayout llCard;
        CustomTextView tvCountOpportunity;
        CustomTextView tvCountTask;
        CustomTextView tvCountEvent;
        CardView cv;
    }
}
