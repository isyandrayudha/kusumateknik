package com.msf.project.sales1crm.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.msf.project.sales1crm.model.NoOrderModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by christianmacbook on 25/05/18.
 */

public class NoOrderDao extends BaseDao<NoOrderModel> implements DBSchema.NoOrderReason {

    public NoOrderDao(Context c) {
        super(c, TABLE_NAME);
    }

    public NoOrderDao(Context c, boolean willWrite) {
        super(c, TABLE_NAME, willWrite);
    }

    public NoOrderDao(DBHelper db) {
        super(db, TABLE_NAME);
    }

    public NoOrderDao(DBHelper db, boolean willWrite) {
        super(db, TABLE_NAME, willWrite);
    }

    public NoOrderModel getById(int id) {
        String qry = "SELECT * FROM " + getTable() + " WHERE " + COL_ID + " = " + id;
        Cursor c = getSqliteDb().rawQuery(qry, null);
        NoOrderModel model = new NoOrderModel();
        try {
            if(c != null && c.moveToFirst()) {
                model = getByCursor(c);
            }
        } finally {
            c.close();
        }
        return model;
    }

    public List<NoOrderModel> getNoVisitList() {
        String query = "SELECT * FROM " + getTable();

        Cursor c = getSqliteDb().rawQuery(query, null);
        List<NoOrderModel> list = new ArrayList<>();
        try {
            if(c != null && c.moveToFirst()) {
                list.add(getByCursor(c));
                while (c.moveToNext()) {
                    list.add(getByCursor(c));

                }
            }
        } finally {
            c.close();
        }
        return list;
    }

    @Override
    public NoOrderModel getByCursor(Cursor c) {
        NoOrderModel model = new NoOrderModel();
        model.setId(c.getString(0));
        model.setName(c.getString(1));
        return model;
    }

    @Override
    protected ContentValues upDataValues(NoOrderModel model, boolean update) {
        ContentValues cv = new ContentValues();
        if (update == true)
            cv.put(COL_ID, model.getId());
        cv.put(KEY_NAME, model.getName());
        return cv;
    }
}
