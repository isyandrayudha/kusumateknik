package com.msf.project.sales1crm.callback;

public interface DomainCheckCallback {
    void finishedDomain(String result, String response);
}
