package com.msf.project.sales1crm.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.msf.project.sales1crm.fragment.ViewReportAccountEventFragment;
import com.msf.project.sales1crm.fragment.ViewReportAccountTaskFragment;
import com.msf.project.sales1crm.fragment.ViewReportLeadConversionFragment;
import com.msf.project.sales1crm.fragment.ViewReportLeadStatusFragment;
import com.msf.project.sales1crm.fragment.ViewReportOpportunityConversionFragment;
import com.msf.project.sales1crm.fragment.ViewReportOpportunityDurationFragment;
import com.msf.project.sales1crm.fragment.ViewReportOpportunityIndustryFragment;
import com.msf.project.sales1crm.fragment.ViewReportOpportunitySourceFragment;
import com.msf.project.sales1crm.fragment.ViewReportProductRevenueFragment;
import com.msf.project.sales1crm.fragment.ViewReportProductSoldFragment;
import com.msf.project.sales1crm.fragment.ViewReportSalesCycleFragment;
import com.msf.project.sales1crm.fragment.ViewReportTotalCallsFragment;
import com.msf.project.sales1crm.fragment.ViewReportTotalLeadsFragment;
import com.msf.project.sales1crm.fragment.ViewReportTotalOpportunityFragment;

public class StatsReportTabAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;
    Bundle bundle;

    public StatsReportTabAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                ViewReportTotalLeadsFragment tab1 = new ViewReportTotalLeadsFragment();
                return tab1;
            case 1:
                ViewReportLeadConversionFragment tab2 = new ViewReportLeadConversionFragment();
                return tab2;
            case 2:
                ViewReportTotalOpportunityFragment tab3 = new ViewReportTotalOpportunityFragment();
                return tab3;
            case 3:
                ViewReportOpportunityConversionFragment tab4 = new ViewReportOpportunityConversionFragment();
                return tab4;
            case 4:
                ViewReportLeadStatusFragment tab5 = new ViewReportLeadStatusFragment();
                return tab5;
            case 5:
                ViewReportOpportunityIndustryFragment tab6 = new ViewReportOpportunityIndustryFragment();
                return tab6;
            case 6:
                ViewReportOpportunitySourceFragment tab7 = new ViewReportOpportunitySourceFragment();
                return tab7;
            case 7:
                ViewReportAccountTaskFragment tab8 = new ViewReportAccountTaskFragment();
                return tab8;
            case 8:
                ViewReportAccountEventFragment tab9 = new ViewReportAccountEventFragment();
                return tab9;
            case 9:
                ViewReportProductSoldFragment tab10 = new ViewReportProductSoldFragment();
                return tab10;
            case 10:
                ViewReportProductRevenueFragment tab11 = new ViewReportProductRevenueFragment();
                return tab11;
            case 11:
                ViewReportOpportunityDurationFragment tab12 = new ViewReportOpportunityDurationFragment();
                return tab12;
            case 12:
                ViewReportSalesCycleFragment tab13 = new ViewReportSalesCycleFragment();
                return tab13;
            case 13:
                ViewReportTotalCallsFragment tab14 = new ViewReportTotalCallsFragment();
                return tab14;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
