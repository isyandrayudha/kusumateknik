package com.msf.project.sales1crm.customview;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.EditText;

import com.msf.project.sales1crm.callback.CustomEditTextCallback;

/**
 * Created by christianmacbook on 11/05/18.
 */

public class CustomEditText extends EditText {

    private CustomEditTextCallback callbackBack = null;
    private static final String TAG = "EditText";

    public CustomEditText(Context context) {
        super(context);
        // TODO Auto-generated constructor stub
        // this.setOnEditorActionListener(this);
        setCustomFont(context);
    }

    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        // TODO Auto-generated constructor stub
        // this.setOnEditorActionListener(this);

        setCustomFont(context);
    }

    public CustomEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setCustomFont(context);
    }

    public boolean setCustomFont(Context ctx) {
        Typeface tf = null;
        try {
            tf = Typeface.createFromAsset(ctx.getAssets(), "OpenSans-Regular.ttf");
        } catch (Exception e) {
            Log.e(TAG, "Could not get typeface: " + e.getMessage());
            return false;
        }

        setTypeface(tf);
        return true;
    }

    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            // this.setCursorVisible(false);
            if (getCallbackBack() != null) {
                callbackBack.setOnBackPressed();
            }
        }
        return super.onKeyPreIme(keyCode, event);

    }

    public CustomEditTextCallback getCallbackBack() {
        return callbackBack;
    }

    public void setCallbackBack(CustomEditTextCallback callbackBack) {
        this.callbackBack = callbackBack;
    }
}
