package com.msf.project.sales1crm.model;

import java.util.List;

public class ToDoList{
	private List<Object> today;
	private List<Object> list;

	public void setToday(List<Object> today){
		this.today = today;
	}

	public List<Object> getToday(){
		return today;
	}

	public void setList(List<Object> list){
		this.list = list;
	}

	public List<Object> getList(){
		return list;
	}
}