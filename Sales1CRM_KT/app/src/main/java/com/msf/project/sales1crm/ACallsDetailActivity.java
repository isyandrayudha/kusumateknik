package com.msf.project.sales1crm;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.msf.project.sales1crm.callback.DashboardCallback;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.model.AbsenceModel;
import com.msf.project.sales1crm.model.ActivityCallsModel;
import com.msf.project.sales1crm.model.DashboardFirstModel;
import com.msf.project.sales1crm.model.DashboardSecondModel;
import com.msf.project.sales1crm.model.DashboardThirdModel;
import com.msf.project.sales1crm.model.TotalOpportunityModel;
import com.msf.project.sales1crm.presenter.DashboardPresenter;
import com.msf.project.sales1crm.utility.ApiParam;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ACallsDetailActivity extends BaseActivity implements DashboardCallback {

    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.tvSubjectTitle)
    CustomTextView tvSubjectTitle;
    @BindView(R.id.ivEdit)
    ImageView ivEdit;
    @BindView(R.id.tvCreateDate)
    CustomTextView tvCreateDate;
    @BindView(R.id.tvAccountName)
    CustomTextView tvAccountName;
    @BindView(R.id.tvSubject)
    CustomTextView tvSubject;
    @BindView(R.id.tvDate)
    CustomTextView tvDate;
    @BindView(R.id.tvDuration)
    CustomTextView tvDuration;
    @BindView(R.id.tvPurpose)
    CustomTextView tvPurpose;
    @BindView(R.id.tvType)
    CustomTextView tvType;
    @BindView(R.id.tvDetail)
    CustomTextView tvDetail;

    private Context context;
    private ActivityCallsModel aCallModels;
    static ACallsDetailActivity callsDetailActivity;
    private DashboardPresenter dashboardPresenter;

    public static ACallsDetailActivity getInstance() {
        return callsDetailActivity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acalls_detail);

        ButterKnife.bind(this);
        this.context = this;
        this.dashboardPresenter = new DashboardPresenter(context, this);
        callsDetailActivity = this;

        try {
            aCallModels = getIntent().getExtras().getParcelable("aCallsModel");
        } catch (Exception e) {

        }
        initviewCalls();
    }

        private void initviewCalls() {
            dashboardPresenter.setupFirst(ApiParam.API_005);
            tvSubjectTitle.setText(aCallModels.getSubject());
            tvSubject.setText(aCallModels.getSubject());
            tvAccountName.setText(aCallModels.getAccount_name());
            tvCreateDate.setText("Created at " + aCallModels.getCreatedAt());
            tvDate.setText(aCallModels.getDate_interval());
            tvType.setText(aCallModels.getCall_type_name());
            tvDuration.setText(aCallModels.getDuration_interval());
            tvPurpose.setText(aCallModels.getCall_purpose_name());
            tvDetail.setText(aCallModels.getDetail());

            ivBack.setOnClickListener(click);

        }

        View.OnClickListener click = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (view.getId()){
                    case R.id.ivBack:
                        finish();
                        break;

                }
            }
        };

    @Override
    public void finishFirst(String result, DashboardFirstModel model) {
        if(result.equalsIgnoreCase("OK")){
            if(model.getCrm_call_edit_permission().equalsIgnoreCase("1")){
                ivEdit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                            Intent intent = new Intent(context, ACallAddActivity.class);
                            intent.putExtra("aCallsModels", aCallModels);
                            intent.putExtra("from_detail_calls", true);
                            startActivity(intent);
                    }
                });
                ivEdit.setVisibility(View.VISIBLE);
            } else {
                ivEdit.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void finishSecond(String result, DashboardSecondModel model) {

    }

    @Override
    public void finishThird(String result, List<DashboardThirdModel> listLead, List<DashboardThirdModel> listLead1, List<DashboardThirdModel> listOppor, List<DashboardThirdModel> listOppor1) {

    }

    @Override
    public void TotOpportunity(String result, TotalOpportunityModel totOporModel) {

    }

    @Override
    public void absence(String result, AbsenceModel model) {

    }
}
