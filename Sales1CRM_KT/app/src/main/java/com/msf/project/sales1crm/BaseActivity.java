package com.msf.project.sales1crm;

import android.support.v7.app.AppCompatActivity;

import com.msf.project.sales1crm.utility.CustomLoadingCRM;

/**
 * Created by christianmacbook on 11/05/18.
 */

public class BaseActivity extends AppCompatActivity {
    private CustomLoadingCRM dialog;

    public void showLoadingDialog() {
        dialog = new CustomLoadingCRM(this);
        dialog.show(this.getSupportFragmentManager(), "customLoadingDialog");
    }

    public void dismissLoadingDialog() {
        if (null == dialog) return;
        dialog.dismiss();
    }
}
