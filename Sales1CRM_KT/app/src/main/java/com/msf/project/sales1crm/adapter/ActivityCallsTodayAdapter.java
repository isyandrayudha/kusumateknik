package com.msf.project.sales1crm.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.msf.project.sales1crm.ACallsDetailActivity;
import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.model.ActivityCallsModel;

import java.util.List;

import butterknife.BindView;

public class ActivityCallsTodayAdapter extends RecyclerView.Adapter<ActivityCallsTodayAdapter.CallsTodayHolder> {


    private Context context;
    private List<ActivityCallsModel> aCallsModels;

    public ActivityCallsTodayAdapter(Context context, List<ActivityCallsModel> object) {
        this.context = context;
        this.aCallsModels = object;
    }

    public static abstract class Row {

    }

    public static final class Section extends Row {
        public final String text;

        public Section(String text) {
            this.text = text;
        }
    }

    public static final class Item extends Row {
        public final ActivityCallsModel text;


        public Item(ActivityCallsModel text) {
            this.text = text;
        }

        public ActivityCallsModel getItem() {
            return text;
        }
    }

    private List<Row> rows;

    public void setRows(List<Row> rows) {
        this.rows = rows;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return rows.size();
    }

    @NonNull
    @Override
    public CallsTodayHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_activity_calls_today, null);
        return new CallsTodayHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CallsTodayHolder holder, int position) {
        final Item item = (Item) rows.get(position);
        holder.tvAccountName.setText(item.text.getAccount_name());
        holder.tvSubject.setText(item.text.getSubject());
        holder.tvDuration.setText(item.text.getDuration_interval());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ACallsDetailActivity.class);
                intent.putExtra("aCallsModel",item.getItem());
                context.startActivity(intent);
            }
        });

    }


    public class CallsTodayHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvAccountName)
        CustomTextView tvAccountName;
        @BindView(R.id.tvSubject)
        CustomTextView tvSubject;
        @BindView(R.id.tvDuration)
        CustomTextView tvDuration;
        public CallsTodayHolder(View itemView) {
            super(itemView);
            tvAccountName = (CustomTextView) itemView.findViewById(R.id.tvAccountName);
            tvSubject = (CustomTextView) itemView.findViewById(R.id.tvSubject);
            tvDuration = (CustomTextView) itemView.findViewById(R.id.tvDuration);
        }
    }
}
