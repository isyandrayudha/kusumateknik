package com.msf.project.sales1crm.callback;

import com.msf.project.sales1crm.model.ReasonModel;

import java.util.List;

public interface ReasonCallback {
    void finishReason(String result, List<ReasonModel>list);
}
