package com.msf.project.sales1crm;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.msf.project.sales1crm.callback.ConvertCallback;
import com.msf.project.sales1crm.callback.DashboardCallback;
import com.msf.project.sales1crm.customview.CustomDialog;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.model.AbsenceModel;
import com.msf.project.sales1crm.model.DashboardFirstModel;
import com.msf.project.sales1crm.model.DashboardSecondModel;
import com.msf.project.sales1crm.model.DashboardThirdModel;
import com.msf.project.sales1crm.model.LeadConvertModel;
import com.msf.project.sales1crm.model.LeadModel;
import com.msf.project.sales1crm.model.TotalOpportunityModel;
import com.msf.project.sales1crm.presenter.DashboardPresenter;
import com.msf.project.sales1crm.presenter.LeadConvertPresenter;
import com.msf.project.sales1crm.utility.ApiParam;
import com.msf.project.sales1crm.utility.PreferenceUtility;
import com.msf.project.sales1crm.utility.Sales1CRMUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by christianmacbook on 30/05/18.
 */

public class LeadDetailActivity extends BaseActivity implements DashboardCallback, ConvertCallback {

    @BindView(R.id.ivBack)
    ImageView ivBack;

    @BindView(R.id.ivEdit)
    ImageView ivEdit;

    @BindView(R.id.ivDetailPict)
    ImageView ivDetailPict;

    @BindView(R.id.tvIndustry)
    CustomTextView tvIndustry;

    @BindView(R.id.tvLeadName)
    CustomTextView tvLeadName;

    @BindView(R.id.tvPosition)
    CustomTextView tvPosition;

    @BindView(R.id.tvCompany)
    CustomTextView tvCompany;

    @BindView(R.id.tvLastUpdated)
    CustomTextView tvLastUpdated;

    @BindView(R.id.tvCreateDate)
    CustomTextView tvCreateDate;

    @BindView(R.id.tvAssignTo)
    CustomTextView tvAssignTo;

    @BindView(R.id.tvTotalEmployee)
    CustomTextView tvTotalEmployee;

    @BindView(R.id.tvLeadStatus)
    CustomTextView tvLeadStatus;

    @BindView(R.id.tvLeadSource)
    CustomTextView tvLeadSource;

    @BindView(R.id.tvDescription)
    CustomTextView tvDesc;

    @BindView(R.id.llEmail)
    LinearLayout llEmail;

    @BindView(R.id.tvEmail)
    CustomTextView tvEmail;

    @BindView(R.id.llPhone)
    LinearLayout llPhone;

    @BindView(R.id.tvPhone)
    CustomTextView tvPhone;

    @BindView(R.id.tvDistance)
    CustomTextView tvDistance;

    @BindView(R.id.llAlamat)
    LinearLayout llAlamat;

    @BindView(R.id.tvAlamat)
    CustomTextView tvAlamat;

    @BindView(R.id.tvProvinsi)
    CustomTextView tvProvinsi;

    @BindView(R.id.tvKabupaten)
    CustomTextView tvKabupaten;

    @BindView(R.id.tvKecamatan)
    CustomTextView tvKecamatan;

    @BindView(R.id.tvKelurahan)
    CustomTextView tvKelurahan;

    @BindView(R.id.tvKodepos)
    CustomTextView tvKodepos;
    @BindView(R.id.ivConvert)
    ImageView ivConvert;
    @BindView(R.id.llDistanceBilling)
    LinearLayout llDistanceBilling;

    private Context context;
    private LeadModel leadModel;
    private String url = "";
    private LinearLayout llCall, llSMS, llWhatsapp;
    private DashboardPresenter dashboardPresenter;
    private LeadConvertModel leadConvertModel;
    private LeadConvertPresenter leadConvertPresenter;

    static LeadDetailActivity leadDetailActivity;

    public static LeadDetailActivity getInstance() {
        return leadDetailActivity;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leaddetail);

        ButterKnife.bind(this);
        this.context = this;
        this.dashboardPresenter = new DashboardPresenter(context, this);
        this.leadConvertPresenter = new LeadConvertPresenter(context, this);
        leadDetailActivity = this;

        this.url = PreferenceUtility.getInstance().loadDataString(context, PreferenceUtility.URL);

        initView();
    }

    private void initView() {
        dashboardPresenter.setupFirst(ApiParam.API_005);
        try {
            leadModel = getIntent().getExtras().getParcelable("leadModel");
        } catch (Exception e) {
        }

        Sales1CRMUtils.Utils.imageLoader.DisplayImage(url + leadModel.getImage_url(),
                ivDetailPict, Bitmap.Config.ARGB_8888, 500, Sales1CRMUtils.TYPE_RECT);

        tvLeadName.setText(leadModel.getFirst_name() + " " + leadModel.getLast_name());
        tvIndustry.setText(leadModel.getIndustry_name());
        tvPosition.setText(leadModel.getPosition_name());
        tvCompany.setText(leadModel.getCompany());
        tvLastUpdated.setText("Created by " + leadModel.getOwner_name());
        tvAssignTo.setText(leadModel.getAssign_to_name());
        tvCreateDate.setText(leadModel.getCreate_date());
        tvTotalEmployee.setText(leadModel.getTotal_employee() + " People");
        tvLeadStatus.setText(leadModel.getLead_status_name());
        tvLeadSource.setText(leadModel.getLead_source_name());
        tvDesc.setText(leadModel.getDescription());
        tvDistance.setText(leadModel.getDistance());

        if (leadModel.getPhone().equalsIgnoreCase("")) {
            llPhone.setVisibility(View.GONE);
        } else {
            tvPhone.setText(leadModel.getPhone());
            llPhone.setVisibility(View.VISIBLE);
        }

        if (leadModel.getEmail().equalsIgnoreCase("")) {
            llEmail.setVisibility(View.GONE);
        } else {
            tvEmail.setText(leadModel.getEmail());
            llEmail.setVisibility(View.VISIBLE);
        }

        tvAlamat.setText(leadModel.getAddress());
        tvProvinsi.setText(leadModel.getProvinsi_name());
        tvKabupaten.setText(leadModel.getKabupaten_name());
        tvKecamatan.setText(leadModel.getKecamatan_name());
        tvKelurahan.setText(leadModel.getKelurahan_name());
        tvKodepos.setText(leadModel.getKodepos());

        llEmail.setOnClickListener(click);
        llPhone.setOnClickListener(click);
        llAlamat.setOnClickListener(click);
        ivBack.setOnClickListener(click);
        ivConvert.setOnClickListener(click);
    }

    View.OnClickListener click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.ivBack:
                    finish();
                    break;
                case R.id.llEmail:
                    if (!leadModel.getEmail().equalsIgnoreCase("")) {
                        String mailto = "mailto:" + leadModel.getEmail();

                        Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
                        emailIntent.setData(Uri.parse(mailto));

                        try {
                            startActivity(Intent.createChooser(emailIntent, "Pilih Email"));
                        } catch (ActivityNotFoundException e) {
                            //TODO: Handle case where no email app is available
                        }
                    } else {
                        llEmail.setVisibility(View.GONE);
                    }
                    break;
                case R.id.llPhone:
                    final Dialog dialog = new Dialog(context);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.dialog_call);

                    llCall = (LinearLayout) dialog
                            .findViewById(R.id.llCall);
                    llSMS = (LinearLayout) dialog
                            .findViewById(R.id.llSMS);
                    llWhatsapp = (LinearLayout) dialog
                            .findViewById(R.id.llWhatsapp);

                    llCall.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (ContextCompat.checkSelfPermission(context,
                                    Manifest.permission.CALL_PHONE)
                                    != PackageManager.PERMISSION_GRANTED) {

                                ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.CALL_PHONE}, Sales1CRMUtils.REQUEST_PHONE_CALL);
                            } else {
                                callNumber(leadModel.getPhone());
                            }
                            dialog.dismiss();
                        }
                    });

                    llSMS.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            sendSMS(leadModel.getPhone());
                            dialog.dismiss();
                        }
                    });

                    llWhatsapp.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent();
                            openWhatsApp(leadModel.getPhone());
                        }
                    });
                    dialog.show();
                    break;
                case R.id.llAlamat:
                    String strUri1 = "http://maps.google.com/maps?q=loc:" + leadModel.getLatitude() + "," + leadModel.getLongitude();
                    Intent intent1 = new Intent(Intent.ACTION_VIEW, Uri.parse(strUri1));
                    intent1.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
                    startActivity(intent1);
                    break;
                case R.id.ivConvert:
                    final CustomDialog  dialogConvert = CustomDialog.setupDialogConvert(context);
                    RadioGroup radioGroup = (RadioGroup) dialogConvert.findViewById(R.id.radioGroup);
                    RadioButton rb_1 = (RadioButton) dialogConvert.findViewById(R.id.rb_1);
                    RadioButton rb_2 = (RadioButton) dialogConvert.findViewById(R.id.rb_2);
                    CustomTextView btnCancel = (CustomTextView) dialogConvert.findViewById(R.id.tvCancel);
                    CustomTextView btnSave = (CustomTextView) dialogConvert.findViewById(R.id.tvSave);

                    btnCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialogConvert.dismiss();
                        }
                    });

                    btnSave.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(rb_1.isChecked()) {
                                leadConvertPresenter.setupConvert(ApiParam.API_127,leadModel.getId(),"1");
                                dialogConvert.dismiss();
                                finish();
                            } else {
                                leadConvertPresenter.setupConvert(ApiParam.API_127, leadModel.getId(),"2");
                                dialogConvert.dismiss();
//                                dialogConvert.dismiss();
////                                Intent intent = new Intent(context, Oppo)
                            }
                        }
                    });
                    dialogConvert.show();
                    break;
                default:
                    break;
            }
        }
    };

    private void openWhatsApp(String number) {
        final String appPackageName = "com.whatsapp";

        if (Sales1CRMUtils.Utils.appInstalledOrNot(context, appPackageName)) {
            number = PhoneNumberUtils.formatNumber(number).replace("-", "");
            if (number.substring(0, 2).equalsIgnoreCase("08")) {
                number = "62" + number.substring(1);
            } else {
                number = number.replace(" ", "").replace("+", "");
            }

            Log.d("TAG", "Number : " + number);
            try {
                Intent sendIntent = new Intent("android.intent.action.MAIN");
                sendIntent.setComponent(new ComponentName("com.whatsapp", "com.whatsapp.Conversation"));
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.setType("text/plain");
                sendIntent.putExtra(Intent.EXTRA_TEXT, "");
                sendIntent.putExtra("jid", number + "@s.whatsapp.net");
                sendIntent.setPackage("com.whatsapp");
                startActivity(sendIntent);
            } catch (Exception e) {
                Toast.makeText(context, "Error/n" + e.toString(), Toast.LENGTH_SHORT).show();
            }
        } else {
            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
            } catch (ActivityNotFoundException anfe) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
            }
        }
    }

    private void callNumber(String number) {
        PackageManager packageManager = context.getPackageManager();
        if (packageManager.hasSystemFeature(PackageManager.FEATURE_TELEPHONY)) {
            // call phone
            Intent intent = new Intent(Intent.ACTION_CALL);

            intent.setData(Uri.parse("tel:" + number));

            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            startActivity(intent);
        }
    }

    private void sendSMS(String number) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("smsto:" + Uri.encode(number)));
        startActivity(intent);
    }

    @Override
    public void finishFirst(String result, DashboardFirstModel model) {
        if (result.equalsIgnoreCase("OK")) {
            if (model.getLead_edit_permission().equalsIgnoreCase("1")) {
                ivEdit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, LeadAddActivity.class);
                        intent.putExtra("leadModel", leadModel);
                        intent.putExtra("from_detail", true);
                        startActivity(intent);
                    }
                });
                ivEdit.setVisibility(View.VISIBLE);
            } else {
                ivEdit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(context, "You don't have this access, Please contact your administrator...", Toast.LENGTH_SHORT).show();
                    }
                });
                ivEdit.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void finishSecond(String result, DashboardSecondModel model) {

    }

    @Override
    public void finishThird(String result, List<DashboardThirdModel> listLead, List<DashboardThirdModel> listLead1, List<DashboardThirdModel> listOppor, List<DashboardThirdModel> listOppor1) {

    }

    @Override
    public void TotOpportunity(String result, TotalOpportunityModel totOporModel) {

    }

    @Override
    public void absence(String result, AbsenceModel model) {

    }

    @Override
    public void finishConvert(String result, LeadConvertModel model) {
        if(result.equalsIgnoreCase("OK")){
            if(model.getConvert_status().equalsIgnoreCase("1")){
                finish();
            } else {
                finish();
                Intent intent = new Intent(context, OpportunityAddActivity.class);

                intent.putExtra("account_id", model.getAccount_id());
                intent.putExtra("account_name", model.getAccount_name());
                intent.putExtra("contact", model.getContact());
                intent.putExtra("lead_id", model.getLead_id());
                intent.putExtra("convert",true);
                Log.d("TAG","ID " + model.getAccount_id());
                Log.d("TAG","NAMA " + model.getAccount_name());
                startActivity(intent);
            }

        }
    }
}
