package com.msf.project.sales1crm.model;

public class UserPermissionModel {
	private String functionCodeName;
	private String functionName;
	private String functionId;

	public void setFunctionCodeName(String functionCodeName){
		this.functionCodeName = functionCodeName;
	}

	public String getFunctionCodeName(){
		return functionCodeName;
	}

	public void setFunctionName(String functionName){
		this.functionName = functionName;
	}

	public String getFunctionName(){
		return functionName;
	}

	public void setFunctionId(String functionId){
		this.functionId = functionId;
	}

	public String getFunctionId(){
		return functionId;
	}
}
