package com.msf.project.sales1crm.callback;

import android.os.Bundle;

public interface CurrencyDialogCallback {
    void onCurrencyDialogCallback(Bundle data);
}
