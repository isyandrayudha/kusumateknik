package com.msf.project.sales1crm.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.msf.project.sales1crm.R;
import com.msf.project.sales1crm.customview.CustomTextView;
import com.msf.project.sales1crm.model.OpportunityModel;

import java.util.List;


public class OpportunityListByStageAdapter extends RecyclerView.Adapter<OpportunityListByStageAdapter.OpportunityStageHolder> {

    private Context context;
    private Bitmap icon;
    private List<OpportunityModel> listData;
    private String url, iterated;
    private int size;


    public OpportunityListByStageAdapter(Context context, List<OpportunityModel> objects) {
        this.context = context;
        this.listData = objects;
    }

    public static abstract class Row {
    }

    // This is for making Section, ex: A B C D etc
    public static final class Section extends Row {
        public final String text;

        public Section(String text) {
            this.text = text;
        }
    }

    // This is for making Row, ex: A B C D etc
    public static final class Item extends Row {
        public final OpportunityModel text;

        public Item(OpportunityModel text) {
            this.text = text;
        }

        public OpportunityModel getItem() {
            return text;
        }
    }

    private List<Row> rows;

    public void setRows(List<Row> rows) {
        this.rows = rows;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public int getItemCount() {
        return rows.size();
    }

    @NonNull
    @Override
    public OpportunityStageHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_vp_list, null);
        return new OpportunityStageHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OpportunityStageHolder holder, int position) {
        final Item item = (Item) rows.get(position);


    }


    private char[] c = new char[]{'K', 'M', 'B', 'T'};

    private String coolFormat(double n, int iteration) {
        double d = ((long) n / 100) / 10.0;
        boolean isRound = (d * 10) % 10 == 0;// true if the decimal part is
        // equal to 0 (then it's trimmed
        // anyway)
        Log.i("TAG", "num : " + d);
        size = (int) d;
        iterated = String.valueOf(c[iteration]);
        Log.i("TAG", "iteration : " + c[iteration]);
        return (d < 1000 ? // this determines the class, i.e. 'k', 'm' etc
                ((d > 99.9 || isRound || (!isRound && d > 9.99) ? // this decides
                        // whether to trim
                        // the decimals
                        (int) d * 10 / 10
                        : d + "" // (int) d * 10 / 10 drops the decimal
                ) + "" + c[iteration])
                : coolFormat(d, iteration + 1));
    }

    public class OpportunityStageHolder extends RecyclerView.ViewHolder {
        CustomTextView stageName;
        CustomTextView tvPercentage;
        CustomTextView tvsize;
        CustomTextView tvTotPrice;
        RecyclerView rcOpportunities;

        public OpportunityStageHolder(View view) {
            super(view);
            stageName = itemView.findViewById(R.id.stageName);
            tvPercentage = itemView.findViewById(R.id.tvPercentage);
            tvsize = itemView.findViewById(R.id.tvsize);
            tvTotPrice = itemView.findViewById(R.id.tvTotPrice);
            rcOpportunities = itemView.findViewById(R.id.rcOpportunities);

        }
    }
}

