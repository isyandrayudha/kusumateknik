package com.msf.project.sales1crm.model;

public class TotalNPriceOpporModel{
	private String totalOpportunity;
	private String totalPrice;

	public void setTotalOpportunity(String totalOpportunity){
		this.totalOpportunity = totalOpportunity;
	}

	public String getTotalOpportunity(){
		return totalOpportunity;
	}

	public void setTotalPrice(String totalPrice){
		this.totalPrice = totalPrice;
	}

	public String getTotalPrice(){
		return totalPrice;
	}

}
